<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */
OW::getRouter()->addRoute(new OW_Route('smileys.admin', 'smileys/admin', 'SMILEYS_CTRL_Admin', 'index'));
OW::getRouter()->addRoute(new OW_Route('smileys.admin_view', 'smileys/admin-view', 'SMILEYS_CTRL_Admin', 'view'));
OW::getRouter()->addRoute(new OW_Route('smileys.admin_reorder', 'smileys/admin-reorder', 'SMILEYS_CTRL_Admin', 'reorder'));
OW::getRouter()->addRoute(new OW_Route('smileys.admin_add', 'smileys/admin-add', 'SMILEYS_CTRL_Admin', 'add'));
OW::getRouter()->addRoute(new OW_Route('smileys.admin_edit', 'smileys/admin-edit', 'SMILEYS_CTRL_Admin', 'edit'));
OW::getRouter()->addRoute(new OW_Route('smileys.admin_delete', 'smileys/admin-delete', 'SMILEYS_CTRL_Admin', 'delete'));
OW::getRouter()->addRoute(new OW_Route('smileys.admin_add_category', 'smileys/admin-add-category', 'SMILEYS_CTRL_Admin', 'addCategory'));
OW::getRouter()->addRoute(new OW_Route('smileys.admin_delete_category', 'smileys/admin-delete-category', 'SMILEYS_CTRL_Admin', 'deleteCategory'));
OW::getRouter()->addRoute(new OW_Route('smileys.admin_set_category', 'smileys/admin-set-category', 'SMILEYS_CTRL_Admin', 'changeCaption'));

OW::getRouter()->addRoute(new OW_Route('smileys.admin-rsp', 'smileys/admin-rsp', 'SMILEYS_CTRL_Admin', 'smileysRsp'));
OW::getRouter()->addRoute(new OW_Route('smileys.smileLoader', 'smileys/loader', 'SMILEYS_CTRL_Smileys', 'getSmileysByCategory'));

SMILEYS_CLASS_EventHandler::getInstance()->init();


// ************************* Begin: Deprecated ************************** \\

$eventManager = OW::getEventManager();

function smileys_after_master_page_render( OW_Event $event )
{
    SMILEYS_CLASS_HtmlDocument::getInstance()->replaceBaseWysisyg();
    SMILEYS_CLASS_HtmlDocument::getInstance()->replaceBaseComment();
}
$eventManager->bind( OW_EventManager::ON_BEFORE_DOCUMENT_RENDER, 'smileys_after_master_page_render', 99 );

function smileys_feed_on_item_render( OW_Event $event )
{
    $params = $event->getParams();
    $entityTypes = array('forum-topic', 'user-comment', 'question');

    if ( !in_array($params['action']['entityType'], $entityTypes) )
    {
        return;
    }
    
    $data = $event->getData();
    $language = OW::getLanguage();

    switch ( $params['action']['entityType'] )
    {
        case 'forum-topic':
            $service = FORUM_BOL_ForumService::getInstance();
            $postCount = $service->findTopicPostCount( $params['action']['entityId'] ) - 1;

            if ( !$postCount )
            {
                return;
            }

            $event->setData($data);

            $postIds = array();
            foreach ( $params['activity'] as $activity )
            {
                if ( $activity['activityType'] == 'forum-post' )
                {
                    $postIds[] = $activity['data']['postId'];
                }
            }

            if ( empty($postIds) )
            {
                return;
            }

            $postDto = null;
            foreach ( $postIds as $pid )
            {
                $postDto = $service->findPostById( $pid );
                if ( $postDto !== null )
                {
                    break;
                }
            }

            if ( $postDto === null )
            {
                return;
            }

            $postUrl = $service->getPostUrl( $postDto->topicId, $postDto->id );
            
            $content = preg_replace( SMILEYS_CLASS_HtmlDocument::PATTERN, '', strip_tags(UTIL_String::truncate(str_replace("&nbsp;", '', $postDto->text), 1000, '...'), '<img>') );
            $usersData = BOL_AvatarService::getInstance()->getDataForUserAvatars( array($postDto->userId), true, true, true, false );

            $avatarData = $usersData[$postDto->userId];
            $postUrl = $service->getPostUrl( $postDto->topicId, $postDto->id );

            $ipcContent = OW::getThemeManager()->processDecorator( 'mini_ipc', array(
                    'avatar' => $avatarData, 'profileUrl' => $avatarData['url'], 'displayName' => $avatarData['title'], 'content' => $content) );

            $data['assign']['activity'] = array('template' => 'activity', 'vars' => array(
                    'title' => $language->text('forum', 'feed_activity_last_reply', array('postUrl' => $postUrl)),
                    'content' => $ipcContent
                ));
            break;
        case 'user-comment':
        case 'question':
            if ( isset($data['string']['vars']['comment']) )
            {
                $data['string']['vars']['comment'] = preg_replace( '/\[([^\/]+\/[^\/]+)\]/', '<img src="' . OW::getPluginManager()->getPlugin('smileys')->getUserFilesUrl() . 'images/' . '${1}.gif" />', $data['string']['vars']['comment'] );
            }
            else
            {
                $data['string'] = preg_replace( '/\[([^\/]+\/[^\/]+)\]/', '<img src="' . OW::getPluginManager()->getPlugin('smileys')->getUserFilesUrl() . 'images/' . '${1}.gif" />', $data['string'] );
            }
            break;
        case 'user-status':
            OW::getDocument()->addScriptDeclarationBeforeIncludes(
                UTIL_JsGenerator::composeJsString('
                    ;window["commentedotir-user-status" + {$actionId}] = {$status}
                    ',
                    array(
                        'actionId' => $params['action']['id'],
                        'status' => UTIL_HtmlTag::autoLink($data['data']['status'])
                    )
                )
            );
            
            if ( isset($data['string']['vars']['comment']) )
            {
                $data['string']['vars']['comment'] = preg_replace( '/\[([^\/]+\/[^\/]+)\]/', '<img src="' . OW::getPluginManager()->getPlugin('smileys')->getUserFilesUrl() . 'images/' . '${1}.gif" />', $data['string']['vars']['comment'] );
            }
            elseif ( isset($data['data']['status']) )
            {
                $data['data']['status'] = preg_replace( '/\[([^\/]+\/[^\/]+)\]/', '<img src="' . OW::getPluginManager()->getPlugin('smileys')->getUserFilesUrl() . 'images/' . '${1}.gif" />', $data['data']['status'] );
            }
            else
            {
                $data['string'] = preg_replace( '/\[([^\/]+\/[^\/]+)\]/', '<img src="' . OW::getPluginManager()->getPlugin('smileys')->getUserFilesUrl() . 'images/' . '${1}.gif" />', $data['string'] );
            }
            break;
    }

    $event->setData($data);
}
OW::getEventManager()->bind( 'feed.on_item_render', 'smileys_feed_on_item_render');

// *********************** End: Deprecated ****************************** \\
