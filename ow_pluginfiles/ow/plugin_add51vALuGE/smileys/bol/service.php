<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow <kainisoft@gmail.com>
 * @package ow_plugins.smileys.bol
 * @since 1.0
 */
class SMILEYS_BOL_Service
{
    CONST SMILEYS_DIR_NAME = 'smileys';
    CONST PROHIBIT_CHAR_REPLACER = '_';
    
    private static $classInstance;
    
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }
    
    private $plugin;
    private $smilyesDto;
    
    private $prohibitedChars;

    private function __construct()
    {
        $this->smilyesDto = SMILEYS_BOL_SmileysDao::getInstance();
        $this->plugin = OW::getPluginManager()->getPlugin( 'smileys' );
        
        $this->prohibitedChars = array('"', "'", '<', '>');
    }
    
    public function findSmileById( $id )
    {
        return $this->smilyesDto->findById($id);
    }

    public function getSmileysDir()
    {
        return $this->plugin->getUserFilesDir() . DS . self::SMILEYS_DIR_NAME . DS;
    }
    
    public function getSmileysUrl()
    {
        return $this->plugin->getUserFilesUrl() . self::SMILEYS_DIR_NAME . '/';
    }

    public function getAllSmileys()
    {
        return $this->smilyesDto->getAllSmileys();
    }

    public function updateSmileysOrder( $order )
    {
        return $this->smilyesDto->updateSmileysOrder($order);
    }
    
    public function isSmileCodeBusy( $code )
    {
        return $this->smilyesDto->findSmileByCode($code) !== NULL;
    }
    
    public function sanitizeCode( $code )
    {
        return trim(str_replace($this->prohibitedChars, self::PROHIBIT_CHAR_REPLACER, $code));
    }

    public function getProhibitedChars()
    {
        return $this->prohibitedChars;
    }
    
    public function getFreeOrder()
    {
        $maxOrder = $this->smilyesDto->getMaxOrder();
        
        return ++$maxOrder;
    }
    
    public function getSmileysKeyPair()
    {
        static $keyPair = array();
        
        if ( empty($keyPair) )
        {
            foreach ( $this->getAllSmileys() as $smile )
            {
                $keyPair[$smile->code] = $smile->name;
            }
        }
        
        return $keyPair;
    }
    
    public function getSmileysKeyPairWrapInTag()
    {
        static $keyPair = array();
        
        if ( empty($keyPair) )
        {
            $url = $this->getSmileysUrl();
            
            foreach ( $this->getSmileysKeyPair() as $code => $name )
            {
                $keyPair[$code] = '<img src="' . $url . $name . '" title="' . $code . '" />';
            }
        }
        
        return $keyPair;
    }

    public function replace( $text )
    {
        $json = $this->getSmileysKeyPairWrapInTag();
        
        return str_ireplace(array_keys($json), array_values($json), $text);
    }
    
    public function getFreeSmileCategory()
    {
        $maxId = $this->smilyesDto->getMaxId();
        
        return ++$maxId;
    }
    
    public function findSmileysByCategory( $categoryId )
    {
        return $this->smilyesDto->findSmileysByCategory($categoryId);
    }
    
    public function deleteSmileysByCategory( $categoryId )
    {
        return $this->smilyesDto->deleteSmileysByCategory($categoryId);
    }
    
    public function setSmileCaption( $smileId, $categoryId )
    {
        return $this->smilyesDto->setSmileCaption($smileId, $categoryId);
    }

    // ************************* Begin: Deprecated ************************** \\
    
    public function getSmilesCategories()
    {
        $categories = scandir( $this->plugin->getUserFilesDir() . 'images' . DS );
        unset( $categories[0] );
        unset( $categories[1] );

        return array_unique( array_map('strtolower', $categories) );
    }
    
    public function getSmileysByCategory( $category )
    {
        $dir = $this->plugin->getUserFilesDir() . 'images' . DS . strtolower( $category );
        
        if ( !empty($category) && file_exists($dir) )
        {
            $smileys = scandir( $dir );
            unset( $smileys[0] );
            unset( $smileys[1] );
            
            return array_unique( array_map('strtolower', $smileys) );
        }
        else
        {
            return array();
        }
    }
    
    public function getThemeList()
    {
        $themes = scandir( $this->plugin->getStaticDir() . 'css' . DS . 'ui' );
        unset( $themes[0] );
        unset( $themes[1] );
        
        return $themes;
    }
    
    // *********************** End: Deprecated ****************************** \\
}
