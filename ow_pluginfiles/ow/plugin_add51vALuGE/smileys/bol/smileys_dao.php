<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow <kainisoft@gmail.com>
 * @package ow_plugins.smileys.bol
 * @since 1.0
 */
class SMILEYS_BOL_SmileysDao extends OW_BaseDao
{
    CONST ORDER = 'order';
    CONST CODE = 'code';
    CONST CATEGORY = 'category';
    CONST IS_CAPTION = 'isCaption';
    
    private static $classInstance;

    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public function getDtoClassName()
    {
        return 'SMILEYS_BOL_Smileys';
    }
    
    public function getTableName()
    {
        return OW_DB_PREFIX . 'smileys_smileys';
    }
    
    public function getAllSmileys()
    {
        return $this->dbo->queryForObjectList('SELECT * FROM `' . $this->getTableName() . '` ORDER BY `' . self::ORDER . '`, `id`', $this->getDtoClassName());
    }
    
    public function updateSmileysOrder( $order )
    {
        if ( empty($order) )
        {
            return FALSE;
        }
        
        $sql = 'UPDATE `' . $this->getTableName() . '` SET `' . self::ORDER . '` = CASE `id` ';
        
        foreach ( $order as $id => $value )
        {
            $sql .= "WHEN $id THEN $value ";
        }
        
        $sql .= 'END WHERE `id` IN(' . implode(',', array_map('intval', array_keys($order))) . ')';
        
        return $this->dbo->update($sql);
    }
    
    public function findSmileByCode( $code )
    {
        if ( empty($code) )
        {
            return NULL;
        }
        
        $example = new OW_Example();
        $example->andFieldEqual(self::CODE, $code);
        
        return $this->findObjectByExample($example);
    }
    
    public function getMaxOrder()
    {
        $sql = 'SELECT MAX(`' . self::ORDER . '`) FROM `' . $this->getTableName() . '`';
        
        return $this->dbo->queryForColumn($sql);
    }
    
    public function getMaxId()
    {
        $sql = 'SELECT MAX(`' . self::CATEGORY . '`) FROM `' . $this->getTableName() . '`';
        
        return $this->dbo->queryForColumn($sql);
    }
    
    public function findSmileysByCategory( $categoryId )
    {
        if ( empty($categoryId) )
        {
            return array();
        }
        
        $sql = 'SELECT * '
                . 'FROM `' . $this->getTableName() . '` '
                . 'WHERE `' . self::CATEGORY . '` = :category';
        
        return $this->dbo->queryForObjectList($sql, $this->getDtoClassName(), array('category' => $categoryId));
    }
    
    public function deleteSmileysByCategory( $categoryId )
    {
        if ( empty($categoryId) )
        {
            return FALSE;
        }
        
        $example = new OW_Example();
        $example->andFieldEqual(self::CATEGORY, $categoryId);
        
        return $this->deleteByExample($example);
    }
    
    public function setSmileCaption( $smileId, $categoryId )
    {
        if ( empty($_POST['id']) || empty($_POST['categoryId']) )
        {
            return FALSE;
        }
        
        $sql = 'UPDATE `' . $this->getTableName() . '` '
                . 'SET `' . self::IS_CAPTION . '` = 0 '
                . 'WHERE `' . self::CATEGORY . '` = :category; '
                . 'UPDATE `' . $this->getTableName() . '` '
                . 'SET `' . self::IS_CAPTION . '` = 1 '
                . 'WHERE `id` = :id';
        
        return (bool)$this->dbo->query($sql, array('id' => $smileId, 'category' => $categoryId));
    }
}

/** 
 * 
 *
 * @author Kairat Bakytow <kainisoft@gmail.com>
 * @package ow_plugins.smileys.bol
 * @since 1.0
 */
class SMILEYS_BOL_Smileys extends OW_Entity
{
    public $category;
    public $isCaption;
    public $order;
    public $code;
    public $name;
}
