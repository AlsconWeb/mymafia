<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */
$minId = Updater::getDbo()->queryForColumn('SELECT MIN(`id`) FROM `' . OW_DB_PREFIX . 'smileys_smileys`');

$sql = array(
    'ALTER TABLE  `' . OW_DB_PREFIX . 'smileys_smileys` ADD  `category` INT UNSIGNED NOT NULL AFTER  `id` ;',
    'UPDATE `' . OW_DB_PREFIX . 'smileys_smileys` SET `category` = 1 ;',
    'ALTER TABLE  `' . OW_DB_PREFIX . 'smileys_smileys` ADD  `isCaption` TINYINT( 1 ) UNSIGNED NOT NULL AFTER  `category` ;',
    'UPDATE `' . OW_DB_PREFIX . 'smileys_smileys` SET `isCaption` = 1 WHERE `id` = ' . (int)$minId
);

foreach ( $sql as $s )
{
    try
    {
        Updater::getDbo()->query($s);
    }
    catch ( Exception $ex )
    {
        Updater::getLogger()->addEntry(json_encode($ex));
    }
}

Updater::getLanguageService()->importPrefixFromZip(dirname(__FILE__) . DS . 'langs.zip', 'smileys');
