<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow
 * @package ow_plugins.smileys.controllers
 * @since 1.0
 * @deprecated since version 16
 */
class SMILEYS_CTRL_Smileys extends OW_ActionController
{
    public function getSmileysByCategory( array $param = array() )
    {
        exit(json_encode(SMILEYS_BOL_Service::getInstance()->getSmileysByCategory($_POST['category'])));
    }
}
