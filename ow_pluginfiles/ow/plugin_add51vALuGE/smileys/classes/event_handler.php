<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow <kainisoft@gmail.com>
 * @package ow_plugins.smileys.classes
 * @since 1.0
 */
class SMILEYS_CLASS_EventHandler
{
    private static $classInstance;

    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }
    
    private $service;

    private function __construct()
    {
        $this->service = SMILEYS_BOL_Service::getInstance();
    }
    
    public function init()
    {
        OW::getEventManager()->bind(OW_EventManager::ON_FINALIZE, array($this, 'onFinalize'));
        OW::getEventManager()->bind('base.comment_item_process', array($this, 'commentProcess'));
        OW::getEventManager()->bind('feed.on_item_render', array($this, 'onFeedItemRender'), 9999);
        OW::getEventManager()->bind('feed.after_render_format', array($this, 'onAfterRenderFormat'));
        OW::getEventManager()->bind('questions.on_list_item_render', array($this, 'onQuestionRender'));
        OW::getEventManager()->bind('questions.on_question_render', array($this, 'onQuestionRender'));

        OW::getEventManager()->bind('forum.after_topic_edit', array($this, 'onForumTopicSave'));
        OW::getEventManager()->bind('forum.after_topic_add', array($this, 'onForumTopicSave'));
        OW::getEventManager()->bind('forum.add_post', array($this, 'onForumPostSave'));

        OW::getEventManager()->bind('event_after_create_event', array($this, 'onEventSave'));
        OW::getEventManager()->bind('event_after_event_edit', array($this, 'onEventSave'));

        OW::getEventManager()->bind('blogs.after_edit', array($this, 'onBlogSave'));
        OW::getEventManager()->bind('blogs.after_add', array($this, 'onBlogSave'));

        OW::getEventManager()->bind('groups_group_create_complete', array($this, 'onGroupSave'));
        OW::getEventManager()->bind('groups_group_edit_complete', array($this, 'onGroupSave'));
        
        OW::getEventManager()->bind('video.add_clip', array($this, 'onVideoCreate'), 1000);
        OW::getEventManager()->bind('video.after_edit', array($this, 'onVideoEdit'));
        
        OW::getEventManager()->bind('mailbox.before_create_conversation', array($this, 'onBeforeCreateConversation'));
        OW::getEventManager()->bind('acomments.repliesReady', array($this, 'onAcommentsRepliesReady'));
    }
    
    public function genericInit()
    {
        OW::getEventManager()->bind(OW_EventManager::ON_BEFORE_DOCUMENT_RENDER, array($this, 'onBeforeDocumentRender'));
        OW::getEventManager()->bind('base.comment_item_process', array($this, 'commentProcess'));
        OW::getEventManager()->bind('feed.on_item_render', array($this, 'onFeedItemRender'), 9999);
        OW::getEventManager()->bind('feed.after_render_format', array($this, 'onAfterRenderFormat'));
    }

    public function onFinalize( OW_event $event )
    {
        if ( OW::getRequest()->isAjax() )
        {
            return;
        }
        
        $plugin = OW::getPluginManager()->getPlugin('smileys');
        
        if ( $plugin->getDto()->getBuild() < 20 )
        {
            return;
        }
        
        $cmp = new SMILEYS_CMP_Panel();
        OW::getDocument()->appendBody($cmp->render());
    }
    
    public function onBeforeDocumentRender()
    {
        $pregQuote = array();
        $smileys = $this->service->getSmileysKeyPairWrapInTag();
        
        foreach ( $smileys as $code => $smile )
        {
            $pregQuote[] = preg_quote($code, '/');
        }
        
        $pattern = '/(?:' . implode('|', $pregQuote) . ')(?:(?![^<]*?>))/i';
        $body = preg_replace_callback($pattern, 'SMILEYS_CLASS_EventHandler::mobileContentReplace', OW::getDocument()->getBody());
        OW::getDocument()->setBody($body);
    }
    
    public static function mobileContentReplace( $code )
    {
        static $smileys = array();
        
        if ( empty($smileys) )
        {
            $smileys = SMILEYS_BOL_Service::getInstance()->getSmileysKeyPairWrapInTag();
        }
        
        if ( isset($smileys[$code[0]]) )
        {
            return $smileys[$code[-0]];
        }
        
        return '';
    }

    public function commentProcess( BASE_CLASS_EventProcessCommentItem $event )
    {
        $message = $this->service->replace($event->getDataProp('content'));
        $event->setDataProp('content', $message);
    }
    
    public function onFeedItemRender( OW_Event $event )
    {
        $data = $event->getData();
        
        if ( !empty($data['string']) )
        {
            $data['string'] = $this->service->replace($data['string']);
        }
        
        $event->setData($data);
    }
    
    public function onAfterRenderFormat( OW_Event $event )
    {
        $params = $event->getParams();
        
        if ( empty($params['vars']) || empty($params['vars']['status']) )
        {
            return;
        }
        
        static $pattern = NULL;
        
        if ( $pattern === NULL )
        {
            $pregQuote = array();
            $smileys = $this->service->getSmileysKeyPairWrapInTag();

            foreach ( $smileys as $code => $smile )
            {
                $pregQuote[] = preg_quote($code, '/');
            }

            $pattern = '/(?:' . implode('|', $pregQuote) . ')(?:(?![^<]*?>))/i';
        }
        
        $data = preg_replace_callback($pattern, 'SMILEYS_CLASS_EventHandler::mobileContentReplace', $event->getData());
        $event->setData($data);
    }

    public function onQuestionRender( OW_Event $event )
    {
        $data = $event->getData();
        
        if ( !empty($data['text']) )
        {
            $data['text'] = $this->service->replace($data['text']);
        }
        
        $event->setData($data);
    }
    
    private function replace( $text )
    {
        return BOL_TextFormatService::getInstance()->processWsForOutput($this->service->replace($text), array('buttons' => array(
            BOL_TextFormatService::WS_BTN_BOLD,
            BOL_TextFormatService::WS_BTN_ITALIC,
            BOL_TextFormatService::WS_BTN_UNDERLINE,
            BOL_TextFormatService::WS_BTN_LINK,
            BOL_TextFormatService::WS_BTN_ORDERED_LIST,
            BOL_TextFormatService::WS_BTN_UNORDERED_LIST,
            BOL_TextFormatService::WS_BTN_IMAGE,
            BOL_TextFormatService::WS_BTN_VIDEO
        )));
    }

    public function onBlogSave( OW_Event $event )
    {
        $params = $event->getParams();


        $dto = PostDao::getInstance();
        $blogDto = $dto->findById($params['postId']);

        if ( $blogDto )
        {
            $blogDto->post = $this->replace($_POST['post']);
            $dto->save($blogDto);
        }
    }

    public function onForumTopicSave( OW_Event $event )
    {
        $params = $event->getParams();

        $postDto = FORUM_BOL_ForumService::getInstance()->findTopicFirstPost($params['topicId']);

        if ( $postDto )
        {
            $postDto->text = $this->replace($_POST['text']);

            $dto = FORUM_BOL_PostDao::getInstance();
            $dto->save($postDto);
        }
    }

    public function onForumPostSave( OW_Event $event )
    {

        $params = $event->getParams();

        $dto = FORUM_BOL_PostDao::getInstance();
        $postDto = $dto->findById($params['postId']);

        if ( $postDto )
        {
            $postDto->text = $this->replace($_POST['text']);
            $dto->save($postDto);
        }
    }

    public function onEventSave( OW_Event $event )
    {
        $params = $event->getParams();
        
        if ( OW::getPluginManager()->isPluginActive('eventx') )
        {
            $dto = EVENTX_BOL_EventDao::getInstance();
            $id = $params['eventDto']->id;
        }
        else
        {
            $dto = EVENT_BOL_EventDao::getInstance();
            $id = $params['eventId'];
        }

        $eventDto = $dto->findById($id);

        if ( $eventDto === NULL )
        {
            return;
        }

        $eventDto->setDescription($this->replace($_POST['desc']));
        $dto->save($eventDto);
    }
    
    public function onGroupSave( OW_Event $event )
    {
        $params = $event->getParams();
        $groupDto = GROUPS_BOL_GroupDao::getInstance()->findById($params['groupId']);
        
        if ( $groupDto === NULL )
        {
            return;
        }

        $groupDto->description = $this->replace($_POST['description']);
        GROUPS_BOL_GroupDao::getInstance()->save($groupDto);
    }
    
    public function onVideoCreate( OW_Event $event )
    {
        $data = $event->getData();
        
        if ( empty($data['id']) || ($clipDto = VIDEO_BOL_ClipDao::getInstance()->findById($data['id'])) === NULL )
        {
            return;
        }
        
        if ( isset($_POST['feedType']) && $_POST['feedType'] == 'user' )
        {
            $clipDto->description = $this->service->replace($_POST['status']);
        }
        else
        {
            $clipDto->description = $this->replace($_POST['description']);
        }
        
        VIDEO_BOL_ClipDao::getInstance()->save($clipDto);
    }
    
    public function onVideoEdit( OW_Event $event )
    {
        $params = $event->getParams();
        
        if ( empty($params['clipId']) || ($clipDto = VIDEO_BOL_ClipDao::getInstance()->findById($params['clipId'])) === NULL )
        {
            return;
        }
        
        $clipDto->description = $this->replace($_POST['description']);
        VIDEO_BOL_ClipDao::getInstance()->save($clipDto);
    }
    
    public function onBeforeCreateConversation( OW_Event $event )
    {
        $data = $event->getData();
        
        if ( empty($data['message']) ) return;
        
        $smileys = $this->service->getSmileysKeyPair();
        $pattern = '/<img src="' . preg_quote($this->service->getSmileysUrl(), '/') . '([^"]+)" \/>/i';
        $message = $this->replace($_POST['message']);
        
        $data['message'] = preg_replace_callback($pattern, 'SMILEYS_CLASS_EventHandler::pregReplace', $message);
        
        $event->setData($data);
    }
    
    public static function pregReplace( $match )
    {
        static $smileys = NULL;
        
        if ( empty($smileys) )
        {
            $smileys = SMILEYS_BOL_Service::getInstance()->getSmileysKeyPair();
        }
        
        if ( ($key = array_search($match[1], $smileys)) !== FALSE )
        {
            return $key;
        }

        return '';
    }
    
    public function onAcommentsRepliesReady( OW_Event $event )
    {
        $data = array();
        
        foreach ( $event->getData() as $replie )
        {
            $data[$replie['id']] = $replie;
            $data[$replie['id']]['message'] = $this->service->replace($replie['message']);
        }
        
        $event->setData($data);
    }
}
