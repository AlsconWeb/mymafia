<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow <kainisoft@gmail.com>
 * @package ow_plugins.smileys.classes
 * @since 1.0
 */
class SMILEYS_CLASS_AddForm extends Form
{
    CONST FORM_NAME = 'smile-add';
    CONST ELEMENT_CATEGORY = 'category';
    CONST ELEMENT_SMILE_CODE = 'smile-code';
    CONST ELEMENT_FILE = 'smile-file';
    CONST ELEMENT_SUBMIT = 'save';
    
    public function __construct( $categoryId = NULL )
    {
        parent::__construct(self::FORM_NAME);
        
        $this->setEnctype(Form::ENCTYPE_MULTYPART_FORMDATA);
        $this->setAction(OW::getRouter()->urlForRoute('smileys.admin_add'));
        
        $hidden = new HiddenField(self::ELEMENT_CATEGORY);
        $hidden->setValue($categoryId);
        $this->addElement($hidden);

        $code = new TextField(self::ELEMENT_SMILE_CODE);
        $code->setRequired();
        $code->addValidator(new SMILEYS_CLASS_SmileCodeValidator());
        $code->setLabel(OW::getLanguage()->text('smileys', 'edit_code_label'));
        $code->setDescription(OW::getLanguage()->text('smileys', 'prohibited_chars_desc', array(
            'prohibited' => implode(',', SMILEYS_BOL_Service::getInstance()->getProhibitedChars()),
            'replacer' => SMILEYS_BOL_Service::PROHIBIT_CHAR_REPLACER
        )));
        $this->addElement($code);
        
        $file = new FileField(self::ELEMENT_FILE);
        $file->addValidator(new SMILEYS_CLASS_FileValidator(self::ELEMENT_FILE));
        $file->addAttribute('accept', 'image/jpeg,image/png,image/gif');
        $file->setLabel(OW::getLanguage()->text('smileys', 'file_label'));
        $file->setDescription(OW::getLanguage()->text('smileys', 'file_desc'));
        $this->addElement($file);

        $submit = new Submit(self::ELEMENT_SUBMIT);
        $submit->setValue(OW::getLanguage()->text('smileys', 'smile_edit_save'));
        $this->addElement($submit);
    }
}
