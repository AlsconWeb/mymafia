<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow
 * @package ow_plugins.smileys.components
 * @since 1.0
 */
class SMILEYS_CMP_Panel extends OW_Component
{
    public function __construct()
    {
        parent::__construct();
        
        $service = SMILEYS_BOL_Service::getInstance();
        $plugin = OW::getPluginManager()->getPlugin('smileys');
        $document = OW::getDocument();
        
        $document->addStyleSheet($plugin->getStaticCssUrl() . 'smileys.css');

        $document->addScriptDeclarationBeforeIncludes(
            UTIL_JsGenerator::composeJsString(
                ';window.SMILEYSPARAMS = Object.defineProperties({}, {
                    smileysUrl: {value: {$smileysUrl}},
                    smileys: {value: {$smileys}},
                    btnBackground: {value: {$backgroundUrl}}
                });Object.freeze(window.SMILEYSPARAMS);', array(
                    'smileysUrl' => $service->getSmileysUrl(),
                    'smileys' => $service->getSmileysKeyPair(),
                    'backgroundUrl' => $plugin->getStaticUrl() . 'images/wysiwyg.png'
                )
            )
        );

        $document->addScript($plugin->getStaticJsUrl() . 'smileys.js', 'text/javascript', 9999);

        $this->assign('width', (int)OW::getConfig()->getValue('smileys', 'width'));
        $this->assign('url', $service->getSmileysUrl());
        
        $smileys = array();
        $captions = array();
        
        foreach ( $service->getAllSmileys() as $smile )
        {
            if ( !isset($smileys[$smile->category]) )
            {
                $smileys[$smile->category] = array();
            }
            
            $smileys[$smile->category][] = $smile;
            
            if ( !empty($smile->isCaption) && !isset($captions[$smile->category]) )
            {
                $captions[$smile->category] = $smile->name;
            }
        }
        
        $this->assign('captions', $captions);
        
        if ( count($smileys) === 1 )
        {
            $keys = array_keys($smileys);
            $this->assign('smileys', $smileys[$keys[0]]);
            $this->assign('isSingle', TRUE);
        }
        else
        {
            $this->assign('smileys', $smileys);
            $this->assign('isSingle', FALSE);
        }
    }
}
