<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow
 * @package ow_plugins.smileys.components
 * @since 1.0
 */
class SMILEYS_CMP_AddCategory extends OW_Component
{
    public function __construct()
    {
        parent::__construct();
        
        $this->addForm(new SMILEYS_CLASS_AddCategoryForm());
    }
}
