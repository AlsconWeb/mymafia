<?php
/* Smarty version 3.1.29, created on 2019-01-29 16:18:45
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/smileys/views/components/panel.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5c5052b5646982_17425661',
  'file_dependency' => 
  array (
    '6267c7468cf32958e49c189be4bef384596ec4af' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/smileys/views/components/panel.html',
      1 => 1545129112,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c5052b5646982_17425661 ($_smarty_tpl) {
?>


<div id="smileys-panel" class="smileys_panel smileys_tooltip_top_right" style="width: <?php echo $_smarty_tpl->tpl_vars['width']->value;?>
px">
    <?php if (!$_smarty_tpl->tpl_vars['isSingle']->value) {?>
        <div class="smileys_tooltip_body clearfix" style="padding: 0">
            <div class="smile_tab_container_top">
                <?php
$_from = $_smarty_tpl->tpl_vars['captions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_caption_0_saved_item = isset($_smarty_tpl->tpl_vars['caption']) ? $_smarty_tpl->tpl_vars['caption'] : false;
$__foreach_caption_0_saved_key = isset($_smarty_tpl->tpl_vars['id']) ? $_smarty_tpl->tpl_vars['id'] : false;
$_smarty_tpl->tpl_vars['caption'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['id'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['caption']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['caption']->value) {
$_smarty_tpl->tpl_vars['caption']->_loop = true;
$__foreach_caption_0_saved_local_item = $_smarty_tpl->tpl_vars['caption'];
?>
                    <a href="javascript://" class="smileys_tab smileys" data-category-id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['caption']->value;?>
" data-title="<?php echo $_smarty_tpl->tpl_vars['caption']->value;?>
" class="smileys" />
                    </a>
                <?php
$_smarty_tpl->tpl_vars['caption'] = $__foreach_caption_0_saved_local_item;
}
if ($__foreach_caption_0_saved_item) {
$_smarty_tpl->tpl_vars['caption'] = $__foreach_caption_0_saved_item;
}
if ($__foreach_caption_0_saved_key) {
$_smarty_tpl->tpl_vars['id'] = $__foreach_caption_0_saved_key;
}
?>
            </div>
            <?php
$_from = $_smarty_tpl->tpl_vars['smileys']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_smile_1_saved_item = isset($_smarty_tpl->tpl_vars['smile']) ? $_smarty_tpl->tpl_vars['smile'] : false;
$__foreach_smile_1_saved_key = isset($_smarty_tpl->tpl_vars['id']) ? $_smarty_tpl->tpl_vars['id'] : false;
$_smarty_tpl->tpl_vars['smile'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['id'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['smile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['smile']->value) {
$_smarty_tpl->tpl_vars['smile']->_loop = true;
$__foreach_smile_1_saved_local_item = $_smarty_tpl->tpl_vars['smile'];
?>
                <div class="clearfix smilyes_category" id="smilyes-category-<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" data-category-id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
" style="display: none">
                    <?php
$_from = $_smarty_tpl->tpl_vars['smile']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_s_2_saved_item = isset($_smarty_tpl->tpl_vars['s']) ? $_smarty_tpl->tpl_vars['s'] : false;
$_smarty_tpl->tpl_vars['s'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['s']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['s']->value) {
$_smarty_tpl->tpl_vars['s']->_loop = true;
$__foreach_s_2_saved_local_item = $_smarty_tpl->tpl_vars['s'];
?>
                        <img src="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['s']->value->name;?>
" data-title="<?php echo $_smarty_tpl->tpl_vars['s']->value->code;?>
" data-code="<?php echo $_smarty_tpl->tpl_vars['s']->value->code;?>
" />
                    <?php
$_smarty_tpl->tpl_vars['s'] = $__foreach_s_2_saved_local_item;
}
if ($__foreach_s_2_saved_item) {
$_smarty_tpl->tpl_vars['s'] = $__foreach_s_2_saved_item;
}
?>
                </div>
            <?php
$_smarty_tpl->tpl_vars['smile'] = $__foreach_smile_1_saved_local_item;
}
if ($__foreach_smile_1_saved_item) {
$_smarty_tpl->tpl_vars['smile'] = $__foreach_smile_1_saved_item;
}
if ($__foreach_smile_1_saved_key) {
$_smarty_tpl->tpl_vars['id'] = $__foreach_smile_1_saved_key;
}
?>
            <div class="smile_tab_container_bottom">
                <?php
$_from = $_smarty_tpl->tpl_vars['captions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_caption_3_saved_item = isset($_smarty_tpl->tpl_vars['caption']) ? $_smarty_tpl->tpl_vars['caption'] : false;
$__foreach_caption_3_saved_key = isset($_smarty_tpl->tpl_vars['id']) ? $_smarty_tpl->tpl_vars['id'] : false;
$_smarty_tpl->tpl_vars['caption'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['id'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['caption']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value => $_smarty_tpl->tpl_vars['caption']->value) {
$_smarty_tpl->tpl_vars['caption']->_loop = true;
$__foreach_caption_3_saved_local_item = $_smarty_tpl->tpl_vars['caption'];
?>
                    <a href="javascript://" class="smileys_tab smileys" data-category-id="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['caption']->value;?>
" data-title="<?php echo $_smarty_tpl->tpl_vars['caption']->value;?>
" class="smileys" />
                    </a>
                <?php
$_smarty_tpl->tpl_vars['caption'] = $__foreach_caption_3_saved_local_item;
}
if ($__foreach_caption_3_saved_item) {
$_smarty_tpl->tpl_vars['caption'] = $__foreach_caption_3_saved_item;
}
if ($__foreach_caption_3_saved_key) {
$_smarty_tpl->tpl_vars['id'] = $__foreach_caption_3_saved_key;
}
?>
            </div>
        </div>
    <?php } else { ?>
        <div class="smileys_tooltip_body" style="padding: 0">
            <div class="clearfix smilyes_category" id="smilyes-category-0" data-category-id="0" style="display: none">
                <?php
$_from = $_smarty_tpl->tpl_vars['smileys']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_smile_4_saved_item = isset($_smarty_tpl->tpl_vars['smile']) ? $_smarty_tpl->tpl_vars['smile'] : false;
$_smarty_tpl->tpl_vars['smile'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['smile']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['smile']->value) {
$_smarty_tpl->tpl_vars['smile']->_loop = true;
$__foreach_smile_4_saved_local_item = $_smarty_tpl->tpl_vars['smile'];
?>
                    <img src="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['smile']->value->name;?>
" data-title="<?php echo $_smarty_tpl->tpl_vars['smile']->value->code;?>
" data-code="<?php echo $_smarty_tpl->tpl_vars['smile']->value->code;?>
" />
                <?php
$_smarty_tpl->tpl_vars['smile'] = $__foreach_smile_4_saved_local_item;
}
if ($__foreach_smile_4_saved_item) {
$_smarty_tpl->tpl_vars['smile'] = $__foreach_smile_4_saved_item;
}
?>
            </div>
        </div>
    <?php }?>
</div>
<?php }
}
