/**
 * Copyright (c) 2013 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package ow_plugins.oacvote.static.js
 * @since 1.5
 */

var OAVote = function( params ){
    this.params = {};
    var self = this;
    $.extend(this.params, params);
    var $context = $('#'+params.cmpId);
    this.$totalC = $('.oac_total_votes', $context);
    this.$upC = $('.oac_vote_up', $context);
    this.$downC = $('.oac_vote_down', $context);
    this.$upCounter = $('.oa_votes_count_up', $context);
    this.$downCounter = $('.oa_votes_count_down', $context);
    this.$upW = this.$upC.closest('.ow_newsfeed_btn_wrap');
    this.$downW = this.$downC.closest('.ow_newsfeed_btn_wrap');
    this.$cmntItem = $context.closest('.' + this.params.parentClass);
    this.$cmntItemCnt = $('.oac_cont', this.$cmntItem);

    if( params.total < OAVoteData.actionLevel ){
        this.addNegativeBehav();
    }

    if( !OAVoteData.displayControls ){
        this.$cmntItem.hover(
            function(){
                self.$upW.show();
                self.$downW.show();
            },
            function(){
                self.$upW.hide();
                self.$downW.hide();
            }
        );
    }
    
    if( params.userVote === false ){
        var loginMsg = function(){
            OW.error(OAVoteData.loginMessage);
        };

        this.$upC.click(loginMsg);
        this.$downC.click(loginMsg);
        this.initUserList();
        return;
    }
    
    this.setVote(params.userVote);
    this.updateView();
}

OAVote.prototype = {
    addNegativeBehav: function(){
        var self = this;
        if( OAVoteData.actionFade ){
            this.$cmntItem.hover(
                function(){
                    self.$cmntItemCnt.animate({opacity:1}, 500);
                },
                function(){
                    self.$cmntItemCnt.animate({opacity:OAVoteData.opacityLevel}, 500);
                }
            );
        }
        
        if( OAVoteData.actionHide ){
            $('.oachide_h', self.$cmntItem).one('click', 
                function(){
                    self.$cmntItemCnt.show();
                    $(this).parent().remove();
                }
            );
        }
    },
    updateView: function(){
        var self = this;
        
        this.$totalC.removeClass('ow_green').removeClass('ow_red');
        var signString = '0';

        if( this.params.total > 0 ){
            signString = '+' + this.params.total;
            this.$totalC.addClass('ow_green');
        }
        else if( this.params.total < 0 ){
            signString = this.params.total;
            this.$totalC.addClass('ow_red');
        }

        this.$totalC.html(signString);
        this.$upCounter.html(this.params.up);
        this.$downCounter.html(this.params.down);

        this.$upC.unbind('click').removeClass('active').click(
            function(){                
                if( self.params.ownerBlock ){
                    OW.error(OAVoteData.ownerMessage);
                    return;
                }

                if( self.params.userVote == 1 ){
                    self.setVote(0);
                }
                else{
                    self.setVote(1);
                }

                self.updateView();
                self.updateVote();
            }
        );
        
        this.$downC.unbind('click').removeClass('active').click(
            function(){                
                if( self.params.ownerBlock ){
                    OW.error(OAVoteData.ownerMessage);
                    return;
                }

                if( self.params.userVote == -1 ){
                    self.setVote(0);
                }
                else{
                    self.setVote(-1);                    
                }

                self.updateView();
                self.updateVote();
            }
        );
            
        if( this.params.userVote > 0 ){
            this.$upC.addClass('active');
        }
        
        if( this.params.userVote < 0 ){
            this.$downC.addClass('active');
        }
        
        // userLists
        this.initUserList();
    },
    
    initUserList: function(){
        var self = this;
        
        if( OAVoteData.positiveList && this.params.upUserId.length > 0 ){
            this.$upCounter.css({cursor:'pointer'}).click(function(){OW.showUsers(self.params.upUserId, OAVoteData.upListLabel);});
        }
        
        if( OAVoteData.negativeList && this.params.downUserId.length > 0 ){
            this.$downCounter.css({cursor:'pointer'}).click(function(){OW.showUsers(self.params.downUserId, OAVoteData.downListLabel);});
        }
        
        if( OAVoteData.commonList && this.params.commonUserId.length > 0 ){
            this.$totalC.css({cursor:'pointer'}).click(function(){OW.showUsers(self.params.commonUserId, OAVoteData.commonListLabel);});
        }
    },
    
    setVote: function( vote ){
        var currentUId = OAVoteData.currentUserId;
        this.removeUserFromLists();
        this.$totalC.css({cursor:'auto'}).unbind('click');
        
        // rollback prev vote
        if( this.params.userVote == 1 ){
            this.params.total--;
            this.params.count--;
            this.params.up--;
            this.$upCounter.css({cursor:'auto'}).unbind('click');
        }

        if( this.params.userVote == -1 ){
            this.params.total++;
            this.params.count--;
            this.params.down--;
            this.$downCounter.css({cursor:'auto'}).unbind('click');
        }        

        this.params.userVote = vote;

        // set new vote
        if( this.params.userVote == 1 ){
            this.params.total++;
            this.params.count++;
            this.params.up++;
            this.params.upUserId.push(currentUId);
            this.params.commonUserId.push(currentUId);
        }

        if( this.params.userVote == -1 ){
            this.params.total--;
            this.params.count++;
            this.params.down++;
            this.params.downUserId.push(currentUId);
            this.params.commonUserId.push(currentUId);
        }        
    },

    removeUserFromLists: function(){
        var lists = [this.params.upUserId, this.params.downUserId, this.params.commonUserId], index;
        
        for( var i=0; i<lists.length; i++ ){
            index = $.inArray(OAVoteData.currentUserId, lists[i]);
            if( index > -1 ){
                lists[i].splice(index, 1);
            }
        }
    },

    updateVote: function(){
        var self = this;
        $.ajax({
            type: 'POST',
            url: OAVoteData.respondUrl,
            data: {entityId:self.params.entityId, entityType: self.params.entityType, ownerId:self.params.ownerId, total:self.params.total, userVote: self.params.userVote, uri: OAVoteData.currentUri},
            dataType: 'json',
            success : function(data){
            },
            error : function( XMLHttpRequest, textStatus, errorThrown ){
                OW.error('Ajax Error: '+textStatus+'!');
                throw textStatus;
            }
        });
    }
}