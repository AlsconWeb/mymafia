(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"logo_atlas_", frames: [[0,0,512,512],[0,1542,200,158],[0,1028,512,512],[0,514,512,512]]}
];


// symbols:



(lib._143374 = function() {
	this.spriteSheet = ss["logo_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.head_ground2 = function() {
	this.spriteSheet = ss["logo_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.logo512 = function() {
	this.spriteSheet = ss["logo_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.unnamed = function() {
	this.spriteSheet = ss["logo_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.yellow_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E5D900").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.yellow_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.red_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF5232").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.red_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.rad_ico = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.unnamed();
	this.instance.parent = this;
	this.instance.setTransform(-41.8,-49.7,0.168,0.141);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("AgXAtQgJgFgDgKQgDgHAAgRIAAgmQAAgKgCgCQgDgDgFAAIgEAAIAAgDIArAAIAAADIgEAAQgGAAgDAEQgBACAAAJIAAAqIABAOQABAHACAEQADAEAGADQAFACAGAAQAKABAIgEQAHgFADgHQADgGAAgQIAAgnQAAgKgCgCQgDgDgFAAIgEAAIAAgDIAjAAIAAADIgEAAQgGAAgDAFQgBACAAAIIAAAoQAAAOgDAJQgDAIgJAGQgJAGgPAAQgPAAgIgGg");
	this.shape.setTransform(41.8,39.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF00").s().p("AgFAzIgKgDIgJgCIgDABIgCAFIgCAAIAAgiIACAAQACALAEAFQADAFAHAEQAGADAHAAQAJAAAGgFQAFgEAAgHQAAgEgCgEQgCgEgEgCIgPgKQgNgGgFgFQgGgDgCgGQgDgFAAgGQAAgKAIgJQAJgHAMAAQAHAAAJAEIAFACIAEgBQAAgBAAAAQABgBAAAAQAAgBAAAAQAAgBAAgBIADAAIAAAiIgDAAQgBgJgDgHQgEgFgGgEQgGgDgGAAQgHAAgFAEQgGAFAAAGQAAAEADAEQAFAGARAJIATAKQAEAEADAGQADAFAAAGQAAALgJAJQgIAIgOAAIgHgBg");
	this.shape_1.setTransform(32.3,39.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF00").s().p("AgFAGQgCgCAAgEQAAgCACgDQADgCACAAQADAAADACQACADAAACQAAAEgCACQgDACgDAAQgCAAgDgCg");
	this.shape_2.setTransform(26.2,43.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFF00").s().p("AgiAlQgNgPAAgWQAAgXAPgPQAOgNATAAQATAAAPAPQANAOAAAWQAAAWgNAPQgPAPgUAAQgUAAgOgPgAgVgkQgKANAAAXQAAAXALAOQAHAKANgBQAOABAIgLQAKgMAAgXQAAgYgLgNQgHgJgOAAQgNAAgIAJg");
	this.shape_3.setTransform(18.9,39.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFF00").s().p("AgUAyIAAgDIADAAQAGAAADgEQACgCAAgJIAAg/QAAgIgBgCQAAAAAAgBQgBAAAAgBQAAAAgBAAQAAgBgBAAQgDgCgEAAIgDAAIAAgDIAqAAIAAADIgFAAQgFAAgDAEQgCACAAAJIAAA/QAAAIABACIADADQADACADAAIAFAAIAAADg");
	this.shape_4.setTransform(11,39.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFF00").s().p("AgxAyIAAgDIAEAAQAGAAACgEQACgCAAgJIAAg/QAAgJgCgDQgCgDgGAAIgEAAIAAgDIAoAAQAWAAAMAFQALAGAHALQAHAMAAAPQAAAUgNAOQgOAQgbAAgAgUgpIAABUQAIACAHAAQAQAAALgMQAMgMAAgVQAAgTgMgMQgLgMgRAAQgGAAgIACg");
	this.shape_5.setTransform(2.9,39.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFF00").s().p("AAOAzIAAgDQAGAAACgCQAAAAABgBQAAAAAAgBQABAAAAgBQAAgBAAAAQAAgDgDgHIgGgPIgmAAIgGAQQgDAGAAADQAAAAAAABQAAABABAAQAAABAAAAQABAAAAABQADACAHAAIAAADIggAAIAAgDQAHgBABgCQAEgDAFgLIAjhRIABAAIAjBSQAEAKADADQAEADAGAAIAAADgAgUAMIAhAAIgQgng");
	this.shape_6.setTransform(-7.9,39.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFF00").s().p("AAXAyIghgvIgGAAIgCAAIgCAAIAAAdQAAAKACACQADADAFAAIAEAAIAAADIgrAAIAAgDIAFAAQAFAAAEgEQABgCAAgJIAAg/QAAgJgCgDQgDgDgFAAIgFAAIAAgDIAkAAQAQAAAHADQAIACAFAGQAFAGAAAJQAAAJgGAHQgGAHgNABIAVAdQAHAKAFADQAFADAIABIAAADgAgUgqIAAAqIADAAIABAAQAOAAAGgGQAIgHgBgJQABgJgGgGQgGgGgIAAIgMABg");
	this.shape_7.setTransform(-18,39.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFF00").s().p("AgUAyIAAgDIADAAQAHAAACgEQACgCAAgIIAAgYIgbgpIgHgJIgHgEIgEgBIAAgDIAqAAIAAADIgCAAQgEAAgCACQgDABAAAEQAAACAEAHIAUAfIATgdQAFgHAAgEIgBgDQAAgBgBAAQAAgBgBAAQAAAAAAAAQgBgBAAAAIgGgBIAAgDIAiAAIAAADIgCAAIgFACIgHAFIgHAKIgYAlIAAAZQAAAJACACQADADAGAAIADAAIAAADg");
	this.shape_8.setTransform(-28.7,39.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFF00").s().p("AAWAyIAAgDIADAAQAHAAACgEQACgCAAgIIAAhCIglBTIgDAAIgmhTIAABCQAAAJACACQACADAGAAIAEAAIAAADIgjAAIAAgDIAEAAQAGAAADgEQABgCAAgIIAAhAQAAgHgBgDIgEgDQgDgCgGAAIAAgDIAcAAIAjBNIAkhNIAcAAIAAADIgDAAQgHAAgCAEQgCACAAAJIAABAQAAAJACACQADADAGAAIADAAIAAADg");
	this.shape_9.setTransform(-40.8,39.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-49.4,-49.7,98.9,99.5);


(lib.purple_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF5D89").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.purple_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.lightp_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F9D0D4").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.lightp_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.green_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00BE1B").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.green_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.blue_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#596BBD").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.blue_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.bg2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EghtAJTIAAylMBDbAAAIAASlg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-215.7,-59.5,431.6,119);


(lib.bg_grad = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#000000","rgba(0,0,0,0.498)","rgba(0,0,0,0.498)","#000000"],[0,0.502,0.51,1],-507.4,0,507.5,0).s().p("EhPSAKUIAA0nMCelAAAIAAUng");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.bg_grad, new cjs.Rectangle(-507.4,-66,1015,132), null);


(lib.bg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.instance = new lib.head_ground2();
	this.instance.parent = this;
	this.instance.setTransform(410,-59.9,0.82,0.759);

	this.instance_1 = new lib.head_ground2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(246,-59.9,0.82,0.759);

	this.instance_2 = new lib.head_ground2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(82,-59.9,0.82,0.759);

	this.instance_3 = new lib.head_ground2();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-82,-59.9,0.82,0.759);

	this.instance_4 = new lib.head_ground2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-246,-59.9,0.82,0.759);

	this.instance_5 = new lib.head_ground2();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-410,-59.9,0.82,0.759);

	this.instance_6 = new lib.head_ground2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-574,-59.9,0.82,0.759);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.bg, new cjs.Rectangle(-574,-59.9,1148,120), null);


(lib.Буквы = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.m_157 = new lib.yellow_block();
	this.m_157.name = "m_157";
	this.m_157.parent = this;
	this.m_157.setTransform(309.3,35.1,1.049,1.049);

	this.m_162 = new lib.yellow_block();
	this.m_162.name = "m_162";
	this.m_162.parent = this;
	this.m_162.setTransform(341.5,52.5,1.049,1.049);

	this.m_161 = new lib.purple_block();
	this.m_161.name = "m_161";
	this.m_161.parent = this;
	this.m_161.setTransform(325.7,52.5,1.049,1.049);

	this.m_144 = new lib.blue_block();
	this.m_144.name = "m_144";
	this.m_144.parent = this;
	this.m_144.setTransform(274.3,52.5,1.049,1.049);

	this.m_193 = new lib.green_block();
	this.m_193.name = "m_193";
	this.m_193.parent = this;
	this.m_193.setTransform(257.5,52.5,1.049,1.049);

	this.m_160 = new lib.blue_block();
	this.m_160.name = "m_160";
	this.m_160.parent = this;
	this.m_160.setTransform(342.4,-51.5,1.049,1.049);

	this.m_159 = new lib.green_block();
	this.m_159.name = "m_159";
	this.m_159.parent = this;
	this.m_159.setTransform(325.7,-51.5,1.049,1.049);

	this.m_142 = new lib.yellow_block();
	this.m_142.name = "m_142";
	this.m_142.parent = this;
	this.m_142.setTransform(257.3,-51.5,1.049,1.049);

	this.m_143 = new lib.green_block();
	this.m_143.name = "m_143";
	this.m_143.parent = this;
	this.m_143.setTransform(274.3,-51.5,1.049,1.049);

	this.m_158 = new lib.yellow_block();
	this.m_158.name = "m_158";
	this.m_158.parent = this;
	this.m_158.setTransform(309.3,52.5,1.049,1.049);

	this.m_156 = new lib.green_block();
	this.m_156.name = "m_156";
	this.m_156.parent = this;
	this.m_156.setTransform(309.3,18,1.049,1.049);

	this.m_152 = new lib.purple_block();
	this.m_152.name = "m_152";
	this.m_152.parent = this;
	this.m_152.setTransform(309.3,-51.5,1.049,1.049);

	this.m_154 = new lib.lightp_block();
	this.m_154.name = "m_154";
	this.m_154.parent = this;
	this.m_154.setTransform(309.3,-16.8,1.049,1.049);

	this.m_153 = new lib.blue_block();
	this.m_153.name = "m_153";
	this.m_153.parent = this;
	this.m_153.setTransform(309.3,-34.5,1.049,1.049);

	this.m_155 = new lib.purple_block();
	this.m_155.name = "m_155";
	this.m_155.parent = this;
	this.m_155.setTransform(309.3,0.6,1.049,1.049);

	this.m_145 = new lib.blue_block();
	this.m_145.name = "m_145";
	this.m_145.parent = this;
	this.m_145.setTransform(291.4,-51.5,1.049,1.049);

	this.m_146 = new lib.purple_block();
	this.m_146.name = "m_146";
	this.m_146.parent = this;
	this.m_146.setTransform(291.4,-34.5,1.049,1.049);

	this.m_147 = new lib.yellow_block();
	this.m_147.name = "m_147";
	this.m_147.parent = this;
	this.m_147.setTransform(291.4,-16.8,1.049,1.049);

	this.m_148 = new lib.green_block();
	this.m_148.name = "m_148";
	this.m_148.parent = this;
	this.m_148.setTransform(291,0.6,1.049,1.049);

	this.m_149 = new lib.blue_block();
	this.m_149.name = "m_149";
	this.m_149.parent = this;
	this.m_149.setTransform(291,17.7,1.049,1.049);

	this.m_150 = new lib.lightp_block();
	this.m_150.name = "m_150";
	this.m_150.parent = this;
	this.m_150.setTransform(291.4,35.1,1.049,1.049);

	this.m_151 = new lib.green_block();
	this.m_151.name = "m_151";
	this.m_151.parent = this;
	this.m_151.setTransform(291.4,52.5,1.049,1.049);

	this.m_140 = new lib.blue_block();
	this.m_140.name = "m_140";
	this.m_140.parent = this;
	this.m_140.setTransform(205.7,0.6,1.049,1.049);

	this.m_136 = new lib.yellow_block();
	this.m_136.name = "m_136";
	this.m_136.parent = this;
	this.m_136.setTransform(172,0.6,1.049,1.049);

	this.m_138 = new lib.green_block();
	this.m_138.name = "m_138";
	this.m_138.parent = this;
	this.m_138.setTransform(189,0.6,1.049,1.049);

	this.m_141 = new lib.blue_block();
	this.m_141.name = "m_141";
	this.m_141.parent = this;
	this.m_141.setTransform(223.6,-51.8,1.049,1.049);

	this.m_137 = new lib.yellow_block();
	this.m_137.name = "m_137";
	this.m_137.parent = this;
	this.m_137.setTransform(189.8,-51.8,1.049,1.049);

	this.m_139 = new lib.green_block();
	this.m_139.name = "m_139";
	this.m_139.parent = this;
	this.m_139.setTransform(206.8,-51.8,1.049,1.049);

	this.m_135 = new lib.purple_block();
	this.m_135.name = "m_135";
	this.m_135.parent = this;
	this.m_135.setTransform(173.1,-51.5,1.049,1.049);

	this.m_134 = new lib.yellow_block();
	this.m_134.name = "m_134";
	this.m_134.parent = this;
	this.m_134.setTransform(156.2,52.5,1.049,1.049);

	this.m_133 = new lib.lightp_block();
	this.m_133.name = "m_133";
	this.m_133.parent = this;
	this.m_133.setTransform(156.2,35.1,1.049,1.049);

	this.m_132 = new lib.green_block();
	this.m_132.name = "m_132";
	this.m_132.parent = this;
	this.m_132.setTransform(156.2,18,1.049,1.049);

	this.m_128 = new lib.purple_block();
	this.m_128.name = "m_128";
	this.m_128.parent = this;
	this.m_128.setTransform(156.2,-51.5,1.049,1.049);

	this.m_130 = new lib.lightp_block();
	this.m_130.name = "m_130";
	this.m_130.parent = this;
	this.m_130.setTransform(156.2,-16.8,1.049,1.049);

	this.m_129 = new lib.blue_block();
	this.m_129.name = "m_129";
	this.m_129.parent = this;
	this.m_129.setTransform(156.2,-34.5,1.049,1.049);

	this.m_131 = new lib.purple_block();
	this.m_131.name = "m_131";
	this.m_131.parent = this;
	this.m_131.setTransform(156.2,0.6,1.049,1.049);

	this.m_121 = new lib.blue_block();
	this.m_121.name = "m_121";
	this.m_121.parent = this;
	this.m_121.setTransform(138.3,-51.5,1.049,1.049);

	this.m_122 = new lib.purple_block();
	this.m_122.name = "m_122";
	this.m_122.parent = this;
	this.m_122.setTransform(138.3,-34.5,1.049,1.049);

	this.m_123 = new lib.yellow_block();
	this.m_123.name = "m_123";
	this.m_123.parent = this;
	this.m_123.setTransform(138.3,-16.8,1.049,1.049);

	this.m_124 = new lib.green_block();
	this.m_124.name = "m_124";
	this.m_124.parent = this;
	this.m_124.setTransform(137.9,0.6,1.049,1.049);

	this.m_125 = new lib.blue_block();
	this.m_125.name = "m_125";
	this.m_125.parent = this;
	this.m_125.setTransform(137.9,17.7,1.049,1.049);

	this.m_126 = new lib.lightp_block();
	this.m_126.name = "m_126";
	this.m_126.parent = this;
	this.m_126.setTransform(138.3,35.1,1.049,1.049);

	this.m_127 = new lib.green_block();
	this.m_127.name = "m_127";
	this.m_127.parent = this;
	this.m_127.setTransform(138.3,52.5,1.049,1.049);

	this.m_192 = new lib.yellow_block();
	this.m_192.name = "m_192";
	this.m_192.parent = this;
	this.m_192.setTransform(479.9,51.5,1.049,1.049);

	this.m_187 = new lib.lightp_block();
	this.m_187.name = "m_187";
	this.m_187.parent = this;
	this.m_187.setTransform(462.1,51.5,1.049,1.049);

	this.m_186 = new lib.purple_block();
	this.m_186.name = "m_186";
	this.m_186.parent = this;
	this.m_186.setTransform(462.1,34.5,1.049,1.049);

	this.m_191 = new lib.blue_block();
	this.m_191.name = "m_191";
	this.m_191.parent = this;
	this.m_191.setTransform(479.9,34.5,1.049,1.049);

	this.m_190 = new lib.yellow_block();
	this.m_190.name = "m_190";
	this.m_190.parent = this;
	this.m_190.setTransform(479.9,16.7,1.049,1.049);

	this.m_189 = new lib.lightp_block();
	this.m_189.name = "m_189";
	this.m_189.parent = this;
	this.m_189.setTransform(479.9,-0.4,1.049,1.049);

	this.m_185 = new lib.lightp_block();
	this.m_185.name = "m_185";
	this.m_185.parent = this;
	this.m_185.setTransform(462.1,16.7,1.049,1.049);

	this.m_181 = new lib.blue_block();
	this.m_181.name = "m_181";
	this.m_181.parent = this;
	this.m_181.setTransform(445.4,16.7,1.049,1.049);

	this.m_176 = new lib.yellow_block();
	this.m_176.name = "m_176";
	this.m_176.parent = this;
	this.m_176.setTransform(411.6,16.7,1.049,1.049);

	this.m_178 = new lib.green_block();
	this.m_178.name = "m_178";
	this.m_178.parent = this;
	this.m_178.setTransform(428.6,16.7,1.049,1.049);

	this.m_184 = new lib.purple_block();
	this.m_184.name = "m_184";
	this.m_184.parent = this;
	this.m_184.setTransform(462.1,-0.4,1.049,1.049);

	this.m_188 = new lib.blue_block();
	this.m_188.name = "m_188";
	this.m_188.parent = this;
	this.m_188.setTransform(479.9,-17.8,1.049,1.049);

	this.m_183 = new lib.green_block();
	this.m_183.name = "m_183";
	this.m_183.parent = this;
	this.m_183.setTransform(462.1,-17.8,1.049,1.049);

	this.m_182 = new lib.yellow_block();
	this.m_182.name = "m_182";
	this.m_182.parent = this;
	this.m_182.setTransform(462.1,-35,1.049,1.049);

	this.m_180 = new lib.lightp_block();
	this.m_180.name = "m_180";
	this.m_180.parent = this;
	this.m_180.setTransform(445.4,-35,1.049,1.049);

	this.m_179 = new lib.blue_block();
	this.m_179.name = "m_179";
	this.m_179.parent = this;
	this.m_179.setTransform(445.4,-52.5,1.049,1.049);

	this.m_177 = new lib.green_block();
	this.m_177.name = "m_177";
	this.m_177.parent = this;
	this.m_177.setTransform(428.6,-52.5,1.049,1.049);

	this.m_174 = new lib.lightp_block();
	this.m_174.name = "m_174";
	this.m_174.parent = this;
	this.m_174.setTransform(411.6,-52.5,1.049,1.049);

	this.m_175 = new lib.blue_block();
	this.m_175.name = "m_175";
	this.m_175.parent = this;
	this.m_175.setTransform(411.6,-35,1.049,1.049);

	this.m_168 = new lib.green_block();
	this.m_168.name = "m_168";
	this.m_168.parent = this;
	this.m_168.setTransform(394.9,-35,1.049,1.049);

	this.m_169 = new lib.purple_block();
	this.m_169.name = "m_169";
	this.m_169.parent = this;
	this.m_169.setTransform(394.9,-17.8,1.049,1.049);

	this.m_163 = new lib.yellow_block();
	this.m_163.name = "m_163";
	this.m_163.parent = this;
	this.m_163.setTransform(377.6,-17.8,1.049,1.049);

	this.m_170 = new lib.lightp_block();
	this.m_170.name = "m_170";
	this.m_170.parent = this;
	this.m_170.setTransform(394.9,-0.4,1.049,1.049);

	this.m_164 = new lib.green_block();
	this.m_164.name = "m_164";
	this.m_164.parent = this;
	this.m_164.setTransform(377.6,-0.4,1.049,1.049);

	this.m_165 = new lib.blue_block();
	this.m_165.name = "m_165";
	this.m_165.parent = this;
	this.m_165.setTransform(377.6,17,1.049,1.049);

	this.m_171 = new lib.purple_block();
	this.m_171.name = "m_171";
	this.m_171.parent = this;
	this.m_171.setTransform(394.9,17,1.049,1.049);

	this.m_173 = new lib.purple_block();
	this.m_173.name = "m_173";
	this.m_173.parent = this;
	this.m_173.setTransform(394.9,51.5,1.049,1.049);

	this.m_172 = new lib.yellow_block();
	this.m_172.name = "m_172";
	this.m_172.parent = this;
	this.m_172.setTransform(394.9,34.5,1.049,1.049);

	this.m_166 = new lib.lightp_block();
	this.m_166.name = "m_166";
	this.m_166.parent = this;
	this.m_166.setTransform(377.6,34.5,1.049,1.049);

	this.m_167 = new lib.green_block();
	this.m_167.name = "m_167";
	this.m_167.parent = this;
	this.m_167.setTransform(377.6,51.5,1.049,1.049);

	this.m_120 = new lib.yellow_block();
	this.m_120.name = "m_120";
	this.m_120.parent = this;
	this.m_120.setTransform(103.3,52.5,1.049,1.049);

	this.m_115 = new lib.lightp_block();
	this.m_115.name = "m_115";
	this.m_115.parent = this;
	this.m_115.setTransform(85.5,52.5,1.049,1.049);

	this.m_114 = new lib.purple_block();
	this.m_114.name = "m_114";
	this.m_114.parent = this;
	this.m_114.setTransform(85.5,35.5,1.049,1.049);

	this.m_119 = new lib.blue_block();
	this.m_119.name = "m_119";
	this.m_119.parent = this;
	this.m_119.setTransform(103.3,35.5,1.049,1.049);

	this.m_118 = new lib.yellow_block();
	this.m_118.name = "m_118";
	this.m_118.parent = this;
	this.m_118.setTransform(103.3,17.7,1.049,1.049);

	this.m_117 = new lib.lightp_block();
	this.m_117.name = "m_117";
	this.m_117.parent = this;
	this.m_117.setTransform(103.3,0.6,1.049,1.049);

	this.m_113 = new lib.lightp_block();
	this.m_113.name = "m_113";
	this.m_113.parent = this;
	this.m_113.setTransform(85.5,17.7,1.049,1.049);

	this.m_109 = new lib.blue_block();
	this.m_109.name = "m_109";
	this.m_109.parent = this;
	this.m_109.setTransform(68.8,17.7,1.049,1.049);

	this.m_104 = new lib.yellow_block();
	this.m_104.name = "m_104";
	this.m_104.parent = this;
	this.m_104.setTransform(35,17.7,1.049,1.049);

	this.m_106 = new lib.green_block();
	this.m_106.name = "m_106";
	this.m_106.parent = this;
	this.m_106.setTransform(52,17.7,1.049,1.049);

	this.m_112 = new lib.purple_block();
	this.m_112.name = "m_112";
	this.m_112.parent = this;
	this.m_112.setTransform(85.5,0.6,1.049,1.049);

	this.m_116 = new lib.blue_block();
	this.m_116.name = "m_116";
	this.m_116.parent = this;
	this.m_116.setTransform(103.3,-16.8,1.049,1.049);

	this.m_111 = new lib.green_block();
	this.m_111.name = "m_111";
	this.m_111.parent = this;
	this.m_111.setTransform(85.5,-16.8,1.049,1.049);

	this.m_110 = new lib.yellow_block();
	this.m_110.name = "m_110";
	this.m_110.parent = this;
	this.m_110.setTransform(85.5,-34,1.049,1.049);

	this.m_108 = new lib.lightp_block();
	this.m_108.name = "m_108";
	this.m_108.parent = this;
	this.m_108.setTransform(68.8,-34,1.049,1.049);

	this.m_107 = new lib.blue_block();
	this.m_107.name = "m_107";
	this.m_107.parent = this;
	this.m_107.setTransform(68.8,-51.5,1.049,1.049);

	this.m_105 = new lib.green_block();
	this.m_105.name = "m_105";
	this.m_105.parent = this;
	this.m_105.setTransform(52,-51.5,1.049,1.049);

	this.m_102 = new lib.lightp_block();
	this.m_102.name = "m_102";
	this.m_102.parent = this;
	this.m_102.setTransform(35,-51.5,1.049,1.049);

	this.m_103 = new lib.blue_block();
	this.m_103.name = "m_103";
	this.m_103.parent = this;
	this.m_103.setTransform(35,-34,1.049,1.049);

	this.m_96 = new lib.green_block();
	this.m_96.name = "m_96";
	this.m_96.parent = this;
	this.m_96.setTransform(18.3,-34,1.049,1.049);

	this.m_97 = new lib.purple_block();
	this.m_97.name = "m_97";
	this.m_97.parent = this;
	this.m_97.setTransform(18.3,-16.8,1.049,1.049);

	this.m_91 = new lib.yellow_block();
	this.m_91.name = "m_91";
	this.m_91.parent = this;
	this.m_91.setTransform(1,-16.8,1.049,1.049);

	this.m_98 = new lib.lightp_block();
	this.m_98.name = "m_98";
	this.m_98.parent = this;
	this.m_98.setTransform(18.3,0.6,1.049,1.049);

	this.m_92 = new lib.green_block();
	this.m_92.name = "m_92";
	this.m_92.parent = this;
	this.m_92.setTransform(1,0.6,1.049,1.049);

	this.m_93 = new lib.blue_block();
	this.m_93.name = "m_93";
	this.m_93.parent = this;
	this.m_93.setTransform(1,18,1.049,1.049);

	this.m_99 = new lib.purple_block();
	this.m_99.name = "m_99";
	this.m_99.parent = this;
	this.m_99.setTransform(18.3,18,1.049,1.049);

	this.m_101 = new lib.purple_block();
	this.m_101.name = "m_101";
	this.m_101.parent = this;
	this.m_101.setTransform(18.3,52.5,1.049,1.049);

	this.m_100 = new lib.yellow_block();
	this.m_100.name = "m_100";
	this.m_100.parent = this;
	this.m_100.setTransform(18.3,35.5,1.049,1.049);

	this.m_94 = new lib.lightp_block();
	this.m_94.name = "m_94";
	this.m_94.parent = this;
	this.m_94.setTransform(1,35.5,1.049,1.049);

	this.m_95 = new lib.green_block();
	this.m_95.name = "m_95";
	this.m_95.parent = this;
	this.m_95.setTransform(1,52.5,1.049,1.049);

	this.m_90 = new lib.yellow_block();
	this.m_90.name = "m_90";
	this.m_90.parent = this;
	this.m_90.setTransform(-33.4,52.5,1.049,1.049);

	this.m_83 = new lib.purple_block();
	this.m_83.name = "m_83";
	this.m_83.parent = this;
	this.m_83.setTransform(-50.6,52.5,1.049,1.049);

	this.m_89 = new lib.lightp_block();
	this.m_89.name = "m_89";
	this.m_89.parent = this;
	this.m_89.setTransform(-33.4,35.1,1.049,1.049);

	this.m_82 = new lib.blue_block();
	this.m_82.name = "m_82";
	this.m_82.parent = this;
	this.m_82.setTransform(-50.6,35.1,1.049,1.049);

	this.m_81 = new lib.yellow_block();
	this.m_81.name = "m_81";
	this.m_81.parent = this;
	this.m_81.setTransform(-50.6,18,1.049,1.049);

	this.m_75 = new lib.lightp_block();
	this.m_75.name = "m_75";
	this.m_75.parent = this;
	this.m_75.setTransform(-84.4,17.7,1.049,1.049);

	this.m_88 = new lib.green_block();
	this.m_88.name = "m_88";
	this.m_88.parent = this;
	this.m_88.setTransform(-33.4,18,1.049,1.049);

	this.m_84 = new lib.purple_block();
	this.m_84.name = "m_84";
	this.m_84.parent = this;
	this.m_84.setTransform(-33.4,-51.5,1.049,1.049);

	this.m_86 = new lib.lightp_block();
	this.m_86.name = "m_86";
	this.m_86.parent = this;
	this.m_86.setTransform(-33.4,-16.8,1.049,1.049);

	this.m_85 = new lib.blue_block();
	this.m_85.name = "m_85";
	this.m_85.parent = this;
	this.m_85.setTransform(-33.4,-34.5,1.049,1.049);

	this.m_78 = new lib.yellow_block();
	this.m_78.name = "m_78";
	this.m_78.parent = this;
	this.m_78.setTransform(-50.6,-34,1.049,1.049);

	this.m_87 = new lib.purple_block();
	this.m_87.name = "m_87";
	this.m_87.parent = this;
	this.m_87.setTransform(-33.4,0.6,1.049,1.049);

	this.m_79 = new lib.green_block();
	this.m_79.name = "m_79";
	this.m_79.parent = this;
	this.m_79.setTransform(-50.6,-16.8,1.049,1.049);

	this.m_80 = new lib.lightp_block();
	this.m_80.name = "m_80";
	this.m_80.parent = this;
	this.m_80.setTransform(-50.6,0.6,1.049,1.049);

	this.m_76 = new lib.lightp_block();
	this.m_76.name = "m_76";
	this.m_76.parent = this;
	this.m_76.setTransform(-67.4,-16.8,1.049,1.049);

	this.m_77 = new lib.blue_block();
	this.m_77.name = "m_77";
	this.m_77.parent = this;
	this.m_77.setTransform(-67.4,0.6,1.049,1.049);

	this.m_66 = new lib.green_block();
	this.m_66.name = "m_66";
	this.m_66.parent = this;
	this.m_66.setTransform(-119,-34.5,1.049,1.049);

	this.m_72 = new lib.blue_block();
	this.m_72.name = "m_72";
	this.m_72.parent = this;
	this.m_72.setTransform(-101.3,-16.8,1.049,1.049);

	this.m_59 = new lib.blue_block();
	this.m_59.name = "m_59";
	this.m_59.parent = this;
	this.m_59.setTransform(-135.3,-51.5,1.049,1.049);

	this.m_67 = new lib.lightp_block();
	this.m_67.name = "m_67";
	this.m_67.parent = this;
	this.m_67.setTransform(-118.4,-16.8,1.049,1.049);

	this.m_60 = new lib.purple_block();
	this.m_60.name = "m_60";
	this.m_60.parent = this;
	this.m_60.setTransform(-135.3,-34.5,1.049,1.049);

	this.m_74 = new lib.green_block();
	this.m_74.name = "m_74";
	this.m_74.parent = this;
	this.m_74.setTransform(-84.4,0.6,1.049,1.049);

	this.m_73 = new lib.yellow_block();
	this.m_73.name = "m_73";
	this.m_73.parent = this;
	this.m_73.setTransform(-101.3,0.6,1.049,1.049);

	this.m_61 = new lib.yellow_block();
	this.m_61.name = "m_61";
	this.m_61.parent = this;
	this.m_61.setTransform(-135.3,-16.8,1.049,1.049);

	this.m_62 = new lib.green_block();
	this.m_62.name = "m_62";
	this.m_62.parent = this;
	this.m_62.setTransform(-135.7,0.6,1.049,1.049);

	this.m_68 = new lib.purple_block();
	this.m_68.name = "m_68";
	this.m_68.parent = this;
	this.m_68.setTransform(-118.4,0.6,1.049,1.049);

	this.m_63 = new lib.blue_block();
	this.m_63.name = "m_63";
	this.m_63.parent = this;
	this.m_63.setTransform(-135.7,17.7,1.049,1.049);

	this.m_69 = new lib.red_block();
	this.m_69.name = "m_69";
	this.m_69.parent = this;
	this.m_69.setTransform(-118.4,18,1.049,1.049);

	this.m_64 = new lib.lightp_block();
	this.m_64.name = "m_64";
	this.m_64.parent = this;
	this.m_64.setTransform(-135.3,35.1,1.049,1.049);

	this.m_70 = new lib.yellow_block();
	this.m_70.name = "m_70";
	this.m_70.parent = this;
	this.m_70.setTransform(-118.4,35.1,1.049,1.049);

	this.m_71 = new lib.purple_block();
	this.m_71.name = "m_71";
	this.m_71.parent = this;
	this.m_71.setTransform(-118.4,52.5,1.049,1.049);

	this.m_65 = new lib.green_block();
	this.m_65.name = "m_65";
	this.m_65.parent = this;
	this.m_65.setTransform(-135.3,52.5,1.049,1.049);

	this.m_56 = new lib.lightp_block();
	this.m_56.name = "m_56";
	this.m_56.parent = this;
	this.m_56.setTransform(-241.9,-51.5,1.049,1.049);

	this.m_53 = new lib.yellow_block();
	this.m_53.name = "m_53";
	this.m_53.parent = this;
	this.m_53.setTransform(-258.8,-34,1.049,1.049);

	this.m_57 = new lib.blue_block();
	this.m_57.name = "m_57";
	this.m_57.parent = this;
	this.m_57.setTransform(-241.9,-34,1.049,1.049);

	this.m_58 = new lib.purple_block();
	this.m_58.name = "m_58";
	this.m_58.parent = this;
	this.m_58.setTransform(-241.9,-16.8,1.049,1.049);

	this.m_55 = new lib.lightp_block();
	this.m_55.name = "m_55";
	this.m_55.parent = this;
	this.m_55.setTransform(-258.8,0.6,1.049,1.049);

	this.m_54 = new lib.green_block();
	this.m_54.name = "m_54";
	this.m_54.parent = this;
	this.m_54.setTransform(-258.8,-16.8,1.049,1.049);

	this.m_48 = new lib.lightp_block();
	this.m_48.name = "m_48";
	this.m_48.parent = this;
	this.m_48.setTransform(-275.8,-16.8,1.049,1.049);

	this.m_49 = new lib.blue_block();
	this.m_49.name = "m_49";
	this.m_49.parent = this;
	this.m_49.setTransform(-275.8,0.6,1.049,1.049);

	this.m_50 = new lib.yellow_block();
	this.m_50.name = "m_50";
	this.m_50.parent = this;
	this.m_50.setTransform(-275.8,18,1.049,1.049);

	this.m_51 = new lib.green_block();
	this.m_51.name = "m_51";
	this.m_51.parent = this;
	this.m_51.setTransform(-275.8,35.1,1.049,1.049);

	this.m_52 = new lib.lightp_block();
	this.m_52.name = "m_52";
	this.m_52.parent = this;
	this.m_52.setTransform(-275.8,52.5,1.049,1.049);

	this.m_47 = new lib.yellow_block();
	this.m_47.name = "m_47";
	this.m_47.parent = this;
	this.m_47.setTransform(-292.8,52.5,1.049,1.049);

	this.m_43 = new lib.purple_block();
	this.m_43.name = "m_43";
	this.m_43.parent = this;
	this.m_43.setTransform(-309.6,52.5,1.049,1.049);

	this.m_46 = new lib.lightp_block();
	this.m_46.name = "m_46";
	this.m_46.parent = this;
	this.m_46.setTransform(-292.8,35.5,1.049,1.049);

	this.m_45 = new lib.purple_block();
	this.m_45.name = "m_45";
	this.m_45.parent = this;
	this.m_45.setTransform(-292.8,18,1.049,1.049);

	this.m_44 = new lib.green_block();
	this.m_44.name = "m_44";
	this.m_44.parent = this;
	this.m_44.setTransform(-292.8,0.6,1.049,1.049);

	this.m_42 = new lib.blue_block();
	this.m_42.name = "m_42";
	this.m_42.parent = this;
	this.m_42.setTransform(-309.6,35.1,1.049,1.049);

	this.m_41 = new lib.lightp_block();
	this.m_41.name = "m_41";
	this.m_41.parent = this;
	this.m_41.setTransform(-309.6,17.7,1.049,1.049);

	this.m_39 = new lib.blue_block();
	this.m_39.name = "m_39";
	this.m_39.parent = this;
	this.m_39.setTransform(-309.6,-16.8,1.049,1.049);

	this.m_40 = new lib.yellow_block();
	this.m_40.name = "m_40";
	this.m_40.parent = this;
	this.m_40.setTransform(-309.6,0.6,1.049,1.049);

	this.m_38 = new lib.purple_block();
	this.m_38.name = "m_38";
	this.m_38.parent = this;
	this.m_38.setTransform(-326.5,0.6,1.049,1.049);

	this.m_36 = new lib.green_block();
	this.m_36.name = "m_36";
	this.m_36.parent = this;
	this.m_36.setTransform(-326.5,-34.5,1.049,1.049);

	this.m_37 = new lib.lightp_block();
	this.m_37.name = "m_37";
	this.m_37.parent = this;
	this.m_37.setTransform(-326.5,-16.8,1.049,1.049);

	this.m_35 = new lib.yellow_block();
	this.m_35.name = "m_35";
	this.m_35.parent = this;
	this.m_35.setTransform(-343.7,-16.8,1.049,1.049);

	this.m_34 = new lib.purple_block();
	this.m_34.name = "m_34";
	this.m_34.parent = this;
	this.m_34.setTransform(-343.7,-34.5,1.049,1.049);

	this.m_33 = new lib.blue_block();
	this.m_33.name = "m_33";
	this.m_33.parent = this;
	this.m_33.setTransform(-343.7,-51.5,1.049,1.049);

	this.m_32 = new lib.yellow_block();
	this.m_32.name = "m_32";
	this.m_32.parent = this;
	this.m_32.setTransform(-377.5,52.5,1.049,1.049);

	this.m_25 = new lib.purple_block();
	this.m_25.name = "m_25";
	this.m_25.parent = this;
	this.m_25.setTransform(-394.7,52.5,1.049,1.049);

	this.m_31 = new lib.lightp_block();
	this.m_31.name = "m_31";
	this.m_31.parent = this;
	this.m_31.setTransform(-377.5,35.1,1.049,1.049);

	this.m_24 = new lib.blue_block();
	this.m_24.name = "m_24";
	this.m_24.parent = this;
	this.m_24.setTransform(-394.7,35.1,1.049,1.049);

	this.m_23 = new lib.yellow_block();
	this.m_23.name = "m_23";
	this.m_23.parent = this;
	this.m_23.setTransform(-394.7,18,1.049,1.049);

	this.m_17 = new lib.lightp_block();
	this.m_17.name = "m_17";
	this.m_17.parent = this;
	this.m_17.setTransform(-428.5,17.7,1.049,1.049);

	this.m_30 = new lib.green_block();
	this.m_30.name = "m_30";
	this.m_30.parent = this;
	this.m_30.setTransform(-377.5,18,1.049,1.049);

	this.m_26 = new lib.purple_block();
	this.m_26.name = "m_26";
	this.m_26.parent = this;
	this.m_26.setTransform(-377.5,-51.5,1.049,1.049);

	this.m_28 = new lib.lightp_block();
	this.m_28.name = "m_28";
	this.m_28.parent = this;
	this.m_28.setTransform(-377.5,-16.8,1.049,1.049);

	this.m_27 = new lib.blue_block();
	this.m_27.name = "m_27";
	this.m_27.parent = this;
	this.m_27.setTransform(-377.5,-34.5,1.049,1.049);

	this.m_20 = new lib.yellow_block();
	this.m_20.name = "m_20";
	this.m_20.parent = this;
	this.m_20.setTransform(-394.7,-34,1.049,1.049);

	this.m_29 = new lib.purple_block();
	this.m_29.name = "m_29";
	this.m_29.parent = this;
	this.m_29.setTransform(-377.5,0.6,1.049,1.049);

	this.m_21 = new lib.green_block();
	this.m_21.name = "m_21";
	this.m_21.parent = this;
	this.m_21.setTransform(-394.7,-16.8,1.049,1.049);

	this.m_22 = new lib.lightp_block();
	this.m_22.name = "m_22";
	this.m_22.parent = this;
	this.m_22.setTransform(-394.7,0.6,1.049,1.049);

	this.m_19 = new lib.lightp_block();
	this.m_19.name = "m_19";
	this.m_19.parent = this;
	this.m_19.setTransform(-411.5,-16.8,1.049,1.049);

	this.m_18 = new lib.blue_block();
	this.m_18.name = "m_18";
	this.m_18.parent = this;
	this.m_18.setTransform(-411.5,0.6,1.049,1.049);

	this.m_12 = new lib.green_block();
	this.m_12.name = "m_12";
	this.m_12.parent = this;
	this.m_12.setTransform(-463.1,-34.5,1.049,1.049);

	this.m_14 = new lib.blue_block();
	this.m_14.name = "m_14";
	this.m_14.parent = this;
	this.m_14.setTransform(-445.4,-16.8,1.049,1.049);

	this.m_13 = new lib.blue_block();
	this.m_13.name = "m_13";
	this.m_13.parent = this;
	this.m_13.setTransform(-479.3,-51.5,1.049,1.049);

	this.m_10 = new lib.lightp_block();
	this.m_10.name = "m_10";
	this.m_10.parent = this;
	this.m_10.setTransform(-462.4,-16.8,1.049,1.049);

	this.m_11 = new lib.purple_block();
	this.m_11.name = "m_11";
	this.m_11.parent = this;
	this.m_11.setTransform(-479.3,-34.5,1.049,1.049);

	this.m_16 = new lib.green_block();
	this.m_16.name = "m_16";
	this.m_16.parent = this;
	this.m_16.setTransform(-428.5,0.6,1.049,1.049);

	this.m_15 = new lib.yellow_block();
	this.m_15.name = "m_15";
	this.m_15.parent = this;
	this.m_15.setTransform(-445.4,0.6,1.049,1.049);

	this.m_9 = new lib.yellow_block();
	this.m_9.name = "m_9";
	this.m_9.parent = this;
	this.m_9.setTransform(-479.3,-16.8,1.049,1.049);

	this.m_7 = new lib.green_block();
	this.m_7.name = "m_7";
	this.m_7.parent = this;
	this.m_7.setTransform(-479.7,0.6,1.049,1.049);

	this.m_8 = new lib.purple_block();
	this.m_8.name = "m_8";
	this.m_8.parent = this;
	this.m_8.setTransform(-462.4,0.6,1.049,1.049);

	this.m_5 = new lib.blue_block();
	this.m_5.name = "m_5";
	this.m_5.parent = this;
	this.m_5.setTransform(-479.7,17.7,1.049,1.049);

	this.m_6 = new lib.red_block();
	this.m_6.name = "m_6";
	this.m_6.parent = this;
	this.m_6.setTransform(-462.4,18,1.049,1.049);

	this.m_3 = new lib.lightp_block();
	this.m_3.name = "m_3";
	this.m_3.parent = this;
	this.m_3.setTransform(-479.3,35.1,1.049,1.049);

	this.m_4 = new lib.yellow_block();
	this.m_4.name = "m_4";
	this.m_4.parent = this;
	this.m_4.setTransform(-462.4,35.1,1.049,1.049);

	this.m_2 = new lib.purple_block();
	this.m_2.name = "m_2";
	this.m_2.parent = this;
	this.m_2.setTransform(-462.4,52.5,1.049,1.049);

	this.m_1 = new lib.green_block();
	this.m_1.name = "m_1";
	this.m_1.parent = this;
	this.m_1.setTransform(-479.3,52.5,1.049,1.049);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.m_1},{t:this.m_2},{t:this.m_4},{t:this.m_3},{t:this.m_6},{t:this.m_5},{t:this.m_8},{t:this.m_7},{t:this.m_9},{t:this.m_15},{t:this.m_16},{t:this.m_11},{t:this.m_10},{t:this.m_13},{t:this.m_14},{t:this.m_12},{t:this.m_18},{t:this.m_19},{t:this.m_22},{t:this.m_21},{t:this.m_29},{t:this.m_20},{t:this.m_27},{t:this.m_28},{t:this.m_26},{t:this.m_30},{t:this.m_17},{t:this.m_23},{t:this.m_24},{t:this.m_31},{t:this.m_25},{t:this.m_32},{t:this.m_33},{t:this.m_34},{t:this.m_35},{t:this.m_37},{t:this.m_36},{t:this.m_38},{t:this.m_40},{t:this.m_39},{t:this.m_41},{t:this.m_42},{t:this.m_44},{t:this.m_45},{t:this.m_46},{t:this.m_43},{t:this.m_47},{t:this.m_52},{t:this.m_51},{t:this.m_50},{t:this.m_49},{t:this.m_48},{t:this.m_54},{t:this.m_55},{t:this.m_58},{t:this.m_57},{t:this.m_53},{t:this.m_56},{t:this.m_65},{t:this.m_71},{t:this.m_70},{t:this.m_64},{t:this.m_69},{t:this.m_63},{t:this.m_68},{t:this.m_62},{t:this.m_61},{t:this.m_73},{t:this.m_74},{t:this.m_60},{t:this.m_67},{t:this.m_59},{t:this.m_72},{t:this.m_66},{t:this.m_77},{t:this.m_76},{t:this.m_80},{t:this.m_79},{t:this.m_87},{t:this.m_78},{t:this.m_85},{t:this.m_86},{t:this.m_84},{t:this.m_88},{t:this.m_75},{t:this.m_81},{t:this.m_82},{t:this.m_89},{t:this.m_83},{t:this.m_90},{t:this.m_95},{t:this.m_94},{t:this.m_100},{t:this.m_101},{t:this.m_99},{t:this.m_93},{t:this.m_92},{t:this.m_98},{t:this.m_91},{t:this.m_97},{t:this.m_96},{t:this.m_103},{t:this.m_102},{t:this.m_105},{t:this.m_107},{t:this.m_108},{t:this.m_110},{t:this.m_111},{t:this.m_116},{t:this.m_112},{t:this.m_106},{t:this.m_104},{t:this.m_109},{t:this.m_113},{t:this.m_117},{t:this.m_118},{t:this.m_119},{t:this.m_114},{t:this.m_115},{t:this.m_120},{t:this.m_167},{t:this.m_166},{t:this.m_172},{t:this.m_173},{t:this.m_171},{t:this.m_165},{t:this.m_164},{t:this.m_170},{t:this.m_163},{t:this.m_169},{t:this.m_168},{t:this.m_175},{t:this.m_174},{t:this.m_177},{t:this.m_179},{t:this.m_180},{t:this.m_182},{t:this.m_183},{t:this.m_188},{t:this.m_184},{t:this.m_178},{t:this.m_176},{t:this.m_181},{t:this.m_185},{t:this.m_189},{t:this.m_190},{t:this.m_191},{t:this.m_186},{t:this.m_187},{t:this.m_192},{t:this.m_127},{t:this.m_126},{t:this.m_125},{t:this.m_124},{t:this.m_123},{t:this.m_122},{t:this.m_121},{t:this.m_131},{t:this.m_129},{t:this.m_130},{t:this.m_128},{t:this.m_132},{t:this.m_133},{t:this.m_134},{t:this.m_135},{t:this.m_139},{t:this.m_137},{t:this.m_141},{t:this.m_138},{t:this.m_136},{t:this.m_140},{t:this.m_151},{t:this.m_150},{t:this.m_149},{t:this.m_148},{t:this.m_147},{t:this.m_146},{t:this.m_145},{t:this.m_155},{t:this.m_153},{t:this.m_154},{t:this.m_152},{t:this.m_156},{t:this.m_158},{t:this.m_143},{t:this.m_142},{t:this.m_159},{t:this.m_160},{t:this.m_193},{t:this.m_144},{t:this.m_161},{t:this.m_162},{t:this.m_157}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Буквы, new cjs.Rectangle(-486.8,-59.5,973.6,119), null);


// stage content:
(lib.logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var elem_pos=[];
				var hide_elem=[];
				var hide_int_num=1;
				var num_elem=194;
				var i = 1;
				var anim_speed=8;
				var elem;
				var anim;
				var max_hide;
				var speed_hide;
				var i_hide;
				var hide_int;
				var rot_speed=10;
				var stage=this;
				
				i=1;
				while (i < num_elem) 
				{
				var elem=this.movieClip_1['m_'+i];
				var pos=[elem.x,elem.y,rot_speed];
				elem_pos.push(pos);
					
				elem.x-=getRandomInt(-500, 500);
				elem.y=60;
				  i++;
				}
				
				
				start_me();
				this.addEventListener("tick", show_anim.bind(this));
				
				function start_me()
				{
				hide_elem=[];
				anim='show';
				max_hide=100;
				speed_hide=10;
					change_color();
				i_hide=0;
				}
				function getRandomInt(min, max)
				{
				  return Math.floor(Math.random() * (max - min + 1)) + min;
				}
				
				
				
				
				function add_hide()
				{
					i=hide_int_num;
					hide_int_num++;
					var hide_elem_item=stage.movieClip_1['m_'+i];
					var hide_elem_prop;
					var is_find=false;
				
					hide_elem_prop=[];
					hide_elem_prop.push(getRandomInt(-20, 20));
					hide_elem_prop.push(getRandomInt(10, 30));
					hide_elem_prop.push(getRandomInt(40, 80));
					hide_elem.push([hide_elem_item,hide_elem_prop]);
				
				if (hide_elem.length>190)
				{
					i=1;
					hide_elem=[];
					while (i < num_elem) 
						{
				    hide_elem_item=stage.movieClip_1['m_'+i];
					hide_elem_prop=[];
					hide_elem_prop.push(getRandomInt(-20, 20));
					hide_elem_prop.push(getRandomInt(10, 30));
					hide_elem_prop.push(getRandomInt(40, 80));
					hide_elem.push([hide_elem_item,hide_elem_prop]);
				    i++;
				       }
					   clearInterval(hide_int);
				}
				}
				
				
				function show_anim()
				{
				var i_c=0;
				if (anim=='show')
				{
				
					i=1;
				while (i < num_elem) 
				{
				
				elem=this.movieClip_1['m_'+i];
				
				var rs_x=elem_pos[i-1][0]-elem.x;
				var rs_y=elem_pos[i-1][1]-elem.y;
					elem.rotation++;
					if (rs_x!=0)
					{
				elem.x+=(rs_x/anim_speed);
					}
					if (rs_y!=0)
					{
				elem.y+=(rs_y/anim_speed);
					}
					if (Math.abs(rs_x)<4 && Math.abs(rs_y)<4)
					{
						i_c++;
					    elem.rotation=0;
						elem.x=elem_pos[i-1][0];
						elem.y=elem_pos[i-1][1];
					}
					else
					{
						i_c=0;
						elem.rotation+=elem_pos[i-1][2];
						if (elem.rotation>180)
						{
							elem.rotation=0;
							elem_pos[i-1][2]=0;
						}
						
							
							
					}
				  i++;
				}
				
				if (i_c>192 && anim!='wait' && anim!='hide')
				{
					anim='wait';
					setTimeout(function()
					{
				    anim='hide';
					hide_int=setInterval(hide_control,1);
					},2000);
				}
				}
				else
				{
				hide_elem.forEach(function(item, i, arr) 
				{
				if (item[0].y>60)
				{
					item[0].y=61;
					i_c++;
				}
				else
				{
				item[0].x+=item[1][0]/100;
				item[0].y+=item[1][1];
				item[0].rotation+=item[1][2];
					i_c=0;
				}
				});
				if (i_c>192)
				{
					hide_int_num=1;
					start_me();
				}
				
				}
				}
		
				
				function hide_control()
				{
				
					i_hide=0;
					add_hide();
					//max_hide-=speed_hide;
					speed_hide+=0.1;
				}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Слой_1
	this.rad_ico = new lib.rad_ico();
	this.rad_ico.name = "rad_ico";
	this.rad_ico.parent = this;
	this.rad_ico.setTransform(1092.9,62.8);
	new cjs.ButtonHelper(this.rad_ico, 0, 1, 1);

	this.instance = new lib.logo512();
	this.instance.parent = this;
	this.instance.setTransform(1173,13,0.156,0.149);

	this.instance_1 = new lib._143374();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1288,24,0.141,0.119);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFF00").s().p("AgXAtQgJgGgDgJQgDgGAAgSIAAgmQAAgKgCgCQgDgDgFAAIgEAAIAAgDIArAAIAAADIgEAAQgGAAgDAEQgBACAAAJIAAAqIABAOQABAHACAEQADAEAGADQAFADAGgBQAKAAAIgDQAHgFADgGQADgHAAgQIAAgnQAAgKgCgCQgDgDgFAAIgEAAIAAgDIAjAAIAAADIgEAAQgGAAgDAFQgBACAAAJIAAAmQAAAQgDAIQgDAIgJAGQgJAGgPAAQgPAAgIgGg");
	this.shape.setTransform(1368,102.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFF00").s().p("AgGAzIgJgCIgJgDIgDABIgBAFIgDAAIAAgjIADAAQACAMACAFQAEAFAGAEQAHADAHAAQAJAAAFgFQAGgFAAgGQAAgEgCgDQgCgFgFgCIgOgKQgNgGgFgEQgFgFgEgEQgCgGAAgGQAAgKAIgJQAIgHANAAQAHAAAIAEIAGACIADgBQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBIAEAAIAAAiIgEAAQgBgKgDgGQgEgGgGgDQgGgDgGAAQgIAAgFAFQgFAEAAAGQAAAFAEAEQAEAFAQAJIATALQAGAEACAFQADAFAAAGQAAALgJAJQgJAIgOAAIgHgBg");
	this.shape_1.setTransform(1358.5,102.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFF00").s().p("AgFAFQgCgCAAgDQAAgCACgDQADgCACAAQADAAADACQACADAAACQAAADgCACQgDADgDAAQgCAAgDgDg");
	this.shape_2.setTransform(1352.4,106.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFF00").s().p("AgFAzIgKgCIgJgDIgDABIgCAFIgCAAIAAgjIACAAQADAMADAFQADAFAHAEQAGADAHAAQAJAAAFgFQAGgFAAgGQAAgEgCgDQgCgFgEgCIgPgKQgNgGgFgEQgGgFgDgEQgCgGAAgGQAAgKAIgJQAJgHAMAAQAHAAAJAEIAGACIADgBQAAgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBIADAAIAAAiIgDAAQgBgKgDgGQgEgGgGgDQgGgDgGAAQgIAAgEAFQgGAEAAAGQAAAFADAEQAFAFARAJIATALQAEAEADAFQADAFAAAGQAAALgJAJQgJAIgNAAIgHgBg");
	this.shape_3.setTransform(1346.4,102.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFF00").s().p("AgUAyIAAgDIADAAQAGAAADgEQACgCAAgIIAAhLIgMAAIgKABQgEABgDAEQgCAEgBAHIgDAAIABgYIBRAAIABAYIgDAAQgBgGgBgDQgDgEgDgCQgEgCgGAAIgOAAIAABLQAAAJACACQACADAGAAIAEAAIAAADg");
	this.shape_4.setTransform(1337.6,102.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFF00").s().p("AAOAzIAAgDQAGAAACgCQAAAAABgBQAAAAAAgBQABAAAAgBQAAgBAAAAQAAgDgDgHIgGgPIgmAAIgGAQQgDAGAAADQAAAAAAABQABABAAAAQAAABAAAAQABAAAAABQADACAHAAIAAADIggAAIAAgDQAHgBABgCQAEgDAFgLIAjhRIABAAIAjBSQAEAKADADQAEADAGAAIAAADgAgUAMIAhAAIgQgng");
	this.shape_5.setTransform(1327.5,102.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFF00").s().p("AAJAyIAAgDIAEAAQAGAAACgEQACgCAAgJIAAgeIgtAAIAAAeQAAAIABACIADADQADACAEAAIADAAIAAADIgqAAIAAgDIADAAQAGAAADgEQACgCAAgJIAAg/QAAgIgBgCIgDgDIgHgCIgDAAIAAgDIAqAAIAAADIgDAAQgEAAgDACIgDADIgBAKIAAAdIAtAAIAAgdQAAgIgBgCQAAAAAAgBQAAAAgBgBQAAAAgBAAQAAgBgBAAQgDgCgDAAIgEAAIAAgDIAqAAIAAADIgDAAQgEAAgDACQAAAAgBABQAAAAgBAAQAAABAAAAQgBABAAAAIgBAKIAAA/IABAKIADADQADACAEAAIADAAIAAADg");
	this.shape_6.setTransform(1316.7,102.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFF00").s().p("AgiAiQgKgOAAgTQAAgNAHgNQAHgNALgGQAMgHANAAQAKAAALAFIAEACQABAAAAAAQABAAAAAAQABgBAAAAQAAAAABAAQACgDAAgDIADAAIACAhIgCAAQgEgOgJgHQgIgHgMAAQgJAAgIAFQgIAFgFALQgEALAAAPQAAAOAEAKQAFAKAIAFQAJAGAKgBQAKABAHgFQAHgEAJgMIADABQgIAOgKAFQgKAHgNAAQgYAAgOgSg");
	this.shape_7.setTransform(1306.3,102.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFF00").s().p("AgUAyIAAgDIADAAQAHAAACgEQACgCAAgIIAAgYIgbgpIgHgJIgHgEIgEgBIAAgDIAqAAIAAADIgCAAQgEAAgCACQgDABAAAEQAAACAEAHIAUAfIATgdQAFgHAAgEIgBgDQAAgBgBAAQAAgBAAAAQgBAAAAAAQgBgBAAAAIgGgBIAAgDIAiAAIAAADIgCAAIgFACIgHAFIgHAKIgYAlIAAAZQAAAJACACQADADAGAAIADAAIAAADg");
	this.shape_8.setTransform(1295.8,102.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFF00").s().p("AAWAyIAAgDIADAAQAHAAACgEQACgCAAgIIAAhCIglBTIgDAAIgmhTIAABCQAAAJACACQACADAGAAIAEAAIAAADIgjAAIAAgDIAEAAQAGAAADgEQABgCAAgIIAAhAQAAgHgBgDIgEgDQgDgCgGAAIAAgDIAcAAIAjBNIAkhNIAcAAIAAADIgDAAQgHAAgCAEQgCACAAAJIAABAQAAAJACACQADADAGAAIADAAIAAADg");
	this.shape_9.setTransform(1283.7,102.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFF00").s().p("AgXAtQgJgGgDgJQgDgGAAgSIAAgmQAAgKgCgCQgDgDgFAAIgEAAIAAgDIArAAIAAADIgEAAQgGAAgDAEQgBACAAAJIAAAqIABAOQABAHACAEQADAEAGADQAFADAGgBQAKAAAIgDQAHgFADgGQADgHAAgQIAAgnQAAgKgCgCQgDgDgFAAIgEAAIAAgDIAjAAIAAADIgEAAQgGAAgDAFQgBACAAAJIAAAmQAAAQgDAIQgDAIgJAGQgJAGgPAAQgPAAgIgGg");
	this.shape_10.setTransform(1254,102.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFF00").s().p("AgGAzIgJgCIgJgDIgDABIgCAFIgCAAIAAgjIACAAQADAMACAFQAEAFAGAEQAHADAHAAQAJAAAFgFQAGgFAAgGQAAgEgCgDQgCgFgFgCIgOgKQgNgGgFgEQgFgFgEgEQgCgGAAgGQAAgKAIgJQAIgHANAAQAHAAAIAEIAHACIACgBQABgBAAAAQAAgBABAAQAAgBAAAAQAAgBAAgBIAEAAIAAAiIgEAAQgBgKgDgGQgDgGgHgDQgGgDgGAAQgIAAgEAFQgFAEgBAGQAAAFAEAEQAEAFAQAJIATALQAFAEADAFQADAFAAAGQAAALgJAJQgJAIgOAAIgHgBg");
	this.shape_11.setTransform(1244.6,102.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFF00").s().p("AgFAFQgCgCAAgDQAAgCACgDQADgCACAAQADAAADACQACADAAACQAAADgCACQgDADgDAAQgCAAgDgDg");
	this.shape_12.setTransform(1238.4,106.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFF00").s().p("AghAlQgPgOABgXQAAgXAQgPQAOgNASAAQATAAAOAOQAOAPABAWQgBAWgOAPQgOAPgUAAQgUAAgNgPgAgVgkQgKAMAAAYQAAAXAKANQAIAKANAAQANAAAKgLQAJgKAAgYQAAgZgKgMQgJgKgNABQgNgBgIAKg");
	this.shape_13.setTransform(1231.1,102.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFF00").s().p("AgqAyIAAgDIAEAAQAEAAADgCQAAAAABAAQAAgBABAAQAAgBAAAAQABgBAAAAIABgKIAAg/QAAgKgCgCQgDgDgGAAIgEAAIAAgDIBMAAIABAWIgCAAQgCgIgCgDQgCgDgDgBIgLgBIgaAAIAAAnIAUAAQAJAAADgCQAEgEABgIIACAAIAAAhIgCAAIgDgJQgBgDgDgBQgDgCgHAAIgUAAIAAAhIABAIIABADIAGAAIAQAAIAMgBQAEgBAEgDQAEgFAFgJIACAAIgIAZg");
	this.shape_14.setTransform(1221.1,102.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFF00").s().p("AgxAyIAAgDIAEAAQAGAAACgEQACgCABgJIAAg/QgBgJgCgDQgDgDgFAAIgEAAIAAgDIAoAAQAWAAALAFQAMAGAHALQAHAMAAAPQAAAUgMAOQgOAQgcAAgAgVgpIAABUQAKACAGAAQAQAAALgMQALgMAAgVQAAgTgLgMQgLgMgQAAQgHAAgJACg");
	this.shape_15.setTransform(1211,102.3);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFF00").s().p("AgVAyIAAgDIAEAAQAHAAACgEQACgCAAgJIAAg/QAAgIgBgCQAAAAAAgBQgBAAAAgBQAAAAgBAAQAAgBgBAAQgDgCgEAAIgEAAIAAgDIArAAIAAADIgFAAQgFAAgDAEQgCACAAAJIAAA/IABAKIADADQADACADAAIAFAAIAAADg");
	this.shape_16.setTransform(1203.2,102.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFF00").s().p("AAAAzIgkhVIgEgIIgFgEIgHgBIAAgDIApAAIAAADQgHAAgCACQgBABAAAAQAAABgBAAQAAABAAAAQAAABAAABQAAADAEAIIAYA5IAXg5QAEgIAAgDQAAgBgBAAQAAgBAAAAQAAAAgBgBQAAAAgBgBQgCgCgFgBIgBAAIAAgDIAfAAIAAADQgGABgDACQgEAEgDAHIgiBUg");
	this.shape_17.setTransform(1195.3,102.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFF00").s().p("AgUAyIAAgDIADAAQAHAAACgEQACgCAAgIIAAgYIgbgpIgHgJIgHgEIgEgBIAAgDIAqAAIAAADIgCAAQgEAAgCACQgDABAAAEQAAACAEAHIAUAfIATgdQAFgHAAgEIgBgDQgBgBAAAAQAAgBgBAAQAAAAAAAAQgBgBAAAAIgGgBIAAgDIAiAAIAAADIgCAAIgFACIgHAFIgHAKIgYAlIAAAZQAAAJACACQADADAGAAIADAAIAAADg");
	this.shape_18.setTransform(1184.4,102.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFF00").s().p("AAWAyIAAgDIADAAQAHAAACgEQACgCAAgIIAAhCIglBTIgDAAIgmhTIAABCQAAAJACACQACADAGAAIAEAAIAAADIgjAAIAAgDIAEAAQAGAAADgEQABgCAAgIIAAhAQAAgHgBgDIgEgDQgDgCgGAAIAAgDIAcAAIAjBNIAkhNIAcAAIAAADIgDAAQgHAAgCAEQgCACAAAJIAABAQAAAJACACQADADAGAAIADAAIAAADg");
	this.shape_19.setTransform(1172.3,102.3);

	this.movieClip_1 = new lib.Буквы();
	this.movieClip_1.name = "movieClip_1";
	this.movieClip_1.parent = this;
	this.movieClip_1.setTransform(502.4,60.5);

	this.instance_2 = new lib.bg2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(1193.8,59.5,1,1.008);
	new cjs.ButtonHelper(this.instance_2, 0, 1, 1);

	this.instance_3 = new lib.bg_grad();
	this.instance_3.parent = this;
	this.instance_3.setTransform(500,59.5,0.985,0.902);

	this.instance_4 = new lib.bg();
	this.instance_4.parent = this;
	this.instance_4.setTransform(500,60,0.871,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.movieClip_1},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_1},{t:this.instance},{t:this.rad_ico}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(700,59,1409.6,120.5);
// library properties:
lib.properties = {
	id: '840AA6F6A156274BA6AB22E718DE69AF',
	width: 1400,
	height: 119,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"//mymafia.su/logo_files/images/logo_atlas_.png", id:"logo_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['840AA6F6A156274BA6AB22E718DE69AF'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;