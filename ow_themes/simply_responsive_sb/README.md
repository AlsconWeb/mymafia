# SimplyResponsive - SB

Theme based on the Oxwall Simplicity theme. Added sidebar, enhanced mobile capabilities, and other customizations to enhance the appearance.
