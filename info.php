<!DOCTYPE html>
<html>
	<head>
		<title>MyMafia</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel='stylesheet' id='dashicons-css'  href='https://myradio.su/wp-includes/css/dashicons.min.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='https://myradio.su/wp-includes/css/admin-bar.min.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='Crop_css-css'  href='https://myradio.su/wp-content/themes/myradio/js/tapmodo-Jcrop-1902fbc/css/jquery.Jcrop.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css-css'  href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='gstyle-css'  href='https://myradio.su/wp-content/themes/myradio/style.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='owl_css-css'  href='https://myradio.su/wp-content/themes/myradio/Owl/assets/owl.carousel.min.css?ver=4.9.9' type='text/css' media='all' />
<link rel='stylesheet' id='owl_css2-css'  href='https://myradio.su/wp-content/themes/myradio/Owl/assets/owl.theme.default.min.css?ver=4.9.9' type='text/css' media='all' />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js?ver=4.9.9'></script>
<script type='text/javascript' src='https://code.createjs.com/createjs-2015.11.26.min.js?ver=4.9.9'></script>
<script type='text/javascript' src='https://mymafia.su/info/logo_info.js'></script>
<script type='text/javascript' src='https://mymafia.su/info/logo_chats.js'></script>

<script type='text/javascript' src='https://code.jquery.com/jquery-3.2.1.min.js?ver=4.9.9'></script>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js?ver=4.9.9'></script>
<script type='text/javascript' src='https://myradio.su/wp-content/themes/myradio/js/tapmodo-Jcrop-1902fbc/js/jquery.Jcrop.js?ver=4.9.9'></script>
<script type='text/javascript' src='https://myradio.su/wp-content/themes/myradio/js/gs_script/Myradio.js?ver=4.9.9'></script>
<script type='text/javascript' src='https://myradio.su/wp-content/themes/myradio/Owl/owl.carousel.min.js?ver=4.9.9'></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js?ver=4.9.9'></script>
<script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js?ver=4.9.9'></script>
<script src="//cdn.jsdelivr.net/npm/jquery.scrollto@2.1.2/jquery.scrollTo.min.js"></script>
<script src="//mymafia.su/logo_files/logo.js"></script>

<link rel="icon" href="https://myradio.su/wp-content/uploads/2018/11/cropped-favicon-32x32.png" sizes="32x32" />
<link rel="icon" href="https://myradio.su/wp-content/uploads/2018/11/cropped-favicon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://myradio.su/wp-content/uploads/2018/11/cropped-favicon-180x180.png" />
<meta name="msapplication-TileImage" content="https://myradio.su/wp-content/uploads/2018/11/cropped-favicon-270x270.png" />
	</head>
	<body>
<div style="position:fixed;right:10px;bottom:10px;color:white;z-index:999;cursor:pointer;display:none;" id="home">
<i class="material-icons" style="font-size:60px;text-shadow: 0px 0px 20px rgba(0,0,0,0.6);">
eject
</i>
</div>
<div class="container-fluid">
<div class="no_padding_boot row">
<div class="no_padding_boot col-12" style="position:fixed;top:0px;height:100vh;height: 100%;padding:0px;margin:0px;background: url(https://mymafia.su/info/all.png);">
<table style="min-height:100vh;height: 100%;border:0px;">
  <tbody style="border:none;">
    <tr>

      <td class="align-middle">
    <CENTER>
<div id="animation_container_non" style="position:relative;">

		<canvas id="canvas_non" style="position: absolute; display: block; "></canvas>
		<div id="dom_overlay_container_non" style="pointer-events:none; overflow:hidden;  position: absolute; left: 0px; top: 0px; display: block;">
		</div>
</CENTER>
</div>
</td>
</tr>
</tbody>
</table>
<script>
var canvas_non, stage_non, exportRoot_non, anim_container_non, dom_overlay_container_non, fnStartAnimation_non;
var scroll_anim_func;
var scroll_anim_func_up;
$( document ).ready(function() {
    init_non();
    
});
function init_non() {
     if (stage_non===undefined)
 {
	canvas_non = document.getElementById("canvas_non");
	anim_container_non = document.getElementById("animation_container_non");
	dom_overlay_container_non = document.getElementById("dom_overlay_container_non");
	var comp=AdobeAn.getComposition("B538056CE7A67D45BD58ED4698A763DF");
	var lib=comp.getLibrary();
	handleComplete_non({},comp);
 }
}
function handleComplete_non(evt,comp) {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	var lib=comp.getLibrary();
	var ss=comp.getSpriteSheet();
	exportRoot_non = new lib.logo_info();
	stage_non = new lib.Stage(canvas_non);	
	//Registers the "tick" event listener.
	fnStartAnimation_non = function() {
		stage_non.addChild(exportRoot_non);
		createjs.Ticker.setFPS(lib.properties.fps);
		createjs.Ticker.addEventListener("tick", stage_non);
	}	    
	//Code to support hidpi screens and responsive scaling.
	function makeResponsive_non(isResp, respDim, isScale, scaleType) {		
		var lastW, lastH, lastS=1;	
		
		window.addEventListener('resize', resizeCanvas_non);		
		resizeCanvas_non();		
		function resizeCanvas_non() {
		    var h_menu=$('#main_menu_body').height();
		    var w_menu=$('#main_menu_body').width();
		    var my_item_w=(w_menu-40)/2;
		    var my_item_h=(h_menu-40)/2;
		    var my_item_s=Math.min(my_item_w,my_item_h);

		   $('#mymafia').css('width',my_item_s+"px");
		   $('#mymafia').css('height',my_item_s+"px");

		    
		    
		   $('#myvideo').width(my_item_s);
		  $('#myvideo').height(my_item_s);
		    $('#myradio').width(my_item_s);
		   $('#myradio').height(my_item_s);
		     $('#mychats').width(my_item_s);
		    $('#mychats').height(my_item_s);

		   // $('#main_menu_div').css("height",window.innerHeight+"px");
		    
			var w = lib.properties.width, h = lib.properties.height;	
			
			var iw = window.innerWidth, ih=window.innerHeight;			
			var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
			if(isResp) {                
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
					sRatio = lastS;                
				}				
				else if(!isScale) {					
					if(iw<w || ih<h)						
						sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==1) {					
					sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==2) {					
					sRatio = Math.max(xRatio, yRatio);				
				}			
			}			
			canvas_non.width = w*pRatio*sRatio;			
			canvas_non.height = h*pRatio*sRatio;
			
			canvas_non.style.width = dom_overlay_container_non.style.width = anim_container_non.style.width =  w*sRatio+'px';				
			canvas_non.style.height = anim_container_non.style.height = dom_overlay_container_non.style.height = h*sRatio+'px';

            anim_container_non.style.height=h/(pRatio*sRatio)+"px";
			stage_non.scaleX = pRatio*sRatio;			
			stage_non.scaleY = pRatio*sRatio;			
			lastW = iw; lastH = ih; lastS = sRatio;            
			stage_non.tickOnUpdate = false;            
			stage_non.update();            
			stage_non.tickOnUpdate = true;		
		}
	}
	makeResponsive_non(false,'height',false,2);	
	AdobeAn.compositionLoaded(lib.properties.id);
	fnStartAnimation_non();
}
</script>
</div>
</div>
</div>
<div class="container-fluid">
<div class="no_padding_boot row">
<div class="no_padding_boot col-12" id="radio_vid" style="opacity:0;position:fixed;top:0px;height:100vh;height: 100%;padding:0px;margin:0px;background: url(https://myradio.su/wp-content/themes/myradio/img/radio_1.png);">


</div>
</div>
</div>



<div class="container-fluid">
<div class="no_padding_boot row">
<div class="no_padding_boot col-12" id="video_vid" style="opacity:0;position:fixed;top:0px;height:100vh;height: 100%;padding:0px;margin:0px;background: url(https://mymafia.su/info/myvideo.png);">


</div>
</div>
</div>

<div class="container-fluid">
<div class="no_padding_boot row">
<div class="no_padding_boot col-12" id="chat_vid" style="opacity:0;position:fixed;top:0px;height:100vh;height: 100%;padding:0px;margin:0px;background: url(https://mymafia.su/info/chats.png);">


</div>
</div>
</div>

<div class="container-fluid">
<div class="no_padding_boot row">
<div class="no_padding_boot col-12" id="mafia_vid" style="opacity:0;position:fixed;top:0px;height:100vh;height: 100%;padding:0px;margin:0px;background: url(https://mymafia.su/info/mafia.png);">


</div>
</div>
</div>

<div class="container-fluid" id="main_menu">
<div class="no_padding_boot row">
<div class="no_padding_boot col-12" style="min-height:100vh;padding:0px;margin:0px;" id="main_menu_div">
    <CENTER>
<table style="min-height:100vh;border:0px;" id="main_menu_body">
  <tbody style="border:0px;">
    <tr style="border:none;">

      <td class="align-bottom" style="text-align:right;border:none;">
          <img src="https://mymafia.su/info/video-1.png" id="myvideo" style="width:2px;cursor:pointer;transform: scale3d(1,1,1);box-shadow: 20px 5px 10px rgba(0,0,0,0.3);border:1px solid black;">
      </td>
       <td class="align-bottom" style="text-align:left;border:none;">
          <img src="https://mymafia.su/info/mafia-1.png" id="mymafia" style="width:2px;cursor:pointer;transform: scale3d(1,1,1);box-shadow: 20px 5px 10px rgba(0,0,0,0.3);border:1px solid black;">
      </td>
    </tr>
     <tr style="border:none;">
 <td class="align-top" style="text-align:right;border:none;">
          <img src="https://mymafia.su/info/radio-1.png" id="myradio"  style="width:2px;cursor:pointer;transform: scale3d(1,1,1);box-shadow: 20px 5px 10px rgba(0,0,0,0.3);border:1px solid black;">
      </td>
       <td class="align-top" style="text-align:left;border:none;">
          <img src="https://mymafia.su/info/chats-1.png" id="mychats"  style="width:2px;cursor:pointer;transform: scale3d(1,1,1);box-shadow: 20px 5px 10px rgba(0,0,0,0.3);border:1px solid black;">
      </td>
    </tr>
</tbody>
</table>
    </CENTER>

</div>
</div>
</div>
<div class="container-fluid" id="ps_rad_main">
<div class="no_padding_boot row">
<div class="no_padding_boot col-12" style="min-height:100vh;padding:0px;margin:0px;">
<table style="min-height:100vh;border:0px;">
  <tbody style="border:none;">
    <tr>

      <td class="align-middle" style="border:none;padding:0px;">
          <CENTER>
          <div style="background:rgba(85,99,112,0.79);width:99%;border-radius:10px;color:white;padding-bottom:15px;transition: all 0.5s ease-in-out;" id="ps_index">
<div id="animation_container_main">
<canvas id="canvas_main"></canvas>   
<div id="dom_overlay_container_main" style="max-height:150px;pointer-events:none; overflow:hidden; display: inline-block;">
</div>

<script>  


var canvas_main, stage_main, exportRoot_main, anim_container_main, dom_overlay_container_main, fnStartAnimation;
function hide_all_button()
{
        $('#mymafia').css('opacity',0);
	    $('#myradio').css('opacity',0);
	    $('#myvideo').css('opacity',0);
	    $('#mychats').css('opacity',0);
}
function show_all_button()
{
        $('#mymafia').css('opacity',1);
	    $('#myradio').css('opacity',1);
	    $('#myvideo').css('opacity',1);
	    $('#mychats').css('opacity',1);
}
$(function () {
  $('[data-toggle="popover"]').popover();
});

jQuery(document).ready(function(){
    
    
    jQuery('#home').click(function() {
        
	    jQuery('body').scrollTo('#main_menu',1000);
	});
	jQuery('#mymafia').click(function() {
	   
	    scroll_anim_func();
	    hide_all_button();
	    setTimeout(function()
	    {
	    jQuery('body').scrollTo('#ps_mafia_main',500);    
	    },500
	    );
	    
	});
	jQuery('#myradio').click(function() {
	      scroll_anim_func();
	      hide_all_button();
	    setTimeout(function()
	    {
	     jQuery('body').scrollTo('#ps_rad_main',500); 
	    },500
	    );
	   
	});
	jQuery('#myvideo').click(function() {
	       scroll_anim_func();
	       hide_all_button();
	    setTimeout(function()
	    {
	     jQuery('body').scrollTo('#ps_vid_main',500);
	    },500
	    );
	   
	    
	});
	jQuery('#mychats').click(function() {
	      scroll_anim_func();
	      hide_all_button();
	    setTimeout(function()
	    {
	     jQuery('body').scrollTo('#ps_chat_main',500);
	    },500
	    );
	    
	});
});
var xy=1;
var yx=1;
var anim_s_speed=0.0005;
setInterval(anim_button,10);


function anim_button()
{
    
    
    xy-=anim_s_speed;
    yx-=anim_s_speed;

    if (xy<0.94 || xy>1)
    {
        anim_s_speed=-anim_s_speed;
    }
    
    $('#myvideo').css("transform","scale3d("+xy+","+yx+",1)");
    $('#mychats').css("transform","scale3d("+xy+","+yx+",1)");
    $('#myradio').css("transform","scale3d("+xy+","+yx+",1)");
    $('#mymafia').css("transform","scale3d("+xy+","+yx+",1)");
}

function clearCanvas(cnv) {
  var ctx = cnv.getContext('2d');     // gets reference to canvas context
  ctx.beginPath();    // clear existing drawing paths
  ctx.save();         // store the current transformation matrix

  // Use the identity matrix while clearing the canvas
  ctx.setTransform(1, 0, 0, 1, 0, 0);
  ctx.clearRect(0, 0, cnv.width, cnv.height);

  ctx.restore();        // restore the transform
}
function clear_chat()
{
 if (stage_chat)
 {
    // createjs.Ticker.removeEventListener("tick", stage_main);
     stage_chat.removeChild(exportRoot_chat);
     canvas_chat = document.getElementById("canvas_chat");
     canvas_chat.width = canvas_chat.width;
     stage_chat=undefined;
 }
}
function clear_non()
{
 if (stage_non)
 {
    // createjs.Ticker.removeEventListener("tick", stage_main);
     stage_non.removeChild(exportRoot_non);
     canvas_non = document.getElementById("canvas_non");
     canvas_non.width = canvas_non.width;
     stage_non=undefined;
 }
}
function clear_mafia()
{
 if (stage)
 {
    // createjs.Ticker.removeEventListener("tick", stage_main);
     stage.removeChild(exportRoot);
     canvas = document.getElementById("canvas");
     canvas.width = canvas.width;
     stage=undefined;
 }
}
function clear_main()
{
 if (stage_main)
 {
    // createjs.Ticker.removeEventListener("tick", stage_main);
     stage_main.removeChild(exportRoot_main);
     canvas_main = document.getElementById("canvas_main");
     canvas_main.width = canvas_main.width;
     stage_main=undefined;
 }
}
function init_main() {

     if (stage_main===undefined)
 {
	canvas_main = document.getElementById("canvas_main");
	anim_container_main = document.getElementById("animation_container_main");
	dom_overlay_container_main = document.getElementById("dom_overlay_container_main");
	var comp=AdobeAn.getComposition("1ADA54BB60BE3F488F364DEA711B6F36");
	var lib=comp.getLibrary();
	handleComplete_main({},comp);
 }
}
function handleComplete_main(evt,comp) {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	var lib=comp.getLibrary();
	var ss=comp.getSpriteSheet();
	exportRoot_main = new lib.Myradio();
	stage_main = new lib.Stage(canvas_main);	
	//Registers the "tick" event listener.
	fnStartAnimation = function() {
		stage_main.addChild(exportRoot_main);
		createjs.Ticker.setFPS(lib.properties.fps);
	createjs.Ticker.addEventListener("tick", stage_main);
	}	    
	//Code to support hidpi screens and responsive scaling.
	function makeResponsive_main(isResp, respDim, isScale, scaleType) {		
		var lastW, lastH, lastS=1;		
		window.addEventListener('resize', resizeCanvas_main);
		window.addEventListener("orientationchange", resizeCanvas_main);
		resizeCanvas_main();		
		function resizeCanvas_main() {			
			var w = lib.properties.width, h = lib.properties.height;			
			var iw = window.innerWidth, ih=window.innerHeight;
			var cont=document.getElementById("ps_index");
			if (iw>cont.clientWidth)
			{
			    iw=cont.clientWidth;
			}
		   
			var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
			if(isResp) {                
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
					sRatio = lastS;                
				}				
				else if(!isScale) {					
					if(iw<w || ih<h)						
						sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==1) {					
					sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==2) {					
					sRatio = Math.max(xRatio, yRatio);				
				}			
			}			
			canvas_main.width = w*pRatio*sRatio;			
			canvas_main.height = h*pRatio*sRatio;
			canvas_main.style.width =  w*sRatio+'px';				
			canvas_main.style.height = h*sRatio+'px';
			stage_main.scaleX = pRatio*sRatio;			
			stage_main.scaleY = pRatio*sRatio;			
			lastW = iw; lastH = ih; lastS = sRatio;            
			stage_main.tickOnUpdate = false;            
			stage_main.update();            
			stage_main.tickOnUpdate = true;		
		}
	}
	makeResponsive_main(true,'both',false,1);	
	AdobeAn.compositionLoaded(lib.properties.id);
	fnStartAnimation();
}
   // init_main();
 function getCoords(elem) { // кроме IE8-
  var box = elem.getBoundingClientRect();

  return {
    top: box.top + pageYOffset,
    left: box.left + pageXOffset
  };

}


 window.onscroll = function() 
 {
  var cord_rad=getCoords(document.getElementById("ps_rad_main"));
  
  var scrolled = window.pageYOffset || document.documentElement.scrollTop;
  if (scrolled>cord_rad.top-40 && scrolled<(cord_rad.top+(window.innerHeight/2)))
  {
  init_main();
  }
  else
  {
clear_main();
  }
  var op_rad=scrolled/cord_rad.top;
 
      
 
  if (scrolled>window.innerHeight)
  {
      $('#home').css("display","block");
      clear_non();
      
  }
  else
  {
      $('#home').css("display","none");
      init_non();
         if (scrolled>100)
  {
      scroll_anim_func();
      
}
else
{
      show_all_button();
      scroll_anim_func_up();
}
  }

  
  if (op_rad>1){op_rad=1;

  }
  $('#radio_vid').css("opacity",op_rad);
   
  if (scrolled>(cord_rad.top+(window.innerHeight/2)))
  {

  $('#video_vid').css("opacity",1);
  
  }
  else
  {
    $('#video_vid').css("opacity",0); 
  }
  cord_rad=getCoords(document.getElementById("ps_vid_main"));
  
  if (scrolled>(cord_rad.top+(window.innerHeight/2)))
  {
  init_chat();
  $('#chat_vid').css("opacity",1);
  
  }
  else
  {
  clear_chat();
  $('#chat_vid').css("opacity",0);  
  }
  cord_rad=getCoords(document.getElementById("ps_chat_main"));
 
    if (scrolled>(cord_rad.top+(window.innerHeight/2)))
  {
  init();
  $('#mafia_vid').css("opacity",1);
  }
  else
  {
  clear_mafia();
  $('#mafia_vid').css("opacity",0);  
  }
//  op_rad=scrolled/cord_rad.top;
//  if (op_rad>1){op_rad=1;}
  //$('#chat_vid').css("opacity",op_rad);
  
}
</script>    
              <h2>Радио</h2>
<div class="d-none d-md-block">
Желаете создать свое радио, выйти в прямой эфир для своих друзей?<BR> MyRadio это то что вам нужно.
Быстрое создание своего канала. Легкая настройка, оптимизированная и удобная панель управления.<BR>
Предусмотрены все инструменты для ведения и планирования эфира. Тарифы и возможности Вас
приятно удивят.<BR> И так начнем...
</div>
<div class="container">
<div class="row">
    <div class="col-md-3 col-12" id="ps_1" style="overflow:hidden;white-space: normal;
text-overflow: ellipsis;text-align:left;font-size:20px;background:rgba(255,0,0,0);border:1px solid white;border-radius:12px;">
<CENTER><b style="font-size:24px;">ИНФОРМАЦИЯ</b></CENTER><hr>


<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Используются сервера с большими ресурсами: дискового пространства и
высокой производительности оперативной памяти/процессора для обеспечения хорошей скорости">1. Сервер</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Вы можете выбрать любое качество вещания от 32 Kbps до 256 Kbps. Поток
любого формата MP3 , AAC , Ogg">2. Качество</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Вещать в эфир в режиме Robo Dj 24/7. Прямой эфир и ретрансляция">3. Режим</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="У нас отзывчивая тех поддержка, готовая решить все возникшие вопросы">4. Поддержка</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Всегда доступен бесплатный тариф, а также более продвинутые тарифы для
профессионалов">5. Тарифы</button><BR>
    </div>
     <div class="col-md-3 col-12" id="ps_2" style="overflow:hidden;white-space: normal;
text-overflow: ellipsis;text-align:left;font-size:20px;background:rgba(255,0,0,0);border:1px solid white;border-radius:12px;">
         
<CENTER><b style="font-size:24px;">ЭФИР</b></CENTER><hr>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Предусмотрено множество функций для ведения эфира в авто режиме,
вывод все информации.">1. Эфир</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Создания паролей для диджеев и прав доступа. Для диджеев
предусмотрена своя рекламная страничка.">2. Диджеи</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Микшер собственной разработки для выхода в прямой эфир для нескольких
диджеев одновременно, также с возможностью принимать звонки слушателей на радио.">3. Микшер</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Удобный календарь эфира с вашим описанием передач и возможностью
прикрепить изображение и поделится этим в соцсетях.">4. Календарь</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Размещение рекламы, заставок как между треками, так и наложение.">5. Интро</button>
    </div>
    
 <div class="col-md-3 col-12" id="ps_3" style="overflow:hidden;white-space: normal;
text-overflow: ellipsis;text-align:left;font-size:20px;background:rgba(255,0,0,0);border:1px solid white;border-radius:12px;">
         
<CENTER><b style="font-size:24px;">УПРАВЛЕНИЕ</b></CENTER><hr>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Удобная панель управления, разработанная непосредственно для нашего радио.">1. Панель</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Предоставляется формирование неограниченного количества плеейлистов, управление, изменение, вывод в эфир без задержек.">2. Плейлисты</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Загрузка файлов до 50 Mb, автоматическая перекодировка в выбранный
вами формат и битрейт.">3. Файлы</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Возможна запись вашего эфира. Прослушивание в удобное время, а также
повтор в эфир по расписанию.">4. Запись</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Статистика проигранных песен.">5. Статистика</button><BR>
    </div>
 <div class="col-md-3 col-12" id="ps_4" style="overflow:hidden;white-space: normal;
text-overflow: ellipsis;text-align:left;font-size:20px;background:rgba(255,0,0,0);border:1px solid white;border-radius:12px;">
         
<CENTER><b style="font-size:24px;">ДОПОЛНЕНИЯ</b></CENTER><hr>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Возможно создать мини сайт используя конструктор сайтов. Простое
дополнение с профессиональными функциями.">1. Сайт</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Библиотека файлов, удобный поиск по сайту.">2. Библиотека</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Они не заменимы при создании собственного сайта или разработки мини
сайта на нашем хостинге. Возможно изменение стиля для каждого в отдельности.">3. Модули</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Имеются плееры с возможностью смены стиля.">4. Плееры</button><BR>
<button type="button" class="button_tooltip" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="При помощи этой функции вы без труда создадите свою собственную
рекламу, джингл или заставку. Простой и понятный инструмент, не требующий установки на компьютер.">5. Мультитрек</button><BR>

    </div>
    
</div>
</div>
          </div>
          </CENTER>
          
      </td>

    </tr>
  </tbody>
</table>
    
</div>
</div>
</div>
<div class="container-fluid" id="ps_vid_main">
<div class="no_padding_boot row">
<div class="no_padding_boot col-12" style="min-height:100vh;padding:0px;margin:0px;">
<table style="min-height:100vh;border:0px;">
  <tbody style="border:none;">
    <tr>

      <td class="align-middle" style="border:none;">
          <CENTER>
          <div style="background:rgba(85,99,112,0.79);width:99%;border-radius:10px;color:white;padding-bottom:15px;transition: all 0.5s ease-in-out;height:400px;">
             <h2>Видео</h2> 
          </div>
          </CENTER>
       </td>
    </tr>
</tbody>
</table>
    
</div>
</div>
</div>  

<div class="container-fluid" id="ps_chat_main">
<div class="no_padding_boot row">
<div class="no_padding_boot col-12" style="min-height:100vh;padding:0px;margin:0px;">
<table style="min-height:100vh;border:0px;">
  <tbody style="border:none;">
    <tr>

      <td class="align-middle" style="border:none;">
          <CENTER>
          <div style="overflow:hidden;background:rgba(85,99,112,0.79);width:99%;border-radius:10px;color:white;padding-bottom:15px;transition: all 0.5s ease-in-out;height:400px;" id="ps_c_index">
             <div id="animation_container_chat">
<canvas id="canvas_chat"></canvas>   
<div id="dom_overlay_container_chat" style="max-height:150px;pointer-events:none; overflow:hidden; display: inline-block;">
</div>

<script>  
var canvas_chat, stage_chat, exportRoot_chat, anim_container_chat, dom_overlay_container_chat, fnStartAnimation_chat;
function init_chat() {
    if (stage_chat===undefined)
    {
	canvas_chat = document.getElementById("canvas_chat");
	anim_container_chat = document.getElementById("animation_container_chat");
	dom_overlay_container_chat = document.getElementById("dom_overlay_container_chat");
	var comp=AdobeAn.getComposition("840AA6F6A156274BA6AB22E718DE60AF");
	var lib=comp.getLibrary();
	handleComplete_chat({},comp);
    }
}
function handleComplete_chat(evt,comp) {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	var lib=comp.getLibrary();
	var ss=comp.getSpriteSheet();
	exportRoot_chat = new lib.logo_chats();
	stage_chat = new lib.Stage(canvas_chat);	
	//Registers the "tick" event listener.
	fnStartAnimation_chat = function() {
		stage_chat.addChild(exportRoot_chat);
		createjs.Ticker.setFPS(lib.properties.fps);
		createjs.Ticker.addEventListener("tick", stage_chat);
	}	    
	//Code to support hidpi screens and responsive scaling.
	function makeResponsive_chat(isResp, respDim, isScale, scaleType) {		
		var lastW, lastH, lastS=1;		
		window.addEventListener('resize', resizeCanvas_chat);		
		resizeCanvas_chat();		
		function resizeCanvas_chat() {			
			var w = lib.properties.width, h = lib.properties.height;			
			var iw = window.innerWidth, ih=window.innerHeight;			
				var cont=document.getElementById("ps_c_index");
			if (iw>cont.clientWidth)
			{
			    iw=cont.clientWidth;
			}
			var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
			if(isResp) {                
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
					sRatio = lastS;                
				}				
				else if(!isScale) {					
					if(iw<w || ih<h)						
						sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==1) {					
					sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==2) {					
					sRatio = Math.max(xRatio, yRatio);				
				}			
			}			
			canvas_chat.width = w*pRatio*sRatio;			
			canvas_chat.height = h*pRatio*sRatio;
			canvas_chat.style.width = dom_overlay_container_chat.style.width = anim_container_chat.style.width =  w*sRatio+'px';				
			canvas_chat.style.height = anim_container_chat.style.height = dom_overlay_container_chat.style.height = h*sRatio+'px';
			stage_chat.scaleX = pRatio*sRatio;			
			stage_chat.scaleY = pRatio*sRatio;			
			lastW = iw; lastH = ih; lastS = sRatio;            
			stage_chat.tickOnUpdate = false;            
			stage_chat.update();            
			stage_chat.tickOnUpdate = true;		
		}
	}
	makeResponsive_chat(true,'both',true,1);	
	AdobeAn.compositionLoaded(lib.properties.id);
	fnStartAnimation_chat();
	
}
</script>    
</div>             
             <h2>Чаты</h2> 
          </div>
          </CENTER>
       </td>
    </tr>
</tbody>
</table>
    
</div>
</div>
</div>

<div class="container-fluid" id="ps_mafia_main">
<div class="no_padding_boot row">
<div class="no_padding_boot col-12" style="min-height:100vh;padding:0px;margin:0px;">
<table style="min-height:100vh;border:0px;">
  <tbody style="border:none;">
    <tr>

      <td class="align-middle" style="border:none;">
          <CENTER>
          <div id="ps_m_index" style="background:rgba(85,99,112,0.79);width:99%;border-radius:10px;color:white;padding-bottom:15px;transition: all 0.5s ease-in-out;height:400px;">
             	    <div id="animation_container">
		<canvas id="canvas"></canvas>
		<div id="dom_overlay_container">
		</div>
	
	</div>
	<script>
	    var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function init() {
    if (stage===undefined)
    {
	canvas = document.getElementById("canvas");
	anim_container = document.getElementById("animation_container");
	dom_overlay_container = document.getElementById("dom_overlay_container");
	var comp=AdobeAn.getComposition("840AA6F6A156274BA6AB22E718DE69AF");
	var lib=comp.getLibrary();
	handleComplete({},comp);
    }
}
function handleComplete(evt,comp) {
	//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
	var lib=comp.getLibrary();
	var ss=comp.getSpriteSheet();
	exportRoot = new lib.logo();
	stage = new lib.Stage(canvas);	
	//Registers the "tick" event listener.
	fnStartAnimation = function() {
		stage.addChild(exportRoot);
		createjs.Ticker.setFPS(lib.properties.fps);
		createjs.Ticker.addEventListener("tick", stage);
	}	    
	//Code to support hidpi screens and responsive scaling.
	function makeResponsive(isResp, respDim, isScale, scaleType) {		
		var lastW, lastH, lastS=1;		
		window.addEventListener('resize', resizeCanvas);		
		resizeCanvas();		
		function resizeCanvas() {			
			var w = lib.properties.width, h = lib.properties.height;			
			var iw = window.innerWidth, ih=window.innerHeight;	
	    	var cont=document.getElementById("ps_m_index");
			if (iw>cont.clientWidth)
			{
			    iw=cont.clientWidth;
			}
			var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
			if(isResp) {                
				if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
					sRatio = lastS;                
				}				
				else if(!isScale) {					
					if(iw<w || ih<h)						
						sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==1) {					
					sRatio = Math.min(xRatio, yRatio);				
				}				
				else if(scaleType==2) {					
					sRatio = Math.max(xRatio, yRatio);				
				}			
			}			
			canvas.width = w*pRatio*sRatio;			
			canvas.height = h*pRatio*sRatio;
			canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';				
			canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
			stage.scaleX = pRatio*sRatio;			
			stage.scaleY = pRatio*sRatio;			
			lastW = iw; lastH = ih; lastS = sRatio;            
			stage.tickOnUpdate = false;            
			stage.update();            
			stage.tickOnUpdate = true;		
		}
	}
	makeResponsive(true,'both',true,1);	
	AdobeAn.compositionLoaded(lib.properties.id);
	fnStartAnimation();
}
$( document ).ready(function() {
    init_chat();
  //  init();
});
	    
	</script>
             <h2>Соц. сеть</h2> 
          </div>
          </CENTER>
       </td>
    </tr>
</tbody>
</table>
    
</div>
</div>
</div>
	</body>
</html>
