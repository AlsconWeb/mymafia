<?php
/* Smarty version 3.1.29, created on 2019-04-30 13:04:06
  from "/var/www/www-root/data/www/mymafia.su/ow_themes/fitbit-3d/master_pages/sign_in.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc81d96b901f9_39971183',
  'file_dependency' => 
  array (
    'f7f1e961ebdc9955df2dec9866bf0338f117bee7' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_themes/fitbit-3d/master_pages/sign_in.html',
      1 => 1549231712,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc81d96b901f9_39971183 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_component')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.component.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" />
<head DIR="ltr">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="all">
 <link href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="ow_static/themes/fitbit-3d/css/bootstrap-ltr.css" type="text/css" />

</head>
<body>
<div id="pjaxcontainer" class="hp100">
        <div class="b-header">
            <div class="container">
                <div class="row">
                    <div class="b-header__topline">
                        <div class="col-sm-6">
                            <div class="b-header__logo">
                                </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="b-header__topnav">




                           </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-md-5">
                        <div class="b-header__message">
                            <h2><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_line1'),$_smarty_tpl);?>
</h2>
                            <p><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_line2'),$_smarty_tpl);?>
</p>
                            <p>
                                <a class="btn btn-primary btn-lg" href="join" data-bind="signup">
                                    <?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_button_1'),$_smarty_tpl);?>
                                </a>
                                <a class="btn btn-secondary btn-lg" href="contact" data-bind="login">
                                    <?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_button_2'),$_smarty_tpl);?>
                                </a>
                            </p>
                            <p>
                                                            </p>
                        

                       </div>
                    </div>
                </div>
            </div>
        </div><!-- b-header -->

        <div class="b-afterheader">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <p><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_line3'),$_smarty_tpl);?>
</p>
                        <p>
                            <a class="btn-getapp btn-getapp_white btn-getatpp_android" target="_blank" href="https://play.google.com/store/apps/details?id=com.skadatexapp&hl=en"></a>
                            <a class="btn-getapp btn-getapp_white btn-getatpp_ios" target="_blank" href="https://itunes.apple.com/us/app/dating-app/id872986237?ls=1&mt=8"></a>
                        </p>
                    </div>
                </div>
            </div>
        </div><!-- b-afterheader -->

<div class= "landing_avatar"><?php echo smarty_function_component(array('class'=>'BASE_CMP_UserList'),$_smarty_tpl);?>
</div>

		<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

	
            <div class="b-features">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="b-features__item">
                            <div class="b-features__image"><img " BORDER="0"></div>
                            <h3 class="b-features__title"><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_icon_title_1'),$_smarty_tpl);?>
</h3>
                            <p><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_icon_body_1'),$_smarty_tpl);?>
</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="b-features__item">
                            <div class="b-features__image"><img " BORDER="0"></div>
                            <h3 class="b-features__title"><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_icon_title_2'),$_smarty_tpl);?>
</h3>
                            <p><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_icon_body_2'),$_smarty_tpl);?>
</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="b-features__item">
                            <div class="b-features__image"><img " BORDER="0"></div>
                            <h3 class="b-features__title"><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_icon_title_3'),$_smarty_tpl);?>
</h3>
                            <p><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_icon_body_3'),$_smarty_tpl);?>
</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- b-features -->

        <div class="b-mobile">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="b-mobile__img">
                            <img class="img-responsive">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="b-mobile__text">
                            <h2><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_page_mobile_application_title'),$_smarty_tpl);?>
</h2>
                            <p><?php echo smarty_function_text(array('key'=>'fitbit_theme_plugin+landing_page_mobile_application_body'),$_smarty_tpl);?>
</p>
                            <p>
                                <a class="btn-getapp btn-getapp_black btn-getatpp_android" target="_blank" href="https://play.google.com/store/apps/details?id=com.skadatexapp&hl=en"></a>
                                <a class="btn-getapp btn-getapp_black btn-getatpp_ios" target="_blank" href="https://itunes.apple.com/us/app/dating-app/id872986237?ls=1&mt=8"></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- b-mobile -->

        <!-- b-registration -->

        <div class="ow_footer">
	<div class="ow_canvas"></div>
		<div class="ow_page clearfix">
			<?php echo $_smarty_tpl->tpl_vars['bottom_menu']->value;?>

			<div class="ow_copyright ow_stdmargin"><?php echo smarty_function_text(array('key'=>'base+copyright'),$_smarty_tpl);?>
</div>
            <div class="ow_right"><?php echo $_smarty_tpl->tpl_vars['bottomPoweredByLink']->value;?>
</div></div>


<?php echo '<script'; ?>
 type="text/javascript">
	$('.ow_sign_up').after($('.ow_sign_in'));
	$('<div class="clearfix"></div>').appendTo($('.ow_sign_in'));
	$('.ow_sign_in > .clearfix').append($('.ow_form_options .ow_button'));
	$('.ow_sign_in > .clearfix').append($('.ow_sign_up p:last-child a'));
	$('.ow_sign_in > .clearfix').after($('.ow_connect_buttons '));
<?php echo '</script'; ?>
>

        
</div><!-- pjaxcontainer -->

                
</body>
</html>





<style>



body {
    color: #111;
    font-family: 'Open Sans', sans-serif;
    font-size: 24px;
    line-height: 1.5833;
}

h2 {
    font-size: 36px;
}

h3 {
    font-size: 24px;
}

a {
    color: #03a9f4;
}
a:hover {
    color: #111;
    text-decoration: none;
}
a.btn.btn-primary.btn-lg {
    color: #fff;
}
p {
    margin-bottom: 1em;
}


/*--------------------------------------------------------
*   BOOTSTRAP FIXES
--------------------------------------------------------*/

.btn {
    transition: 0.3s cubic-bezier(0.645, 0.045, 0.355, 1) 0s;
}
.btn-lg,
.btn-group-lg > .btn {
    padding: 10px 30px;
    font-size: 0.6666em;
    line-height: 1.33333;
    border-radius: 5px;
    text-transform: uppercase;
}
/*--------------------------------------------------------
*   END BOOTSTRAP FIXES
--------------------------------------------------------*/

.btn-getapp {
    display: inline-block;
    height: 50px;
    margin-top: 8px;
}

.btn-getatpp_android {
    background-position: 0 0;
    width: 151px;
}
.btn-getatpp_ios {
    background-position: -155px 0;
    width: 165px;
}


/*--------------------------------------------------------
*   HEADER
--------------------------------------------------------*/

.b-header {
    color: #fff;
    /*min-height: 500px;*/
    min-height: calc(100vh - 110px);
}
.b-header__topline {
    font-size: 14px;
    line-height: 16px;
    padding: 30px 0 0;
    text-align: center;
}
.b-header__message {
    padding: 35px 0 50px;
    text-align: center;
}
.b-header__message h2 {
    font-size: 1.6em;
    text-transform: uppercase;
}
.b-header__message p {
    font-size: 0.95em;
}
.b-header__message .btn {
    margin-bottom: 5px;
}
.b-header__topnav {
    padding: 50px 0 30px;
}

.b-header__topnav a {
    color: #fff;
    font-weight: bold;
    text-decoration: underline;
    text-transform: uppercase;
}
.b-header__topnav a:hover {
    color: #e82864;
    font-weight: bold;
    text-decoration: underline;
}

@media (min-width: 768px) {
    .b-header {
    }
    .b-header__topline {
        text-align: left;
    }
    .b-header__topnav {
        padding: 0;
        text-align: right;
    }
    .b-header__message {
        padding: 150px 0 50px;
        text-align: left;
    }
    .b-header__message h2 {
        font-size: 2em;
    }
    .b-header__message p {
        font-size: 1em;
    }
}



/*--------------------------------------------------------
*   END HEADER
--------------------------------------------------------*/





/*--------------------------------------------------------
*   FEATURES
--------------------------------------------------------*/

.b-features {
    font-size: 0.5833em;
    line-height: 1.5715;
    padding: 80px 0 5px;
    text-align: center;
}

.b-features__item {
    padding: 0 30px 75px;
}

.b-features__image {
    height: 160px;
}
.b-features__image img {
    max-width: 100%;
    position: relative;
    top: 50%;
    -webkit-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
}

.b-features__title {}

@media (min-width: 768px) {
    .b-features {
        padding-bottom: 80px;
    }

    .b-features__item {
        padding-bottom: 0;
    }
}

/*--------------------------------------------------------
*   END FEATURES
--------------------------------------------------------*/


/*--------------------------------------------------------
*   MOBILE
--------------------------------------------------------*/

.b-mobile {
    padding: 30px 0;
    text-align: center;
}

.b-mobile__img {
    margin: 0 auto;
}

@media (min-width: 768px) {
    .b-mobile {
        text-align: left;
    }

    .b-mobile__text {
        padding: 45px 0;
    }
}

@media (min-width: 1080px) {
    .b-mobile__text {
        padding-top: 90px;
    }
}

/*--------------------------------------------------------
*   END MOBILE
--------------------------------------------------------*/

/*--------------------------------------------------------
*   REGISTRATION
--------------------------------------------------------*/

.b-registration {
    color: #fff;
    font-size: 0.5em;
    padding: 100px 0;
    text-align: center;
}
.b-registration h2 {
    margin-bottom: 50px;
    margin-top: 0;
}
.b-registration .form-group {
    text-align: left;
}
.b-registration .form-control {
    font-size: 18px;
    height: 49px;
}
.has-success .help-block {
    color: #94e596;
}
.has-error .help-block {
    color: #ea9e9c;
}
.b-registration .form-control-feedback {
    height: 49px;
    line-height: 49px;
}

/*--------------------------------------------------------
*   END REGISTRATION
--------------------------------------------------------*/

/*--------------------------------------------------------
*   FOOTER
--------------------------------------------------------*/

.b-footer {
    background: #322b37;
    color: #fff;
    font-size: 12px;
    line-height: 28px;
    padding: 15px 0;
    text-align: center;
}

.b-footer a:hover {
    color: #fff;
}



@media (min-width: 768px) {
    .b-footer {
        text-align: left;
    }
    .b-footer__copy {
        text-align: right;
    }
}

</style><?php }
}
