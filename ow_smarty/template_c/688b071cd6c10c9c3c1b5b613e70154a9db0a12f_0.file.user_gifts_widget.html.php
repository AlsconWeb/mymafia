<?php
/* Smarty version 3.1.29, created on 2019-04-30 08:31:52
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/virtual_gifts/views/components/user_gifts_widget.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc7ddc8b07ea2_79269145',
  'file_dependency' => 
  array (
    '688b071cd6c10c9c3c1b5b613e70154a9db0a12f' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/virtual_gifts/views/components/user_gifts_widget.html',
      1 => 1470288120,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc7ddc8b07ea2_79269145 ($_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.url_for_route.php';
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


    .ow_user_gifts .ow_gift_wrapper {
        margin: 0px 2px 0px 0px;
        height: 83px;
        width: 80px;
        overflow: hidden;
    }
    
    .ow_user_gifts .ow_gift_wrapper img { width: 80px; }

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<div class="clearfix ow_user_gifts">
<?php
$_from = $_smarty_tpl->tpl_vars['gifts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_gift_0_saved_item = isset($_smarty_tpl->tpl_vars['gift']) ? $_smarty_tpl->tpl_vars['gift'] : false;
$_smarty_tpl->tpl_vars['gift'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['gift']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['gift']->value) {
$_smarty_tpl->tpl_vars['gift']->_loop = true;
$__foreach_gift_0_saved_local_item = $_smarty_tpl->tpl_vars['gift'];
?>
	<div class="ow_gift_wrapper">
        <a href="<?php echo smarty_function_url_for_route(array('for'=>"virtual_gifts_view_gift:[giftId=>".((string)$_smarty_tpl->tpl_vars['gift']->value['dto']->id)."]"),$_smarty_tpl);?>
">
            <img <?php if ($_smarty_tpl->tpl_vars['gift']->value['dto']->message != '') {?>title="<?php echo $_smarty_tpl->tpl_vars['gift']->value['dto']->message;?>
"<?php }?> src="<?php echo $_smarty_tpl->tpl_vars['gift']->value['imageUrl'];?>
" />
        </a>
	</div>
<?php
$_smarty_tpl->tpl_vars['gift'] = $__foreach_gift_0_saved_local_item;
}
if ($__foreach_gift_0_saved_item) {
$_smarty_tpl->tpl_vars['gift'] = $__foreach_gift_0_saved_item;
}
?>
</div><?php }
}
