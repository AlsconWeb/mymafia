<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:19:15
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/photo/views/components/album_name_list.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6ebc3ea8e09_63526404',
  'file_dependency' => 
  array (
    '9a9dc89e7bd909bce0c3b99b09c84a677a40379e' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/photo/views/components/album_name_list.html',
      1 => 1470288120,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6ebc3ea8e09_63526404 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
?>
<div class="ow_dropdown_list_wrap">
    <ul class="ow_dropdown_list" style="z-index: 10;">
        <li><?php echo smarty_function_text(array('key'=>"photo+create_album"),$_smarty_tpl);?>
<span class="ow_add_item"></span></li>
        <?php if (!empty($_smarty_tpl->tpl_vars['albumNameList']->value)) {?>
            <li class="ow_dropdown_delimeter"><div></div></li>
            <?php
$_from = $_smarty_tpl->tpl_vars['albumNameList']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_album_0_saved_item = isset($_smarty_tpl->tpl_vars['album']) ? $_smarty_tpl->tpl_vars['album'] : false;
$_smarty_tpl->tpl_vars['album'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['album']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['album']->value) {
$_smarty_tpl->tpl_vars['album']->_loop = true;
$__foreach_album_0_saved_local_item = $_smarty_tpl->tpl_vars['album'];
?>
                <li data-name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value, ENT_QUOTES, 'UTF-8', true);?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value, ENT_QUOTES, 'UTF-8', true);?>
</li>
            <?php
$_smarty_tpl->tpl_vars['album'] = $__foreach_album_0_saved_local_item;
}
if ($__foreach_album_0_saved_item) {
$_smarty_tpl->tpl_vars['album'] = $__foreach_album_0_saved_item;
}
?>
        <?php }?>
    </ul>
</div>
<div class="ow_dropdown_arrow_down upload_photo_spinner"></div><?php }
}
