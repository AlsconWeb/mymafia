<?php
/* Smarty version 3.1.29, created on 2019-05-20 05:23:12
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/forms_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ce20f90bea9b4_68649439',
  'file_dependency' => 
  array (
    'da041715a817c8a2224c95683f4d69e276a84cd0' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/forms_index.html',
      1 => 1545767778,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ce20f90bea9b4_68649439 ($_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_desc')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.desc.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    .skeleton-form-output {
        padding: 10px;
        border-width: 1px;
    }

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<div class="page_description">
    <?php echo smarty_function_text(array('key'=>"skeleton+description_forms"),$_smarty_tpl);?>


    <div class="example_file">
        <table>
            <tr>
                <td>
                    <?php echo smarty_function_text(array('key'=>"skeleton+example_file"),$_smarty_tpl);?>

                </td>
                <td>
                    /ow_plugins/skeleton/controllers/forms.php;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    /ow_plugins/skeleton/classes/form.php
                </td>
            </tr>
        </table>
    </div>
</div>

<?php if (!empty($_smarty_tpl->tpl_vars['submitedFields']->value)) {?>
    <h3 class="ow_center" style="margin-bottom: 10px;"><?php echo smarty_function_text(array('key'=>"skeleton+form_output_data"),$_smarty_tpl);?>
</h3>
    <div class="ow_stdmargin skeleton-form-output ow_border">
        <?php
$_from = $_smarty_tpl->tpl_vars['submitedFields']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_submitedField_0_saved_item = isset($_smarty_tpl->tpl_vars['submitedField']) ? $_smarty_tpl->tpl_vars['submitedField'] : false;
$_smarty_tpl->tpl_vars['submitedField'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['submitedField']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['submitedField']->value) {
$_smarty_tpl->tpl_vars['submitedField']->_loop = true;
$__foreach_submitedField_0_saved_local_item = $_smarty_tpl->tpl_vars['submitedField'];
?>
            <div class="skeleton-form-output-line">
                <?php echo $_smarty_tpl->tpl_vars['submitedField']->value['label'];?>
 = "<?php echo $_smarty_tpl->tpl_vars['submitedField']->value['value'];?>
"
            </div>
        <?php
$_smarty_tpl->tpl_vars['submitedField'] = $__foreach_submitedField_0_saved_local_item;
}
if ($__foreach_submitedField_0_saved_item) {
$_smarty_tpl->tpl_vars['submitedField'] = $__foreach_submitedField_0_saved_item;
}
?>
    </div>
<?php }?>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>"skeleton_form")); $_block_repeat=true; echo smarty_block_form(array('name'=>"skeleton_form"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


    <table class="ow_table_1 ow_form ow_stdmargin">
        <tbody>
            <tr class="ow_tr_first ow_alt1">
                    <td class="ow_label">
                        <?php echo smarty_function_label(array('name'=>"text"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_value">
                        <?php echo smarty_function_input(array('name'=>"text"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_desc ow_small">
                        <?php echo smarty_function_desc(array('name'=>"text"),$_smarty_tpl);?>

                    </td>
            </tr>
            <tr class="ow_alt2">
                    <td class="ow_label">
                        <?php echo smarty_function_label(array('name'=>"extended_text"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_value">
                        <?php echo smarty_function_input(array('name'=>"extended_text"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_desc ow_small">
                        &nbsp;
                    </td>
            </tr>
            
            <tr class="ow_alt1">
                    <td class="ow_label">
                        <?php echo smarty_function_label(array('name'=>"selectbox"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_value">
                        <?php echo smarty_function_input(array('name'=>"selectbox"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_desc ow_small">
                        &nbsp;
                    </td>
            </tr>
            
            <tr class="ow_alt2">
                    <td class="ow_label">
                        <?php echo smarty_function_label(array('name'=>"multichoice"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_value">
                        <?php echo smarty_function_input(array('name'=>"multichoice"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_desc ow_small">
                        &nbsp;
                    </td>
            </tr>
            
            <tr class="ow_tr_last ow_alt1">
                    <td class="ow_label">
                        <?php echo smarty_function_label(array('name'=>"file"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_value">
                        <?php echo smarty_function_input(array('name'=>"file"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_desc ow_small">
                        &nbsp;
                    </td>
            </tr>
            
        </tbody>
    </table>

    <div class="clearfix ow_stdmargin">
        <div class="ow_right">
             <?php echo smarty_function_submit(array('name'=>"submit"),$_smarty_tpl);?>

        </div>
    </div>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"skeleton_form"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);
}
}
