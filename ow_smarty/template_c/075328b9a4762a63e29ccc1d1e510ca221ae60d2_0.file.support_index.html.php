<?php
/* Smarty version 3.1.29, created on 2019-05-02 01:25:34
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/support_center/views/controllers/support_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cca1cde1e5fc9_80242840',
  'file_dependency' => 
  array (
    '075328b9a4762a63e29ccc1d1e510ca221ae60d2' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/support_center/views/controllers/support_index.html',
      1 => 1553098458,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cca1cde1e5fc9_80242840 ($_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.submit.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/www-root/data/www/mymafia.su/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

.support_page{
	display:block;
	width:100%;
	padding:1px;
}

.support_search{
	display:inline-block;
	width:100%;
	text-align:center;
	background: #DEDEDE;
	padding-top: 2px;
	padding-bottom: 2px;
	border-bottom: 1px solid #000;
	margin-bottom: 10px;
	height: 85px;
}
.search_td{
	width:100%!important;
}

.search_field{
	width: 100%!important;
	padding:5px;
}

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

<?php echo $_smarty_tpl->tpl_vars['menu']->value;?>

<div class="clearfix support_page">
	<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>"supportcenter_form_search")); $_block_repeat=true; echo smarty_block_form(array('name'=>"supportcenter_form_search"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	<div class="support_search">
      <table class="ow_form ow_automargin" style="margin: 20px;">
            <tr>
                <td class="ow_value ow_valign_middle search_td"><?php echo smarty_function_input(array('name'=>'text','class'=>'search_field'),$_smarty_tpl);?>
<br /><?php echo smarty_function_error(array('name'=>'text'),$_smarty_tpl);?>
</td>
                <td class="ow_submit ow_valign_middle"><?php echo smarty_function_submit(array('name'=>'submit'),$_smarty_tpl);?>
</td>
            </tr>
        </table>
	</div>
	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"supportcenter_form_search"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

    <div class="ow_column ow_right" style="width:35%;">
	<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('block_decorator', array('name'=>"box",'iconClass'=>"ow_ic_tag",'label'=>$_smarty_tpl->tpl_vars['welcome']->value)); $_block_repeat=true; echo smarty_block_block_decorator(array('name'=>"box",'iconClass'=>"ow_ic_tag",'label'=>$_smarty_tpl->tpl_vars['welcome']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		<div><?php echo $_smarty_tpl->tpl_vars['welcome_msg']->value;?>
</div>
	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_block_decorator(array('name'=>"box",'iconClass'=>"ow_ic_tag",'label'=>$_smarty_tpl->tpl_vars['welcome']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

	<?php if ($_smarty_tpl->tpl_vars['help_forum_con']->value == 'TRUE') {?>
	<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('block_decorator', array('name'=>"box",'iconClass'=>"ow_ic_forum",'label'=>$_smarty_tpl->tpl_vars['community_help']->value)); $_block_repeat=true; echo smarty_block_block_decorator(array('name'=>"box",'iconClass'=>"ow_ic_forum",'label'=>$_smarty_tpl->tpl_vars['community_help']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

		<div><?php echo $_smarty_tpl->tpl_vars['hf_promo']->value;?>
</div>
	<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_block_decorator(array('name'=>"box",'iconClass'=>"ow_ic_forum",'label'=>$_smarty_tpl->tpl_vars['community_help']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

	<?php }?>
	
	
        <?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('block_decorator', array('name'=>"box",'iconClass'=>"ow_ic_tag",'label'=>$_smarty_tpl->tpl_vars['text_article_menu']->value)); $_block_repeat=true; echo smarty_block_block_decorator(array('name'=>"box",'iconClass'=>"ow_ic_tag",'label'=>$_smarty_tpl->tpl_vars['text_article_menu']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


	<table class="ow_table_1">
	<tr class="ow_alt1" >
	<td><a href="<?php echo $_smarty_tpl->tpl_vars['url_ticket_new']->value;?>
"><?php echo smarty_function_text(array('key'=>"supportcenter+submit_ticket"),$_smarty_tpl);?>
</a></td></tr>
	<tr class="ow_alt1" >
	<td><a href="<?php echo $_smarty_tpl->tpl_vars['url_ticket_my']->value;?>
"><?php echo smarty_function_text(array('key'=>"supportcenter+my_tickets"),$_smarty_tpl);?>
</a></td>
	</tr>
	</table>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_block_decorator(array('name'=>"box",'iconClass'=>"ow_ic_tag",'label'=>$_smarty_tpl->tpl_vars['text_article_menu']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>



    </div>
    <div class="ow_column" style="width:63%;">


        <?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('block_decorator', array('name'=>"box",'iconClass'=>"ow_ic_edit",'label'=>$_smarty_tpl->tpl_vars['top_articles']->value)); $_block_repeat=true; echo smarty_block_block_decorator(array('name'=>"box",'iconClass'=>"ow_ic_edit",'label'=>$_smarty_tpl->tpl_vars['top_articles']->value), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


        <table class="ow_table_2">
	        <?php
$_from = $_smarty_tpl->tpl_vars['top_articles_list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_item_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$__foreach_item_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
            <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
 <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_item']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_item']->value['first'] : null)) {?>ow_tr_first<?php }
if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_item']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_item']->value['last'] : null)) {?>ow_tr_last<?php }?>">
                <td style="width: 85%;">
                    <div class="ow_left ow_txtleft">
		        	<a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['url_view'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
</a>
                    </div>
                </td>
        </tr>
	        <?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_local_item;
}
if ($__foreach_item_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_item_0_saved_item;
}
?>
		<tr class="ow_tr_last ow_right">
			<td><a href="<?php echo $_smarty_tpl->tpl_vars['url_knowledgebase']->value;?>
"><?php echo smarty_function_text(array('key'=>"supportcenter+all_articles"),$_smarty_tpl);?>
</a></td>
		</tr>
	</table>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_block_decorator(array('name'=>"box",'iconClass'=>"ow_ic_edit",'label'=>$_smarty_tpl->tpl_vars['top_articles']->value), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>



    </div>



</div>




<?php }
}
