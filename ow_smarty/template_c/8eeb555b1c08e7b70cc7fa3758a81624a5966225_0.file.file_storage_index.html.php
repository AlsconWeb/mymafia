<?php
/* Smarty version 3.1.29, created on 2019-05-19 23:30:48
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/file_storage_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ce1bcf8b77a94_48469657',
  'file_dependency' => 
  array (
    '8eeb555b1c08e7b70cc7fa3758a81624a5966225' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/file_storage_index.html',
      1 => 1545767778,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ce1bcf8b77a94_48469657 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.submit.php';
?>
<div class="page_description">
    <?php echo smarty_function_text(array('key'=>"skeleton+description_file_storage"),$_smarty_tpl);?>


    <div class="example_file"><?php echo smarty_function_text(array('key'=>"skeleton+example_file"),$_smarty_tpl);?>
 /ow_plugins/skeleton/controllers/file_storage.php</div>
</div>

<?php if ($_smarty_tpl->tpl_vars['showForm']->value) {
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>"FileStorageExampleForm")); $_block_repeat=true; echo smarty_block_form(array('name'=>"FileStorageExampleForm"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

<table class="ow_table_1 ow_form ow_stdmargin">
    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'image'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo smarty_function_input(array('name'=>'image'),$_smarty_tpl);
echo smarty_function_error(array('name'=>'image'),$_smarty_tpl);?>
</td>
        <td class="ow_desc ow_small"></td>
    </tr>
    <tr class="ow_alt1 ow_tr_first">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'description'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo smarty_function_input(array('name'=>'description'),$_smarty_tpl);
echo smarty_function_error(array('name'=>'description'),$_smarty_tpl);?>
</td>
        <td class="ow_desc ow_small"></td>
    </tr>
</table>
<div class="clearfix ow_submit ow_stdmargin">
    <div class="ow_right"><?php echo smarty_function_submit(array('name'=>'submit','class'=>'ow_ic_add ow_positive'),$_smarty_tpl);?>
</div>
</div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"FileStorageExampleForm"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

<?php } else { ?>
<div class="ow_center">
    <div class="ow_std_margin"><img src="<?php echo $_smarty_tpl->tpl_vars['imageUrl']->value;?>
"/></div>
    <?php if ($_smarty_tpl->tpl_vars['description']->value) {?><div class="ow_std_margin"><p><?php echo smarty_function_text(array('key'=>'skeleton+description'),$_smarty_tpl);?>
:</p><?php echo $_smarty_tpl->tpl_vars['description']->value;?>
</div><?php }?>
    <div class="ow_std_margin"><a class='ow_center' href="<?php echo $_smarty_tpl->tpl_vars['backUrl']->value;?>
"><?php echo smarty_function_text(array('key'=>'skeleton+upload_again'),$_smarty_tpl);?>
</a></div>
</div>
<?php }
}
}
