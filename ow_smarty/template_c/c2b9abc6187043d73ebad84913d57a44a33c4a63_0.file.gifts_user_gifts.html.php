<?php
/* Smarty version 3.1.29, created on 2019-04-30 06:18:50
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/virtual_gifts/views/controllers/gifts_user_gifts.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc7be9ac542f6_66091269',
  'file_dependency' => 
  array (
    'c2b9abc6187043d73ebad84913d57a44a33c4a63' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/virtual_gifts/views/controllers/gifts_user_gifts.html',
      1 => 1470288120,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc7be9ac542f6_66091269 ($_smarty_tpl) {
if (!is_callable('smarty_function_format_date')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.format_date.php';
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.url_for_route.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
?>

<div class="ow_column ow_automargin">
<?php if ($_smarty_tpl->tpl_vars['gifts']->value) {
$_from = $_smarty_tpl->tpl_vars['gifts']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_gift_0_saved_item = isset($_smarty_tpl->tpl_vars['gift']) ? $_smarty_tpl->tpl_vars['gift'] : false;
$_smarty_tpl->tpl_vars['gift'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['gift']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['gift']->value) {
$_smarty_tpl->tpl_vars['gift']->_loop = true;
$__foreach_gift_0_saved_local_item = $_smarty_tpl->tpl_vars['gift'];
?>
    <?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'giftId', null); ob_start();
echo $_smarty_tpl->tpl_vars['gift']->value['dto']->id;
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'senderId', null); ob_start();
echo $_smarty_tpl->tpl_vars['gift']->value['dto']->senderId;
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    
    <?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'infoString', null); ob_start(); ?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['avatars']->value[$_smarty_tpl->tpl_vars['senderId']->value]['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['avatars']->value[$_smarty_tpl->tpl_vars['senderId']->value]['title'];?>
</a>
        <span class="ow_nowrap ow_tiny ow_ipc_date"><?php echo smarty_function_format_date(array('timestamp'=>$_smarty_tpl->tpl_vars['gift']->value['dto']->sendTimestamp),$_smarty_tpl);?>
</span>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    
    <?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'content', null); ob_start(); ?>
    <div class="clearfix">
        <div class="ow_gift_image">
            <a href="<?php echo smarty_function_url_for_route(array('for'=>"virtual_gifts_view_gift:[giftId=>".((string)$_smarty_tpl->tpl_vars['gift']->value['dto']->id)."]"),$_smarty_tpl);?>
">
                <img src="<?php echo $_smarty_tpl->tpl_vars['gift']->value['imageUrl'];?>
" />
            </a>
        </div>
        <div class="ow_gift_message"><?php if ($_smarty_tpl->tpl_vars['gift']->value['dto']->message != '') {
echo $_smarty_tpl->tpl_vars['gift']->value['dto']->message;
}?></div>
    </div>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    
    <?php echo smarty_function_decorator(array('name'=>'ipc','addClass'=>'ow_stdmargin','infoString'=>$_smarty_tpl->tpl_vars['infoString']->value,'avatar'=>$_smarty_tpl->tpl_vars['avatars']->value[$_smarty_tpl->tpl_vars['senderId']->value],'content'=>$_smarty_tpl->tpl_vars['content']->value),$_smarty_tpl);?>

<?php
$_smarty_tpl->tpl_vars['gift'] = $__foreach_gift_0_saved_local_item;
}
if ($__foreach_gift_0_saved_item) {
$_smarty_tpl->tpl_vars['gift'] = $__foreach_gift_0_saved_item;
}
} else { ?>
    <div class="ow_nocontent"><?php echo smarty_function_text(array('key'=>'virtualgifts+no_gifts'),$_smarty_tpl);?>
</div>
<?php }?>

<?php echo $_smarty_tpl->tpl_vars['paging']->value;?>

</div><?php }
}
