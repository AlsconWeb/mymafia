<?php
/* Smarty version 3.1.29, created on 2019-05-19 23:29:15
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/example_devtools.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ce1bc9b03aa71_42391117',
  'file_dependency' => 
  array (
    '805faab6953678da8b1c0bb1f955fa039fa3b1f7' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/example_devtools.html',
      1 => 1545767778,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ce1bc9b03aa71_42391117 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
?>
<div class="page_description">
    <?php echo smarty_function_text(array('key'=>"skeleton+description_dev_tools"),$_smarty_tpl);?>

</div>

<table class="ow_table_1 ow_form ow_stdmargin">

    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label">define('OW_DEBUG_MODE', true);</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+devtools_desc_debug_mode"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="ow_label">define('OW_DEV_MODE', true);</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+devtools_desc_dev_mode"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="ow_label">define('OW_PROFILER_ENABLE', true);</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+devtools_desc_profiler"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label"><a href="<?php echo $_smarty_tpl->tpl_vars['devtoolsUrl']->value;?>
" target="_blank" ><?php echo smarty_function_text(array('key'=>"skeleton+devtools_link_langs"),$_smarty_tpl);?>
</a></td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+devtools_desc_link_langs"),$_smarty_tpl);?>
</td>
    </tr>
</table><?php }
}
