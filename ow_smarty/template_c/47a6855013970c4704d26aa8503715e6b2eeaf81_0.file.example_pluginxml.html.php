<?php
/* Smarty version 3.1.29, created on 2019-05-19 23:32:03
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/example_pluginxml.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ce1bd434f2b23_61455709',
  'file_dependency' => 
  array (
    '47a6855013970c4704d26aa8503715e6b2eeaf81' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/example_pluginxml.html',
      1 => 1545767778,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ce1bd434f2b23_61455709 ($_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


.ow_alt2 .left_indent, .ow_alt1 .left_indent{
    padding-left: 5%;
    text-align: left;
}

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<div class="page_description">
    <?php echo smarty_function_text(array('key'=>"skeleton+description_pluginxml"),$_smarty_tpl);?>

</div>


<table class="ow_table_1 ow_form ow_stdmargin">
    <tr class="ow_alt1 ow_tr_first">
        <td class="">&lt;?xml version="1.0" encoding="utf-8"?&gt;</td>
        <td class="ow_desc ow_small"></td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="">&lt;plugin&gt;</td>
        <td class="ow_desc ow_small"></td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="left_indent">&lt;name&gt;Skeleton&lt;/name&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_name'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="left_indent">&lt;key&gt;skeleton&lt;/key&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_key'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="left_indent">&lt;description&gt;Skeleton&lt;/description&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_desc'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="left_indent">&lt;author&gt;Oxwall Foundation&lt;/author&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_author'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="left_indent">&lt;authorEmail&gt;plugins@oxwall.org&lt;/authorEmail&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_authoremail'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="left_indent">&lt;authorUrl&gt;http://www.oxwall.org/foundation&lt;/authorUrl&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_authorurl'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="left_indent">&lt;developerKey&gt;a1b2c3d4e5f6g7h8i9j0k1l2m3n4o5p6&lt;/developerKey&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_devkey'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="left_indent">&lt;build&gt;1&lt;/build&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_build'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="left_indent">&lt;copyright&gt;(C) 2009 Oxwall Foundation. All rights reserved.&lt;/copyright&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_copyright'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="left_indent">&lt;license&gt;The BSD License&lt;/license&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_license'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="left_indent">&lt;licenseUrl&gt;http://www.opensource.org/licenses/bsd-license.php&lt;/licenseUrl&gt;</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>'skeleton+pluginxml_licenseurl'),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="">&lt;/plugin&gt;</td>
        <td class="ow_desc ow_small"></td>
    </tr>


</table><?php }
}
