<?php
/* Smarty version 3.1.29, created on 2019-05-19 22:43:28
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/example_file_structure.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ce1b1e0dcda87_42548526',
  'file_dependency' => 
  array (
    '8c09b049b9103251ffa6fc52405a05ff406ded4b' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/example_file_structure.html',
      1 => 1545767778,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ce1b1e0dcda87_42548526 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
?>
<div class="page_description">
    <?php echo smarty_function_text(array('key'=>"skeleton+description_file_structure"),$_smarty_tpl);?>

</div>

<table class="ow_table_1 ow_form ow_stdmargin">
    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label"><b>/bol</b></td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_bol"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="ow_label"><b>/classes</b></td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_classes"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label"><b>/components</b></td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_components"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="ow_label"><b>/controllers</b></td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_controllers"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label"><b>/static</b></td>
        <td class="ow_desc ow_small">
            <table>
                <tr class="ow_alt2 ow_tr_first">
                    <td class="ow_label"><b>/css</b></td>
                    <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_static_css"),$_smarty_tpl);?>
</td>
                </tr>

                <tr class="ow_alt1 ow_tr_first">
                    <td class="ow_label"><b>/js</b></td>
                    <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_static_js"),$_smarty_tpl);?>
</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="ow_alt1 ow_tr_first">
        <td class="ow_label"><b>/views</b></td>
        <td class="ow_desc ow_small">
            <table>
                <tr class="ow_alt2 ow_tr_first">
                    <td class="ow_label"><b>/components</b></td>
                    <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_views_components"),$_smarty_tpl);?>
</td>
                </tr>

                <tr class="ow_alt1 ow_tr_first">
                    <td class="ow_label"><b>/controllers</b></td>
                    <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_views_controllers"),$_smarty_tpl);?>
</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label">/activate.php</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_activate"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="ow_label"><a href="<?php echo $_smarty_tpl->tpl_vars['cronUrl']->value;?>
" target="_blank">/cron.php</a></td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_cron"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label">/deactivate.php</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_deactivate"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="ow_label">/init.php</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_init"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label">/install.php</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_install"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt1 ow_tr_first">
        <td class="ow_label"><a href="<?php echo $_smarty_tpl->tpl_vars['pluginxmlUrl']->value;?>
" target="_blank">/plugin.xml</a></td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_pluginxml"),$_smarty_tpl);?>
</td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label">/uninstall.php</td>
        <td class="ow_desc ow_small"><?php echo smarty_function_text(array('key'=>"skeleton+file_structure_uninstall"),$_smarty_tpl);?>
</td>
    </tr>


</table>
<?php }
}
