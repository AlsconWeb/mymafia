<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:17:35
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/forum/views/components/forum_search.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6eb5fad91d0_04025287',
  'file_dependency' => 
  array (
    'c659c7eb42005b335d4354b7d7d56c774194de06' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/forum/views/components/forum_search.html',
      1 => 1470288118,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6eb5fad91d0_04025287 ($_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


    .forum_search_form { display: inline-block; }
    .forum_search_tag_input {
        border-width: 1px;
        border-top: none; border-bottom: none; border-right: none;
        width: 110px;
        padding: 4px;
    }
    .forum_search_wrap {
        display: inline-block;
        background-image: url(<?php echo $_smarty_tpl->tpl_vars['themeUrl']->value;?>
inputbg.gif);
        background-repeat: repeat-x;
        background-position: 0px 0px;
        font-size: 0px;
        line-height: 28px;
        margin: 0px 0px 20px 0px;
        padding: 0px 24px 0px 0px;
        border-radius: 3px;
        border-width: 1px;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        position: relative;
        vertical-align: top;
    }
    body .forum_search_wrap input {
        border: none;
        background-image: none;
        padding: 0px 0px 3px;
        height: 20px;
    }
    .forum_search_input { padding: 4px; width: 130px; }
    .forum_search_inputs .forum_search_input { float: left; }
    .forum_search_inputs .forum_search_tag_input { float: right; }
    .ow_forum_search_context_menu {
        display: none;
        position: absolute;
        right: 4px;
        top: 4px;
    }
    .ow_context_more.ow_miniic_delete { background-position: -1px center; }
    .ow_context_action:hover .ow_context_more.ow_miniic_delete {
        background-image: url(<?php echo $_smarty_tpl->tpl_vars['themeUrl']->value;?>
/miniic_x.png);
        background-position: -1px center;
    }

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<form id="forum_search" class="forum_search_form">
    <div class="forum_search_wrap ow_border">
        <div id="forum_search_cont" class="clearfix<?php if ($_smarty_tpl->tpl_vars['userToken']->value) {?> forum_search_inputs<?php }?>">
            <div class="forum_search_input"><?php echo $_smarty_tpl->tpl_vars['input']->value;?>
</div>
            <div class="ow_forum_search_context_menu" style="display: block;">
                <div class="add_filter"<?php if ($_smarty_tpl->tpl_vars['userToken']->value) {?> style="display: none;"<?php }?>><?php echo $_smarty_tpl->tpl_vars['filterContext']->value;?>
</div>
            </div>
            <div class="forum_search_tag_input ow_border"<?php if (!$_smarty_tpl->tpl_vars['userToken']->value) {?> style="visibility: hidden; height: 0px; padding: 0px;"<?php }?>>
                <div class="ow_forum_search_context_menu" style="display: block;">
                    <div class="ow_context_action_block clearfix">
                        <div class="ow_context_action">
                            <span class="ow_context_more ow_miniic_delete"></span>
                        </div>
                    </div>
                </div>
                <?php echo $_smarty_tpl->tpl_vars['userInput']->value;?>

                <input type="submit" name="search_submit" style="visibility: hidden; position: absolute; top: -1000px;" />
            </div>
        </div>
    </div>
</form><?php }
}
