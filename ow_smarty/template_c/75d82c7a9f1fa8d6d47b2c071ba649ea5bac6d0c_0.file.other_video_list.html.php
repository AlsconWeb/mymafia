<?php
/* Smarty version 3.1.29, created on 2019-04-29 20:21:41
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/video/views/components/other_video_list.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc732a5032494_57072326',
  'file_dependency' => 
  array (
    '75d82c7a9f1fa8d6d47b2c071ba649ea5bac6d0c' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/video/views/components/other_video_list.html',
      1 => 1550930522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc732a5032494_57072326 ($_smarty_tpl) {
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.url_for_route.php';
?>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('block_decorator', array('name'=>'box','iconClass'=>'ow_ic_files','langLabel'=>'video+other_video','addClass'=>'ow_small')); $_block_repeat=true; echo smarty_block_block_decorator(array('name'=>'box','iconClass'=>'ow_ic_files','langLabel'=>'video+other_video','addClass'=>'ow_small'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


	<div class="ow_other_video_list clearfix">
	<?php if ($_smarty_tpl->tpl_vars['clips']->value) {?>
		<?php
$_from = $_smarty_tpl->tpl_vars['clips']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_clip_0_saved_item = isset($_smarty_tpl->tpl_vars['clip']) ? $_smarty_tpl->tpl_vars['clip'] : false;
$_smarty_tpl->tpl_vars['clip'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['clip']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['clip']->value) {
$_smarty_tpl->tpl_vars['clip']->_loop = true;
$__foreach_clip_0_saved_local_item = $_smarty_tpl->tpl_vars['clip'];
?>      
		    <div class="ow_other_video_list_item ow_smallmargin clearfix">
		        <?php $_smarty_tpl->_cache['capture_stack'][] = array('url', null, null); ob_start();
echo smarty_function_url_for_route(array('for'=>"view_clip:[id=>".((string)$_smarty_tpl->tpl_vars['clip']->value['id'])."]"),$_smarty_tpl);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
		
                <div class="ow_other_video_thumb ow_left">
                    <a href="<?php echo (isset($_smarty_tpl->_cache['__smarty_capture']['url']) ? $_smarty_tpl->_cache['__smarty_capture']['url'] : null);?>
"><?php if ($_smarty_tpl->tpl_vars['clip']->value['thumb'] != 'undefined') {?><img src="<?php echo $_smarty_tpl->tpl_vars['clip']->value['thumb'];?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clip']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
" /><?php }?></a>
			    </div>
		        <div class="ow_other_video_item_title"><a href="<?php echo (isset($_smarty_tpl->_cache['__smarty_capture']['url']) ? $_smarty_tpl->_cache['__smarty_capture']['url'] : null);?>
"><?php echo $_smarty_tpl->tpl_vars['clip']->value['title'];?>
</a></div>
		    </div>
		<?php
$_smarty_tpl->tpl_vars['clip'] = $__foreach_clip_0_saved_local_item;
}
if ($__foreach_clip_0_saved_item) {
$_smarty_tpl->tpl_vars['clip'] = $__foreach_clip_0_saved_item;
}
?>
	<?php }?>
	</div>
	
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_block_decorator(array('name'=>'box','iconClass'=>'ow_ic_files','langLabel'=>'video+other_video','addClass'=>'ow_small'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);
}
}
