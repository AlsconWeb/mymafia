<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:17:45
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/blogs/views/components/blog_widget.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6eb69a0b8a4_19485268',
  'file_dependency' => 
  array (
    'dcd3342f7d38aefbdbb3dd6623287abef26faf90' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/blogs/views/components/blog_widget.html',
      1 => 1470288120,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6eb69a0b8a4_19485268 ($_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.decorator.php';
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.url_for_route.php';
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

.ow_ipc .ow_ipc_info .clearfix .ow_ipc_toolbar{
    white-space: normal;
}

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<?php if (!empty($_smarty_tpl->tpl_vars['list']->value)) {?>
	<?php
$_from = $_smarty_tpl->tpl_vars['list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_post_0_saved_item = isset($_smarty_tpl->tpl_vars['post']) ? $_smarty_tpl->tpl_vars['post'] : false;
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['post']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$__foreach_post_0_saved_local_item = $_smarty_tpl->tpl_vars['post'];
?>
		
		<?php $_smarty_tpl->tpl_vars["dto"] = new Smarty_Variable($_smarty_tpl->tpl_vars['post']->value['dto'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "dto", 0);?>
		<?php $_smarty_tpl->tpl_vars['userId'] = new Smarty_Variable($_smarty_tpl->tpl_vars['dto']->value->getAuthorId(), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'userId', 0);?>
		<?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'info_string', null); ob_start(); ?>
			<a href="<?php echo $_smarty_tpl->tpl_vars['post']->value['url'];?>
"><?php echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['post']->value['dto']->getTitle());?>
</a>
		<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

		<?php $_smarty_tpl->_cache['capture_stack'][] = array('default', "moreLink", null); ob_start(); ?><a class="ow_lbutton" href="<?php echo $_smarty_tpl->tpl_vars['post']->value['url'];?>
"><?php echo smarty_function_text(array('key'=>'blogs+more'),$_smarty_tpl);?>
</a><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

		<?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'content', null); ob_start(); ?>
			<?php if ($_smarty_tpl->tpl_vars['commentInfo']->value[$_smarty_tpl->tpl_vars['dto']->value->id] > 0) {?>
				<div class="ow_small ow_smallmargin">
					<span class="ow_txt_value">
						<?php echo $_smarty_tpl->tpl_vars['commentInfo']->value[$_smarty_tpl->tpl_vars['dto']->value->id];?>

					</span>
					<a href="<?php echo $_smarty_tpl->tpl_vars['post']->value['url'];?>
">
						<?php echo smarty_function_text(array('key'=>'blogs+toolbar_comments'),$_smarty_tpl);?>

					</a>
				</div>
			<?php }?>
			<div class="ow_smallmargin"><?php if ($_smarty_tpl->tpl_vars['post']->value['truncated']) {
echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['post']->value['text']);?>
... <?php echo $_smarty_tpl->tpl_vars['moreLink']->value;
} else {
echo preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['post']->value['text']);
}?></div>
		<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

		<?php echo smarty_function_decorator(array('name'=>'ipc','addClass'=>'ow_smallmargin','infoString'=>$_smarty_tpl->tpl_vars['info_string']->value,'content'=>$_smarty_tpl->tpl_vars['content']->value,'avatar'=>$_smarty_tpl->tpl_vars['avatars']->value[$_smarty_tpl->tpl_vars['userId']->value],'toolbar'=>$_smarty_tpl->tpl_vars['tbars']->value[$_smarty_tpl->tpl_vars['dto']->value->id]),$_smarty_tpl);?>

	<?php
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_0_saved_local_item;
}
if ($__foreach_post_0_saved_item) {
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_0_saved_item;
}
} else { ?>
	<div class="ow_nocontent">
		<?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'addnewurl', null); ob_start();
echo smarty_function_url_for_route(array('for'=>'post-save-new'),$_smarty_tpl);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
		<?php echo smarty_function_text(array('key'=>"blogs+index_widget_empty"),$_smarty_tpl);?>
 <a href="<?php echo $_smarty_tpl->tpl_vars['addnewurl']->value;?>
"><?php echo smarty_function_text(array('key'=>'blogs+add_new'),$_smarty_tpl);?>
</a>
	</div>
<?php }
}
}
