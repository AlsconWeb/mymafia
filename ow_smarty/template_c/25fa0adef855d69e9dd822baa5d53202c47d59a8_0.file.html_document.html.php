<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:17:35
  from "/var/www/www-root/data/www/mymafia.su/ow_themes/fitbit-3d/master_pages/html_document.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6eb5fd492b6_97545928',
  'file_dependency' => 
  array (
    '25fa0adef855d69e9dd822baa5d53202c47d59a8' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_themes/fitbit-3d/master_pages/html_document.html',
      1 => 1549235710,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6eb5fd492b6_97545928 ($_smarty_tpl) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $_smarty_tpl->tpl_vars['language']->value;?>
" dir="<?php echo $_smarty_tpl->tpl_vars['direction']->value;?>
">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
<?php echo $_smarty_tpl->tpl_vars['headData']->value;?>

</head>
<!--[if IE 8]><body class="ow ie8<?php echo $_smarty_tpl->tpl_vars['bodyClass']->value;?>
"><![endif]-->
<!--[if !IE 8]><!--><body class="ow<?php echo $_smarty_tpl->tpl_vars['bodyClass']->value;?>
"><!--<![endif]-->
<link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel='stylesheet' href='https://cdn.rawgit.com/daneden/animate.css/v3.1.0/animate.min.css'>
<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
<?php echo '<script'; ?>
 src="https://malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="ow_static/themes/fitbit-3d/js/theia-sticky-sidebar.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
>
(function($) {
/**
   * https://css-tricks.com/slide-in-as-you-scroll-down-boxes/
   * Copyright 2012, Digital Fusion
   * Licensed under the MIT license.
   * http://teamdf.com/jquery-plugins/license/
   *
   * @author Sam Sehnert
   * @desc A small plugin that checks whether elements are within
   *     the user visible viewport of a web browser.
   *     only accounts for vertical position, not horizontal.
   */


  $.fn.visible = function(partial) {
    
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;
    
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };
    
})(jQuery);

<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
>

$(window).scroll(function(event) {
  
  $(".ow_miniic_comment.newsfeed_comment_btn,.ow_miniic_like.newsfeed_like_btn,.ow_newsfeed_content, .ow_newsfeed_avatar").each(function(i, el) {
    var el = $(el);
    if (el.visible(false)) {
      el.addClass("animation_start"); 
    } 
  });
  
});
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
>
var win = $(window);
var allMods = $(".ow_miniic_comment.newsfeed_comment_btn,.ow_miniic_like.newsfeed_like_btn,.ow_canvas .ow_newsfeed_avatar, .ow_newsfeed_content");

allMods.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("animation_start"); 
    } 
  });
  
});

<?php echo '</script'; ?>
>
<?php echo $_smarty_tpl->tpl_vars['pageBody']->value;?>

</body>
</html>
<?php }
}
