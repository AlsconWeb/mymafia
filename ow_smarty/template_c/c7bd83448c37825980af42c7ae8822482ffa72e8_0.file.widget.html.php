<?php
/* Smarty version 3.1.29, created on 2019-04-30 04:41:36
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/profile_progressbar/views/components/widget.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc7a7d0562257_41642644',
  'file_dependency' => 
  array (
    'c7bd83448c37825980af42c7ae8822482ffa72e8' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/profile_progressbar/views/components/widget.html',
      1 => 1550837830,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc7a7d0562257_41642644 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
?>


<table class="ow_table_1 ow_form ow_stdmargin" style="margin: 0">
    <tr class="ow_alt1">
        <td class="ow_label"><?php echo smarty_function_text(array('key'=>'profileprogressbar+progressbar_label_for_user'),$_smarty_tpl);?>
</td>
        <td class="ow_value">
            <div id="profile_progressbar" class="profile-progressbar">
                <span class="profile-progressbar-caption"><?php echo $_smarty_tpl->tpl_vars['percent']->value;?>
%</span>
                <div class="profile-progressbar-complete" style="width: <?php echo $_smarty_tpl->tpl_vars['percent']->value;?>
%"></div>
            </div>
        </td>
    </tr>
</table>
<?php }
}
