<?php
/* Smarty version 3.1.29, created on 2019-05-04 12:01:02
  from "/var/www/www-root/data/www/mymafia.su/ow_system_plugins/base/views/sitemap_part.xml" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ccd54ce2aef83_84392611',
  'file_dependency' => 
  array (
    '0a49f9156088e327084f5fb9cf735b29482049c2' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_system_plugins/base/views/sitemap_part.xml',
      1 => 1549280214,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ccd54ce2aef83_84392611 ($_smarty_tpl) {
echo '<?xml ';?>
version="1.0" encoding="UTF-8"<?php echo '?>';?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
<?php
$_from = $_smarty_tpl->tpl_vars['urls']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_url_0_saved_item = isset($_smarty_tpl->tpl_vars['url']) ? $_smarty_tpl->tpl_vars['url'] : false;
$_smarty_tpl->tpl_vars['url'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['url']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['url']->value) {
$_smarty_tpl->tpl_vars['url']->_loop = true;
$__foreach_url_0_saved_local_item = $_smarty_tpl->tpl_vars['url'];
?>
    <url>
        <changefreq><?php echo $_smarty_tpl->tpl_vars['url']->value['changefreq'];?>
</changefreq>
        <priority><?php echo $_smarty_tpl->tpl_vars['url']->value['priority'];?>
</priority>
        <loc><?php echo $_smarty_tpl->tpl_vars['url']->value['url'];?>
</loc>
        <?php
$_from = $_smarty_tpl->tpl_vars['url']->value['alternateLanguages'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_langData_1_saved_item = isset($_smarty_tpl->tpl_vars['langData']) ? $_smarty_tpl->tpl_vars['langData'] : false;
$_smarty_tpl->tpl_vars['langData'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['langData']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['langData']->value) {
$_smarty_tpl->tpl_vars['langData']->_loop = true;
$__foreach_langData_1_saved_local_item = $_smarty_tpl->tpl_vars['langData'];
?>
        <xhtml:link rel="alternate" hreflang="<?php echo $_smarty_tpl->tpl_vars['langData']->value['code'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['langData']->value['url'];?>
" />
        <?php
$_smarty_tpl->tpl_vars['langData'] = $__foreach_langData_1_saved_local_item;
}
if ($__foreach_langData_1_saved_item) {
$_smarty_tpl->tpl_vars['langData'] = $__foreach_langData_1_saved_item;
}
?>
    </url>
<?php
$_smarty_tpl->tpl_vars['url'] = $__foreach_url_0_saved_local_item;
}
if ($__foreach_url_0_saved_item) {
$_smarty_tpl->tpl_vars['url'] = $__foreach_url_0_saved_item;
}
?>
</urlset>
<?php }
}
