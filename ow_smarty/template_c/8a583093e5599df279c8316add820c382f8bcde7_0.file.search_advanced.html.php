<?php
/* Smarty version 3.1.29, created on 2019-04-30 18:08:52
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/forum/views/controllers/search_advanced.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc865045c0bc5_27318644',
  'file_dependency' => 
  array (
    '8a583093e5599df279c8316add820c382f8bcde7' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/forum/views/controllers/search_advanced.html',
      1 => 1470288118,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc865045c0bc5_27318644 ($_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.submit.php';
if (!is_callable('smarty_block_script')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.script.php';
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    
        .ow_forum_advanced_search .ow_multiselect {
            width: 100%;
            height:200px;
        }

        .ow_forum_advanced_search #search_usage_example_wrapper {
            padding-top:10px;
        }

        #forum_manage_buttons .ow_forum_breadcrumb_wrapper .ow_box_empty {
            margin:0
        }
    
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>



<div style="display: table; width: 100%; margin-bottom: 16px;" id="forum_manage_buttons">
    <div class="ow_forum_breadcrumb_wrapper" style="display: table-cell; vertical-align: bottom; width: 50%;">
        <?php echo $_smarty_tpl->tpl_vars['breadcrumb']->value;?>

    </div>
    <div class="ow_txtright ow_btn_delimiter" style="display: table-cell; vertical-align: bottom;">
        <?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('block_decorator', array('name'=>'button','langLabel'=>"forum+back_forum",'class'=>"ow_ic_left_arrow ow_forum_back")); $_block_repeat=true; echo smarty_block_block_decorator(array('name'=>'button','langLabel'=>"forum+back_forum",'class'=>"ow_ic_left_arrow ow_forum_back"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_block_decorator(array('name'=>'button','langLabel'=>"forum+back_forum",'class'=>"ow_ic_left_arrow ow_forum_back"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

    </div>
</div>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>"search_form")); $_block_repeat=true; echo smarty_block_form(array('name'=>"search_form"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


    <div class="ow_superwide ow_automargin ow_forum_advanced_search">
        <table class="ow_table_1">
            <tr class="ow_tr_first">
                <th colspan="2"><?php echo smarty_function_text(array('key'=>"forum+search"),$_smarty_tpl);?>
</th>
            </tr>
            <tr class="ow_alt1">
                <td colspan="2" class="ow_txtleft ow_small">
                    <?php echo smarty_function_input(array('name'=>"keyword",'class'=>"ow_smallmargin"),$_smarty_tpl);?>

                    <a id="search_usage_example" href="#"><?php echo smarty_function_text(array('key'=>"forum+search_example_usage"),$_smarty_tpl);?>
</a>
                    <div id="search_usage_example_wrapper" style="display:none">
                        <?php echo smarty_function_text(array('key'=>"forum+search_example_usage_text"),$_smarty_tpl);?>
 
                    </div>
                </td>
            </tr>
            <tr class="ow_alt1">
                <td class="ow_txtcenter" width="35%">
                    <?php echo smarty_function_label(array('name'=>"username"),$_smarty_tpl);?>

                </td>
                <td class="ow_txtleft">
                    <?php echo smarty_function_input(array('name'=>"username"),$_smarty_tpl);?>

                </td>
            </tr>
            <tr class="ow_tr_delimiter"><td></td></tr>
            <tr class="ow_tr_first">
                <th colspan="2"><?php echo smarty_function_text(array('key'=>"forum+where_to_search"),$_smarty_tpl);?>
</th>
            </tr>
            <tr class="ow_alt1">
                <td class="ow_txtcenter">
                    <?php echo smarty_function_label(array('name'=>"parts[]"),$_smarty_tpl);?>

                </td>
                <td class="ow_txtleft">
                    <?php echo smarty_function_input(array('name'=>"parts[]"),$_smarty_tpl);?>

                </td>
            </tr>
            <tr class="ow_alt1">
                <td class="ow_txtcenter">
                    <?php echo smarty_function_label(array('name'=>"search_in"),$_smarty_tpl);?>

                </td>
                <td class="ow_txtleft">
                    <?php echo smarty_function_input(array('name'=>"search_in"),$_smarty_tpl);?>

                </td>
            </tr>
            <tr class="ow_tr_delimiter"><td></td></tr>
            <tr class="ow_tr_first">
                <th colspan="2"><?php echo smarty_function_text(array('key'=>"forum+additional"),$_smarty_tpl);?>
</th>
            </tr>
            <tr class="ow_alt1">
                <td class="ow_txtcenter">
                    <?php echo smarty_function_label(array('name'=>"period"),$_smarty_tpl);?>

                </td>
                <td class="ow_txtleft">
                    <?php echo smarty_function_input(array('name'=>"period"),$_smarty_tpl);?>

                </td>
            </tr>
            <tr class="ow_alt1">
                <td class="ow_txtcenter">
                    <?php echo smarty_function_label(array('name'=>"sort"),$_smarty_tpl);?>

                </td>
                <td class="ow_txtleft">
                    <?php echo smarty_function_input(array('name'=>"sort"),$_smarty_tpl);?>

                    <?php echo smarty_function_input(array('name'=>"sort_direction"),$_smarty_tpl);?>

                </td>
            </tr>
        </table>
        
        <div class="clearfix">
            <div class="ow_right">
                <span class="ow_button">
                    <span class=" ow_ic_search">
                        <?php echo smarty_function_submit(array('name'=>"submit"),$_smarty_tpl);?>

                    </span>
                </span>
            </div>
        </div>

    </div>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"search_form"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('script', array()); $_block_repeat=true; echo smarty_block_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    $("#search_usage_example").click(function(e) {
        e.preventDefault();
        $("#search_usage_example_wrapper").toggle("slow");
    });

    $("#forum_manage_buttons .ow_forum_back").bind("click", function(e){
        location.href="<?php echo $_smarty_tpl->tpl_vars['backUrl']->value;?>
";
    });
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);
}
}
