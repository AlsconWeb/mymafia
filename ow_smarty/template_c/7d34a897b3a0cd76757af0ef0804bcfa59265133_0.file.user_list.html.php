<?php
/* Smarty version 3.1.29, created on 2019-04-30 12:28:47
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/hint/views/components/user_list.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc8154fafd3f1_24900188',
  'file_dependency' => 
  array (
    '7d34a897b3a0cd76757af0ef0804bcfa59265133' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/hint/views/components/user_list.html',
      1 => 1546066584,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc8154fafd3f1_24900188 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
?>




<div class="hint-user-list" id="<?php echo $_smarty_tpl->tpl_vars['uniqId']->value;?>
">
    <?php
$_from = $_smarty_tpl->tpl_vars['avatars']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_user_0_saved_item = isset($_smarty_tpl->tpl_vars['user']) ? $_smarty_tpl->tpl_vars['user'] : false;
$_smarty_tpl->tpl_vars['user'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['user']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['user']->value) {
$_smarty_tpl->tpl_vars['user']->_loop = true;
$__foreach_user_0_saved_local_item = $_smarty_tpl->tpl_vars['user'];
?>
        <a href="<?php echo $_smarty_tpl->tpl_vars['user']->value['url'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['user']->value['src'];?>
" /></a>
    <?php
$_smarty_tpl->tpl_vars['user'] = $__foreach_user_0_saved_local_item;
}
if ($__foreach_user_0_saved_item) {
$_smarty_tpl->tpl_vars['user'] = $__foreach_user_0_saved_item;
}
?>
    <?php if ($_smarty_tpl->tpl_vars['viewAll']->value) {?>
        <a href="javascript://" class="hint-view-all ow_border" title="<?php echo smarty_function_text(array('key'=>"hint+view_all_users_title"),$_smarty_tpl);?>
"></a>
    <?php }?>
</div><?php }
}
