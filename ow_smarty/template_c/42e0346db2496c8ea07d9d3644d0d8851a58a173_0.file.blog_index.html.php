<?php
/* Smarty version 3.1.29, created on 2019-04-29 17:41:03
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/blogs/views/controllers/blog_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc70cff953471_57539004',
  'file_dependency' => 
  array (
    '42e0346db2496c8ea07d9d3644d0d8851a58a173' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/blogs/views/controllers/blog_index.html',
      1 => 1470288120,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc70cff953471_57539004 ($_smarty_tpl) {
if (!is_callable('smarty_function_add_content')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.add_content.php';
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
?>
<div class="clearfix"><?php echo smarty_function_add_content(array('key'=>'blogs.add_content.list.top','listType'=>$_smarty_tpl->tpl_vars['listType']->value),$_smarty_tpl);?>
</div>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

.ow_wrap_normal{
    white-space: normal;
}
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<?php if ($_smarty_tpl->tpl_vars['addNew_isAuthorized']->value) {?>
    <?php if ($_smarty_tpl->tpl_vars['addNew_promoted']->value) {?>
        <div class="ow_right"><?php echo smarty_function_decorator(array('name'=>'button','class'=>'ow_ic_add','id'=>'btn-add-new-post','langLabel'=>'blogs+add_new'),$_smarty_tpl);?>
</div>
    <?php } else { ?>
        <div class="ow_right"><?php echo smarty_function_decorator(array('name'=>'button','class'=>'ow_ic_add','id'=>'btn-add-new-post','langLabel'=>'blogs+add_new','onclick'=>"location.href='".((string)$_smarty_tpl->tpl_vars['url_new_post']->value)."'"),$_smarty_tpl);?>
</div>
    <?php }
}?>
	<?php echo $_smarty_tpl->tpl_vars['menu']->value;?>

      <div class="ow_blogs_list clearfix">

         <div class="ow_superwide" style="float:left;">

			<?php if ($_smarty_tpl->tpl_vars['isBrowseByTagCase']->value) {?>       
				<?php if ($_smarty_tpl->tpl_vars['tag']->value) {?>
	         	<div class="ow_anno ow_stdmargin ow_center ow_ic_warning">
					<?php echo smarty_function_text(array('key'=>"blogs+results_by_tag",'tag'=>$_smarty_tpl->tpl_vars['tag']->value),$_smarty_tpl);?>

				</div>
				<?php } else { ?>
					<?php echo $_smarty_tpl->tpl_vars['tagCloud']->value;?>
				
				<?php }?>
         	<?php }?>
                
            <?php if ($_smarty_tpl->tpl_vars['showList']->value) {?>
            <?php
$_from = $_smarty_tpl->tpl_vars['list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_post_0_saved_item = isset($_smarty_tpl->tpl_vars['post']) ? $_smarty_tpl->tpl_vars['post'] : false;
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['post']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$__foreach_post_0_saved_local_item = $_smarty_tpl->tpl_vars['post'];
?>
            
				<?php $_smarty_tpl->tpl_vars['dto'] = new Smarty_Variable($_smarty_tpl->tpl_vars['post']->value['dto'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'dto', 0);?>

				

				<?php $_smarty_tpl->tpl_vars['userId'] = new Smarty_Variable($_smarty_tpl->tpl_vars['dto']->value->getAuthorId(), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'userId', 0);?>

				<?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'info_string', null); ob_start(); ?>
					<a href="<?php echo $_smarty_tpl->tpl_vars['post']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['dto']->value->getTitle();?>
</a>
				<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

				<?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'content', null); ob_start(); ?>
					<?php echo $_smarty_tpl->tpl_vars['post']->value['text'];
if ($_smarty_tpl->tpl_vars['post']->value['showMore']) {?>... <a class="ow_lbutton" href="<?php echo $_smarty_tpl->tpl_vars['post']->value['url'];?>
"><?php echo smarty_function_text(array('key'=>'blogs+more'),$_smarty_tpl);?>
</a><?php }?>
				<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
				<?php $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable($_smarty_tpl->tpl_vars['dto']->value->getId(), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'id', 0);?>

				

            	<?php echo smarty_function_decorator(array('name'=>'ipc','infoString'=>$_smarty_tpl->tpl_vars['info_string']->value,'addClass'=>"ow_stdmargin",'content'=>$_smarty_tpl->tpl_vars['content']->value,'toolbar'=>$_smarty_tpl->tpl_vars['toolbars']->value[$_smarty_tpl->tpl_vars['id']->value],'avatar'=>$_smarty_tpl->tpl_vars['avatars']->value[$_smarty_tpl->tpl_vars['userId']->value]),$_smarty_tpl);?>

            <?php
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_0_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['post']->_loop) {
?>
            	<?php echo smarty_function_text(array('key'=>'base+empty_list'),$_smarty_tpl);?>

            <?php
}
if ($__foreach_post_0_saved_item) {
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_0_saved_item;
}
?>

            <?php if ($_smarty_tpl->tpl_vars['paging']->value) {?><center><?php echo $_smarty_tpl->tpl_vars['paging']->value;?>
</center><?php }?>
            <?php }?>
        </div>    

         <div class="ow_supernarrow" style="float:right;">
         	<?php echo $_smarty_tpl->tpl_vars['tagSearch']->value;?>

         	<?php if (count($_smarty_tpl->tpl_vars['list']->value) > 0) {?>
	         	<?php echo $_smarty_tpl->tpl_vars['tagCloud']->value;?>

         	<?php }?>
         </div>

      </div><?php }
}
