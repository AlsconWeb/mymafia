<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:17:45
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/questions/views/components/question_add.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6eb69db84f5_01219494',
  'file_dependency' => 
  array (
    'b637fa1f524bf46fe57a0013c60256daae54d25b' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/questions/views/components/question_add.html',
      1 => 1546678374,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6eb69db84f5_01219494 ($_smarty_tpl) {
if (!is_callable('smarty_block_form')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.submit.php';
?>
<div class="questions-add clearfix" id="<?php echo $_smarty_tpl->tpl_vars['uniqId']->value;?>
">
    <?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>"questions_add")); $_block_repeat=true; echo smarty_block_form(array('name'=>"questions_add"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

    <div class="form_auto_click">
        <div class="questions-add-question">
            <?php echo smarty_function_input(array('name'=>"question",'class'=>"questions-input"),$_smarty_tpl);?>

        </div>
        <div class="ow_submit_auto_click" style="display: none;">

            <div class="questions-add-answers" style="display: none;">
                <?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('block_decorator', array('name'=>'tooltip','addClass'=>'qaa-tooltip ow_small ')); $_block_repeat=true; echo smarty_block_block_decorator(array('name'=>'tooltip','addClass'=>'qaa-tooltip ow_small '), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                    <div class="q-add-answers-field">
                        <div class="ow_smallmargin">
                            <div class="qaa-label-c">
                                <span class="qaa-label"><strong><?php echo smarty_function_text(array('key'=>"questions+question_add_answers_label"),$_smarty_tpl);?>
</strong></span>
                            </div>
                        </div>

                        <?php echo smarty_function_input(array('name'=>"answers"),$_smarty_tpl);?>

                    </div>
                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_block_decorator(array('name'=>'tooltip','addClass'=>'qaa-tooltip ow_small '), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

            </div>

            <div class="clearfix qa-submit-c ow_border">
                <div class="ow_left questions-add-answers-btn-c">
                    <a href="javascript://" class="questions-add-answers-btn"><?php echo smarty_function_text(array('key'=>"questions+question_add_show_options_btn"),$_smarty_tpl);?>
</a>
                    <div class="questions-add-answers-options" style="display: none;">
                        <?php echo smarty_function_input(array('name'=>"allowAddOprions"),$_smarty_tpl);
echo smarty_function_label(array('name'=>"allowAddOprions"),$_smarty_tpl);?>

                    </div>
                </div>

                <div class="ow_right q-save-c">
                    <span class="ow_attachment_btn"><?php echo smarty_function_submit(array('name'=>"save"),$_smarty_tpl);?>
</span>
                </div>
                
                <div class="ow_inprogress q-status-preloader Q_StatusPreloader"></div>
            </div>
        </div>
    </div>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"questions_add"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

</div><?php }
}
