<?php
/* Smarty version 3.1.29, created on 2019-04-29 16:18:56
  from "/var/www/www-root/data/www/mymafia.su/ow_system_plugins/base/decorators/ic.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6f9c062baf6_68738280',
  'file_dependency' => 
  array (
    '5060a887aeea9f6bad3c0efcbb3c8a4849e1ae73' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_system_plugins/base/decorators/ic.html',
      1 => 1549280214,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6f9c062baf6_68738280 ($_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
?>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


.ow_ic_toolbar span{
    display:inline-block;
}

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

<?php if ($_smarty_tpl->tpl_vars['data']->value['first']) {?>
	<ul class="ow_regular ow_ic">
<?php }?>

<li>
	<div class="ow_ic_header"><a <?php if (!empty($_smarty_tpl->tpl_vars['data']->value['titleHrefBlank'])) {?>target="_blank"<?php }?> href="<?php echo $_smarty_tpl->tpl_vars['data']->value['titleHref'];?>
"><?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>
</a></div>

	<div class="ow_ic_description"><?php echo $_smarty_tpl->tpl_vars['data']->value['info'];?>
</div>

	<?php if (!empty($_smarty_tpl->tpl_vars['data']->value['toolbar'])) {?>
  		<div class="ow_ic_toolbar ow_small ow_remark clearfix">
  		<?php
$_from = $_smarty_tpl->tpl_vars['data']->value['toolbar'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_toolbar_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_foreach_toolbar']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_toolbar'] : false;
$__foreach_toolbar_0_saved_item = isset($_smarty_tpl->tpl_vars['item']) ? $_smarty_tpl->tpl_vars['item'] : false;
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['__smarty_foreach_toolbar'] = new Smarty_Variable(array());
$__foreach_toolbar_0_first = true;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$_smarty_tpl->tpl_vars['__smarty_foreach_toolbar']->value['first'] = $__foreach_toolbar_0_first;
$__foreach_toolbar_0_first = false;
$__foreach_toolbar_0_saved_local_item = $_smarty_tpl->tpl_vars['item'];
?>
            <?php if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_toolbar']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_toolbar']->value['first'] : null) && (empty($_smarty_tpl->tpl_vars['item']->value['class']) || $_smarty_tpl->tpl_vars['item']->value['class'] != 'ow_ipc_date')) {?> <span class="ow_delimiter">&middot;</span> <?php }?>
  		    <span class="ow_nowrap<?php if (!empty($_smarty_tpl->tpl_vars['item']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['item']->value['class'];
}?>">
                <?php if (!empty($_smarty_tpl->tpl_vars['item']->value['href'])) {?><a <?php if (!empty($_smarty_tpl->tpl_vars['item']->value['id'])) {?> id="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
"<?php }?> href="<?php echo $_smarty_tpl->tpl_vars['item']->value['href'];?>
"><?php }
echo $_smarty_tpl->tpl_vars['item']->value['label'];
if (!empty($_smarty_tpl->tpl_vars['item']->value['href'])) {?></a><?php }?>
  		    </span>
  		<?php
$_smarty_tpl->tpl_vars['item'] = $__foreach_toolbar_0_saved_local_item;
}
if ($__foreach_toolbar_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_foreach_toolbar'] = $__foreach_toolbar_0_saved;
}
if ($__foreach_toolbar_0_saved_item) {
$_smarty_tpl->tpl_vars['item'] = $__foreach_toolbar_0_saved_item;
}
?>
  		</div>
    <?php }?>
</li>

<?php if ($_smarty_tpl->tpl_vars['data']->value['last']) {?>
	</ul>
<?php }
}
}
