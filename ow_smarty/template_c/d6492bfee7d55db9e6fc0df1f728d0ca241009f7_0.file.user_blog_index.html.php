<?php
/* Smarty version 3.1.29, created on 2019-04-30 02:14:01
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/blogs/views/controllers/user_blog_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc785395e3066_31439760',
  'file_dependency' => 
  array (
    'd6492bfee7d55db9e6fc0df1f728d0ca241009f7' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/blogs/views/controllers/user_blog_index.html',
      1 => 1470288120,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc785395e3066_31439760 ($_smarty_tpl) {
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.block_decorator.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_add_content')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.add_content.php';
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.url_for_route.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.decorator.php';
?>
<div class="clearfix">
    <div class="ow_superwide" style="float:left;">
        <?php
$_from = $_smarty_tpl->tpl_vars['list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_post_foreach_0_saved_item = isset($_smarty_tpl->tpl_vars['post']) ? $_smarty_tpl->tpl_vars['post'] : false;
$_smarty_tpl->tpl_vars['post'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['post']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['post']->_loop = true;
$__foreach_post_foreach_0_saved_local_item = $_smarty_tpl->tpl_vars['post'];
?>
            <?php $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable($_smarty_tpl->tpl_vars['post']->value['id'], null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, 'id', 0);?>

                    <?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('block_decorator', array('name'=>"box",'type'=>"empty",'label'=>$_smarty_tpl->tpl_vars['post']->value['title'],'href'=>$_smarty_tpl->tpl_vars['post']->value['href'],'addClass'=>"ow_stdmargin ow_stdpadding",'toolbar'=>$_smarty_tpl->tpl_vars['tb']->value[$_smarty_tpl->tpl_vars['id']->value])); $_block_repeat=true; echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty",'label'=>$_smarty_tpl->tpl_vars['post']->value['title'],'href'=>$_smarty_tpl->tpl_vars['post']->value['href'],'addClass'=>"ow_stdmargin ow_stdpadding",'toolbar'=>$_smarty_tpl->tpl_vars['tb']->value[$_smarty_tpl->tpl_vars['id']->value]), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                        <?php echo $_smarty_tpl->tpl_vars['post']->value['text'];
if ($_smarty_tpl->tpl_vars['post']->value['truncated']) {?>... <a class="ow_lbutton" href="<?php echo $_smarty_tpl->tpl_vars['post']->value['href'];?>
"><?php echo smarty_function_text(array('key'=>'blogs+more'),$_smarty_tpl);?>
</a><?php }?>


                    <?php if (!empty($_smarty_tpl->tpl_vars['post']->value['parts']) && count($_smarty_tpl->tpl_vars['post']->value['parts']) > 1) {?>
                        <br />
                        <a href="<?php echo $_smarty_tpl->tpl_vars['post']->value['href'];?>
"><?php echo smarty_function_text(array('key'=>"blogs+read_more"),$_smarty_tpl);?>
</a>&nbsp;&raquo;
                    <?php }?>
                    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_block_decorator(array('name'=>"box",'type'=>"empty",'label'=>$_smarty_tpl->tpl_vars['post']->value['title'],'href'=>$_smarty_tpl->tpl_vars['post']->value['href'],'addClass'=>"ow_stdmargin ow_stdpadding",'toolbar'=>$_smarty_tpl->tpl_vars['tb']->value[$_smarty_tpl->tpl_vars['id']->value]), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

        <?php
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_foreach_0_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['post']->_loop) {
?>
            <?php echo smarty_function_text(array('key'=>'base+empty_list'),$_smarty_tpl);?>

        <?php
}
if ($__foreach_post_foreach_0_saved_item) {
$_smarty_tpl->tpl_vars['post'] = $__foreach_post_foreach_0_saved_item;
}
?>

        <div class="ow_stdmargin"><?php echo $_smarty_tpl->tpl_vars['paging']->value;?>
</div>

    </div>

    <div class="ow_supernarrow" style="float:right;">

       <?php echo smarty_function_add_content(array('key'=>'socialsharing.get_sharing_buttons','entityType'=>'user_blog','entityId'=>$_smarty_tpl->tpl_vars['author']->value->id),$_smarty_tpl);?>


       <?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('block_decorator', array('name'=>"box",'langLabel'=>"blogs+blog_archive_lbl_archives",'iconClass'=>"ow_ic_clock")); $_block_repeat=true; echo smarty_block_block_decorator(array('name'=>"box",'langLabel'=>"blogs+blog_archive_lbl_archives",'iconClass'=>"ow_ic_clock"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

          <ul class="ow_regular">
            <li><a href="<?php echo smarty_function_url_for_route(array('for'=>"user-blog:[user=>".((string)$_smarty_tpl->tpl_vars['username']->value)."]"),$_smarty_tpl);?>
"><?php echo smarty_function_text(array('key'=>"base+all"),$_smarty_tpl);?>
</a></li>
            <?php
$_from = $_smarty_tpl->tpl_vars['archive']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_months_1_saved_item = isset($_smarty_tpl->tpl_vars['months']) ? $_smarty_tpl->tpl_vars['months'] : false;
$__foreach_months_1_saved_key = isset($_smarty_tpl->tpl_vars['year']) ? $_smarty_tpl->tpl_vars['year'] : false;
$_smarty_tpl->tpl_vars['months'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['year'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['months']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['year']->value => $_smarty_tpl->tpl_vars['months']->value) {
$_smarty_tpl->tpl_vars['months']->_loop = true;
$__foreach_months_1_saved_local_item = $_smarty_tpl->tpl_vars['months'];
?>
                <?php
$_from = $_smarty_tpl->tpl_vars['months']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_month_2_saved_item = isset($_smarty_tpl->tpl_vars['month']) ? $_smarty_tpl->tpl_vars['month'] : false;
$_smarty_tpl->tpl_vars['month'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['month']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['month']->value) {
$_smarty_tpl->tpl_vars['month']->_loop = true;
$__foreach_month_2_saved_local_item = $_smarty_tpl->tpl_vars['month'];
?>
                    <li><a href="<?php echo smarty_function_url_for_route(array('for'=>"user-blog:[user=>".((string)$_smarty_tpl->tpl_vars['username']->value)."]"),$_smarty_tpl);?>
?month=<?php echo $_smarty_tpl->tpl_vars['month']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['year']->value;?>
"><?php echo smarty_function_text(array('key'=>"base+month_".((string)$_smarty_tpl->tpl_vars['month']->value)),$_smarty_tpl);?>
 <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
</a> </li>
                <?php
$_smarty_tpl->tpl_vars['month'] = $__foreach_month_2_saved_local_item;
}
if ($__foreach_month_2_saved_item) {
$_smarty_tpl->tpl_vars['month'] = $__foreach_month_2_saved_item;
}
?>
            <?php
$_smarty_tpl->tpl_vars['months'] = $__foreach_months_1_saved_local_item;
}
if ($__foreach_months_1_saved_item) {
$_smarty_tpl->tpl_vars['months'] = $__foreach_months_1_saved_item;
}
if ($__foreach_months_1_saved_key) {
$_smarty_tpl->tpl_vars['year'] = $__foreach_months_1_saved_key;
}
?>
          </ul>
       <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_block_decorator(array('name'=>"box",'langLabel'=>"blogs+blog_archive_lbl_archives",'iconClass'=>"ow_ic_clock"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

       <?php if ($_smarty_tpl->tpl_vars['isOwner']->value) {?>
       <div style="" class="ow_box ow_stdmargin clearfix ow_no_cap ow_break_word">
            <div class="ow_my_drafts_widget clearfix ow_center">
                <?php echo smarty_function_decorator(array('name'=>"button",'langLabel'=>"blogs+my_drafts",'class'=>"ow_ic_draft",'onclick'=>"location.href='".((string)$_smarty_tpl->tpl_vars['my_drafts_url']->value)."'"),$_smarty_tpl);?>

            </div>
        </div>
       <?php }?>
       <?php echo smarty_function_add_content(array('key'=>'blogs.user_blog.content.after_archive'),$_smarty_tpl);?>

    </div>
</div><?php }
}
