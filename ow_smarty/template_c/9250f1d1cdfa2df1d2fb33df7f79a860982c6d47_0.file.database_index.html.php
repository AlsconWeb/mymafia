<?php
/* Smarty version 3.1.29, created on 2019-05-19 23:30:10
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/database_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ce1bcd2870ba7_58655680',
  'file_dependency' => 
  array (
    '9250f1d1cdfa2df1d2fb33df7f79a860982c6d47' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/database_index.html',
      1 => 1545767778,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ce1bcd2870ba7_58655680 ($_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_desc')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.desc.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.submit.php';
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


.skeleton-list {
    padding: 10px;
}

.skeleton-list-item {
    padding: 5px;
    border-width: 1px;
}

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<div class="page_description">
    <?php echo smarty_function_text(array('key'=>"skeleton+description_database"),$_smarty_tpl);?>


    <div class="example_file">
        <table>
            <tr>
                <td>
                    <?php echo smarty_function_text(array('key'=>"skeleton+example_file"),$_smarty_tpl);?>

                </td>
                <td>
                    /ow_plugins/skeleton/controllers/example.php;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    /ow_plugins/skeleton/bol/service.php;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    /ow_plugins/skeleton/bol/record.php;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    /ow_plugins/skeleton/bol/record_dao.php
                </td>
            </tr>
        </table>
    </div>
</div>

<?php if ($_smarty_tpl->tpl_vars['list']->value) {?><h3 class="ow_center" style="margin-bottom: 10px;"><?php echo smarty_function_text(array('key'=>"skeleton+database_records"),$_smarty_tpl);?>
</h3><?php }?>
    <table class="ow_table_1 ow_form ow_stdmargin">
    <?php
$_from = $_smarty_tpl->tpl_vars['list']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_listItem_0_saved_item = isset($_smarty_tpl->tpl_vars['listItem']) ? $_smarty_tpl->tpl_vars['listItem'] : false;
$_smarty_tpl->tpl_vars['listItem'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['listItem']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['listItem']->value) {
$_smarty_tpl->tpl_vars['listItem']->_loop = true;
$__foreach_listItem_0_saved_local_item = $_smarty_tpl->tpl_vars['listItem'];
?>
        <tr onmouseover="$('a.del-cont', this).show();" onmouseout="$('a.del-cont', this).hide();" class="skeleton-list-item ow_smallmargin ow_border">
            <td>
                <div><?php echo smarty_function_text(array('key'=>"skeleton+forms_text_field_label"),$_smarty_tpl);?>
: <?php echo $_smarty_tpl->tpl_vars['listItem']->value['text'];?>
</div>
                <div><?php echo smarty_function_text(array('key'=>"skeleton+forms_textarea_field_label"),$_smarty_tpl);?>
: <?php echo $_smarty_tpl->tpl_vars['listItem']->value['extendedText'];?>
</div>
                <div><?php echo smarty_function_text(array('key'=>"skeleton+forms_selectbox_field_label"),$_smarty_tpl);?>
: <?php echo $_smarty_tpl->tpl_vars['listItem']->value['choice'];?>
</div>
            </td>
            <td class="ow_center ns-hover-block" style="width: 100px;"><a style="display: none;" class="ow_lbutton ow_red del-cont" href="<?php echo $_smarty_tpl->tpl_vars['listItem']->value['deleteUrl'];?>
" onclick="if (confirm('<?php echo smarty_function_text(array('key'=>"base+are_you_sure"),$_smarty_tpl);?>
')) return true; else return false;"><?php echo smarty_function_text(array('key'=>'base+delete'),$_smarty_tpl);?>
</a></td>
        </tr>
    <?php
$_smarty_tpl->tpl_vars['listItem'] = $__foreach_listItem_0_saved_local_item;
}
if ($__foreach_listItem_0_saved_item) {
$_smarty_tpl->tpl_vars['listItem'] = $__foreach_listItem_0_saved_item;
}
?>
    </table>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>"database_form")); $_block_repeat=true; echo smarty_block_form(array('name'=>"database_form"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


    <table class="ow_table_1 ow_form ow_stdmargin">
        <tbody>
            <tr class="ow_tr_first ow_alt1">
                    <td class="ow_label">
                        <?php echo smarty_function_label(array('name'=>"text"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_value">
                        <?php echo smarty_function_input(array('name'=>"text"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_desc ow_small">
                        <?php echo smarty_function_desc(array('name'=>"text"),$_smarty_tpl);?>

                    </td>
            </tr>
            
            <tr class="ow_alt2">
                    <td class="ow_label">
                        <?php echo smarty_function_label(array('name'=>"extended_text"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_value">
                        <?php echo smarty_function_input(array('name'=>"extended_text"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_desc ow_small">
                        <?php echo smarty_function_desc(array('name'=>"extended_text"),$_smarty_tpl);?>

                    </td>
            </tr>
            
            <tr class="ow_alt1">
                    <td class="ow_label">
                        <?php echo smarty_function_label(array('name'=>"selectbox"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_value">
                        <?php echo smarty_function_input(array('name'=>"selectbox"),$_smarty_tpl);?>

                    </td>
                    <td class="ow_desc ow_small">
                        <?php echo smarty_function_desc(array('name'=>"selectbox"),$_smarty_tpl);?>

                    </td>
            </tr>

        </tbody>
    </table>

    <div class="clearfix ow_stdmargin">
        <div class="ow_right">
             <?php echo smarty_function_submit(array('name'=>"submit"),$_smarty_tpl);?>

        </div>
    </div>

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"database_form"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);
}
}
