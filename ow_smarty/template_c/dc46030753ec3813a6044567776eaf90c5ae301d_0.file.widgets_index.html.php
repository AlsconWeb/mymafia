<?php
/* Smarty version 3.1.29, created on 2019-05-20 05:28:57
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/widgets_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ce210e945d9a9_91730277',
  'file_dependency' => 
  array (
    'dc46030753ec3813a6044567776eaf90c5ae301d' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/widgets_index.html',
      1 => 1545767780,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ce210e945d9a9_91730277 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.submit.php';
?>
<div class="page_description">
    <?php echo smarty_function_text(array('key'=>"skeleton+description_widgets"),$_smarty_tpl);?>


    <div class="example_file"><?php echo smarty_function_text(array('key'=>"skeleton+example_file"),$_smarty_tpl);?>
 /ow_plugins/skeleton/controllers/widgets.php</div>
</div>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>"WidgetExampleForm")); $_block_repeat=true; echo smarty_block_form(array('name'=>"WidgetExampleForm"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

<table class="ow_table_1 ow_form ow_stdmargin">
    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label"><?php echo smarty_function_text(array('key'=>'skeleton+widget'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo smarty_function_text(array('key'=>'skeleton+latest_users_widget'),$_smarty_tpl);?>
</td>
    </tr>
    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'place'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo smarty_function_input(array('name'=>'place'),$_smarty_tpl);
echo smarty_function_error(array('name'=>'place'),$_smarty_tpl);?>
</td>
    </tr>
    <tr class="ow_alt1 ow_tr_last">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'section'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo smarty_function_input(array('name'=>'section'),$_smarty_tpl);
echo smarty_function_error(array('name'=>'section'),$_smarty_tpl);?>
</td>
    </tr>
</table>
<div class="clearfix ow_submit ow_stdmargin">
    <div class="ow_right"><?php echo smarty_function_submit(array('name'=>'add','class'=>'ow_positive'),$_smarty_tpl);?>
</div>
</div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"WidgetExampleForm"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<div class="ow_center ow_stdmargin"><h2><?php echo smarty_function_text(array('key'=>'skeleton+created_widgets'),$_smarty_tpl);?>
</h2></div>
<table class="ow_table_1 ow_form ow_stdmargin">
    <tr>
        <th><?php echo smarty_function_text(array('key'=>'skeleton+widget_title'),$_smarty_tpl);?>
</th>
        <th><?php echo smarty_function_text(array('key'=>'skeleton+widget_placement'),$_smarty_tpl);?>
</th>
        <th>&nbsp;</th>
    </tr>
<?php
$_from = $_smarty_tpl->tpl_vars['addedComponentList']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_component_0_saved_item = isset($_smarty_tpl->tpl_vars['component']) ? $_smarty_tpl->tpl_vars['component'] : false;
$_smarty_tpl->tpl_vars['component'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['component']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['component']->value) {
$_smarty_tpl->tpl_vars['component']->_loop = true;
$__foreach_component_0_saved_local_item = $_smarty_tpl->tpl_vars['component'];
?>
    <tr onmouseover="$('a.del-cont', this).show();" onmouseout="$('a.del-cont', this).hide();">
        <td class="ow_label"><?php echo smarty_function_text(array('key'=>'skeleton+latest_users_widget'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo $_smarty_tpl->tpl_vars['component']->value['place'];?>
</td>
        <td class="ow_center ns-hover-block" style="width: 100px;"><a style="display: none;" class="ow_lbutton ow_red del-cont" href="<?php echo $_smarty_tpl->tpl_vars['component']->value['deleteUrl'];?>
" onclick="if (confirm('<?php echo smarty_function_text(array('key'=>"base+are_you_sure"),$_smarty_tpl);?>
')) return true; else return false;"><?php echo smarty_function_text(array('key'=>'skeleton+remove'),$_smarty_tpl);?>
</a></td>
    </tr>
<?php
$_smarty_tpl->tpl_vars['component'] = $__foreach_component_0_saved_local_item;
}
if ($__foreach_component_0_saved_item) {
$_smarty_tpl->tpl_vars['component'] = $__foreach_component_0_saved_item;
}
?>
</table>
<?php }
}
