<?php
/* Smarty version 3.1.29, created on 2019-05-20 05:24:50
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/notifications_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ce20ff2190645_23407909',
  'file_dependency' => 
  array (
    'b827b9c3e86b73b2213749ea92763e760dfdb844' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/notifications_index.html',
      1 => 1545767780,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ce20ff2190645_23407909 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.submit.php';
?>

<div class="page_description">
    <?php echo smarty_function_text(array('key'=>"skeleton+description_notifications"),$_smarty_tpl);?>


    <div class="example_file">
        <table>
            <tr>
                <td>
                    <?php echo smarty_function_text(array('key'=>"skeleton+example_file"),$_smarty_tpl);?>

                </td>
                <td>
                    /ow_plugins/skeleton/controllers/notifications.php;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    /ow_plugins/skeleton/classes/notification_example_form.php
                </td>
            </tr>
        </table>
    </div>
</div>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>"NotificationExampleForm")); $_block_repeat=true; echo smarty_block_form(array('name'=>"NotificationExampleForm"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

<table class="ow_table_1 ow_form ow_stdmargin">
    <tr class="ow_alt1 ow_tr_first">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'content'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo smarty_function_input(array('name'=>'content'),$_smarty_tpl);
echo smarty_function_error(array('name'=>'content'),$_smarty_tpl);?>
</td>
    </tr>
    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'image'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo smarty_function_input(array('name'=>'image'),$_smarty_tpl);
echo smarty_function_error(array('name'=>'image'),$_smarty_tpl);?>
</td>
    </tr>
    <tr>
        <td colspan="2" class="ow_center ow_small">
            <?php echo smarty_function_text(array('key'=>'skeleton+notification_note'),$_smarty_tpl);?>

        </td>
    </tr>
</table>
<div class="clearfix ow_submit ow_stdmargin">
    <div class="ow_right"><?php echo smarty_function_submit(array('name'=>'send','class'=>'ow_positive'),$_smarty_tpl);?>
</div>
</div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"NotificationExampleForm"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<?php }
}
