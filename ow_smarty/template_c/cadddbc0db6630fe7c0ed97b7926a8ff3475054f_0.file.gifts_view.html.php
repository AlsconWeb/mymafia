<?php
/* Smarty version 3.1.29, created on 2019-05-02 12:55:13
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/virtual_gifts/views/controllers/gifts_view.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ccabe817c5611_52619818',
  'file_dependency' => 
  array (
    'cadddbc0db6630fe7c0ed97b7926a8ff3475054f' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/virtual_gifts/views/controllers/gifts_view.html',
      1 => 1470288120,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ccabe817c5611_52619818 ($_smarty_tpl) {
if (!is_callable('smarty_function_format_date')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.format_date.php';
if (!is_callable('smarty_function_add_content')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.add_content.php';
?>

<?php if (isset($_smarty_tpl->tpl_vars['noPermission']->value)) {?>
    <div class="ow_anno ow_std_margin ow_nocontent"><?php echo $_smarty_tpl->tpl_vars['noPermission']->value;?>
</div>
<?php } else { ?>
	<div class="ow_wide ow_automargin">
        <div class="ow_stdmargin ow_center"><img src="<?php echo $_smarty_tpl->tpl_vars['gift']->value['imageUrl'];?>
" /></div>
        
        <?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'infoString', null); ob_start(); ?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['senderUrl']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['senderName']->value;?>
</a>
            <span class="ow_nowrap ow_tiny ow_ipc_date"><?php echo smarty_function_format_date(array('timestamp'=>$_smarty_tpl->tpl_vars['gift']->value['dto']->sendTimestamp),$_smarty_tpl);?>
</span>
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
        <?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'content', null); ob_start(); ?>
            <div class="ow_smallmargin"><?php if ($_smarty_tpl->tpl_vars['gift']->value['dto']->message != '') {
echo $_smarty_tpl->tpl_vars['gift']->value['dto']->message;
}?></div>
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['decorator'][0][0]->processDecorator(array('name'=>'ipc','addClass'=>'ow_stdmargin','infoString'=>$_smarty_tpl->tpl_vars['infoString']->value,'avatar'=>$_smarty_tpl->tpl_vars['senderAvatar']->value,'content'=>$_smarty_tpl->tpl_vars['content']->value,'toolbar'=>$_smarty_tpl->tpl_vars['toolbar']->value),$_smarty_tpl);?>

        <?php echo smarty_function_add_content(array('key'=>'socialsharing.get_sharing_buttons','title'=>$_smarty_tpl->tpl_vars['title']->value,'description'=>$_smarty_tpl->tpl_vars['gift']->value['dto']->message,'image'=>$_smarty_tpl->tpl_vars['gift']->value['imageUrl'],'entityType'=>'virtualgifts','entityId'=>$_smarty_tpl->tpl_vars['gift']->value['dto']->id),$_smarty_tpl);?>

	</div>
    
	<?php echo smarty_function_add_content(array('key'=>'virtualgifts.gifts_view.content.between_gift_and_send_button'),$_smarty_tpl);?>

	<?php if ($_smarty_tpl->tpl_vars['sendToFriends']->value) {?>
	<div class="ow_wide ow_automargin ow_center">
        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['decorator'][0][0]->processDecorator(array('name'=>'button','id'=>'send_gift_btn','langLabel'=>'virtualgifts+send_this_gift','class'=>'ow_ic_heart'),$_smarty_tpl);?>

	</div>
	<?php }
}
}
}
