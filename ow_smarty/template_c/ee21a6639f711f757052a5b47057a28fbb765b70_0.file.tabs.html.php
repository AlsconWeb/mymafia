<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:17:45
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/questions/views/components/tabs.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6eb69dcf2b5_35716687',
  'file_dependency' => 
  array (
    'ee21a6639f711f757052a5b47057a28fbb765b70' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/questions/views/components/tabs.html',
      1 => 1546678374,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6eb69dcf2b5_35716687 ($_smarty_tpl) {
?>
<div class="gtabs" id="<?php echo $_smarty_tpl->tpl_vars['uniqId']->value;?>
">

    <div class="gtabs-tabs-c ow_smallmargin clearfix">
    <?php
$_from = $_smarty_tpl->tpl_vars['tabs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_t_0_saved_item = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$__foreach_t_0_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['t']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
$__foreach_t_0_saved_local_item = $_smarty_tpl->tpl_vars['t'];
?>
        <a class="gtabs-tab <?php echo $_smarty_tpl->tpl_vars['t']->value['icon'];?>
 <?php if ($_smarty_tpl->tpl_vars['t']->value['active']) {?>gtabs-active<?php }?>" href="javascript://" data-key="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
">
            <?php echo $_smarty_tpl->tpl_vars['t']->value['label'];?>

        </a>
    <?php
$_smarty_tpl->tpl_vars['t'] = $__foreach_t_0_saved_local_item;
}
if ($__foreach_t_0_saved_item) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_t_0_saved_item;
}
if ($__foreach_t_0_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_t_0_saved_key;
}
?>
    </div>

    <div class="gtabs-contents-c">
    <?php
$_from = $_smarty_tpl->tpl_vars['tabs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_t_1_saved_item = isset($_smarty_tpl->tpl_vars['t']) ? $_smarty_tpl->tpl_vars['t'] : false;
$__foreach_t_1_saved_key = isset($_smarty_tpl->tpl_vars['k']) ? $_smarty_tpl->tpl_vars['k'] : false;
$_smarty_tpl->tpl_vars['t'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['k'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['t']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['t']->value) {
$_smarty_tpl->tpl_vars['t']->_loop = true;
$__foreach_t_1_saved_local_item = $_smarty_tpl->tpl_vars['t'];
?>
        <div class="gtabs-contents" data-key="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if (!$_smarty_tpl->tpl_vars['t']->value['active']) {?>style="display: none;"<?php }?>>
            <?php echo $_smarty_tpl->tpl_vars['t']->value['content'];?>

        </div>
    <?php
$_smarty_tpl->tpl_vars['t'] = $__foreach_t_1_saved_local_item;
}
if ($__foreach_t_1_saved_item) {
$_smarty_tpl->tpl_vars['t'] = $__foreach_t_1_saved_item;
}
if ($__foreach_t_1_saved_key) {
$_smarty_tpl->tpl_vars['k'] = $__foreach_t_1_saved_key;
}
?>
    </div>

</div><?php }
}
