<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:17:32
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/oaboost/views/controllers/admin_cache_control.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6eb5c074904_73220712',
  'file_dependency' => 
  array (
    'af885cb1ec7e5549a05aca606d3d1f6ab2774e47' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/oaboost/views/controllers/admin_cache_control.html',
      1 => 1555185269,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6eb5c074904_73220712 ($_smarty_tpl) {
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/www-root/data/www/mymafia.su/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.decorator.php';
$_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


.oacompress_form .ow_value{
    text-align:center;
}
.oacompress_form .ow_label{
    width:35% !important;
}

.oacompress_form .ow_desc{
    width:45% !important;
}
.oacompress_form label{
    cursor:pointer;
}

<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

<?php echo $_smarty_tpl->tpl_vars['adminLogo']->value;?>

<?php echo $_smarty_tpl->tpl_vars['contentMenu']->value;?>

<form name="oacache" action="" method="post">
<table class="ow_table_1 ow_form oacompress_form">
    <tr>
        <th class="ow_name ow_txtleft" colspan="3">
            <span class="ow_section_icon ow_ic_gear_wheel"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_section_label"),$_smarty_tpl);?>
</span>
        </th>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><label for="oa_compress"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_oacompress_label"),$_smarty_tpl);?>
</label></td>
        <td class="ow_value"><input type="checkbox" name="oacompress" id="oa_compress" /></td>
        <td class="ow_desc"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_oacompress_desc"),$_smarty_tpl);?>
</td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><label for="oa_template"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_template_label"),$_smarty_tpl);?>
</label></td>
        <td class="ow_value"><input type="checkbox" name="mode[]" value="<?php echo $_smarty_tpl->tpl_vars['checkboxData']->value['oa_template'];?>
" id="oa_template" /></td>
        <td class="ow_desc"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_template_desc"),$_smarty_tpl);?>
</td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><label for="oa_theme"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_theme_label"),$_smarty_tpl);?>
</label></td>
        <td class="ow_value"><input type="checkbox" name="mode[]" value="<?php echo $_smarty_tpl->tpl_vars['checkboxData']->value['oa_theme'];?>
" id="oa_theme" /></td>
        <td class="ow_desc"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_theme_desc"),$_smarty_tpl);?>
</td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><label for="oa_lang"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_lang_label"),$_smarty_tpl);?>
</label></td>
        <td class="ow_value"><input type="checkbox" name="mode[]" value="<?php echo $_smarty_tpl->tpl_vars['checkboxData']->value['oa_lang'];?>
" id="oa_lang" /></td>
        <td class="ow_desc"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_lang_desc"),$_smarty_tpl);?>
</td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><label for="oa_dbcache"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_dbcache_label"),$_smarty_tpl);?>
</label></td>
        <td class="ow_value"><input type="checkbox" name="mode[]" value="<?php echo $_smarty_tpl->tpl_vars['checkboxData']->value['oa_dbcache'];?>
" id="oa_dbcache" /></td>
        <td class="ow_desc"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_dbcache_desc"),$_smarty_tpl);?>
</td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><label for="oa_plugin"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_plugin_label"),$_smarty_tpl);?>
</label></td>
        <td class="ow_value"><input type="checkbox" name="mode[]" value="<?php echo $_smarty_tpl->tpl_vars['checkboxData']->value['oa_plugin'];?>
" id="oa_plugin" /></td>
        <td class="ow_desc"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_plugin_desc"),$_smarty_tpl);?>
</td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_label"><label for="oa_all"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_all_label"),$_smarty_tpl);?>
</label></td>
        <td class="ow_value"><input type="checkbox" name="oa_all" id="oa_all" /></td>
        <td class="ow_desc"><?php echo smarty_function_text(array('key'=>"oacompress+admin_cache_control_btn_all_desc"),$_smarty_tpl);?>
</td>
    </tr>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1, ow_alt2'),$_smarty_tpl);?>
">
        <td class="ow_value" colspan="3"><div class="ow_right"><?php echo smarty_function_decorator(array('name'=>"button",'type'=>"submit",'langLabel'=>"oacompress+clear_btn_label",'id'=>"oa_all"),$_smarty_tpl);?>
</div></td>
    </tr>
</table>
</form><?php }
}
