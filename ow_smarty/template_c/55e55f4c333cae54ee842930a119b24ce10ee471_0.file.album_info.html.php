<?php
/* Smarty version 3.1.29, created on 2019-04-30 22:02:05
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/photo/views/components/album_info.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc89bad171a75_96586170',
  'file_dependency' => 
  array (
    '55e55f4c333cae54ee842930a119b24ce10ee471' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/photo/views/components/album_info.html',
      1 => 1470288120,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc89bad171a75_96586170 ($_smarty_tpl) {
if (!is_callable('smarty_function_add_content')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.add_content.php';
?>


<div id="photo-album-info" class="ow_photo_album_info_wrap clearfix">
    <div class="ow_photo_album_info">
        <div class="ow_photo_album_cover ow_high1" style="background-image: url(<?php echo $_smarty_tpl->tpl_vars['coverUrl']->value;?>
)">
            <img src="<?php echo $_smarty_tpl->tpl_vars['coverUrlOrig']->value;?>
" alt="" class="cover_orig ow_hidden" />
        </div>
        <h3 class="ow_photo_album_name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</h3>
        <div class="ow_photo_album_description ow_stdmargin"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['album']->value->description, ENT_QUOTES, 'UTF-8', true);?>
</div>

        <?php echo smarty_function_add_content(array('key'=>'photo.view_album','album'=>$_smarty_tpl->tpl_vars['album']->value),$_smarty_tpl);?>

    </div>
</div>
<?php }
}
