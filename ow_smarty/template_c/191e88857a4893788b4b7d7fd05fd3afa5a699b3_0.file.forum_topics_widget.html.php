<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:17:45
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/forum/views/components/forum_topics_widget.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6eb69964dc0_42097386',
  'file_dependency' => 
  array (
    '191e88857a4893788b4b7d7fd05fd3afa5a699b3' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/forum/views/components/forum_topics_widget.html',
      1 => 1470288118,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6eb69964dc0_42097386 ($_smarty_tpl) {
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.url_for_route.php';
if (!is_callable('smarty_modifier_truncate')) require_once '/var/www/www-root/data/www/mymafia.su/ow_libraries/vendor/smarty/smarty/libs/plugins/modifier.truncate.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.decorator.php';
?>

<?php
$_from = $_smarty_tpl->tpl_vars['topics']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_topic_0_saved_item = isset($_smarty_tpl->tpl_vars['topic']) ? $_smarty_tpl->tpl_vars['topic'] : false;
$_smarty_tpl->tpl_vars['topic'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['topic']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['topic']->value) {
$_smarty_tpl->tpl_vars['topic']->_loop = true;
$__foreach_topic_0_saved_local_item = $_smarty_tpl->tpl_vars['topic'];
?>

	<?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'info_string', null); ob_start(); ?>
	    <a href="<?php echo $_smarty_tpl->tpl_vars['topic']->value['lastPost']['postUrl'];?>
"><?php echo $_smarty_tpl->tpl_vars['topic']->value['title'];?>
</a>
	<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	
	<?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'content', null); ob_start(); ?>
    	<?php $_smarty_tpl->tpl_vars["sectId"] = new Smarty_Variable($_smarty_tpl->tpl_vars['groups']->value[$_smarty_tpl->tpl_vars['topic']->value['groupId']]->sectionId, null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "sectId", 0);?>
		<div class="ow_smallmargin ow_small">
		    <a href="<?php echo smarty_function_url_for_route(array('for'=>"forum-default"),$_smarty_tpl);?>
#section-<?php echo $_smarty_tpl->tpl_vars['sectId']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['sections']->value[$_smarty_tpl->tpl_vars['sectId']->value]->name;?>
</a> &raquo; <a href="<?php echo smarty_function_url_for_route(array('for'=>"group-default:[groupId=>".((string)$_smarty_tpl->tpl_vars['topic']->value['groupId'])."]"),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->tpl_vars['groups']->value[$_smarty_tpl->tpl_vars['topic']->value['groupId']]->name;?>
</a>
		</div>
	    <div class="ow_smallmargin">
            <?php $_smarty_tpl->_cache['capture_stack'][] = array('default', 'length', null); ob_start();
echo preg_match_all('/[^\s]/u',$_smarty_tpl->tpl_vars['topic']->value['lastPost']['text'], $tmp);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['topic']->value['lastPost']['text'],$_smarty_tpl->tpl_vars['postLength']->value);?>
 <?php if ($_smarty_tpl->tpl_vars['length']->value > $_smarty_tpl->tpl_vars['postLength']->value) {?><a href="<?php echo $_smarty_tpl->tpl_vars['topic']->value['lastPost']['postUrl'];?>
" class="ow_lbutton"><?php echo smarty_function_text(array('key'=>'base+more'),$_smarty_tpl);?>
</a><?php }?>
	    </div>
	<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_cache['capture_stack']);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
$_smarty_tpl->_cache['__smarty_capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
	
	<?php echo smarty_function_decorator(array('name'=>'ipc','addClass'=>'ow_smallmargin','avatar'=>$_smarty_tpl->tpl_vars['avatars']->value[$_smarty_tpl->tpl_vars['topic']->value['lastPost']['userId']],'infoString'=>$_smarty_tpl->tpl_vars['info_string']->value,'content'=>$_smarty_tpl->tpl_vars['content']->value,'toolbar'=>$_smarty_tpl->tpl_vars['toolbars']->value[$_smarty_tpl->tpl_vars['topic']->value['lastPost']['postId']]),$_smarty_tpl);?>

	
<?php
$_smarty_tpl->tpl_vars['topic'] = $__foreach_topic_0_saved_local_item;
}
if (!$_smarty_tpl->tpl_vars['topic']->_loop) {
?>
    <div class="ow_nocontent"><?php echo smarty_function_text(array('key'=>'forum+no_topics'),$_smarty_tpl);?>
, <a href="<?php echo smarty_function_url_for_route(array('for'=>'add-topic-default'),$_smarty_tpl);?>
"><?php echo smarty_function_text(array('key'=>'forum+add_new'),$_smarty_tpl);?>
</a></div>
<?php
}
if ($__foreach_topic_0_saved_item) {
$_smarty_tpl->tpl_vars['topic'] = $__foreach_topic_0_saved_item;
}
}
}
