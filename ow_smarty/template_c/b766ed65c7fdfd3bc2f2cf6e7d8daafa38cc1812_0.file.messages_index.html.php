<?php
/* Smarty version 3.1.29, created on 2019-05-06 17:39:53
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/mailbox/views/controllers/messages_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cd0473953ecc8_69371026',
  'file_dependency' => 
  array (
    'b766ed65c7fdfd3bc2f2cf6e7d8daafa38cc1812' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/mailbox/views/controllers/messages_index.html',
      1 => 1470288118,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cd0473953ecc8_69371026 ($_smarty_tpl) {
if (!is_callable('smarty_function_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.decorator.php';
if ($_smarty_tpl->tpl_vars['mailModeEnabled']->value && $_smarty_tpl->tpl_vars['isAuthorizedSendMessage']->value) {?>
<div class="ow_content_menu_wrap clearfix">
    <div class="ow_right">
        <?php echo smarty_function_decorator(array('name'=>"button",'type'=>"button",'class'=>"ow_ic_add",'id'=>"newMessageBtn",'langLabel'=>'mailbox+label_btn_new_message'),$_smarty_tpl);?>

    </div>
</div>
<?php }?>
<div class="ow_mailbox_table ow_alt1 clearfix <?php if (!$_smarty_tpl->tpl_vars['mailModeEnabled']->value || !$_smarty_tpl->tpl_vars['chatModeEnabled']->value) {?>ow_mailbox_table_single<?php }?>" id="messagesContainerControl">
    <?php echo $_smarty_tpl->tpl_vars['conversationList']->value;?>

    <?php echo $_smarty_tpl->tpl_vars['conversationContainer']->value;?>

</div><?php }
}
