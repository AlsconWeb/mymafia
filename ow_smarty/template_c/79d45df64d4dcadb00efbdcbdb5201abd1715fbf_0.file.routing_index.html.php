<?php
/* Smarty version 3.1.29, created on 2019-05-19 23:29:40
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/routing_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ce1bcb500eb33_10946707',
  'file_dependency' => 
  array (
    '79d45df64d4dcadb00efbdcbdb5201abd1715fbf' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/routing_index.html',
      1 => 1545767780,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ce1bcb500eb33_10946707 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
?>
<div class="page_description">
    <?php echo smarty_function_text(array('key'=>"skeleton+description_routing"),$_smarty_tpl);?>

</div>


<div style="margin-bottom: 10px;">
    <?php echo smarty_function_text(array('key'=>'skeleton+routing_adding_route'),$_smarty_tpl);?>

</div>

<div class="ow_std_margin code_reference">
    <pre>
    OW::getRouter()->addRoute(new OW_Route('routing-example', 'example/:some_param', 'SKELETON_CTRL_Example', 'someaction'));
</pre>
</div>

<div style="margin-bottom: 10px;">
    <?php echo smarty_function_text(array('key'=>'skeleton+routing_adding_route_desc'),$_smarty_tpl);?>


</div>


<div style="margin-bottom: 10px;">
    <?php echo smarty_function_text(array('key'=>'skeleton+routing_classname'),$_smarty_tpl);?>

</div>

<div class="ow_std_margin code_reference">
<pre>
class PLUGINNAME_CTRL_Example extends OW_ActionController
{
    public function someaction($params)
    {
        //perform some actions and assign data to the view
        $this->assign('some_param', $params['some_param']);
    }
}
</pre>
</div>

<div style="margin-bottom: 10px;">
    <?php echo smarty_function_text(array('key'=>'skeleton+routing_adding_view'),$_smarty_tpl);?>

</div>

<div style="margin-bottom: 10px;">
    <?php echo smarty_function_text(array('key'=>'skeleton+routing_adding_menu_item'),$_smarty_tpl);?>

</div>

<div class="ow_std_margin code_reference">
<pre>
    OW::getNavigation()->addMenuItem(OW_Navigation::MAIN, 'routing-example', 'skeleton', 'routing_example_menu_item', OW_Navigation::VISIBLE_FOR_ALL);</pre>
</div>

<div style="margin-bottom: 10px;">
    <?php echo smarty_function_text(array('key'=>'skeleton+routing_adding_menu_item_desc'),$_smarty_tpl);?>


</div>

<div class="ow_std_margin code_reference">
<pre>
    OW::getNavigation()->deleteMenuItem('skeleton', 'routing_example_menu_item');
</div>

<?php }
}
