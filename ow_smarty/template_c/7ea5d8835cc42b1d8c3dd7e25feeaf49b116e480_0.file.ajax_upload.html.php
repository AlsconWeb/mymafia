<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:19:15
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/photo/views/components/ajax_upload.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6ebc3ec6f94_29263935',
  'file_dependency' => 
  array (
    '7ea5d8835cc42b1d8c3dd7e25feeaf49b116e480' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/photo/views/components/ajax_upload.html',
      1 => 1470288120,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6ebc3ec6f94_29263935 ($_smarty_tpl) {
if (!is_callable('smarty_function_url_for_route')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.url_for_route.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_add_content')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.add_content.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.submit.php';
?>


<div class="ow_photo_upload_wrap" id="add-new-photo-container">
    <div class="ow_hidden">
        <iframe name="iframe_upload" id="iframe_upload" src="about:blank"></iframe>
        <form id="upload-form" target="iframe_upload" enctype="multipart/form-data" method="post" action="<?php echo smarty_function_url_for_route(array('for'=>'photo.ajax_upload'),$_smarty_tpl);?>
">
            <input type="file" name="file" accept="image/jpeg,image/png,image/gif" multiple />
        </form>
        <div id="slot-prototype" class="ow_photo_preview_edit">
            <div class="ow_photo_preview_action">
                <div class="ow_photo_preview_image ow_photo_preview_loading">
                    <div class="ow_photo_preview_image_filter"></div>
                </div>
                <div class="ow_photo_preview_x"></div>
                <div class="ow_photo_preview_rotate"></div>
            </div>
            <div class="ow_photo_upload_description" style="min-height: 58px">
                <textarea class="ow_hidden invitation"></textarea>
            </div>
        </div>
    </div>

    <div class="ow_photo_dragndrop">
        <div id="drop-area" ondragover="return false;"></div>
        <span id="drop-area-label"><?php echo smarty_function_text(array('key'=>"photo+dnd_support"),$_smarty_tpl);?>
</span>
    </div>

    <?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>"ajax-upload")); $_block_repeat=true; echo smarty_block_form(array('name'=>"ajax-upload"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

        <div id="slot-area" class="ow_photo_preview_block_wrap clearfix"></div>

        <div id="photo-album-form" class="ow_photo_upload_bottom clearfix">
            <div id="photo-album-list" class="ow_left">
                <div class="ow_suggest_field ow_smallmargin">
                    <?php echo smarty_function_input(array('name'=>'album'),$_smarty_tpl);?>

                    <?php echo $_smarty_tpl->tpl_vars['albumNames']->value;?>

                    <?php echo smarty_function_error(array('name'=>'album-name'),$_smarty_tpl);?>

                </div>
                <div class="new-album ow_smallmargin" style="display: none">
                    <?php echo smarty_function_input(array('name'=>"album-name",'class'=>'ow_smallmargin'),$_smarty_tpl);?>

                    <?php echo smarty_function_input(array('name'=>"description"),$_smarty_tpl);?>

                </div>
                <?php
$_from = $_smarty_tpl->tpl_vars['extendInputs']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_input_0_saved_item = isset($_smarty_tpl->tpl_vars['input']) ? $_smarty_tpl->tpl_vars['input'] : false;
$_smarty_tpl->tpl_vars['input'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['input']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['input']->value) {
$_smarty_tpl->tpl_vars['input']->_loop = true;
$__foreach_input_0_saved_local_item = $_smarty_tpl->tpl_vars['input'];
?>
                    <div id="<?php echo $_smarty_tpl->tpl_vars['input']->value;?>
" style="margin-bottom: 8px;">
                        <div class="ow_smallmargin">
                            <?php echo smarty_function_input(array('name'=>$_smarty_tpl->tpl_vars['input']->value),$_smarty_tpl);
echo smarty_function_label(array('name'=>$_smarty_tpl->tpl_vars['input']->value),$_smarty_tpl);?>
<br /><?php echo smarty_function_error(array('name'=>$_smarty_tpl->tpl_vars['input']->value),$_smarty_tpl);?>

                        </div>
                    </div>
                <?php
$_smarty_tpl->tpl_vars['input'] = $__foreach_input_0_saved_local_item;
}
if ($__foreach_input_0_saved_item) {
$_smarty_tpl->tpl_vars['input'] = $__foreach_input_0_saved_item;
}
?>
            </div>

            <?php echo smarty_function_add_content(array('key'=>"photo.onRenderAjaxUpload",'albumId'=>$_smarty_tpl->tpl_vars['albumId']->value,'userId'=>$_smarty_tpl->tpl_vars['userId']->value),$_smarty_tpl);?>


            <div class="ow_photo_upload_submit ow_right">
                <span class="ow_button">
                    <span class=" ow_ic_submit ow_positive">
                        <?php echo smarty_function_submit(array('name'=>"submit"),$_smarty_tpl);?>

                    </span>
                </span>
            </div>
        </div>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"ajax-upload"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

</div>
<?php }
}
