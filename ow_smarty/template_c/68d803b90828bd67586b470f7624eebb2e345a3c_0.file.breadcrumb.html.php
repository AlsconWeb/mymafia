<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:17:35
  from "/var/www/www-root/data/www/mymafia.su/ow_system_plugins/base/views/components/breadcrumb.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6eb5fa70350_12275972',
  'file_dependency' => 
  array (
    '68d803b90828bd67586b470f7624eebb2e345a3c' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_system_plugins/base/views/components/breadcrumb.html',
      1 => 1549280214,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6eb5fa70350_12275972 ($_smarty_tpl) {
if (!is_callable('smarty_block_block_decorator')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.block_decorator.php';
?>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('block_decorator', array('name'=>'box','type'=>'empty','addClass'=>'ow_breadcrumb ow_stdmargin clearfix')); $_block_repeat=true; echo smarty_block_block_decorator(array('name'=>'box','type'=>'empty','addClass'=>'ow_breadcrumb ow_stdmargin clearfix'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

	
	<?php if ($_smarty_tpl->tpl_vars['title']->value != '') {?><span class="ow_breadcrumb_title"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</span><?php }?>
	
	<?php
$_from = $_smarty_tpl->tpl_vars['items']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_c_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_foreach_c']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_c'] : false;
$__foreach_c_0_saved_item = isset($_smarty_tpl->tpl_vars['crumb']) ? $_smarty_tpl->tpl_vars['crumb'] : false;
$__foreach_c_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
$_smarty_tpl->tpl_vars['crumb'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['__smarty_foreach_c'] = new Smarty_Variable(array());
$__foreach_c_0_iteration=0;
$_smarty_tpl->tpl_vars['crumb']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['crumb']->value) {
$_smarty_tpl->tpl_vars['crumb']->_loop = true;
$__foreach_c_0_iteration++;
$_smarty_tpl->tpl_vars['__smarty_foreach_c']->value['last'] = $__foreach_c_0_iteration == $__foreach_c_0_total;
$__foreach_c_0_saved_local_item = $_smarty_tpl->tpl_vars['crumb'];
?>
        <span class="ow_breadcrumb_item<?php if (!empty($_smarty_tpl->tpl_vars['crumb']->value['class'])) {?> <?php echo $_smarty_tpl->tpl_vars['crumb']->value['class'];
}?>">
        <?php if (!empty($_smarty_tpl->tpl_vars['crumb']->value['href'])) {?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['crumb']->value['href'];?>
"><?php echo $_smarty_tpl->tpl_vars['crumb']->value['label'];?>
</a>
        <?php } else { ?>
            <?php echo $_smarty_tpl->tpl_vars['crumb']->value['label'];?>

        <?php }?>
        </span>
        <?php if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_c']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_c']->value['last'] : null)) {?>&raquo;<?php }?>
	<?php
$_smarty_tpl->tpl_vars['crumb'] = $__foreach_c_0_saved_local_item;
}
if ($__foreach_c_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_foreach_c'] = $__foreach_c_0_saved;
}
if ($__foreach_c_0_saved_item) {
$_smarty_tpl->tpl_vars['crumb'] = $__foreach_c_0_saved_item;
}
?>
	
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_block_decorator(array('name'=>'box','type'=>'empty','addClass'=>'ow_breadcrumb ow_stdmargin clearfix'), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);
}
}
