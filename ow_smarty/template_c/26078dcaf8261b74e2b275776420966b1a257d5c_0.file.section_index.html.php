<?php
/* Smarty version 3.1.29, created on 2019-04-30 21:43:31
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/forum/views/controllers/section_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc89753691b90_12156693',
  'file_dependency' => 
  array (
    '26078dcaf8261b74e2b275776420966b1a257d5c' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/forum/views/controllers/section_index.html',
      1 => 1470288118,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc89753691b90_12156693 ($_smarty_tpl) {
if (!is_callable('smarty_function_add_content')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.add_content.php';
if (!is_callable('smarty_block_style')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.style.php';
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/www-root/data/www/mymafia.su/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_function_user_link')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.user_link.php';
if (!is_callable('smarty_function_format_date')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.format_date.php';
?>
<div class="clearfix"><?php echo smarty_function_add_content(array('key'=>'forum.add_content.list.top','listType'=>'section'),$_smarty_tpl);?>
</div>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('style', array()); $_block_repeat=true; echo smarty_block_style(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

 .ow_author { width: 25%; } 
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_style(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>


<?php echo $_smarty_tpl->tpl_vars['breadcrumb']->value;?>

<div class="clearfix">
    <div class="ow_right ow_smallmargin">
        <?php echo $_smarty_tpl->tpl_vars['search']->value;?>

    </div>
</div>

<table class="ow_table_1 ow_stdmargin ow_forum">
    <?php
$_from = $_smarty_tpl->tpl_vars['sectionGroupList']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_section_0_saved_item = isset($_smarty_tpl->tpl_vars['section']) ? $_smarty_tpl->tpl_vars['section'] : false;
$_smarty_tpl->tpl_vars['section'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['section']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['section']->value) {
$_smarty_tpl->tpl_vars['section']->_loop = true;
$__foreach_section_0_saved_local_item = $_smarty_tpl->tpl_vars['section'];
?>
    <tr class="ow_tr_first">
        <th class="ow_name"><span id="section-<?php echo $_smarty_tpl->tpl_vars['section']->value['sectionId'];?>
"><?php echo $_smarty_tpl->tpl_vars['section']->value['sectionName'];?>
</span></th>
        <th class="ow_topics"><?php echo smarty_function_text(array('key'=>'forum+topics'),$_smarty_tpl);?>
</th>
        <th class="ow_replies"><?php echo smarty_function_text(array('key'=>'forum+replies'),$_smarty_tpl);?>
</th>
        <th class="ow_author"><?php echo smarty_function_text(array('key'=>'forum+last_reply'),$_smarty_tpl);?>
</th>
    </tr>

    <?php
$_from = $_smarty_tpl->tpl_vars['section']->value['groups'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_g_1_saved = isset($_smarty_tpl->tpl_vars['__smarty_foreach_g']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_g'] : false;
$__foreach_g_1_saved_item = isset($_smarty_tpl->tpl_vars['group']) ? $_smarty_tpl->tpl_vars['group'] : false;
$__foreach_g_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
$_smarty_tpl->tpl_vars['group'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['__smarty_foreach_g'] = new Smarty_Variable(array());
$__foreach_g_1_first = true;
$__foreach_g_1_iteration=0;
$_smarty_tpl->tpl_vars['group']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['group']->value) {
$_smarty_tpl->tpl_vars['group']->_loop = true;
$__foreach_g_1_iteration++;
$_smarty_tpl->tpl_vars['__smarty_foreach_g']->value['first'] = $__foreach_g_1_first;
$_smarty_tpl->tpl_vars['__smarty_foreach_g']->value['last'] = $__foreach_g_1_iteration == $__foreach_g_1_total;
$__foreach_g_1_first = false;
$__foreach_g_1_saved_local_item = $_smarty_tpl->tpl_vars['group'];
?>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt1,ow_alt2','reset'=>(isset($_smarty_tpl->tpl_vars['__smarty_foreach_g']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_g']->value['first'] : null)),$_smarty_tpl);?>
 <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_g']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_g']->value['last'] : null)) {?>ow_tr_last<?php }?>">
        <td class="ow_name">
            <a href="<?php echo $_smarty_tpl->tpl_vars['group']->value['groupUrl'];?>
"><b><?php echo $_smarty_tpl->tpl_vars['group']->value['name'];?>
</b></a>
            <div class="ow_small"><?php echo $_smarty_tpl->tpl_vars['group']->value['description'];?>
</div>
            <?php if ($_smarty_tpl->tpl_vars['group']->value['isPrivate']) {?>
            <span class="ow_lbutton ow_green"><?php echo smarty_function_text(array('key'=>'forum+is_private'),$_smarty_tpl);?>
</span>
            <span class="ow_small ow_remark"><?php echo smarty_function_text(array('key'=>'forum+visible_to'),$_smarty_tpl);?>
: <?php
$_from = $_smarty_tpl->tpl_vars['group']->value['roles'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_r_2_saved = isset($_smarty_tpl->tpl_vars['__smarty_foreach_r']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_r'] : false;
$__foreach_r_2_saved_item = isset($_smarty_tpl->tpl_vars['r']) ? $_smarty_tpl->tpl_vars['r'] : false;
$__foreach_r_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
$_smarty_tpl->tpl_vars['r'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['__smarty_foreach_r'] = new Smarty_Variable(array());
$__foreach_r_2_iteration=0;
$_smarty_tpl->tpl_vars['r']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['r']->value) {
$_smarty_tpl->tpl_vars['r']->_loop = true;
$__foreach_r_2_iteration++;
$_smarty_tpl->tpl_vars['__smarty_foreach_r']->value['last'] = $__foreach_r_2_iteration == $__foreach_r_2_total;
$__foreach_r_2_saved_local_item = $_smarty_tpl->tpl_vars['r'];
echo $_smarty_tpl->tpl_vars['r']->value;
if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_r']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_r']->value['last'] : null)) {?>, <?php }
$_smarty_tpl->tpl_vars['r'] = $__foreach_r_2_saved_local_item;
}
if ($__foreach_r_2_saved) {
$_smarty_tpl->tpl_vars['__smarty_foreach_r'] = $__foreach_r_2_saved;
}
if ($__foreach_r_2_saved_item) {
$_smarty_tpl->tpl_vars['r'] = $__foreach_r_2_saved_item;
}
?></span>
            <?php }?>
        </td>
        <td class="ow_topics"><?php echo $_smarty_tpl->tpl_vars['group']->value['topicCount'];?>
</td>
        <td class="ow_replies"><?php echo $_smarty_tpl->tpl_vars['group']->value['replyCount'];?>
</td>
        <td class="ow_txtleft ow_small">
            <?php if (!empty($_smarty_tpl->tpl_vars['group']->value['lastReply'])) {?>
            <div class="ow_reply_info">
	            <span class="ow_nowrap ow_icon_control ow_ic_user">
	                <?php echo smarty_function_user_link(array('username'=>$_smarty_tpl->tpl_vars['userNames']->value[$_smarty_tpl->tpl_vars['group']->value['lastReply']['userId']],'name'=>$_smarty_tpl->tpl_vars['displayNames']->value[$_smarty_tpl->tpl_vars['group']->value['lastReply']['userId']]),$_smarty_tpl);?>

	            </span> &middot;
                <span class="ow_nowrap ow_remark"><?php echo smarty_function_format_date(array('timestamp'=>$_smarty_tpl->tpl_vars['group']->value['lastReply']['createStamp']),$_smarty_tpl);?>
</span>
            </div>
            <?php echo smarty_function_text(array('key'=>'forum+in'),$_smarty_tpl);?>
 <a href="<?php echo $_smarty_tpl->tpl_vars['group']->value['lastReply']['postUrl'];?>
"><?php echo $_smarty_tpl->tpl_vars['group']->value['lastReply']['title'];?>
</a>
            <?php }?>
        </td>
    </tr>
    <?php
$_smarty_tpl->tpl_vars['group'] = $__foreach_g_1_saved_local_item;
}
if ($__foreach_g_1_saved) {
$_smarty_tpl->tpl_vars['__smarty_foreach_g'] = $__foreach_g_1_saved;
}
if ($__foreach_g_1_saved_item) {
$_smarty_tpl->tpl_vars['group'] = $__foreach_g_1_saved_item;
}
?>

    <?php
$_smarty_tpl->tpl_vars['section'] = $__foreach_section_0_saved_local_item;
}
if ($__foreach_section_0_saved_item) {
$_smarty_tpl->tpl_vars['section'] = $__foreach_section_0_saved_item;
}
?>
</table><?php }
}
