<?php
/* Smarty version 3.1.29, created on 2019-05-20 05:23:40
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/localization_index.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5ce20facb18fc9_51883537',
  'file_dependency' => 
  array (
    '7bf1b6829f6f92621928907c3432b3e3e9118397' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/skeleton/views/controllers/localization_index.html',
      1 => 1545767778,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ce20facb18fc9_51883537 ($_smarty_tpl) {
if (!is_callable('smarty_function_text')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.text.php';
if (!is_callable('smarty_block_form')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/block.form.php';
if (!is_callable('smarty_function_label')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.label.php';
if (!is_callable('smarty_function_input')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.input.php';
if (!is_callable('smarty_function_error')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.error.php';
if (!is_callable('smarty_function_submit')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/function.submit.php';
if (!is_callable('smarty_function_cycle')) require_once '/var/www/www-root/data/www/mymafia.su/ow_libraries/vendor/smarty/smarty/libs/plugins/function.cycle.php';
?>
<div class="page_description">
    <?php echo smarty_function_text(array('key'=>"skeleton+description_localization"),$_smarty_tpl);?>


    <div class="example_file"><?php echo smarty_function_text(array('key'=>"skeleton+example_file"),$_smarty_tpl);?>
 /ow_plugins/skeleton/controllers/localization.php</div>
</div>

<?php $_smarty_tpl->smarty->_cache['tag_stack'][] = array('form', array('name'=>"LocalizationExampleForm")); $_block_repeat=true; echo smarty_block_form(array('name'=>"LocalizationExampleForm"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

<table class="ow_table_1 ow_form ow_stdmargin">
    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'key'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo smarty_function_input(array('name'=>'key'),$_smarty_tpl);
echo smarty_function_error(array('name'=>'key'),$_smarty_tpl);?>
</td>
        <td class="ow_desc ow_small"></td>
    </tr>

    <tr class="ow_alt2 ow_tr_first">
        <td class="ow_label"><?php echo smarty_function_label(array('name'=>'value'),$_smarty_tpl);?>
</td>
        <td class="ow_value"><?php echo smarty_function_input(array('name'=>'value'),$_smarty_tpl);
echo smarty_function_error(array('name'=>'value'),$_smarty_tpl);?>
</td>
        <td class="ow_desc ow_small"></td>
    </tr>

</table>
<div class="clearfix ow_submit ow_stdmargin">
    <div class="ow_right"><?php echo smarty_function_submit(array('name'=>'create','class'=>'ow_ic_add ow_positive'),$_smarty_tpl);?>
</div>
</div>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_form(array('name'=>"LocalizationExampleForm"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_cache['tag_stack']);?>

<div class="ow_center ow_stdmargin"><h2><?php echo smarty_function_text(array('key'=>'skeleton+created_text_keys'),$_smarty_tpl);?>
</h2></div>
<table class="ow_table_1 ow_form ow_stdmargin">
    <tr>
        <th><?php echo smarty_function_text(array('key'=>'skeleton+text_key_values'),$_smarty_tpl);?>
</th>
        <th><?php echo smarty_function_text(array('key'=>'skeleton+original_values'),$_smarty_tpl);?>
</th>
        <th><?php echo smarty_function_text(array('key'=>'skeleton+actions'),$_smarty_tpl);?>
</th>
    </tr>
    <?php
$_from = $_smarty_tpl->tpl_vars['keys']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_key_0_saved_item = isset($_smarty_tpl->tpl_vars['key']) ? $_smarty_tpl->tpl_vars['key'] : false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['key']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value) {
$_smarty_tpl->tpl_vars['key']->_loop = true;
$__foreach_key_0_saved_local_item = $_smarty_tpl->tpl_vars['key'];
?>
    <tr class="<?php echo smarty_function_cycle(array('values'=>'ow_alt2, ow_alt1'),$_smarty_tpl);?>
">
        <td class="ow_center">{text key='skeleton+<?php echo $_smarty_tpl->tpl_vars['key']->value['key'];?>
'}</td>
        <td class="ow_center"><?php echo smarty_function_text(array('key'=>"skeleton+".((string)$_smarty_tpl->tpl_vars['key']->value['key'])),$_smarty_tpl);?>
</td>
        <td class="ow_center"><a class="ow_lbutton ow_red" href="<?php echo $_smarty_tpl->tpl_vars['key']->value['deleteUrl'];?>
" onclick="if (confirm('<?php echo smarty_function_text(array('key'=>"base+are_you_sure"),$_smarty_tpl);?>
')) return true; else return false;" ><?php echo smarty_function_text(array('key'=>'base+delete'),$_smarty_tpl);?>
</a></td>
    </tr>
    <?php
$_smarty_tpl->tpl_vars['key'] = $__foreach_key_0_saved_local_item;
}
if ($__foreach_key_0_saved_item) {
$_smarty_tpl->tpl_vars['key'] = $__foreach_key_0_saved_item;
}
?>
</table><?php }
}
