<?php
/* Smarty version 3.1.29, created on 2019-04-29 15:17:45
  from "/var/www/www-root/data/www/mymafia.su/ow_plugins/newsfeed/views/formats/image.html" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5cc6eb69b8e2b4_69740306',
  'file_dependency' => 
  array (
    '049f5e16100835b14724d1ddce3dc2801fd3ecc8' => 
    array (
      0 => '/var/www/www-root/data/www/mymafia.su/ow_plugins/newsfeed/views/formats/image.html',
      1 => 1470288118,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc6eb69b8e2b4_69740306 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_more')) require_once '/var/www/www-root/data/www/mymafia.su/ow_smarty/plugin/modifier.more.php';
if (!empty($_smarty_tpl->tpl_vars['vars']->value['status'])) {?><div class="ow_newsfeed_body_status ow_smallmargin"><?php echo smarty_modifier_more($_smarty_tpl->tpl_vars['vars']->value['status'],300);?>
</div><?php }?>

<div class="ow_newsfeed_large_image clearfix">
    <div class="ow_newsfeed_item_picture">
        <a <?php if (empty($_smarty_tpl->tpl_vars['vars']->value['url'])) {?>href="<?php echo $_smarty_tpl->tpl_vars['vars']->value['image'];?>
" onclick="OW.showImageInFloatBox(this.href); return false;"<?php } else { ?>href="<?php echo $_smarty_tpl->tpl_vars['vars']->value['url'];?>
"<?php }?>>
            <img alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vars']->value['title'], ENT_QUOTES, 'UTF-8', true);?>
" src="<?php echo $_smarty_tpl->tpl_vars['vars']->value['image'];?>
"<?php if (!empty($_smarty_tpl->tpl_vars['vars']->value['class'])) {?> class="<?php echo $_smarty_tpl->tpl_vars['vars']->value['class'];?>
"<?php }?>></a>
    </div>
</div><?php }
}
