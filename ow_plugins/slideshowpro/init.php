<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


OW::getRouter()->addRoute(new OW_Route('slideshowpro.index', 'slideshowpro', "SLIDESHOWPRO_CTRL_Slideshowpro", 'index'));
//OW::getRouter()->addRoute(new OW_Route('slideshowpro.admin', 'admin/plugins/slideshowpro', "SLIDESHOWPRO_CTRL_Admin", 'dept'));
OW::getRouter()->addRoute(new OW_Route('slideshowpro.admin', 'admin/plugins/slideshowpro', "SLIDESHOWPRO_CTRL_Admin", 'dept'));

$config = OW::getConfig();
if ( !$config->configExists('slideshowpro', 'slideshowpro_enable') ){
    $config->addConfig('slideshowpro', 'slideshowpro_enable', '1', '');
}
if ( !$config->configExists('slideshowpro', 'maxitems') ){
    $config->addConfig('slideshowpro', 'maxitems', '10', '');
}
if ( !$config->configExists('slideshowpro', 'width_slider') ){
    $config->addConfig('slideshowpro', 'width_slider', '700', '');
}
if ( !$config->configExists('slideshowpro', 'height_slider') ){
    $config->addConfig('slideshowpro', 'height_slider', '450', '');
}
if ( !$config->configExists('slideshowpro', 'transition_speed') ){
    $config->addConfig('slideshowpro', 'transition_speed', '600', '');
}
if ( !$config->configExists('slideshowpro', 'display_time') ){
    $config->addConfig('slideshowpro', 'display_time', '6000', '');
}

/*
$config = OW::getConfig();
if ( !$config->configExists('slideshowpro', 'toclock') ){
    $config->addConfig('slideshowpro', 'toclock', '1', '');
}
if ( !$config->configExists('slideshowpro', 'tonews') ){
    $config->addConfig('slideshowpro', 'tonews', '1', '');
}
*/

//OW::getRouter()->addRoute(new OW_Route('pay24.dopay', 'pay24/:option/:iditem', "PAY24_CTRL_Dopay", 'index'));
//OW::getRouter()->addRoute(new OW_Route('pay24.back', 'pay24/back', "PAY24_CTRL_Dopay", 'index'));;
//OW::getRouter()->addRoute(new OW_Route('slideshowpro.payresult', 'slideshowpro/:com', "SLIDESHOWPRO_CTRL_Slideshowpro", 'index'));;

/*
function setrestriction_tick(){
    if (OW::getConfig()->getValue('slideshowpro', 'slideshowpro_enable')=="1"){
        SLIDESHOWPRO_BOL_Service::getInstance()->minix();
    }
}

if (!OW::getRequest()->isPost() AND !OW::getRequest()->isAjax() AND 
    strpos($_SERVER["REQUEST_URI"],"base/media-panel")===false AND 
    strpos($_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"],"ow_cron")===false AND 
    SLIDESHOWPRO_BOL_Service::getInstance()->check_url_allow()==true
    ){

    OW::getEventManager()->bind('core.finalize', 'setrestriction_tick');
//echo "--".OW::getRequest()->isPost();exit;
//    OW::getEventManager()->bind('core.app_init', 'setrestriction_tick');
}
*/


/*
function contactus_handler_after_install( BASE_CLASS_EventCollector $event )
{
    if ( count(CONTACTUS_BOL_Service::getInstance()->getDepartmentList()) < 1 )
    {
        $url = OW::getRouter()->urlForRoute('contactus.admin');
        $event->add(OW::getLanguage()->text('contactus', 'after_install_notification', array('url' => $url)));
    }
}

OW::getEventManager()->bind('admin.add_admin_notification', 'contactus_handler_after_install');
*/

/*
function slideshowpro_ads_enabled( BASE_EventCollector $event )
{
    $event->add('slideshowpro');
}
OW::getEventManager()->bind('ads.enabled_plugins', 'slideshowpro_ads_enabled');
OW::getRequestHandler()->addCatchAllRequestsExclude('base.suspended_user', 'SLIDESHOWPRO_CTRL_Slideshowpro');
*/
