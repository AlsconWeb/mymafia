<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://test.a6.pl
 * Full license available at: http://test.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


class SLIDESHOWPRO_CMP_IndexWidgeti extends BASE_CLASS_Widget
{

    public function __construct( BASE_CLASS_WidgetParameter $params )
    {
        parent::__construct();
/* 
        $params = $paramObject->customParamList;
 
        if ( !empty($params['content']) )
        {
            $this->content = $paramObject->customizeMode && !empty($_GET['disable-js']) ? UTIL_HtmlTag::stripJs($params['content']) : $params['content'];
        }
 
        if ( isset($params['nl_to_br']) )
        {
            $this->nl2br = (bool) $params['nl_to_br'];
        }
*/
    }

    public static function getSettingList()
    {

        $options = array();
        $settingList=array();
/*
        for ( $i = 3; $i <= 10; $i++ )
        {
            $options[$i] = $i;
        }

        $settingList['count'] = array(
            'presentation' => self::PRESENTATION_SELECT,
            'label' => OW::getLanguage()->text('shoppro', 'cmp_widget_post_count'),
            'optionList' => $options,
            'value' => 3,
        );
        $settingList['previewLength'] = array(
            'presentation' => self::PRESENTATION_TEXT,
            'label' => OW::getLanguage()->text('shoppro', 'blog_widget_preview_length_lbl'),
            'value' => 50,
        );
*/
        return $settingList;
    }

    public static function getStandardSettingValueList()
    {
        $list = array(
            self::SETTING_TITLE => OW::getLanguage()->text('slideshowpro', 'admin_dept_title'),
            self::SETTING_SHOW_TITLE => false,
                self::SETTING_WRAP_IN_BOX => true,
            self::SETTING_ICON => 'ow_ic_write'
        );

        return $list;
    }

    public static function getAccess()
    {
        return self::ACCESS_ALL;
    }
	
    public function onBeforeRender() // The standard method of the component that is called before rendering
    {
            $content="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
//                    $curent_url .= $_SERVER["SERVER_NAME"]."/";

        $curent_url=OW_URL_HOME; 

//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/slideshowpro/ext/jquery.movingboxes.min.js');
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/slideshowpro/ext/slidershop.js?'.rand(100,500));
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/slideshowpro/ext/slidershop.js?a=1');




        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/slideshowpro/jquery.infinitecarousel3.js');
//        OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/slideshowpro/w_clock/jquery.tzineClock.css');


//        progressRingColorOpacity: '0,153,51,.85',
//        progressRingColorOpacity: '0,0,0,.9',

$transition_speed=OW::getConfig()->getValue('slideshowpro', 'transition_speed');
if (!$transition_speed OR $transition_speed<1) $transition_speed=600;

$display_time=OW::getConfig()->getValue('slideshowpro', 'display_time');
if (!$display_time OR $display_time<1) $display_time=6000;



$script="
<script>
$(document).ready(function() {

    $('#carousel').infiniteCarousel({
        progressRingColorOpacity: '0,0,0,.9',
        autoPilot: true,
//        easeLeft: 'easeOutExpo',
//        easeRight:'easeOutQuart',
        imagePath: '".OW_URL_HOME."ow_static/plugins/slideshowpro/images/',

    transitionSpeed:".$transition_speed.",
    displayTime: ".$display_time.",
        internalThumbnails: false,
//        thumbnailType: 'images',
//        customClass: 'myCarousel',

        progressRingBackgroundOn: true,
        inView: 1,
        advance: 1,
        prevNextInternal: true,
        autoHideCaptions: true,
        margin: 'auto'

    });





    $('#carousel .car_other').show();
});

</script>
";
//$content.=$script;
//OW::getDocument()->addOnloadScript($script);
$widthsli=OW::getConfig()->getValue('slideshowpro', 'width_slider');
if (!$widthsli OR $widthsli<1) $widthsli=750;

$heightsli=OW::getConfig()->getValue('slideshowpro', 'height_slider');
if (!$heightsli OR $heightsli<1) $heightsli=450;

$items_each=OW::getConfig()->getValue('slideshowpro', 'maxitems');
if (!$items_each OR $items_each<1) $items_each=10;
//echo $items_each;exit;

$items=SLIDESHOWPRO_BOL_Service::getInstance()->generate_list("all",$widthsli,$heightsli,$items_each);



                if ($items){
                $content.="<ul id=\"carousel\" style=\"\" class=\"ow_center\">";

                $content.=$items;

/*
        $content.="<li><img width=\"".$widthsli."px\" height=\"".$heightsli."px alt=\"\" src=\"http://0.s3.envato.com/files/1967560/megamenupreview.1.1.2.jpg\" /><p>This is <strong>a caption</strong></p></li>

        <li class=\"car_other\" style=\"display:none;\"><a href=\"http://www.google.com\"><img width=\"".$widthsli."px height=\"".$heightsli."px alt=\"\" src=\"http://2.s3.envato.com/files/5247620/01-styles-with-shortcodes-1-7-4-preview.jpg\" /></a></li>
        <li class=\"car_other\" style=\"display:none;\"><a href=\"http://www.google.com\"><img width=\"".$widthsli."px height=\"".$heightsli."px alt=\"\" src=\"http://www.1stwebdesigner.com/wp-content/uploads/2009/09/wordpress-jquery-plugins/howto-carousel-wordpress-jquery-plugin.jpg\" /></a></li>

<li class=\"car_other\" style=\"display:none;\"><iframe longdesc=\"http://img.youtube.com/vi/QQh56geU0X8/2.jpg\" width=\"640\" height=\"360\" src=\"http://www.youtube.com/embed/QQh56geU0X8?rel=0&amp;hd=1\" wmode=\"transparent\" frameborder=\"0\" allowfullscreen></iframe></li>

    <li class=\"car_other\" style=\"display:none;\"><iframe longdesc=\"http://img.youtube.com/vi/X_uyuuJVlw4/2.jpg\"  width=\"640\" height=\"360\" src=\"http://www.youtube.com/embed/X_uyuuJVlw4?rel=0\" frameborder=\"0\" allowfullscreen></iframe></li>
    <li class=\"car_other\" style=\"display:none;\"><iframe longdesc=\"http://img.youtube.com/vi/mopar3ddxOM/2.jpg\" width=\"640\" height=\"360\" src=\"http://www.youtube.com/embed/mopar3ddxOM?rel=0&amp;hd=1\" frameborder=\"0\" allowfullscreen></iframe></li>
    <li class=\"car_other\" style=\"display:none;\"><iframe longdesc=\"http://img.youtube.com/vi/qrO4YZeyl0I/2.jpg\" width=\"640\" height=\"360\" src=\"http://www.youtube.com/embed/qrO4YZeyl0I?rel=0\" frameborder=\"0\" allowfullscreen></iframe></li>
    <li class=\"car_other\" style=\"display:none;\"><iframe longdesc=\"http://b.vimeocdn.com/ts/145/026/145026168_200.jpg\" src=\"http://player.vimeo.com/video/22439234\" width=\"640\" height=\"360\" frameborder=\"0\" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></li>
    <li class=\"car_other\" style=\"display:none;\"><iframe src=\"http://www.funnyordie.com/embed/5ef1adb57b\" longdesc=\"video.png\" width=\"640\" height=\"360\" frameborder=\"0\"></iframe></li>";
*/
                $content.="</ul>";
                    $this->assign('content', $script.$content);
                }else{
		    $this->assign('content', $script);
                }
	}
	
}

