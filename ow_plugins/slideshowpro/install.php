<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


OW::getLanguage()->importPluginLangs(OW::getPluginManager()->getPlugin('slideshowpro')->getRootDir().'langs.zip', 'slideshowpro');
OW::getPluginManager()->addPluginSettingsRouteName('slideshowpro', 'slideshowpro.admin');

//BOL_LanguageService::getInstance()->addPrefix('contactus', 'Contact Us');

$config = OW::getConfig();
if ( !$config->configExists('slideshowpro', 'slideshowpro_enable') ){
    $config->addConfig('slideshowpro', 'slideshowpro_enable', '1', '');
}
if ( !$config->configExists('slideshowpro', 'maxitems') ){
    $config->addConfig('slideshowpro', 'maxitems', '10', '');
}
if ( !$config->configExists('slideshowpro', 'width_slider') ){
    $config->addConfig('slideshowpro', 'width_slider', '700', '');
}
if ( !$config->configExists('slideshowpro', 'height_slider') ){
    $config->addConfig('slideshowpro', 'height_slider', '450', '');
}
if ( !$config->configExists('slideshowpro', 'transition_speed') ){
    $config->addConfig('slideshowpro', 'transition_speed', '600', '');
}
if ( !$config->configExists('slideshowpro', 'display_time') ){
    $config->addConfig('slideshowpro', 'display_time', '6000', '');
}


/*
if ( !$config->configExists('slideshowpro', 'slideshowpro_protect_key') ){
    $key=substr(md5(date('d-m-Y H:i:s')),0,10);
    $config->addConfig('slideshowpro', 'slideshowpro_protect_key', $key, 'Protect Key');
}

if ( !$config->configExists('slideshowpro', 'slideshowpro_generateforusers') ){
    $config->addConfig('slideshowpro', 'slideshowpro_generateforusers', '1', 'Site Map for members too');
}

if ( !$config->configExists('slideshowpro', 'slideshowpro_max_items_eachplugin') ){
    $config->addConfig('slideshowpro', 'slideshowpro_max_items_eachplugin', '10', 'Max items for each plugins');
}
if ( !$config->configExists('slideshowpro', 'slideshowpro_max_items_sitemap') ){
    $config->addConfig('slideshowpro', 'slideshowpro_max_items_sitemap', '9999', 'Max items sitemap');
}


$sql = "CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "slideshowpro_department` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(200) NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE=MyISAM
ROW_FORMAT=DEFAULT";
OW::getDbo()->query($sql);
*/
