<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

//SLIDESHOWPRO_BOL_Service::getInstance()->generate_list();

class SLIDESHOWPRO_BOL_Service
{
    /**
     * Singleton instance.
     *
     * @var CONTACTUS_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return CONTACTUS_BOL_Service
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    private function __construct()
    {

    }

    public function getDepartmentLabel( $id )
    {
        return OW::getLanguage()->text('pay24', $this->getDepartmentKey($id));
    }

 public function html2txt($document){
    $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
    );
    $text = preg_replace($search, '', $document);
        $text=preg_replace("/(&?!amp;)/i", " ", $text);
        $text=preg_replace("/(&#\d+);/i", " ", $text); // For numeric entities
        $text=preg_replace("/(&\w+);/i", " ", $text); // For literal entities
    return $text;
 }

//    public function url_toparam ()
    public function check_url_allow ()
    {

//            return false;
/*
        if (OW::getRequest()->isPost()){
            return false;
        }else if (OW::getRequest()->isAjax()){
            return false;
        }
*/
/*
file_put_contents("TTTTTTTTTTTTTTT.txt", "\n==================================\n", FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", print_r($_GET,1), FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", print_r($_POST,1), FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", "\n==================================\n\n", FILE_APPEND );
*/
        $curent_url=OW_URL_HOME;
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) AND $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        $tmp=str_replace(OW_URL_HOME,"",$pageURL);
        if (substr($tmp,0,1)=="/") $tmp=substr($tmp,1);

        $test_furl=$tmp;
        $test_purl="";
        $test_aurl=array();

        $testx_tmp=explode("?",$test_furl);
        if (isset($testx_tmp[0])) $test_furl=$testx_tmp[0];
        if (isset($testx_tmp[1])) $test_purl=$testx_tmp[1];
        $test_aurl=explode("/",$test_furl);
//echo $test_furl;exit;
//            return false;
        if (isset($_POST['form_name']) AND $_POST['form_name']=="sign-in"){
            return false;
        }else if ( strpos($test_furl,"ajaxim")!==false){
            return false;
        }else if ( strpos($test_furl,"ow_cron")!==false){
            return false;
        }else if ( strpos($test_furl,"ow_static")!==false){
            return false;
        }else if ( strpos($test_furl,"ping")!==false){
            return false;
//        }else if (!OW::getUser()->isAuthenticated()){//for members only
//            return false;
        }else if ( strpos($test_furl,"base/ping/index")!==false OR strpos($test_furl,"base/ping")!==false){
            return false;
        }else{
            return true;
        }
    }

    public function generate_list_OLD($type="all",$iwidth="700",$iheight="333" )
    {
        $content="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;

        $is_first=true;


        if ($type=="all" OR $type=="photo"){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "photo 
            WHERE privacy='everybody' ORDER BY RAND() DESC LIMIT 2";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value )
            {
                    if ($is_first){
                        $st="";
                        $cl="";
                        $is_first=false;
                    }else{
                        $st="display:none;";
                        $cl="car_other";
                    }


//                if (isset($value['hash'])){
//                    $arr['photos']['photo'][$i]['url_m']=$curent_url."ow_userfiles/plugins/photo/photo_".$value['id']."_".$value['hash'].".jpg";
//                    $arr['photos']['photo'][$i]['url_s']=$curent_url."ow_userfiles/plugins/photo/photo_preview_".$value['id']."_".$value['hash'].".jpg";

                    if (isset($value['hash']) AND $value['hash']){
                        $imm=$curent_url."ow_userfiles/plugins/photo/photo_".$value['id']."_".$value['hash'].".jpg";
                    }else{
                        $imm=$curent_url."ow_userfiles/plugins/photo/photo_".$value['id'].".jpg";
                    }
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                    $content .="<a href=\"".$curent_url."video/".$value['id']."\"><img width=\"".$iwidth."\" height=\"".$iheight."\" src=\"".$imm."\"></a>";
                    if ($value['description']){
                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['description'])."</strong></a></p>";
                    }
                    $content .="</li>";                
            }
        }

        if ($type=="all" OR $type=="videopro"){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "videopro_link ddl
        LEFT JOIN " . OW_DB_PREFIX. "videopro_content ddd ON (ddd.id_link=ddl.id) 
            WHERE privacy='everybody' ORDER BY RAND() DESC LIMIT 2";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value )
            {

                    if ($is_first){
                        $st="";
                        $cl="";
                        $is_first=false;
                    }else{
                        $st="display:none;";
                        $cl="car_other";
                    }


//SLIDESHOWPRO_BOL_Service::getInstance()->generate_list()
                $matches="";
//            preg_match('<iframe.+?src="(.+?)".+?<\/iframe>', $value['code'], $matches);
                preg_match('/<embed.+?src="([^"]+)"/', stripslashes($value['embed']), $matches);
//            var_dump($matches);
//echo $matches[1];
//echo $value['code'];
//exit;
                if (isset($matches[1])){
//echo $matches[1];
                    $embed=str_replace("autoplay=1","",stripslashes($matches[1]));
                    $embed=str_replace("autoplay=true","",$embed);
                    $embed=str_replace("autoplay=","",$embed);
                    $embed=str_replace("autoplay =","",$embed);
                    $embed=str_replace("autoplay","",$embed);
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<iframe width=\"".$iwidth."\" height=\"".$iheight."\" src=\"".$embed."\"></iframe>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
//echo $content;exit;
                }else{
                    $embed=str_replace("autoplay=1","",stripslashes($value['embed']));
                    $embed=str_replace("autoplay=true","",$embed);
                    $embed=str_replace("autoplay=","",$embed);
                    $embed=str_replace("autoplay =","",$embed);
                    $embed=str_replace("autoplay","",$embed);
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<a width=\"".$iwidth."\" height=\"".$iheight."\" style=\"display:inline-block;\" href=\"".$curent_url."video/".$value['id']."\">".$embed."</a>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
                }
            }
        }

        if ($type=="all" OR $type=="video"){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "video_clip WHERE privacy='everybody' AND status='approved' ORDER BY RAND() DESC LIMIT 2";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value )
            {

                    if ($is_first){
                        $st="";
                        $cl="";
                        $is_first=false;
                    }else{
                        $st="display:none;";
                        $cl="car_other";
                    }


//SLIDESHOWPRO_BOL_Service::getInstance()->generate_list()
                $matches="";
//            preg_match('<iframe.+?src="(.+?)".+?<\/iframe>', $value['code'], $matches);
                preg_match('/<embed.+?src="([^"]+)"/', stripslashes($value['code']), $matches);
//            var_dump($matches);
//echo $matches[1];
//echo $value['code'];
//exit;
                if (isset($matches[1])){
//echo $matches[1];
                    $embed=str_replace("autoplay=1","",stripslashes($matches[1]));
                    $embed=str_replace("autoplay=true","",$embed);
                    $embed=str_replace("autoplay=","",$embed);
                    $embed=str_replace("autoplay =","",$embed);
                    $embed=str_replace("autoplay","",$embed);
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<iframe width=\"".$iwidth."\" height=\"".$iheight."\" src=\"".$embed."\"></iframe>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
//echo $content;exit;
                }else{
                    $embed=str_replace("autoplay=1","",stripslashes($value['code']));
                    $embed=str_replace("autoplay=true","",$embed);
                    $embed=str_replace("autoplay=","",$embed);
                    $embed=str_replace("autoplay =","",$embed);
                    $embed=str_replace("autoplay","",$embed);
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<a href=\"".$curent_url."video/".$value['id']."\">".$embed."</a>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
                }
            }
        }

        return $content;
    }


    public function generate_list($type="all",$iwidth="700",$iheight="333",$limit_each=10 )
    {
		$type="photo";
        $content="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        if (!$limit_each OR $limit_each<1) $limit_each=10;

        $is_first=true;

        $arrr_res=array();
        $ar_i=0;

        $item_photo=0;
        $item_videopro=0;
        $item_video=0;

        
        if ( OW::getPluginManager()->isPluginActive('photo') AND OW::getPluginManager()->isPluginActive('video') AND OW::getPluginManager()->isPluginActive('videopro')){
            $mmma=round($limit_each/3);

            $item_photo=$mmma;
            $item_videopro=$mmma;
            $item_video=$mmma;

            $add_corect=$limit_each-($limit_each*3);
            if ($add_corect>0){
                $item_photo=$item_photo+1;
            }
        }else if ( (OW::getPluginManager()->isPluginActive('photo') AND OW::getPluginManager()->isPluginActive('video')) ){
            $mmma=round($limit_each/2);

            $item_photo=$mmma;
            $item_video=$mmma;

            $add_corect=$limit_each-($limit_each*2);
            if ($add_corect>0){
                $item_photo=$item_photo+1;
            }
        }else if ( (OW::getPluginManager()->isPluginActive('photo') AND OW::getPluginManager()->isPluginActive('videopro')) ){
            $mmma=round($limit_each/2);

            $item_photo=$mmma;
            $item_videopro=$mmma;

            $add_corect=$limit_each-($limit_each*2);
            if ($add_corect>0){
                $item_photo=$item_photo+1;
            }
        }else if ( (OW::getPluginManager()->isPluginActive('videopro') AND OW::getPluginManager()->isPluginActive('video')) ){
            $mmma=round($limit_each/2);

            $item_videopro=$mmma;
            $item_video=$mmma;

            $add_corect=$limit_each-($limit_each*2);
            if ($add_corect>0){
                $item_videopro=$item_videopro+1;
            }
        }else if ( (OW::getPluginManager()->isPluginActive('photo')) ){
            $mmma=$limit_each;
            $item_photo=$mmma;
        }else if ( (OW::getPluginManager()->isPluginActive('videopro')) ){
            $mmma=$limit_each;
            $item_videopro=$mmma;
        }else if ( (OW::getPluginManager()->isPluginActive('video')) ){
            $mmma=$limit_each;
            $item_video=$mmma;
        }


        if ( OW::getPluginManager()->isPluginActive('photo') AND $item_photo>0){
        if ($type=="all" OR $type=="photo"){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "photo 
            WHERE privacy='everybody' ORDER BY RAND() DESC LIMIT ".$item_photo;
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value )
            {
                    
                    if ($is_first){
                        $st="";
                        $cl="";
                        $is_first=false;
                    }else{
                        $st="display:none;";
                        $cl="car_other";
                    }
            $content="";

                    if (isset($value['hash']) AND $value['hash']){
                        $imm=$curent_url."ow_userfiles/plugins/photo/photo_".$value['id']."_".$value['hash'].".jpg";
                    }else{
                        $imm=$curent_url."ow_userfiles/plugins/photo/photo_".$value['id'].".jpg";
                    }
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                    $content .="<a href=\"".$curent_url."photo/view/".$value['id']."\"><img width=\"".$iwidth."\" height=\"".$iheight."\" src=\"".$imm."\"></a>";
                    if ($value['description']){
                        $content .="<p><a href=\"".$curent_url."photo/view/".$value['id']."\"><strong>".stripslashes($value['description'])."</strong></a></p>";
                    }
                    $content .="</li>";                

                    $arrr_res[$ar_i]['id']=$ar_i;
                    $arrr_res[$ar_i]['ordd']=rand(1,100);
                    $arrr_res[$ar_i]['content']=$content;
                    $ar_i=$ar_i+1;

            }
        }
        }

        if ( OW::getPluginManager()->isPluginActive('videopro') AND $item_videopro>0){
        if ($type=="all" OR $type=="videopro"){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "videopro_link ddl
        LEFT JOIN " . OW_DB_PREFIX. "videopro_content ddd ON (ddd.id_link=ddl.id) 
            WHERE privacy='everybody' ORDER BY RAND() DESC LIMIT ".$item_videopro;
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value )
            {

                    if ($is_first){
                        $st="";
                        $cl="";
                        $is_first=false;
                    }else{
                        $st="display:none;";
                        $cl="car_other";
                    }


//SLIDESHOWPRO_BOL_Service::getInstance()->generate_list()
                $matches="";
//            preg_match('<iframe.+?src="(.+?)".+?<\/iframe>', $value['code'], $matches);
                preg_match('/<embed.+?src="([^"]+)"/', stripslashes($value['embed']), $matches);
                preg_match('/<iframe.+?src="([^"]+)"/', stripslashes($value['embed']), $matches2);
                preg_match('/<object.+?data="([^"]+)"/', stripslashes($value['embed']), $matches3);
//            var_dump($matches);
//echo $matches[1];
//echo $value['code'];
//exit;
                $content="";
                if (isset($matches[1])){
//echo $matches[1];
                    $embed=str_replace("autoplay=1","",stripslashes($matches[1]));
                    $embed=str_replace("autoplay=true","",$embed);
                    $embed=str_replace("autoplay=","",$embed);
                    $embed=str_replace("autoplay =","",$embed);
                    $embed=str_replace("autoplay","",$embed);
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<iframe width=\"".$iwidth."\" height=\"".$iheight."\" src=\"".$embed."\"></iframe>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
//echo $content;exit;
                }else if (isset($matches2[1])){
//echo $matches[1];
                    $embed=str_replace("autoplay=1","",stripslashes($matches2[1]));
                    $embed=str_replace("autoplay=true","",$embed);
                    $embed=str_replace("autoplay=","",$embed);
                    $embed=str_replace("autoplay =","",$embed);
                    $embed=str_replace("autoplay","",$embed);
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<iframe width=\"".$iwidth."\" height=\"".$iheight."\" src=\"".$embed."\"></iframe>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
//echo $content;exit;
                }else if (isset($matches3[1])){
//echo $matches[1];
                    $embed=str_replace("autoplay=1","",stripslashes($matches3[1]));
                    $embed=str_replace("autoplay=true","",$embed);
                    $embed=str_replace("autoplay=","",$embed);
                    $embed=str_replace("autoplay =","",$embed);
                    $embed=str_replace("autoplay","",$embed);
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<iframe width=\"".$iwidth."\" height=\"".$iheight."\" src=\"".$embed."\"></iframe>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
//echo $content;exit;
                }else{
/*
                    $code=stripslashes($value['embed']);
                    $code= preg_replace('/(width)="[0-9]*"/i', '$1="'.$iwidth.'"', $code);
                    $code= preg_replace('/(height)="[0-9]*"/i', '$1="'.$iheight.'"', $code);

                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<a width=\"".$iwidth."\" height=\"".$iheight."\" style=\"display:inline-block;\" href=\"".$curent_url."videopro/".$value['id']."\">".$code."</a>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
*/
                }


                if ($content){
                    $arrr_res[$ar_i]['id']=$ar_i;
                    $arrr_res[$ar_i]['ordd']=rand(1,100);
                    $arrr_res[$ar_i]['content']=$content;
                    $ar_i=$ar_i+1;
                }


            }
        }
        }

        if ( OW::getPluginManager()->isPluginActive('video') AND $item_video>0){
        if ($type=="all" OR $type=="video"){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "video_clip WHERE privacy='everybody' AND status='approved' ORDER BY RAND() DESC LIMIT ".$item_video;
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value )
            {

                    if ($is_first){
                        $st="";
                        $cl="";
                        $is_first=false;
                    }else{
                        $st="display:none;";
                        $cl="car_other";
                    }


//SLIDESHOWPRO_BOL_Service::getInstance()->generate_list()
                $matches="";
//            preg_match('<iframe.+?src="(.+?)".+?<\/iframe>', $value['code'], $matches);
                preg_match('/<embed.+?src="([^"]+)"/', stripslashes($value['code']), $matches);
                preg_match('/<iframe.+?src="([^"]+)"/', stripslashes($value['code']), $matches2);
                preg_match('/<object.+?data="([^"]+)"/', stripslashes($value['code']), $matches3);
//            var_dump($matches);
//echo $matches[1];
//echo $value['code'];
//exit;
                $content="";
                if (isset($matches[1])){
//echo $matches[1];
                    $embed=str_replace("autoplay=1","",stripslashes($matches[1]));
                    $embed=str_replace("autoplay=true","",$embed);
                    $embed=str_replace("autoplay=","",$embed);
                    $embed=str_replace("autoplay =","",$embed);
                    $embed=str_replace("autoplay","",$embed);
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<iframe width=\"".$iwidth."\" height=\"".$iheight."\" src=\"".$embed."\"></iframe>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
//echo $content;exit;
                }else if (isset($matches2[1])){
//echo $matches[1];
                    $embed=str_replace("autoplay=1","",stripslashes($matches2[1]));
                    $embed=str_replace("autoplay=true","",$embed);
                    $embed=str_replace("autoplay=","",$embed);
                    $embed=str_replace("autoplay =","",$embed);
                    $embed=str_replace("autoplay","",$embed);
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<iframe width=\"".$iwidth."\" height=\"".$iheight."\" src=\"".$embed."\"></iframe>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
//echo $content;exit;
                }else if (isset($matches3[1])){
//echo $matches[1];
                    $embed=str_replace("autoplay=1","",stripslashes($matches3[1]));
                    $embed=str_replace("autoplay=true","",$embed);
                    $embed=str_replace("autoplay=","",$embed);
                    $embed=str_replace("autoplay =","",$embed);
                    $embed=str_replace("autoplay","",$embed);
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<iframe width=\"".$iwidth."\" height=\"".$iheight."\" src=\"".$embed."\"></iframe>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
//echo $content;exit;
                }else{
/*
                    $code=stripslashes($value['code']);
                    $code= preg_replace('/(width)="[0-9]*"/i', '$1="'.$iwidth.'"', $code);
                    $code= preg_replace('/(height)="[0-9]*"/i', '$1="'.$iheight.'"', $code);
                    
                    $content .="<li class=\"".$cl."\" style=\"".$st."\">";
                        $content .="<a href=\"".$curent_url."video/view/".$value['id']."\">".$code."</a>";
//                    if ($value['title']){
//                        $content .="<p><a href=\"".$curent_url."video/".$value['id']."\"><strong>".stripslashes($value['title'])."</strong></a></p>";
//                    }
                    $content .="</li>";
*/
                }

                if ($content){
                    $arrr_res[$ar_i]['id']=$ar_i;
                    $arrr_res[$ar_i]['ordd']=rand(1,100);
                    $arrr_res[$ar_i]['content']=$content;
                    $ar_i=$ar_i+1;
                }

            }
        }
        }

        $content="";
//echo count($arrr_res);exit;
        if (count($arrr_res)>0){
//            usort($arrr_res, 'sortByOption');            
//            ksort($arrr_res);
//            array_multisort($arrr_res['ordd'],SORT_DESC,$arrr_res);
//                array_rand($arrr_res,count($arrr_res));
//                array_multisort($arrr_res, SORT_DESC);
            $rand_keys = array_rand($arrr_res, count($arrr_res));
//print_r($rand_keys);
//print_r($arrr_res);exit;
            for ($i=0;$i<count($arrr_res);$i++){
                $xx=$rand_keys[$i];
//echo $xx."--";
                $content .=$arrr_res[$xx]['content'];
            }
        }
        unset($arrr_res);
        return $content;
    }


// private function sortByOption($a, $b) {
  // return strcmp($a['optionNumber'], $b['optionNumber']);
// }


    public function get_news( )
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;

        $sql = "SELECT * FROM " . OW_DB_PREFIX. "news WHERE active='1' AND is_published='1'  ORDER BY `data_added` DESC LIMIT 10";
        $arrl = OW::getDbo()->queryForList($sql);
        $content="";
        foreach ( $arrl as $value )
        {
            $content .="<li>
                <div>
                    <a href=\"".$curent_url."news/".$value['id']."/index.html\">".stripslashes($value['topic_name'])."</a>
                </div>
            </li> ";
        }
        if ($content){
            $content ="<ul>".$content."</ul>";
        }
        return $content;
    }



    public function minix ()
    {
        $content="";
        $script="";
        $css="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $pluginStaticURL=OW::getPluginManager()->getPlugin('slideshowpro')->getStaticUrl();

        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/slideshowpro/slideshowpro.js');
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/slideshowpro/slideshowpro_main.js');

//        $content .="<div id=\"dnews\" class=\"news-wrapper\">
//        <div class=\"news\"></div>
//        </div>";
    

$content.="<div id=\"slideshowpro_news\" class=\"slideshowpro_news-wrapper\" style=\"display:none;\">";
$content.="<div class=\"slideshowpro_news\">";

        if ( OW::getPluginManager()->isPluginActive('news')){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "news nn 
            LEFT JOIN " . OW_DB_PREFIX. "news_content nnd ON (nnd.id_news= nn.id) 
            WHERE nn.active='1' AND nn.is_published='1'  ORDER BY nn.data_added DESC LIMIT 3";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value ){
                $desc=mb_substr($this->html2txt(stripslashes($value['content'])),0,200);
                $title=mb_substr($this->html2txt(stripslashes($value['topic_name'])),0,100);
                $url=$curent_url."news/".$value['id']."/index.html";
                $content .="<div class=\"slideshowpro_headline\"><a title=\"".$desc."\" href=\"".$url."\" style=\"overflow: hidden;max-width: 680px;display: inline-block;\">".$title."</a></div>";
            }
        }

        if ( OW::getPluginManager()->isPluginActive('blogs')){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "blogs_post WHERE isDraft='0' AND privacy='everybody' ORDER BY `timestamp` DESC LIMIT 3";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value ){
                $desc=mb_substr($this->html2txt(stripslashes($value['post'])),0,200);
                $title=mb_substr($this->html2txt(stripslashes($value['title'])),0,100);
                $url=$curent_url."blogs/".$value['id'];
                $content .="<div class=\"slideshowpro_headline\"><a title=\"".$desc."\" href=\"".$url."\" style=\"overflow: hidden;max-width: 680px;display: inline-block;\">".$title."</a></div>";
            }
        }

        if ( OW::getPluginManager()->isPluginActive('event')){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "event_item WHERE status='1' AND whoCanView='1' ORDER BY `createTimeStamp` DESC LIMIT 3";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value ){
                $desc=mb_substr($this->html2txt(stripslashes($value['description'])),0,200);
                $title=mb_substr($this->html2txt(stripslashes($value['title'])),0,100);
                $url=$curent_url."event/".$value['id'];
                $content .="<div class=\"slideshowpro_headline\"><a title=\"".$desc."\" href=\"".$url."\" style=\"overflow: hidden;max-width: 680px;display: inline-block;\">".$title."</a></div>";
            }
        }

        if ( OW::getPluginManager()->isPluginActive('groups')){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "groups_group WHERE privacy='everybody' AND whoCanView='anyone' ORDER BY `timeStamp` DESC LIMIT 3";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value ){
                $desc=mb_substr($this->html2txt(stripslashes($value['description'])),0,200);
                $title=mb_substr($this->html2txt(stripslashes($value['title'])),0,100);
                $url=$curent_url."groups/".$value['id'];
                $content .="<div class=\"slideshowpro_headline\"><a title=\"".$desc."\" href=\"".$url."\" style=\"overflow: hidden;max-width: 680px;display: inline-block;\">".$title."</a></div>";
            }
        }

        if ( OW::getPluginManager()->isPluginActive('forum')){
//            $sql = "SELECT * FROM " . OW_DB_PREFIX. "forum_post WHERE 1 ORDER BY `createStamp` DESC LIMIT 3";
            $sql = "SELECT pss.* FROM " . OW_DB_PREFIX. "forum_post pss 
            LEFT JOIN " . OW_DB_PREFIX. "forum_topic ft ON (ft.id=pss.topicId) 
            LEFT JOIN " . OW_DB_PREFIX. "forum_group fg ON (fg.id=ft.groupId) 
            LEFT JOIN " . OW_DB_PREFIX. "forum_section se ON (se.id=fg.sectionId) 
            WHERE (fg.isPrivate='0' OR fg.isPrivate IS NULL) AND entity IS NULL AND isHidden='0' 
            ORDER BY pss.createStamp DESC LIMIT 3";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value ){
                $desc=mb_substr($this->html2txt(stripslashes($value['text'])),0,200);
                $title=mb_substr($this->html2txt(stripslashes($value['text'])),0,100);
                $url=$curent_url."forum/topic/".$value['topicId'];
                $content .="<div class=\"slideshowpro_headline\"><a title=\"".$desc."\" href=\"".$url."\" style=\"overflow: hidden;max-width: 680px;display: inline-block;\">".$title."</a></div>";
            }
        }



$content.="</div>";
$content.="</div>";



$css .="<style>
.slideshowpro_news-wrapper
{
    width: 660px;
    position: fixed;
    overflow: hidden;
    bottom:0;
    left:0;
}
.slideshowpro_news-title
{
    float: left;
    background-image: url(".$pluginStaticURL."title-news.png);
    background-color: #2f2f2f;
    width: 18px;
    height: 36px;
    z-index: 11;
    border-radius-topleft: 3px;
    border-radius-bottomleft: 3px;
    -moz-border-radius-topleft: 3px;
    -moz-border-radius-bottomleft: 3px;
    -webkit-border-top-left-radius: 3px;
    -webkit-border-bottom-left-radius: 3px;
}
.slideshowpro_news
{
    background-color: #aed0ec;
    width: 640px;
    height: 36px;
    border-radius-topright: 4px;
    border-radius-bottomright: 4px;
    -moz-border-radius-topright: 4px;
    -moz-border-radius-bottomright: 4px;
    -webkit-border-top-right-radius: 4px;
    -webkit-border-bottom-right-radius: 4px;
    overflow: hidden;
}
.slideshowpro_headline
{
    position: absolute;
    font-size: 10pt;
    height: 10px;
    padding: 8px 12px 8px 8px;
    overflow: hidden;
    z-index: 1;
    /*overflow: hidden;*/
}
.slideshowpro_headline a
{
/*    background-image: url(".$pluginStaticURL."gaindi.png);
    background-repeat: no-repeat;
    background-position: right bottom;

    padding-right: 16px;        
*/
}
.slideshowpro_headline a, .slideshowpro_headline a:active, .slideshowpro_headline a:visited
{
    color: #1e1e1e;
    text-decoration: none;
}
.slideshowpro_headline a:hover
{
    color: #163b5b;
    text-decoration: none;
}
.slideshowpro_control-bar
{
    background-image: url(".$pluginStaticURL."control-bar.png);
    background-repeat: no-repeat;
    width: 97px;
    height: 21px;
    position: absolute;
    z-index: -1;
    top: 35px;
}
.slideshowpro_controls-wrapper
{
    position: relative;
    width: 48px;
    height: 12px;
    margin-left: 24px;
}
.slideshowpro_controls-wrapper div
{
    position: relative;
    width: 7px;
    height: 8px;
    margin-left: 6px;
    margin-top: 4px;
    float: left;
}
.slideshowpro_controls-wrapper div.next
{
    background: url(".$pluginStaticURL."next.png) no-repeat;
    cursor: pointer;
}
.slideshowpro_controls-wrapper div.prev
{
    background: url(".$pluginStaticURL."prev.png) no-repeat;
    cursor: pointer;
}
.slideshowpro_controls-wrapper div.play
{
    background: url(".$pluginStaticURL."play.png) no-repeat;
    cursor: pointer;
}
.slideshowpro_controls-wrapper div.pause
{
    background: url(".$pluginStaticURL."pause.png) no-repeat;
    cursor: pointer;
}
.slideshowpro_preview-wrapper
{
    font-size: 10pt;
    padding-top: 8px;
    width: 308px;
    height: 85px;
    position: absolute; /*margin: auto;*/
    z-index: 1000;
}
.slideshowpro_preview
{
    width: 292px;
    height: 61px;
    background-color: #2c2c2c;
    border-radius: 8px;
    -moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    border: solid 1px #212121;
    padding: 8px;
    font-size: 9pt; /*overflow: hidden;*/ /*background: url(".$pluginStaticURL."bg.jpg) repeat-x repeat-y;*/
}
.slideshowpro_preview h4, .slideshowpro_preview p
{
    margin: 0;
    padding: 0;
    overflow: hidden;
}
.slideshowpro_preview h4
{
    font-size: 10pt;
    color: White;
    font-weight: normal;
    width: 290px;
    height: 16px;
}
.slideshowpro_preview p
{
    color: #9f9e9e;
    width: 290px;
    height: 54px;
}
.slideshowpro_preview a
{
    font-size: 11px;
    color: #9f9e9e;
    text-decoration: none;
    margin-left: 4px;
}
.slideshowpro_preview a:hover
{
    text-decoration: underline;
    color: Yellow;
}
.slideshowpro_tip
{
    margin: auto;
    width: 13px;
    height: 9px;
    background-image: url(".$pluginStaticURL."cor.png);
}
.slideshowpro_news-wrapper.multiple
{
    width: 680px;
    overflow: hidden;
}
.slideshowpro_news-wrapper.multiple .slideshowpro_news-title
{
    float: left;
}
.slideshowpro_news-wrapper.multiple .slideshowpro_news
{
    float: left;
    width: 2000px;
}
.slideshowpro_news-wrapper.multiple .slideshowpro_headline
{
    position: relative;
    float: left;
    margin-left: 0px;
}
</style>
";

//    $script .="<script type=\"text/javascript\">";
//{ feedurl: 'http://www.dawn.com/rss', showdetail: true, controlsalwaysvisible: true, entries: 10, controls: true, target: "_blank" }
    $script .="
//    $(document).ready(function() {
        $('#slideshowpro_news').slideshowpronews({ showdetail: true, controlsalwaysvisible: false });
        $('#slideshowpro_news').show();
//    });
    ";
//    $script .="</script>\n";
//    OW::getDocument()->appendBody($content.$script.$css);
//    OW::getDocument()->appendBody($content.$css);

    OW::getDocument()->appendBody($content);
    OW::getDocument()->appendBody($css);
    OW::getDocument()->addOnloadScript($script);


//    echo "sfdsdF";exit;
//        return $content;
    }

/*
$pluginStaticURL2=OW::getPluginManager()->getPlugin('topmenu')->getStaticUrl();


        $script="";
        OW::getDocument()->addOnloadScript($script);

*/


}
