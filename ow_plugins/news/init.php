<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

OW::getRouter()->addRoute(new OW_Route('news.index', 'news', "NEWS_CTRL_News", 'index'));
OW::getRouter()->addRoute(new OW_Route('news.indexscat', 'news/:icat', "NEWS_CTRL_News", 'index'));

OW::getRouter()->addRoute(new OW_Route('news.indexzoom', 'news/:idnews/:title', "NEWS_CTRL_News", 'index'));
OW::getRouter()->addRoute(new OW_Route('news.admin', 'admin/plugins/news', "NEWS_CTRL_Admin", 'dept'));

OW::getRouter()->addRoute(new OW_Route('news.indexedcat', 'newscat/tabc/:ctab', "NEWS_CTRL_News", 'indexcat'));
OW::getRouter()->addRoute(new OW_Route('news.indexed', 'newsx/:action/:idobj', "NEWS_CTRL_News", 'indexaddedit'));

OW::getRouter()->addRoute(new OW_Route('news.index5', 'news_adm/:optionadm/:idcat', "NEWS_CTRL_News", 'index'));



/*
OW::getRouter()->addRoute(new OW_Route('news.index', 'news', "NEWS_CTRL_News", 'index'));

OW::getRouter()->addRoute(new OW_Route('news.admin', 'admin/plugins/news', "NEWS_CTRL_Admin", 'dept'));


OW::getRouter()->addRoute(new OW_Route('news.index2', 'product/:idproduct', "NEWS_CTRL_News", 'index'));
OW::getRouter()->addRoute(new OW_Route('news.index3', 'product/:idproduct/:options', "NEWS_CTRL_News", 'index'));
OW::getRouter()->addRoute(new OW_Route('news.index4', 'product/:idproduct/:options/:title', "NEWS_CTRL_News", 'index'));

OW::getRouter()->addRoute(new OW_Route('news.index6', 'news/:idcat', "NEWS_CTRL_News", 'index'));
OW::getRouter()->addRoute(new OW_Route('news.index7', 'news/:idcat/:title', "NEWS_CTRL_News", 'index'));
OW::getRouter()->addRoute(new OW_Route('news.baynow', 'baynow/:idproduct', "NEWS_CTRL_News", 'baynow'));
OW::getRouter()->addRoute(new OW_Route('news.basket', 'basket/:option', "NEWS_CTRL_News", 'index'));
OW::getRouter()->addRoute(new OW_Route('news.order', 'order/:option', "NEWS_CTRL_News", 'index'));

OW::getRouter()->addRoute(new OW_Route('news.orderdownload', 'news/:download/:option/:prot', "NEWS_CTRL_News", 'index'));

OW::getRouter()->addRoute(new OW_Route('news.buynowstatus', 'shopbuynow/:option', "NEWS_CTRL_News", 'indexbuynow'));
*/

/*
function news_handler_after_install( BASE_CLASS_EventCollector $event )
{
    if ( count(NEWS_BOL_Service::getInstance()->getDepartmentList()) < 1 )
    {
        $url = OW::getRouter()->urlForRoute('news.admin');
        $event->add(OW::getLanguage()->text('news', 'after_install_notification', array('url' => $url)));
    }
}

OW::getEventManager()->bind('admin.add_admin_notification', 'news_handler_after_install');
*/


function news_add_auth_labels( BASE_CLASS_EventCollector $event )
{
    $language = OW::getLanguage();
    $event->add(
        array(
            'youtubeplay' => array(
                'label' => $language->text('news', 'auth_group_label'),
                'actions' => array(
                    'add' => $language->text('news', 'auth_action_label_add'),
                    'view' => $language->text('news', 'auth_action_label_display')
                )
            )
        )
    );
}
OW::getEventManager()->bind('admin.add_auth_labels', 'news_add_auth_labels');

/*
function news_ads_enabled( BASE_EventCollector $event )
{
    $event->add('news');
}

OW::getEventManager()->bind('ads.enabled_plugins', 'news_ads_enabled');

OW::getRequestHandler()->addCatchAllRequestsExclude('base.suspended_user', 'NEWS_CTRL_News');
*/

/*
$config = OW::getConfig();
if ( !$config->configExists('news', 'mode') ){
    $config->addConfig('news', 'mode', "rss", '');
}
*/



/*
function setrestriction(){
$script ="";
$hh=OW_URL_HOME;
$hh=str_replace("https://","",$hh);
$hh=str_replace("http://","",$hh);
$hh=str_replace("'","",$hh);
$hh=str_replace("`","",$hh);
$hh=str_replace("\\","",$hh);
$hh_tmp=explode("/",$hh);
if (isset($hh_tmp[0]) AND $hh_tmp[0]!="") {
    $hh=$hh_tmp[0];
$script .="<script type=\"text/javascript\">
$('a').each(function(index, element) {
var searchx = new RegExp('/".$hh."/');
if(!searchx.test(this.href)) {
$(this).attr('rel','nofollow');
//    $(this).replaceWith( ' '+$(this).text()+' ' );
}
});";
$script .="</script>\n";
OW::getDocument()->appendBody($script);
}
}
//if (!OW::getRequest()->isPost() OR OW::getRequest()->isAjax() OR strpos($_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"],"ow_cron")!==false){
if (!isset($_SERVER["SERVER_NAME"])) $_SERVER["SERVER_NAME"]="";
if (!isset($_SERVER["REQUEST_URI"])) $_SERVER["REQUEST_URI"]="";
if (!OW::getRequest()->isPost() AND !OW::getRequest()->isAjax() AND strpos($_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"],"ow_cron")===false){
    OW::getEventManager()->bind('core.finalize', 'setrestriction');
}
*/



/*

$config = OW::getConfig();
if ( !$config->configExists('news', 'rss_can_1') ){
    $config->addConfig('news', 'rss_can_1', "", '');
}
if ( !$config->configExists('news', 'rss_can_2') ){
    $config->addConfig('news', 'rss_can_2', "", '');
}
if ( !$config->configExists('news', 'rss_can_3') ){
    $config->addConfig('news', 'rss_can_3', "", '');
}
if ( !$config->configExists('news', 'rss_can_4') ){
    $config->addConfig('news', 'rss_can_4', "", '');
}
if ( !$config->configExists('news', 'rss_can_5') ){
    $config->addConfig('news', 'rss_can_5', "", '');
}
if ( !$config->configExists('news', 'rss_can_6') ){
    $config->addConfig('news', 'rss_can_6', "", '');
}
if ( !$config->configExists('news', 'rss_can_7') ){
    $config->addConfig('news', 'rss_can_7', "", '');
}
if ( !$config->configExists('news', 'rss_can_8') ){
    $config->addConfig('news', 'rss_can_8', "", '');
}
if ( !$config->configExists('news', 'rss_can_9') ){
    $config->addConfig('news', 'rss_can_9', "", '');
}
if ( !$config->configExists('news', 'rss_can_10') ){
    $config->addConfig('news', 'rss_can_10', "", '');
}

if ( !$config->configExists('news', 'rss_own_1') ){
    $config->addConfig('news', 'rss_own_1', "", '');
}
if ( !$config->configExists('news', 'rss_own_2') ){
    $config->addConfig('news', 'rss_own_2', "", '');
}
if ( !$config->configExists('news', 'rss_own_3') ){
    $config->addConfig('news', 'rss_own_3', "", '');
}
if ( !$config->configExists('news', 'rss_own_4') ){
    $config->addConfig('news', 'rss_own_4', "", '');
}
if ( !$config->configExists('news', 'rss_own_5') ){
    $config->addConfig('news', 'rss_own_5', "", '');
}
if ( !$config->configExists('news', 'rss_own_6') ){
    $config->addConfig('news', 'rss_own_6', "", '');
}
if ( !$config->configExists('news', 'rss_own_7') ){
    $config->addConfig('news', 'rss_own_7', "", '');
}
if ( !$config->configExists('news', 'rss_own_8') ){
    $config->addConfig('news', 'rss_own_8', "", '');
}
if ( !$config->configExists('news', 'rss_own_9') ){
    $config->addConfig('news', 'rss_own_9', "", '');
}
if ( !$config->configExists('news', 'rss_own_10') ){
    $config->addConfig('news', 'rss_own_10', "", '');
}

if ( !$config->configExists('news', 'rss_item_1') ){
    $config->addConfig('news', 'rss_item_1', "2", '');
}
if ( !$config->configExists('news', 'rss_item_2') ){
    $config->addConfig('news', 'rss_item_2', "2", '');
}
if ( !$config->configExists('news', 'rss_item_3') ){
    $config->addConfig('news', 'rss_item_3', "2", '');
}
if ( !$config->configExists('news', 'rss_item_4') ){
    $config->addConfig('news', 'rss_item_4', "2", '');
}
if ( !$config->configExists('news', 'rss_item_5') ){
    $config->addConfig('news', 'rss_item_5', "2", '');
}
if ( !$config->configExists('news', 'rss_item_6') ){
    $config->addConfig('news', 'rss_item_6', "2", '');
}
if ( !$config->configExists('news', 'rss_item_7') ){
    $config->addConfig('news', 'rss_item_7', "2", '');
}
if ( !$config->configExists('news', 'rss_item_8') ){
    $config->addConfig('news', 'rss_item_8', "2", '');
}
if ( !$config->configExists('news', 'rss_item_9') ){
    $config->addConfig('news', 'rss_item_9', "2", '');
}
if ( !$config->configExists('news', 'rss_item_10') ){
    $config->addConfig('news', 'rss_item_10', "2", '');
}

if ( !$config->configExists('news', 'rss_topic_1') ){
    $config->addConfig('news', 'rss_topic_1', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_2') ){
    $config->addConfig('news', 'rss_topic_2', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_3') ){
    $config->addConfig('news', 'rss_topic_3', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_4') ){
    $config->addConfig('news', 'rss_topic_4', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_5') ){
    $config->addConfig('news', 'rss_topic_5', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_6') ){
    $config->addConfig('news', 'rss_topic_6', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_7') ){
    $config->addConfig('news', 'rss_topic_7', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_8') ){
    $config->addConfig('news', 'rss_topic_8', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_9') ){
    $config->addConfig('news', 'rss_topic_9', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_10') ){
    $config->addConfig('news', 'rss_topic_10', "RSS", '');
}
*/

