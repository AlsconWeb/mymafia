<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


/*
//BOL_LanguageService::getInstance()->addPrefix('news', 'Contact Us');
$config = OW::getConfig();

if ( !$config->configExists('news', 'mode_payment') ){
    $config->addConfig('news', 'mode_payment', "paypal", 'paypal,pay24');
}

if ( !$config->configExists('news', 'mode') ){
    $config->addConfig('news', 'mode', "0", 'With sitebar or not');
}

if ( !$config->configExists('news', 'max_cat_title_chars') ){
    $config->addConfig('news', 'max_cat_title_chars', "20", 'Max category name length');
}

if ( !$config->configExists('news', 'paypal_currency') ){
    $config->addConfig('news', 'paypal_currency', "USD", 'Currency (USD,PLN,...)');
}

if ( !$config->configExists('news', 'pay24_currency') ){
    $config->addConfig('news', 'pay24_currency', "PLN", 'Currency (PLN,...)');
}

if ( !$config->configExists('news', 'paypal_account') ){
    $config->addConfig('news', 'paypal_account', "", 'PayPal account');
}
if ( !$config->configExists('news', 'pay24_account') ){
    $config->addConfig('news', 'pay24_account', "", 'Platnosci24.pl account');
}

if ( !$config->configExists('news', 'pay24_account_crc') ){
    $config->addConfig('news', 'pay24_account_crc', "", 'Account Platnosci24.pl SCR');
}

if ( !$config->configExists('news', 'mode_membercanshell') ){
    $config->addConfig('news', 'mode_membercanshell', "1", 'Member can shell each products....');
}
if ( !$config->configExists('news', 'mode_sellfiles') ){
    $config->addConfig('news', 'mode_sellfiles', "0", 'Member can shell files...');
}

if ( !$config->configExists('news', 'publish_newproduct_onwall') ){
    $config->addConfig('news', 'publish_newproduct_onwall', "1", 'Publish new product on the wall...');
}

if ( !$config->configExists('news', 'publish_updateproduct_onwall') ){
    $config->addConfig('news', 'publish_updateproduct_onwall', "0", 'Publish update product on the wall...');
}
*/
$config = OW::getConfig();


if ( !$config->configExists('news', 'widget_big_full') ){
    $config->addConfig('news', 'widget_big_full', "0", '');
}

if ( !$config->configExists('news', 'allow_add_for_users') ){
    $config->addConfig('news', 'allow_add_for_users', "1", '');
}

if ( !$config->configExists('news', 'embed_video_width') ){
    $config->addConfig('news', 'embed_video_width', "580", '');
}
if ( !$config->configExists('news', 'embed_video_height') ){
    $config->addConfig('news', 'embed_video_height', "387", '');
}
if ( !$config->configExists('news', 'embed_image_width') ){
    $config->addConfig('news', 'embed_image_width', "600", '');
}
if ( !$config->configExists('news', 'embed_image_height') ){
    $config->addConfig('news', 'embed_image_height', "auto", '');
}


if ( !$config->configExists('news', 'rss_url') ){
    $config->addConfig('news', 'rss_url', "http://finance.yahoo.com/rss/usmarkets", '');
}
if ( !$config->configExists('news', 'rss_items') ){
    $config->addConfig('news', 'rss_items', "6", '');
}
if ( !$config->configExists('news', 'rss_title_length_main') ){
    $config->addConfig('news', 'rss_title_length_main', "55", '');
}
if ( !$config->configExists('news', 'rss_title_length_sidebar') ){
    $config->addConfig('news', 'rss_title_length_sidebar', "25", '');
}
if ( !$config->configExists('news', 'rss_title_length_dashboard') ){
    $config->addConfig('news', 'rss_title_length_dashboard', "25", '');
}
if ( !$config->configExists('news', 'rss_content_length_main') ){
    $config->addConfig('news', 'rss_content_length_main', "95", '');
}
if ( !$config->configExists('news', 'rss_content_length_sidebar') ){
    $config->addConfig('news', 'rss_content_length_sidebar', "0", '');
}
if ( !$config->configExists('news', 'rss_content_length_dashboard') ){
    $config->addConfig('news', 'rss_content_length_dashboard', "0", '');
}

if ( !$config->configExists('news', 'rss_url_sidebar') ){
    $config->addConfig('news', 'rss_url_sidebar', "http://news.yahoo.com/rss/science", '');
}
if ( !$config->configExists('news', 'rss_url_dashboard') ){
    $config->addConfig('news', 'rss_url_dashboard', "http://news.yahoo.com/rss/sports", '');
}
if ( !$config->configExists('news', 'rss_cachetime_minuts') ){
    $config->addConfig('news', 'rss_cachetime_minuts', "15", '');
}
if ( !$config->configExists('news', 'rss_items_sidebar') ){
    $config->addConfig('news', 'rss_items_sidebar', "5", '');
}
if ( !$config->configExists('news', 'rss_items_dashboard') ){
    $config->addConfig('news', 'rss_items_dashboard', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather1') ){
    $config->addConfig('news', 'rss_url_ather1', "http://news.yahoo.com/rss/energy", '');
}
if ( !$config->configExists('news', 'rss_url_ather2') ){
    $config->addConfig('news', 'rss_url_ather2', "http://news.yahoo.com/rss/elections-2012", '');
}
if ( !$config->configExists('news', 'rss_url_ather3') ){
    $config->addConfig('news', 'rss_url_ather3', "http://news.yahoo.com/obituaries/", '');
}
if ( !$config->configExists('news', 'rss_items_ather1') ){
    $config->addConfig('news', 'rss_items_ather1', "5", '');
}
if ( !$config->configExists('news', 'rss_items_ather2') ){
    $config->addConfig('news', 'rss_items_ather2', "5", '');
}
if ( !$config->configExists('news', 'rss_items_ather3') ){
    $config->addConfig('news', 'rss_items_ather3', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather4') ){
    $config->addConfig('news', 'rss_url_ather4', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather4') ){
    $config->addConfig('news', 'rss_items_ather4', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather5') ){
    $config->addConfig('news', 'rss_url_ather5', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather5') ){
    $config->addConfig('news', 'rss_items_ather5', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather6') ){
    $config->addConfig('news', 'rss_url_ather6', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather6') ){
    $config->addConfig('news', 'rss_items_ather6', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather7') ){
    $config->addConfig('news', 'rss_url_ather7', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather7') ){
    $config->addConfig('news', 'rss_items_ather7', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather8') ){
    $config->addConfig('news', 'rss_url_ather8', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather8') ){
    $config->addConfig('news', 'rss_items_ather8', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather9') ){
    $config->addConfig('news', 'rss_url_ather9', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather9') ){
    $config->addConfig('news', 'rss_items_ather9', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather10') ){
    $config->addConfig('news', 'rss_url_ather10', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather10') ){
    $config->addConfig('news', 'rss_items_ather10', "5", '');
}
if ( !$config->configExists('news', 'mode') ){
    $config->addConfig('news', 'mode', "news", '');
}


if ( !$config->configExists('news', 'rss_can_1') ){
    $config->addConfig('news', 'rss_can_1', "", '');
}
if ( !$config->configExists('news', 'rss_can_2') ){
    $config->addConfig('news', 'rss_can_2', "", '');
}
if ( !$config->configExists('news', 'rss_can_3') ){
    $config->addConfig('news', 'rss_can_3', "", '');
}
if ( !$config->configExists('news', 'rss_can_4') ){
    $config->addConfig('news', 'rss_can_4', "", '');
}
if ( !$config->configExists('news', 'rss_can_5') ){
    $config->addConfig('news', 'rss_can_5', "", '');
}
if ( !$config->configExists('news', 'rss_can_6') ){
    $config->addConfig('news', 'rss_can_6', "", '');
}
if ( !$config->configExists('news', 'rss_can_7') ){
    $config->addConfig('news', 'rss_can_7', "", '');
}
if ( !$config->configExists('news', 'rss_can_8') ){
    $config->addConfig('news', 'rss_can_8', "", '');
}
if ( !$config->configExists('news', 'rss_can_9') ){
    $config->addConfig('news', 'rss_can_9', "", '');
}
if ( !$config->configExists('news', 'rss_can_10') ){
    $config->addConfig('news', 'rss_can_10', "", '');
}

if ( !$config->configExists('news', 'rss_own_1') ){
    $config->addConfig('news', 'rss_own_1', "", '');
}
if ( !$config->configExists('news', 'rss_own_2') ){
    $config->addConfig('news', 'rss_own_2', "", '');
}
if ( !$config->configExists('news', 'rss_own_3') ){
    $config->addConfig('news', 'rss_own_3', "", '');
}
if ( !$config->configExists('news', 'rss_own_4') ){
    $config->addConfig('news', 'rss_own_4', "", '');
}
if ( !$config->configExists('news', 'rss_own_5') ){
    $config->addConfig('news', 'rss_own_5', "", '');
}
if ( !$config->configExists('news', 'rss_own_6') ){
    $config->addConfig('news', 'rss_own_6', "", '');
}
if ( !$config->configExists('news', 'rss_own_7') ){
    $config->addConfig('news', 'rss_own_7', "", '');
}
if ( !$config->configExists('news', 'rss_own_8') ){
    $config->addConfig('news', 'rss_own_8', "", '');
}
if ( !$config->configExists('news', 'rss_own_9') ){
    $config->addConfig('news', 'rss_own_9', "", '');
}
if ( !$config->configExists('news', 'rss_own_10') ){
    $config->addConfig('news', 'rss_own_10', "", '');
}

if ( !$config->configExists('news', 'rss_item_1') ){
    $config->addConfig('news', 'rss_item_1', "2", '');
}
if ( !$config->configExists('news', 'rss_item_2') ){
    $config->addConfig('news', 'rss_item_2', "2", '');
}
if ( !$config->configExists('news', 'rss_item_3') ){
    $config->addConfig('news', 'rss_item_3', "2", '');
}
if ( !$config->configExists('news', 'rss_item_4') ){
    $config->addConfig('news', 'rss_item_4', "2", '');
}
if ( !$config->configExists('news', 'rss_item_5') ){
    $config->addConfig('news', 'rss_item_5', "2", '');
}
if ( !$config->configExists('news', 'rss_item_6') ){
    $config->addConfig('news', 'rss_item_6', "2", '');
}
if ( !$config->configExists('news', 'rss_item_7') ){
    $config->addConfig('news', 'rss_item_7', "2", '');
}
if ( !$config->configExists('news', 'rss_item_8') ){
    $config->addConfig('news', 'rss_item_8', "2", '');
}
if ( !$config->configExists('news', 'rss_item_9') ){
    $config->addConfig('news', 'rss_item_9', "2", '');
}
if ( !$config->configExists('news', 'rss_item_10') ){
    $config->addConfig('news', 'rss_item_10', "2", '');
}

if ( !$config->configExists('news', 'rss_topic_1') ){
    $config->addConfig('news', 'rss_topic_1', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_2') ){
    $config->addConfig('news', 'rss_topic_2', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_3') ){
    $config->addConfig('news', 'rss_topic_3', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_4') ){
    $config->addConfig('news', 'rss_topic_4', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_5') ){
    $config->addConfig('news', 'rss_topic_5', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_6') ){
    $config->addConfig('news', 'rss_topic_6', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_7') ){
    $config->addConfig('news', 'rss_topic_7', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_8') ){
    $config->addConfig('news', 'rss_topic_8', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_9') ){
    $config->addConfig('news', 'rss_topic_9', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_10') ){
    $config->addConfig('news', 'rss_topic_10', "RSS", '');
}



//installing language pack
OW::getLanguage()->importPluginLangs(OW::getPluginManager()->getPlugin('news')->getRootDir().'langs.zip', 'news');
//adding admin settings page
OW::getPluginManager()->addPluginSettingsRouteName('news', 'news.admin');


$authorization = OW::getAuthorization();
$groupName = 'news';
$authorization->addGroup($groupName);
$authorization->addAction($groupName, 'add');
$authorization->addAction($groupName, 'view',true);




//----databases:
$sql = "CREATE TABLE IF NOT EXISTS `".OW_DB_PREFIX."news` (
  `id` bigint(22) unsigned NOT NULL auto_increment,
  `active` enum('0','1') collate utf8_bin NOT NULL default '1',
  `id_sender` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL,
  `mlat` varchar(11) collate utf8_bin NOT NULL default '',
  `mlon` varchar(11) collate utf8_bin NOT NULL default '',
  `ftom_ip` varchar(16) collate utf8_bin NOT NULL default '',
  `data_mod` int(11) NOT NULL,
  `is_published` enum('1','0','waiting','denny','approved') collate utf8_bin NOT NULL default '1',
  `data_added` int(11) NOT NULL,
  `topic_name` varchar(255) collate utf8_bin NOT NULL,
  `sender_name` varchar(128) collate utf8_bin NOT NULL,
  `ext_ident` varchar(64) collate utf8_bin default NULL,
  `ext_unique` varchar(200) collate utf8_bin default NULL,
  `ext_lat_mod` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `ext_ident` (`ext_ident`,`ext_unique`),
  KEY `data_added` (`data_added`),
  KEY `active` (`active`),
  KEY `topic` (`topic_name`),
  KEY `id_sender` (`id_sender`),
  KEY `id_topic` (`id_topic`),
  KEY `ext_lat_mod` (`ext_lat_mod`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;";
OW::getDbo()->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `".OW_DB_PREFIX."news_content` (
  `id_news` bigint(22) unsigned NOT NULL,
  `content` text collate utf8_bin,
  `ext_footer` text collate utf8_bin NOT NULL,
  UNIQUE KEY `id_news` (`id_news`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
OW::getDbo()->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `".OW_DB_PREFIX."news_topics` (
  `idt` int(11) NOT NULL auto_increment,
  `active` enum('0','1') collate utf8_bin NOT NULL default '1',
  `t_name` varchar(255) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`idt`),
  UNIQUE KEY `t_name` (`t_name`),
  KEY `active` (`active`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;";
OW::getDbo()->query($sql);

$sql = "INSERT INTO  `".OW_DB_PREFIX."news_topics` (`idt` ,`active` ,`t_name`)VALUES ('',  '1',  'Default');";
OW::getDbo()->query($sql);



