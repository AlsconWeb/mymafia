<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


Updater::getLanguageService()->importPrefixFromZip(dirname(__FILE__).DS.'langs.zip', 'news');

/*
        BOL_ComponentAdminService::getInstance()->deleteWidget('NEWS_CMP_MenuWidget');
        BOL_ComponentAdminService::getInstance()->deleteWidget('NEWS_CMP_IndexWidget');
        BOL_ComponentAdminService::getInstance()->deleteWidget('NEWS_CMP_DashboardWidget');

        $cmpService = BOL_ComponentAdminService::getInstance();
        $widget = $cmpService->addWidget('NEWS_CMP_MenuWidget', false);
        $placeWidget = $cmpService->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_INDEX);
        $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_SIDEBAR,0);

        $widget = $cmpService->addWidget('NEWS_CMP_IndexWidget', false);
        $placeWidget = $cmpService->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_INDEX);
        $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_RIGHT,0);

        $widget = $cmpService->addWidget('NEWS_CMP_DashboardWidget', false);
        $placeWidget = BOL_ComponentAdminService::getInstance()->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_DASHBOARD);
        $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_RIGHT, 0);
*/
/*

$config = OW::getConfig();
if ( !$config->configExists('news', 'rss_can_1') ){
    $config->addConfig('news', 'rss_can_1', "", '');
}
if ( !$config->configExists('news', 'rss_can_2') ){
    $config->addConfig('news', 'rss_can_2', "", '');
}
if ( !$config->configExists('news', 'rss_can_3') ){
    $config->addConfig('news', 'rss_can_3', "", '');
}
if ( !$config->configExists('news', 'rss_can_4') ){
    $config->addConfig('news', 'rss_can_4', "", '');
}
if ( !$config->configExists('news', 'rss_can_5') ){
    $config->addConfig('news', 'rss_can_5', "", '');
}
if ( !$config->configExists('news', 'rss_can_6') ){
    $config->addConfig('news', 'rss_can_6', "", '');
}
if ( !$config->configExists('news', 'rss_can_7') ){
    $config->addConfig('news', 'rss_can_7', "", '');
}
if ( !$config->configExists('news', 'rss_can_8') ){
    $config->addConfig('news', 'rss_can_8', "", '');
}
if ( !$config->configExists('news', 'rss_can_9') ){
    $config->addConfig('news', 'rss_can_9', "", '');
}
if ( !$config->configExists('news', 'rss_can_10') ){
    $config->addConfig('news', 'rss_can_10', "", '');
}

if ( !$config->configExists('news', 'rss_own_1') ){
    $config->addConfig('news', 'rss_own_1', "", '');
}
if ( !$config->configExists('news', 'rss_own_2') ){
    $config->addConfig('news', 'rss_own_2', "", '');
}
if ( !$config->configExists('news', 'rss_own_3') ){
    $config->addConfig('news', 'rss_own_3', "", '');
}
if ( !$config->configExists('news', 'rss_own_4') ){
    $config->addConfig('news', 'rss_own_4', "", '');
}
if ( !$config->configExists('news', 'rss_own_5') ){
    $config->addConfig('news', 'rss_own_5', "", '');
}
if ( !$config->configExists('news', 'rss_own_6') ){
    $config->addConfig('news', 'rss_own_6', "", '');
}
if ( !$config->configExists('news', 'rss_own_7') ){
    $config->addConfig('news', 'rss_own_7', "", '');
}
if ( !$config->configExists('news', 'rss_own_8') ){
    $config->addConfig('news', 'rss_own_8', "", '');
}
if ( !$config->configExists('news', 'rss_own_9') ){
    $config->addConfig('news', 'rss_own_9', "", '');
}
if ( !$config->configExists('news', 'rss_own_10') ){
    $config->addConfig('news', 'rss_own_10', "", '');
}

if ( !$config->configExists('news', 'rss_item_1') ){
    $config->addConfig('news', 'rss_item_1', "2", '');
}
if ( !$config->configExists('news', 'rss_item_2') ){
    $config->addConfig('news', 'rss_item_2', "2", '');
}
if ( !$config->configExists('news', 'rss_item_3') ){
    $config->addConfig('news', 'rss_item_3', "2", '');
}
if ( !$config->configExists('news', 'rss_item_4') ){
    $config->addConfig('news', 'rss_item_4', "2", '');
}
if ( !$config->configExists('news', 'rss_item_5') ){
    $config->addConfig('news', 'rss_item_5', "2", '');
}
if ( !$config->configExists('news', 'rss_item_6') ){
    $config->addConfig('news', 'rss_item_6', "2", '');
}
if ( !$config->configExists('news', 'rss_item_7') ){
    $config->addConfig('news', 'rss_item_7', "2", '');
}
if ( !$config->configExists('news', 'rss_item_8') ){
    $config->addConfig('news', 'rss_item_8', "2", '');
}
if ( !$config->configExists('news', 'rss_item_9') ){
    $config->addConfig('news', 'rss_item_9', "2", '');
}
if ( !$config->configExists('news', 'rss_item_10') ){
    $config->addConfig('news', 'rss_item_10', "2", '');
}

if ( !$config->configExists('news', 'rss_topic_1') ){
    $config->addConfig('news', 'rss_topic_1', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_2') ){
    $config->addConfig('news', 'rss_topic_2', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_3') ){
    $config->addConfig('news', 'rss_topic_3', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_4') ){
    $config->addConfig('news', 'rss_topic_4', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_5') ){
    $config->addConfig('news', 'rss_topic_5', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_6') ){
    $config->addConfig('news', 'rss_topic_6', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_7') ){
    $config->addConfig('news', 'rss_topic_7', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_8') ){
    $config->addConfig('news', 'rss_topic_8', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_9') ){
    $config->addConfig('news', 'rss_topic_9', "RSS", '');
}
if ( !$config->configExists('news', 'rss_topic_10') ){
    $config->addConfig('news', 'rss_topic_10', "RSS", '');
}
*/
/*
$plname="news";
$source=OW_DIR_PLUGIN.$plname. DS.'static'. DS;
$pluginStaticDir = OW_DIR_STATIC .'plugins'.DS.$plname.DS;


//echo $source;
//echo "<hr>";
//echo $pluginStaticDir;
//exit;

//CMS_BOL_Service::getInstance()->cpydir($source, $pluginStaticDir);
//echo "sss";exit;
cms_cpydir($source, $pluginStaticDir);
function cms_cpydir($source,$dest){
        if(is_dir($source)) {
            $dir_handle=opendir($source);
            while($file=readdir($dir_handle)){
                if($file!="." && $file!=".."){
                    if(is_dir($source.$file)){

                        if (!is_dir($dest.$file.DS)) mkdir($dest.$file.DS);

                        cms_cpydir($source.$file.DS, $dest.$file.DS);
                    } else {
//echo $source.$file."<br>".$dest.$file."<hr>";
//                        if (!is_file($dest.$file)) copy($source.$file, $dest.$file);
                         copy($source.$file, $dest.$file);
                    }
                }
            }
            closedir($dir_handle);
        } else {
            copy($source, $dest);
        }
}
*/

/*
$sql = "ALTER TABLE  `".OW_DB_PREFIX."news_content` ADD  `ext_footer` TEXT NOT NULL";
Updater::getDbo()->query($sql);

*/



