<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


Updater::getLanguageService()->importPrefixFromZip(dirname(__FILE__).DS.'langs.zip', 'news');

/*
$config = OW::getConfig();
if ( !$config->configExists('news', 'mode') ){
    $config->addConfig('news', 'mode', "rss", '');
}

$plname="news";
$source=OW_DIR_PLUGIN.$plname. DS.'static'. DS;
$pluginStaticDir = OW_DIR_STATIC .'plugins'.DS.$plname.DS;


//echo $source;
//echo "<hr>";
//echo $pluginStaticDir;
//exit;

//CMS_BOL_Service::getInstance()->cpydir($source, $pluginStaticDir);
//echo "sss";exit;
cms_cpydir($source, $pluginStaticDir);
function cms_cpydir($source,$dest){
        if(is_dir($source)) {
            $dir_handle=opendir($source);
            while($file=readdir($dir_handle)){
                if($file!="." && $file!=".."){
                    if(is_dir($source.$file)){

                        if (!is_dir($dest.$file.DS)) mkdir($dest.$file.DS);

                        cms_cpydir($source.$file.DS, $dest.$file.DS);
                    } else {
//echo $source.$file."<br>".$dest.$file."<hr>";
//                        if (!is_file($dest.$file)) copy($source.$file, $dest.$file);
                         copy($source.$file, $dest.$file);
                    }
                }
            }
            closedir($dir_handle);
        } else {
            copy($source, $dest);
        }
}

$sql = "DROP TABLE IF EXISTS `".OW_DB_PREFIX."news` ";
Updater::getDbo()->query($sql);
$sql = "DROP TABLE IF EXISTS `".OW_DB_PREFIX."news_topics` ";
Updater::getDbo()->query($sql);
$sql = "DROP TABLE IF EXISTS `".OW_DB_PREFIX."news_content` ";
Updater::getDbo()->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `".OW_DB_PREFIX."news` (
  `id` bigint(22) unsigned NOT NULL auto_increment,
  `active` enum('0','1') collate utf8_bin NOT NULL default '1',
  `id_sender` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL,
  `mlat` varchar(11) collate utf8_bin NOT NULL default '',
  `mlon` varchar(11) collate utf8_bin NOT NULL default '',
  `ftom_ip` varchar(16) collate utf8_bin NOT NULL default '',
  `data_mod` int(11) NOT NULL,
  `is_published` enum('1','0','waiting','denny','approved') collate utf8_bin NOT NULL default '1',
  `data_added` int(11) NOT NULL,
  `topic_name` varchar(255) collate utf8_bin NOT NULL,
  `sender_name` varchar(128) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `data_added` (`data_added`),
  KEY `active` (`active`),
  KEY `topic` (`topic_name`),
  KEY `id_sender` (`id_sender`),
  KEY `id_topic` (`id_topic`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;";
Updater::getDbo()->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `".OW_DB_PREFIX."news_content` (
  `id_news` bigint(22) unsigned NOT NULL,
  `content` text collate utf8_bin,
  UNIQUE KEY `id_news` (`id_news`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
Updater::getDbo()->query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `".OW_DB_PREFIX."news_topics` (
  `idt` int(11) NOT NULL auto_increment,
  `active` enum('0','1') collate utf8_bin NOT NULL default '1',
  `t_name` varchar(255) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`idt`),
  KEY `active` (`active`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;";
Updater::getDbo()->query($sql);

$sql = "ALTER TABLE  `".OW_DB_PREFIX."blogs_post` ADD FULLTEXT (`post`)";
Updater::getDbo()->query($sql);


$sql = "INSERT INTO  `".OW_DB_PREFIX."news_topics` (`idt` ,`active` ,`t_name`)VALUES ('',  '1',  'Default');";
Updater::getDbo()->query($sql);

*/

