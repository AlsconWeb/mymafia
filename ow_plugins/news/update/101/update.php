<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


Updater::getLanguageService()->importPrefixFromZip(dirname(__FILE__).DS.'langs.zip', 'news');


$config = OW::getConfig();
if ( !$config->configExists('news', 'rss_url') ){
    $config->addConfig('news', 'rss_url', "http://finance.yahoo.com/rss/usmarkets", '');
}
if ( !$config->configExists('news', 'rss_items') ){
    $config->addConfig('news', 'rss_items', "6", '');
}
if ( !$config->configExists('news', 'rss_title_length_main') ){
    $config->addConfig('news', 'rss_title_length_main', "55", '');
}
if ( !$config->configExists('news', 'rss_title_length_sidebar') ){
    $config->addConfig('news', 'rss_title_length_sidebar', "25", '');
}
if ( !$config->configExists('news', 'rss_title_length_dashboard') ){
    $config->addConfig('news', 'rss_title_length_dashboard', "25", '');
}
if ( !$config->configExists('news', 'rss_content_length_main') ){
    $config->addConfig('news', 'rss_content_length_main', "95", '');
}
if ( !$config->configExists('news', 'rss_content_length_sidebar') ){
    $config->addConfig('news', 'rss_content_length_sidebar', "0", '');
}
if ( !$config->configExists('news', 'rss_content_length_dashboard') ){
    $config->addConfig('news', 'rss_content_length_dashboard', "0", '');
}
if ( !$config->configExists('news', 'rss_url_sidebar') ){
    $config->addConfig('news', 'rss_url_sidebar', "http://news.yahoo.com/rss/science", '');
}
if ( !$config->configExists('news', 'rss_url_dashboard') ){
    $config->addConfig('news', 'rss_url_dashboard', "http://news.yahoo.com/rss/sports", '');
}
if ( !$config->configExists('news', 'rss_cachetime_minuts') ){
    $config->addConfig('news', 'rss_cachetime_minuts', "15", '');
}
if ( !$config->configExists('news', 'rss_items_sidebar') ){
    $config->addConfig('news', 'rss_items_sidebar', "5", '');
}
if ( !$config->configExists('news', 'rss_items_dashboard') ){
    $config->addConfig('news', 'rss_items_dashboard', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather1') ){
    $config->addConfig('news', 'rss_url_ather1', "http://news.yahoo.com/rss/energy", '');
}
if ( !$config->configExists('news', 'rss_url_ather2') ){
    $config->addConfig('news', 'rss_url_ather2', "http://news.yahoo.com/rss/elections-2012", '');
}
if ( !$config->configExists('news', 'rss_url_ather3') ){
    $config->addConfig('news', 'rss_url_ather3', "http://news.yahoo.com/obituaries/", '');
}
if ( !$config->configExists('news', 'rss_items_ather1') ){
    $config->addConfig('news', 'rss_items_ather1', "5", '');
}
if ( !$config->configExists('news', 'rss_items_ather2') ){
    $config->addConfig('news', 'rss_items_ather2', "5", '');
}
if ( !$config->configExists('news', 'rss_items_ather3') ){
    $config->addConfig('news', 'rss_items_ather3', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather4') ){
    $config->addConfig('news', 'rss_url_ather4', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather4') ){
    $config->addConfig('news', 'rss_items_ather4', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather5') ){
    $config->addConfig('news', 'rss_url_ather5', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather5') ){
    $config->addConfig('news', 'rss_items_ather5', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather6') ){
    $config->addConfig('news', 'rss_url_ather6', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather6') ){
    $config->addConfig('news', 'rss_items_ather6', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather7') ){
    $config->addConfig('news', 'rss_url_ather7', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather7') ){
    $config->addConfig('news', 'rss_items_ather7', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather8') ){
    $config->addConfig('news', 'rss_url_ather8', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather8') ){
    $config->addConfig('news', 'rss_items_ather8', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather9') ){
    $config->addConfig('news', 'rss_url_ather9', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather9') ){
    $config->addConfig('news', 'rss_items_ather9', "5", '');
}
if ( !$config->configExists('news', 'rss_url_ather10') ){
    $config->addConfig('news', 'rss_url_ather10', "", '');
}
if ( !$config->configExists('news', 'rss_items_ather10') ){
    $config->addConfig('news', 'rss_items_ather10', "5", '');
}

OW::getNavigation()->addMenuItem(OW_Navigation::MAIN, 'news.index', 'news', 'main_menu_item', OW_Navigation::VISIBLE_FOR_ALL);

/*
$errors = array();

try
{
    $sql = "ALTER TABLE  `".OW_DB_PREFIX."pages` ADD  `is_visibleinmenu` ENUM(  '0',  '1' ) NOT NULL DEFAULT  '1' AFTER  `active` ,
    ADD  `is_localurl` ENUM(  '0',  '1' ) NOT NULL DEFAULT  '0' AFTER  `is_visibleinmenu`";

    Updater::getDbo()->query($sql);
}
catch ( Exception $ex )
{
    $errors[] = $ex;
}

if ( !empty($errors) )
{
//    printVar($errors);
    print_r($errors);
}
*/

