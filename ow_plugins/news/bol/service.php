<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

//NEWS_BOL_Service::getInstance()->make_menu();
//NEWS_BOL_Service::getInstance()->safe_content();

class NEWS_BOL_Service
{
    /**
     * Singleton instance.
     *
     * @var news_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return news_BOL_Service
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    private function __construct()
    {

    }

    public function getDepartmentLabel( $id )
    {
        return OW::getLanguage()->text('news', $this->getDepartmentKey($id));
    }

    public function addDepartment( $email, $label )
    {
        $contact = new NEWS_BOL_Department();
        $contact->email = $email;
        NEWS_BOL_DepartmentDao::getInstance()->save($contact);

        BOL_LanguageService::getInstance()->addValue(
            OW::getLanguage()->getCurrentId(),
            'news',
            $this->getDepartmentKey($contact->id),
            trim($label));
    }
    public function test( $xx )
    {
        return "OK:".$xx;
    }
    public function deleteDepartment( $id )
    {
        $id = (int) $id;
        if ( $id > 0 )
        {
            $key = BOL_LanguageService::getInstance()->findKey('news', $this->getDepartmentKey($id));
            BOL_LanguageService::getInstance()->deleteKey($key->id, true);
            NEWS_BOL_DepartmentDao::getInstance()->deleteById($id);
        }
    }

    private function getDepartmentKey( $name )
    {
        return 'dept_' . trim($name);
    }

    public function getDepartmentList()
    {
        return NEWS_BOL_DepartmentDao::getInstance()->findAll();
    }


    public function make_slider_news()
    {
        $content="";
        $curent_url=OW_URL_HOME;

        $limit="0,10";
        $query = "SELECT * FROM " . OW_DB_PREFIX. "news ns 
    LEFT JOIN " . OW_DB_PREFIX. "news_content descx ON (ns.id=descx.id_news) 
        WHERE ns.active='1' AND ns.is_published='1' ORDER BY ns.data_added DESC LIMIT ".$limit;
//echo $query;exit;
        $arr = OW::getDbo()->queryForList($query);
        $ret="";
        $ret_t="";
        foreach ( $arr as $value ){
            $desc=stripslashes($value['content']);
//            $desc=$this->html2txt($desc);
//            $desc=mb_substr($desc,0,200);

            $desc_s=$this->html2txt($desc);
            $desc_s=mb_substr($desc_s,0,40);



            $title=stripslashes($value['topic_name']);
            $title=$this->html2txt($title);
            $title=mb_substr($title,0,200);
/*
            $ret .="<div style=\"width:100%;display:inline-block;margin:auto;border-bottom:1px dotted #ddd;margin-bottom:10px;\">
                <a href=\"".$curent_url."news/".$value['id']."/index.html\">".$title."</a>
                <div style=\"float:right;\">
                    <a class=\"ow_lbutton\" href=\"".$curent_url."news/".$value['id']."/index.html\">".OW::getLanguage()->text('base', 'more')."</a>
                </div>
                <p style=\"text-align:justify;font-style:italic;\">".$desc."</p>
            </div>";
*/
            $ret .="<div class=\"sp-slide\">
                    <a href=\"".$curent_url."news/".$value['id']."/index.html\">
                    <h1 class=\"sp-layerX\">
                    ".$title."
                    </h1>
                    </a>
                    <p class=\"sp-layerX\">".$desc."</p>


            </div>";
//                <img class=\"sp-image\" src=\"path/to/image1.jpg\"/>

//---------

            if ($title) {
                $ret_t .="<div class=\"sp-thumbnail\">
                    <p class=\"sp-thumbnail-text\">".$title."</p>
                </div>";
            }else{
                $ret_t .="<div class=\"sp-thumbnail\">
                    <p class=\"sp-thumbnail-text\">".$desc_s."</p>
                </div>";
            }


        }

        if ($ret){

            $content .="<div class=\"slider-pro\" id=\"news_pro_slider\">
                <div class=\"sp-slides\">
                    ".$ret."
                </div>

                <div class=\"sp-thumbnails\">
                    ".$ret_t."
                </div>
            </div>";

/*
<div class="sp-thumbnails">
        <img class="sp-thumbnail" src="path/to/blank.gif" data-src="path/to/thumbnail1.jpg" data-retina="path/to/thumbnail1@2x.jpg"/>
        <img class="sp-thumbnail" src="path/to/blank.gif" data-src="path/to/thumbnail2.jpg" data-retina="path/to/thumbnail2@2x.jpg"/>
    </div>



    <div class=\"sp-thumbnails\">
        <img class=\"sp-thumbnail\" src=\"path/to/thumbnail.jpg\"/>

        <p class=\"sp-thumbnail\">Thumbnail 2</p>

        <div class=\"sp-thumbnail\">
            <img class=\"sp-thumbnail-image\" src=\"path/to/thumbnail.jpg\"/>
            <p class=\"sp-thumbnail-text\">Tempor incididunt ut labore et dolore magna</p>
        </div>
    </div>

*/


/*
        $content .="<div class=\"slider-pro\" id=\"my-slider\">
            <div class=\"sp-slides\">

                <!-- Slide 1 -->
                <div class=\"sp-slide\">
                    <img class=\"sp-image\" src=\"path/to/image1.jpg\"/>
                </div>

                <!-- Slide 2 -->
                <div class=\"sp-slide\">
                    <p>Lorem ipsum dolor sit amet</p>
                </div>

                <!-- Slide 3 -->
                <div class=\"sp-slide\">
                    <h3 class=\"sp-layer\">Lorem ipsum dolor sit amet</h3>
                </div>

            </div>

        </div>";
*/
        }

        return $content;
    }

    private function get_image_form_content($content="",$only_one=true)
    {
        preg_match_all('/<img[^>]+>/i',$content, $result);

//print_r($result);
//        $images = array();
        foreach( $result as $image){
            if (is_array($image) AND isset($image[0])){
                preg_match_all('/src="([^"]*)"/i',$image[0], $images);
            }
        }

        if (isset($images) AND is_array($images)){
//print_r($images);exit;
            if ($only_one==true){
//echo $images[1][0];exit;
                if (isset($images[1]) AND isset($images[1][0])){
                    return $images[1][0];
                }else{
                    return "";
                }
            }else{
                if (count($images[0])>0){
                    return $images[0];
                }else{
                    return "";
                }
            }
        }else{
            return "";
        }
    }


    public function make_slider_news_big($full=true)
    {
        $content="";
        $curent_url=OW_URL_HOME;
        $pluginStaticURL2=OW::getPluginManager()->getPlugin('news')->getStaticUrl();


        if ($full==true){
            $limit="0,12";
        }else{
            $limit="0,6";
        }
        $query = "SELECT * FROM " . OW_DB_PREFIX. "news ns 
    LEFT JOIN " . OW_DB_PREFIX. "news_content descx ON (ns.id=descx.id_news) 
        WHERE ns.active='1' AND ns.is_published='1' ORDER BY ns.data_added DESC LIMIT ".$limit;
//echo $query;exit;
        $arr = OW::getDbo()->queryForList($query);
        $ret="";
        $ret_t="";

        $lp=1;
        foreach ( $arr as $value ){
            $desc=stripslashes($value['content']);
//            $desc=$this->html2txt($desc);
//            $desc=mb_substr($desc,0,200);

            $img=$this->get_image_form_content($desc,true);

            $image_is="";
            if (!$img){
                $img=$pluginStaticURL2."no.jpg";
            }



            $desc_s=$this->html2txt($desc);
            $desc_s=mb_substr($desc_s,0,40);

            $title=stripslashes($value['topic_name']);
            $title=$this->html2txt($title);
            $title=mb_substr($title,0,200);

/*
            $ret .="<div class=\"sp-slide\">
                    <a href=\"".$curent_url."news/".$value['id']."/index.html\">
                    <h1 class=\"sp-layerX\">
                    ".$title."
                    </h1>
                    </a>
                    <p class=\"sp-layerX\">".$desc."</p>


            </div>";
//                <img class=\"sp-image\" src=\"path/to/image1.jpg\"/>

//---------

            if ($title) {
                $ret_t .="<div class=\"sp-thumbnail\">
                    <p class=\"sp-thumbnail-text\">".$title."</p>
                </div>";
            }else{
                $ret_t .="<div class=\"sp-thumbnail\">
                    <p class=\"sp-thumbnail-text\">".$desc_s."</p>
                </div>";
            }
*/


            
            if ($full==true AND ($lp==1 OR $lp==4)){
                $class="nt_529x317";
            }else if ($lp==1){
                $class="nt_529x317";
            }else{
                $class="nt_258x152";
            }



            if ($full==true AND 
                (
                    $lp==1 OR $lp==4 OR 
                    $lp==7 OR $lp==8 OR
                    $lp==10 OR $lp==11 OR
                    $lp==13 OR $lp==14
                )
            ) {
                $class .=" marginr13";
            } else if ($lp==1 OR 
                $lp==4 OR $lp==5 OR
                $lp==17 OR $lp==8 OR
                $lp==10 OR $lp==11 
            ){
                $class .=" marginr13";
            }

            $ret_t .="<div class=\"navthumb ".$class."\">
                <div class=\"img\"><a href=\"".$curent_url."news/".$value['id']."/index.html\" title=\"".$title."\"><img src=\"".$img."\" alt=\"".$title."\"></a></div>
                <div class=\"title\"><a href=\"".$curent_url."news/".$value['id']."/index.html\" title=\"".$title."\">".$title."</a></div>
            </div>";
            

            $lp=$lp+1;
        }

        if ($ret_t){

             $content ="<div class=\"clearfix sub_800\">";
             $content .=$ret_t;
             $content .="</div>";
            

        }else{
//             $content ="<div class=\"clearfix\">";
//             $content .=OW::getLanguage()->text('news', 'no_found');
//             $content .="</div>";
             $content ="";
        }

/*
 $content ="<div class=\"sub_800\">

        <div class=\"navthumb nt_529x317 marginr13\">
            <div class=\"img\"><a href=\"wiadomosc,23552,Koscierzyna-Bajkowy-Festyn-Rodzinny-za-nami.html\" title=\"Kościerzyna. Bajkowy Festyn Rodzinny za nami\"><img src=\"http://koscierski.info/images/news/152505.jpg\" alt=\"Kościerzyna. Bajkowy Festyn Rodzinny za nami\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23552,Koscierzyna-Bajkowy-Festyn-Rodzinny-za-nami.html\" title=\"Kościerzyna. Bajkowy Festyn Rodzinny za nami\">Kościerzyna. Bajkowy Festyn Rodzinny za nami</a></div>
        </div>

        <div class=\"navthumb nt_258x152\">
            <div class=\"img\"><a href=\"wiadomosc,23551,Zaginela-82-latka-Kobiete-odnalezli-policjanci.html\" title=\"Zaginęła 82-latka. Kobietę odnaleźli policjanci\"><img src=\"http://koscierski.info/images/news/152504.jpg\" alt=\"Zaginęła 82-latka. Kobietę odnaleźli policjanci\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23551,Zaginela-82-latka-Kobiete-odnalezli-policjanci.html\" title=\"Zaginęła 82-latka. Kobietę odnaleźli policjanci\">Zaginęła 82-latka. Kobietę odnaleźli policjanci </a></div>
        </div>

        <div class=\"navthumb nt_258x152\">
            <div class=\"img\"><a href=\"wiadomosc,23550,Wlaczala-sie-do-ruchu-nie-ustapila-pierwszenstwa-i-spowodowala-wypadek.html\" title=\"Włączała się do ruchu, nie ustąpiła pierwszeństwa i spowodowała wypadek\"><img src=\"http://koscierski.info/images/news/152503.jpg\" alt=\"Włączała się do ruchu, nie ustąpiła pierwszeństwa i spowodowała wypadek\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23550,Wlaczala-sie-do-ruchu-nie-ustapila-pierwszenstwa-i-spowodowala-wypadek.html\" title=\"Włączała się do ruchu, nie ustąpiła pierwszeństwa i spowodowała wypadek\">Włączała się do ruchu, nie ustąpiła pierwszeństwa i spowodowała wypadek</a></div>
        </div>
//----
        <div class=\"navthumb nt_529x317 marginr13\">
            <div class=\"img\"><a href=\"wiadomosc,23548,Nowy-Klincz-Rozbil-auto-na-drzewie-stracil-prawo-jazdy.html\" title=\"Nowy Klincz. Rozbił auto na drzewie - stracił prawo jazdy\"><img src=\"http://koscierski.info/images/news/152501.jpg\" alt=\"Nowy Klincz. Rozbił auto na drzewie - stracił prawo jazdy\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23548,Nowy-Klincz-Rozbil-auto-na-drzewie-stracil-prawo-jazdy.html\" title=\"Nowy Klincz. Rozbił auto na drzewie - stracił prawo jazdy\">Nowy Klincz. Rozbił auto na drzewie - stracił prawo jazdy</a></div>
        </div>

        <div class=\"navthumb nt_258x152\">
            <div class=\"img\"><a href=\"wiadomosc,23549,Balachy-Dachowal-fiat-kierowca-byl-w-stanie-po-spozyciu.html\" title=\"Bałachy. Dachował fiat - kierowca był w stanie po spożyciu\"><img src=\"http://koscierski.info/images/news/152502.jpg\" alt=\"Bałachy. Dachował fiat - kierowca był w stanie po spożyciu\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23549,Balachy-Dachowal-fiat-kierowca-byl-w-stanie-po-spozyciu.html\" title=\"Bałachy. Dachował fiat - kierowca był w stanie po spożyciu\">Bałachy. Dachował fiat - kierowca był w stanie po spożyciu </a></div>
        </div>

        <div class=\"navthumb nt_258x152\">
            <div class=\"img\"><a href=\"wiadomosc,23545,Pilka-reczna-Strat-Elblag-lepszy-od-UKS-PCM-Koscierzyna.html\" title=\"Piłka ręczna. Strat Elbląg lepszy od UKS PCM Kościerzyna\"><img src=\"http://koscierski.info/images/news/152378.jpg\" alt=\"Piłka ręczna. Strat Elbląg lepszy od UKS PCM Kościerzyna\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23545,Pilka-reczna-Strat-Elblag-lepszy-od-UKS-PCM-Koscierzyna.html\" title=\"Piłka ręczna. Strat Elbląg lepszy od UKS PCM Kościerzyna\">Piłka ręczna. Strat Elbląg lepszy od UKS PCM Kościerzyna</a></div>
        </div>
//----

        <div class=\"navthumb nt_258x152 marginr13\">
            <div class=\"img\"><a href=\"wiadomosc,23540,Koscierzyna-II-Festiwal-Zurawiny.html\" title=\"Kościerzyna. II Festiwal Żurawiny\"><img src=\"http://koscierski.info/images/news/152190.jpg\" alt=\"Kościerzyna. II Festiwal Żurawiny\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23540,Koscierzyna-II-Festiwal-Zurawiny.html\" title=\"Kościerzyna. II Festiwal Żurawiny\">Kościerzyna. II Festiwal Żurawiny</a></div>
        </div>

        <div class=\"navthumb nt_258x152 marginr13\">
            <div class=\"img\"><a href=\"wiadomosc,23537,Koscierzyna-Ruszylo-glosowanie-na-budzet-obywatelski-2016.html\" title=\"Kościerzyna. Ruszyło głosowanie na budżet obywatelski 2016\"><img src=\"http://koscierski.info/images/news/152175.jpg\" alt=\"Kościerzyna. Ruszyło głosowanie na budżet obywatelski 2016\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23537,Koscierzyna-Ruszylo-glosowanie-na-budzet-obywatelski-2016.html\" title=\"Kościerzyna. Ruszyło głosowanie na budżet obywatelski 2016\">Kościerzyna. Ruszyło głosowanie na budżet obywatelski 2016</a></div>
        </div>

        <div class=\"navthumb nt_258x152\">
            <div class=\"img\"><a href=\"wiadomosc,23534,Weekend-z-Festiwalem-Zurawiny-i-Bajkowym-Festynem-Rodzinnym.html\" title=\"Weekend z Festiwalem Żurawiny i Bajkowym Festynem Rodzinnym\"><img src=\"http://koscierski.info/images/news/152158.jpg\" alt=\"Weekend z Festiwalem Żurawiny i Bajkowym Festynem Rodzinnym\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23534,Weekend-z-Festiwalem-Zurawiny-i-Bajkowym-Festynem-Rodzinnym.html\" title=\"Weekend z Festiwalem Żurawiny i Bajkowym Festynem Rodzinnym\">Weekend z Festiwalem Żurawiny i Bajkowym Festynem Rodzinnym</a></div>
        </div>

        <div class=\"navthumb nt_258x152 marginr13\">
            <div class=\"img\"><a href=\"wiadomosc,23532,Koscierzyna-Mial-dwa-promile-bil-zone-i-rzucil-sie-na-policjantow.html\" title=\"Kościerzyna. Miał dwa promile, bił żonę i rzucił się na policjantów\"><img src=\"http://koscierski.info/images/news/152156.jpg\" alt=\"Kościerzyna. Miał dwa promile, bił żonę i rzucił się na policjantów\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23532,Koscierzyna-Mial-dwa-promile-bil-zone-i-rzucil-sie-na-policjantow.html\" title=\"Kościerzyna. Miał dwa promile, bił żonę i rzucił się na policjantów\">Kościerzyna. Miał dwa promile, bił żonę i rzucił się na policjantów</a></div>
        </div>

        <div class=\"navthumb nt_258x152 marginr13\">
            <div class=\"img\"><a href=\"wiadomosc,23529,Liniewo-Kierownik-Posterunku-Policji-odszedl-na-emeryture.html\" title=\"Liniewo. Kierownik Posterunku Policji odszedł na emeryturę\"><img src=\"http://koscierski.info/images/news/152153.jpg\" alt=\"Liniewo. Kierownik Posterunku Policji odszedł na emeryturę\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23529,Liniewo-Kierownik-Posterunku-Policji-odszedl-na-emeryture.html\" title=\"Liniewo. Kierownik Posterunku Policji odszedł na emeryturę\">Liniewo. Kierownik Posterunku Policji odszedł na emeryturę</a></div>
        </div>

        <div class=\"navthumb nt_258x152\">
            <div class=\"img\"><a href=\"wiadomosc,23528,Muzyka-na-zywo-pokazy-i-konkursy-czyli-piknik-rodzinny-w-Koscierzynie.html\" title=\"Muzyka na żywo, pokazy i konkursy, czyli piknik rodzinny w Kościerzynie\"><img src=\"http://koscierski.info/images/news/152132.jpg\" alt=\"Muzyka na żywo, pokazy i konkursy, czyli piknik rodzinny w Kościerzynie\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23528,Muzyka-na-zywo-pokazy-i-konkursy-czyli-piknik-rodzinny-w-Koscierzynie.html\" title=\"Muzyka na żywo, pokazy i konkursy, czyli piknik rodzinny w Kościerzynie\">Muzyka na żywo, pokazy i konkursy, czyli piknik rodzinny w Kościerzynie</a></div>
        </div>

        <div class=\"navthumb nt_258x152 marginr13\">
            <div class=\"img\"><a href=\"wiadomosc,23527,Koscierzyna-Policjanci-z-wizyta-w-Nowym-Klinczu.html\" title=\"Kościerzyna. Policjanci z wizytą w Nowym Klinczu\"><img src=\"http://koscierski.info/images/news/152127.jpg\" alt=\"Kościerzyna. Policjanci z wizytą w Nowym Klinczu\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23527,Koscierzyna-Policjanci-z-wizyta-w-Nowym-Klinczu.html\" title=\"Kościerzyna. Policjanci z wizytą w Nowym Klinczu\">Kościerzyna. Policjanci z wizytą w Nowym Klinczu</a></div>
        </div>

        <div class=\"navthumb nt_258x152 marginr13\">
            <div class=\"img\"><a href=\"wiadomosc,23520,Koscierzyna-Nowy-pasterz-w-Parafii-Swietej-Trojcy.html\" title=\"Kościerzyna. Nowy pasterz w Parafii Świętej Trójcy\"><img src=\"http://koscierski.info/images/news/152074.jpg\" alt=\"Kościerzyna. Nowy pasterz w Parafii Świętej Trójcy\"></a></div>
            <div class=\"title\"><a href=\"wiadomosc,23520,Koscierzyna-Nowy-pasterz-w-Parafii-Swietej-Trojcy.html\" title=\"Kościerzyna. Nowy pasterz w Parafii Świętej Trójcy\">Kościerzyna. Nowy pasterz w Parafii Świętej Trójcy</a></div>
        </div>

        <div class=\"navthumb nt_258x152\">
            <div class=\"img\"><a href=\"wiadomosc,23502,5-sygnalow-ze-to-juz-odpowiedni-czas-na-zmiane-konta-w-banku-Znasz-je.html\" title=\"5 sygnałów, że to już odpowiedni czas na zmianę konta w banku. Znasz je?\"><img src=\"http://koscierski.info/images/news/151943.jpg\" alt=\"5 sygnałów, że to już odpowiedni czas na zmianę konta w banku. Znasz je?\"></a></div>
            <div class=\"title\"><span class=\"subtitle\">SPONSOROWANY</span><br><a href=\"wiadomosc,23502,5-sygnalow-ze-to-juz-odpowiedni-czas-na-zmiane-konta-w-banku-Znasz-je.html\" title=\"5 sygnałów, że to już odpowiedni czas na zmianę konta w banku. Znasz je?\">5 sygnałów, że to już odpowiedni czas na zmianę konta w banku. Znasz je?</a></div>
        </div>
 </div>
";
*/


        return $content;
    }


    public function make_for_widget_news()
    {
        $ret="";
        $curent_url=OW_URL_HOME;
        $limit="0,10";
        $query = "SELECT * FROM " . OW_DB_PREFIX. "news ns 
    LEFT JOIN " . OW_DB_PREFIX. "news_content descx ON (ns.id=descx.id_news) 
        WHERE ns.active='1' AND ns.is_published='1' ORDER BY ns.data_added DESC LIMIT ".$limit;
//echo $query;exit;
        $arr = OW::getDbo()->queryForList($query);
        $items="";
        foreach ( $arr as $value ){
            $desc=stripslashes($value['content']);
            $desc=$this->html2txt($desc);
            $desc=mb_substr($desc,0,200);
            $title=stripslashes($value['topic_name']);
            $title=$this->html2txt($title);
            $title=mb_substr($title,0,200);
            $items .="<div style=\"width:100%;display:inline-block;margin:auto;border-bottom:1px dotted #ddd;margin-bottom:10px;\">
                <a href=\"".$curent_url."news/".$value['id']."/index.html\">".$title."</a>
                <div style=\"float:right;\">
                    <a class=\"ow_lbutton\" href=\"".$curent_url."news/".$value['id']."/index.html\">".OW::getLanguage()->text('base', 'more')."</a>
                </div>
                <p style=\"text-align:justify;font-style:italic;\">".$desc."</p>
            </div>";
        }
        $ret .="<div class=\"ow_box_empty ow_stdmargin clearfix index-NEWS_CMP_IndexWidget ow_break_word\" style=\"\">";
        $ret .=$items;
        $ret .="</div>";
        return $ret;
    }



















    var $tags = array();
    var $output = array();
    var $retval = "";
    var $data = array();


    public function startRSSreader($file)
    {

        $this->tags = array();
        $this->output =array();
        $this->retval = "";
        $this->data = array();
/*
      $xml_parser = xml_parser_create("");
      xml_set_object($xml_parser, $this);
      xml_set_element_handler($xml_parser, "startElement", "endElement");
      xml_set_character_data_handler($xml_parser, "parseData");
*/
/*
      $fp = @fopen($file, "r");// or die("<b>myRSSParser:</b> Could not open $file for input");
      while($data = fread($fp, 4096)) {
        xml_parse($xml_parser, $data, feof($fp));
      }
      fclose($fp);
*/
//        $data=$this->get_file_cache($file);
//$file="http://news.yahoo.com/rss/elections-2012";
        $this->data=$this->get_file_cache($file);
//echo substr($this->data,1);exit;
if (substr($this->data,0,5)!="<?rss" AND substr($this->data,0,5)!="<?xml"){
    $this->data="";
}
//print_r($this->data);exit;

/*
        xml_parse($xml_parser, $data, true);
      xml_parser_free($xml_parser);
*/
    }

























    public function startElement($parser, $tagname, $attrs=array())
    {
      // RSS 2.0
      if($tagname == "ENCLOSURE" && $attrs) {
        $this->startElement($parser, "ENCLOSURE");
        foreach($attrs as $attr => $attrval) {
          $this->startElement($parser, $attr);
          $this->parseData($parser, $attrval);
          $this->endElement($parser, $attr);
        }
        $this->endElement($parser, "ENCLOSURE");
      }

      // Yahoo! Media RSS
      if (!isset($attrs['MEDIUM'])) $attrs['MEDIUM']="";
      if($tagname == "MEDIA:CONTENT" && $attrs['URL'] && $attrs['MEDIUM'] == 'image') {
        $this->startElement($parser, "IMAGE");
        $this->parseData($parser, $attrs['URL']);
        $this->endElement($parser, "IMAGE");
      }

      if(preg_match("/^(RDF|RSS|CHANNEL|IMAGE|ITEM)/", $tagname)) {
        if($this->tags) {
          $depth = count($this->tags);
          $tmp = end($this->tags);
          list($parent, $num) = each($tmp);
          if($parent) {
            if (!isset($this->tags[$depth-1][$parent][$tagname])) $this->tags[$depth-1][$parent][$tagname]=1;
                else $this->tags[$depth-1][$parent][$tagname]++;
            }
        }
        array_push($this->tags, array($tagname => array()));
      } else {
        if(!preg_match("/^(A|B|I)$/", $tagname)) {
          // add tag to tags array
          array_push($this->tags, $tagname);
        }
      }
    }

    public function endElement($parser, $tagname)
    {
      if(!preg_match("/^(A|B|I)$/", $tagname)) {
        // remove tag from tags array
        array_pop($this->tags);
      }
    }

    public function parseData($parser, $data)
    {
      // return if data contains no text
      if(!trim($data)) return;

      $evalcode = "\$this->output";
      foreach($this->tags as $tag) {
        if(is_array($tag)) {
          list($tagname, $indexes) = each($tag);
          $evalcode .= "[\"$tagname\"]";
          if(isset(${$tagname})) $evalcode .= "[" . (${$tagname} - 1) . "]";
          if($indexes) extract($indexes);
        } else {
          if(preg_match("/^([A-Z]+):([A-Z]+)$/", $tag, $matches)) {
            $evalcode .= "[\"$matches[1]\"][\"$matches[2]\"]";
          } else {
            $evalcode .= "[\"$tag\"]";
          }
        }
      }
//    if (!isset($evalcode)) $evalcode="";
//      eval("if (!isset($evalcode)) $evalcode=''; $evalcode = $evalcode . '" . addslashes($data) . "';");
      eval("if (!isset($evalcode)) $evalcode=''; $evalcode = $evalcode . '" . addslashes($data) . "';");
//        if (!isset($evalcode)) $evalcode=''; 
//        $evalcode .=addslashes($data);
    }

    public function display_channel($data, $limit,$type,$rss_pos,$length_title=50,$length_content=0)
    {
//echo $length_content;
//print_r($data);exit;
      extract($data);
      if($IMAGE) {

        // display channel image(s)
//        foreach($IMAGE as $image) $this->display_image($image);
      }
      if($TITLE) {
/*
        // display channel information
        $this->retval .= "<h1>";
        if($LINK) $this->retval .= "<a href=\"$LINK\" target=\"_blank\">";
        $this->retval .= stripslashes($TITLE);
        if($LINK) $this->retval .= "</a>";
        $this->retval .= "</h1>\n";
        if($DESCRIPTION) $this->retval .= "<p>$DESCRIPTION</p>\n\n";
        $tmp = array();
        if($PUBDATE) $tmp[] = "<small>Published: $PUBDATE</small>";
        if($COPYRIGHT) $tmp[] = "<small>Copyright: $COPYRIGHT</small>";
        if($tmp) $this->retval .= "<p>" . implode("<br>\n", $tmp) . "</p>\n\n";
        $this->retval .= "<div class=\"divider\"><!-- --></div>\n\n";
*/
      }
      if($ITEM) {
//echo $limit;exit;
//print_r($data);exit;
        $position=0;
        foreach($ITEM as $item) {
//print_r($item);exit;
//echo $rss_pos."--".$position;exit;
                if ($type=="short"){
                    $this->display_item_short($item, "CHANNEL",$limit,$rss_pos,$position,$type,$length_title,$length_content);
                }else if ($type=="widget"){
//                         display_item_short_widget($data, $parent,  $limit,$rss_pos,$position=0,$type="",$length_title=50,$length_content=0)
                    $this->display_item_short_widget($item, "CHANNEL",$limit,$rss_pos,$position  ,$type   ,$length_title   ,$length_content);
                }else{
                    $this->display_item($item, "CHANNEL",$limit,$rss_pos,$position,$type,$length_title,$length_content);
                }
        $position++;
//$limit=44;
//echo "--".is_int($limit)."--".$limit;exit;
          if(is_int($limit)){
                $limit=$limit-1;
                if ($limit<0) break;
            }
        }
      }
    }

    public function display_channel_zoom($data, $limit,$type,$rss_id,$rss_pos)
    {
//print_r($data);exit;
      extract($data);

      if($ITEM) {
//echo $limit;exit;
//print_r($data);exit;
        $position=0;
//$rss_id,$rss_pos
        foreach($ITEM as $item) {
//echo $position."----".$rss_pos;
            if ($position==$rss_pos){
//echo "ssss";exit;
              $this->display_item_short_zoo($item, "CHANNEL",$limit,$rss_pos,$position);
                break;
            }
            $position++;
            if ($position>$rss_pos){
                break;
            }
        }
//echo "---";exit;
      }

      if($IMAGE) {
        // display channel image(s)
        foreach($IMAGE as $image) $this->display_image($image);
      }
      if($TITLE) {

        // display channel information
        $this->retval .= "<h1 style=\"font-size:12px;font-style:italic;\">";
        if($LINK) $this->retval .= "<a href=\"$LINK\" target=\"_blank\" rel=\"nofollow\" >";
        $this->retval .= stripslashes($TITLE);
        if($LINK) $this->retval .= "</a>";
        $this->retval .= "</h1>\n";

        

        if($DESCRIPTION) {
            $desc=$DESCRIPTION;
            $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:right;">',$desc);
            $this->retval .= "<p style=\"font-size:12px;font-style:italic;\">".$desc."</p>\n\n";
        }
        $tmp = array();
        if($PUBDATE) $tmp[] = "<small style=\"font-size:10px;font-style:italic;color:#888;\">Published: $PUBDATE</small>";
        if($COPYRIGHT) $tmp[] = "<small style=\"font-size:10px;font-style:italic;color:#888;\">Copyright: $COPYRIGHT</small>";
        if($tmp) $this->retval .= "<p style=\"text-align:right;\">" . implode("<br>\n", $tmp) . "</p>\n\n";
        $this->retval .= "<div class=\"divider\"><!-- --></div>\n\n";

      }

    }

    // display a single image as HTML
    public function display_image($data, $parent="")
    {
      extract($data);
      if(!$URL) return;
if (!isset($WIDTH)) $WIDTH="";
if (!isset($HEIGHT)) $HEIGHT="";
if (!isset($LINK)) $LINK="";
      $this->retval .= "<p>";
      if($LINK) $this->retval .= "<a href=\"$LINK\" target=\"_blank\" rel=\"nofollow\" >";
      $this->retval .= "<img src=\"$URL\"";
      if($WIDTH && $HEIGHT) $this->retval .= " width=\"$WIDTH\" height=\"$HEIGHT\"";
//        $this->retval .= " width=\"100px\" height=\"$HEIGHT\"";
//        $this->retval .= " width=\"100px\" ";
        $this->retval .= " align=\"left\" ";
    $this->retval .= " style=\"float:left;\" ";
      $this->retval .= " border=\"0\" alt=\"$TITLE\">";
      if($LINK) $this->retval .= "</a>";
      $this->retval .= "</p>\n\n";
    }

//              display_item_short($data, $parent,$limit,$rss_pos,$position=0,$type="",$length_title=50,$length_content=0)
    public function display_item($data, $parent,$limit,$rss_pos,$position=0,$type="",$length_title=50,$length_content=0)
    {
      extract($data);
      if(!$TITLE) return;


        $title_url=$this->make_seo_url(stripslashes($TITLE),100);
        if (!$title_url) $title_url="index.html";
            else $title_url .=".html";



                $this->retval .="<div class=\"ow_ipc_header\" style=\"height:20px;\">"; 
                $this->retval .="<h4 style=\"margin-left:10px;margin-top:1px;\">";
//                $this->retval .=strtoupper("Sample title 1...");
      $this->retval .=  "<b>";
//      if($LINK) $this->retval .=  "<a href=\"$LINK\" target=\"_blank\">";
//      $this->retval .=  "<a href=\"".OW_URL_HOME."news/1_0/mikowska-bruczko_nowym_prezesem_widzewa.html\" target=\"_blank\">";
//      $this->retval .=  "<a href=\"".OW_URL_HOME."news/".$rss_pos."_".$position."/".$title_url."\" target=\"_blank\">";
      $this->retval .=  "<a href=\"".OW_URL_HOME."news/".$rss_pos."_".$position."/".$title_url."\" >";
      $this->retval .= stripslashes($TITLE);
//      if($LINK) $this->retval .= "</a>";
      $this->retval .= "</a>";
      $this->retval .=  "</b>";


      if(!$PUBDATE && $DC["DATE"]) $PUBDATE = $DC["DATE"];
      if($PUBDATE) {
        $this->retval .=  "<div style=\"float:right;font-size:8px;font-weight:normal;\">";
            $this->retval .= " <small>($PUBDATE)</small>";
        $this->retval .=  "</div>";
      }
                $this->retval .="</h4>";
                $this->retval .="</div>";





                $this->retval .="<div class=\"ow_ipc_content\" style=\"margin-bottom:20px;width:100%;display:inline-block;\">";
//                $this->retval .="Sample content";

if (!isset($CONTENT['ENCODED'])) $CONTENT['ENCODED']="";
if (!isset($DESCRIPTION)) $DESCRIPTION="";
if (!isset($IMAGE)) $IMAGE="";
if (!isset($ENCLOSURE)) $ENCLOSURE="";
if (!isset($COMMENTS)) $COMMENTS="";

      if($CONTENT['ENCODED']) {
        $this->retval .= "<p>" . stripslashes($CONTENT['ENCODED']) . "</p>\n";
      } elseif($DESCRIPTION) {
        if($IMAGE) {
//          foreach($IMAGE as $IMG) $this->retval .= "<img src=\"$IMG\" style=\"margin-right:10px;\">\n";
//          foreach($IMAGE as $IMG) $this->retval .= "<img src=\"$IMG\" width=\"100px\">\n";
        }
        $desc=stripslashes($DESCRIPTION);
//$this->retval .= "<textarea>".$desc."</textarea>";;
//        $desc = preg_replace("/<img[^>]+\>/i", " ", $desc);

        $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)\/>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:left;">',$desc);
        $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:left;">',$desc);
//        $desc=preg_replace('/<table(.*?)table>/','xxx',$desc);
//        $desc=preg_replace('/<table(.*?)>/','<table style="width:auto;margin:auto;"$1>',$desc);
//        $max_width=250;
//        $max_height=250;
        $max_width=130;
        $max_height=150;
        if($ENCLOSURE) {
            $desc = preg_replace('/<img(.*?)src="(.*?)"(.*?)>/i', '<a href=\"{'.$ENCLOSURE['URL'].'}\"><img src="$2" align="left" style="float:left;max-width:'.$max_width.'px;max-height:'.$max_height.'px;margin-right:10px;margin-bottom:5px;" title="'.stripslashes($TITLE).'"></a>', $desc);
        }else{
            $desc = preg_replace('/<img(.*?)src="(.*?)"(.*?)>/i', '<img src="$2" align="left" style="float:left;max-width:'.$max_width.'px;max-height:'.$max_height.'px;margin-right:10px;margin-bottom:5px;" title="'.stripslashes($TITLE).'">', $desc);
        }

//        $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:left;">',$desc);

//        $this->retval .=  "<p>" . $desc . "</p>\n\n";
        $this->retval .= "<p style=\"text-align:justify;margin-right:10px;\">".$desc . "</p>\n";
//        $this->retval .= "<span style=\"text-align:justify;margin-right:10px;\">".$desc . "</span>\n";

//        $this->retval .=  "<p>" . stripslashes($DESCRIPTION) . "</p>\n\n";
      }
/*
      // RSS 2.0 
      if($ENCLOSURE) {
        $this->retval .= "<p><small><b>Media:</b> <a href=\"{$ENCLOSURE['URL']}\">";
        $this->retval .= $ENCLOSURE['TYPE'];
        $this->retval .= "</a> ({$ENCLOSURE['LENGTH']} bytes)</small></p>\n\n";
      }
*/

      if($COMMENTS) {
        $this->retval .= "<p style=\"text-align: right;\"><small>";
        $this->retval .= "<a href=\"$COMMENTS\">Comments</a>";
        $this->retval .= "</small></p>\n\n";
      }


                $this->retval .="</div>";



    }

    // display a single item as HTML
    public function display_item_short($data, $parent,$limit,$rss_pos,$position=0,$type="",$length_title=50,$length_content=0)
    {
      extract($data);
      if(!$TITLE) return;

      $this->retval .=  "<div style=\"width:100%;display:block;margin:auto;border-bottom:1px dotted #ddd;margin-bottom:10px;\">";
//$this->retval .= $limit.") ";
      // RSS 2.0
      if(isset($ENCLOSURE)) {
        if ($ENCLOSURE['TYPE']=="image/jpeg"){
            $this->retval .= "<img src=\"{$ENCLOSURE['URL']}\" style=\"max-width:45px;max-height:45px;margin-right:10px;margin-borrom:10px;\" align=\"left\">";
        }
      }

//      if($LINK) $this->retval .=  "<a href=\"$LINK\" target=\"_blank\">";
//        $length_title=OW::getConfig()->getValue('news', 'rss_title_length_main');
//echo $length_title;exit;
        if (!$length_title) $length_title=55;
        $text=mb_substr(stripslashes($TITLE),0,$length_title);
        if (mb_strlen(stripslashes($TITLE))>$length_title) $text .="..";
        $this->retval .="<a href=\"".$LINK."\" target=\"_blank\">";
          $this->retval .= $text;
        $this->retval .="</a>";
//      if($LINK) $this->retval .= "</a>";
//          $this->retval .=  "</b>";
//          if(!$PUBDATE && $DC["DATE"]) $PUBDATE = $DC["DATE"];
//          if($PUBDATE) $this->retval .= "<br/><small style=\"font-size:8px;\">($PUBDATE)</small>";
//print_r($data);exit;
/*
Array ( 
[TITLE] => Apple Breach DISPUTED by FBI and 4 Hot Stocks Moving the Market Mid-Week 
[LINK] => http://us.rd.yahoo.com/finance/external/xwscheats/rss/SIG=13j7clrve/*http://wallstcheatsheet.com/stocks/apple-breach-disputed-by-fbi-and-4-hot-stocks-moving-the-market-mid-week.html/?ref=YF 
[GUID] => yahoo_finance/2433034744 
[PUBDATE] => Wed, 05 Sep 2012 15:21:44 GMT 
)
*/
        $title_url=$this->make_seo_url(stripslashes($TITLE),100);
        if (!$title_url) $title_url="index.html";
            else $title_url .=".html";
        $this->retval .="<div style=\"float:right;\">";
//        $this->retval .="<a class=\"ow_lbutton\" href=\"".OW_URL_HOME."news-show/".$rss_pos."_".$position."\">";
        $this->retval .="<a class=\"ow_lbutton\" href=\"".OW_URL_HOME."news/".$rss_pos."_".$position."/".$title_url."\">";
//        $this->retval .="<a class=\"ow_lbutton\" href=\"".$LINK."\" target=\"_blank\">";
        $this->retval .=OW::getLanguage()->text('news', 'read_more');
        $this->retval .="</a>";
        $this->retval .="</div>";


//      $this->retval .=  "</p>\n";

/*
      // use feed-formatted HTML if provided
      if($CONTENT['ENCODED']) {
        $this->retval .= "<p>" . stripslashes($CONTENT['ENCODED']) . "</p>\n";
      } else if($DESCRIPTION) {
//        if($IMAGE) {
//          foreach($IMAGE as $IMG) $this->retval .= "<img src=\"$IMG\">\n";
//        }
        $desc=stripslashes($DESCRIPTION);
        $desc = preg_replace("/<img[^>]+\>/i", " ", $desc); 
//        $this->retval .=  "<p>" . $desc . "</p>\n\n";
        $this->retval .=  $desc . "\n";
      }
*/

/*
      // RSS 2.0 - ENCLOSURE
      if($ENCLOSURE) {
        $this->retval .= "<p><small><b>Media:</b> <a href=\"{$ENCLOSURE['URL']}\">";
        $this->retval .= $ENCLOSURE['TYPE'];
        $this->retval .= "</a> ({$ENCLOSURE['LENGTH']} bytes)</small></p>\n\n";
      }
*/
/*
      if($COMMENTS) {
//        $this->retval .= "<p style=\"text-align: right;\"><small>";
//        $this->retval .= "<a href=\"$COMMENTS\">Comments</a>";
//        $this->retval .= "</small></p>\n\n";
        $this->retval .= "<p style=\"text-align: right;\"><small>";
        $this->retval .= "<a href=\"$COMMENTS\">Comments</a>";
        $this->retval .= "</small></p>\n";
      }
*/
     $this->retval .=  "</div>";
    }


    // display a single item as HTML
    public function display_item_short_widget($data, $parent,$limit,$rss_pos,$position=0,$type="",$length_title=50,$length_content=0)
    {
      extract($data);
//print_r($data);
      if(!$TITLE) return;
//print_r($data);exit;
//      if($LINK) $this->retval .=  "<a href=\"$LINK\" target=\"_blank\">";
//        $length_title=OW::getConfig()->getValue('news', 'rss_title_length_main');
//echo $length_title;exit;
        if (!$length_title) $length_title=55;
        $text=mb_substr(stripslashes($TITLE),0,$length_title);
        if (mb_strlen(stripslashes($TITLE))>$length_title) $text .="..";

//        $length_content=296;

        $title_url=$this->make_seo_url(stripslashes($TITLE),100);
        if (!$title_url) $title_url="index.html";
            else $title_url .=".html";

    $is_image=false;
      $this->retval .=  "<div style=\"width:100%;display:inline-block;margin:auto;border-bottom:1px dotted #ddd;margin-bottom:10px;\">";
//$this->retval .= $limit.") ";
//echo "<br>".$ENCLOSURE['URL']."<br>";
      // RSS 2.0
      if(isset($ENCLOSURE)) {
//echo $ENCLOSURE['URL']."----".$ENCLOSURE['TYPE']."<br>";
        if ($ENCLOSURE['TYPE']=="image/jpeg"){
            $this->retval .= "<img src=\"{$ENCLOSURE['URL']}\" style=\"max-width:80px;max-height:45px;margin-right:10px;margin-bottom:5px;\" align=\"left\" title=\"".$TITLE."\">";
    $is_image=true;
        }else{
//            $this->retval .= "<div style=\"display:inline-block;width:80px;height:45px;;margin-right:10px;margin-bottom:5px;  float:left;\">&nbsp;</div>";
        }
      }else{
//            $this->retval .= "<div style=\"display:inline-block;width:80px;height:45px;;margin-right:10px;margin-bottom:5px;  float:left;\">&nbsp;</div>";
      }



//        $this->retval .="<a href=\"".$LINK."\" target=\"_blank\">";
        $this->retval .="<a href=\"".OW_URL_HOME."news/".$rss_pos."_".$position."/".$title_url."\">";
          $this->retval .= $text;
        $this->retval .="</a>";
//      if($LINK) $this->retval .= "</a>";
//          $this->retval .=  "</b>";
//          if(!$PUBDATE && $DC["DATE"]) $PUBDATE = $DC["DATE"];
//          if($PUBDATE) $this->retval .= "<br/><small style=\"font-size:8px;\">($PUBDATE)</small>";
//print_r($data);exit;
/*
Array ( 
[TITLE] => Apple Breach DISPUTED by FBI and 4 Hot Stocks Moving the Market Mid-Week 
[LINK] => http://us.rd.yahoo.com/finance/external/xwscheats/rss/SIG=13j7clrve/*http://wallstcheatsheet.com/stocks/apple-breach-disputed-by-fbi-and-4-hot-stocks-moving-the-market-mid-week.html/?ref=YF 
[GUID] => yahoo_finance/2433034744 
[PUBDATE] => Wed, 05 Sep 2012 15:21:44 GMT 
)
*/
//echo $rss_pos."--".$position;exit;
        $this->retval .="<div style=\"float:right;\">";
//        $this->retval .="<a class=\"ow_lbutton\" href=\"".OW_URL_HOME."news-show/".$rss_pos."_".$position."\">";
        $this->retval .="<a class=\"ow_lbutton\" href=\"".OW_URL_HOME."news/".$rss_pos."_".$position."/".$title_url."\">";
//        $this->retval .="<a class=\"ow_lbutton\" href=\"".$LINK."\" target=\"_blank\">";
        $this->retval .=OW::getLanguage()->text('news', 'read_more');
        $this->retval .="</a>";
        $this->retval .="</div>";



//      $this->retval .=  "</p>\n";


if (!isset($CONTENT['ENCODED'])) $CONTENT['ENCODED']="";
if (!isset($DESCRIPTION)) $DESCRIPTION="";
      // use feed-formatted HTML if provided
      if($CONTENT['ENCODED']) {
        $this->retval .= "<p>" . stripslashes($CONTENT['ENCODED']) . "</p>\n";
      } else if($DESCRIPTION) {
//        if($IMAGE) {
//          foreach($IMAGE as $IMG) $this->retval .= "<img src=\"$IMG\">\n";
//        }
        $desc=stripslashes($DESCRIPTION);
//        $desc=$DESCRIPTION;
        if ($is_image==true){
            $desc=preg_replace("/<img[^>]+\>/i", " ", $desc); 
        }else{
            $desc= preg_replace('#<img(.*?)src="([^"]*/)?(([^"/]*)\.[^"]*)"([^>]*?)>#', '<img src="$2$3" style="float:left;max-width:130px; margin-right:10px;">', $desc);  
        }

        $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:right;">',$desc);        
//echo $length_content;
        if (!$length_content) {
            $length_content=0;  
            $desc="";
        }else{
//            $desc=mb_substr($desc,0,$length_content).$descend;
            $desc=$this->cropText($desc,$length_content);
        }
//echo $length_content;exit;
        $this->retval .= "<p style=\"text-align:justify;font-style:italic;\">".$desc . "</p>\n";
      }




/*
      // RSS 2.0 - ENCLOSURE
      if($ENCLOSURE) {
        $this->retval .= "<p><small><b>Media:</b> <a href=\"{$ENCLOSURE['URL']}\">";
        $this->retval .= $ENCLOSURE['TYPE'];
        $this->retval .= "</a> ({$ENCLOSURE['LENGTH']} bytes)</small></p>\n\n";
      }
*/
/*
      if($COMMENTS) {
//        $this->retval .= "<p style=\"text-align: right;\"><small>";
//        $this->retval .= "<a href=\"$COMMENTS\">Comments</a>";
//        $this->retval .= "</small></p>\n\n";
        $this->retval .= "<p style=\"text-align: right;\"><small>";
        $this->retval .= "<a href=\"$COMMENTS\">Comments</a>";
        $this->retval .= "</small></p>\n";
      }
*/
     $this->retval .=  "</div>";
    }

    // display a single item as HTML
    public function display_item_short_zoo($data, $parent,$limit,$rss_pos,$position=0)
    {
      extract($data);
      if(!$TITLE) return;

//      $this->retval .=  "<div style=\"height:30px;display:inline-block;widdth:1100%;\">&nbsp;</div>";

//      $this->retval .=  "<a href=\"".$LINK."\" target=\"_blank\"  rel=\"nofollow\" >";
      $this->retval .=  "<div style=\"width:100%;display:inline-block;margin:auto;border-bottom:1px dotted #ddd;margin-bottom:10px;\">";
//$this->retval .= $limit.") ";
/*
      // RSS 2.0
      if(isset($ENCLOSURE)) {
        if ($ENCLOSURE['TYPE']=="image/jpeg"){
            $this->retval .= "<img src=\"{$ENCLOSURE['URL']}\" style=\"max-width:45px;max-height:45px;margin-right:10px;margin-borrom:10px;\" align=\"left\">";
        }
      }
*/

/*
//      if($LINK) $this->retval .=  "<a href=\"$LINK\" target=\"_blank\">";
        $length_title=OW::getConfig()->getValue('news', 'rss_title_length_main');
        if (!$length_title) $length_title=55;
        $text=mb_substr(stripslashes($TITLE),0,$length_title);
        if (mb_strlen(stripslashes($TITLE))>$length_title) $text .="..";
        $this->retval .="<a href=\"".$LINK."\" target=\"_blank\">";
          $this->retval .= "<h1>".$text."</h1>";
        $this->retval .="</a>";
*/
//            NEWS_CTRL_News::setPageTitle(stripslashes($TITLE)); //title menu
//            NEWS_CTRL_News::setPageHeading(stripslashes($TITLE)); //title menu
            OW::getDocument()->setTitle(stripslashes($TITLE));
            OW::getDocument()->setHeading(stripslashes($TITLE));



//      if($LINK) $this->retval .= "</a>";
//          $this->retval .=  "</b>";
//          if(!$PUBDATE && $DC["DATE"]) $PUBDATE = $DC["DATE"];
//          if($PUBDATE) $this->retval .= "<br/><small style=\"font-size:8px;\">($PUBDATE)</small>";
//print_r($data);exit;
/*
        $title_url=$this->make_seo_url(stripslashes($TITLE),100);
        if (!$title_url) $title_url="index.html";
            else $title_url .=".html";
        $this->retval .="<div style=\"float:right;\">";
//        $this->retval .="<a class=\"ow_lbutton\" href=\"".OW_URL_HOME."news-show/".$rss_pos."_".$position."\">";
        $this->retval .="<a class=\"ow_lbutton\" href=\"".OW_URL_HOME."news/".$rss_pos."_".$position."/".$title_url."\">";
//        $this->retval .="<a class=\"ow_lbutton\" href=\"".$LINK."\" target=\"_blank\">";
        $this->retval .=OW::getLanguage()->text('news', 'read_more');
        $this->retval .="</a>";
        $this->retval .="</div>";
*/

//      $this->retval .=  "</p>\n";


/*
      // use feed-formatted HTML if provided
      if($CONTENT['ENCODED']) {
        $this->retval .= "<p>" . stripslashes($CONTENT['ENCODED']) . "</p>\n";
      } else if($DESCRIPTION) {
//        if($IMAGE) {
//          foreach($IMAGE as $IMG) $this->retval .= "<img src=\"$IMG\">\n";
//        }
        $desc=stripslashes($DESCRIPTION);
        $desc = preg_replace("/<img[^>]+\>/i", " ", $desc); 
//        $this->retval .=  "<p>" . $desc . "</p>\n\n";
        $this->retval .=  $desc . "\n";
      }
*/


      // use feed-formatted HTML if provided
if (!isset($CONTENT['ENCODED'])) $CONTENT['ENCODED']="";
if (!isset($DESCRIPTION)) $DESCRIPTION="";
if (!isset($IMAGE)) $IMAGE="";
if (!isset($ENCLOSURE)) $ENCLOSURE="";
if (!isset($COMMENTS)) $COMMENTS="";
if (!isset($WIDTH)) $WIDTH="";
      if($CONTENT['ENCODED']) {
        $this->retval .= "<p>" . stripslashes($CONTENT['ENCODED']) . "</p>\n";
      } elseif($DESCRIPTION) {
        if($IMAGE) {
          foreach($IMAGE as $IMG) $this->retval .= "<img src=\"$IMG\">\n";
        }
        $desc=stripslashes($DESCRIPTION);
//        $desc ="<a href=\"".$LINK."\" target=\"_blank\"  rel=\"nofollow\" >" . $desc ."</a>";
//        $desc= preg_replace("/<img[^>]+\>/i", " ", $desc); 
        $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:left;">',$desc);

        if($ENCLOSURE) {
            $aa="{$ENCLOSURE['URL']}";
            $desc = preg_replace('/<img(.*?)src="(.*?)"(.*?)>/i', '<a href="'.$aa.'" rel="nofollow" target="_blank" ><img src="$2" align="left" style="float:left;max-width:250px;max-height:250px;margin-right:10px;margin-bottom:5px;" title="'.stripslashes($TITLE).'"></a>', $desc);
        }else{
            $desc = preg_replace('/<img(.*?)src="(.*?)"(.*?)>/i', '<img src="$2" align="left" style="float:left;max-width:250px;max-height:250px;margin-right:10px;margin-bottom:5px;" title="'.stripslashes($TITLE).'">', $desc);
        }
//        $desc ="<a href=\"".$LINK."\" target=\"_blank\"  rel=\"nofollow\" >" . $desc ."</a>";

        $desc .="<div style=\"float:right;display:inline-block;margin:20px;margin-top:5px;\">";
        $desc .="<a class=\"ow_lbutton\" href=\"".$LINK."\" target=\"_blank\" rel=\"nofollow\" title=\"".stripslashes($TITLE)."\">";
        $desc .="&nbsp;&nbsp;&nbsp;".OW::getLanguage()->text('news', 'read_more')."...&nbsp;&nbsp;&nbsp;";
        $desc .="</a>";
        $desc .="</div>";

        $this->retval .=  "<p>" . $desc ."</p>\n\n";
/*
        $more ="<div style=\"float:right;\">";
        $more .="<a class=\"ow_lbutton\" href=\"".$LINK."\" target=\"_blank\">";
        $more .=OW::getLanguage()->text('news', 'read_more');
        $more .="</a>";
        $more .="</div>";
        $this->retval .=  $more;
*/


//        $this->retval .=  "" . $desc ."\n\n";
      }


/*
      // RSS 2.0 - ENCLOSURE
      if($ENCLOSURE) {
        $this->retval .= "<p><small><b>Media:</b> <a href=\"{$ENCLOSURE['URL']}\">";
        $this->retval .= $ENCLOSURE['TYPE'];
        $this->retval .= "</a> ({$ENCLOSURE['LENGTH']} bytes)</small></p>\n\n";
      }
*/

      if($COMMENTS) {
//        $this->retval .= "<p style=\"text-align: right;\"><small>";
//        $this->retval .= "<a href=\"$COMMENTS\">Comments</a>";
//        $this->retval .= "</small></p>\n\n";
        $this->retval .= "<p style=\"text-align: right;\"><small>";
        $this->retval .= "<a href=\"$COMMENTS\">Comments</a>";
        $this->retval .= "</small></p>\n";
      }

     $this->retval .=  "</div>";
//    $this->retval .=  "</a>";
      $this->retval .=  "<div style=\"height:30px;display:inline-block;width:100%;\">&nbsp;</div>";
    }


    public function fixEncoding(&$input, $key, $output_encoding)
    {
      if(!function_exists('mb_detect_encoding')) return $input;

      $encoding = mb_detect_encoding($input);
      switch($encoding)
      {
        case 'ASCII':
        case $output_encoding:
          break;
        case '':
          $input = mb_convert_encoding($input, $output_encoding);
          break;
        default:
          $input = mb_convert_encoding($input, $output_encoding, $encoding);
      }
    }


    public function getOutput($limit=false,$rss_id=0,$rss_pos="", $type="" ,$length_title=50,$length_content=0,$output_encoding='UTF-8')
    {
/*
//        echo print_r($this->data);
//xml_parse
//$url = 'http://www.globaltimes.cn/DesktopModules/DnnForge%20-%20NewsArticles/Rss.aspx?TabID=99&ModuleID=405&CategoryID=44&MaxCount=100&sortBy=StartDate&sortDirection=DESC';
//$url = 'http://news.yahoo.com/rss/tv';
//$url = '';

//        $rss = simplexml_load_file($url);
        $rss =simplexml_load_string($this->data);
        if($rss){
            echo $rss->channel->title;
            echo $rss->channel->pubDate;
            $items = $rss->channel->item;
            foreach($items as $item){
                $title = $item->title;
                $link = $item->link;
                $published_on = $item->pubDate;
                $description = $item->description;

                echo $title;
                echo '('.$published_on.')';
                echo $description;
                echo "<hr>";
            }
        }else{
            echo 'Problems loading RSS feed';
        }
*/
//    $rss_pos="main";//id or name
//            $rss_type="widget";
/*
        if ($type=="widget"){
            return $this->make_for_widget($limit=false,$rss_id=0,$rss_pos="", $type="" ,$length_title=50,$length_content=0,$output_encoding='UTF-8');
        }else{
            return $this->make_for_widget($limit=false,$rss_id=0,$rss_pos="", $type="" ,$length_title=50,$length_content=0,$output_encoding='UTF-8');
        }
*/
    }//getOutput




    public function make_one_zoom($file="",$rss_id=0,$rss_pos=0)
    {
        $ret="";
        $ret_header ="";
        $position=0;
        if ($file){
            $this->startRSSreader($file);
            if (isset($this->data)){
                $rss =simplexml_load_string($this->data);
//print_r($rss);exit;
                if($rss){
                    $c_title=$rss->channel->title;
                    $c_pubDate=$rss->channel->pubDate;

                    $c_link=$rss->channel->link;
                    $c_description=$rss->channel->description;
                    $c_language=$rss->channel->language;
                    $c_copyright=$rss->channel->copyright;

                    $c_image_title=$rss->channel->image->title;
                    $c_image_link=$rss->channel->image->link;
                    $c_image_url=$rss->channel->image->url;

$ret_header .="<div class=\"ow_ipc_content\" style=\"margin-bottom:20px;width:100%;display:inline-block;\">";

if ($c_image_title AND $c_image_link){
      $ret_header .= "<p>";
      if ($c_image_link) $ret_header .= "<a href=\"".$c_image_link."\" target=\"_blank\" rel=\"nofollow\" >";
      $ret_header .= "<img src=\"".$c_image_url."\"";
//      $ret_header .= " width=\"$WIDTH\" height=\"$HEIGHT\"";
//        $this->retval .= " width=\"100px\" height=\"$HEIGHT\"";
//        $this->retval .= " width=\"100px\" ";
        $ret_header .= " align=\"left\" ";
        $ret_header .= " style=\"float:left;max-width:130px;max-height:130px;margin-right:10px;\" ";
      $ret_header .= " border=\"0\" alt=\"".$c_image_title."\">";
      if ($c_image_link) $ret_header .= "</a>";
      $ret_header .= "</p>";

}

if ($c_title){
        $ret_header .="<h1 style=\"font-size:12px;font-style:italic;\">";
//        if ($c_link) $ret_header .= "<a href=\"".$c_link."\" target=\"_blank\" rel=\"nofollow\" >";
        $ret_header .= $c_title;
//        if ($c_link) $ret_header .= "</a>";
        $ret_header .= "</h1>";
}

if ($c_description) {
            $desc=$c_description;
            $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:right;">',$desc);

            $ret_header .= "<p style=\"font-size:12px;font-style:italic;\">".$desc."</p>\n\n";
}

$ret_header .= "<p style=\"text-align:right;\">";
if ($c_pubDate) $ret_header .= "<small style=\"font-size:10px;font-style:italic;color:#888;\">Published: ".$c_pubDate."</small><br>";
if ($c_copyright) $ret_header .= "<small style=\"font-size:10px;font-style:italic;color:#888;\">Copyright: ".$c_copyright."</small><br>";
$ret_header .= "</p>";

$ret_header .="</div>";
/*
[title] => Science News Headlines - Yahoo! News
            [link] => http://news.yahoo.com/science/
            [description] => Get the latest Science news headlines from Yahoo! News. Find breaking Science news, including analysis and opinion on top Science stories.
            [language] => en-US
            [copyright] => Copyright (c) 2013 Yahoo! Inc. All rights reserved
            [pubDate] => Fri, 03 May 2013 10:50:07 -0400
            [ttl] => 5
            [image] => SimpleXMLElement Object
                (
                    [title] => Science News Headlines - Yahoo! News
                    [link] => http://news.yahoo.com/science/
                    [url] => http://l.yimg.com/a/i/us/nws/th/main_142c.gif
                )

*/
//print_r($rss);exit;


                    $items = $rss->channel->item;
                    foreach($items as $item){

//print_r($item);exit;
        if ($position==$rss_pos){
//echo "sfdsdF";exit;
                        $title = $item->title;
                        $link = $item->link;
                        $published_on = $item->pubDate;
                        $description = $item->description;
/*
                    $ret .= $title;
                    $ret .=  '('.$published_on.')';
                    $ret .=  $description;
                    $ret .=  "<hr>";
*/
                        $title_url=$this->make_seo_url(stripslashes($title),100);
                        if (!$title_url) $title_url="index.html";
                            else $title_url .=".html";


                    $ret .= "<div class=\"clearfix ow_ipc_header\" style=\"min-height:20px;margin-bottom:10px;\">";
//                        $ret .="<h1 style=\"margin-left:10px;margin-top:1px;\">";
                        $ret .="<h1 style=\"font-size:120%;\">";
//                        $ret .= "<b>";
                        if ($link){
                            $ret .="<a href=\"".$link."\" title=\"".$title_url."\" target=\"_blank\" style=\"font-size: initial;\">";
                        }
                        $ret .=$title;
                        if ($link){
                            $ret .="</a>";
                        }
//                        $ret .="</b>";



 if ($published_on) {
        $ret .= "<div style=\"float:right;font-size:8px;font-weight:normal;\">";
            $ret .= " <small>(".$published_on.")</small>";
        $ret .="</div>";
 }
                        $ret .="</h1>";
                    $ret .="</div>";


$ret .="<div class=\"ow_ipc_content\" style=\"margin-bottom:20px;width:100%;display:inline-block;\">";

        $desc=$description;

//        $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)\/>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:left;">',$desc);
//        $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:left;">',$desc);
$desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:right;">',$desc);

        $max_width=250;
        $max_height=250;
//        if($ENCLOSURE) {
//            $desc = preg_replace('/<img(.*?)src="(.*?)"(.*?)>/i', '<a href=\"{'.$ENCLOSURE['URL'].'}\"><img src="$2" align="left" style="float:left;max-width:'.$max_width.'px;max-height:'.$max_height.'px;margin-right:10px;margin-bottom:5px;" title="'.stripslashes($TITLE).'"></a>', $desc);
//        }else{
//            $desc = preg_replace('/<img(.*?)src="(.*?)"(.*?)>/i', '<img src="$2" align="left" style="float:left;max-width:'.$max_width.'px;max-height:'.$max_height.'px;margin-right:10px;margin-bottom:5px;" title="'.$title.'">', $desc);
            $desc = preg_replace('/<img(.*?)src="(.*?)"(.*?)>/i', '<img src="$2" align="right" style="float:right;max-width:'.$max_width.'px;max-height:'.$max_height.'px;margin-right:10px;margin-bottom:5px;" title="'.$c_title.'">', $desc);
//        }

//        $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:left;">',$desc);

//        $this->retval .=  "<p>" . $desc . "</p>\n\n";
        $ret .= "<p style=\"text-align:justify;margin-right:10px;\">".$desc . "</p>\n";


//      if($COMMENTS) {
//        $ret .="<p style=\"text-align: right;\"><small>";
////        $ret .= "<a href=\"\">Comments</a>";
    //    $ret .="</small></p>\n\n";
//      }


$ret .="</div>";



                break;
            }//if ($position==$rss_pos){
                        $position=$position+1;
                    }//for
                }else{
                    $ret .=  OW::getLanguage()->text('news', 'problem_with_reading_feeds');
                }        
            }
        }
        $ret .="<hr/>".$ret_header;
        return $ret;
    }



//    public function make_for_zoom($file,$limit=false,$rss_id=0,$rss_pos="", $type="" ,$length_title=50,$length_content=0,$output_encoding='UTF-8')
    public function make_for_zoom($file="",$rss_id=0,$rss_pos=0)
    {
        $ret="";
        $position=0;
        if ($file){
            $this->startRSSreader($file);
            if (isset($this->data)){
                $rss =simplexml_load_string($this->data);
                if($rss){
//                    echo $rss->channel->title;
//                    echo $rss->channel->pubDate;

                    $items = $rss->channel->item;
                    foreach($items as $item){
                        $title = $item->title;
                        $link = $item->link;
                        $published_on = $item->pubDate;
                        $description = $item->description;
/*
                    $ret .= $title;
                    $ret .=  '('.$published_on.')';
                    $ret .=  $description;
                    $ret .=  "<hr>";
*/
                        $title_url=$this->make_seo_url(stripslashes($title),100);
                        if (!$title_url) $title_url="index.html";
                            else $title_url .=".html";

                    $ret .="<div class=\"clearfix\" style=\"width:100%;display:inline-block;margin:auto;border-bottom:1px dotted #ddd;margin-bottom:10px;\">";


    
                    $ret .="<div class=\"clearfix ow_ipc_header\" style=\"height:20px;\">";
                    $ret .="<h4 style=\"margin-left:10px;margin-top:1px;\">";
                    $ret .= "<b>";
                    $ret .=  "<a href=\"".OW_URL_HOME."news/".$rss_pos."_".$position."_zoom/".$title_url."\" >";
//                $ret .= stripslashes($title);
                    $ret .= $title;
                    $ret .= "</a>";
                    $ret .=  "</b>";
                    $ret .=  "</div>";


                    if($published_on) {
                        $ret .=  "<div style=\"float:right;font-size:8px;font-weight:normal;\">";
                            $ret .= " <small>(".$published_on.")</small>";
                        $ret .=  "</div>";
                    }


                    $ret .="<div class=\"ow_ipc_content\" style=\"margin-bottom:20px;width:100%;display:inline-block;\">";
                    if ($description) {
                        $desc=$description;
                        $desc=preg_replace('/<a(.*?)href="(.*?)"(.*?)>/','<br/><a href="$2" rel="nofollow" target="_blank" style="font-size:9px;font-style:italic;float:right;">',$desc);
                        $desc=preg_replace('/<img(.*?)src="(.*?)"(.*?)>/','<img src="$2" style="margin:0 10px 0 10px;max-width:130px;">',$desc);
                        $ret .= "<p style=\"font-size:12px;font-style:italic;\">".$desc."</p>";
                    }
/*
                        $ret .= "<p style=\"text-align: right;\"><small>";
                        $ret .= "<a href=\"".$description."\">Comments</a>";
                        $ret .= "</small></p>\n\n";
*/
                    $ret .="</div>";



                    $ret .="</div>";

                        $position=$position+1;

                    }//for
                }else{
                    $ret .=  OW::getLanguage()->text('news', 'problem_with_reading_feeds');
                }        
            }
        }
        return $ret;
    }



    public function make_for_widget($file,$limit=false,$rss_id=0,$rss_pos="", $type="" ,$length_title=50,$length_content=0,$output_encoding='UTF-8')
    {
        $ret="";
        $position=0;
        $this->startRSSreader($file);
        if (isset($this->data)){
            $rss =simplexml_load_string($this->data);
            if($rss){
                $ret .=  "<div style=\"width:100%;display:inline-block;margin:auto;border-bottom:1px dotted #ddd;margin-bottom:10px;\">";
                $ret .=  $rss->channel->title;
//                if ($rss->channel->pubDate){
//                    $ret .="<br/>";
//                    $ret .=  $rss->channel->pubDate;
//                }
                $ret .= "</div>";

                $items = $rss->channel->item;
                foreach($items as $item){
                    if (!$item->title) return;
                    $title = $item->title;
                    $link = $item->link;
                    $published_on = $item->pubDate;
                    $description = $item->description;
/*
                    $ret .= $title;
                    $ret .=  '('.$published_on.')';
                    $ret .=  $description;
                    $ret .=  "<hr>";
*/
                    $title_url=$this->make_seo_url(stripslashes($title),100);
                    if (!$title_url) $title_url="index.html";
                        else $title_url .=".html";

                    if (!$length_title) $length_title=55;
                    $text=mb_substr(stripslashes($title),0,$length_title);
                    if (mb_strlen(stripslashes($title))>$length_title) $text .="..";


            
                    $ret .=  "<div style=\"width:100%;display:inline-block;margin:auto;border-bottom:1px dotted #ddd;margin-bottom:10px;\">";
                    $ret .="<a href=\"".OW_URL_HOME."news/".$rss_pos."_".$position."_zoom/".$title_url."\">";
                    $ret .= $text;
                    $ret .="</a>";


                    $ret .="<div style=\"float:right;\">";
                    $ret .="<a class=\"ow_lbutton\" href=\"".OW_URL_HOME."news/".$rss_pos."_".$position."_zoom/".$title_url."\">";
                    $ret .=OW::getLanguage()->text('news', 'read_more');
                    $ret .="</a>";
                    $ret .="</div>";
                    $description=$this->cleatHTMLtags($description);
//echo "===================|".$description."|==================";exit;
                    if ($description){
                        if (!$length_content) {
                            $length_content=0;
                            $description="";
                        }else{
                            $description=$this->cropText($description,$length_content);
                        }
//echo $length_content;exit;
                        $ret .="<p style=\"text-align:justify;font-style:italic;\">".$description . "</p>\n";
                    }
    
                    $ret .= "</div>";

/*
                $ret .="<div class=\"ow_ipc_header\" style=\"height:20px;\">";
                $ret .="<h4 style=\"margin-left:10px;margin-top:1px;\">";
                $ret .= "<b>";
                $ret .=  "<a href=\"".OW_URL_HOME."news/".$rss_pos."_".$position."/".$title_url."\" >";
//                $ret .= stripslashes($title);
                $ret .= $title;
                $ret .= "</a>";
                $ret .=  "</b>";



                if($published_on) {
                    $ret .=  "<div style=\"float:right;font-size:8px;font-weight:normal;\">";
                        $ret .= " <small>(".$published_on.")</small>";
                    $ret .=  "</div>";
                }


                $ret .="<div class=\"ow_ipc_content\" style=\"margin-bottom:20px;width:100%;display:inline-block;\">";

//                $ret .= "<p style=\"text-align:justify;margin-right:10px;\">".$desc . "</p>\n";

                if($description) {
                    $ret .= "<p style=\"text-align: right;\"><small>";
                    $ret .= "<a href=\"".$description."\">Comments</a>";
                    $ret .= "</small></p>\n\n";
                }


                $ret .="</div>";
*/

                    $position=$position+1;
                    if ($limit>0){
                        if ($position>$limit){
                            break;
                        }
                    }
                }//for
            }else{
                $ret .=  OW::getLanguage()->text('news', 'problem_with_reading_feeds');
            }        
        }
        return $ret;
    }



































//    $items,$rss_id,$rss_pos,$rss_type)
//                            $items,      $rss_id,  $rss_pos,    $rss_type,$length_title,   $length_content
    public function getOutputX($limit=false,$rss_id=0,$rss_pos="", $type="" ,$length_title=50,$length_content=0,$output_encoding='UTF-8')
    {

//        $this->tags = array();
//        $this->output ="";
//        $this->retval = "";
//        $this->data = array();

//echo "<br><br><br><br><br>".$length_title." ".$length_content."<hr>";

      $this->retval = "";
      $start_tag = key($this->output);
if ($limit!=false) {
    $limit=(int) $limit;
    $limit=$limit-1;
    if ($limit<0) $limit=0;
}

//return "ss";
//return "--".print_r($this->output,1);
//return $start_tag;
      switch($start_tag)
      {
        case "RSS":
          foreach($this->output[$start_tag]["CHANNEL"] as $channel) {
//                 display_channel($data,    $limit,$type,$rss_pos,$length_title,$length_content=0)
            $this->display_channel($channel, $limit,$type,$rss_pos,$length_title,$length_content);

          }
          break;

        case "RDF:RDF":
//            $this->retval .="Not supported...";
          break;
/*
          if(isset($this->output[$start_tag]['IMAGE'])) {
            foreach($this->output[$start_tag]['IMAGE'] as $image) {
              $this->display_image($image);
            }
          }
          foreach($this->output[$start_tag]['CHANNEL'] as $channel) {
            $this->display_channel($channel, $limit);
          }
          foreach($this->output[$start_tag]['ITEM'] as $item) {
            $this->display_item($item, $start_tag);
          }
          break;
*/

        case "HTML":
//          die("Error: cannot parse HTML document as RSS");
//            $this->retval .="Not supported...";
          break;

        default:
//          die("Error: unrecognized start tag '$start_tag' in getOutput()");
//            $this->retval .="Not supported...";
          break;
      }

      if($this->retval && is_array($this->retval)) {
        array_walk_recursive($this->retval, 'myRSSParser::fixEncoding', $output_encoding);
      }
      return $this->retval;
    }

//getOutputZoom($rss_id,$rss_pos)
    public function getOutputZoom($rss_id=0,$rss_pos=0, $type="short",$output_encoding='UTF-8')
    {
      $this->retval = "";
      $start_tag = key($this->output);
      $limit=0;

//return "ss";
//return "--".print_r($this->output,1);
//return $start_tag;
      switch($start_tag)
      {
        case "RSS":
          foreach($this->output[$start_tag]["CHANNEL"] as $channel) {
            $this->display_channel_zoom($channel, $limit,$type,$rss_id,$rss_pos);
          }
          break;

        case "RDF:RDF":
          // old format - channel and items are separate
          if(isset($this->output[$start_tag]['IMAGE'])) {
            foreach($this->output[$start_tag]['IMAGE'] as $image) {
              $this->display_image($image);
            }
          }
          foreach($this->output[$start_tag]['CHANNEL'] as $channel) {
            $this->display_channel($channel, $limit);
          }
          foreach($this->output[$start_tag]['ITEM'] as $item) {
            $this->display_item($item, $start_tag);
          }
          break;

        case "HTML":
//          die("Error: cannot parse HTML document as RSS");

        default:
//          die("Error: unrecognized start tag '$start_tag' in getOutput()");
      }

      if($this->retval && is_array($this->retval)) {
        array_walk_recursive($this->retval, 'myRSSParser::fixEncoding', $output_encoding);
      }
      return $this->retval;
    }

    public function getRawOutput($output_encoding='UTF-8')
    {
      array_walk_recursive($this->output, 'myRSSParser::fixEncoding', $output_encoding);
      return $this->output;
    }


    public function get_file_cache($url='',$name='file'){

    $name=md5($url."_".$name);
    //save the rss file cache
    $validCache = false;
    $cachetime=1;//hours
//    $cachetime=1;//hours
    $cachetime=OW::getConfig()->getValue('news', 'rss_cachetime_minuts');//minuts
    if (!$cachetime OR $cachetime=="" OR $cachetime<0) $cachetime=1;
    $path="ow_userfiles".DS."plugins".DS."news".DS;
    if (file_exists($path.'file_'.$name.'_cache.data')) {
        $contents = file_get_contents($path.'file_'.$name.'_cache.data');
        $data = unserialize($contents);

//        if (time() - $data['time'] < ($cachetime * 60 * 60)) {//hours
        if (time() - $data['time'] < ($cachetime * 60)) {//minuts
            $validCache = true;
            $feed = $data['feed'];
        }
    }

    if (!$validCache) {
        $feed = file_get_contents($url);
        $data = array ('feed' => $feed, 'time' => time() );
        file_put_contents($path.'file_'.$name.'_cache.data', serialize($data));
    }

    return $feed;

    }

    public function cleatHTMLtags($name)
    {
        return strip_tags($name);
    }

    public function cropText($string, $charLimit, $preserveWords=true){
        if (!is_numeric($charLimit)) {
            return $string;            
        } else if (strlen($string) > $charLimit) {
                
            if ($preserveWords) {
                while (--$charLimit > 0 && $string[$charLimit] != " "); 
            }
            
            $string = mb_substr($string, 0, $charLimit).'...';
        }
        
        return $string;
    }


     public function html2txt($document){
        $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
        );
        $text = preg_replace($search, '', $document);
        $text=preg_replace("/(&?!amp;)/i", " ", $text);
        $text=preg_replace("/(&#\d+);/i", " ", $text); // For numeric entities
        $text=preg_replace("/(&\w+);/i", " ", $text); // For literal entities
        return $text;
    }

    public static function checkcurentlang()
    {
        $curent_language_id=0;
        if (isset($_SESSION['base.language_id']) AND $_SESSION['base.language_id']>0){
            $curent_language_id=$_SESSION['base.language_id'];
        }
        return $curent_language_id;
    }


    public function safe_content($content="",$more=1){
        if ($more==1){
            $content = preg_replace("/(<iframe[^<]+<\/iframe>)/", '', $content);
        }else{
//            $newWidth="100%";
//            $newHeight="auto";

            $newWidth=OW::getConfig()->getValue('news', 'embed_video_width');
            if (!$newWidth) $newWidth=580;
            $newHeight=OW::getConfig()->getValue('news', 'embed_video_height');
            if (!$newHeight) $newHeight=387;


            $newWidthIm=OW::getConfig()->getValue('news', 'embed_image_width');
            if (!$newWidthIm) $newWidthIm="600";
            $newHeightIm=OW::getConfig()->getValue('news', 'embed_image_height');
            if (!$newHeightIm) $newHeightIm="800";


//echo "cccc".$content;exit;
/*
            $content = preg_replace(array('/width="\d+"/i', '/height="\d+"/i'),array(sprintf('width="%d"', $newWidth), sprintf('height="%d"', $newHeight)),$content);

            $content = preg_replace('/(<iframe.*?width=)"\d+"( height=)"\d+/i', '$1"'.$newWidth.'"$2"'.$newHeight, $content);

$content = preg_replace('~width\s?=\s?["\'][^"\']+["\']~i', 'width="580"', $content);
$content = preg_replace('~height\s?=\s?["\'][^"\']+["\']~i', 'height="387"',$content);
*/
//resize embedded video
//            $find = preg_match("/<iframe(.*)/iframe>/", $content, $content);

//            if ($find) {
            $content = preg_replace('~(<iframe[^>]*width\s?=\s?["\'])[^"\']+(["\'])~i', '${1}'.$newWidth.'${2}', $content);
            $content = preg_replace('~(<iframe[^>]*height\s?=\s?["\'])[^"\']+(["\'])~i', '${1}'.$newHeight.'${2}', $content);

            $content = preg_replace('~(<embed[^>]*width\s?=\s?["\'])[^"\']+(["\'])~i', '${1}'.$newWidth.'${2}', $content);
            $content = preg_replace('~(<embed[^>]*height\s?=\s?["\'])[^"\']+(["\'])~i', '${1}'.$newHeight.'${2}', $content);

/*
            //image 
            $content = preg_replace('~(<img[^>]*width\s?=\s?["\'])[^"\']+(["\'])~i', '${1}'.$newWidth.'${2}', $content);
            $content = preg_replace('~(<img[^>]*height\s?=\s?["\'])[^"\']+(["\'])~i', '${1}'.$newHeight.'${2}', $content);
*/

//            }
//echo $content;exit;
        }

        $content = preg_replace('/<script\b[^>]*>(.*?)<\/script>/i', "", $content);
        $content = preg_replace('~<\s*\bscript\b[^>]*>(.*?)<\s*\/\s*script\s*>~is', '', $content);

        $content = str_replace("'","`",$content);
//        if ($more){
//            $content=addslashes($content);
//        }
        return $content;
    }


    public function make_seo_url($name,$lengthtext=100)
    {
        $seo_title=stripslashes($name);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace(chr(160),"_",$seo_title);
        $seo_title=str_replace("~","",$seo_title);
        $seo_title=str_replace("(","",$seo_title);
        $seo_title=str_replace(")","",$seo_title);
        $seo_title=str_replace("]","",$seo_title);
        $seo_title=str_replace("[","",$seo_title);
        $seo_title=str_replace("}","",$seo_title);
        $seo_title=str_replace("{","",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("\\","",$seo_title);
        $seo_title=str_replace("+","",$seo_title);
        $seo_title=str_replace(":","",$seo_title);
        $seo_title=str_replace(";","",$seo_title);
        $seo_title=str_replace("\"","",$seo_title);
        $seo_title=str_replace("<","",$seo_title);
        $seo_title=str_replace(">","",$seo_title);
        $seo_title=str_replace("?","",$seo_title);
        $seo_title=str_replace(",",".",$seo_title);
        $seo_title=str_replace("!","",$seo_title);
        $seo_title=str_replace("`","",$seo_title);
        $seo_title=str_replace("'","",$seo_title);
        $seo_title=str_replace("@","",$seo_title);
        $seo_title=str_replace("#","",$seo_title);
        $seo_title=str_replace("$","",$seo_title);
        $seo_title=str_replace("%","",$seo_title);
        $seo_title=str_replace("^","",$seo_title);
        $seo_title=str_replace("&","",$seo_title);
        $seo_title=str_replace("*","",$seo_title);
        $seo_title=str_replace("|","",$seo_title);
        $seo_title=str_replace("=","",$seo_title);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("?","_",$seo_title);
        $seo_title=str_replace("#","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("&amp;","_",$seo_title);
        $seo_title=str_replace("__","_",$seo_title);
        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }
















    public function aupdaterss()
    {
//        $url = "http://news.google.com/?ned=us&topic=t&output=rss"; // url of google news
//        $xml = simplexml_load_file($url); //loading the document
//        $title = $xml->channel->title; //gets the title of the document.

for ($i=1;$i<11;$i++){
    if (OW::getConfig()->getValue('news', 'rss_topic_'.$i)!="" AND OW::getConfig()->getValue('news', 'rss_topic_'.$i)!="0"){
        $sender_name="";
        $sender_id=0;
        $idtopic=0;

        $query = "SELECT * FROM " . OW_DB_PREFIX. "base_user WHERE username='".addslashes(OW::getConfig()->getValue('news', 'rss_own_'.$i))."' LIMIT 1";
        $arr = OW::getDbo()->queryForList($query);
        if (isset($arr[0]) AND $arr[0]['id']>0){
            $sender_id=$arr[0]['id'];
            $sender_name=BOL_UserService::getInstance()->getDisplayName($sender_id);
        }else{
            $query = "SELECT * FROM " . OW_DB_PREFIX. "base_user WHERE id='".addslashes(OW::getConfig()->getValue('news', 'rss_own_'.$i))."' LIMIT 1";
            $arr = OW::getDbo()->queryForList($query);
            if (isset($arr[0]) AND $arr[0]['id']>0){
                $sender_id=$arr[0]['id'];
                $sender_name=BOL_UserService::getInstance()->getDisplayName($sender_id);
            }else{
                $query = "SELECT * FROM " . OW_DB_PREFIX. "base_user WHERE username LIKE '".addslashes(OW::getConfig()->getValue('news', 'rss_own_'.$i))."' LIMIT 1";
                $arr = OW::getDbo()->queryForList($query);
                if (isset($arr[0]) AND $arr[0]['id']>0){
                    $sender_id=$arr[0]['id'];
                    $sender_name=BOL_UserService::getInstance()->getDisplayName($sender_id);
                }
            }
        }

        if ($sender_id>0 AND $sender_name){
//        OW::getConfig()->getValue('news', 'rss_topic_'.$i)!="" AND $idtopic>0
            $query = "SELECT * FROM " . OW_DB_PREFIX. "news_topics WHERE t_name LIKE '".addslashes(OW::getConfig()->getValue('news', 'rss_topic_'.$i))."' LIMIT 1";
            $arr = OW::getDbo()->queryForList($query);
            if (isset($arr[0]) AND $arr[0]['idt']>0){
                $idtopic=$arr[0]['idt'];
            }else{
                $query = "SELECT * FROM " . OW_DB_PREFIX. "news_topics WHERE idt='".addslashes(OW::getConfig()->getValue('news', 'rss_topic_'.$i))."' LIMIT 1";
                $arr = OW::getDbo()->queryForList($query);
                if (isset($arr[0]) OR $arr[0]['idt']>0){
                    $idtopic=$arr[0]['idt'];
                }
            }
        }

//echo $sender_name."--".$idtopic;
//exit;

        if ($sender_name!="" AND $sender_id>0 AND OW::getConfig()->getValue('news', 'rss_can_'.$i) AND 
            OW::getConfig()->getValue('news', 'rss_item_'.$i)>0 AND OW::getConfig()->getValue('news', 'rss_own_'.$i)!="" AND 
            OW::getConfig()->getValue('news', 'rss_topic_'.$i)!="" AND $idtopic>0
        ){

//if (1==1){
//            $url = "http://news.google.com/?ned=us&topic=t&output=rss";
            $max=OW::getConfig()->getValue('news', 'rss_item_'.$i);

            $url =OW::getConfig()->getValue('news', 'rss_can_'.$i);
            $rss = simplexml_load_file($url);
            if($rss)
            {
/*
        echo '<h1>'.$rss->channel->title.'</h1>';
        echo '<li>'.$rss->channel->pubDate.'</li>';
*/

                $citem=0;
                $items = $rss->channel->item;
                foreach($items as $item)
                {
//            $title = $item->title;
                    $link = $item->link;
                    $published_on = $item->pubDate;
//            $description = $item->description;
/*
            echo '<h3><a href="'.$link.'">'.$title.'</a></h3>';
            echo '<span>('.$published_on.')</span>';
            echo '<p>'.$description.'</p>';
*/
//$sender_name="Google";
//$sender_name=OW::getConfig()->getValue('news', 'rss_own_'.$i);
$timestamp=strtotime(date('Y-m-d h:i:s'));
//$timestamp_p=strtotime(date('Y-m-d h:i:s'),$item->pubDate);
$timestamp_p=$timestamp;
//$id_sender=1;
//$id_sender=OW::getConfig()->getValue('news', 'rss_own_'.$i);
$id_sender=$sender_id;
//$id_topic=241;
//$id_topic=OW::getConfig()->getValue('news', 'rss_topic_'.$i);
$id_topic=$idtopic;
$topic_name=$item->title;

//$ext_ident="google";
$ext_ident="";
$ext_unique=$item->guid;
$ext_lat_mod=$timestamp;
                    $query ="SELECT * FROM " . OW_DB_PREFIX. "news WHERE ext_unique='".addslashes($ext_unique)."' LIMIT 1";
//echo $query;exit;
                    $arr = OW::getDbo()->queryForList($query);
                    if (!isset($arr[0]) OR !$arr[0]['id']){
//                $value=$arr[0];

                        $query = "INSERT INTO " . OW_DB_PREFIX. "news (
                            id ,     active , id_sender ,      id_topic    ,    mlat    ,mlon  ,  ftom_ip ,
                        data_mod     ,   is_published  ,  data_added    ,  topic_name  ,    sender_name,
                            ext_ident,       ext_unique    ,  ext_lat_mod
                        )VALUES(
                            '','1','".addslashes($id_sender)."','".addslashes($id_topic)."','','','',
                            '".addslashes($timestamp)."','1','".addslashes($timestamp_p)."','".addslashes($topic_name)."','".addslashes($sender_name)."',
                            '".addslashes($ext_ident)."','".addslashes($ext_unique)."','".addslashes($ext_lat_mod)."' 
                        )";
// ON DUPLICATE KEY UPDATE ext_lat_mod='".addslashes($ext_lat_mod)."' ";
//echo $query;
//            LEFT JOIN " . OW_DB_PREFIX. "news_content nnc ON (nnc.id_news=nn.id) 
//            WHERE nn.id='".addslashes($params['idnews'])."' ".$add." LIMIT 1";//location
//            $arr = OW::getDbo()->queryForList($query);
//            OW::getDbo()->query($query);
$ext_footer="";
if ($rss->channel->image->url){
    if ($rss->channel->image->link) $ext_footer .="<a href=\"".$rss->channel->image->link."\" nofollow target=\"blank\">";
    $ext_footer .= "<img src=\"".$rss->channel->image->url."\" align=\"left\" style=\"border:0;margin:5px;padding:0;\">";
    if ($rss->channel->image->link) $ext_footer .="</a>";
}

if ($rss->channel->title){
    $ext_footer .= "<div class=\"clearfix\">";
    if ($rss->channel->link) $ext_footer .="<a href=\"".$rss->channel->link."\" nofollow target=\"blank\">";
    if ($rss->channel->title) $ext_footer .= $rss->channel->title;
    if ($rss->channel->link) $ext_footer .="</a>";
    $ext_footer .= "</div>";
}



if ($rss->channel->description) {
    $ext_footer .= "<div class=\"clearfix\">";
    $ext_footer .= $rss->channel->description;
    $ext_footer .= "</div>";
}

                        $last_insert_id = OW::getDbo()->insert($query);
                        if ($last_insert_id>0){
                        $query = "INSERT INTO " . OW_DB_PREFIX. "news_content (
                                id_news ,    content, ext_footer
                            )VALUES(
                                '".addslashes($last_insert_id)."','".addslashes($item->description)."','".addslashes($ext_footer)."'
                                
                        )";
                        OW::getDbo()->insert($query);

                            $citem++;
                            if ($citem>$max) break;
                        }


                        }//if not exist
                    }//for
                }//if rss
                unset($rss);

            }//if require
    }//if not turnoff
}//for 10
                unset($item);
                unset($items);

    }

    public function make_menu($selected="")
    {
        $content="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;

        $query = "SELECT * FROM " . OW_DB_PREFIX. "news_topics  
        WHERE active='1' ORDER BY t_name";
        $contentt="";
        $arr = OW::getDbo()->queryForList($query);
        $alt=1;
        foreach ( $arr as $value ){
/*
            $contentt .="<tr>";
            $contentt .="<td class=\"ow_alt".$alt." \">";
            $contentt .="<input type=\"text\" name=\"f_name[".$value['idt']."]\" value=\"".stripslashes($value['t_name'])."\">";
            $contentt .="</td>";
            $contentt .="<td class=\"ow_alt".$alt." \">";
            if ($value['active']==1) $sel=" CHECKED ";
                else $sel="";
            $contentt .="<input ".$sel." type=\"checkbox\" name=\"f_active[".$value['idt']."]\" value=\"1\">";
            $contentt .="</td>";
            $contentt .="<td class=\"ow_alt".$alt." \">";
            $sel="";
            $contentt .="<input style=\"border:2px solid #f00;\" ".$sel." type=\"checkbox\" name=\"f_delete[".$value['idt']."]\" value=\"1\">";
            $contentt .="</td>";
            $contentt .="</tr>";
            $alt++;
            if ($alt>2){
                $alt=1;
            }
*/

                $title=stripslashes($value['t_name']);
                if (OW::getConfig()->getValue('shoppro', 'max_cat_title_chars')>0){
                    $max=OW::getConfig()->getValue('shoppro', 'max_cat_title_chars');
                }else{
                    $max=20;
                }
                $title=mb_substr($title,0,$max);
//$contentt .="<div class=\"ow_box_cap ow_dnd_configurable_component clearfix\" style=\"margin-bottom: 5px;\">
$contentt .="<div class=\"ow_box_cap ow_dnd_configurable_component clearfix\" style=\"margin-bottom: 3px;\" >
    <div class=\"ow_box_cap_right \">
        <div class=\"ow_box_cap_body ow_ic_right_arrowx \" style=\"overflow:hidden;max-width: 180px;\">";
            if ($selected>0 AND $selected==$value['idt']){
//                $contentt .="<h3 class=\"ow_ic_right_arrow\" style=\"font-size:9px;padding:5px 0 7px 27px;\">";
                $contentt .="<h3 class=\"ow_ic_right_arrow\" style=\"padding:5px 0 7px 22px;font-size:100%;  background-position: 4px 50%;\">";
            }else{            
//                $contentt .="<h3 class=\"ow_ic_doc\" style=\"font-size:9px;padding:5px 0 7px 27px;\">";
                $contentt .="<h3 class=\"ow_ic_doc\" style=\"padding:5px 0 7px 22px;font-size:100%;   background-position: 4px 50%;\">";
            }
//                    <h3 style=\"float:none;display:inline-block;font-size:10px;margin:0;padding:0;background:transparent; -webkit-margin-before:0; webkit-margin-after:0;\">".$title."</h3>
                $contentt .="<a href=\"".$curent_url."news/".$value['idt']."\" style=\"\">";
//                $contentt .="<h3 style=\"float:none;display:inline-block;font-size:90%;margin:0;padding:0;background:transparent; -webkit-margin-before:0; webkit-margin-after:0;\">".$title."</h3>";
                $contentt .=$title;
                $contentt .="</a>
            </h3>
        </div>
    </div>
</div>";

/*
                if ($selected>0 AND $selected==$value['idt']){
                    $contentt .="<li class=\"ow_ic_right_arrow\" style=\"list-style-type:none;list-style-position: inside;\">";
                }else if ($is_admin){
                    $contentt .="<li style=\"list-style-type:none;list-style-position: inside;\">";
                }else{
                    $contentt .="<li style=\"list-style-position: inside;list-style-type: disc;\">";
                }
                
                $contentt .="<a href=\"".$curent_url."news/".$value['idt']."\">";
                if (!$value['active']) $content_menu .="<i style=\"color:#f00;\">";
                $title=stripslashes($value['t_name']);
                if (OW::getConfig()->getValue('shoppro', 'max_cat_title_chars')>0){
                    $max=OW::getConfig()->getValue('shoppro', 'max_cat_title_chars');
                }else{
                    $max=20;
                }
                $title=mb_substr($title,0,$max);
                $contentt .=$title;
                if (!$value['active']) $content_menu .="</i>";
                $contentt .="</a>";



                $contentt .="</li>";
*/

        }//for
        if ($contentt){
            $content .="<div style=\"margin:0px;\"><ul>".$contentt."</ul></div>";
        }
        return $content;
    }




 
    public function make_tabs($selected="",$content="")
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
$content_t ="";

        $content_t .="<div class=\"ow_content\">";

        $content_t .="<div class=\"ow_content_menu_wrap\">";
        $content_t .="<ul class=\"ow_content_menu clearfix\">";

//$mapmode="edit";
//        if ($selected=="1" OR $selected=="newsfeed" OR !$selected) $sel=" active ";
        if ($selected=="news" OR !$selected) $sel=" active ";
            else $sel="";
//echo $selected."===".$sel;exit;
//        $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."wall/newsfeed\"><span class=\"ow_ic_plugin\">".OW::getLanguage()->text('map', 'ta_newsfeed')."</span></a></li>";
        $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."news\"><span class=\"ow_ic_file\">".OW::getLanguage()->text('news', 'ta_news')."</span></a></li>";

/*
        if ( OW::getPluginManager()->isPluginActive('shoppro') AND OW::getConfig()->getValue('map', 'tabdisable_shop')!=1){
//            if ($id_user>0 OR $is_admin){
                if ($selected=="shop") $sel=" active ";
                    else $sel="";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."wall/shop\"><span class=\"ow_ic_cart\">".OW::getLanguage()->text('map', 'ta_shop')."</span></a></li>";
//            }
        }

        if ( OW::getPluginManager()->isPluginActive('fanpage') AND OW::getConfig()->getValue('map', 'tabdisable_fanpage')!=1){
//            if ($id_user>0 OR $is_admin){
                if ($selected=="fanpage") $sel=" active ";
                    else $sel="";
//                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."wall/fanpage\"><span class=\"ow_ic_photo\">".OW::getLanguage()->text('map', 'ta_fanpage')."</span></a></li>";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."wall/fanpage\"><span class=\"ow_ic_friends\">".OW::getLanguage()->text('map', 'ta_fanpage')."</span></a></li>";
//            }
        }
*/

    if (OW::getUser()->isAdmin() OR OW::getUser()->isAuthorized('news', 'add')){

        if ($id_user AND ($is_admin OR OW::getConfig()->getValue('news', 'allow_add_for_users')=="1")){
            if ($selected=="add") $sel=" active ";
                else $sel="";
            $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."newsx/add/new\"><span class=\"ow_ic_add\">".OW::getLanguage()->text('news', 'ta_add_new_article')."</span></a></li>";

            if ($selected=="edit"){
                if ($selected=="edit") $sel=" active ";
                    else $sel="";
//            if ($is_admin){
//                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."news?add_new=true\"><span class=\"ow_ic_edit\">".OW::getLanguage()->text('map', 'ta_admin_edit_markers')."</span></a></li>";
//            }else{
                    $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"javascript:void(0);\"><span class=\"ow_ic_edit\">".OW::getLanguage()->text('news', 'ta_edit_article')."</span></a></li>";
//            }
            }
        }

    }

        if ($selected=="zoom"){
            $sel=" active ";
//            $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."map?mapmode=ed#ef\"><span class=\"ow_ic_edit\">".OW::getLanguage()->text('map', 'ta_edityourmarkers')."</span></a></li>";
            $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"javascript:void(0);\"><span class=\"ow_ic_doc\">".OW::getLanguage()->text('news', 'ta_article_zoom')."</span></a></li>";
//            $content_t .="<li class=\"_store_my_items ".$sel."\"><span class=\"ow_ic_doc\">".OW::getLanguage()->text('map', 'ta_marker_zoom')."</span></li>";
        }

        if ($is_admin){
                if ($selected=="category") $sel=" active ";
                    else $sel="";
//                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."wall/fanpage\"><span class=\"ow_ic_photo\">".OW::getLanguage()->text('map', 'ta_fanpage')."</span></a></li>";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."newscat/tabc/category\"><span class=\"ow_ic_write\">".OW::getLanguage()->text('news', 'ta_category')."</span></a></li>";
//            }
        }

/*

        if ( OW::getPluginManager()->isPluginActive('shoppro') AND OW::getConfig()->getValue('map', 'tabdisable_shop')!=1){
//            if ($id_user>0 OR $is_admin){
                if ($selected=="shop") $sel=" active ";
                    else $sel="";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."map/tab/shop\"><span class=\"ow_ic_cart\">".OW::getLanguage()->text('map', 'ta_shop')."</span></a></li>";
//            }
        }

        if ( OW::getPluginManager()->isPluginActive('fanpage') AND OW::getConfig()->getValue('map', 'tabdisable_fanpage')!=1){
//            if ($id_user>0 OR $is_admin){
                if ($selected=="fanpage") $sel=" active ";
                    else $sel="";
//                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."wall/fanpage\"><span class=\"ow_ic_photo\">".OW::getLanguage()->text('map', 'ta_fanpage')."</span></a></li>";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."map/tab/fanpage\"><span class=\"ow_ic_friends\">".OW::getLanguage()->text('map', 'ta_fanpage')."</span></a></li>";
//            }
        }
*/

$content_t .="</ul>";
$content_t .="</div>";

//        }//if not disabled tabs

        $content_t .=$content;
        $content_t .="</div>";
        return $content_t;
    }





    public function get_cat_select($selected=0)
    {
        $ret="";
        $query = "SELECT * FROM " . OW_DB_PREFIX. "news_topics 
        WHERE active='1' ORDER BY t_name";
        $arr = OW::getDbo()->queryForList($query);
        $alt=1;
//        if (!$selected){
            $ret .="<option SELECTED value=\"\">".OW::getLanguage()->text('news', 'turn_off')."</option>";
            $ret .="<option value=\"\">-------------------</option>";
//        }
        foreach ( $arr as $value ){
            if ($value['idt']==$selected) $sel=" SELECTED ";
                else $sel="";
            $ret .="<option ".$sel." value=\"".$value['idt']."\">".stripslashes($value['t_name'])."</option>";
        }
        return $ret;
    }





    public function makePagination($page = 1, $totalitems, $limit = 15, $adjacents = 1, $targetpage = "/", $pagestring = "?page=",$position="right")
    {               
    //defaults
    if(!$adjacents) $adjacents = 1;
    if(!$limit) $limit = 15;
    if ($limit==0) $limit=1;
//    if(!$page) $page = 0;
//    $page=$page-1;
//    if ($page<0) $page=0;
    if(!$targetpage) $targetpage = "/";
    if(!isset($margin) OR !$margin) $margin = "";
    if(!isset($padding) OR !$padding) $padding="";
    if(!isset($pagestring1) OR !$pagestring1) $pagestring1="";

    //other vars
    $prev = $page - 1;                                                                  //previous page is page - 1
    $next = $page + 1;                                                                  //next page is page + 1
    $lastpage = ceil($totalitems / $limit);                             //lastpage is = total items / items per page, rounded up.
    $lpm1 = $lastpage - 1;                                                              //last page minus 1
    $space=" ";
//$lastpage++;    
//return "--".$lastpage;    
    if ($position=="center") $position="ow_center";
    else if ($position=="left") $position="ow_left";
    else $position="ow_right";
    $pagination = "";
    if($lastpage > 1)
    {   
        $pagination .= "<div class=\"".$position." ow_paging clearfix ow_smallmargin\"";
        if($margin || $padding)
        {
            $pagination .= " style=\"";
            if($margin)
                $pagination .= "margin: $margin;";
            if($padding)
                $pagination .= "padding: $padding;";
            $pagination .= "\"";
        }
        $pagination .= ">";

        //previous button
        if ($page > 1) 
            $pagination .= "<a href=\"".$targetpage.$pagestring.$prev."\">«</a>".$space;
        else
            $pagination .= "<a class=\"disabled\" href=\"".$targetpage.$pagestring1."\">«</a>".$space;
//            $pagination .= "<span class=\"disabled\">«</span>";    
        $pagination .= "&nbsp;&nbsp;&nbsp;";
        
        //pages 
        if ($lastpage < 7 + ($adjacents * 2))   //not enough pages to bother breaking it up
        {       
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
//                    $pagination .= "<span class=\"active\">$counter</span>";
                if ($counter == $page)
                    $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                     
                else
                    $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                     
            }
        }
        elseif($lastpage >= 7 + ($adjacents * 2))       //enough pages to hide some
        {
            //close to beginning; only hide later pages
            if($page < 1 + ($adjacents * 3))            
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
//                        $pagination .= "<span class=\"active\">$counter</span>";
                    if ($counter == $page)
                        $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                    else
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                }
                $pagination .= "<span class=\"elipses\">...</span>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>".$space;               
            }
            //in middle; hide some front and some back
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>".$space;
                $pagination .= "<span class=\"elipses\">...</span>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
//                        $pagination .= "<span class=\"active\">$counter</span>";
                    if ($counter == $page)
                        $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                  
                    else
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                }
                $pagination .= "...";
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>".$space;               
            }
            //close to end; only hide early pages
            else
            {
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>".$space;
                $pagination .= "<span class=\"elipses\">...</span>".$space;
                for ($counter = $lastpage - (1 + ($adjacents * 3)); $counter <= $lastpage; $counter++)
                {
//                      $pagination .= "<span class=\"active\">$counter</span>";
                    if ($counter == $page)
                        $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                    else
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                }
            }
        }
        
        //next button
        $pagination .= "&nbsp;&nbsp;&nbsp;";
        if ($page < $counter - 1) 
            $pagination .= "<a href=\"" . $targetpage . $pagestring . $next . "\">»</a>".$space;
        else
            $pagination .= "<a class=\"disabled\" href=\"" . $targetpage . $pagestring . $lastpage . "\">»</a>".$space;
//            $pagination .= "<a class=\"disabled\" href=\"" . $targetpage . $pagestring . $next . "\">»</a>".$space;
//            $pagination .= "<span class=\"disabled\">»</span>";
        $pagination .= "</div>\n";
    }

    if ($pagination){
        return "<div class=\"clearfix ow_smallmargin\">".$pagination."</div>";
    }else{
        return $pagination;
    }

    }





    public function add_to_wall($last_insert_id=0,$action_name="",$url_to_ads="",$title_post="", $action_key="")
    {
//---------------------------------feed start
        $id_user = OW::getUser()->getId();//citent login user (uwner)
                    $curent_url = 'http';
                    if (isset($_SERVER["HTTPS"])) {$curent_url .= "s";}
                    $curent_url .= "://";
                    $curent_url .= $_SERVER["SERVER_NAME"]."/";
$curent_url=OW_URL_HOME;                                        

$action_name=$this->html2txt($action_name);
$title_post=$this->html2txt($title_post);

/*
$from_config=$curent_url;
$from_config=str_replace("https://","",$from_config);
$from_config=str_replace("http://","",$from_config);
$trash=explode($from_config,$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
$url_detect=$trash[1];
//print_r($trash);
//echo $url_detect;
*/
                    $is_ok=false;

//                if ($last_insert_id>0){
            if ($id_user AND $action_name AND ($url_to_ads AND $title_post) AND $last_insert_id>0){
//                    $timestamp=strtotime(date('Y-m-d',time()));
                    $timestamp=strtotime(date('Y-m-d H:i:s'));
/*
$xx=date('Y-m-d H:i:s');
echo $xx;
echo "<hr>";
echo UTIL_DateTime::parseDate($xx);
echo "<hr>";
print_r(UTIL_DateTime::parseDate($xx,UTIL_DateTime::MYSQL_DATETIME_DATE_FORMAT));
//if ( !UTIL_Validator::isDateValid($date[UTIL_DateTime::PARSE_DATE_MONTH], $date[UTIL_DateTime::PARSE_DATE_DAY], $date[UTIL_DateTime::PARSE_DATE_YEAR]) )
exit;
*/
//OW::getConfig()->getValue('base', 'date_field_format');
/*
$xx=date('Y-m-d H:i:s');
echo $xx;
if (OW::getConfig()->getValue('base', 'site_timezone')){
    ini_set('date.timezone', OW::getConfig()->getValue('base', 'site_timezone'));
//    echo OW::getConfig()->getValue('base', 'site_timezone');
}
echo "<hr>";
$xx=date('Y-m-d H:i:s');
echo $xx;
exit;
*/

//                    $subject="Added a new page";//from param
//                    $url_to_ads=$curent_url."html/".$id_user."/".$last_insert_id."/index.html";//from param

                    $title_post_lenght=255;
                    if ($action_key) {
                        $entityType=$action_key;//ident in wall!!!!!!!!!!!
                    }else{
                        $entityType="news_add_product";//ident in wall!!!!!!!!!!!
                    }
                    $pluginKey="news";//plugin key!!!!!

//                    $curent_url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
                    $array_toolbar[0]=array("href" => $url_to_ads, "label" => "URL");
                    $array=array(
                        "time" => "".$timestamp."",
                        "ownerId" => "".$id_user."",
                        "string" => "".mb_substr($action_name,0,$title_post_lenght)."",
                        "content" => "<a href=\"".$url_to_ads."\">".$url_to_ads."</a>\r\n<div class=\"ow_remark\" style=\"paddig-top: 4px\">".mb_substr($title_post,0,$title_post_lenght)."</div>",
                        "view" => array("iconClass" => "ow_ic_link"),
                        "toolbar" => array_values($array_toolbar)
                    );
                    $datadata=json_encode($array);

                    //------check for exist start
                    $query = "SELECT * FROM " . OW_DB_PREFIX. "newsfeed_action  WHERE entityId='".addslashes($last_insert_id)."' AND entityType='".addslashes($entityType)."' AND pluginKey='".addslashes($pluginKey)."' LIMIT 1";
                    $arrx = OW::getDbo()->queryForList($query);
                    $value=$arrx[0];
                    if (!$value['id']){
//                        $uniquer_prefix=OW::getConfig()->getValue('news', 'mode_uniquer_prefix');
                        $uniquer_prefix="_".md5(date('Y-m-d-H-m-s'));
                    }else{
                        $uniquer_prefix="";
                    }
                    //------check for exist end
//                if (!$value['id']){
                    $query = "INSERT INTO " . OW_DB_PREFIX. "newsfeed_action  (
                        id ,     entityId ,       entityType ,     pluginKey   ,    data
                    )VALUES(
                        '','".addslashes($last_insert_id)."','".addslashes($entityType.$uniquer_prefix)."','".addslashes($pluginKey)."','".addslashes($datadata)."'
                    )";
//echo "(1)".$query;
                    $last_insert_id_a = OW::getDbo()->insert($query);
                    if ($last_insert_id_a>0){
                        $query = "INSERT INTO " . OW_DB_PREFIX. "newsfeed_action_set  (
                            id , actionId ,       userId,  timestamp
                        )VALUES(
                            '','".addslashes($last_insert_id_a)."','".addslashes($id_user)."','".addslashes($timestamp)."'
                        )";
                        $last_insert_id_as = OW::getDbo()->insert($query);
                        if ($last_insert_id_as>0){
                            $query = "INSERT INTO " . OW_DB_PREFIX. "newsfeed_activity  (
                                id ,     activityType,    activityId ,     userId  ,data ,   actionId  ,      timeStamp ,      privacy ,visibility    ,  status
                            )VALUES(
                                '','create','1','".addslashes($id_user)."','[]','".addslashes($last_insert_id_a)."','".addslashes($timestamp)."','everybody','15','active'
                            )";
                            $last_insert_id_ac = OW::getDbo()->insert($query);
                            if ($last_insert_id_ac>0){
                                $query = "INSERT INTO " . OW_DB_PREFIX. "newsfeed_action_feed  (
                                    id , feedType ,feedId , activityId
                                )VALUES(
                                    '','user','1','".addslashes($last_insert_id_ac)."'
                                )";
                                $last_insert_id_af = OW::getDbo()->insert($query);
                                if (!$last_insert_id_af){
                                    $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action WHERE id='".addslashes($last_insert_id_a)."' LIMIT 1";
                                    $arr = OW::getDbo()->query($query); 
                                    $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action_set WHERE id='".addslashes($last_insert_id_as)."' LIMIT 1";
                                    $arr = OW::getDbo()->query($query); 
                                    $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_activity WHERE id='".addslashes($last_insert_id_ac)."' LIMIT 1";
                                    $arr = OW::getDbo()->query($query); 
                                }else{
                                    $is_ok=true;
                                }
                            }else{
                                $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action WHERE id='".addslashes($last_insert_id_a)."' LIMIT 1";
                                $arr = OW::getDbo()->query($query); 
                                $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action_set WHERE id='".addslashes($last_insert_id_as)."' LIMIT 1";
                                $arr = OW::getDbo()->query($query); 
                            }

                        }else{//end 2
                            $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action WHERE id='".addslashes($last_insert_id_a)."' LIMIT 1";
                            $arr = OW::getDbo()->query($query); 
                        }
                    }//end1
//                }//if (!$value['id']){
            }
            return $is_ok;
//---------------------------------getInsertId

    }//func	
	

    public function add_to_wall_new($last_insert_id=0,$entityType="news-item", $title="",$content="",$url_to_ads="",$for_user=0, $pluginKey="news",$icon_class="ow_ic_link")
    {


        if (!OW::getPluginManager()->isPluginActive('newsfeed')){
            return;
        }

        if (!$last_insert_id OR !$entityType){
            return;
        }

        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $curent_url=OW_URL_HOME;                                        

        if (!$id_user){
            return;
        }

        if (!$for_user AND $id_user>0) $for_user=$id_user;

        $title_ok=$this->html2txt($title);

        $content=$this->html2txt($content);

        if (!$title_ok) $title_ok=UTIL_String::truncate($content, 32, '..');
        if (!$title_ok) $title_ok="no title..";

        $title_post_lenght=255;
        if (!$icon_class) $icon_class="";

        if ($url_to_ads){
//            $title="<a href=\"".$url_to_ads."\">".$url_to_ads."</a>\r\n<div class=\"ow_remark\" style=\"paddig-top: 4px\">".UTIL_String::truncate($title, $title_post_lenght, '..')."</div>";
            $title="<a href=\"".$url_to_ads."\"><div class=\"ow_remark\" style=\"paddig-top: 4px\">".UTIL_String::truncate($title_ok, $title_post_lenght, '..')."</div></a>";
        }

        $event = new OW_Event('feed.action', array(
            'pluginKey' => $pluginKey,
            'entityType' => $entityType,
            'entityId' => $last_insert_id,
            'userId' => $for_user,
            'replace' => true,
            'feedType' => 'user',
            'feedId' => $last_insert_id,
            'features' => array('likes','comments'),
            ), array(
                'string' =>  UTIL_String::truncate($title_ok, $title_post_lenght, '..'),
                'content' => UTIL_String::truncate($content, 200, '..'),
                'view' => array("iconClass" => $icon_class)
            )
        );
        OW::getEventManager()->trigger($event);

    }











}