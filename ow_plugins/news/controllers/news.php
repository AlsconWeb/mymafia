<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/



class NEWS_CTRL_News extends OW_ActionController
{

    private $curent_hash="";
    public function ajax_text() 
    {
        echo "Test ajax OK";
exit;
    }

    public function indexaddedit($params)
    {
        $content="";
 $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
        $curent_url=OW_URL_HOME;
if (isset($params['action']) AND $params['action']){
    $param_action=$params['action'];
}else{
    $param_action="new";
}
if (isset($params['idobj']) AND $params['idobj']>0){
    $param_idobj=$params['idobj'];
}else{
    $param_idobj=0;
}


        if (!OW::getUser()->isAdmin() AND !OW::getUser()->isAuthorized('news', 'add')){
            return;
        }


//newsx/:action/:idobj
//print_r($params);
//exit;
        if ($id_user AND ($is_admin OR OW::getConfig()->getValue('news', 'allow_add_for_users')=="1")){

            if ($param_action=="delete" AND isset($_GET['ss']) AND $_GET['ss']==substr(session_id(),3,3) AND isset($_GET['delit']) AND $_GET['delit']=="true" AND $param_idobj>0){
                $sql="DELETE FROM " . OW_DB_PREFIX. "news WHERE id='".addslashes($param_idobj)."' LIMIT 1";
                OW::getDbo()->query($sql);
                $sql="DELETE FROM " . OW_DB_PREFIX. "news_content WHERE id_news='".addslashes($param_idobj)."' LIMIT 1";
                OW::getDbo()->query($sql);

                        //----------------------------delete newsfeed
                            $query = "SELECT * FROM " . OW_DB_PREFIX. "newsfeed_action  WHERE entityId='".addslashes($param_idobj)."' AND pluginKey='news' LIMIT 1";
                            $arrx = OW::getDbo()->queryForList($query);
                            
                            if (isset($arrx[0]) AND $arrx[0]['id']>0){
                                $value=$arrx[0];        
                                $sql="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action WHERE entityId='".addslashes($param_idobj)."' AND pluginKey='news' LIMIT 1";
                                OW::getDbo()->query($sql);
/*                                
                                $sql="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action_set WHERE actionId='".addslashes($arrx[0]['id'])."' ";
                                OW::getDbo()->query($sql);

                                $sql="DELETE FROM " . OW_DB_PREFIX. "newsfeed_activity WHERE actionId='".addslashes($arrx[0]['id'])."' ";
                                OW::getDbo()->query($sql);                        
*/

                            }


                OW::getFeedback()->info(OW::getLanguage()->text('news', 'deleted_succesfull'));
                OW::getApplication()->redirect($curent_url."news?page=".$_GET['fp']);
                exit;
            }

            if ($param_action=="save" AND isset($_POST['ss']) AND $_POST['ss']==substr(session_id(),5,5)){
$timestamp=strtotime(date('Y-m-d H:i:s'));
                if (isset($_POST['edi']) AND $_POST['edi']>0){
                    if ($is_admin) $add=" ";
                        else $add=" AND id_sender='".addslashes($id_user)."' ";

if (isset($_POST['f_active']) AND $_POST['f_active']){
    $_POST['f_active']=1;
}else{
    $_POST['f_active']=0;
}
if (isset($_POST['f_approved']) AND $_POST['f_approved']){
    $_POST['f_approved']=1;
}else{
    $_POST['f_approved']=0;
}

if ($is_admin){    
    $approv_n=",is_published ";
    $approv_v=",'".addslashes($_POST['f_approved'])."' ";
    $approv_u=",is_published='".addslashes($_POST['f_approved'])."' ";
}else{
    $approv_n="";
    $approv_v="";
    $approv_u="";
}
                    
                    $sql="UPDATE " . OW_DB_PREFIX. "news SET 
                        topic_name='".addslashes(NEWS_BOL_Service::getInstance()->safe_content($_POST['f_title']))."',
                        id_topic='".addslashes($_POST['f_cat'])."',
mlat='".addslashes($_POST['f_lat'])."',
mlon='".addslashes($_POST['f_lon'])."',
data_mod='".addslashes($timestamp)."',
active='".addslashes($_POST['f_active'])."'
".$approv_u."  
                    WHERE id='".addslashes($_POST['edi'])."' ".$add." LIMIT 1";
//echo $sql;exit;
                    OW::getDbo()->query($sql);

                    $sql="UPDATE " . OW_DB_PREFIX. "news_content SET 
                        content='".addslashes(NEWS_BOL_Service::getInstance()->safe_content($_POST['f_content'],0))."' 
                    WHERE id_news='".addslashes($_POST['edi'])."' LIMIT 1";
//echo "<textarea>".$sql."</textarea>";exit;
                    OW::getDbo()->query($sql);

                    OW::getFeedback()->info(OW::getLanguage()->text('news', 'update_succesfull'));
                }else if (isset($_POST['edi']) AND $_POST['edi']=="new"){
                    $dname=BOL_UserService::getInstance()->getDisplayName($id_user);
                    $sql="INSERT INTO " . OW_DB_PREFIX. "news (
                        id ,     active,  id_sender  ,     id_topic  ,      data_added      ,topic_name   ,   sender_name,
mlat,mlon,data_mod
".$approv_n."
                    )VALUES(
                        '','".addslashes($_POST['f_active'])."','".addslashes($id_user)."','".addslashes($_POST['f_cat'])."','".addslashes($timestamp)."','".addslashes(NEWS_BOL_Service::getInstance()->safe_content($_POST['f_title']))."','".addslashes($dname)."',
'".addslashes($_POST['f_lat'])."','".addslashes($_POST['f_lon'])."','".addslashes($timestamp)."'
".$approv_v."
                    )";
                    $last_insert_id= OW::getDbo()->insert($sql);
                    if ($last_insert_id>0){
                        $sql="INSERT INTO " . OW_DB_PREFIX. "news_content (
                            id_news,content
                        )VALUES(
                            '".addslashes($last_insert_id)."','".addslashes(NEWS_BOL_Service::getInstance()->safe_content($_POST['f_content'],0))."'
                        )";
//echo $sql;exit;
                        OW::getDbo()->insert($sql);


//--------wall start
//                                if (OW::getConfig()->getValue('news', 'publish_updateproduct_onwall')){
//                                    $action_name="Added new product to shop";
                                    $action_name=OW::getLanguage()->text('news', 'config_publish_updateproduct_onwall_infotect');
                                    $title_post=addslashes(NEWS_BOL_Service::getInstance()->safe_content($_POST['f_title']));

                                    if (!$name) $name="index";
                                    $seo_title=$name;
                                    $seo_title=NEWS_BOL_Service::getInstance()->make_seo_url($seo_title,100);

                                    $url_to_ads=$curent_url."news/".$last_insert_id."/".$seo_title.".html";
//                                    NEWS_BOL_Service::getInstance()->add_to_wall($last_insert_id,    $action_name, $url_to_ads,$title_post);

                                    NEWS_BOL_Service::getInstance()->add_to_wall_new($last_insert_id=0,"news-item", $title_post,UTIL_String::truncate(NEWS_BOL_Service::getInstance()->safe_content($_POST['f_content'],0), 140, '...'),$url_to_ads,0, "news","ow_ic_file");
//                                }
//--------wall end

                        OW::getFeedback()->info(OW::getLanguage()->text('news', 'save_succesfull'));
                    }
                }else{
                    OW::getFeedback()->error(OW::getLanguage()->text('news', 'unknow_error'));
                }
                OW::getApplication()->redirect($curent_url."newsx/add/new?fcat=".$_POST['f_cat']);
                exit;
            }

            $row['topic_name']="";
            $row['content']="";
            $row['id_topic']=0;
            $row['mlat']="";
            $row['mlon']="";
            $row['is_published']="";
            $row['active']="";

            if ($param_action=="edit" AND $param_idobj>0){
                $add=" ";
                if (!$is_admin) $add=" AND nn.active='1' ";            
                $query = "SELECT * FROM " . OW_DB_PREFIX. "news nn 
                LEFT JOIN " . OW_DB_PREFIX. "news_content nnc ON (nnc.id_news=nn.id) 
                WHERE nn.id='".addslashes($param_idobj)."' ".$add." LIMIT 1";//location
                $arr = OW::getDbo()->queryForList($query);
                if (isset($arr[0])){
                    $row=$arr[0];
                }
            }

            $content .="<form method=\"POST\" action=\"".$curent_url."newsx/save/".$param_idobj."\">";
            $content .="<input type=\"hidden\" name=\"ss\" value=\"".substr(session_id(),5,5)."\">";
            if ($param_idobj>0){
                $content .="<input type=\"hidden\" name=\"edi\" value=\"".$param_idobj."\">";
            }else{
                $content .="<input type=\"hidden\" name=\"edi\" value=\"new\">";
            }
            $content .="<table style=\"width:100%;\">";

            $content .="<tr>";
//            $content .="<td align=\"right\" colspan=\"2\">";
            $content .="<td align=\"left\" colspan=\"2\">";
            $content .="<b>".OW::getLanguage()->text('news', 'title').":</b> ";
//            $content .="</td>";
//            $content .="<td>";
            $content .="<input type=\"text\" name=\"f_title\" value=\"".$row['topic_name']."\" style=\"width:80%;\">";

//            $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                $content .="<div class=\"ow_right\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('news', 'config_save')."\" name=\"send\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>";
//            $content .="</div>";

            $content .="</td>";
            $content .="</tr>";

            $content .="<tr>";
            $content .="<td align=\"left\" colspan=\"2\">";
            $content .="<b>".OW::getLanguage()->text('news', 'category').":</b> ";
//            $content .="</td>";
//            $content .="<td>";
//            $content .="<input type=\"text\" name=\"f_title\" value=\"".$row['topic_name']."\">";

            $content .="<select name=\"f_cat\">";

            $add="";
            if ($is_admin) $add=" 1 ";            
                else $add=" active='1' ";            
                

            $query = "SELECT * FROM " . OW_DB_PREFIX. "news_topics
            WHERE ".$add." ORDER BY t_name";

            $arr = OW::getDbo()->queryForList($query);
            foreach ( $arr as $value ){
                if (isset($_GET['fcat']) AND $_GET['fcat']>0 AND $_GET['fcat']==$value['idt']) $sel=" SELECTED ";
                else if ($row['id_topic']==$value['idt']) $sel=" SELECTED ";
                else $sel="";
                $content .="<option ".$sel." value=\"".$value['idt']."\">".stripslashes($value['t_name'])."</option>";
            }
            $content .="</select>";

            $content .=", <b>".OW::getLanguage()->text('news', 'set_active').":</b> ";
            if ($row['active']!="0") $sel=" checked ";
                else $sel="";
            $content .="<input ".$sel." type=\"checkbox\" name=\"f_active\" value=\"1\">";


            if ($is_admin){    
                $content .=", <b>".OW::getLanguage()->text('news', 'set_approved').":</b> ";
                if ($row['is_published']=="1" OR !$row['is_published'] AND $row['is_published']!="0") $sel=" checked ";
                    else $sel="";
                $content .="<input ".$sel." type=\"checkbox\" name=\"f_approved\" value=\"1\">";
            }else{
                $content .="<input type=\"hidden\" name=\"f_approved\" value=\"1\">";
            }

            $content .="</td>";
            $content .="</tr>";



            $content .="<tr>";
            $content .="<td align=\"left\" colspan=\"2\">";
            $content .="<b>".OW::getLanguage()->text('news', 'content').":</b> ";
            $content .="</td>";
            $content .="</tr>";

            $content .="<tr>";
            $content .="<td align=\"left\" colspan=\"2\">";
//            $content .="<textarea class=\"html\" name=\"f_content\" style=\"width:90%;min-height:600px;\">".$row['content']."</textarea>";

//--------editor s
                $fname="f_content";
                $content_edit=stripslashes($row['content']);
                $minheight=450;

//                if (!OW::getPluginManager()->isPluginActive('wysiwygeditor') AND !class_exists('WysiwygTextarea')){
//                    include_once(OW_DIR_CORE."form_element.php");
//                }

//                $content_t.="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\">".$description."</textarea>";
                if ( OW::getPluginManager()->isPluginActive('wysiwygeditor') ){
//                    $ret.="<textarea class=\"html\" name=\"".$name."\" id=\"".$name."\" style=\"\">".$description."</textarea>";
                    $content .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:350px;min-height:450px;\">".$content_edit."</textarea>";
                }else if (class_exists('WysiwygTextarea')){

//        $body = new Textarea($fname);
        $body = new WysiwygTextarea($fname,array( BOL_TextFormatService::WS_BTN_IMAGE, BOL_TextFormatService::WS_BTN_VIDEO ), true);
        $body->setValue($content_edit);
        $body->addAttribute('id',$fname);
        $body->setHasInvitation(true);
        $body->setSize($minheight);//SIZE_S,SIZE_M,SIZE_L 
        $content .=$body->renderInput();

                }else if (class_exists('NEWS_CLASS_Wysiwygtextarea')){

        $body = new NEWS_CLASS_Wysiwygtextarea($fname,array( BOL_TextFormatService::WS_BTN_IMAGE, BOL_TextFormatService::WS_BTN_VIDEO ), true);
        $body->setValue($content_edit);
        $body->addAttribute('id',$fname);
        $body->setHasInvitation(true);
        $body->setSize($minheight);//SIZE_S,SIZE_M,SIZE_L 
        $content .=$body->renderInput();

                }else{
                    $content .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:350px;min-height:450px;\">".$content_edit."</textarea>";
                }
//--------editor e

            $content .="</td>";
            $content .="</tr>";


            $content .="<tr>";
            $content .="<td align=\"left\" colspan=\"2\">";
            $content .="<b>".OW::getLanguage()->text('news', 'map_position').":</b> ";
            $content .="</td>";
            $content .="</tr>";

            $content .="<tr>";
            $content .="<td align=\"left\" colspan=\"2\">";
            $content .="<b>".OW::getLanguage()->text('news', 'map_lat').":</b> ";
            $content .="<input type=\"text\" name=\"f_lat\" id=\"f_lat\" value=\"".$row['mlat']."\" style=\"width:150px;\">";
            $content .="<b>".OW::getLanguage()->text('news', 'map_lon').":</b> ";
            $content .="<input type=\"text\" name=\"f_lon\" id=\"f_lon\" value=\"".$row['mlon']."\" style=\"width:150px;\">";
            $content .="</td>";
            $content .="</tr>";




            $content .="<tr>";
            $content .="<td align=\"left\" colspan=\"2\">";

            $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_right\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('news', 'config_save')."\" name=\"send\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
            $content .="</td>";
            $content .="</tr>";

            $content .="</table>";

            $content .="</form>";








    $content .="<table style=\"width:100%;\">";

            $content .="<tr>";
            $content .="<td align=\"left\" colspan=\"2\">";

/*
$content .="<div>
      <input id=\"searchTextField\" type=\"text\" size=\"50\" style=\"width:200px;\">
      <input type=\"radio\" name=\"type\" id=\"changetype-all\" checked=\"checked\">
      <label for=\"changetype-all\">All</label>

      <input type=\"radio\" name=\"type\" id=\"changetype-establishment\">
      <label for=\"changetype-establishment\">Establishments</label>

      <input type=\"radio\" name=\"type\" id=\"changetype-geocode\">
      <label for=\"changetype-geocode\">Geocodes</lable>
    </div>";
*/
$content .="<div class=\"ow_right\">
      <input id=\"searchTextField\" type=\"text\" size=\"50\" style=\"width:300px;\">
    </div>";

            $content .="<div id=\"mapdiv\" style=\"width:100%; height:350px;\"></div>";
            $content .="<br/><b><i>".OW::getLanguage()->text('news', 'map_position_info')."</i></b>";

//$content .="<script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=false\"></script>";
$content .="<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places\"></script>";

if ($row['mlat']) $mlat=$row['mlat'];
    else $mlat="39.36827914916013";
if ($row['mlon']) $mlon=$row['mlon'];
    else $mlon="-38.671875";
$zoom=3;

$content .="<script type=\"text/javascript\">
var map;
var was_marker=0;

var xcmarkers = [];

function mapa()
{





    var opts = {'center': new google.maps.LatLng(".$mlat.", ".$mlon."), 'zoom':".$zoom.", 'mapTypeId': google.maps.MapTypeId.ROADMAP }
    map = new google.maps.Map(document.getElementById('mapdiv'),opts);";

    if ($row['mlat'] AND $row['mlon']){
        $content .="addMarker(new google.maps.LatLng(".$mlat.", ".$mlon."));";
        $content .="was_marker=1;";
    }
    
    $content .="google.maps.event.addListener(map,'click',function(event) {
        if (was_marker==0){
            document.getElementById('f_lat').value = event.latLng.lat();
            document.getElementById('f_lon').value = event.latLng.lng();

            addMarker(event.latLng);
            was_marker=1;
        }
    })";

$content .="
        var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

google.maps.event.addListener(autocomplete, 'place_changed', function() {
//          infowindow.close();
//          xcmarker.setVisible(false);
          input.className = '';
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // Inform the user that the place was not found and return.
            input.className = 'notfound';
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
/*
          var image = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          };
*/
//          xcmarker.setIcon(image);
//alert(place.geometry.location);
//         xcmarker.setPosition(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));
//         xcmarker.setPosition(new google.maps.LatLng(".$mlat.", ".$mlon."));
//alert(place.geometry.location.lat());
//        xcmarker.setPosition(place.geometry.location);
//          xcmarker.setVisible(true);
if (xcmarkers[0]!=undefined){
    xcmarkers[0].setPosition(place.geometry.location);
}
//xcmarkers[0].setPosition(new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng()));

        document.getElementById('f_lat').value = place.geometry.location.lat();
        document.getElementById('f_lon').value = place.geometry.location.lng();


          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

//          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
//          infowindow.open(map, xcmarker);
        });

/*
 // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          google.maps.event.addDomListener(radioButton, 'click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);
*/
";



$content .="function addMarker(location) {

var  xcmarker = new google.maps.Marker({
    position: location,
    map: map,
    draggable: true,
    visible: true
  });
  xcmarkers.push(xcmarker);

  google.maps.event.addListener(xcmarker, 'click', function(event) {
    this.setMap(null);
    xcmarkers=[];
    was_marker=0;
        document.getElementById('f_lat').value = '';
        document.getElementById('f_lon').value = '';
  });
  google.maps.event.addListener(xcmarker, 'dragend', function(event) {
        document.getElementById('f_lat').value = event.latLng.lat();
        document.getElementById('f_lon').value = event.latLng.lng();
  });
  google.maps.event.addListener(xcmarker, 'dragstart', function() {
  });

}

}

$(function(){
mapa();
});

</script>";
            $content .="</td>";
            $content .="</tr>";
    $content .="</table>";



            if ($param_action=="edit" AND $param_idobj>0){
                $content=NEWS_BOL_Service::getInstance()->make_tabs('edit',$content);
            }else{
                $content=NEWS_BOL_Service::getInstance()->make_tabs('add',$content);
            }

            $this->assign('content', $content);
        }else{
                OW::getFeedback()->error(OW::getLanguage()->text('news', 'acticle_not_found'));
                OW::getApplication()->redirect($curent_url."news");
                exit;
        }
    }

	


    public function indexcat($params)
    {
    $content="";
$curent_url=OW_URL_HOME;
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();


        if (!$is_admin) {
            OW::getApplication()->redirect($curent_url."news");
        }
//print_r($params);exit;
//print_r($_POST);exit;
//echo $_POST['ss']."--".substr(session_id(),3,5);
//exit;


        if (isset($params['ctab']) AND $params['ctab']=="save" AND isset($_POST['ss']) AND $_POST['ss']==substr(session_id(),3,5)){
//print_r($_POST);
//exit;

                if (isset($_POST['f_name'])){

                    $fname=$_POST['f_name'];
                    $factive=$_POST['f_active'];
                    $fdelete=$_POST['f_delete'];

                    $query = "SELECT * FROM " . OW_DB_PREFIX. "news_topics 
                    WHERE 1 ORDER BY t_name";
                    $arr = OW::getDbo()->queryForList($query);
                    foreach ( $arr as $value ){
                        if (isset($fname[$value['idt']]) AND $fname[$value['idt']]){
                            $name=$fname[$value['idt']];
                        }else{
                            $name="Category";
                        }
                        if (isset($factive[$value['idt']]) AND $factive[$value['idt']]){
                            $active=1;
                        }else{
                            $active=0;
                        }
                        if (isset($fdelete[$value['idt']]) AND $fdelete[$value['idt']]==1){
                            $sql="DELETE FROM " . OW_DB_PREFIX. "news_topics 
                            WHERE idt='".$value['idt']."' LIMIT 1";
                            OW::getDbo()->query($sql);
                        }else{
                            $sql="UPDATE " . OW_DB_PREFIX. "news_topics SET 
                                t_name='".addslashes($name)."',
                                active='".addslashes($active)."' 
                            WHERE idt='".$value['idt']."' LIMIT 1";
                            OW::getDbo()->query($sql);
                        }
                    }//for
                }
            
                    if (isset($_POST['f_name_new']) AND $_POST['f_name_new']){
                        if (isset($_POST['f_active_new']) AND $_POST['f_active_new']) $active=1;
                            else $active=0;
                        $sql="INSERT INTO " . OW_DB_PREFIX. "news_topics (
                            idt,  active , t_name 
                        )VALUES(
                            '','".addslashes($active)."','".addslashes($_POST['f_name_new'])."'
                        )";
                        OW::getDbo()->query($sql);
                    }


            OW::getApplication()->redirect($curent_url."newscat/tabc/category");
            exit;
        }

        $query = "SELECT * FROM " . OW_DB_PREFIX. "news_topics  
        WHERE 1 ORDER BY t_name";
        $contentt="";
        $arr = OW::getDbo()->queryForList($query);
        $alt=1;
        foreach ( $arr as $value ){
            $contentt .="<tr>";
            $contentt .="<td class=\"ow_alt".$alt." \">";
            $contentt .="<input type=\"text\" name=\"f_name[".$value['idt']."]\" value=\"".stripslashes($value['t_name'])."\">";
            $contentt .="</td>";
            $contentt .="<td class=\"ow_alt".$alt." \">";
            if ($value['active']==1) $sel=" CHECKED ";
                else $sel="";
            $contentt .="<input ".$sel." type=\"checkbox\" name=\"f_active[".$value['idt']."]\" value=\"1\">";
            $contentt .="</td>";
            $contentt .="<td class=\"ow_alt".$alt." \">";
            $sel="";
            $contentt .="<input style=\"border:2px solid #f00;\" ".$sel." type=\"checkbox\" name=\"f_delete[".$value['idt']."]\" value=\"1\">";
            $contentt .="</td>";
            $contentt .="</tr>";
            $alt++;
            if ($alt>2){
                $alt=1;
            }
        }
        
        if ($contentt){
            $content .="<form method=\"POST\" action=\"".$curent_url."newscat/tabc/save\">";
            $content .="<input type=\"hidden\" name=\"ss\" value=\"".substr(session_id(),3,5)."\">";
            $content .="<table class=\"ow_table_1 ow_form\">";
            $content .="<tr class=\"ow_tr_first\">";
            $content .="<th>";
            $content .="<b>".OW::getLanguage()->text('news', 'cname')."</b>";
            $content .="</th>";
            $content .="<th>";
            $content .="<b>".OW::getLanguage()->text('news', 'cactive')."</b>";
            $content .="</th>";
            $content .="<th>";
            $content .="<b>".OW::getLanguage()->text('news', 'cdelete')."</b>";
            $content .="</th>";
            $content .="</tr>";
            $content .= $contentt;

            $content .="<tr>";
            $content .="<td colspan=\"3\">";
            $content .="<b>".OW::getLanguage()->text('news', 'cadd_new').":</b>";
            $content .="</td>";
            $content .="</tr>";

            $content .="<tr>";
            $content .="<td>";
            $content .="<input type=\"text\" name=\"f_name_new\" value=\"\">";
            $content .="</td>";
            $content .="<td >";
            $content .="<input checked type=\"checkbox\" name=\"f_active_new\" value=\"1\">";
            $content .="</td>";
            $content .="<td >";

        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\"ow_positive\">
                            <input type=\"submit\" name=\"saveb\" value=\"".OW::getLanguage()->text('news', 'config_save')."\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";

            $content .="</td>";
            $content .="</tr>";

            $content .="</table>";



            $content .="</form>";
        }

        $content=NEWS_BOL_Service::getInstance()->make_tabs('category',$content);
        $this->assign('content', $content);
    }






    public function index($params)
    {
        $content="";
$perpagex=10;
$limit_all=10;
$limit_single=$limit_all;
$add_paramurl="";
$option="";
//return $this->aa();

            if (isset($_GET['page']) AND $_GET['page']>1){
                $curent_page=$_GET['page'];
            }else{
                $curent_page=1;
            }

if (isset($params['option'])) $option=$params['option'];

if (isset($params['icat']) AND $params['icat']>0) $ccat=$params['icat'];
else if (isset($_GET['cat']) AND $_GET['cat']>0) $ccat=$_GET['cat'];
else $ccat=0;





            $this->setPageTitle(OW::getLanguage()->text('news', 'index_page_title')); //title menu
            $this->setPageHeading(OW::getLanguage()->text('news', 'index_page_heading')); //title page

 $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin

//        $content .=$this->make_menu();
        $curent_url=OW_URL_HOME;

            if (!OW::getUser()->isAdmin() AND !OW::getUser()->isAuthorized('news', 'view')){
                OW::getFeedback()->error(OW::getLanguage()->text('news', 'you_have_not_access'));
                OW::getApplication()->redirect($curent_url);
                exit;
            }



    if (OW::getConfig()->getValue('news', 'mode')=="news"){//-----------------------------------------------------------------NEWS 
//        $content .="fsdf";


        if (isset($params['idnews']) AND $params['idnews']>0){//--------------------zoom
                $add=" ";
                if (!$is_admin) $add=" AND nn.active='1' AND nn.is_published='1' ";            

            $query = "SELECT * FROM " . OW_DB_PREFIX. "news nn 
            LEFT JOIN " . OW_DB_PREFIX. "news_content nnc ON (nnc.id_news=nn.id) 
            WHERE nn.id='".addslashes($params['idnews'])."' ".$add." LIMIT 1";//location
            $arr = OW::getDbo()->queryForList($query);
            if (isset($arr[0])){
                $value=$arr[0];
            }else{
                $value=array();
                $value['id']=0;
            }
            if ($value['id']>0){
                    $pluginStaticDir =OW::getPluginManager()->getPlugin('news')->getStaticUrl();

            $dname=BOL_UserService::getInstance()->getDisplayName($value['id_sender']);
            $uurl=BOL_UserService::getInstance()->getUserUrl($value['id_sender']);
            $uimg=BOL_AvatarService::getInstance()->getAvatarUrl($value['id_sender']);
$imghtml="";
            if ($uimg) {
                $imghtml="<img alt=\"".$dname."\" title=\"".$dname."\" src=\"".$uimg."\">";
            }
$seotitle=NEWS_BOL_Service::getInstance()->make_seo_url(stripslashes($value['topic_name']));
if (!$value['topic_name']){
    $value['topic_name']=OW::getLanguage()->text('news', 'notitle');
}

$content_ed="";
if ($is_admin OR $value['id_sender']==$id_user){
                $del_link="&nbsp;<a href=\"".$curent_url."newsx/delete/".$value['id']."/?delit=true&ss=".substr(session_id(),3,3)."&fp=".$curent_page."\" onclick=\"return confirm('Are you sure you want to delete?');\" title=\"Delete this page\">";
//                $del_link="<b style=\"color:#f00;\">[-]</b>";
                $del_link .="<img src=\"".$pluginStaticDir."delete.gif\">";
                $del_link .="</a>&nbsp;";

                $edit_link ="<a href=\"".$curent_url."newsx/edit/".$value['id']."\" title=\"Edit this page\">";
//                $edit_link="<b style=\"color:#080;\">[*]</b>";
                $edit_link .="<img src=\"".$pluginStaticDir."edit3.gif\">";
                $edit_link .="</a>";

                $content_ed= "<div class=\"ow_right ow_stdmargin\">".$del_link.$edit_link."</div>";
}





            $content .="<div class=\"ow_ipc ow_stdmargin clearfix\">";

    $content .="<div class=\"ow_ipc_picture\">
        <a href=\"".$uurl."\">
                    <div class=\"ow_avatar\">".$imghtml."</div>
        </a>
    </div>";

    $content .="<div class=\"ow_ipc_info\">
        <div class=\"ow_ipc_header\">                                   
                <h1><a style=\"font-size:initial;\" href=\"".$curent_url."news/".$value['id']."/".$seotitle.".html\" title=\"".stripslashes($value['topic_name'])."\">".stripslashes($value['topic_name'])."</a>".$content_ed."</h1>

                </div>          <div class=\"ow_ipc_content comment more\">".stripslashes($value['content'])."</div>
                    <div class=\"clearfix\">
            <div class=\"ow_ipc_toolbar ow_remark\">";

//$content .="

            $content .="<div class=\"ow_nowrap ow_icon_control ow_ic_user\" style=\"padding: 0px 0 2px 20px;\">
                <a href=\"".$uurl."\">".$dname."</a>                        
            </div>
                                            <span class=\"ow_nowrap ow_ipc_date\">
                                ".date("d-m-Y H:i:s",$value['data_added'])."
                                </span>
                            <span class=\"ow_delimiter\">·</span>";

if ($is_admin OR $value['id_sender']==$id_user){    
            if ($value['active']!="1"){
                $content .="<span class=\"ow_nowrap\">
                <span class=\"ow_wrap_normal\">".OW::getLanguage()->text('news', 'is_active').": <b style=\"color:#f00;\">".OW::getLanguage()->text('news', 'no')."</b></span>
                </span>";
            }else{
                $content .="<span class=\"ow_nowrap\">
                <span class=\"ow_wrap_normal\">".OW::getLanguage()->text('news', 'is_active').": <b>".OW::getLanguage()->text('news', 'yes')."</b></span>
                </span>";
            }
            $content .= "<span class=\"ow_delimiter\">·</span>";
            if ($value['is_published']!="1"){
                $content .="<span class=\"ow_nowrap\">
                <span class=\"ow_wrap_normal\">".OW::getLanguage()->text('news', 'is_approved').": <b style=\"color:#f00;\">".OW::getLanguage()->text('news', 'no')."</b></span>
                </span>";
            }else{
                $content .="<span class=\"ow_nowrap\">
                <span class=\"ow_wrap_normal\">".OW::getLanguage()->text('news', 'is_approved').": <b>".OW::getLanguage()->text('news', 'yes')."</b></span>
                </span>";
            }
            $content .= "<span class=\"ow_delimiter\">·</span>";
}

if ($value['mlat']!="" AND $value['mlon']!=""){
                $content .="<span class=\"ow_nowrap\">
                <span class=\"ow_wrap_normal\"><b><a href=\"http://maps.google.pl/?q=".$value['mlat'].",".$value['mlon']."\" target=\"blank\">".OW::getLanguage()->text('news', 'show_ongooglemap')."</b></a></span>
                </span>";
                $content .= "<span class=\"ow_delimiter\">·</span>";
}

//                            $content .="<span class=\"ow_nowrap\">
//                                <span class=\"ow_wrap_normal\">Tags: <a href=\"http://test3.a6.pl/blogs/list/browse-by-tag?tag=ee\">ee</a></span>
//                            </span>";

                        $content .="</div>
            </div>
           </div>
            </div>";


if ($value['ext_footer']){
    $content .="<div class=\"clearfix ow_center\">";
    $content .="<div class=\"clearfix ow_right\">";
    $content .=stripslashes($value['ext_footer']);
    $content .="</div>";
    $content .="</div>";
    $content .="<hr/>";
}

                $content=NEWS_BOL_Service::getInstance()->make_tabs('zoom',$content);


            }else{
                OW::getFeedback()->info(OW::getLanguage()->text('news', 'acticle_not_found'));
                OW::getApplication()->redirect($curent_url."news");
                exit;
            }
        }else{//--------------------------------------------------------------------list


            $per_page=20;
            $start_form=0;
            if (isset($_GET['page']) AND $_GET['page']>1){
                $curent_page=$_GET['page'];
            }else{
                $curent_page=1;
            }

            $start_form=(($curent_page-1)*$per_page);
            if (!$start_form) $start_form=0;

        

//                if (!$option) {
                if (!isset($_GET['page'])) {
                    $limit=$limit_all;
                }else {
                    $limit=$start_form.",".$per_page;
                }

                $add=" ";
                if (!$is_admin) $add=" nn.active='1' AND nn.is_published='1' ";            
                    else $add=" 1 ";            
        if ($ccat>0){
            $add .=" AND nn.id_topic='".addslashes($ccat)."' ";
        }

        $query = "SELECT * FROM " . OW_DB_PREFIX. "news nn 
            LEFT JOIN " . OW_DB_PREFIX. "news_content nnc ON (nnc.id_news=nn.id) 
        WHERE ".$add." ORDER BY nn.data_added DESC LIMIT ".$limit;//location



        $sqlll = "SELECT COUNT(*) as allp FROM " . OW_DB_PREFIX. "news nn 
        WHERE ".$add;
//--all s
                        $arrll = OW::getDbo()->queryForList($sqlll);
                        if (isset($arrll['0'])){
                            $all_results=$arrll['0']['allp'];
                        }else{
                            $all_results=0;
                        }
//--all e



$content_f="";

        $arr = OW::getDbo()->queryForList($query);
        foreach ( $arr as $value ){
/*
            $content_f .="<div class=\"ow_content clearfix\">
                <div class=\"clearfix\">
                    <a href=\"http://www.oxwall.a6.pl/fanpage/autokarowe_przewozy_bis_14762\"><b>.Autokarowe Przewozy BIS</b></a>
                </div>
                <div class=\"clearfix text ow_remark\" style=\"font-size:10px;\"><b>City:</b> 81-533 Gdynia; <b>Street:</b> Lidzka 47; </div>
                <div class=\"clearfix text ow_remark\" style=\"font-size:10px;\"><b>Phone:</b> 601 63 23 83; <b>Fax:</b> (058) 664 87 07; </div>
                <div class=\"clearfix text ow_remark\" style=\"font-size:10px;\"><b>URL:</b> www.busbis.of.pl; </div>
            </div>";
*/
    $pluginStaticDir =OW::getPluginManager()->getPlugin('news')->getStaticUrl();

            $dname=BOL_UserService::getInstance()->getDisplayName($value['id_sender']);
            $uurl=BOL_UserService::getInstance()->getUserUrl($value['id_sender']);
            $uimg=BOL_AvatarService::getInstance()->getAvatarUrl($value['id_sender']);
$imghtml="";
            if ($uimg) {
                $imghtml="<img alt=\"".$dname."\" title=\"".$dname."\" src=\"".$uimg."\">";
            }
$seotitle=NEWS_BOL_Service::getInstance()->make_seo_url(stripslashes($value['topic_name']));
if (!$value['topic_name']){
    $value['topic_name']=OW::getLanguage()->text('news', 'notitle');
}


$content_ed="";
if ($is_admin OR $value['id_sender']==$id_user){
                $del_link="&nbsp;<a href=\"".$curent_url."newsx/delete/".$value['id']."/?delit=true&ss=".substr(session_id(),3,3)."&fp=".$curent_page."\" onclick=\"return confirm('Are you sure you want to delete?');\" title=\"Delete this page\">";
//                $del_link="<b style=\"color:#f00;\">[-]</b>";
                $del_link .="<img src=\"".$pluginStaticDir."delete.gif\">";
                $del_link .="</a>&nbsp;";

                $edit_link ="<a href=\"".$curent_url."newsx/edit/".$value['id']."\" title=\"Edit this page\">";
//                $edit_link="<b style=\"color:#080;\">[*]</b>";
                $edit_link .="<img src=\"".$pluginStaticDir."edit3.gif\">";
                $edit_link .="</a>";

                $content_ed= "<div class=\"ow_right ow_stdmargin\">".$del_link.$edit_link."</div>";
}


            $content_f .="<div class=\"ow_ipc ow_stdmargin clearfix\">";

/*
    $content_f .="<div class=\"ow_ipc_picture\">
        <a href=\"".$uurl."\">
                    <div class=\"ow_avatar\">".$imghtml."</div>
        </a>
    </div>";
*/
    $content_f .="<div class=\"ow_ipc_info\" style=\"margin: auto;\">
        <div class=\"ow_ipc_header\">                                     <a href=\"".$curent_url."news/".$value['id']."/".$seotitle.".html\" title=\"".stripslashes($value['topic_name'])."\">".stripslashes($value['topic_name'])."</a>
                ".$content_ed."</div>          <div class=\"ow_ipc_content comment more\">".stripslashes($value['content'])."</div>
                    <div class=\"clearfix\">
            <div class=\"ow_ipc_toolbar ow_remark\">";

//$content_f .="


            $content_f .="<div class=\"ow_nowrap ow_icon_control ow_ic_user\" style=\"padding: 0px 0 2px 20px;\">
                <a href=\"".$uurl."\">".$dname."</a>                        
            </div>
                                            <span class=\"ow_nowrap ow_ipc_date\">
                                ".date("d-m-Y H:i:s",$value['data_added'])."
                                </span>
                            <span class=\"ow_delimiter\">·</span>";

//                            $content_f .="<span class=\"ow_nowrap\">
//                                <span class=\"ow_wrap_normal\">Tags: <a href=\"http://test3.a6.pl/blogs/list/browse-by-tag?tag=ee\">ee</a></span>
//                            </span>";
if ($is_admin OR $value['id_sender']==$id_user){    
            if ($value['active']!="1"){
                $content_f .="<span class=\"ow_nowrap\">
                <span class=\"ow_wrap_normal\">".OW::getLanguage()->text('news', 'is_active').": <b style=\"color:#f00;\">".OW::getLanguage()->text('news', 'no')."</b></span>
                </span>";
            }else{
                $content_f .="<span class=\"ow_nowrap\">
                <span class=\"ow_wrap_normal\">".OW::getLanguage()->text('news', 'is_active').": <b>".OW::getLanguage()->text('news', 'yes')."</b></span>
                </span>";
            }
            $content_f .= "<span class=\"ow_delimiter\">·</span>";
            if ($value['is_published']!="1"){
                $content_f .="<span class=\"ow_nowrap\">
                <span class=\"ow_wrap_normal\">".OW::getLanguage()->text('news', 'is_approved').": <b style=\"color:#f00;\">".OW::getLanguage()->text('news', 'no')."</b></span>
                </span>";
            }else{
                $content_f .="<span class=\"ow_nowrap\">
                <span class=\"ow_wrap_normal\">".OW::getLanguage()->text('news', 'is_approved').": <b>".OW::getLanguage()->text('news', 'yes')."</b></span>
                </span>";
            }
            $content_f .= "<span class=\"ow_delimiter\">·</span>";
}

if ($value['mlat']!="" AND $value['mlon']!=""){
                $content_f .="<span class=\"ow_nowrap\">
                <span class=\"ow_wrap_normal\"><b><a href=\"http://maps.google.pl/?q=".$value['mlat'].",".$value['mlon']."\" target=\"blank\">".OW::getLanguage()->text('news', 'show_ongooglemap')."</b></a></span>
                </span>";
                $content_f .= "<span class=\"ow_delimiter\">·</span>";
}
                

            $content_f .="</div>
            </div>
           </div>
            </div>";

        }//for

        $paging ="";

//        $paging =NEWS_BOL_Service::getInstance()->makePagination($curent_page, $all_results, $per_page, 1, $curent_url."news/page?".$add_paramurl,"&page=");
        $paging =NEWS_BOL_Service::getInstance()->makePagination($curent_page, $all_results, $per_page, 1, $curent_url."news/?ac=1&cat=".$ccat.$add_paramurl,"&page=");

        $menu=NEWS_BOL_Service::getInstance()->make_menu($ccat);

//$menu="<div class=\"ow_left\">".NEWS_BOL_Service::getInstance()->make_menu()."</div>";
//$content="<div class=\"ow_superwide ow_right\">".$content."</div>";
        if (!$content_f){
            $content_f="<h1>".OW::getLanguage()->text('news', 'searchfraze_nofound')."</h1>";
        }
$content="<table style=\"width:100%;\"><tr><td valign=\"top\" class=\"ow_nowrap\" style=\"min-width:150px;max-width:300px;\">".$menu."</td><td valign=\"top\" style=\"width:100%;\">".$content_f."</td></tr></table>";

//        $content=NEWS_BOL_Service::getInstance()->make_tabs('news',$paging.$menu.$content.$paging);
        $content=NEWS_BOL_Service::getInstance()->make_tabs('news',$paging.$content.$paging);
//$content =$paging.$content.$paging;

/*

$content .="<style>
.morecontent span {
    display: none;
}
.morecontent div {
    display: none;
}
.comment {
    margin: 10px;
}
</style>";



//<div style="float:right;"><a class="ow_lbutton" href="http://test3.a6.pl/news/sidebar_4/scientists_await_new_worlds_as_cern_collider_is_refitted.html">More</a></div>



$content .="<script>
    var showChar = 250;
    var ellipsestext = '...';
    var moretext = '".OW::getLanguage()->text('news', 'more')."';
    var lesstext = '".OW::getLanguage()->text('news', 'less')."';
    $('.more').each(function() {
        var content = $(this).html();

        if (content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar-1, content.length - showChar);
 
//            var html = c + '<span class=\"moreellipses\">' + ellipsestext+ '&nbsp;</span><span class=\"morecontent\"><span>' + h + '</span>&nbsp;&nbsp;<a href=\"\" class=\"ow_lbutton amorelink\">' + moretext + '</a></span>';
//            var html = c + '<span class=\"moreellipses\">' + ellipsestext+ '&nbsp;</span><div class=\"morecontent\"><div style=\"border:1px solid #f00;\">' + h + '</div>&nbsp;&nbsp;<a href=\"\" class=\"ow_lbutton amorelink\">' + moretext + '</a></div>';
//            var html = c + '<span class=\"moreellipses\">' + ellipsestext+ '&nbsp;</span><div class=\"morecontent\"><div style=\"border:1px solid #f00;\">' + h + '</div></div>&nbsp;&nbsp;<a href=\"\" class=\"ow_lbutton amorelink\">' + moretext + '</a>';
            var html = c + '<span class=\"moreellipses\">' + ellipsestext+ '&nbsp;</span><div class=\"morecontent\"><span style=\"border:1px solid #f00;\">' + h + '</span></div>&nbsp;&nbsp;<a href=\"\" class=\"ow_lbutton amorelink\">' + moretext + '</a>';
 
            $(this).html(html);
        }
 
    });
 
    $('.amorelink').click(function(){
        if($(this).hasClass('less')) {
            $(this).removeClass('less');
            $(this).html(moretext);
        } else {
            $(this).addClass('less');
            $(this).html(lesstext);
        }
//        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
</script>";
*/
        }//if not zoom - end list

    }else{//------------------------------------------------------------------------------------------------------------------RSSS

        if (!isset($params['idnews'])) {
            $params['idnews']="";
            $rss_id=0;
            $rss_pos=0;
//    OW::getApplication()->redirect($curent_url."news");
        }else{
            $rss_id=0;
            $rss_pos=0;
            $op="";
            if (isset($params['idnews'])){
                $xx=explode("_",$params['idnews']);
                if (isset($xx[0])) $rss_id=$xx[0];
                if (isset($xx[1])) $rss_pos=$xx[1];
                if (isset($xx[2])) $op=$xx[2];
//                list($rss_id,$rss_pos,$op)=explode("_",$params['idnews']);
            }
        }
        if (!isset($params['title'])) $params['title']="";

    //    if ($params['idnews']>0 AND $params['title']!=""){
//echo "sdfsdF";exit;



        if ($rss_id!="" AND $rss_pos!=""){//---zoom start
//            $title="---".$rss_id."-".$rss_pos;
//            $this->setPageTitle($title); //title page
            $url="";
            if ($rss_id=="main"){
                $url=OW::getConfig()->getValue('news', 'rss_url');
            }else if ($rss_id=="sidebar"){
                $url=OW::getConfig()->getValue('news', 'rss_url_sidebar');
            }else if ($rss_id=="dashboard"){
                $url=OW::getConfig()->getValue('news', 'rss_url_dashboard');
            }else if ($rss_id=="ather1"){
                $url=OW::getConfig()->getValue('news', 'rss_url_ather1');
            }else if ($rss_id=="ather2"){
                $url=OW::getConfig()->getValue('news', 'rss_url_ather2');
            }else if ($rss_id=="ather3"){
                $url=OW::getConfig()->getValue('news', 'rss_url_ather3');
            }
//echo $url;exit;
            if (!$url){
                for ($i=4;$i<11;$i++){
                    if ($rss_id=="ather".$i){
                        $url=OW::getConfig()->getValue('news', 'rss_url_ather'.$i);
                        break;
                    }
                }
            }

            if ($url){
//                $rss_parser = new newsRSSreader_NEWS($url);
//                $content .= $rss_parser->getOutputZoom($rss_id,$rss_pos); // returns string containing HTML

//                NEWS_BOL_Service::getInstance()->startRSSreader($url);
//                $content .= NEWS_BOL_Service::getInstance()->getOutputZoom($rss_id,$rss_pos); // returns string containing HTML
                if ($op!="zoom"){
                    $content .= NEWS_BOL_Service::getInstance()->make_for_zoom($url,$rss_id,$rss_pos); // returns string containing HTML
                }else{
                    $content .= NEWS_BOL_Service::getInstance()->make_one_zoom($url,$rss_id,$rss_pos); // returns string containing HTML
                }
            }else{
                OW::getApplication()->redirect($curent_url."news");
            }

//            $content .=NEWS_BOL_Service::getInstance()->test('aaaaa');
//            $content .=$title;
        }else{//---page start (zoom end)
//            $content .="....news...todo";
//            $content .="<div class=\"ow_content\">";

//echo "sfdsdfSDF";exit;
$length_title="";
$length_content="";
            $url=OW::getConfig()->getValue('news', 'rss_url_ather1');
            if ($url){
//            if ($url){
                $items=5;
                $items=OW::getConfig()->getValue('news', 'rss_items_ather1');
                if (!$items) $items=5;

                $rss_id="ather1";
//                $rss_pos=0;
                $rss_pos="ather1";//id or name
                $rss_type="big";

//                NEWS_BOL_Service::getInstance()->startRSSreader($url);
//                $content .= NEWS_BOL_Service::getInstance()->getOutput($items,$rss_id,$rss_pos,$rss_type,$length_title,$length_content); // returns string containing HTML
//                $content .= NEWS_BOL_Service::getInstance()->make_one_zoom($url,$rss_id,$rss_pos); // returns string containing HTML
                $content .= NEWS_BOL_Service::getInstance()->make_for_zoom($url,$rss_id,$rss_pos);
            }

            $url=OW::getConfig()->getValue('news', 'rss_url_ather2');
            if ($url){
                $items=5;
                $items=OW::getConfig()->getValue('news', 'rss_items_ather2');
                if (!$items) $items=5;

                $rss_id="ather2";
//                $rss_pos=0;
                $rss_pos="ather2";//id or name
                $rss_type="big";

//                NEWS_BOL_Service::getInstance()->startRSSreader($url);
//                $content .= NEWS_BOL_Service::getInstance()->getOutput($items,$rss_id,$rss_pos,$rss_type,$length_title,$length_content); // returns string containing HTML
                $content .= NEWS_BOL_Service::getInstance()->make_for_zoom($url,$rss_id,$rss_pos);
            }

            $url=OW::getConfig()->getValue('news', 'rss_url_ather3');
            if ($url){
                $items=5;
                $items=OW::getConfig()->getValue('news', 'rss_items_ather3');
                if (!$items) $items=5;

                $rss_id="ather3";//id or name
//                $rss_pos=0;
                $rss_pos="ather3";//id or name
                $rss_type="big";

//            $rss_type="short";
//            $content .= NEWS_BOL_Service::getInstance()->getOutput($items,$rss_id,$rss_pos,$rss_type,$length_title,$length_content); // returns string containing HTML

//                NEWS_BOL_Service::getInstance()->startRSSreader($url);
//                $content .= NEWS_BOL_Service::getInstance()->getOutput($items,$rss_id,$rss_pos,$rss_type,$length_title,$length_content); // returns string containing HTML
                $content .= NEWS_BOL_Service::getInstance()->make_for_zoom($url,$rss_id,$rss_pos);
            }


            for ($i=4;$i<11;$i++){
                $namex='rss_url_ather'.$i;
                $url=OW::getConfig()->getValue('news', $namex);
                if ($url){
                    $items=5;
                    $namex='rss_items_ather'.$i;
                    $items=OW::getConfig()->getValue('news', $namex);
                    if (!$items) $items=5;

                    $rss_id="ather".$i;//id or name
//                    $rss_pos=0;
                    $rss_pos="ather".$i;//id or name
                    $rss_type="big";

//                    NEWS_BOL_Service::getInstance()->startRSSreader($url);
//                    $content .= NEWS_BOL_Service::getInstance()->getOutput($items,$rss_id,$rss_pos,$rss_type,$length_title,$length_content); // returns string containing HTML
                    $content .= NEWS_BOL_Service::getInstance()->make_for_zoom($url,$rss_id,$rss_pos);
                }
            }




//            $content .="</div>";
        }//---page end


        }//rss end ----------------------------------------------------------------------------------------------------------------RSS END --------------------------

        $this->assign('content', $content);
 
    }


	    public function editpage($params) 
    {

//            OW::getApplication()->redirect($curent_url."shop");
        $this->assign('content', $content);

    }
	
	
    public function delcat($params)
    {
        $content="";
                    $id_user = OW::getUser()->getId();//citent login user (uwner)
                    $is_admin = OW::getUser()->isAdmin();//iss admin

                    $curent_url = 'http';
                    if (isset($_SERVER["HTTPS"])) {$curent_url .= "s";}
                    $curent_url .= "://";
                    $curent_url .= $_SERVER["SERVER_NAME"]."/";
$curent_url=OW_URL_HOME;                                        
/*
$from_config=$curent_url;
$from_config=str_replace("https://","",$from_config);
$from_config=str_replace("http://","",$from_config);
$trash=explode($from_config,$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
$url_detect=$trash[1];
//print_r($trash);
//echo $url_detect;
*/

            if ($is_admin AND $id_user>0){//edit dir
                if ($params['optionadm']=="del" AND $params['idcat']>0){
                    $query = "DELETE FROM " . OW_DB_PREFIX. "news_categories WHERE id='".addslashes($params['idcat'])."' LIMIT 1";
                    $arr = OW::getDbo()->query($query);
                    $query = "UPDATE " . OW_DB_PREFIX. "news_products SET cat_id='0' WHERE cat_id='".addslashes($params['idcat'])."' ";
                    $arr = OW::getDbo()->query($query);
                    
                }

            }

//            $this->assign('content', $content);
            OW::getApplication()->redirect($curent_url."shop");
    }

	
    public function editcat($params)
    {
        $content="";
                    $id_user = OW::getUser()->getId();//citent login user (uwner)
                    $is_admin = OW::getUser()->isAdmin();//iss admin


                    $curent_url = 'http';
                    if (isset($_SERVER["HTTPS"])) {$curent_url .= "s";}
                    $curent_url .= "://";
                    $curent_url .= $_SERVER["SERVER_NAME"]."/";
$curent_url=OW_URL_HOME;                                        
/*
$from_config=$curent_url;
$from_config=str_replace("https://","",$from_config);
$from_config=str_replace("http://","",$from_config);
$trash=explode($from_config,$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
$url_detect=$trash[1];
//print_r($trash);
//echo $url_detect;
*/

            if (!$is_admin OR !$id_user>0){//edit dir
                OW::getApplication()->redirect($curent_url."shop");
            }
$value['id']="";
        $ctitle="";
        $active=0;
        $csort=0;
            if ($params['optionadm']=="edit" AND $params['idcat']>0){
                $query = "SELECT * FROM " . OW_DB_PREFIX. "news_categories WHERE id='".addslashes($params['idcat'])."' LIMIT 1";
                $arr = OW::getDbo()->queryForList($query);
                $value=$arr[0];
                $ctitle=stripslashes($value['name']);
                $active=$value['active'];
                $csort=$value['sort'];
            }

//        $content .="<form>";
//            $content .="<form action=\"".$curent_url."product/".$params['idproduct']."/adds\" method=\"POST\" enctype=\"multipart/form-data\">";
            $content .="<form action=\"".$curent_url."shop\" method=\"POST\" >";
        if ($value['id']>0){
            $content .="<input type=\"hidden\" name=\"caction\" value=\"update\">";
            $content .="<input type=\"hidden\" name=\"cactionid\" value=\"".$value['id']."\">";
        }else{
            $content .="<input type=\"hidden\" name=\"caction\" value=\"insert\">";
        }
        

        $content .="<table>";

        $content .="<tr>";
        $content .="<td>";
        $content .="<b>".OW::getLanguage()->text('news', 'product_table_category').":</b>";
        $content .="</td>";
        $content .="<td>";
        $content .="<input type=\"text\" name=\"ctitle\" value=\"".$ctitle."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td>";
        $content .="<b>".OW::getLanguage()->text('news', 'product_table_active').":</b>";
        $content .="</td>";
        $content .="<td>";
        if ($active){
            $sel=" checked ";
        }else{
            $sel=" ";
        }
        $content .="<input ".$sel." type=\"checkbox\" name=\"cactive\" value=\"1\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td>";
        $content .="<b>".OW::getLanguage()->text('news', 'product_table_sort').":</b>";
        $content .="</td>";
        $content .="<td>";
        $content .="<input type=\"text\" name=\"csort\" value=\"".$csort."\">";
        $content .="</td>";
        $content .="</tr>";


            $content .="<tr>";
            $content .="<td valign=\"top\">";
            $content .="</td>";
            $content .="<td align=\"right\" valign=\"top\">";
            $content .="<input type=\"submit\" name=\"submit\" value=\"Submit\" />";
            $content .="</td>";
            $content .="</tr>";


        $content .="</table>";
        $content .="</form>";

            $this->assign('content', $content);
    }
	
	
	
	
	public function make_menu()
	{
	           $content="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
$curent_url=OW_URL_HOME;                                        
$from_config=$curent_url;
		
//        if ($url_detect=="shop" OR $url_detect=="news" OR $url_detect=="news_adm" OR $url_detect=="basket" OR $url_detect=="order" OR $url_detect=="product"){
            if ($id_user>0 AND $is_admin){
                $query = "SELECT * FROM " . OW_DB_PREFIX. "news_topics 
                        ORDER BY active DESC, sort DESC, name";
            }else{
                $query = "SELECT * FROM " . OW_DB_PREFIX. "news_topics 
                        WHERE active  = '1' ORDER BY sort DESC, name";
            }

            $content_menu="";
            $arr = OW::getDbo()->queryForList($query);
            foreach ( $arr as $value )
            {
                

                if ($is_admin){
                    $content_menu .="<li style=\"list-style-position: inside;list-style-type: none;font-size:10px;\">";


                    $content_menu .="<a href=\"".$curent_url."news_adm/del/".$value['id']."\" onclick=\"return confirm('Are you sure you want to delete?');\"   title=\"".OW::getLanguage()->text('news', 'menu_delete_cat')."\">";
                    $content_menu .="<b style=\"color:#f00;\">[-]</b>";
                    $content_menu .="</a>";
//                    $content_menu .="&nbsp;|&nbsp;";
                    $content_menu .="<a href=\"".$curent_url."news_adm/edit/".$value['id']."\" title=\"".OW::getLanguage()->text('news', 'menu_edit_cat')."\">";
                    $content_menu .="<b style=\"color:#080;\">[*]</b>";
                    $content_menu .="</a>";
                    $content_menu .="&nbsp;&nbsp;";

                }else{
                    $content_menu .="<li style=\"list-style-position: inside;list-style-type: disc;padding-left:0px;margin-left:0px;\">";
                }

                $content_menu .="<a href=\"".$curent_url."news/".$value['id']."\">";
                if (!$value['active']) $content_menu .="<i style=\"color:#f00;\">";
                $title=stripslashes($value['name']);
                if (OW::getConfig()->getValue('news', 'max_cat_title_chars')>0){
                    $max=OW::getConfig()->getValue('news', 'max_cat_title_chars');
                }else{
                    $max=20;
                }
                $title=mb_substr($title,0,$max);
                $content_menu .=$title;
                if (!$value['active']) $content_menu .="</i>";
                $content_menu .="</a>";


                $content_menu .="</li>";
            }

            if ($is_admin){
                $content .="<a href=\"".$curent_url."news_adm/add/new\" title=\"".OW::getLanguage()->text('news', 'menu_addnew_cat')."\">";
                $content .="<b style=\"color:#f00;\">[+]&nbsp;".OW::getLanguage()->text('news', 'menu_addnew_cat')."</b>";
                $content .="</a>";
                $content .="<hr>";
            }

            if ($content_menu){
                $content .="<div style=\"margin:5px;float:left;width:180px;\"><ul>".$content_menu."</ul></div>";
            }
/*
        }else{
            $content  ="<script>\n";
            $content .="\$(document).ready(function(){\n";
            $content .="\$(\".index-NEWS_CMP_MenuWidget\").empty().remove();\n";
            $content .="    });\n";
            $content .="</script>";
        }
*/
		return $content;
	}
	
	
	
    public function make_seo_url($name,$lengthtext=100)
    {
//        $seo_title=stripslashes($name);
        $seo_title=$name;

        $arrPlSpecialChars = array('ą','ć','ę','ł','ń','ó','ś','ź','ż','Ą','Ć','Ę','Ł','Ń','Ó','Ś','Ź','Ż');
        $arrAsciiChars = array('a','c','e','l','n','o','s','z','z','A','C','E','L','N','O','S','Z','Z');
        $seo_title =str_replace($arrPlSpecialChars, $arrAsciiChars, $seo_title);

        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace(chr(160),"_",$seo_title);
        $seo_title=str_replace("~","",$seo_title);
        $seo_title=str_replace("(","",$seo_title);
        $seo_title=str_replace(")","",$seo_title);
        $seo_title=str_replace("]","",$seo_title);
        $seo_title=str_replace("[","",$seo_title);
        $seo_title=str_replace("}","",$seo_title);
        $seo_title=str_replace("{","",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("\\","",$seo_title);
        $seo_title=str_replace("+","",$seo_title);
        $seo_title=str_replace(":","",$seo_title);
        $seo_title=str_replace(";","",$seo_title);
        $seo_title=str_replace("\"","",$seo_title);
        $seo_title=str_replace("<","",$seo_title);
        $seo_title=str_replace(">","",$seo_title);
        $seo_title=str_replace("?","",$seo_title);
        $seo_title=str_replace(",","_",$seo_title);
        $seo_title=str_replace(".","_",$seo_title);
        $seo_title=str_replace("!","",$seo_title);
        $seo_title=str_replace("`","",$seo_title);
        $seo_title=str_replace("'","",$seo_title);
        $seo_title=str_replace("@","",$seo_title);
        $seo_title=str_replace("#","",$seo_title);
        $seo_title=str_replace("$","",$seo_title);
        $seo_title=str_replace("%","",$seo_title);
        $seo_title=str_replace("^","",$seo_title);
        $seo_title=str_replace("&","",$seo_title);
        $seo_title=str_replace("*","",$seo_title);
        $seo_title=str_replace("|","",$seo_title);
        $seo_title=str_replace("=","",$seo_title);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("?","_",$seo_title);
        $seo_title=str_replace("#","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("&amp;","_",$seo_title);
        $seo_title=str_replace("__","_",$seo_title);
        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title=str_replace("_.",".",$seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }
	
	
	
	
	
    public function add_to_wall($last_insert_id=0,$action_name="",$url_to_ads="",$title_post="", $action_key="")
    {
//---------------------------------feed start
        $id_user = OW::getUser()->getId();//citent login user (uwner)
                    $curent_url = 'http';
                    if (isset($_SERVER["HTTPS"])) {$curent_url .= "s";}
                    $curent_url .= "://";
                    $curent_url .= $_SERVER["SERVER_NAME"]."/";
$curent_url=OW_URL_HOME;                                        
/*
$from_config=$curent_url;
$from_config=str_replace("https://","",$from_config);
$from_config=str_replace("http://","",$from_config);
$trash=explode($from_config,$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
$url_detect=$trash[1];
//print_r($trash);
//echo $url_detect;
*/
                    $is_ok=false;

//                if ($last_insert_id>0){
            if ($id_user AND $action_name AND ($url_to_ads AND $title_post) AND $last_insert_id>0){
//                    $timestamp=strtotime(date('Y-m-d',time()));
                    $timestamp=strtotime(date('Y-m-d h:i:s'));
//                    $subject="Added a new page";//from param
//                    $url_to_ads=$curent_url."html/".$id_user."/".$last_insert_id."/index.html";//from param

                    $title_post_lenght=255;
                    if ($action_key) {
                        $entityType=$action_key;//ident in wall!!!!!!!!!!!
                    }else{
                        $entityType="news_add_product";//ident in wall!!!!!!!!!!!
                    }
                    $pluginKey="news";//plugin key!!!!!

//                    $curent_url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
                    $array_toolbar[0]=array("href" => $url_to_ads, "label" => "URL");
                    $array=array(
                        "time" => "".$timestamp."",
                        "ownerId" => "".$id_user."",
                        "string" => "".NEWS_BOL_Service::getInstance()->safe_content($action_name)."",
                        "content" => "<a href=\"".$url_to_ads."\">".$url_to_ads."</a>\r\n<div class=\"ow_remark\" style=\"paddig-top: 4px\">".mb_substr($title_post,0,$title_post_lenght)."</div>",
                        "view" => array("iconClass" => "ow_ic_link"),
                        "toolbar" => array_values($array_toolbar)
                    );
                    $datadata=json_encode($array);

                    //------check for exist start
                    $query = "SELECT * FROM " . OW_DB_PREFIX. "newsfeed_action  WHERE entityId='".addslashes($last_insert_id)."' AND entityType='".addslashes($entityType)."' AND pluginKey='".addslashes($pluginKey)."' LIMIT 1";
                    $arrx = OW::getDbo()->queryForList($query);
                    $value=$arrx[0];
                    if (!$value['id']){
//                        $uniquer_prefix=OW::getConfig()->getValue('news', 'mode_uniquer_prefix');
                        $uniquer_prefix="_".md5(date('Y-m-d-H-m-s'));
                    }else{
                        $uniquer_prefix="";
                    }
                    //------check for exist end
//                if (!$value['id']){
                    $query = "INSERT INTO " . OW_DB_PREFIX. "newsfeed_action  (
                        id ,     entityId ,       entityType ,     pluginKey   ,    data
                    )VALUES(
                        '','".addslashes($last_insert_id)."','".addslashes($entityType.$uniquer_prefix)."','".addslashes($pluginKey)."','".addslashes($datadata)."'
                    )";
//echo "(1)".$query;
                    $last_insert_id_a = OW::getDbo()->insert($query);
                    if ($last_insert_id_a>0){
                        $query = "INSERT INTO " . OW_DB_PREFIX. "newsfeed_action_set  (
                            id , actionId ,       userId,  timestamp
                        )VALUES(
                            '','".addslashes($last_insert_id_a)."','".addslashes($id_user)."','".addslashes($timestamp)."'
                        )";
                        $last_insert_id_as = OW::getDbo()->insert($query);
                        if ($last_insert_id_as>0){
                            $query = "INSERT INTO " . OW_DB_PREFIX. "newsfeed_activity  (
                                id ,     activityType,    activityId ,     userId  ,data ,   actionId  ,      timeStamp ,      privacy ,visibility    ,  status
                            )VALUES(
                                '','create','1','".addslashes($id_user)."','[]','".addslashes($last_insert_id_a)."','".addslashes($timestamp)."','everybody','15','active'
                            )";
                            $last_insert_id_ac = OW::getDbo()->insert($query);
                            if ($last_insert_id_ac>0){
                                $query = "INSERT INTO " . OW_DB_PREFIX. "newsfeed_action_feed  (
                                    id , feedType ,feedId , activityId
                                )VALUES(
                                    '','user','1','".addslashes($last_insert_id_ac)."'
                                )";
                                $last_insert_id_af = OW::getDbo()->insert($query);
                                if (!$last_insert_id_af){
                                    $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action WHERE id='".addslashes($last_insert_id_a)."' LIMIT 1";
                                    $arr = OW::getDbo()->query($query); 
                                    $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action_set WHERE id='".addslashes($last_insert_id_as)."' LIMIT 1";
                                    $arr = OW::getDbo()->query($query); 
                                    $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_activity WHERE id='".addslashes($last_insert_id_ac)."' LIMIT 1";
                                    $arr = OW::getDbo()->query($query); 
                                }else{
                                    $is_ok=true;
                                }
                            }else{
                                $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action WHERE id='".addslashes($last_insert_id_a)."' LIMIT 1";
                                $arr = OW::getDbo()->query($query); 
                                $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action_set WHERE id='".addslashes($last_insert_id_as)."' LIMIT 1";
                                $arr = OW::getDbo()->query($query); 
                            }

                        }else{//end 2
                            $query="DELETE FROM " . OW_DB_PREFIX. "newsfeed_action WHERE id='".addslashes($last_insert_id_a)."' LIMIT 1";
                            $arr = OW::getDbo()->query($query); 
                        }
                    }//end1
//                }//if (!$value['id']){
            }
            return $is_ok;
//---------------------------------getInsertId

    }//func	
	
	
	
	
	
	
	
	
	
	
	
	
    public function sent()
    {
	
        $dept = null;

        if ( OW::getSession()->isKeySet('news.dept') )
        {
            $dept = OW::getSession()->get('news.dept');
            OW::getSession()->delete('news.dept');
        }
        else
        {
            $this->redirectToAction('index');
        }

        $feedback = $this->text('news', 'message_sent', ( $dept === null ) ? null : array('dept' => $dept));
        $this->assign('feedback', $feedback);

    }

    private function text( $prefix, $key, array $vars = null )
    {
        return OW::getLanguage()->text($prefix, $key, $vars);
    }
}



