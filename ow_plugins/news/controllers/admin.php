<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/



class NEWS_CTRL_Admin extends ADMIN_CTRL_Abstract
{

    public function dept()
    {

        $content="";
        $config = OW::getConfig();
        $curent_url = 'http';
        if (isset($_SERVER["HTTPS"])) {$curent_url .= "s";}
        $curent_url .= "://";
        $curent_url .= $_SERVER["SERVER_NAME"]."/";
$curent_url=OW_URL_HOME;                                        
/*
$from_config=$curent_url;
$from_config=str_replace("https://","",$from_config);
$from_config=str_replace("http://","",$from_config);
$trash=explode($from_config,$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
$url_detect=$trash[1];
//print_r($trash);
//echo $url_detect;
*/

        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin

        $this->setPageTitle(OW::getLanguage()->text('news', 'config_global_setting')); //title menu
        $this->setPageHeading(OW::getLanguage()->text('news', 'config_global_setting')); //title page


if (!isset($_POST['save_config'])) $_POST['save_config']="";


/*
        if ( !$config->configExists('news', 'mode') ){
            $config->addConfig('news', 'mode', "0", '');//make

            $config->saveConfig('news', 'mode', 0);//save
        }

//        $config->saveConfig('news', 'mode', 1);

        $photosPerPage = $config->getValue('news', 'mode');//read

echo "===================".$photosPerPage;
*/
//        $photosPerPage = $config->getValue('news', 'mode');//read
//echo "===================".$photosPerPage;
//print_r($_POST);
        if ($_POST['save_config']==session_id() AND $id_user>0 AND $is_admin){       

            $config->saveConfig('news', 'mode', $_POST['c_mode']);
            $config->saveConfig('news', 'widget_big_full', $_POST['c_widget_big_full']);
            $config->saveConfig('news', 'embed_video_width', $_POST['c_embed_video_width']);
            $config->saveConfig('news', 'embed_video_height', $_POST['c_embed_video_height']);
            $config->saveConfig('news', 'embed_image_width', $_POST['c_embed_image_width']);
//            $config->saveConfig('news', 'embed_image_height', $_POST['c_embed_image_height']);

            $config->saveConfig('news', 'allow_add_for_users', $_POST['c_allow_add_for_users']);

    if (OW::getConfig()->getValue('news', 'mode')=="news"){
/*
        BOL_ComponentAdminService::getInstance()->deleteWidget('NEWS_CMP_MenuWidget');
        BOL_ComponentAdminService::getInstance()->deleteWidget('NEWS_CMP_IndexWidget');
        BOL_ComponentAdminService::getInstance()->deleteWidget('NEWS_CMP_DashboardWidget');
*/

        $config->saveConfig('news', 'rss_can_1', $_POST['c_rss_can_1']);
        $config->saveConfig('news', 'rss_own_1', $_POST['c_rss_own_1']);
        $config->saveConfig('news', 'rss_item_1', $_POST['c_rss_item_1']);
        $config->saveConfig('news', 'rss_can_2', $_POST['c_rss_can_2']);
        $config->saveConfig('news', 'rss_own_2', $_POST['c_rss_own_2']);
        $config->saveConfig('news', 'rss_item_2', $_POST['c_rss_item_2']);
        $config->saveConfig('news', 'rss_can_3', $_POST['c_rss_can_3']);
        $config->saveConfig('news', 'rss_own_3', $_POST['c_rss_own_3']);
        $config->saveConfig('news', 'rss_item_3', $_POST['c_rss_item_3']);
        $config->saveConfig('news', 'rss_can_4', $_POST['c_rss_can_4']);
        $config->saveConfig('news', 'rss_own_4', $_POST['c_rss_own_4']);
        $config->saveConfig('news', 'rss_item_4', $_POST['c_rss_item_4']);
        $config->saveConfig('news', 'rss_can_5', $_POST['c_rss_can_5']);
        $config->saveConfig('news', 'rss_own_5', $_POST['c_rss_own_5']);
        $config->saveConfig('news', 'rss_item_5', $_POST['c_rss_item_5']);
        $config->saveConfig('news', 'rss_can_6', $_POST['c_rss_can_6']);
        $config->saveConfig('news', 'rss_own_6', $_POST['c_rss_own_6']);
        $config->saveConfig('news', 'rss_item_6', $_POST['c_rss_item_6']);
        $config->saveConfig('news', 'rss_can_7', $_POST['c_rss_can_7']);
        $config->saveConfig('news', 'rss_own_7', $_POST['c_rss_own_7']);
        $config->saveConfig('news', 'rss_item_7', $_POST['c_rss_item_7']);
        $config->saveConfig('news', 'rss_can_8', $_POST['c_rss_can_8']);
        $config->saveConfig('news', 'rss_own_8', $_POST['c_rss_own_8']);
        $config->saveConfig('news', 'rss_item_8', $_POST['c_rss_item_8']);
        $config->saveConfig('news', 'rss_can_9', $_POST['c_rss_can_9']);
        $config->saveConfig('news', 'rss_own_9', $_POST['c_rss_own_9']);
        $config->saveConfig('news', 'rss_item_9', $_POST['c_rss_item_9']);
        $config->saveConfig('news', 'rss_can_10', $_POST['c_rss_can_10']);
        $config->saveConfig('news', 'rss_own_10', $_POST['c_rss_own_10']);
        $config->saveConfig('news', 'rss_item_10', $_POST['c_rss_item_10']);

        $config->saveConfig('news', 'rss_topic_1', $_POST['c_rss_topic_1']);
        $config->saveConfig('news', 'rss_topic_2', $_POST['c_rss_topic_2']);
        $config->saveConfig('news', 'rss_topic_3', $_POST['c_rss_topic_3']);
        $config->saveConfig('news', 'rss_topic_4', $_POST['c_rss_topic_4']);
        $config->saveConfig('news', 'rss_topic_5', $_POST['c_rss_topic_5']);
        $config->saveConfig('news', 'rss_topic_6', $_POST['c_rss_topic_6']);
        $config->saveConfig('news', 'rss_topic_7', $_POST['c_rss_topic_7']);
        $config->saveConfig('news', 'rss_topic_8', $_POST['c_rss_topic_8']);
        $config->saveConfig('news', 'rss_topic_9', $_POST['c_rss_topic_9']);
        $config->saveConfig('news', 'rss_topic_10', $_POST['c_rss_topic_10']);

//rss_can_1
//rss_own_1
//rss_item_1

    }else{
/*
        BOL_ComponentAdminService::getInstance()->deleteWidget('NEWS_CMP_MenuWidget');
        BOL_ComponentAdminService::getInstance()->deleteWidget('NEWS_CMP_IndexWidget');
        BOL_ComponentAdminService::getInstance()->deleteWidget('NEWS_CMP_DashboardWidget');

        $cmpService = BOL_ComponentAdminService::getInstance();
        $widget = $cmpService->addWidget('NEWS_CMP_MenuWidget', false);
        $placeWidget = $cmpService->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_INDEX);
        $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_SIDEBAR,0);

        $widget = $cmpService->addWidget('NEWS_CMP_IndexWidget', false);
        $placeWidget = $cmpService->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_INDEX);
        $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_RIGHT,0);

        $widget = $cmpService->addWidget('NEWS_CMP_DashboardWidget', false);
        $placeWidget = BOL_ComponentAdminService::getInstance()->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_DASHBOARD);
        $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_RIGHT, 0);
*/

            $config->saveConfig('news', 'rss_items', $_POST['c_rss_items']);
            $config->saveConfig('news', 'rss_items_sidebar', $_POST['c_rss_items_sidebar']);
            $config->saveConfig('news', 'rss_items_dashboard', $_POST['c_rss_items_dashboard']);

            $config->saveConfig('news', 'rss_url', $_POST['c_rss_url']);
            $config->saveConfig('news', 'rss_url_sidebar', $_POST['c_rss_url_sidebar']);
            $config->saveConfig('news', 'rss_url_dashboard', $_POST['c_rss_url_dashboard']);

            $config->saveConfig('news', 'rss_title_length_main', $_POST['c_rss_title_length_main']);
            $config->saveConfig('news', 'rss_title_length_sidebar', $_POST['c_rss_title_length_sidebar']);
            $config->saveConfig('news', 'rss_title_length_dashboard', $_POST['c_rss_title_length_dashboard']);

            $config->saveConfig('news', 'rss_content_length_main', $_POST['c_rss_content_length_main']);
            $config->saveConfig('news', 'rss_content_length_sidebar', $_POST['c_rss_content_length_sidebar']);
            $config->saveConfig('news', 'rss_content_length_dashboard', $_POST['c_rss_content_length_dashboard']);

            $config->saveConfig('news', 'rss_cachetime_minuts', $_POST['c_rss_cachetime_minuts']);

            $config->saveConfig('news', 'rss_url_ather1', $_POST['c_rss_url_ather1']);
            $config->saveConfig('news', 'rss_url_ather2', $_POST['c_rss_url_ather2']);
            $config->saveConfig('news', 'rss_url_ather3', $_POST['c_rss_url_ather3']);

            $config->saveConfig('news', 'rss_items_ather1', $_POST['c_rss_items_ather1']);
            $config->saveConfig('news', 'rss_items_ather2', $_POST['c_rss_items_ather2']);
            $config->saveConfig('news', 'rss_items_ather3', $_POST['c_rss_items_ather3']);

            $config->saveConfig('news', 'rss_url_ather4', $_POST['c_rss_url_ather4']);
            $config->saveConfig('news', 'rss_items_ather4', $_POST['c_rss_items_ather4']);
            $config->saveConfig('news', 'rss_url_ather5', $_POST['c_rss_url_ather5']);
            $config->saveConfig('news', 'rss_items_ather5', $_POST['c_rss_items_ather5']);
            $config->saveConfig('news', 'rss_url_ather6', $_POST['c_rss_url_ather6']);
            $config->saveConfig('news', 'rss_items_ather6', $_POST['c_rss_items_ather6']);
            $config->saveConfig('news', 'rss_url_ather7', $_POST['c_rss_url_ather7']);
            $config->saveConfig('news', 'rss_items_ather7', $_POST['c_rss_items_ather7']);
            $config->saveConfig('news', 'rss_url_ather8', $_POST['c_rss_url_ather8']);
            $config->saveConfig('news', 'rss_items_ather8', $_POST['c_rss_items_ather8']);
            $config->saveConfig('news', 'rss_url_ather9', $_POST['c_rss_url_ather9']);
            $config->saveConfig('news', 'rss_items_ather9', $_POST['c_rss_items_ather9']);
            $config->saveConfig('news', 'rss_url_ather10', $_POST['c_rss_url_ather10']);
            $config->saveConfig('news', 'rss_items_ather10', $_POST['c_rss_items_ather10']);








    }

            OW::getApplication()->redirect($curent_url."admin/plugins/news");    
        }

$dname=BOL_UserService::getInstance()->getDisplayName($id_user);

        $content .="<form method=\"post\" action=\"".$curent_url."admin/plugins/news\">";
        $content .="<input type=\"hidden\" name=\"save_config\" value=\"".session_id()."\">";




        $content .="<table style=\"width:100%;\" class=\"ow_table_1 ow_form\">";


        $content .="<tr>";
        $content .="<td style=\"text-align:right;\" class=\"ow_label\">";
        $content .="<b>".OW::getLanguage()->text('news', 'allow_add_for_users').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $mode=$config->getValue('news', 'allow_add_for_users');
        $content .="<select name=\"c_allow_add_for_users\">";
        if ($mode=="1" OR $mode=="") $sel=" selected ";
            else $sel="";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('news', 'yes')."</option>";
        if ($mode=="0") $sel=" selected ";
            else $sel="";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('news', 'no')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr>";
        $content .="<td style=\"text-align:right;\" class=\"ow_label\">";
        $content .="<b>".OW::getLanguage()->text('news', 'widget_big_full').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $mode=$config->getValue('news', 'widget_big_full');
        $content .="<select name=\"c_widget_big_full\">";
        if ($mode=="1" ) $sel=" selected ";
            else $sel="";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('news', 'yes')."</option>";
        if ($mode=="0"OR $mode=="") $sel=" selected ";
            else $sel="";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('news', 'no')."</option>";
        $content .="</select>";

        $content .="</td>";
        $content .="</tr>";



        $content .="<tr>";
        $content .="<td style=\"text-align:right;\" class=\"ow_label\">";
        $content .="<b>".OW::getLanguage()->text('news', 'embed_video_width').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $rss_url=$config->getValue('news', 'embed_video_width');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_embed_video_width\" value=\"".$rss_url."\" style=\"width:100px;\">&nbsp;Default: 580[px]";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\" class=\"ow_label\">";
        $content .="<b>".OW::getLanguage()->text('news', 'embed_video_height').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $rss_url=$config->getValue('news', 'embed_video_height');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_embed_video_height\" value=\"".$rss_url."\" style=\"width:100px;\">&nbsp;Default: 387[px]";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\" class=\"ow_label\">";
        $content .="<b>".OW::getLanguage()->text('news', 'embed_image_width').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $rss_url=$config->getValue('news', 'embed_image_width');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_embed_image_width\" value=\"".$rss_url."\" style=\"width:100px;\">&nbsp;Default: 600[px]";
        $content .="</td>";
        $content .="</tr>";

/*
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\" class=\"ow_label\">";
        $content .="<b>".OW::getLanguage()->text('news', 'embed_image_height').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $rss_url=$config->getValue('news', 'embed_image_height');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_embed_image_height\" value=\"".$rss_url."\" style=\"width:100px;\">&nbsp;Default: 800[px]";
        $content .="</td>";
        $content .="</tr>";
*/









            $content .="<tr class=\"ow_alt1x\">";
            $content .="<td  class=\"ow_label\">";
            $content .="<b>".OW::getLanguage()->text('news', 'admin_select_mode').":</b>";
            $content .="</td>";
            $content .="<td  class=\"ow_value\">";
            $content .="<select name=\"c_mode\" id=\"c_mode\">";
            if (OW::getConfig()->getValue('news', 'mode')=="news" OR OW::getConfig()->getValue('news', 'mode')=="") $sel=" selected ";
                else  $sel=" ";
            $content .="<option ".$sel." value=\"news\">".OW::getLanguage()->text('news', 'mode_news')."</option>";
            if (OW::getConfig()->getValue('news', 'mode')=="rss") $sel=" selected ";
                else  $sel=" ";
            $content .="<option ".$sel." value=\"rss\">".OW::getLanguage()->text('news', 'mode_rss')."</option>";
            $content .="</select>";
            $content .="</td>";
            $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"\" colspan=\"2\">";
//        $content .="<input type=\"submit\" name=\"send\" value=\"".OW::getLanguage()->text('news', 'config_save')."\">";
        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('news', 'config_save')."\" name=\"send\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
        $content .="</td>";
        $content .="</tr>";


/*
//        if (OW::getPluginManager()->isPluginActive('shoppro')){
            $content .="<tr class=\"ow_alt1x\">";
            $content .="<td  class=\"ow_labelx\">";
            $content .="<b>".OW::getLanguage()->text('news', 'admin_select_mode').":</b>";
            $content .="</td>";
            $content .="<td  class=\"ow_valuex\">";
            $content .="<select name=\"c_mode\">";
            if (OW::getConfig()->getValue('news', 'mode')=="news" OR OW::getConfig()->getValue('news', 'mode')=="") $sel=" selected ";
                else  $sel=" ";
            $content .="<option ".$sel." value=\"news\">".OW::getLanguage()->text('map', 'no')."</option>";
            if (OW::getConfig()->getValue('news', 'mode')=="rss") $sel=" selected ";
                else  $sel=" ";
            $content .="<option ".$sel." value=\"rss\">".OW::getLanguage()->text('map', 'yes')."</option>";
            $content .="</select>";
            $content .="</td>";
            $content .="</tr>";
//        }
*/

        $content .="</table>";



$content .="<div id=\"mnews\">";

        $content .="<table style=\"width:100%;\">";

        $content .="<tr>";
        $content .="<td style=\"border-top:1px solid #ddd;text-align:right;\" colspan=\"2\">";

            $content .="<table style=\"margin:auto;\">";
            $content .="<tr class=\"ow_alt1x\">";
            $content .="<td  class=\"ow_label\">";
            $content .="<b>".OW::getLanguage()->text('news', 'admin_lp').":</b>";
            $content .="</td>";

            $content .="<td  class=\"ow_label\">";
            $content .="<b>".OW::getLanguage()->text('news', 'admin_rss_url').":</b>";
            $content .="</td>";

            $content .="<td  class=\"ow_label\">";
            $content .="<b>".OW::getLanguage()->text('news', 'admin_poster').":</b>";
            $content .="</td>";

            $content .="<td  class=\"ow_label\">";
            $content .="<b>".OW::getLanguage()->text('news', 'admin_iemposts').":</b>";
            $content .="</td>";

            $content .="<td  class=\"ow_label\">";
            $content .="<b>".OW::getLanguage()->text('news', 'admin_topic_category').":</b>";
            $content .="</td>";
            $content .="</tr>";

            for ($i=1;$i<11;$i++){
            $content .="<tr class=\"ow_alt1x\">";
            $content .="<td  class=\"ow_value\">";
            $content .="<b>".$i.")</b>";
            $content .="</td>";

            $content .="<td  class=\"ow_value\">";
            $val=$config->getValue('news', 'rss_can_'.$i);
            $content .="<b><input type=\"text\" name=\"c_rss_can_".$i."\" value=\"".$val."\" style=\"width:300px;max-width:380px;\"></b>";
            $content .="</td>";

            $content .="<td  class=\"ow_value\">";
            $val=$config->getValue('news', 'rss_own_'.$i);
            if (!$val) $val=$dname;
            $content .="<b><input type=\"text\" name=\"c_rss_own_".$i."\" value=\"".$val."\" style=\"max-width:130px;\"></b>";
            $content .="</td>";

            $content .="<td  class=\"ow_value\">";
            $val=$config->getValue('news', 'rss_item_'.$i);
            $content .="<b><input type=\"text\" name=\"c_rss_item_".$i."\" value=\"".$val."\" style=\"max-width:60px;\"></b>";
            $content .="</td>";

            $content .="<td  class=\"ow_value\">";
            $val=$config->getValue('news', 'rss_topic_'.$i);
//            $content .="<b><input type=\"text\" name=\"c_rss_topic_".$i."\" value=\"".$val."\" style=\"max-width:60px;\"></b>";
//$content .="---".$val;            
            $content .="<select name=\"c_rss_topic_".$i."\">";
            $content .=NEWS_BOL_Service::getInstance()->get_cat_select($val);
            $content .="</select>";

            $content .="</td>";
            $content .="</tr>";
            }

            $content .="</table>";

        $content .="</td>";
        $content .="</tr>";



        $content .="<tr>";
        $content .="<td style=\"border-top:1px solid #ddd;text-align:right;\" colspan=\"2\">";
//        $content .="<input type=\"submit\" name=\"send\" value=\"".OW::getLanguage()->text('news', 'config_save')."\">";
        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('news', 'config_save')."\" name=\"send\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
        $content .="</td>";
        $content .="</tr>";

        $content .="</table>";

$content .="</div>";


$content .="<div id=\"mrss\">";

        $content .="<fieldset style=\"border:1px solid #555;\">";
        $content .="<legend style=\"margin-left:20px;padding:10px;font-weight:bold;\">".strtoupper(OW::getLanguage()->text('news', 'setting_main')).":</legend>";
        $content .="<table style=\"width:100%;\">";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_cachetime_minuts').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_cachetime_minuts');
        if (!$rss_url) $rss_url="1";
        $content .="<input type=\"text\" name=\"c_rss_cachetime_minuts\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="</table>";
        $content .="</fieldset>";



        $content .="<fieldset style=\"border:1px solid #555;\">";
        $content .="<legend style=\"margin-left:20px;padding:10px;font-weight:bold;\">".strtoupper(OW::getLanguage()->text('news', 'setting_index')).":</legend>";
        $content .="<table style=\"width:100%;\">";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_items=$config->getValue('news', 'rss_items');
        if (!$rss_items) $rss_items=6;
        $content .="<input type=\"text\" name=\"c_rss_items\" value=\"".$rss_items."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_title_length_main').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_items=$config->getValue('news', 'rss_title_length_main');
        if (!$rss_items) $rss_items=55;
        $content .="<input type=\"text\" name=\"c_rss_title_length_main\" value=\"".$rss_items."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_content_length_main').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_items=$config->getValue('news', 'rss_content_length_main');
        if (!$rss_items) $rss_items=0;
        $content .="<input type=\"text\" name=\"c_rss_content_length_main\" value=\"".$rss_items."\">";
        $content .="</td>";
        $content .="</tr>";




        $content .="</table>";
        $content .="</fieldset>";





        $content .="<fieldset style=\"border:1px solid #555;\">";
        $content .="<legend style=\"margin-left:20px;padding:10px;font-weight:bold;\">".strtoupper(OW::getLanguage()->text('news', 'setting_sidebar')).":</legend>";
        $content .="<table style=\"width:100%;\">";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_sidebar');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_sidebar\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_items=$config->getValue('news', 'rss_items_sidebar');
        if (!$rss_items) $rss_items=6;
        $content .="<input type=\"text\" name=\"c_rss_items_sidebar\" value=\"".$rss_items."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_title_length_sidebar').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_items=$config->getValue('news', 'rss_title_length_sidebar');
        if (!$rss_items) $rss_items=20;
        $content .="<input type=\"text\" name=\"c_rss_title_length_sidebar\" value=\"".$rss_items."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_content_length_main').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_items=$config->getValue('news', 'rss_content_length_sidebar');
        if (!$rss_items) $rss_items=0;
        $content .="<input type=\"text\" name=\"c_rss_content_length_sidebar\" value=\"".$rss_items."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="</table>";
        $content .="</fieldset>";


        $content .="<fieldset style=\"border:1px solid #555;\">";
        $content .="<legend style=\"margin-left:20px;padding:10px;font-weight:bold;\">".strtoupper(OW::getLanguage()->text('news', 'setting_dashboard')).":</legend>";
        $content .="<table style=\"width:100%;\">";


        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_dashboard');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_dashboard\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_items=$config->getValue('news', 'rss_items_dashboard');
        if (!$rss_items) $rss_items=6;
        $content .="<input type=\"text\" name=\"c_rss_items_dashboard\" value=\"".$rss_items."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_title_length_dashboard').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_items=$config->getValue('news', 'rss_title_length_dashboard');
        if (!$rss_items) $rss_items=20;
        $content .="<input type=\"text\" name=\"c_rss_title_length_dashboard\" value=\"".$rss_items."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_content_length_main').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_items=$config->getValue('news', 'rss_content_length_dashboard');
        if (!$rss_items) $rss_items=0;
        $content .="<input type=\"text\" name=\"c_rss_content_length_dashboard\" value=\"".$rss_items."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="</table>";
        $content .="</fieldset>";





        $content .="<fieldset style=\"border:1px solid #555;\">";
        $content .="<legend style=\"margin-left:20px;padding:10px;font-weight:bold;\">".strtoupper(OW::getLanguage()->text('news', 'setting_ather_rssfeeds')).":</legend>";
        $content .="<table style=\"width:100%;\">";

//---1
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url_ather1').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_ather1');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_ather1\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_ather1').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_items_ather1');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_items_ather1\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";
//--2
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url_ather2').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_ather2');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_ather2\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_ather2').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_items_ather2');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_items_ather2\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";
//---3
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url_ather3').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_ather3');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_ather3\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_ather3').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_items_ather3');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_items_ather3\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

//---4
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url_ather4').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_ather4');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_ather4\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_ather4').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_items_ather4');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_items_ather4\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";
//---5
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url_ather5').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_ather5');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_ather5\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_ather5').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_items_ather5');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_items_ather5\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";
//---6
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url_ather6').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_ather6');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_ather6\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_ather6').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_items_ather6');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_items_ather6\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";
//---7
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url_ather7').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_ather7');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_ather7\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_ather7').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_items_ather7');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_items_ather7\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";
//---8
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url_ather8').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_ather8');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_ather8\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_ather8').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_items_ather8');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_items_ather8\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";
//---9
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url_ather9').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_ather9');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_ather9\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_ather9').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_items_ather9');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_items_ather9\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";
//---10
        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_url_ather10').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_url_ather10');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_url_ather10\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('news', 'config_rss_items_ather10').":</b>";
        $content .="</td>";
        $content .="<td>";
        $rss_url=$config->getValue('news', 'rss_items_ather10');
        if (!$rss_url) $rss_url="";
        $content .="<input type=\"text\" name=\"c_rss_items_ather10\" value=\"".$rss_url."\">";
        $content .="</td>";
        $content .="</tr>";

//--
        $content .="</table>";
        $content .="</fieldset>";



        $content .="<table style=\"width:100%;\">";
        $content .="<tr>";
        $content .="<td style=\"border-top:1px solid #ddd;text-align:right;\" colspan=\"2\">";
//        $content .="<input type=\"submit\" name=\"send\" value=\"".OW::getLanguage()->text('news', 'config_save')."\">";
        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('news', 'config_save')."\" name=\"send\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
        $content .="</td>";
        $content .="</tr>";
        $content .="</table>";


$content .="</div>";







        $content .="</form>";


$content .="<script type=\"text/javascript\">\n
$(document).ready(function () {\n
";

if (OW::getConfig()->getValue('news', 'mode')=="news" OR OW::getConfig()->getValue('news', 'mode')==""){
$content .="
$('#mnews').show();
$('#mrss').hide();
";
}else{
$content .="
$('#mnews').hide();
$('#mrss').show();
";
}

$content .="

$('#c_mode').on('change', function() {
//  alert( $(this).val()); 
    if ($(this).val()=='news'){
        $('#mnews').show();
        $('#mrss').hide();
    }else{
        $('#mnews').hide();
        $('#mrss').show();
    }
});


});\n
</script>\n";

        $this->assign('content', $content);


    }

    public function delete( $params )
    {
/*	
        if ( isset($params['id']) )
        {
            NEWS_BOL_Service::getInstance()->deleteDepartment((int) $params['id']);
        }
        $this->redirect(OW::getRouter()->urlForRoute('news.admin'));
*/	
    }
}
