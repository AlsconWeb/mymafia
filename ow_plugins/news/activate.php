<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


OW::getNavigation()->addMenuItem(OW_Navigation::MAIN, 'news.index', 'news', 'main_menu_item', OW_Navigation::VISIBLE_FOR_ALL);

$cmpService = BOL_ComponentAdminService::getInstance();
//echo "sdsrFD";exit;
//if (OW::getConfig()->getValue('news', 'mode')==1){    
//if (OW::getConfig()->getValue('news', 'mode')=="rss"){

    $widget = $cmpService->addWidget('NEWS_CMP_MenuWidget', false);
    $placeWidget = $cmpService->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_INDEX);
    $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_SIDEBAR,0);

    $widget = $cmpService->addWidget('NEWS_CMP_IndexWidget', false);
    $placeWidget = $cmpService->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_INDEX);
    $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_RIGHT,0);

    $widget = $cmpService->addWidget('NEWS_CMP_IndexBig', false);
    $placeWidget = $cmpService->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_INDEX);
    $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_TOP,0);


    $widget = $cmpService->addWidget('NEWS_CMP_DashboardWidget', false);
    $placeWidget = BOL_ComponentAdminService::getInstance()->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_DASHBOARD);
    $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_RIGHT, 0);


    $widget = $cmpService->addWidget('NEWS_CMP_SliderWidget', false);
    $placeWidget = $cmpService->addWidgetToPlace($widget, BOL_ComponentAdminService::PLACE_INDEX);
    $cmpService->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_RIGHT,0);
//}

/*
//add 'birthdays' widget to index page when plugin activated
        $placeWidget = BOL_ComponentAdminService::getInstance()->addWidgetToPlace(
            BOL_ComponentAdminService::getInstance()->addWidget('NEWS_CMP_MenuWidget', false),
            BOL_ComponentAdminService::PLACE_INDEX
        );
        
        //place widget on index page's left section
        BOL_ComponentAdminService::getInstance()->addWidgetToPosition($placeWidget, BOL_ComponentAdminService::SECTION_LEFT);
*/
