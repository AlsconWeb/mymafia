<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/



class NEWS_CMP_MenuWidget extends BASE_CLASS_Widget
{
    private $content = false;
    private $nl2br = false;
 
    public function __construct( BASE_CLASS_WidgetParameter $paramObject )
    {
        parent::__construct();

            if (!OW::getUser()->isAdmin() AND !OW::getUser()->isAuthorized('news', 'view')){
                $this->setVisible(false);
                return;
            }

/* 
        $params = $paramObject->customParamList;
 
        if ( !empty($params['content']) )
        {
            $this->content = $paramObject->customizeMode && !empty($_GET['disable-js']) ? UTIL_HtmlTag::stripJs($params['content']) : $params['content'];
        }
 
        if ( isset($params['nl_to_br']) )
        {
            $this->nl2br = (bool) $params['nl_to_br'];
        }
*/
    }
 
    public static function getSettingList() // If you redefine this method, you'll be able to add fields to the widget configuration form 
    {
        $settingList = array();

        $settingList['content'] = array(
            'presentation' => self::PRESENTATION_TEXTAREA, // Field type
            'label' => OW::getLanguage()->text('base', 'custom_html_widget_content_label'), // Field name
            'value' => '' // Default value
        );
 
        $settingList['nl_to_br'] = array(
            'presentation' => self::PRESENTATION_CHECKBOX,
            'label' => OW::getLanguage()->text('base', 'custom_html_widget_nl2br_label'),
            'value' => '0'
        );

        return $settingList;
    }
 /*
    public static function processSettingList( $settings, $place ) // This method is called before saving the widget settings. Here you can process the settings entered by a user before saving them. 
    {

        if ( $place != BOL_ComponentService::PLACE_DASHBOARD && !OW::getUser()->isAdmin() )
        {
            $settings['content'] = UTIL_HtmlTag::stripJs($settings['content']);
            $settings['content'] = UTIL_HtmlTag::stripTags($settings['content'], array('frame'), array(), true, true);
        }
        else
        {
            $settings['content'] = UTIL_HtmlTag::sanitize($settings['content']);
        }
        return $settings;
    }

*/ 
    public static function getStandardSettingValueList() // If you redefine this method, you will be able to set default values for the standard widget settings. 
    {
        return array(
            self::SETTING_TITLE => OW::getLanguage()->text('news', 'main_menu_items') // Set the widget title 
        );
    }
 
    public static function getAccess() // If you redefine this method, you'll be able to manage the widget visibility 
    {
        return self::ACCESS_ALL;
    }
 
    public function onBeforeRender() // The standard method of the component that is called before rendering
    {

        $content="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
        $curent_url=OW_URL_HOME;                                        
/*
$config = OW::getConfig();
if ( !$config->configExists('news', 'rss_url') ){
    $config->addConfig('news', 'rss_url', "http://finance.yahoo.com/rss/usmarkets", '');
}
if ( !$config->configExists('news', 'rss_items') ){
    $config->addConfig('news', 'rss_items', "6", '');
}
*/
//echo "SfsdF";exit;
        if (OW::getConfig()->getValue('news', 'mode')=="rss"){
            $url=OW::getConfig()->getValue('news', 'rss_url_sidebar');
            if ($url){
//        $url = "http://rss.gazeta.pl/pub/rss/aktualnosci.htm";

                $items=5;
                $items=OW::getConfig()->getValue('news', 'rss_items_sidebar');
                if (!$items) $items=5;
                $rss_id=1;
//            $rss_pos=0;
                $rss_pos="sidebar";//id or name
                $rss_type="widget";
//            $rss_type="short";

                $length_title=OW::getConfig()->getValue('news', 'rss_title_length_sidebar');
                if (!$length_title) $length_title=25;
                $length_content=OW::getConfig()->getValue('news', 'rss_content_length_sidebar');
                if (!$length_content) $length_content=0;

//display_item_short
//$content .=$items;
//            NEWS_BOL_Service::getInstance()->startRSSreader($url);
//            $content .= NEWS_BOL_Service::getInstance()->getOutput($items,$rss_id,$rss_pos,$rss_type,$length_title,$length_content); // returns string containing HTML
                $content .= NEWS_BOL_Service::getInstance()->make_for_widget($url,$items,$rss_id,$rss_pos,$rss_type,$length_title,$length_content); // returns string containing HTML
            }
        }else{
            $content .= NEWS_BOL_Service::getInstance()->make_for_widget_news(); // returns string containing HTML
        }
        $this->assign('content', $content);
    }//end function
}


