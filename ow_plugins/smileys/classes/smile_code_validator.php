<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow <kainisoft@gmail.com>
 * @package ow_plugins.smileys.classes
 * @since 1.0
 */

class SMILEYS_CLASS_SmileCodeValidator extends OW_Validator
{
    private $code;
    
    public function __construct( $code = '' )
    {
        $this->code = $code;
        $this->errorMessage = OW::getLanguage()->text('smileys', 'error_msg_code_busy');
    }

    public function isValid( $value )
    {
        if ( strcasecmp($value, $this->code) === 0 )
        {
            return TRUE;
        }
        
        return !SMILEYS_BOL_Service::getInstance()->isSmileCodeBusy($value);
    }
    
    public function getJsValidator()
    {
        $smileys = SMILEYS_BOL_Service::getInstance()->getAllSmileys();
        $codes = array();
        
        foreach ( $smileys as $smile )
        {
            $codes[] = strtolower($smile->code);
        }
        
        return UTIL_JsGenerator::composeJsString('{
                validate : function( value )
                {
                    if ( value.toLowerCase() === {$code}.toLowerCase() )
                    {
                        return true;
                    }

                    if ( {$codes}.indexOf(value.toLowerCase()) !== -1 )
                    {
                        throw ' . json_encode($this->getError()) . '
                    }
                },
                getErrorMessage : function(){ return ' . json_encode($this->getError()) . ' }
            }', array(
            'code' => $this->code,
            'codes' => $codes
        ));
    }
}
