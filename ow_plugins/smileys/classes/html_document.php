<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow <kainisoft@gmail.com>
 * @package ow_plugins.smileys.classes
 * @since 1.0
 * @deprecated since version 16
 */
class SMILEYS_CLASS_HtmlDocument 
{
    CONST PATTERN = '/<img[^>]*(?:(?<=src=")(?!.+\/ow_userfiles\/plugins\/smileys\/images\/.+\/.+\.gif")).*>/i';
    
    private static $classInstance;
    
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }
    
    private $document;
    private $plugin;
    
    private function __construct()
    {
        $this->document = OW::getDocument();
        $this->plugin = OW::getPluginManager()->getPlugin( 'smileys' );
    }

    public function replaceBaseWysisyg()
    {
        $js = UTIL_JsGenerator::composeJsString( ';window.smileysParams = {"url":{$url}, "smileysUrl":{$smileysUrl}, "adminRsp":{$rsp}, "label":{$label}};', 
                array('url' => OW::getRouter()->urlForRoute('smileys.smileLoader'),
                      'smileysUrl' => $this->plugin->getUserFilesUrl() . 'images/',
                      'rsp' => OW::getRouter()->urlForRoute('smileys.admin-rsp'),
                      'label' => OW::getLanguage()->text('smileys', 'smileys')) );
        $this->document->addScriptDeclarationBeforeIncludes($js);
        
        if ( OW::getPluginManager()->isPluginActive('mailbox') )
        {
            $this->document->addScriptDeclaration(';OW.bind("mailbox.update_message", function( data )
            {
                $("#" + "main_tab_contact_" + data.opponentId + " .ow_dialog_in_item p").each(function()
                {
                    $(this).html($(this).html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                });
            });');
            
            $this->document->addScriptDeclaration(';OW.bind("mailbox.after_write_mail_message", function( data )
            {
                $("#conversationLog .ow_dialog_in_item p").each(function()
                {
                    $(this).html($(this).html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                });
                
                $("#conversationLog .ow_mailbox_message_content").each(function()
                {
                    $(this).html($(this).html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                });
            });');
            
            $this->document->addOnloadScript(';OW.bind("mailbox.update_chat_message", function( data )
            {
                $("#" + "main_tab_contact_" + data.recipientId + " .ow_dialog_in_item p").each(function()
                {
                    $(this).html($(this).html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                });
            });', 9999);
            
            $this->document->addOnloadScript(';OW.bind("mailbox.message", function( data )
            {
                $("#" + "main_tab_contact_" + data.senderId + " .ow_dialog_in_item p").each(function()
                {
                    $(this).html($(this).html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                });
            });', 9999);
            
            $this->document->addOnloadScript(';OW.bind("smileys.updateScroll", function( opponentId )
            {
                try {
                for ( var id in OW.Mailbox.contactManagerView.dialogs )
                {
                    var dialog = OW.Mailbox.contactManagerView.dialogs[id];
                    
                    if ( dialog.model.opponentId == opponentId )
                    {
                    
                        setTimeout(function(){dialog.scrollDialog();}, 200);
                        break;
                    }
                }
                }
                catch (e){}
            });', 9999);
        }
        
        $handler = OW::getRequestHandler()->getHandlerAttributes();
        
        if ( $handler[OW_RequestHandler::ATTRS_KEY_CTRL] == 'MAILBOX_CTRL_Messages' && $handler[OW_RequestHandler::ATTRS_KEY_ACTION] == 'index' )
        {
            $this->document->addOnloadScript(';
                OW.bind("mailbox.update_message", function()
                {
                    $("#conversationLog .ow_dialog_in_item p").each(function()
                    {
                        $(this).html($(this).html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                    });
                    
                    $("#conversationLog .ow_mailbox_message_content").each(function()
                    {
                        $(this).html($(this).html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                    });
                });

                OW.bind("mailbox.conversation_marked_read", function()
                {
                    $("#conversationLog .ow_dialog_in_item p").each(function()
                    {
                        $(this).html($(this).html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                    });
                    
                    OW.trigger("smileys.scrollDialog");
                });
                
                OW.bind("mailbox.mark_message_read", function()
                {
                    $("#conversationLog .ow_dialog_in_item p").each(function()
                    {
                        $(this).html($(this).html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                    });
                    
                    OW.trigger("smileys.scrollDialog");
                });
                
                OW.bind("mailbox.history_loaded", function()
                {
                    $(".ow_mailbox_message_content,.ow_dialog_in_item p", "#conversationLog").each(function()
                    {
                        $(this).html($(this).html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                    });
                });
                
                OW.bind("mailbox.render_conversation_item", function( data )
                {
                    var item = data.$el.find(".ow_mailbox_convers_preview");
                    
                    if ( item.length )
                    {
                        item.html(item.html().replace(/\[([^/]+\/[^/]+)\]/ig, "<img src=\'" + window.smileysParams.smileysUrl + "$1.gif" + "\' />"));
                    }
                });
                ', 9999);
            
                $this->document->addOnloadScript(';OW.bind("smileys.scrollDialog", function()
                {
                    try 
                    {
                        setTimeout(function(){OW.Mailbox.conversationController.scrollDialog();}, 200);
                    }
                    catch (e){}
                });', 9999);
        }
    }
    
    public function replaceBaseComment()
    {
        $this->document->addScriptDeclaration( ';OW.bind("base.comments_list_init",function()
            {
                $(".ow_comments_content",this.$context).each(function()
                {
                    var self = $(this);
                    self.html(self.html().replace(/\[([^/]+\/[^/]+)\]/ig,"<img src=\'"+window.smileysParams.smileysUrl+"$1.gif"+"\'/>"));
                });
            });' );
        $this->document->addScriptDeclaration( ';OW.bind("base.comments_list_init",function()
            {
                if(["profile-cover", "avatar-change"].indexOf(this.entityType)!==-1)return;
                var closest=$(this.$context).closest(".ow_newsfeed_body"),content;
                if ( (content=closest.find(".ow_newsfeed_body_status")).length )
                {
                    content.html(content.html().replace(/\[([^/]+\/[^/]+)\]/ig,"<img src=\'"+window.smileysParams.smileysUrl+"$1.gif"+"\'/>"));
                }
                else
                {
                    if ( this.entityType == "photo_comments") return;
                    closest.find(".ow_newsfeed_content").each(function()
                    {
                        var self=$(this);
                        self.data("origCont",self.html());
                        self.html(self.html().replace(/\[([^/]+\/[^/]+)\]/ig,"<img src=\'"+window.smileysParams.smileysUrl+"$1.gif"+"\'/>"));
                    });
                }
            });');
        $this->document->addScriptDeclaration( ';OW.bind("onChatAppendMessage",function(message)
            {
                var p=message.find("p");
                p.html(p.html().replace(/\[([^/]+\/[^/]+)\]/ig,"<img src=\'"+window.smileysParams.smileysUrl+"$1.gif"+"\'/>"));
            });' );
        $this->document->addScriptDeclaration( ';OW.bind("consoleAddItem",function(items)
            {
                for(var item in items)
                {
                    items[item].html=items[item].html.replace(/\[([^/]+\/[^/]+)\]/ig,"<img src=\'"+window.smileysParams.smileysUrl+"$1.gif"+"\'/>");
                }
            });');
        $this->document->addScriptDeclaration( ';OW.bind("photo.onBeforeLoadFromCache",function(items)
            {
                OW.bind("base.comments_list_init",function()
                {
                    $(".ow_comments_content",this.$context).each(function()
                    {
                        var self = $(this);
                        self.data("origCont", self.html());
                        self.html(self.html().replace(/\[([^/]+\/[^/]+)\]/ig,"<img src=\'"+window.smileysParams.smileysUrl+"$1.gif"+"\'/>"));
                    });
                });
            });
            
            OW.bind("photo.onFloatboxClose",function(items)
            {
                OW.bind("base.comments_list_init",function()
                {
                    $(".ow_comments_content",this.$context).each(function()
                    {
                        var self = $(this);
                        self.data("origCont", self.html());
                        self.html(self.html().replace(/\[([^/]+\/[^/]+)\]/ig,"<img src=\'"+window.smileysParams.smileysUrl+"$1.gif"+"\'/>"));
                    });
                });
            });');
    }
}
