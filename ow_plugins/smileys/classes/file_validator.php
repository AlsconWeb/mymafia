<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow <kainisoft@gmail.com>
 * @package ow_plugins.smileys.classes
 * @since 1.0
 */

class SMILEYS_CLASS_FileValidator extends OW_Validator
{
    private $fileName;
    
    public function __construct( $fileName )
    {
        $this->fileName = $fileName;
        $this->errorMessage = OW::getLanguage()->text('smileys', 'not_support_file');
    }

    public function isValid( $value )
    {
        return !empty($_FILES[$this->fileName]) && 
            $_FILES[$this->fileName]['error'] === UPLOAD_ERR_OK && 
            in_array($_FILES[$this->fileName]['type'], array('image/jpeg', 'image/png', 'image/gif')) && 
            is_uploaded_file($_FILES[$this->fileName]['tmp_name']);
    }
    
    public function getJsValidator()
    {
        return '{
            validate : function( value )
            {
                if ( ["jpg", "jpeg", "png", "gif"].indexOf(value.split(".").pop()) === -1 )
                {
                    throw ' . json_encode($this->getError()) . '
                }
            },
            getErrorMessage : function(){ return ' . json_encode($this->getError()) . ' }
        }';
    }
}
