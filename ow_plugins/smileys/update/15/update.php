<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */
$plugin = OW::getPluginManager()->getPlugin('smileys');
$staticDir = OW_DIR_STATIC_PLUGIN . $plugin->getModuleName() . DS . 'js' .DS;

if ( !file_exists($staticDir) )
{
    mkdir($staticDir);
    chmod($staticDir, 0777);
}

@copy($plugin->getStaticJsDir() . 'smileys.js', $staticDir . 'smileys.js');
@copy($plugin->getStaticJsDir() . 'contactmanager.js', $staticDir . 'contactmanager.js');
@copy($plugin->getStaticJsDir() . 'conversation_list.js', $staticDir . 'conversation_list.js');
@copy($plugin->getStaticJsDir() . 'console.js', $staticDir . 'console.js');
