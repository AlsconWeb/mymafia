<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */
$sql = array(
'CREATE TABLE IF NOT EXISTS `' . OW_DB_PREFIX . 'smileys_smileys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order` smallint(5) unsigned NOT NULL DEFAULT "0",
  `code` varchar(12) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `order` (`order`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;',

"INSERT INTO `" . OW_DB_PREFIX . "smileys_smileys` (`id`, `order`, `code`, `name`) VALUES
(NULL, 0, ':)', 'smiling.gif'),
(NULL, 0, ':(', 'sad.gif'),
(NULL, 0, ':D', 'laughing.gif'),
(NULL, 0, '8-)', 'cool.gif'),
(NULL, 0, '(surprised)', 'surprised.gif'),
(NULL, 0, ';)', 'winking.gif'),
(NULL, 0, ';(', 'crying.gif'),
(NULL, 0, '(sweat)', 'sweating.gif'),
(NULL, 0, ':-|', 'speechless.gif'),
(NULL, 0, ':*', 'kissing.gif'),
(NULL, 0, '(makeup)', 'makeup.gif'),
(NULL, 0, '(chuckle)', 'giggling.gif'),
(NULL, 0, '(clap)', 'clapping.gif'),
(NULL, 0, '(think)', 'thinking.gif'),
(NULL, 0, '(bow)', 'bowing.gif'),
(NULL, 0, '(rofl)', 'ROFL.gif'),
(NULL, 0, '(whew)', 'whew.gif'),
(NULL, 0, '(happy)', 'happy.gif'),
(NULL, 0, '(smirk)', 'smirking.gif'),
(NULL, 0, '(nod)', 'nodding.gif'),
(NULL, 0, '(ninja)', 'ninjay.gif'),
(NULL, 0, '(rock)', 'rock.gif'),
(NULL, 0, '(*)', 'star.gif'),
(NULL, 0, '(mooning)', 'mooning.gif'),
(NULL, 0, '(finger)', 'FU.gif'),
(NULL, 0, '(bandit)', 'bandit.gif'),
(NULL, 0, '(drunk)', 'drunk.gif'),
(NULL, 0, '(smoking) ', 'smoking.gif'),
(NULL, 0, '(toivo)', 'toivo.gif'),
(NULL, 0, '(headbang)', 'headbang.gif'),
(NULL, 0, '(bug)', 'bug.gif'),
(NULL, 0, '(fubar)', 'FUBAR.gif'),
(NULL, 0, '(poolparty)', 'pool.gif'),
(NULL, 0, '(swear)', 'swearing.gif'),
(NULL, 0, '(tmi)', 'TMI.gif'),
(NULL, 0, '(shake)', 'no.gif'),
(NULL, 0, '(punch)', 'boxing.gif'),
(NULL, 0, '(emo)', 'emo.gif'),
(NULL, 0, '(y)', 'thumbs up.gif'),
(NULL, 0, '(n)', 'thumbs down.gif'),
(NULL, 0, ':p', 'tongue.gif'),
(NULL, 0, '(blush)', 'blushing.gif'),
(NULL, 0, ':^)', 'wondering.gif'),
(NULL, 0, '|-)', 'sleepy.gif'),
(NULL, 0, '|-(', 'dull.gif'),
(NULL, 0, '(inlove)', 'in love.gif'),
(NULL, 0, ']:)', 'evil.gif'),
(NULL, 0, '(talk)', 'talking.gif'),
(NULL, 0, '(yawn)', 'yawning.gif'),
(NULL, 0, '(puke)', 'sick.gif'),
(NULL, 0, '(handshake)', 'handshake.gif'),
(NULL, 0, '(h)', 'heart.gif'),
(NULL, 0, '(u)', 'broken heart.gif'),
(NULL, 0, '(e)', 'mail.gif'),
(NULL, 0, '(skype)', 'Skype.gif'),
(NULL, 0, '(F)', 'flower.gif'),
(NULL, 0, '(rain)', 'rain cloud.gif'),
(NULL, 0, '(sun)', 'sun.gif'),
(NULL, 0, '(o)', 'clock.gif'),
(NULL, 0, '(music)', 'music.gif'),
(NULL, 0, '(~)', 'movie.gif'),
(NULL, 0, '(mp)', 'cell phone.gif'),
(NULL, 0, '(coffee)', 'coffee.gif'),
(NULL, 0, '(pi)', 'pizza.gif'),
(NULL, 0, '(cash)', 'cash.gif'),
(NULL, 0, '(flex)', 'flexing.gif'),
(NULL, 0, '(^)', 'cake.gif'),
(NULL, 0, '(beer)', 'beer.gif'),
(NULL, 0, '(d)', 'cocktail.gif'),
(NULL, 0, '(dance)', 'dancing.gif'),
(NULL, 0, '(doh)', 'doh.gif'),
(NULL, 0, ':@', 'angry.gif'),
(NULL, 0, '(wasntme)', 'it wasn''t me.gif'),
(NULL, 0, '(party)', 'party.gif'),
(NULL, 0, ':S', 'worried.gif'),
(NULL, 0, '(mm)', 'mmm.gif'),
(NULL, 0, '8-|', 'nerd.gif'),
(NULL, 0, ':x', 'no speak.gif'),
(NULL, 0, '(wave)', 'hi.gif'),
(NULL, 0, '(call)', 'call me.gif'),
(NULL, 0, '(devil)', 'devil.gif'),
(NULL, 0, '(angel)', 'angel.gif'),
(NULL, 0, '(envy)', 'jealous.gif'),
(NULL, 0, '(wait)', 'wait.gif'),
(NULL, 0, '(hug)', 'bear.gif'),
(NULL, 0, '(heidy)', 'furry.gif');"
);

foreach ( $sql as $s )
{
    try
    {
        Updater::getDbo()->query($s);
    }
    catch ( Exception $ex )
    {
        Updater::getLogger()->addEntry(json_encode($ex));
    }
}

$config = Updater::getConfigService();

if ( !$config->configExists('smileys', 'width') )
{
    $config->addConfig('smileys', 'width', 321);
}

$plugin = OW::getPluginManager()->getPlugin('smileys');

UTIL_File::copyDir($plugin->getStaticDir() . 'smileys', $plugin->getUserFilesDir() . 'smileys');

Updater::getLanguageService()->importPrefixFromZip(dirname(__FILE__) . DS . 'langs.zip', 'smileys');
