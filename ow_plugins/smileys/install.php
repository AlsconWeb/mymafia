<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */
$sql = array(
'CREATE TABLE IF NOT EXISTS `' . OW_DB_PREFIX . 'smileys_smileys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` int(10) unsigned NOT NULL,
  `isCaption` tinyint(1) unsigned NOT NULL,
  `order` smallint(5) unsigned NOT NULL DEFAULT "0",
  `code` varchar(12) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `order` (`order`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;',

"INSERT INTO `" . OW_DB_PREFIX . "smileys_smileys` (`id`, `category`, `isCaption`, `order`, `code`, `name`) VALUES
(NULL, 1, 1, 0, ':)', 'smiling.gif'),
(NULL, 1, 0, 0, ':(', 'sad.gif'),
(NULL, 1, 0, 0, ':D', 'laughing.gif'),
(NULL, 1, 0, 0, '8-)', 'cool.gif'),
(NULL, 1, 0, 0, '(surprised)', 'surprised.gif'),
(NULL, 1, 0, 0, ';)', 'winking.gif'),
(NULL, 1, 0, 0, ';(', 'crying.gif'),
(NULL, 1, 0, 0, '(sweat)', 'sweating.gif'),
(NULL, 1, 0, 0, ':-|', 'speechless.gif'),
(NULL, 1, 0, 0, ':*', 'kissing.gif'),
(NULL, 1, 0, 0, '(makeup)', 'makeup.gif'),
(NULL, 1, 0, 0, '(chuckle)', 'giggling.gif'),
(NULL, 1, 0, 0, '(clap)', 'clapping.gif'),
(NULL, 1, 0, 0, '(think)', 'thinking.gif'),
(NULL, 1, 0, 0, '(bow)', 'bowing.gif'),
(NULL, 1, 0, 0, '(rofl)', 'ROFL.gif'),
(NULL, 1, 0, 0, '(whew)', 'whew.gif'),
(NULL, 1, 0, 0, '(happy)', 'happy.gif'),
(NULL, 1, 0, 0, '(smirk)', 'smirking.gif'),
(NULL, 1, 0, 0, '(nod)', 'nodding.gif'),
(NULL, 1, 0, 0, '(ninja)', 'ninjay.gif'),
(NULL, 1, 0, 0, '(rock)', 'rock.gif'),
(NULL, 1, 0, 0, '(*)', 'star.gif'),
(NULL, 1, 0, 0, '(mooning)', 'mooning.gif'),
(NULL, 1, 0, 0, '(finger)', 'FU.gif'),
(NULL, 1, 0, 0, '(bandit)', 'bandit.gif'),
(NULL, 1, 0, 0, '(drunk)', 'drunk.gif'),
(NULL, 1, 0, 0, '(smoking) ', 'smoking.gif'),
(NULL, 1, 0, 0, '(toivo)', 'toivo.gif'),
(NULL, 1, 0, 0, '(headbang)', 'headbang.gif'),
(NULL, 1, 0, 0, '(bug)', 'bug.gif'),
(NULL, 1, 0, 0, '(fubar)', 'FUBAR.gif'),
(NULL, 1, 0, 0, '(poolparty)', 'pool.gif'),
(NULL, 1, 0, 0, '(swear)', 'swearing.gif'),
(NULL, 1, 0, 0, '(tmi)', 'TMI.gif'),
(NULL, 1, 0, 0, '(shake)', 'no.gif'),
(NULL, 1, 0, 0, '(punch)', 'boxing.gif'),
(NULL, 1, 0, 0, '(emo)', 'emo.gif'),
(NULL, 1, 0, 0, '(y)', 'thumbs up.gif'),
(NULL, 1, 0, 0, '(n)', 'thumbs down.gif'),
(NULL, 1, 0, 0, ':p', 'tongue.gif'),
(NULL, 1, 0, 0, '(blush)', 'blushing.gif'),
(NULL, 1, 0, 0, ':^)', 'wondering.gif'),
(NULL, 1, 0, 0, '|-)', 'sleepy.gif'),
(NULL, 1, 0, 0, '|-(', 'dull.gif'),
(NULL, 1, 0, 0, '(inlove)', 'in love.gif'),
(NULL, 1, 0, 0, ']:)', 'evil.gif'),
(NULL, 1, 0, 0, '(talk)', 'talking.gif'),
(NULL, 1, 0, 0, '(yawn)', 'yawning.gif'),
(NULL, 1, 0, 0, '(puke)', 'sick.gif'),
(NULL, 1, 0, 0, '(handshake)', 'handshake.gif'),
(NULL, 1, 0, 0, '(h)', 'heart.gif'),
(NULL, 1, 0, 0, '(u)', 'broken heart.gif'),
(NULL, 1, 0, 0, '(e)', 'mail.gif'),
(NULL, 1, 0, 0, '(skype)', 'Skype.gif'),
(NULL, 1, 0, 0, '(F)', 'flower.gif'),
(NULL, 1, 0, 0, '(rain)', 'rain cloud.gif'),
(NULL, 1, 0, 0, '(sun)', 'sun.gif'),
(NULL, 1, 0, 0, '(o)', 'clock.gif'),
(NULL, 1, 0, 0, '(music)', 'music.gif'),
(NULL, 1, 0, 0, '(~)', 'movie.gif'),
(NULL, 1, 0, 0, '(mp)', 'cell phone.gif'),
(NULL, 1, 0, 0, '(coffee)', 'coffee.gif'),
(NULL, 1, 0, 0, '(pi)', 'pizza.gif'),
(NULL, 1, 0, 0, '(cash)', 'cash.gif'),
(NULL, 1, 0, 0, '(flex)', 'flexing.gif'),
(NULL, 1, 0, 0, '(^)', 'cake.gif'),
(NULL, 1, 0, 0, '(beer)', 'beer.gif'),
(NULL, 1, 0, 0, '(d)', 'cocktail.gif'),
(NULL, 1, 0, 0, '(dance)', 'dancing.gif'),
(NULL, 1, 0, 0, '(doh)', 'doh.gif'),
(NULL, 1, 0, 0, ':@', 'angry.gif'),
(NULL, 1, 0, 0, '(wasntme)', 'it wasn''t me.gif'),
(NULL, 1, 0, 0, '(party)', 'party.gif'),
(NULL, 1, 0, 0, ':S', 'worried.gif'),
(NULL, 1, 0, 0, '(mm)', 'mmm.gif'),
(NULL, 1, 0, 0, '8-|', 'nerd.gif'),
(NULL, 1, 0, 0, ':x', 'no speak.gif'),
(NULL, 1, 0, 0, '(wave)', 'hi.gif'),
(NULL, 1, 0, 0, '(call)', 'call me.gif'),
(NULL, 1, 0, 0, '(devil)', 'devil.gif'),
(NULL, 1, 0, 0, '(angel)', 'angel.gif'),
(NULL, 1, 0, 0, '(envy)', 'jealous.gif'),
(NULL, 1, 0, 0, '(wait)', 'wait.gif'),
(NULL, 1, 0, 0, '(hug)', 'bear.gif'),
(NULL, 1, 0, 0, '(heidy)', 'furry.gif');"
);

foreach ( $sql as $s )
{
    try
    {
        OW::getDbo()->query($s);
    }
    catch ( Exception $ex )
    {
        OW::getLogger()->addEntry(json_encode($ex));
    }
}

$config = OW::getConfig();

if ( !$config->configExists('smileys', 'width') )
{
    $config->addConfig('smileys', 'width', 321);
}

OW::getPluginManager()->addPluginSettingsRouteName('smileys', 'smileys.admin');

$plugin = OW::getPluginManager()->getPlugin('smileys');
UTIL_File::copyDir($plugin->getStaticDir() . 'smileys', $plugin->getUserFilesDir() . 'smileys');

OW::getLanguage()->importPluginLangs($plugin->getRootDir() . 'langs.zip', 'smileys');
