<?php

/**
 * Copyright (c) 2015, Skalfa LLC
 * All rights reserved.
 *
 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is licensed under SkaDate Exclusive License by Skalfa LLC.
 *
 * Full text of this license can be found at http://www.skadate.com/sel.pdf
 */

/**
 * @author Kairat Bakytow
 * @package ow_plugins.smileys.controllers
 * @since 1.0
 */
class SMILEYS_CTRL_Admin extends ADMIN_CTRL_Abstract
{
    private $service;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->service = SMILEYS_BOL_Service::getInstance();
        
        $language = OW::getLanguage();
        
        $language->addKeyForJs('smileys', 'add_smile_label');
        $language->addKeyForJs('smileys', 'edit_smile_label');
        $language->addKeyForJs('smileys', 'are_you_sure');
        $language->addKeyForJs('smileys', 'add_smile_category');
    }
    
    public function init()
    {
        parent::init();
        
        $general = new BASE_MenuItem();
        $general->setLabel(OW::getLanguage()->text('smileys', 'admin_menu_general'));
        $general->setUrl(OW::getRouter()->urlForRoute('smileys.admin'));
        $general->setKey('general');
        $general->setIconClass('ow_ic_gear_wheel');
        $general->setOrder(0);
        
        $view = new BASE_MenuItem();
        $view->setLabel(OW::getLanguage()->text('smileys', 'admin_menu_view'));
        $view->setUrl(OW::getRouter()->urlForRoute('smileys.admin_view'));
        $view->setKey('view');
        $view->setIconClass('ow_ic_picture');
        $view->setOrder(1);
        
        $menu = new BASE_CMP_ContentMenu(array($general, $view));
        $this->addComponent('menu', $menu);
    }

    public function index( array $params = array() )
    {
        $this->assign('smileysUrl', $this->service->getSmileysUrl());
        $smileys = array();
        $captions = array();
        
        foreach ( $this->service->getAllSmileys() as $smile )
        {
            if ( !isset($smileys[$smile->category]) )
            {
                $smileys[$smile->category] = array();
            }
            
            $smileys[$smile->category][] = $smile;
            
            if ( !empty($smile->isCaption) && !isset($captions[$smile->category]) )
            {
                $captions[$smile->category] = $smile->name;
            }
        }
        
        $this->assign('captions', $captions);
        
        if ( count($smileys) === 1 )
        {
            $keys = array_keys($smileys);
            $this->assign('smileys', $smileys[$keys[0]]);
            $this->assign('isSingle', TRUE);
        }
        else
        {
            $this->assign('smileys', $smileys);
            $this->assign('isSingle', FALSE);
        }
    }
    
    public function view( array $params = array() )
    {
        $config = OW::getConfig();
        $language = OW::getLanguage();
        
        $form = new Form('smileys-settings');
        
        $width = new TextField('width');
        $width->setRequired();
        $width->addValidator(new IntValidator(1));
        $width->setLabel($language->text('smileys', 'width_settings_label'));
        
        $submit = new Submit('save');
        $submit->setValue($language->text('smileys', 'save'));
        
        if ( OW::getRequest()->isPost() && $form->isValid($_POST) )
        {
            $config->saveConfig('smileys', 'width', (int)$_POST['width']);
        }
        
        $width->setValue((int)$config->getValue('smileys', 'width'));
        
        $form->addElement($width);
        $form->addElement($submit);
        
        $this->addForm($form);
        
        $this->assign('width', (int)$config->getValue('smileys', 'width'));
        $this->assign('url', SMILEYS_BOL_Service::getInstance()->getSmileysUrl());
        $smileys = array();
        $captions = array();
        
        foreach ( SMILEYS_BOL_Service::getInstance()->getAllSmileys() as $smile )
        {
            if ( !isset($smileys[$smile->category]) )
            {
                $smileys[$smile->category] = array();
            }
            
            $smileys[$smile->category][] = $smile;
            
            if ( !empty($smile->isCaption) && !isset($captions[$smile->category]) )
            {
                $captions[$smile->category] = $smile->name;
            }
        }
        
        $this->assign('captions', $captions);
        
        if ( count($smileys) === 1 )
        {
            $keys = array_keys($smileys);
            $this->assign('smileys', $smileys[$keys[0]]);
            $this->assign('isSingle', TRUE);
        }
        else
        {
            $this->assign('smileys', $smileys);
            $this->assign('isSingle', FALSE);
        }
    }
    
    public function reorder( array $params = array() )
    {
        $this->service->updateSmileysOrder($_POST);
    }
    
    public function add( array $params = array() )
    {
        if ( OW::getRequest()->isPost() )
        {
            $form = new SMILEYS_CLASS_AddForm();
            
            if ( $form->isValid($_POST) )
            {
                $ext = UTIL_File::getExtension($_FILES[SMILEYS_CLASS_AddForm::ELEMENT_FILE]['name']);
                $fileName = uniqid() . '.' . $ext;
                $smileDir = $this->service->getSmileysDir();
                move_uploaded_file($_FILES[SMILEYS_CLASS_AddForm::ELEMENT_FILE]['tmp_name'], $smileDir . $fileName);
                
                $smileEntity = new SMILEYS_BOL_Smileys();
                $smileEntity->order = $this->service->getFreeOrder();
                $smileEntity->name = $fileName;
                $smileEntity->code = $this->service->sanitizeCode($form->getElement(SMILEYS_CLASS_AddForm::ELEMENT_SMILE_CODE)->getValue());
                $smileEntity->isCaption = 0;
                
                if ( $form->getElement(SMILEYS_CLASS_AddForm::ELEMENT_CATEGORY)->getValue() != NULL )
                {
                    $smileEntity->category = $form->getElement(SMILEYS_CLASS_AddForm::ELEMENT_CATEGORY)->getValue();
                }
                
                SMILEYS_BOL_SmileysDao::getInstance()->save($smileEntity);
                
                OW::getFeedback()->info(OW::getLanguage()->text('smileys', 'success_added_message'));
            }
            else
            {
                foreach ( $form->getErrors() as $errors )
                {
                    foreach ( $errors as $message )
                    {
                        OW::getFeedback()->error($message);
                    }
                }
            }
        }
        
        $this->redirect(OW::getRouter()->uriForRoute('smileys.admin'));
    }

    public function edit( array $params = array() )
    {
        if ( OW::getRequest()->isPost() && !empty($_POST[SMILEYS_CLASS_EditForm::ELEMENT_SMILE_ID]) )
        {
            $smileEntity = $this->service->findSmileById($_POST[SMILEYS_CLASS_EditForm::ELEMENT_SMILE_ID]);
            $form = new SMILEYS_CLASS_EditForm($smileEntity->id, $smileEntity->code);
            
            if ( $form->isValid($_POST) )
            {
                $smileEntity->code = $this->service->sanitizeCode($form->getElement(SMILEYS_CLASS_EditForm::ELEMENT_SMILE_CODE)->getValue());
                
                SMILEYS_BOL_SmileysDao::getInstance()->save($smileEntity);
                
                OW::getFeedback()->info('Smile successfully updated');
            }
            else
            {
                foreach ( $form->getErrors() as $errors )
                {
                    foreach ( $errors as $message )
                    {
                        OW::getFeedback()->error($message);
                    }
                }
            }
        }

        $this->redirect(OW::getRouter()->uriForRoute('smileys.admin'));
    }
    
    public function delete( array $params = array() )
    {
        if ( !empty($_POST['id']) && ($smile = $this->service->findSmileById($_POST['id'])) !== NULL )
        {
            if ( $smile->isCaption )
            {
                exit(json_encode(array(
                    'error' => true,
                    'message' => "You can't delete Category Icon smile"
                )));
            }
            else
            {
                @unlink($this->service->getSmileysDir() . $smile->name);
                SMILEYS_BOL_SmileysDao::getInstance()->deleteById($smile->id);
                exit(json_encode(array(
                    'message' => 'Smile successfully deleted'
                )));
            }
        }
    }
    
    public function addCategory( array $params = array() )
    {
        if ( OW::getRequest()->isPost() )
        {
            $form = new SMILEYS_CLASS_AddCategoryForm();
            
            if ( $form->isValid($_POST) )
            {
                $ext = UTIL_File::getExtension($_FILES[SMILEYS_CLASS_AddForm::ELEMENT_FILE]['name']);
                $fileName = uniqid() . '.' . $ext;
                $smileDir = $this->service->getSmileysDir();
                move_uploaded_file($_FILES[SMILEYS_CLASS_AddForm::ELEMENT_FILE]['tmp_name'], $smileDir . $fileName);
                
                $smileEntity = new SMILEYS_BOL_Smileys();
                $smileEntity->order = $this->service->getFreeOrder();
                $smileEntity->name = $fileName;
                $smileEntity->code = $this->service->sanitizeCode($form->getElement(SMILEYS_CLASS_AddForm::ELEMENT_SMILE_CODE)->getValue());
                $smileEntity->category = $this->service->getFreeSmileCategory();
                $smileEntity->isCaption = 1;
                
                SMILEYS_BOL_SmileysDao::getInstance()->save($smileEntity);
                
                OW::getFeedback()->info('Category successfully added');
            }
            else
            {
                foreach ( $form->getErrors() as $errors )
                {
                    foreach ( $errors as $message )
                    {
                        OW::getFeedback()->error($message);
                    }
                }
            }
        }
        
        $this->redirect(OW::getRouter()->uriForRoute('smileys.admin'));
    }
    
    public function deleteCategory( array $params = array() )
    {
        if ( !empty($_POST['id']) )
        {
            $smileys = $this->service->findSmileysByCategory($_POST['id']);
            $smileDir = $this->service->getSmileysDir();
            
            foreach ( $smileys as $smile )
            {
                @unlink($smileDir . $smile->name);
            }
            
            $this->service->deleteSmileysByCategory($_POST['id']);
            exit(json_encode(array(
                'message' => 'Category successfully deleted'
            )));
        }
    }
    
    public function changeCaption( array $params = array() )
    {
        if ( empty($_POST['id']) || empty($_POST['categoryId']) )
        {
            return;
        }
        
        $this->service->setSmileCaption($_POST['id'], $_POST['categoryId']);
    }
}
