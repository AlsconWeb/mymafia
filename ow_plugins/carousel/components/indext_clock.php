<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://test.a6.pl
 * Full license available at: http://test.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


class CAROUSEL_CMP_IndextClock extends BASE_CLASS_Widget
{

    public function __construct( BASE_CLASS_WidgetParameter $params )
    {
        parent::__construct();
        $pluginStaticDir =OW::getPluginManager()->getPlugin('carousel')->getUserFilesDir();
        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_1.jpg") OR 
            CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_1.jpg") OR 
            CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_1.jpg") OR 
            CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_1.jpg") OR 
            CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_1.jpg") 
        ){
        }else{
            $this->setVisible(false);
            return;
        }

/* 
        $params = $paramObject->customParamList;
 
        if ( !empty($params['content']) )
        {
            $this->content = $paramObject->customizeMode && !empty($_GET['disable-js']) ? UTIL_HtmlTag::stripJs($params['content']) : $params['content'];
        }
 
        if ( isset($params['nl_to_br']) )
        {
            $this->nl2br = (bool) $params['nl_to_br'];
        }
*/
    }

    public static function getSettingList()
    {

        $options = array();
        $settingList=array();
/*
        for ( $i = 3; $i <= 10; $i++ )
        {
            $options[$i] = $i;
        }

        $settingList['count'] = array(
            'presentation' => self::PRESENTATION_SELECT,
            'label' => OW::getLanguage()->text('shoppro', 'cmp_widget_post_count'),
            'optionList' => $options,
            'value' => 3,
        );
        $settingList['previewLength'] = array(
            'presentation' => self::PRESENTATION_TEXT,
            'label' => OW::getLanguage()->text('shoppro', 'blog_widget_preview_length_lbl'),
            'value' => 50,
        );
*/
        return $settingList;
    }

    public static function getStandardSettingValueList()
    {
        $list = array(
            self::SETTING_TITLE => OW::getLanguage()->text('carousel', 'carousel'),
            self::SETTING_SHOW_TITLE => true,
                self::SETTING_WRAP_IN_BOX => true,
            self::SETTING_ICON => 'ow_ic_write'
        );

        return $list;
    }

    public static function getAccess()
    {
        return self::ACCESS_ALL;
    }
	
    public function onBeforeRender() // The standard method of the component that is called before rendering
    {
            $content="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
//                    $curent_url .= $_SERVER["SERVER_NAME"]."/";
        $curent_url=OW_URL_HOME; 
        $pluginStaticU=OW::getPluginManager()->getPlugin('carousel')->getStaticUrl();
//        $pluginStaticD=OW::getPluginManager()->getPlugin('startpage')->getStaticDir();


//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/carousel/ext/jquery.movingboxes.min.js');
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/carousel/ext/slidershop.js?'.rand(100,500));
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/carousel/ext/slidershop.js?a=1');


//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/carousel/w_clock/jquery.tzineClock.js?a=1');
//        OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/carousel/w_clock/jquery.tzineClock.css');

        OW::getDocument()->addScript($pluginStaticU.'carousel.alpha.js');
//        OW::getDocument()->addScript($pluginStaticU.'js'.DS.'bootstrap.min.js');

//        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap-responsive.css');
        OW::getDocument()->addStyleSheet($pluginStaticU.'carousel.alpha.css');
//        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap-responsive.min.css');
//        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap.min.css');

/*
$script="
<script>
$(document).ready(function() {
    $('#fancyClock').tzineClock();

});
</script>
";
//$content.=$script;
OW::getDocument()->addOnloadScript($script);
*/


/*
            $('#carousel_flavor_2').agile_carousel({
                
                carousel_data: data,
                carousel_outer_height: 330,
                carousel_height: 230,
                slide_height: 230,

                carousel_outer_width: 420,
                slide_width: 5,
                                                
                transition_type: 'fade',
                transition_time: 600,
                timer: 3000,
                continuous_scrolling: true,
                control_set_1: 'numbered_buttons,previous_button,pause_button,next_button',
                control_set_2: 'content_buttons',
                change_on_hover: 'content_buttons'
            });
*/



$width=OW::getConfig()->getValue('carousel', 'width');
$height=OW::getConfig()->getValue('carousel', 'height');

if (!$width) $width=450;//480
if (!$height) $height=220;//220


$script="
$.getJSON('".$curent_url."carousel/getdata', function(data) {


        $('#carousel_flavor_3').agile_carousel({
            carousel_data: data,
            carousel_outer_height: ".$height.",
            carousel_height: ".$height.",
            slide_height: ".$height.",
        carousel_outer_width: ".$width.",
        slide_width: ".$width.",
            transition_time: 700,
            timer: 4000,
            continuous_scrolling: true,
            control_set_1: 'content_buttons'
        });

});
";
//$content.="<div class=\"clearfix\" id=\"carousel_flavor_2\"></div>";




$content.="<div class=\"clearfix\" id=\"carousel_flavor_3\"></div>";

$content.="<div class=\"clearfix\" id=\"multiple_slides_visible\"></div>";
//$content.=$script;
OW::getDocument()->addOnloadScript($script);



		$this->assign('content', $content);
	}
	
}

