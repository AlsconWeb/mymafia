<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

class CAROUSEL_BOL_Service
{
    /**
     * Singleton instance.
     *
     * @var CONTACTUS_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return CONTACTUS_BOL_Service
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    private function __construct()
    {

    }

    public function getDepartmentLabel( $id )
    {
        return OW::getLanguage()->text('pay24', $this->getDepartmentKey($id));
    }


    public function image_resize($file_source="",$crop=false,$width=800,$height=600)
    {
        return image_copy_resize($file_source,$file_source,$crop,$width,$height);
    }

    public function image_copy_resize($file_source="",$file_dest="",$crop=false,$width=800,$height=600)
    {
        if ($file_source AND $file_dest){
            $image = new UTIL_Image($file_source);
            $mainPhoto = $image ->resizeImage($width, $height,$crop) ->saveImage($file_dest);
            return true;
        }else{
            return false;
        }
    }
    public function file_copy($src="",$dest="")
    {
        if ($src AND $dest){
//                    $this->corect_exif($src);//corect orienatation
            $storage = OW::getStorage();
            return $storage->copyFile($src,$dest);
        }else{
            return false;
        }
    }

    public function file_delete($src="")
    {
        if ($src){
            $storage = OW::getStorage(); 
            if ( $storage->fileExists($src) )
            {
                $storage->removeFile($src);
            }
            return true;
        }else{
            return false;
        }
    }

    public function file_exist($src="")
    {
        if ($src){
            $storage = OW::getStorage(); 
            if ( $storage->fileExists($src) )
            {
                return true;
            }else {
                return false;
            }
        }else{
            return false;
        }
    }
    public function get_plugin_dir($plugin="")
    {
        if ($plugin){
            return OW::getPluginManager()->getPlugin($plugin)->getUserFilesDir();
        }else{
            return false;
        }
    }


    public function corect_exif($path="")
    {


        if ($path AND is_file($path) AND function_exists('exif_read_data') AND function_exists('imagerotate')){
//        if ($path AND is_file($path) AND function_exists('exif_read_data')){
//            ini_set('exif.encode_unicode', 'UTF-8');
            $exif = exif_read_data($path);
            if($exif) {
                if (isset($exif['Orientation'])){
                    $ort = $exif['Orientation'];
                }else if (isset($exif['IFD0']['Orientation'])){
                    $ort = $exif['IFD0']['Orientation'];
                }else if (isset($exif['EXIF']['Orientation'])){
                    $ort = $exif['EXIF']['Orientation'];
                }

                if($ort != 1){
                    $img = imagecreatefromjpeg($path) or die('Error opening file '.$path);
                    $deg = 0;
                    switch ($ort) {
                        case 3:
                            $deg = 180;
                        break;
                        case 6:
                            $deg = 270;
                        break;
                        case 8:
                            $deg = 90;
                        break;
                    }
                    if ($deg) {
                      $imgr = $this->imagerotateXY($img, $deg, 0);        
                    }else{
                      $imgr=$img;
                    }
                    imagejpeg($imgr, $path, 95);
                    imagedestroy($img);
                    imagedestroy($imgr);
                }
            } // if have the exif orientation info
//        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND extension_loaded('magickwand')){
//        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND function_exists("NewMagickWand")){
//        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND extension_loaded('imagick')){
        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND class_exists("Imagick")){
//echo $path;exit;
            $exif = exif_read_data($path);
            if($exif) {
                $ort="";
                if (isset($exif['Orientation'])){
                    $ort = $exif['Orientation'];
                }else if (isset($exif['IFD0']['Orientation'])){
                    $ort = $exif['IFD0']['Orientation'];
                }else if (isset($exif['EXIF']['Orientation'])){
                    $ort = $exif['EXIF']['Orientation'];
                }

                if($ort != 1){
                    $deg = 0;
                    switch ($ort) {
                        case 3:
                            $deg = -180;
                        break;
                        case 6:
                            $deg = -270;
                        break;
                        case 8:
                            $deg = -90;
                        break;
                    }
                    if ($deg) {
                        $imagick = new Imagick(); 
                        $imagick->readImage($path ); 
                        $imagick->rotateImage(new ImagickPixel('none'), $deg); 
                        $imagick->writeImage($path ); 
                        $imagick->clear(); 
                        $imagick->destroy(); 
                    }
                }
            }
        }
    }


    public function corectforjava($text)
    {
        return $this->corect_for_java($text);
    }
    public function corect_for_java($text)
    {
        $text= str_replace("\r\n"," ",$text);
        $text= str_replace("\n"," ",$text);
//        $text= str_replace(" ","%20",$text);
        $text= str_replace("'","`",$text);
//$text="afas`a ( ) dfdsf fddsfsdfsdf sdf 'f sdf sdf sdf sd'sdf sdf'sdf sdf sdf ;sdf sdf?sdf sdfsd=sdf sdfs@sdfs fd&sdv sd$ afsdf% fsdgf sdf";
//return addslashes($text);
return $text;

        $text= str_replace("%","%25",$text);
        $text= str_replace(";","%3B;",$text);

        $text= str_replace(" ","%20",$text);
        $text= str_replace("!","%21",$text);
        $text= str_replace("@","%40",$text);
        $text= str_replace("#","%23",$text);
        $text= str_replace("$","%24",$text);
        $text= str_replace("^","%5E",$text);
        $text= str_replace("&","%26",$text);
        $text= str_replace("*","%2A",$text);
        $text= str_replace("(","%28",$text);
        $text= str_replace(")","%29",$text);
        $text= str_replace("=","%3D",$text);
        $text= str_replace("+","%2B",$text);
        $text= str_replace(":","%3A",$text);

        $text= str_replace("\"","%22",$text);
        $text= str_replace("'","%27",$text);
        $text= str_replace("\\","%5C",$text);
        $text= str_replace("/","%2F",$text);
        $text= str_replace("?","%3F",$text);
        $text= str_replace("<","%3C",$text);
        $text= str_replace(">","%3E",$text);
        $text= str_replace("~","%7E",$text);
        $text= str_replace("[","%5B",$text);
        $text= str_replace("]","%5D",$text);
        $text= str_replace("{","%7B",$text);
        $text= str_replace("}","%7D",$text);
        $text= str_replace("`","%60",$text);

        return $text;
    }

 public function html2txt($document){
    $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
    );
    $text = preg_replace($search, '', $document);
        $text=preg_replace("/(&?!amp;)/i", " ", $text);
        $text=preg_replace("/(&#\d+);/i", " ", $text); // For numeric entities
        $text=preg_replace("/(&\w+);/i", " ", $text); // For literal entities
    return $text;
 }

//    public function url_toparam ()
    public function check_url_allow ()
    {

//            return false;
/*
        if (OW::getRequest()->isPost()){
            return false;
        }else if (OW::getRequest()->isAjax()){
            return false;
        }
*/
/*
file_put_contents("TTTTTTTTTTTTTTT.txt", "\n==================================\n", FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", print_r($_GET,1), FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", print_r($_POST,1), FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", "\n==================================\n\n", FILE_APPEND );
*/
        $curent_url=OW_URL_HOME;
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) AND $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        $tmp=str_replace(OW_URL_HOME,"",$pageURL);
        if (substr($tmp,0,1)=="/") $tmp=substr($tmp,1);

        $test_furl=$tmp;
        $test_purl="";
        $test_aurl=array();

        $testx_tmp=explode("?",$test_furl);
        if (isset($testx_tmp[0])) $test_furl=$testx_tmp[0];
        if (isset($testx_tmp[1])) $test_purl=$testx_tmp[1];
        $test_aurl=explode("/",$test_furl);
//echo $test_furl;exit;
//            return false;
        if (isset($_POST['form_name']) AND $_POST['form_name']=="sign-in"){
            return false;
        }else if ( strpos($test_furl,"ajaxim")!==false){
            return false;
        }else if ( strpos($test_furl,"ow_cron")!==false){
            return false;
        }else if ( strpos($test_furl,"ow_static")!==false){
            return false;
        }else if ( strpos($test_furl,"ping")!==false){
            return false;
//        }else if (!OW::getUser()->isAuthenticated()){//for members only
//            return false;
        }else if ( strpos($test_furl,"base/ping/index")!==false OR strpos($test_furl,"base/ping")!==false){
            return false;
        }else{
            return true;
        }
    }


    public function get_news( )
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;

        $sql = "SELECT * FROM " . OW_DB_PREFIX. "news WHERE active='1' AND is_published='1'  ORDER BY `data_added` DESC LIMIT 10";
        $arrl = OW::getDbo()->queryForList($sql);
        $content="";
        foreach ( $arrl as $value )
        {
            $content .="<li>
                <div>
                    <a href=\"".$curent_url."news/".$value['id']."/index.html\">".stripslashes($value['topic_name'])."</a>
                </div>
            </li> ";
        }
        if ($content){
            $content ="<ul>".$content."</ul>";
        }
        return $content;
    }



    public function minix ()
    {
        $content="";
        $script="";
        $css="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $pluginStaticURL=OW::getPluginManager()->getPlugin('carousel')->getStaticUrl();

        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/carousel/carousel.js');
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/carousel/carousel_main.js');

//        $content .="<div id=\"dnews\" class=\"news-wrapper\">
//        <div class=\"news\"></div>
//        </div>";
    

$content.="<div id=\"carousel_news\" class=\"carousel_news-wrapper\" style=\"display:none;\">";
$content.="<div class=\"carousel_news\">";

        if ( OW::getPluginManager()->isPluginActive('news')){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "news nn 
            LEFT JOIN " . OW_DB_PREFIX. "news_content nnd ON (nnd.id_news= nn.id) 
            WHERE nn.active='1' AND nn.is_published='1'  ORDER BY nn.data_added DESC LIMIT 3";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value ){
                $desc=mb_substr($this->html2txt(stripslashes($value['content'])),0,200);
                $title=mb_substr($this->html2txt(stripslashes($value['topic_name'])),0,100);
                $url=$curent_url."news/".$value['id']."/index.html";
                $content .="<div class=\"carousel_headline\"><a title=\"".$desc."\" href=\"".$url."\" style=\"overflow: hidden;max-width: 680px;display: inline-block;\">".$title."</a></div>";
            }
        }

        if ( OW::getPluginManager()->isPluginActive('blogs')){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "blogs_post WHERE isDraft='0' AND privacy='everybody' ORDER BY `timestamp` DESC LIMIT 3";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value ){
                $desc=mb_substr($this->html2txt(stripslashes($value['post'])),0,200);
                $title=mb_substr($this->html2txt(stripslashes($value['title'])),0,100);
                $url=$curent_url."blogs/".$value['id'];
                $content .="<div class=\"carousel_headline\"><a title=\"".$desc."\" href=\"".$url."\" style=\"overflow: hidden;max-width: 680px;display: inline-block;\">".$title."</a></div>";
            }
        }

        if ( OW::getPluginManager()->isPluginActive('event')){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "event_item WHERE status='1' AND whoCanView='1' ORDER BY `createTimeStamp` DESC LIMIT 3";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value ){
                $desc=mb_substr($this->html2txt(stripslashes($value['description'])),0,200);
                $title=mb_substr($this->html2txt(stripslashes($value['title'])),0,100);
                $url=$curent_url."event/".$value['id'];
                $content .="<div class=\"carousel_headline\"><a title=\"".$desc."\" href=\"".$url."\" style=\"overflow: hidden;max-width: 680px;display: inline-block;\">".$title."</a></div>";
            }
        }

        if ( OW::getPluginManager()->isPluginActive('groups')){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "groups_group WHERE privacy='everybody' AND whoCanView='anyone' ORDER BY `timeStamp` DESC LIMIT 3";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value ){
                $desc=mb_substr($this->html2txt(stripslashes($value['description'])),0,200);
                $title=mb_substr($this->html2txt(stripslashes($value['title'])),0,100);
                $url=$curent_url."groups/".$value['id'];
                $content .="<div class=\"carousel_headline\"><a title=\"".$desc."\" href=\"".$url."\" style=\"overflow: hidden;max-width: 680px;display: inline-block;\">".$title."</a></div>";
            }
        }

        if ( OW::getPluginManager()->isPluginActive('forum')){
//            $sql = "SELECT * FROM " . OW_DB_PREFIX. "forum_post WHERE 1 ORDER BY `createStamp` DESC LIMIT 3";
            $sql = "SELECT pss.* FROM " . OW_DB_PREFIX. "forum_post pss 
            LEFT JOIN " . OW_DB_PREFIX. "forum_topic ft ON (ft.id=pss.topicId) 
            LEFT JOIN " . OW_DB_PREFIX. "forum_group fg ON (fg.id=ft.groupId) 
            LEFT JOIN " . OW_DB_PREFIX. "forum_section se ON (se.id=fg.sectionId) 
            WHERE (fg.isPrivate='0' OR fg.isPrivate IS NULL) AND entity IS NULL AND isHidden='0' 
            ORDER BY pss.createStamp DESC LIMIT 3";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $value ){
                $desc=mb_substr($this->html2txt(stripslashes($value['text'])),0,200);
                $title=mb_substr($this->html2txt(stripslashes($value['text'])),0,100);
                $url=$curent_url."forum/topic/".$value['topicId'];
                $content .="<div class=\"carousel_headline\"><a title=\"".$desc."\" href=\"".$url."\" style=\"overflow: hidden;max-width: 680px;display: inline-block;\">".$title."</a></div>";
            }
        }



$content.="</div>";
$content.="</div>";



$css .="<style>
.carousel_news-wrapper
{
    width: 660px;
    position: fixed;
    overflow: hidden;
    bottom:0;
    left:0;
}
.carousel_news-title
{
    float: left;
    background-image: url(".$pluginStaticURL."title-news.png);
    background-color: #2f2f2f;
    width: 18px;
    height: 36px;
    z-index: 11;
    border-radius-topleft: 3px;
    border-radius-bottomleft: 3px;
    -moz-border-radius-topleft: 3px;
    -moz-border-radius-bottomleft: 3px;
    -webkit-border-top-left-radius: 3px;
    -webkit-border-bottom-left-radius: 3px;
}
.carousel_news
{
    background-color: #aed0ec;
    width: 640px;
    height: 36px;
    border-radius-topright: 4px;
    border-radius-bottomright: 4px;
    -moz-border-radius-topright: 4px;
    -moz-border-radius-bottomright: 4px;
    -webkit-border-top-right-radius: 4px;
    -webkit-border-bottom-right-radius: 4px;
    overflow: hidden;
}
.carousel_headline
{
    position: absolute;
    font-size: 10pt;
    height: 10px;
    padding: 8px 12px 8px 8px;
    overflow: hidden;
    z-index: 1;
    /*overflow: hidden;*/
}
.carousel_headline a
{
/*    background-image: url(".$pluginStaticURL."gaindi.png);
    background-repeat: no-repeat;
    background-position: right bottom;

    padding-right: 16px;        
*/
}
.carousel_headline a, .carousel_headline a:active, .carousel_headline a:visited
{
    color: #1e1e1e;
    text-decoration: none;
}
.carousel_headline a:hover
{
    color: #163b5b;
    text-decoration: none;
}
.carousel_control-bar
{
    background-image: url(".$pluginStaticURL."control-bar.png);
    background-repeat: no-repeat;
    width: 97px;
    height: 21px;
    position: absolute;
    z-index: -1;
    top: 35px;
}
.carousel_controls-wrapper
{
    position: relative;
    width: 48px;
    height: 12px;
    margin-left: 24px;
}
.carousel_controls-wrapper div
{
    position: relative;
    width: 7px;
    height: 8px;
    margin-left: 6px;
    margin-top: 4px;
    float: left;
}
.carousel_controls-wrapper div.next
{
    background: url(".$pluginStaticURL."next.png) no-repeat;
    cursor: pointer;
}
.carousel_controls-wrapper div.prev
{
    background: url(".$pluginStaticURL."prev.png) no-repeat;
    cursor: pointer;
}
.carousel_controls-wrapper div.play
{
    background: url(".$pluginStaticURL."play.png) no-repeat;
    cursor: pointer;
}
.carousel_controls-wrapper div.pause
{
    background: url(".$pluginStaticURL."pause.png) no-repeat;
    cursor: pointer;
}
.carousel_preview-wrapper
{
    font-size: 10pt;
    padding-top: 8px;
    width: 308px;
    height: 85px;
    position: absolute; /*margin: auto;*/
    z-index: 1000;
}
.carousel_preview
{
    width: 292px;
    height: 61px;
    background-color: #2c2c2c;
    border-radius: 8px;
    -moz-border-radius: 8px;
    -webkit-border-radius: 8px;
    border: solid 1px #212121;
    padding: 8px;
    font-size: 9pt; /*overflow: hidden;*/ /*background: url(".$pluginStaticURL."bg.jpg) repeat-x repeat-y;*/
}
.carousel_preview h4, .carousel_preview p
{
    margin: 0;
    padding: 0;
    overflow: hidden;
}
.carousel_preview h4
{
    font-size: 10pt;
    color: White;
    font-weight: normal;
    width: 290px;
    height: 16px;
}
.carousel_preview p
{
    color: #9f9e9e;
    width: 290px;
    height: 54px;
}
.carousel_preview a
{
    font-size: 11px;
    color: #9f9e9e;
    text-decoration: none;
    margin-left: 4px;
}
.carousel_preview a:hover
{
    text-decoration: underline;
    color: Yellow;
}
.carousel_tip
{
    margin: auto;
    width: 13px;
    height: 9px;
    background-image: url(".$pluginStaticURL."cor.png);
}
.carousel_news-wrapper.multiple
{
    width: 680px;
    overflow: hidden;
}
.carousel_news-wrapper.multiple .carousel_news-title
{
    float: left;
}
.carousel_news-wrapper.multiple .carousel_news
{
    float: left;
    width: 2000px;
}
.carousel_news-wrapper.multiple .carousel_headline
{
    position: relative;
    float: left;
    margin-left: 0px;
}
</style>
";

//    $script .="<script type=\"text/javascript\">";
//{ feedurl: 'http://www.dawn.com/rss', showdetail: true, controlsalwaysvisible: true, entries: 10, controls: true, target: "_blank" }
    $script .="
//    $(document).ready(function() {
        $('#carousel_news').carouselnews({ showdetail: true, controlsalwaysvisible: false });
        $('#carousel_news').show();
//    });
    ";
//    $script .="</script>\n";
//    OW::getDocument()->appendBody($content.$script.$css);
//    OW::getDocument()->appendBody($content.$css);

    OW::getDocument()->appendBody($content);
    OW::getDocument()->appendBody($css);
    OW::getDocument()->addOnloadScript($script);


//    echo "sfdsdF";exit;
//        return $content;
    }

/*
$pluginStaticURL2=OW::getPluginManager()->getPlugin('topmenu')->getStaticUrl();


        $script="";
        OW::getDocument()->addOnloadScript($script);

*/


}
