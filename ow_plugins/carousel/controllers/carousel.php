<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/



class CAROUSEL_CTRL_Carousel extends OW_ActionController
{

    public function indexaj($params)
    {

        $curent_url=OW_URL_HOME;
        $pluginStaticURL =OW::getPluginManager()->getPlugin('carousel')->getUserFilesUrl();
        $pluginStaticDir =OW::getPluginManager()->getPlugin('carousel')->getUserFilesDir();

        $contrnt="";
        for ($i=1;$i<6;$i++){
            if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_".$i.".jpg")){
//                $content .="<br/>";
//                $content .="<img src=\"".$pluginStaticURL."image_1.jpg\" style=\"max-width:250px;max-height:250px;\">";

                if (OW::getConfig()->getValue('carousel', 'image_url_'.$i)) $url=OW::getConfig()->getValue('carousel', 'image_url_'.$i);
                    else $url="#";

                if (OW::getConfig()->getValue('carousel', 'button_content_'.$i)) $button_text=OW::getConfig()->getValue('carousel', 'button_content_'.$i);
                    else $button_text="";



                if ($contrnt) $contrnt  .=", ";

                if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_button_".$i.".jpg")){
                    $image_buton=$pluginStaticURL."image_button_".$i.".jpg";
                    $contrnt .="{
                        \"content\": \"<div class='slide_inner'><a class='photo_link' href='".$url."'><img class='photo' src='".$pluginStaticURL."image_".$i.".jpg"."' alt='".CAROUSEL_BOL_Service::getInstance()->corectforjava($button_text)."'></a><a class='caption' href='#'>".CAROUSEL_BOL_Service::getInstance()->corectforjava($button_text)."...</a></div>\",
                        \"content_button\": \"<div class='thumb' style=''><img src='".$image_buton."' alt='".CAROUSEL_BOL_Service::getInstance()->corectforjava($button_text)."'></div><p>".CAROUSEL_BOL_Service::getInstance()->corectforjava($button_text)."</p>\"
                    }";
                }else{
//                    $image_buton=$curent_url."";
                    $contrnt .="{
                        \"content\": \"<div class='slide_inner'><a class='photo_link' href='".$url."'><img class='photo' src='".$pluginStaticURL."image_".$i.".jpg"."' alt='".CAROUSEL_BOL_Service::getInstance()->corectforjava($button_text)."'></a><a class='caption' href='#'>".CAROUSEL_BOL_Service::getInstance()->corectforjava($button_text)."...</a></div>\",
                        \"content_button\": \"<div class='thumb thumb_empty' style='width:5px;border:0;border-seze:0;background: transparent;'></div><p>".CAROUSEL_BOL_Service::getInstance()->corectforjava($button_text)."</p>\"
                    }";
                }





            }
        }
//        $contrnt=CAROUSEL_BOL_Service::getInstance()->corectforjava($contrnt);
        echo "[".$contrnt ."]";

        exit;
    }
    public function index($params)
    {

        $curent_url=OW_URL_HOME;
$output_carousel2="";
$content="";

        $this->setPageTitle(OW::getLanguage()->text('carousel', 'main_menu_item')); //title menu
        $this->setPageHeading(OW::getLanguage()->text('carousel', 'main_menu_item')); //title page

        if (!isset($params)) $params=array();
        if (!isset($params['com'])) $params['com']="";
        if (!$params['com']){
            $maxitems=9999;
        }else{
            $maxitems=999999;
        }

	$this->assign('content', $content);
    }//end index

    public function make_item($arrx,$tagmain="item")
    {
        $ret ="";
        foreach ($arrx as $nam=>$val){
            if ($nam AND $val){
                $ret .="<".$nam.">".$val."</".$nam.">\n";
            }
        }
        if ($ret AND $tagmain){
            $ret="<".$tagmain.">\n".$ret."</".$tagmain.">\n";
        }
        return $ret;
    }

    public function sent()
    {

    }
/*
     public function html2txt($document){
        $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
        );
        $text = preg_replace($search, '', $document);
        $text=preg_replace("/(&?!amp;)/i", " ", $text);
        $text=preg_replace("/(&#\d+);/i", " ", $text); // For numeric entities
        $text=preg_replace("/(&\w+);/i", " ", $text); // For literal entities
        return $text;
    }
*/
    public function make_seo_url($name,$lengthtext=100)
    {
        $seo_title=stripslashes($name);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace(chr(160),"_",$seo_title);
        $seo_title=str_replace("~","",$seo_title);
        $seo_title=str_replace("(","",$seo_title);
        $seo_title=str_replace(")","",$seo_title);
        $seo_title=str_replace("]","",$seo_title);
        $seo_title=str_replace("[","",$seo_title);
        $seo_title=str_replace("}","",$seo_title);
        $seo_title=str_replace("{","",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("\\","",$seo_title);
        $seo_title=str_replace("+","",$seo_title);
        $seo_title=str_replace(":","",$seo_title);
        $seo_title=str_replace(";","",$seo_title);
        $seo_title=str_replace("\"","",$seo_title);
        $seo_title=str_replace("<","",$seo_title);
        $seo_title=str_replace(">","",$seo_title);
        $seo_title=str_replace("?","",$seo_title);
        $seo_title=str_replace(",",".",$seo_title);
        $seo_title=str_replace("!","",$seo_title);
        $seo_title=str_replace("`","",$seo_title);
        $seo_title=str_replace("'","",$seo_title);
        $seo_title=str_replace("@","",$seo_title);
        $seo_title=str_replace("#","",$seo_title);
        $seo_title=str_replace("$","",$seo_title);
        $seo_title=str_replace("%","",$seo_title);
        $seo_title=str_replace("^","",$seo_title);
        $seo_title=str_replace("&","",$seo_title);
        $seo_title=str_replace("*","",$seo_title);
        $seo_title=str_replace("|","",$seo_title);
        $seo_title=str_replace("=","",$seo_title);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("?","_",$seo_title);
        $seo_title=str_replace("#","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("&amp;","_",$seo_title);
        $seo_title=str_replace("__","_",$seo_title);
        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }

    private function make_xml_friendly( $url,$replay=" " )
    {
        $find = array(' ',
      '&quot;',
      'quot',
      '&',
      'amp;',
      '\r\n',
      '\n',
      '/',
      '\\',
      '+',
      '<',
      '>');
       $url = str_replace ($find, $replay, $url);
        return $url;
    }
    private function make_url_friendly( $url,$replay="-" )
    {
        $url = trim($url);
        $url = strtolower($url);
        $find = array('<b>','</b>');
        $url = str_replace ($find, '', $url);
        $url = preg_replace('/<(\/{0,1})img(.*?)(\/{0,1})\>/', 'image', $url);
        $find = array(' ',
      '&quot;',
      'quot',
      '&',
      'amp;',
      '\r\n',
      '\n',
      '/',
      '\\',
      '+',
      '<',
      '>');
       $url = str_replace ($find, $replay, $url);

        $find = array('/[^a-z0-9\-<>]/',
      '/[\-]+/',
      '/<[^>]*>/');
       $repl = array('',
      '-',
      '');
       $url = preg_replace ($find, $repl, $url);
       $url = str_replace ('--', '-', $url);

        return $url;
    }
//----'-------
    private function text( $prefix, $key, array $vars = null )
    {
    }
}


