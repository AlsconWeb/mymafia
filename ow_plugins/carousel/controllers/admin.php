<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


class CAROUSEL_CTRL_Admin extends ADMIN_CTRL_Abstract
{

    public function dept()
    {
        $content="";
        $this->setPageTitle(OW::getLanguage()->text('carousel', 'admin_dept_title'));
        $this->setPageHeading(OW::getLanguage()->text('carousel', 'admin_dept_heading'));    
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
        $curent_url=OW_URL_HOME;
        $config = OW::getConfig();
        $pluginStaticURL =OW::getPluginManager()->getPlugin('carousel')->getUserFilesUrl();
        $pluginStaticDir =OW::getPluginManager()->getPlugin('carousel')->getUserFilesDir();
        $content="";
        if (!isset($_POST['save'])) $_POST['save']="";
        if ($is_admin AND $id_user>0 AND $_POST['save']=="besave"){



/*
            if ($_POST['c_protectkey']){
                $carousel_protect_key=$_POST['c_protectkey'];
            }else{
                $carousel_protect_key=substr(md5(date('d-m-Y H:i:s')),0,10);
            }
            if ($_POST['c_generateforusers']){
                $generateforusers=1;
            }else{
                $generateforusers=0;
            }
            $carousel_max_items_eachplugin=$_POST['c_maxitemseachplugin'];
            if (!$carousel_max_items_eachplugin) $carousel_max_items_eachplugin=10;
            if ($carousel_max_items_eachplugin>50) $carousel_max_items_eachplugin=50;
            $carousel_max_items_sitemap=$_POST['c_maxitemsforsitemap'];
            if (!$carousel_max_items_sitemap) $carousel_max_items_sitemap=1000;
            if ($carousel_max_items_sitemap>9999) $carousel_max_items_sitemap=9999;

            $config->saveConfig('carousel', 'carousel_protect_key', $carousel_protect_key);
            $config->saveConfig('carousel', 'carousel_generateforusers', $generateforusers);
            $config->saveConfig('carousel', 'carousel_max_items_eachplugin', $carousel_max_items_eachplugin);
            $config->saveConfig('carousel', 'carousel_max_items_sitemap', $carousel_max_items_sitemap);
*/
//            $config->saveConfig('carousel', 'carousel_enable', $_POST['c_carousel_enable']);

            $config->saveConfig('carousel', 'width', $_POST['c_width']);
            $config->saveConfig('carousel', 'height', $_POST['c_height']);
            $config->saveConfig('carousel', 'resize_image', $_POST['c_resize_image']);
            $config->saveConfig('carousel', 'corect_exif_image', $_POST['c_corect_exif_image']);

            $config->saveConfig('carousel', 'auto_thumbal_button', $_POST['c_auto_thumbal_button']);


            for ($i=1;$i<6;$i++){

//-----big
                $was_del_big=false;
                if (isset($_POST['c_delete_image_'.$i]) AND $_POST['c_delete_image_'.$i]==1){
                    CAROUSEL_BOL_Service::getInstance()->file_delete($pluginStaticDir."image_".$i.".jpg");
                    CAROUSEL_BOL_Service::getInstance()->file_delete($pluginStaticDir."image_button_".$i.".jpg");
                    $config->saveConfig('carousel', 'image_url_'.$i, '');
                    $config->saveConfig('carousel', 'button_content_'.$i, '');
                    $was_del_big=true;
                }

                if (isset($_FILES['image_'.$i])){
                    if ($_FILES['image_'.$i]['error']=="0" AND $_FILES['image_'.$i]['size']>0 AND isset($_FILES['image_'.$i]['tmp_name']) AND $_FILES['image_'.$i]['tmp_name']){
                            if (OW::getConfig()->getValue('carousel', 'corect_exif_image')=="1"){
                                CAROUSEL_BOL_Service::getInstance()->corect_exif($_FILES['image_'.$i]['tmp_name']);//corect orienatation
                            }
                            if (OW::getConfig()->getValue('carousel', 'resize_image')=="1"){
                                $wx=OW::getConfig()->getValue('carousel', 'width');
                                $wy=OW::getConfig()->getValue('carousel', 'height');
                                if (!$wx) $wx=1000;
                                if (!$wy) $wy=1000;
                                CAROUSEL_BOL_Service::getInstance()->image_copy_resize($_FILES['image_'.$i]['tmp_name'],$pluginStaticDir."image_".$i.".jpg",false,$wx,$wy);
                            }else{
                                CAROUSEL_BOL_Service::getInstance()->file_copy($_FILES['image_'.$i]['tmp_name'],$pluginStaticDir."image_".$i.".jpg");
                            }
                    }
                }

//-----button
                if ($was_del_big==false){
                    $config->saveConfig('carousel', 'image_url_'.$i, $_POST['c_image_url_'.$i]);
                    $config->saveConfig('carousel', 'button_content_'.$i, $_POST['c_button_content_'.$i]);
                
                    if (isset($_POST['c_delete_image_button_'.$i]) AND $_POST['c_delete_image_button_'.$i]==1){
                        CAROUSEL_BOL_Service::getInstance()->file_delete($pluginStaticDir."image_button_".$i.".jpg");
                    }

                    if (isset($_FILES['image_button_'.$i]) AND $_FILES['image_button_'.$i]['error']=="0" AND $_FILES['image_button_'.$i]['size']>0 AND isset($_FILES['image_button_'.$i]['tmp_name']) AND $_FILES['image_button_'.$i]['tmp_name']){

                            if (OW::getConfig()->getValue('carousel', 'corect_exif_image')=="1"){
                                CAROUSEL_BOL_Service::getInstance()->corect_exif($_FILES['image_button_'.$i]['tmp_name']);//corect orienatation
                            }

                            if (OW::getConfig()->getValue('carousel', 'resize_image')=="1"){
                                CAROUSEL_BOL_Service::getInstance()->image_copy_resize($_FILES['image_button_'.$i]['tmp_name'],$pluginStaticDir."image_button_".$i.".jpg",false,50,50);
                            }else{
                                CAROUSEL_BOL_Service::getInstance()->file_copy($_FILES['image_button_'.$i]['tmp_name'],$pluginStaticDir."image_button_".$i.".jpg");
                            }

                    }else if (OW::getConfig()->getValue('carousel', 'auto_thumbal_button')=="1") {//make from big image automaticly
                            if (OW::getConfig()->getValue('carousel', 'resize_image')=="1"){
                                CAROUSEL_BOL_Service::getInstance()->image_copy_resize($_FILES['image_'.$i]['tmp_name'],$pluginStaticDir."image_button_".$i.".jpg",false,50,50);
                            }else{
                                CAROUSEL_BOL_Service::getInstance()->file_copy($_FILES['image_'.$i]['tmp_name'],$pluginStaticDir."image_button_".$i.".jpg");
                            }
                    }
                }

            }//for

//print_r($_FILES);
//print_r($_POST);
//exit;
            OW::getApplication()->redirect($curent_url."admin/plugins/carousel");
        }


        $content .="<form action=\"".$curent_url."admin/plugins/carousel\" method=\"post\" enctype=\"multipart/form-data\">";
        $content .="<input type=\"hidden\" name=\"save\" value=\"besave\">";
//        $content .="<table style=\"width:auto; ow_table_1 ow_form\">";
        $content .="<table style=\"ow_table_1 ow_form\">";


        $content .="<tr class=\"ow_tr_first\">
            <th class=\"ow_name ow_txtleft\" colspan=\"3\">
                <span class=\"ow_section_icon ow_ic_info\">";
                $content .=OW::getLanguage()->text('carousel', 'general_setting');
                $content .="</span>
            </th>
        </tr>";

/*
        $content .="<tr>";
        $content .="<td colspan=\"2\" wrap=\"wrap\" style=\"background-color:#eee;\">";
        $content .=OW::getLanguage()->text('carousel', 'general_setting');
        $content .="</td>";
        $content .="</tr>";
*/

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'with_slider').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'width');
        if (!$itels) $itels="450";
        $content .="<input type=\"text\" name=\"c_width\" value=\"".$itels."\" style=\"display:inline-block;width:100px;\">&nbsp;Defautl: 450 [px]";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'height_slider').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'height');
        if (!$itels) $itels="220";
        $content .="<input type=\"text\" name=\"c_height\" value=\"".$itels."\" style=\"display:inline-block;width:100px;\">&nbsp;Defautl: 220 [px]";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr class=\"ow_alt1\">";
        $content .="<td  class=\"ow_label\" style=\"min-width:300px;\">";
        $content .="<b>".OW::getLanguage()->text('carousel', 'resize_image').":</b>";
        $content .="</td>";
        $content .="<td  class=\"ow_value\">";
        $content .="<select name=\"c_resize_image\">";
        if (OW::getConfig()->getValue('carousel', 'resize_image')=="1" OR OW::getConfig()->getValue('carousel', 'resize_image')=="") $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('carousel', 'yes')."</option>";
        if (OW::getConfig()->getValue('carousel', 'resize_image')=="0" ) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('carousel', 'no')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr class=\"ow_alt1\">";
        $content .="<td  class=\"ow_label\" style=\"min-width:300px;\">";
        $content .="<b>".OW::getLanguage()->text('carousel', 'corect_exif_image').":</b>";
        $content .="</td>";
        $content .="<td  class=\"ow_value\">";
        $content .="<select name=\"c_corect_exif_image\">";
        if (OW::getConfig()->getValue('carousel', 'corect_exif_image')=="1" OR OW::getConfig()->getValue('carousel', 'corect_exif_image')=="") $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('carousel', 'yes')."</option>";
        if (OW::getConfig()->getValue('carousel', 'corect_exif_image')=="0" ) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('carousel', 'no')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";



        $content .="<tr class=\"ow_alt1\">";
        $content .="<td  class=\"ow_label\" style=\"min-width:300px;\">";
        $content .="<b>".OW::getLanguage()->text('carousel', 'auto_thumbal_button').":</b>";
        $content .="</td>";
        $content .="<td  class=\"ow_value\">";
        $content .="<select name=\"c_auto_thumbal_button\">";
        if (OW::getConfig()->getValue('carousel', 'auto_thumbal_button')=="1") $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('carousel', 'yes')."</option>";
        if (OW::getConfig()->getValue('carousel', 'auto_thumbal_button')=="0" OR OW::getConfig()->getValue('carousel', 'auto_thumbal_button')=="" ) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('carousel', 'no')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";



/*
        $content .="<tr>";
        $content .="<td colspan=\"2\" wrap=\"wrap\" style=\"background-color:#eee;\">";
        $content .=OW::getLanguage()->text('carousel', 'xx');
        $content .="</td>";
        $content .="</tr>";
*/

/*
        $content .="<tr class=\"ow_alt1\">";
        $content .="<td  class=\"ow_label\" style=\"min-width:300px;\">";
        $content .="<b>".OW::getLanguage()->text('carousel', 'carousel_enable').":</b>";
        $content .="</td>";
        $content .="<td  class=\"ow_value\">";
        $content .="<select name=\"c_carousel_enable\">";
        if (OW::getConfig()->getValue('carousel', 'carousel_enable')=="1" OR OW::getConfig()->getValue('carousel', 'carousel_enable')=="") $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('carousel', 'yes')."</option>";
        if (OW::getConfig()->getValue('carousel', 'carousel_enable')=="0" ) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('carousel', 'no')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";
*/

//------1111


/*
        $content .="<tr class=\"ow_alt1\">";
        $content .="<td colspan=\"\">";
        $content .="<i>".OW::getLanguage()->text('carousel', 'cart_number')." 1:</i>";
        $content .="</td>";
        $content .="<td colspan=\"\">";
        $content .="<hr/>";
        $content .="</td>";
        $content .="</tr>";
*/

        $content .="<tr class=\"ow_tr_first\">";
        $content .="<th colspan=\"2\" class=\"ow_center\">";
//        $content .="<hr/>";
        $content .="<i>".OW::getLanguage()->text('carousel', 'cat_number')." 1:</i>";
//        $content .="<hr/>";
        $content .="</th>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'select_image').":</b>";
        $content .="</td>";
        $content .="<td >";

        $content .="<input type=\"file\" name=\"image_1\" >";
        $content .="<div class=\"clearfix\">".OW::getLanguage()->text('carousel', 'image_info')."</div>";

//        if (OW::getConfig()->getValue('carousel', 'image_url_1')){
        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_1.jpg")){
            $content .="<br/>";
            $content .="<img src=\"".$pluginStaticURL."image_1.jpg\" style=\"max-width:250px;max-height:250px;\">";
            $content .="<br/>";
            $content .="<input type=\"checkbox\" name=\"c_delete_image_1\" value=\"1\">&nbsp;".OW::getLanguage()->text('carousel', 'delete_image_card');
        }
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'image_url').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'image_url_1');
        if (!$itels) $itels="";
        $content .="<input type=\"text\" name=\"c_image_url_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'admin_maxitemseachplugin_info')."";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'select_button_image').":</b>";
        $content .="</td>";
        $content .="<td >";
//        $itels=OW::getConfig()->getValue('carousel', 'image_url_1');
//        if (!$itels) $itels="";
//        $content .="<input type=\"text\" name=\"c_image_url_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
        $content .="<input type=\"file\" name=\"image_button_1\" >";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'image_button_info')."";
        $content .="<div class=\"clearfix\">".OW::getLanguage()->text('carousel', 'image_button_info')."</div>";
//        if (OW::getConfig()->getValue('carousel', 'image_button_1')){
        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_button_1.jpg")){
            $content .="<br/>";
            $content .="<img src=\"".$pluginStaticURL."image_button_1.jpg\" style=\"max-width:50px;max-height:50px;\">";
            $content .="<br/>";
            $content .="<input type=\"checkbox\" name=\"c_delete_image_button_1\" value=\"1\">&nbsp;".OW::getLanguage()->text('carousel', 'delete_image');
        }
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'button_content').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'button_content_1');
        if (!$itels) $itels="";
        $content .="<input type=\"text\" name=\"c_button_content_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'button_description')."\">";
        $content .="</td>";
        $content .="</tr>";


//-----222

        $content .="<tr class=\"ow_tr_first\">";
        $content .="<th colspan=\"2\" class=\"ow_center\">";
//        $content .="<hr/>";
        $content .="<i>".OW::getLanguage()->text('carousel', 'cat_number')." 2:</i>";
//        $content .="<hr/>";
        $content .="</th>";
        $content .="</tr>";
/*
        $content .="<tr class=\"ow_alt1\">";
        $content .="<td colspan=\"2\" class=\"ow_center\">";
        $content .="<i>".OW::getLanguage()->text('carousel', 'cart_number')." 2:</i>";
        $content .="</td>";
        $content .="</tr>";
*/

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'select_image').":</b>";
        $content .="</td>";
        $content .="<td >";
//        $itels=OW::getConfig()->getValue('carousel', 'image_url_1');
//        if (!$itels) $itels="";
//        $content .="<input type=\"text\" name=\"c_image_url_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
        $content .="<input type=\"file\" name=\"image_2\" >";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'image_info')."";
        $content .="<div class=\"clearfix\">".OW::getLanguage()->text('carousel', 'image_info')."</div>";

        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_2.jpg")){
            $content .="<br/>";
            $content .="<img src=\"".$pluginStaticURL."image_2.jpg\" style=\"max-width:250px;max-height:250px;\">";
            $content .="<br/>";
            $content .="<input type=\"checkbox\" name=\"c_delete_image_2\" value=\"1\">&nbsp;".OW::getLanguage()->text('carousel', 'delete_image_card');
        }
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'image_url').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'image_url_2');
        if (!$itels) $itels="";
        $content .="<input type=\"text\" name=\"c_image_url_2\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'admin_maxitemseachplugin_info')."";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'select_button_image').":</b>";
        $content .="</td>";
        $content .="<td >";
//        $itels=OW::getConfig()->getValue('carousel', 'image_url_1');
//        if (!$itels) $itels="";
//        $content .="<input type=\"text\" name=\"c_image_url_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
        $content .="<input type=\"file\" name=\"image_button_2\" >";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'image_button_info')."";
        $content .="<div class=\"clearfix\">".OW::getLanguage()->text('carousel', 'image_button_info')."</div>";
        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_button_2.jpg")){
            $content .="<br/>";
            $content .="<img src=\"".$pluginStaticURL."image_button_2.jpg\" style=\"max-width:50px;max-height:50px;\">";
            $content .="<br/>";
            $content .="<input type=\"checkbox\" name=\"c_delete_image_button_2\" value=\"1\">&nbsp;".OW::getLanguage()->text('carousel', 'delete_image');
        }
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'button_content').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'button_content_2');
        if (!$itels) $itels="";
        $content .="<input type=\"text\" name=\"c_button_content_2\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'button_description')."\">";
        $content .="</td>";
        $content .="</tr>";



//-------333

/*
        $content .="<tr class=\"ow_alt1\">";
        $content .="<td colspan=\"\">";
        $content .="<i>".OW::getLanguage()->text('carousel', 'cart_number')." 3:</i>";
        $content .="</td>";
        $content .="<td colspan=\"\">";
        $content .="<hr/>";
        $content .="</td>";
        $content .="</tr>";
*/
        $content .="<tr class=\"ow_tr_first\">";
        $content .="<th colspan=\"2\" class=\"ow_center\">";
//        $content .="<hr/>";
        $content .="<i>".OW::getLanguage()->text('carousel', 'cat_number')." 3:</i>";
//        $content .="<hr/>";
        $content .="</th>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'select_image').":</b>";
        $content .="</td>";
        $content .="<td >";
//        $itels=OW::getConfig()->getValue('carousel', 'image_url_1');
//        if (!$itels) $itels="";
//        $content .="<input type=\"text\" name=\"c_image_url_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
        $content .="<input type=\"file\" name=\"image_3\" >";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'image_info')."";
        $content .="<div class=\"clearfix\">".OW::getLanguage()->text('carousel', 'image_info')."</div>";

        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_3.jpg")){
            $content .="<br/>";
            $content .="<img src=\"".$pluginStaticURL."image_3.jpg\" style=\"max-width:250px;max-height:250px;\">";
            $content .="<br/>";
            $content .="<input type=\"checkbox\" name=\"c_delete_image_3\" value=\"1\">&nbsp;".OW::getLanguage()->text('carousel', 'delete_image_card');
        }
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'image_url').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'image_url_3');
        if (!$itels) $itels="";
        $content .="<input type=\"text\" name=\"c_image_url_3\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'admin_maxitemseachplugin_info')."";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'select_button_image').":</b>";
        $content .="</td>";
        $content .="<td >";
//        $itels=OW::getConfig()->getValue('carousel', 'image_url_1');
//        if (!$itels) $itels="";
//        $content .="<input type=\"text\" name=\"c_image_url_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
        $content .="<input type=\"file\" name=\"image_button_3\" >";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'image_button_info')."";
        $content .="<div class=\"clearfix\">".OW::getLanguage()->text('carousel', 'image_button_info')."</div>";
        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_button_3.jpg")){
            $content .="<br/>";
            $content .="<img src=\"".$pluginStaticURL."image_button_3.jpg\" style=\"max-width:50px;max-height:50px;\">";
            $content .="<br/>";
            $content .="<input type=\"checkbox\" name=\"c_delete_image_button_3\" value=\"1\">&nbsp;".OW::getLanguage()->text('carousel', 'delete_image');
        }
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'button_content').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'button_content_3');
        if (!$itels) $itels="";
        $content .="<input type=\"text\" name=\"c_button_content_3\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'button_description')."\">";
        $content .="</td>";
        $content .="</tr>";


//------44444

/*
        $content .="<tr class=\"ow_alt1\">";
        $content .="<td colspan=\"\">";
        $content .="<i>".OW::getLanguage()->text('carousel', 'cart_number')." 4:</i>";
        $content .="</td>";
        $content .="<td colspan=\"\">";
        $content .="<hr/>";
        $content .="</td>";
        $content .="</tr>";
*/

        $content .="<tr class=\"ow_tr_first\">";
        $content .="<th colspan=\"2\" class=\"ow_center\">";
//        $content .="<hr/>";
        $content .="<i>".OW::getLanguage()->text('carousel', 'cat_number')." 4:</i>";
//        $content .="<hr/>";
        $content .="</th>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'select_image').":</b>";
        $content .="</td>";
        $content .="<td >";
//        $itels=OW::getConfig()->getValue('carousel', 'image_url_1');
//        if (!$itels) $itels="";
//        $content .="<input type=\"text\" name=\"c_image_url_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
        $content .="<input type=\"file\" name=\"image_4\" >";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'image_info')."";
        $content .="<div class=\"clearfix\">".OW::getLanguage()->text('carousel', 'image_info')."</div>";

        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_4.jpg")){
            $content .="<br/>";
            $content .="<img src=\"".$pluginStaticURL."image_4.jpg\" style=\"max-width:250px;max-height:250px;\">";
            $content .="<br/>";
            $content .="<input type=\"checkbox\" name=\"c_delete_image_4\" value=\"1\">&nbsp;".OW::getLanguage()->text('carousel', 'delete_image_card');
        }
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'image_url').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'image_url_4');
        if (!$itels) $itels="";
        $content .="<input type=\"text\" name=\"c_image_url_4\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'admin_maxitemseachplugin_info')."";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'select_button_image').":</b>";
        $content .="</td>";
        $content .="<td >";
//        $itels=OW::getConfig()->getValue('carousel', 'image_url_1');
//        if (!$itels) $itels="";
//        $content .="<input type=\"text\" name=\"c_image_url_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
        $content .="<input type=\"file\" name=\"image_button_4\" >";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'image_button_info')."";
        $content .="<div class=\"clearfix\">".OW::getLanguage()->text('carousel', 'image_button_info')."</div>";

        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_button_4.jpg")){
            $content .="<br/>";
            $content .="<img src=\"".$pluginStaticURL."image_button_4.jpg\" style=\"max-width:50px;max-height:50px;\">";
            $content .="<br/>";
            $content .="<input type=\"checkbox\" name=\"c_delete_image_button_4\" value=\"1\">&nbsp;".OW::getLanguage()->text('carousel', 'delete_image');
        }
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'button_content').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'button_content_4');
        if (!$itels) $itels="";
        $content .="<input type=\"text\" name=\"c_button_content_4\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'button_description')."\">";
        $content .="</td>";
        $content .="</tr>";

//-----------------555
/*
        $content .="<tr class=\"ow_alt1\">";
        $content .="<td colspan=\"\">";
        $content .="<i>".OW::getLanguage()->text('carousel', 'cart_number')." 5:</i>";
        $content .="</td>";
        $content .="<td colspan=\"\">";
        $content .="<hr/>";
        $content .="</td>";
        $content .="</tr>";
*/
        $content .="<tr class=\"ow_tr_first\">";
        $content .="<th colspan=\"2\" class=\"ow_center\">";
//        $content .="<hr/>";
        $content .="<i>".OW::getLanguage()->text('carousel', 'cat_number')." 5:</i>";
//        $content .="<hr/>";
        $content .="</th>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'select_image').":</b>";
        $content .="</td>";
        $content .="<td >";
//        $itels=OW::getConfig()->getValue('carousel', 'image_url_1');
//        if (!$itels) $itels="";
//        $content .="<input type=\"text\" name=\"c_image_url_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
        $content .="<input type=\"file\" name=\"image_5\" >";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'image_info')."";
        $content .="<div class=\"clearfix\">".OW::getLanguage()->text('carousel', 'image_info')."</div>";

        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_5.jpg")){
            $content .="<br/>";
            $content .="<img src=\"".$pluginStaticURL."image_5.jpg\" style=\"max-width:250px;max-height:250px;\">";
            $content .="<br/>";
            $content .="<input type=\"checkbox\" name=\"c_delete_image_5\" value=\"1\">&nbsp;".OW::getLanguage()->text('carousel', 'delete_image_card');
        }
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'image_url').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'image_url_5');
        if (!$itels) $itels="";
        $content .="<input type=\"text\" name=\"c_image_url_5\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'admin_maxitemseachplugin_info')."";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'select_button_image').":</b>";
        $content .="</td>";
        $content .="<td >";
//        $itels=OW::getConfig()->getValue('carousel', 'image_url_1');
//        if (!$itels) $itels="";
//        $content .="<input type=\"text\" name=\"c_image_url_1\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'http_url')."\">";
        $content .="<input type=\"file\" name=\"image_button_5\" >";
//        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'image_button_info')."";
        $content .="<div class=\"clearfix\">".OW::getLanguage()->text('carousel', 'image_button_info')."</div>";
        if (CAROUSEL_BOL_Service::getInstance()->file_exist($pluginStaticDir."image_button_5.jpg")){
            $content .="<br/>";
            $content .="<img src=\"".$pluginStaticURL."image_button_5.jpg\" style=\"max-width:50px;max-height:50px;\">";
            $content .="<br/>";
            $content .="<input type=\"checkbox\" name=\"c_delete_image_button_5\" value=\"1\">&nbsp;".OW::getLanguage()->text('carousel', 'delete_image');
        }
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'button_content').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'button_content_5');
        if (!$itels) $itels="";
        $content .="<input type=\"text\" name=\"c_button_content_5\" value=\"".$itels."\" style=\"display:inline-block;\" placeholder=\"".OW::getLanguage()->text('carousel', 'button_description')."\">";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr class=\"ow_alt1\">";
        $content .="<td colspan=\"2\">";
        $content .="<hr/>";
        $content .="</td>";
        $content .="</tr>";










/*
        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'na_clock_').":</b>";
        $content .="</td>";
        $content .="<td nowrap=\"nowrap\">";
        $value=OW::getConfig()->getValue('carousel', 'toclock');
        if ($value=="0") $sel="";
            else $sel=" CHECKED ";
        $content .="<input ".$sel." type=\"checkbox\" name=\"c_toclock\" value=\"1\">";
        $content .="</td>";
        $content .="</tr>";
*/

/*
        $content .="<tr>";
        $content .="<td colspan=\"2\" wrap=\"wrap\" style=\"background-color:#eee;\">";
        $content .=OW::getLanguage()->text('carousel', 'admin_protectkey_informationofuse');
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'admin_userprofiledata').":</b>";
        $content .="</td>";
        $content .="<td >";
        $content .="<select name=\"c_generateforusers\">";
        if (OW::getConfig()->getValue('carousel', 'carousel_generateforusers') OR !OW::getConfig()->getValue('carousel', 'carousel_generateforusers')) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('carousel', 'admin_dontgeneratememberstoo')."</option>";
        if (OW::getConfig()->getValue('carousel', 'carousel_generateforusers')==1) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('carousel', 'admin_generatememberstoo')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'admin_maxitemseachplugin').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('carousel', 'carousel_max_items_eachplugin');
        if (!$itels) $itels=10;
        $content .="<input type=\"text\" name=\"c_maxitemseachplugin\" value=\"".$itels."\" style=\"display:inline-block;width:100px;\">";
        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'admin_maxitemseachplugin_info')."";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('carousel', 'admin_maxitemsforsitemap').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itelss=OW::getConfig()->getValue('carousel', 'carousel_max_items_sitemap');
        if (!$itelss) $itelss=1000;
        $content .="<input type=\"text\" name=\"c_maxitemsforsitemap\" value=\"".$itelss."\" style=\"display:inline-block;width:100px;\">";
        $content .="&nbsp;".OW::getLanguage()->text('carousel', 'admin_maxitemsforsitemap_info')."";
        $content .="</td>";
        $content .="</tr>";
*/
        $content .="<tr>";
        $content .="<td colspan=\"2\">";
//        $content .="<input type=\"submit\" name=\"dosave\" value=\"".OW::getLanguage()->text('carousel', 'admin_save')."\">";
        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('carousel', 'admin_save')."\" name=\"dosave\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
        $content .="</td>";
        $content .="</tr>";
        $content .="</table>";
        $content .="</form>";
        $this->assign('content', $content);
    }

    public function delete( $params )
    {
        $this->redirect(OW::getRouter()->urlForRoute('carousel.admin'));
    }



}
