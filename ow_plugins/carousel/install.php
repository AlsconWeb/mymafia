<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


OW::getLanguage()->importPluginLangs(OW::getPluginManager()->getPlugin('carousel')->getRootDir().'langs.zip', 'carousel');
OW::getPluginManager()->addPluginSettingsRouteName('carousel', 'carousel.admin');

//BOL_LanguageService::getInstance()->addPrefix('contactus', 'Contact Us');

$config = OW::getConfig();
if ( !$config->configExists('carousel', 'carousel_enable') ){
    $config->addConfig('carousel', 'carousel_enable', '1', '');
}

if ( !$config->configExists('carousel', 'width') ){
    $config->addConfig('carousel', 'width', '450', '');
}
if ( !$config->configExists('carousel', 'height') ){
    $config->addConfig('carousel', 'height', '220', '');
}
if ( !$config->configExists('carousel', 'resize_image') ){
    $config->addConfig('carousel', 'resize_image', '1', '');
}
if ( !$config->configExists('carousel', 'corect_exif_image') ){
    $config->addConfig('carousel', 'corect_exif_image', '0', '');
}
if ( !$config->configExists('carousel', 'auto_thumbal_button') ){
    $config->addConfig('carousel', 'auto_thumbal_button', '0', '');
}


if ( !$config->configExists('carousel', 'image_url_1') ){
    $config->addConfig('carousel', 'image_url_1', '', '');
}
if ( !$config->configExists('carousel', 'button_content_1') ){
    $config->addConfig('carousel', 'button_content_1', '', '');
}
if ( !$config->configExists('carousel', 'image_url_2') ){
    $config->addConfig('carousel', 'image_url_2', '', '');
}
if ( !$config->configExists('carousel', 'button_content_2') ){
    $config->addConfig('carousel', 'button_content_2', '', '');
}
if ( !$config->configExists('carousel', 'image_url_3') ){
    $config->addConfig('carousel', 'image_url_3', '', '');
}
if ( !$config->configExists('carousel', 'button_content_3') ){
    $config->addConfig('carousel', 'button_content_3', '', '');
}
if ( !$config->configExists('carousel', 'image_url_4') ){
    $config->addConfig('carousel', 'image_url_4', '', '');
}
if ( !$config->configExists('carousel', 'button_content_4') ){
    $config->addConfig('carousel', 'button_content_4', '', '');
}
if ( !$config->configExists('carousel', 'image_url_5') ){
    $config->addConfig('carousel', 'image_url_5', '', '');
}
if ( !$config->configExists('carousel', 'button_content_5') ){
    $config->addConfig('carousel', 'button_content_5', '', '');
}

/*
if ( !$config->configExists('carousel', 'carousel_protect_key') ){
    $key=substr(md5(date('d-m-Y H:i:s')),0,10);
    $config->addConfig('carousel', 'carousel_protect_key', $key, 'Protect Key');
}

if ( !$config->configExists('carousel', 'carousel_generateforusers') ){
    $config->addConfig('carousel', 'carousel_generateforusers', '1', 'Site Map for members too');
}

if ( !$config->configExists('carousel', 'carousel_max_items_eachplugin') ){
    $config->addConfig('carousel', 'carousel_max_items_eachplugin', '10', 'Max items for each plugins');
}
if ( !$config->configExists('carousel', 'carousel_max_items_sitemap') ){
    $config->addConfig('carousel', 'carousel_max_items_sitemap', '9999', 'Max items sitemap');
}


$sql = "CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "carousel_department` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`email` VARCHAR(200) NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE=MyISAM
ROW_FORMAT=DEFAULT";
OW::getDbo()->query($sql);
*/
