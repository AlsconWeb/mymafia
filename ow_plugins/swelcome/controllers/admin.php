<?php

class SWELCOME_CTRL_Admin extends ADMIN_CTRL_Abstract {

    public function __construct() {
        parent::__construct();

        if (OW::getRequest()->isAjax()) {
            return;
        }
    }

    public function index() {
        $language = OW::getLanguage();
        $config = OW::getConfig();

        $this->setPageHeading(OW::getLanguage()->text('swelcome', 'admin_settings_title'));
        $this->setPageTitle(OW::getLanguage()->text('swelcome', 'admin_settings_title'));
        $this->setPageHeadingIconClass('ow_ic_gear_wheel');

   $textinfadmin = OW::getLanguage()->text('swelcome', 'swelcome_infadmin'); 
		
	$this->assign("textinfadmin",$textinfadmin);

$urleal =  OW_URL_HOME."admin/settings/languages?&prefix=swelcome";
	$this->assign("urleal",$urleal);

   $imgwadmin = OW::getPluginManager()->getPlugin('swelcome')->getStaticUrl() . 'images/wadminflag.png'; 
		
	$this->assign("imgwadmin",$imgwadmin); 
    }

}
