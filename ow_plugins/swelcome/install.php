<?php

$config = OW::getConfig();
BOL_LanguageService::getInstance()->addPrefix('swelcome', 'Welcome Pluss');
OW::getLanguage()->importPluginLangs(OW::getPluginManager()->getPlugin('swelcome')->getRootDir().'langs.zip', 'swelcome');
OW::getPluginManager()->addPluginSettingsRouteName('swelcome', 'swelcome_admin_index');
