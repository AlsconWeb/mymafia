<?php

class SWELCOME_CMP_Widgetcontrol extends BASE_CLASS_Widget 
{
   
    public function __construct(BASE_CLASS_WidgetParameter $params) 
    {
        parent::__construct();

        $lang = OW::getLanguage();
        
        OW::getDocument()->addScript(OW::getPluginManager()->getPlugin('swelcome')->getStaticJsUrl() . 'swelcome.js'); 



$user = OW::getUser()->getId();
$user_service = BOL_UserService::getInstance();
$avat_service = BOL_AvatarService::getInstance();
$displayname  = $user_service->getDisplayName($user);
	$this->assign("displayname",$displayname);



   $msg1 = OW::getLanguage()->text('swelcome', 'widget_msg1'); 
		
	$this->assign("msg1",$msg1);

   $msg2 = OW::getLanguage()->text('swelcome', 'widget_msg2'); 
		
	$this->assign("msg2",$msg2);

   $msg3 = OW::getLanguage()->text('swelcome', 'widget_msg3'); 
		
	$this->assign("msg3",$msg3);

   $msg4 = OW::getLanguage()->text('swelcome', 'widget_msg4'); 
		
	$this->assign("msg4",$msg4);

   $msg5 = OW::getLanguage()->text('swelcome', 'widget_msg5'); 
		
	$this->assign("msg5",$msg5);

   $msg6 = OW::getLanguage()->text('swelcome', 'widget_msg6'); 
		
	$this->assign("msg6",$msg6);


    }//close function
 

    public static function getStandardSettingValueList() 
    {
        return array(
            self::SETTING_TITLE => OW::getLanguage()->text('swelcome', 'user_widget_title'),
            self::SETTING_ICON => self::ICON_CLOCK,
            self::SETTING_SHOW_TITLE => true,
            self::SETTING_WRAP_IN_BOX => true
        );
		
    }//close function
	

    public static function getAccess() 
    {
        return self::ACCESS_ALL;
    }//close function

}//close class

