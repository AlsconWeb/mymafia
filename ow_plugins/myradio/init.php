<?php

/**
 * This software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is
 * licensed under The BSD license.

 * ---
 * Copyright (c) 2011, Oxwall Foundation
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice, this list of conditions and
 *  the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *  the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  - Neither the name of the Oxwall Foundation nor the names of its contributors may be used to endorse or promote products
 *  derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//$plugin = OW::getPluginManager()->getPlugin('myradio');
//OW::getRouter()->addRoute(new OW_Route('wp_auto', 'wp_auto', 'SUPERPLUGIN_CTRL_Index', 'viewList'));

OW::getRouter()->addRoute(new OW_Route('myradio_loginox', 'ox_auto', 'MYRADIO_CTRL_Connect', 'login_ox'));
OW::getRouter()->addRoute(new OW_Route('myradio_login', 'wp_auto', 'MYRADIO_CTRL_Connect', 'login'));
OW::getRouter()->addRoute(new OW_Route('connect.buy', 'buy', 'MYRADIO_CTRL_Connect', 'buy'));
OW::getRouter()->addRoute(new OW_Route('connect.buy_link', 'buy_link', 'MYRADIO_CTRL_Connect', 'buy_link'));
OW::getRouter()->addRoute(new OW_Route('connect.shop_success', 'shop_success', 'MYRADIO_CTRL_Connect', 'shopsuccess'));
OW::getRouter()->addRoute(new OW_Route('connect.shop_fail', 'shop_fail', 'MYRADIO_CTRL_Connect', 'shopfail'));
OW::getRouter()->addRoute(new OW_Route('myradio_user', 'wp_user', 'MYRADIO_CTRL_Connect', 'user'));
OW::getRouter()->addRoute(new OW_Route('connect.shop', 'shop', 'MYRADIO_CTRL_Connect', 'shop'));
OW::getRouter()->addRoute(new OW_Route('myradio_api', 'mapi', 'MYRADIO_CTRL_Mapi', 'Mapi'));
OW::getRouter()->addRoute(new OW_Route('connect.rate', 'rate', 'MYRADIO_CTRL_Connect', 'rate'));
OW::getRouter()->addRoute(new OW_Route('connect.buyitem', 'buy_item', 'MYRADIO_CTRL_Connect', 'buyitem'));
OW::getRouter()->addRoute(new OW_Route('connect.credit_res', 'credit_res', 'MYRADIO_CTRL_Connect', 'credit'));
//mymafia.su?credit_res=20&uid=16
$plugin = OW::getPluginManager()->getPlugin('myradio');

$eventHandler = new MYRADIO_CLASS_EventHandler();
$eventHandler->genericInit();
$eventHandler->init();

