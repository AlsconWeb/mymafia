<?php

/**
 * This software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is
 * licensed under The BSD license.

 * ---
 * Copyright (c) 2011, Oxwall Foundation
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice, this list of conditions and
 *  the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *  the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  - Neither the name of the Oxwall Foundation nor the names of its contributors may be used to endorse or promote products
 *  derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Facebook Connect Controller
 *
 * @author Sergey Kambalin <greyexpert@gmail.com>
 * @package ow_plugins.fbconnect.controllers
 * @since 1.0
 */

class MYRADIO_CTRL_Connect extends OW_ActionController
{
    /**
     *
     * @var FBCONNECT_BOL_Service
     */
    
    public function init()
    {
 
    }
     public function credit()
    {
      $balanceDao = MYRADIO_BOL_BalanceDao::getInstance();  
      $balance = $balanceDao->findByUserId($_GET['uid']);

      if (OW::getUser()->isAdmin())
      {
  if ( $balance )
        {
           $balance->balance = (int) $_GET['cr'];
        }
         else 
        {
            $balance = new MYRADIO_BOL_Balance();
            $balance->userId = $_GET['uid'];
            $balance->balance = (int) $_GET['cr'];
        }
        $balanceDao->save($balance);  
        
      }
      exit;
    }
     public function buyitem()
    {
$ItemsDao = MYRADIO_BOL_ItemsDao::getInstance();
$RadioDao = MYRADIO_BOL_RadioDao::getInstance();
$balanceDao = MYRADIO_BOL_BalanceDao::getInstance();

$userId = OW::getUser()->getId();

$balance = $balanceDao->findByUserId($userId);
$id=$_GET['item'];
$item=$ItemsDao->getItem($id);
$Radio=$RadioDao->getRadio($userId);
$tps_price=0;
 if ($Radio->level>0 && $Radio->time_create>0 && $item->type_item==1)
    {
     $item_last=$ItemsDao->getItem($Radio->level);
     $tps_price=round($item_last->price/30)*$Radio->time_create;
     if ($tps_price>$item_last->price)
     {
         $tps_price=$item_last->price;
     }
    }
if ($balance->balance-$item->price+$tps_price>=0)
{
   
    $src='https://i.pinimg.com/originals/9a/49/94/9a49943236155212b439ed87ab5dcab4.png';
    $trans=true;
    if ($item->type_item==1)
    {
    $lvl=$item->id;
    
    $ssd=5000;
    if ($lvl==1)
    {
        $ssd=15000;
    }
      if ($lvl==2)
    {
        $ssd=25000;
    }
      if ($lvl==3)
    {
        $ssd=50000;
    }
    $rad_space=file_get_contents("http://myradio.su/server?want=rad_space&rid=".$userId);
    if ($rad_space>$ssd+$Radio->ssd_add)
    {
    $res="Вам необходимо освободить место на диске перед переходом на этот тариф.";
    $trans=false;
    $src='http://engineerabroad.ru/wp-content/uploads/2017/08/thumbsdown-768x732.png';
    }
    else
    {
    $Radio->level=$lvl;
    $Radio->ssd_size=$ssd;
    $Radio->time_create=30;
    $RadioDao->save($Radio);
    $res="Вы купили тариф ".$item->iname." для вашего <a href='https://myradio.su'>радио</a>";
    }
    }
      if ($item->type_item==2 && $item->id==7)
    {
        file_get_contents("http://myradio.su/api?request=create_game_account&rid=".$userId);
        $res="Вы открыли вкладку игры, на <a href='https://myradio.su'>радио</a>"; 
    }
    if ($item->type_item==2 && $item->id==6)
    {
    $Radio->ssd_add+=1000;
    $RadioDao->save($Radio);
    
    $res="Вы увеличили место на 1 Gb, на вашем <a href='https://myradio.su'>радио</a>";   
    }
    if ($trans==true)
    {
    $balance->balance -= (int) $item->price-$tps_price;
    $balanceDao->save($balance);
    }
}
else
{
    $res="У вас нет кредитов на это. <a href='https://mymafia.su/buy'>Пополнить</a>";
    $src='http://engineerabroad.ru/wp-content/uploads/2017/08/thumbsdown-768x732.png';
}
$this->assign('src', $src);
$this->assign('res', $res);

    }
   
     public function shopsuccess()
    {
 // регистрационная инsформация (пароль #1)
// registration info (password #1)
$mrh_pass1 = "QpqGcmdFS22eye3d2HM3";

// чтение параметров
// read parameters
$out_summ = $_REQUEST["OutSum"];
$inv_id = $_REQUEST["InvId"];
$shp_item = $_REQUEST["Shp_item"];
$crc = $_REQUEST["SignatureValue"];

$crc = strtoupper($crc);

$my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item"));

// проверка корректности подписи
// check signature
if ($my_crc != $crc)
{
  echo "bad sign\n";
  exit();
}

// проверка наличия номера счета в истории операций
// check of number of the order info in history of operations
$balanceDao = MYRADIO_BOL_BalanceDao::getInstance();
$userId = OW::getUser()->getId();
$balance = $balanceDao->findByUserId($userId);

  if ( $balance )
        {
           $balance->balance += (int) $out_summ;
        }
         else 
        {
            $balance = new MYRADIO_BOL_Balance();
            $balance->userId = $userId;
            $balance->balance = (int) $out_summ;
        }
        $balanceDao->save($balance);
    }
     public function shopfail()
    {
        
    }
     public function buy_link()
    {
        $balance = MYRADIO_BOL_BalanceDao::getInstance();
        $link1=$balance->generate_robo_link($_GET['cr']);
        echo $link1;
        exit;
    }
     public function buy()
    {
        $balance = MYRADIO_BOL_BalanceDao::getInstance();
        $link1=$balance->generate_robo_link(10);
        $this->assign('link1', $link1);
        $link2=$balance->generate_robo_link(100);
        $this->assign('link2', $link2);
        $link3=$balance->generate_robo_link(500);
        $this->assign('link3', $link3);
        $link4=$balance->generate_robo_link(200);
        $this->assign('link4', $link4);
        $userId = OW::getUser()->getId();
        $balance = $balance->findByUserId($userId);
        $this->assign('balance', $balance->balance);
    }
    
        public function rate()
    {
        $UserDao = MYRADIO_BOL_GameaUserDao::getInstance();  
        $Users=$UserDao->getUsers();
       
        $this->assign('users', $Users);
        $this->assign('i', 1);
     // $balance = $balanceDao->findByUserId($_GET['uid']);
        return true;
    }
    public function shop()
    {
$ItemsDao = MYRADIO_BOL_ItemsDao::getInstance();
$RadioDao_b = MYRADIO_BOL_RadioDao::getInstance();
 $balanceDao = MYRADIO_BOL_BalanceDao::getInstance();
   $userId = OW::getUser()->getId();
        $items=$ItemsDao->getItems();
        $RadioDao=$RadioDao_b->getRadio($userId);
        
        $rad_items=[];
        $op_items=[];
        $tps_price=0;
        foreach ($items as $item)
        {
             if ($item->type_item==2)
            {
                if ($RadioDao || $item->id==7)
                {
            if ($item->id!=7 || ($RadioDao->game_status==0 && $RadioDao->level<3))
            {
               $item->style="cursor:pointer;";
               $item->click="window.location.href='https://mymafia.su/buy_item?item=".$item->id."'";
               $item->tps=0;
$op_items[]=$item;
}
}
            }
            if ($item->type_item==1)
            {
               $item->style="cursor:pointer;";
               $item->click="window.location.href='https://mymafia.su/buy_item?item=".$item->id."'";
               $item->tps=0;
               if ($RadioDao)
{
    

    if ($RadioDao->level==$item->id && $RadioDao->time_create>0)
    {
        $tps_price=round($item->price/30)*$RadioDao->time_create;
         if ($tps_price>$item->price)
     {
         $tps_price=$item->price;
     }
        $item->style="display:none;";
        $item->click='';
    }
    
}
$rad_items[]=$item; 

            }
        }
        if ($tps_price>0)
    {
        foreach ($rad_items as &$item)
        {
            if ($item->id!=$RadioDao->level)
            {
         $item->tps=$tps_price;   
            }
        }
    }
        if ( !$userId )
        {
           exit;
            
        }
        else
        {

if ($RadioDao)
{
    
}
else
{
    $item=new MYRADIO_BOL_Items();
    $rad_items=[];
    $item->style="cursor:pointer;";
    $item->type_item=1;
    $item->src="http://www.coral-missia.com/wp-content/uploads/icon_img_24.jpg";
    $item->iname="Создать радио";
    $item->des="У вас не создано радио. Получите бесплатный тариф на месяц.";
               $item->click="window.location.href='https://myradio.su/radio_create'";
               $item->tps=0;
               $item->price=0;
    $rad_items[]=$item;
}
$this->assign('radio', $RadioDao);
$this->assign('rad_items', $rad_items);
$this->assign('op_items', $op_items);
$balance = $balanceDao->findByUserId($userId);
$amount=0;
  if ( $balance )
        {
         //   $balance->balance += (int) $amount;
        }
        else 
        {
            $balance = new MYRADIO_BOL_Balance();
            $balance->userId = $userId;
            $balance->balance = (int) $amount;
            $balanceDao->save($balance);
             return 0;
        }
        
        
        
        }
        $this->assign('balance', $balance->balance);
        return $balance->balance;
    }
    public function user()
    {
    $userId=$_GET['user_id'];
    $displayName = BOL_UserService::getInstance()->getDisplayName($userId);
    $avatarUrl = BOL_AvatarService::getInstance()->getAvatarUrl($userId);
    $user['av']=$avatarUrl;
    $user['name']=$displayName;
    echo JSON_encode($user);
    exit;
    }
    public function login_ox()
    {
        $userId = OW::getUser();
        $_SESSION['ox_user_id']=$userId;
        
        if ($userId)
        {
            $id=OW::getUser()->getId();
        }
        else
        {
            $id=0;
        }
        echo 101;
        exit;
    }
    public function login( $params )
    {

        $authAdapter = new BASE_CLASS_StandardAuth($_GET['login'], $_GET['pass']);
        $result = OW::getUser()->authenticate($authAdapter);
        $result = OW::getUser();
        $backUri = empty($_GET['backUri']) ? '' : urldecode($_GET['backUri']);
        $backUrl = OW_URL_HOME . $backUri;
        //Register if not registered
        $this->redirect($backUrl);
    }

}