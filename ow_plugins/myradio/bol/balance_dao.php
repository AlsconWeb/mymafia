<?php

/**
 * Copyright (c) 2009, Skalfa LLC
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

/**
 * Data Access Object for `usercredits_balance` table.
 *
 * @author Egor Bulgakov <egor.bulgakov@gmail.com>
 * @package ow.plugin.user_credits.bol
 * @since 1.0
 */
class MYRADIO_BOL_BalanceDao extends OW_BaseDao
{
    /**
     * Singleton instance.
     *
     * @var USERCREDITS_BOL_BalanceDao
     */
    private static $classInstance;

    /**
     * Constructor.
     *
     */
    protected function __construct()
    {
        parent::__construct();
    }

    /**
     * Returns an instance of class.
     *
     * @return USERCREDITS_BOL_BalanceDao
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    /**
     * @see OW_BaseDao::getDtoClassName()
     *
     */
    public function getDtoClassName()
    {
        return 'MYRADIO_BOL_Balance';
    }

    /**
     * @see OW_BaseDao::getTableName()
     *
     */
    public function getTableName()
    {
        return OW_DB_PREFIX . 'usercredits_balance';
    }
    //формирование ссылки
    public function generate_robo_link($amount)
    {
        // 1.
// Оплата заданной суммы с выбором валюты на сайте мерчанта
// Payment of the set sum with a choice of currency on merchant site 

// регистрационная информация (логин, пароль #1)
// registration info (login, password #1)
$mrh_login = "MyMafia";
$mrh_pass1 = "QpqGcmdFS22eye3d2HM3";

// номер заказа
// number of order
$inv_id = time();

// описание заказа
// order description
$inv_desc = "Buy ".$amount." credits ID".OW::getUser()->getId();

// сумма заказа
// sum of order
$out_summ = $amount;

// тип товара
// code of goods
$shp_item = 1;

// предлагаемая валюта платежа
// default payment e-currency
$in_curr = "";

// язык
// language
$culture = "ru";

// кодировка
// encoding
$encoding = "utf-8";

// формирование подписи
// generate signature
$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item");

$res="<form action='https://merchant.roboxchange.com/Index.aspx' method=POST>".
      "<input type=hidden name=MrchLogin value=$mrh_login>".
      "<input type=hidden name=OutSum value=$out_summ>".
      "<input type=hidden name=InvId value=$inv_id>".
      "<input type=hidden name=Desc value='$inv_desc'>".
      "<input type=hidden name=SignatureValue value=$crc>".
      "<input type=hidden name=Shp_item value='$shp_item'>".
      "<input type=hidden name=IncCurrLabel value=$in_curr>".
      "<input type=hidden name=Culture value=$culture>".
      "<input type=submit value='Купить' class='buy_button'>".
      "</form>";
return $res;   
    }
    /**
     * Finds user balance
     * 
     * @param int $userId
     */
    public function findByUserId( $userId )
    {
    	$example = new OW_Example();
    	$example->andFieldEqual('userId', $userId);
    	
    	return $this->findObjectByExample($example);
    }
    
    public function getBalanceForUserList( $ids )
    {
        if ( !count($ids) )
        {
            return array();
        }

        $ids = array_unique($ids);

        $example = new OW_Example();
        $example->andFieldInArray('userId', $ids);

        return $this->findListByExample($example);
    }
}