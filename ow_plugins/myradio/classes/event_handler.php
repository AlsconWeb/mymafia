<?php

class MYRADIO_CLASS_EventHandler
{

public function genericInit()
{
}

public function init()
{
$eventManager = OW::getEventManager();
$eventManager->bind('console.collect_items', array($this, 'collectItems'));

}

public function collectItems( BASE_CLASS_ConsoleItemCollector $event )
{
  $balanceDao = MYRADIO_BOL_BalanceDao::getInstance();
  $UipDao = MYRADIO_BOL_UipDao::getInstance(); 
   
        $userId = OW::getUser()->getId();
        
        if ( !$userId )
        {
            return;
        }
        $Uip = $UipDao->getId($userId);
         if ($Uip)
        {
            $Uip->ip=$_SERVER['REMOTE_ADDR'];
            $Uip->time_r=time();
        }
        else 
        {
            $Uip = new MYRADIO_BOL_Uip();
            $Uip->uid = $userId;
            $Uip->ip = $_SERVER['REMOTE_ADDR'];
            $Uip->time_r=time();
        }
        $UipDao->save($Uip); 
        
        $balance = $balanceDao->findByUserId($userId);
        if ($balance)
        {
            $balance=$balance->balance;
        }
        else 
        {
            $balance = new MYRADIO_BOL_Balance();
            $balance->userId = $userId;
            $balance->balance = 0;
            $balanceDao->save($balance); 
            $balance=0;
            
        }
$ico="<div style=\"
background-repeat: no-repeat;
background: url(//mymafia.su/logo_files/moneta.png);
background-position: center;

padding:0;
background-repeat: no-repeat;
background-size:21px;
width: 21px;
height: 21px;
display: inline-block;
vertical-align: middle;
border:0;
font-size: 18px;
background-position: left;
color:#ccd0d4;
padding-left:23px;
\">(".$balance.")</div>"; 
$ico_sites="<div style=\"
background-repeat: no-repeat;
background: url(//mymafia.su/logo_files/earth_ico.png);
background-position: center;

padding:0;
background-repeat: no-repeat;
background-size:21px;
width: 21px;
height: 21px;
display: inline-block;
vertical-align: middle;
border:0;
font-size: 18px;
background-position: left;
color:#ccd0d4;
padding-left:10px;
\"></div>";
$ico_rate="<div style=\"
background-repeat: no-repeat;
background: url(//mymafia.su/logo_files/rate.png);
background-position: center;

padding:0;
background-repeat: no-repeat;
background-size:21px;
width: 21px;
height: 21px;
display: inline-block;
vertical-align: middle;
border:0;
font-size: 18px;
background-position: left;
color:#ccd0d4;
padding-left:10px;
\"></div>";
$language = OW::getLanguage();
$router = OW::getRouter();
$item = new BASE_CMP_ConsoleDropdownMenu($ico_sites);
$item->setUrl("https://myradio.su");
$item->addClass('ow_balance_ico');

$event->addItem($item, 6);

$item->addItem('head', array('label' => 'Радио', 'url' => "https://myradio.su"));
$item->addItem('head', array('label' => 'Видео', 'url' => "https://myvideos.su"));
$item->addItem('head', array('label' => 'Чаты', 'url' => "https://mychats.su"));

$item = new BASE_CMP_ConsoleDropdownMenu($ico_rate);
$item->setUrl($router->urlForRoute('connect.rate'));
$item->addClass('ow_balance_ico');

$event->addItem($item, 0);

$item->addItem('head', array('label' => 'Рейтинги игроков', 'url' => $router->urlForRoute('connect.rate')));

$item = new BASE_CMP_ConsoleDropdownMenu($ico);
$item->setUrl($router->urlForRoute('connect.buy'));
$item->addClass('ow_balance_ico');

$event->addItem($item, 0);

$item->addItem('head', array('label' => 'Пополнить баланс', 'url' => $router->urlForRoute('connect.buy')));
$item->addItem('main', array('label' => 'Магазин', 'url' => $router->urlForRoute('connect.shop')));

}
}