<?php

/**
 * Copyright (c) 2013 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */
/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package ow_plugins.oacvote
 * @since 1.5
 */
OW::getPluginManager()->addPluginSettingsRouteName("oacvote", "oacvote.admin");
OW::getLanguage()->importPluginLangs(OW::getPluginManager()->getPlugin("oacvote")->getRootDir() . "langs.zip", "oacvote");

$config = OW::getConfig();

$configsArray = array(
    "display_controls" => 0,
    "negative_action" => 1,
    "action_level" => 0,
    "opacity_level" => "0.4",
    "fade_time" => 500,
    "allow_positive_user_list" => 0,
    "allow_negative_user_list" => 0,
    "allow_common_user_list" => 1,
    "use_votes_for" => '["oacvote_comment","oacvote_forum_post"]'
);

foreach ( $configsArray as $key => $val )
{
    if ( !OW::getConfig()->configExists("oacvote", $key) )
    {
        OW::getConfig()->addConfig("oacvote", $key, $val);
    }
}