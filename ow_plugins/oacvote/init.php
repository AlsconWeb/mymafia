<?php

/**
 * Copyright (c) 2013 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */
/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package ow_plugins.oacvote
 * @since 1.5
 */
$classDir = OW::getPluginManager()->getPlugin("oacvote")->getClassesDir();

$classArray = array(
    "OA_CCLASS_Service" => $classDir . "oa_service.php",
    "OA_CCLASS_AdminLogo" => $classDir . "admin_logo.php"
);

foreach ( $classArray as $class => $path )
{
    try
    {
        OW::getAutoloader()->addClass($class, $path);
    }
    catch ( Exception $e )
    {
        
    }
}

OW::getRouter()->addRoute(new OW_Route('oacvote.admin', 'admin/oacvote', 'OACVOTE_CTRL_Admin', 'index'));

OACVOTE_BOL_Service::getInstance()->initVotes();

$eventHandler = new OACVOTE_CLASS_EventHandler();
$eventHandler->init();

