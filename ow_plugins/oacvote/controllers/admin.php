<?php

/**
 * Copyright (c) 2013 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package ow_plugins.oaboost.controllers
 * @since 1.0
 */
class OACVOTE_CTRL_Admin extends ADMIN_CTRL_Abstract
{
    /**
     * @var OACVOTE_BOL_Service
     */
    private $service;

    public function __construct()
    {
        parent::__construct();
        $this->service = OACVOTE_BOL_Service::getInstance();
        $this->assign("pluginBuild", OW::getPluginManager()->getPlugin($this->service->getPluginKey())->getDto()->getBuild());
        $this->assign("key", $this->service->getPluginKey());
    }

    public function index()
    {
        $this->setPageHeading($this->service->text('admin_index_heading'));
        $this->setPageHeadingIconClass('ow_ic_gear_wheel');
        OW::getNavigation()->activateMenuItem('admin_plugins', 'admin', 'sidebar_menu_plugins_installed');

        $form = new Form('config_form');

        $elements = array(
            OACVOTE_BOL_Service::CFG_DISPLAY_CTRL,
            OACVOTE_BOL_Service::CFG_NEGATIVE_USER_LIST,
            OACVOTE_BOL_Service::CFG_COMMMON_USER_LIST,
            OACVOTE_BOL_Service::CFG_POSITIVE_USER_LIST
        );

        foreach ( $elements as $el )
        {
            $formEl = new CheckboxField($el);
            $formEl->setLabel($this->service->text($el . "_label"));
            $formEl->setDescription($this->service->text($el . "_desc"));
            $formEl->setValue($this->service->getConfig($el));
            $form->addElement($formEl);
        }

        $negativeAction = new Selectbox(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION);
        $negativeAction->setLabel($this->service->text(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION . "_label"));
        $negativeAction->setDescription($this->service->text(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION . "_desc"));
        $negativeAction->setOptions(array($this->service->text(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION . "_val_0"), $this->service->text(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION . "_val_1"), $this->service->text(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION . "_val_2")));
        $negativeAction->setValue((int) $this->service->getConfig(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION));
        $negativeAction->setHasInvitation(false);
        $form->addElement($negativeAction);

        $actionLevel = new TextField(OACVOTE_BOL_Service::CFG_ACTION_LEVEL);
        $actionLevel->setLabel($this->service->text(OACVOTE_BOL_Service::CFG_ACTION_LEVEL . "_label"));
        $actionLevel->setDescription($this->service->text(OACVOTE_BOL_Service::CFG_ACTION_LEVEL . "_desc"));
        $actionLevel->setValue($this->service->getConfig(OACVOTE_BOL_Service::CFG_ACTION_LEVEL));
        $form->addElement($actionLevel);

        $voteFeatures = array(
            OACVOTE_BOL_Service::VAL_ENTITY_TYPE_COMMENT => $this->service->text("vote_feature_comment"),
            OACVOTE_BOL_Service::VAL_ENTITY_TYPE_POST => $this->service->text("vote_feature_forum"),
        );

        $voteFeatureCh = new CheckboxGroup(OACVOTE_BOL_Service::CFG_USE_VOTES_FOR);
        $voteFeatureCh->setOptions($voteFeatures);
        $voteFeatureCh->setLabel($this->service->text(OACVOTE_BOL_Service::CFG_USE_VOTES_FOR . "_label"));
        $voteFeatureCh->setDescription($this->service->text(OACVOTE_BOL_Service::CFG_USE_VOTES_FOR . "_desc"));
        $voteFeatureCh->setValue(json_decode($this->service->getConfig(OACVOTE_BOL_Service::CFG_USE_VOTES_FOR)));
        $form->addElement($voteFeatureCh);

        $submit = new Submit('submit');
        $submit->setValue(OW::getLanguage()->text('admin', 'save_btn_label'));
        $form->addElement($submit);

        $this->addForm($form);

        if ( OW::getRequest()->isPost() && $form->isValid($_POST) )
        {
            $elementsToSave = array_merge($elements, array(OACVOTE_BOL_Service::CFG_DISPLAY_CTRL, OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION, OACVOTE_BOL_Service::CFG_ACTION_LEVEL));

            $data = $form->getValues();

            foreach ( $elementsToSave as $el )
            {
                $this->service->saveConfig($el, (int) $data[$el]);
            }

            $votesFor = empty($data[OACVOTE_BOL_Service::CFG_USE_VOTES_FOR]) ? json_encode(array()) : json_encode($data[OACVOTE_BOL_Service::CFG_USE_VOTES_FOR]);
            $this->service->saveConfig(OACVOTE_BOL_Service::CFG_USE_VOTES_FOR, $votesFor);

            OW::getFeedback()->info($this->service->text('admin_settings_saved'));
            $this->redirect();
        }
    }
}
