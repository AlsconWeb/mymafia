<?php

/**
 * Copyright (c) 2013 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package ow_plugins.oacvote.controllers
 * @since 1.5
 */
class OACVOTE_CTRL_Voter extends OW_ActionController
{
    /**
     * @var BOL_VoteService
     */
    private $voteService;

    /**
     * Constructor.
     */
    public function __construct()
    {
        if ( !OW::getRequest()->isAjax() )
        {
            throw new Redirect404Exception();
        }

        $this->voteService = BOL_VoteService::getInstance();
    }

    public function vote()
    {
        if ( !OW::getUser()->isAuthenticated() )
        {
            exit(json_encode(array('result' => false, 'message' => OW::getLanguage()->text('oacvote', 'vote_login_msg'))));
        }

        if ( !isset($_POST['entityId']) || !isset($_POST['entityType']) || !isset($_POST['ownerId']) || !isset($_POST['total']) || !isset($_POST['userVote']) || !isset($_POST['uri']) )
        {
            exit(json_encode(array('result' => false, 'message' => 'Backend error!')));
        }

        $service = OACVOTE_BOL_Service::getInstance();

        $entityId = (int) $_POST['entityId'];
        $entityType = trim($_POST['entityType']);
        $ownerId = (int) $_POST['ownerId'];
        $total = (int) $_POST['total'];
        $userVote = (int) $_POST['userVote'];
        $uri = urldecode(trim($_POST['uri']));

        $voteDto = $this->voteService->findUserVote($entityId, $entityType, OW::getUser()->getId());

        if ( $userVote == 0 )
        {
            if ( $voteDto !== null )
            {
                //TODO delete notification
                $this->voteService->delete($voteDto);
            }
        }
        else
        {
            if ( $voteDto === null )
            {
                $voteDto = new BOL_Vote();
                $voteDto->setUserId(OW::getUser()->getId());
                $voteDto->setEntityType($entityType);
                $voteDto->setEntityId($entityId);
            }

            $voteDto->setVote($userVote);
            $voteDto->setTimeStamp(time());
            $this->voteService->saveVote($voteDto);

            switch ( $entityType )
            {
                case OACVOTE_BOL_Service::VAL_ENTITY_TYPE_POST:
                    $action = "oacvote-forum";
                    $langKey = $userVote > 0 ? "notification_post_up_string" : "notification_post_down_string";
                    break;

                case OACVOTE_BOL_Service::VAL_ENTITY_TYPE_COMMENT:
                    $action = "oacvote-comment";
                    $langKey = $userVote > 0 ? "notification_comment_up_string" : "notification_comment_down_string";
                    break;

                case OACVOTE_BOL_Service::VAL_ENTITY_TYPE_USER:
                    $action = "oacvote-user";
                    $langKey = $userVote > 0 ? "notification_user_up_string" : "notification_user_down_string";
                    break;
            }

            $avatars = BOL_AvatarService::getInstance()->getDataForUserAvatars(array($voteDto->getUserId()));
            
            $event = new OW_Event("notifications.add", array(
                "pluginKey" => 'oacvote',
                "entityType" => $entityType,
                "entityId" => $entityId,
                "action" => $action,
                "userId" => $ownerId,
                "time" => time()
                ), array(
                "avatar" => $avatars[$voteDto->getUserId()],
                "string" => array(
                    "key" => "oacvote+" . $langKey,
                    "vars" => array(
                        "displayName" => BOL_UserService::getInstance()->getDisplayName($voteDto->getUserId()),
                        "userUrl" => BOL_UserService::getInstance()->getUserUrl($voteDto->getUserId()),
                        "pageUrl" => OW_URL_HOME . $uri
                    )
                ),
                "url" => OW_URL_HOME . $uri
            ));

            OW::getEventManager()->trigger($event);
        }

        exit(json_encode(array("result" => true, "message" => "Success!")));
    }
}
