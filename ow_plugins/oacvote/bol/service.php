<?php

/**
 * This software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is
 * licensed under The BSD license.

 * ---
 * Copyright (c) 2011, Oxwall Foundation
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice, this list of conditions and
 *  the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *  the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  - Neither the name of the Oxwall Foundation nor the names of its contributors may be used to endorse or promote products
 *  derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package ow_plugins.oacvote.bol
 * @since 1.0
 */
final class OACVOTE_BOL_Service extends OA_CCLASS_Service
{
    const CFG_DISPLAY_CTRL = "display_controls";
    const CFG_NEGATIVE_ACTION = "negative_action";
    const CFG_ACTION_LEVEL = "action_level";
    const CFG_OPACITY_LEVEL = "opacity_level";
    const CFG_FADE_TIME = "fade_time";
    const CFG_POSITIVE_USER_LIST = "allow_positive_user_list";
    const CFG_NEGATIVE_USER_LIST = "allow_negative_user_list";
    const CFG_COMMMON_USER_LIST = "allow_common_user_list";
    const CFG_USE_VOTES_FOR = "use_votes_for";
    const VAL_NEGATIVE_ACTION_FADE = 1;
    const VAL_NEGATIVE_ACTION_HIDE = 2;
    const VAL_ENTITY_TYPE_COMMENT = "oacvote_comment";
    const VAL_ENTITY_TYPE_POST = "oacvote_forum_post";
    const VAL_ENTITY_TYPE_USER = "oacvote_user";

    /**
     * @var BOL_VoteService
     */
    private $voteService;

    /**
     * Constructor.
     */
    protected function __construct()
    {
        parent::__construct("oacvote");
        $this->voteService = BOL_VoteService::getInstance();
    }
    /**
     * Singleton instance.
     *
     * @var OACVOTE_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return OACVOTE_BOL_Service
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    public function initVotes()
    {
        $eventManager = OW::getEventManager();
    }
    private $votesInfo;
    private $voteLimit;
    private $fade;
    private $hide;
    private $opacity;
    private $clickMsg;
    private $userVoteInfo;
    private $entityType;

    public function prepareForumVotesInfo( OW_Event $event )
    {
        $params = $event->getParams();

        if ( empty($params["list"]) )
        {
            return;
        }

        $postIdList = array_keys($params["list"]);

        $eventManager = OW::getEventManager();

        $this->getVoteInfoForList($postIdList, self::VAL_ENTITY_TYPE_POST);
    }

    public function processForumPostBefore( BASE_CLASS_EventCollector $event )
    {
        $params = $event->getParams();

        if ( empty($params['postId']) || empty($params['userId']) )
        {
            return "";
        }

        $postId = (int) $params['postId'];
        $ownerId = (int) $params['userId'];

        $totalVotes = isset($this->votesInfo[$postId]) ? $this->votesInfo[$postId] : array();
        $myVote = ($this->userVoteInfo === null ? false : ( isset($this->userVoteInfo[$postId]) ? $this->userVoteInfo[$postId]->getVote() : 0 ));

        $fade = array_key_exists("sum", $totalVotes) && (int) $totalVotes["sum"] < $this->voteLimit && $this->fade;
        $hide = array_key_exists("sum", $totalVotes) && (int) $totalVotes["sum"] < $this->voteLimit && $this->hide;

        $newContent = ($hide ? '<div style="text-align:center;"><a href="javascript://" class="oachide_h">' . $this->clickMsg . '</a></div>' : '') . '
                    <div class="oac_cont"' . ( $fade ? ' style="opacity:' . $this->opacity . '"' : '') . ( $hide ? ' style="display:none;"' : '' ) . '>';

        $event->add($newContent);
    }

    public function processForumPostAfter( BASE_CLASS_EventCollector $event )
    {
        $params = $event->getParams();

        if ( empty($params['postId']) || empty($params['userId']) )
        {
            return "";
        }

        $postId = (int) $params['postId'];
        $ownerId = (int) $params['userId'];

        $totalVotes = isset($this->votesInfo[$postId]) ? $this->votesInfo[$postId] : array();
        $myVote = ($this->userVoteInfo === null ? false : ( isset($this->userVoteInfo[$postId]) ? $this->userVoteInfo[$postId]->getVote() : 0 ));

        $voteCmp = new OACVOTE_CMP_Voter($postId, $this->entityType, $totalVotes, $myVote, $ownerId, "ow_ipc");

        $event->add('</div>' . $voteCmp->render());
    }

    public function prepareCommentsVotesInfo( OW_Event $event )
    {
        $params = $event->getParams();

        if ( empty($params["list"]) )
        {
            return;
        }

        $commentsIdList = array();
        /* @var $item BOL_Comment */
        foreach ( $params["list"] as $item )
        {
            $commentsIdList[] = $item->getId();
        }

        $this->getVoteInfoForList($commentsIdList, self::VAL_ENTITY_TYPE_COMMENT);
    }

    public function processCommentItem( BASE_CLASS_EventProcessCommentItem $event )
    {
        $comment = $event->getItem();

        $totalVotes = isset($this->votesInfo[$comment->getId()]) ? $this->votesInfo[$comment->getId()] : array();
        $myVote = ($this->userVoteInfo === null ? false : ( isset($this->userVoteInfo[$comment->getId()]) ? $this->userVoteInfo[$comment->getId()]->getVote() : 0 ));

        $voteCmp = new OACVOTE_CMP_Voter($comment->getId(), $this->entityType, $totalVotes, $myVote, $comment->getUserId(), "ow_comments_item");
        $voteCmpMu = $voteCmp->render();
        $fade = array_key_exists("sum", $totalVotes) && (int) $totalVotes["sum"] < $this->voteLimit && $this->fade;
        $hide = array_key_exists("sum", $totalVotes) && (int) $totalVotes["sum"] < $this->voteLimit && $this->hide;

        $newContent = ($hide ? '<div style="text-align:center;"><a href="javascript://" class="oachide_h">' . $this->clickMsg . '</a></div>' : '') . '
                    <div class="oac_cont"' . ( $fade ? ' style="opacity:' . $this->opacity . '"' : '') . ( $hide ? ' style="display:none;"' : '' ) . '>' . $event->getDataProp('content') . '</div>';

        $event->setDataProp('content', $newContent);

        if ( !empty($voteCmpMu) )
        {
            $event->setDataProp('content_add', '</div><div>' . $voteCmpMu . '</div><div>' . $event->getDataProp('content_add'));
        }
    }

    private function getVoteInfoForList( $idList, $entityType )
    {
        $this->entityType = $entityType;

        $tempData = $this->getVotesForList($idList, $entityType);
        $this->votesInfo = array();

        if ( OW::getUser()->isAuthenticated() )
        {
            $this->userVoteInfo = array();
        }

        /* @var $item BOL_Vote */
        foreach ( $tempData as $item )
        {
            if ( !isset($this->votesInfo[$item->getEntityId()]) )
            {
                $this->votesInfo[$item->getEntityId()] = array('id' => $item->getEntityId(), 'sum' => 0, 'count' => 0, 'up' => 0, 'upUserId' => array(), 'down' => 0, 'downUserId' => array());
            }

            $this->votesInfo[$item->getEntityId()]['sum'] += $item->getVote();
            $this->votesInfo[$item->getEntityId()]['count'] ++;

            if ( $item->getVote() > 0 )
            {
                $this->votesInfo[$item->getEntityId()]['up'] ++;
                $this->votesInfo[$item->getEntityId()]['upUserId'][] = $item->getUserId();
            }
            else
            {
                $this->votesInfo[$item->getEntityId()]['down'] ++;
                $this->votesInfo[$item->getEntityId()]['downUserId'][] = $item->getUserId();
            }

            if ( OW::getUser()->isAuthenticated() && OW::getUser()->getId() == $item->getUserId() )
            {
                $this->userVoteInfo[$item->getEntityId()] = $item;
            }
        }

        $this->voteLimit = (int) $this->getConfig(OACVOTE_BOL_Service::CFG_ACTION_LEVEL);
        $this->fade = (int) $this->getConfig(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION) == OACVOTE_BOL_Service::VAL_NEGATIVE_ACTION_FADE;
        $this->hide = (int) $this->getConfig(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION) == OACVOTE_BOL_Service::VAL_NEGATIVE_ACTION_HIDE;
        $this->opacity = $this->getConfig(OACVOTE_BOL_Service::CFG_OPACITY_LEVEL);
        $this->clickMsg = $this->text('click_to_view');
    }

    /**
     * TODO need to move in vote DAO
     * 
     * @param array<int> $idList
     * @param string $entityType
     * @return array<BOL_Vote>
     */
    private function getVotesForList( $idList, $entityType )
    {
        $voteDao = BOL_VoteDao::getInstance();

        if ( method_exists($voteDao, "getEntityTypeVotes") )
        {
            return $voteDao->getEntityTypeVotes($idList, $entityType);
        }

        if ( empty($idList) || empty($entityType) )
        {
            return array();
        }

        $example = new OW_Example();
        $example->andFieldEqual(BOL_VoteDao::ENTITY_TYPE, $entityType);
        $example->andFieldInArray(BOL_VoteDao::ENTITY_ID, $idList);
        $example->andFieldEqual(BOL_VoteDao::ACTIVE, 1);
        return $voteDao->findListByExample($example);
    }
}
