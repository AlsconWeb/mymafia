<?php

/**
 * Copyright (c) 2011 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */
/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package oaseo
 * @since 1.0
 */
$config = Updater::getConfigService();

if ( !$config->configExists("oacvote", "allow_positive_user_list") )
{
    $config->addConfig("oacvote", "allow_positive_user_list", 0);
}

if ( !$config->configExists("oacvote", "allow_negative_user_list") )
{
    $config->addConfig("oacvote", "allow_negative_user_list", 0);
}

if ( !$config->configExists("oacvote", "allow_common_user_list") )
{
    $config->addConfig("oacvote", "allow_common_user_list", 1);
}
Updater::getLanguageService()->importPrefixFromZip(dirname(__FILE__) . DS . 'langs.zip', 'oacvote');


