<?php

/**
 * Copyright (c) 2011 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */
/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package oaseo
 * @since 1.0
 */
$config = Updater::getConfigService();

if ( !$config->configExists("oacvote", "display_controls") )
{
    $config->addConfig("oacvote", "display_controls", 0);
}

if ( !$config->configExists("oacvote", "negative_action") )
{
    $config->addConfig("oacvote", "negative_action", 1);
}

if ( !$config->configExists("oacvote", "action_level") )
{
    $config->addConfig("oacvote", "action_level", 0);
}

if ( !$config->configExists("oacvote", "opacity_level") )
{
    $config->addConfig("oacvote", "opacity_level", "0.4");
}

if ( !$config->configExists("oacvote", "fade_time") )
{
    $config->addConfig("oacvote", "fade_time", 500);
}

OW::getPluginManager()->addPluginSettingsRouteName("oacvote", "oacvote.admin");
Updater::getLanguageService()->importPrefixFromZip(dirname(__FILE__) . DS . 'langs.zip', 'oacvote');

