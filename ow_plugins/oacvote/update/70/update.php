<?php

/**
 * Copyright (c) 2011 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */
/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package oaseo
 * @since 1.0
 */
Updater::getDbo()->query("UPDATE `" . OW_DB_PREFIX . "base_vote` SET `entityType` = 'oacvote_comment' WHERE `entityType` LIKE 'oacvote_%'");
Updater::getDbo()->query("UPDATE `" . OW_DB_PREFIX . "base_language_prefix` SET `label` = '0xArt Votes' WHERE `prefix` = 'oacvote'");

if ( !Updater::getConfigService()->configExists("oacvote", "use_votes_for") )
{
    Updater::getConfigService()->addConfig("oacvote", "use_votes_for", '["oacvote_comment","oacvote_forum_post"]');
}
Updater::getLanguageService()->importPrefixFromZip(dirname(__FILE__) . DS . 'langs.zip', 'oacvote');


