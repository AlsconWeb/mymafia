<?php

/**
 * Copyright (c) 2013 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */
/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package ow_plugins.oacvote
 * @since 1.5
 */
// TODO refactor
OW::getDbo()->query("DELETE FROM `" . BOL_VoteDao::getInstance()->getTableName() . "` WHERE `" . BOL_VoteDao::ENTITY_TYPE . "` LIKE 'oacvote_%'");