<?php

/**
 * Copyright (c) 2013 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package ow_plugins.oacvote
 * @since 1.5
 */
class OACVOTE_CLASS_EventHandler
{
    /**
     *
     * @var OACVOTE_BOL_Service
     */
    private $service;

    /**
     * @var array
     */
    private $features;

    public function __construct()
    {
        $this->service = OACVOTE_BOL_Service::getInstance();        
        $this->features = json_decode($this->service->getConfig(OACVOTE_BOL_Service::CFG_USE_VOTES_FOR));
    }

    public function init()
    {
        $eventManager = OW::getEventManager();
        $includeStaticData = false;

        //forum
        if ( in_array(OACVOTE_BOL_Service::VAL_ENTITY_TYPE_POST, $this->features) )
        {
            if ( OW::getPluginManager()->isPluginActive("forum") && (int) OW::getPluginManager()->getPlugin("forum")->getDto()->getBuild() <= 7676 )
            {
                OW::getAutoloader()->addClass("FORUM_CTRL_Topic", OW::getPluginManager()->getPlugin("oacvote")->getClassesDir() . "temp" . DS . "topic.php");
            }

            $eventManager->bind("forum.topic_post_list", array($this->service, "prepareForumVotesInfo"));
            $eventManager->bind("forum.before_post_add_content", array($this->service, "processForumPostBefore"));
            $eventManager->bind("forum.after_post_add_content", array($this->service, "processForumPostAfter"));

            $includeStaticData = true;
        }

        //comments
        if ( in_array(OACVOTE_BOL_Service::VAL_ENTITY_TYPE_COMMENT, $this->features) )
        {
            if ( class_exists('BASE_MCMP_Comments') )
            {
                $eventManager->bind('base.comment_list_prepare_data', array($this->service, 'prepareCommentsVotesInfo'));
                $eventManager->bind('base.comment_item_process', array($this->service, 'processCommentItem'));
            }
            else
            {
                require_once OW::getPluginManager()->getPlugin('oacvote')->getCmpDir() . 'comments_list.php';
            }

            $includeStaticData = true;
        }

        if ( $includeStaticData )
        {
            $eventManager->bind(OW_EventManager::ON_BEFORE_DOCUMENT_RENDER, array($this, 'addVoterStaticData'));
        }

        $eventManager->bind('notifications.collect_actions', array($this, 'onCollectNotificationActions'));


//               OW::getEventManager()->call('notifications.remove', array(
//            'entityType' => 'event',
//            'entityId' => $eventId
//        ));
//        $eventManager->bind(OW_EventManager::ON_USER_UNREGISTER, array($this, 'onUserEventClearQueryCache'));
//        $eventManager->bind(OW_EventManager::ON_USER_SUSPEND, array($this, 'onUserEventClearQueryCache'));
//        $eventManager->bind(OW_EventManager::ON_USER_UNSUSPEND, array($this, 'onUserEventClearQueryCache'));
    }

    public function onCollectNotificationActions( BASE_CLASS_EventCollector $e )
    {
        $sectionLabel = $this->service->text("notification_section_label");

        if ( in_array(OACVOTE_BOL_Service::VAL_ENTITY_TYPE_COMMENT, $this->features) )
        {
            $e->add(array(
                "section" => 'oacvote',
                "action" => 'oacvote-comment',
                "description" => $this->service->text("comment_notifications_setting"),
                "selected" => true,
                "sectionLabel" => $sectionLabel,
                "sectionIcon" => 'ow_ic_write'
            ));
        }
        if ( in_array(OACVOTE_BOL_Service::VAL_ENTITY_TYPE_POST, $this->features) )
        {
            $e->add(array(
                "section" => 'oacvote',
                "action" => 'oacvote-forum',
                "description" => $this->service->text("forum_notifications_setting"),
                "selected" => true,
                "sectionLabel" => $sectionLabel,
                "sectionIcon" => 'ow_ic_write'
            ));
        }
    }

//    public function onCollectNotificationSendList( BASE_CLASS_EventCollector $event )
//    {
//        $params = $event->getParams();
//        $userIdList = $params["userIdList"];
//
//        $itemsToSend = $this->service->findActiveNotifications($userIdList);
//
//        if ( empty($itemsToSend) )
//        {
//            return array();
//        }
//
//        $voterIdList = array();
//
//        foreach ( $itemsToSend as $item )
//        {
//            $voterIdList[] = $item["voter"];
//        }
//
//        $avatarDataList = BOL_AvatarService::getInstance()->getDataForUserAvatars($voterIdList);
//        $displayNameList = BOL_UserService::getInstance()->getDisplayNamesForList($voterIdList);
//        $userUrlList = BOL_UserService::getInstance()->getUserUrlsForList($voterIdList);
//
//        foreach ( $itemsToSend as $item )
//        {
//            switch ( $item["type"] )
//            {
//                case OACVOTE_BOL_NotificationDao::VAL_TYPE_UP_COMMENT:
//                    $action = "oacvote-comment";
//                    $langKey = "notification_comment_up_string";
//                    break;
//
//                case OACVOTE_BOL_NotificationDao::VAL_TYPE_DOWN_COMMENT:
//                    $action = "oacvote-comment";
//                    $langKey = "notification_comment_down_string";
//                    break;
//
//                case OACVOTE_BOL_NotificationDao::VAL_TYPE_UP_POST:
//                    $action = "oacvote-forum";
//                    $langKey = "notification_post_up_string";
//                    break;
//
//                case OACVOTE_BOL_NotificationDao::VAL_TYPE_DOWN_POST:
//                    $action = "oacvote-forum";
//                    $langKey = "notification_post_down_string";
//                    break;
//
//                case OACVOTE_BOL_NotificationDao::VAL_TYPE_UP_USER:
//                    $action = "oacvote-user";
//                    $langKey = "notification_user_down_string";
//                    break;
//
//                case OACVOTE_BOL_NotificationDao::VAL_TYPE_DOWN_USER:
//                    $action = "oacvote-user";
//                    $langKey = "notification_user_down_string";
//                    break;
//            }
//
//            $event->add(array(
//                "pluginKey" => "oacvote",
//                "entityType" => "oacvote",
//                "entityId" => $item["nId"],
//                "userId" => $item["owner"],
//                "action" => $action,
//                "time" => $item["timeStamp"],
//                "data" => array(
//                    'avatar' => $avatarDataList[$item["voter"]],
//                    'string' => $this->service->text($langKey, array(
//                        "pageUrl" => OW_URL_HOME . $item["uri"],
//                        'displayName' => $displayNameList[$item["voter"]],
//                        'userUrl' => $userUrlList[$item["voter"]]
//                    )))
//            ));
//        }
//
//        $this->service->markAllNotificationsNotActive();
//    }

    public function addVoterStaticData()
    {
        OW::getDocument()->addScript($this->service->getPlugin()->getStaticJsUrl() . 'votes.js?' . OW::getConfig()->getValue('base', 'cachedEntitiesPostfix'));
        $styles = ".oac_vote_cont{font-size: 11px;} .oac_total_votes{font-weight:bold;margin-right:0;padding-left:5px;min-width:20px;text-align:center;} .oac_vote_down{-webkit-transform: rotate(180deg);-moz-transform: rotate(180deg);-o-transform: rotate(180deg);-ms-transform: rotate(180deg);-sand-transform: rotate(180deg);transform: rotate(180deg);}";
        OW::getDocument()->addStyleDeclaration($styles);
    }
}
