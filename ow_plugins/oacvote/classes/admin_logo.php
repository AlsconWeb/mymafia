<?php

/**
 * Copyright (c) 2011 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package oa.components
 * @since 1.0
 */
class OA_CCLASS_AdminLogo extends OW_Component
{

    /**
     * @return Constructor.
     */
    public function __construct( $params )
    {
        parent::__construct();
        
        if ( empty($params["key"]) || empty($params["build"]) )
        {
            $this->setVisible(false);
        }

        $this->setTemplate(OW::getPluginManager()->getPlugin($params['key'])->getCmpViewDir() . "admin_logo.html");

        $data = array(
            "softVersion" => str_replace(".", "", OW::getConfig()->getValue("base", "soft_version")),
            "softBuild" => OW::getConfig()->getValue('base', 'soft_build'),
            "key" => $params["key"],
            "build" => $params["build"],
            "host" => $_SERVER["HTTP_HOST"]
        );

        $this->assign("oaseoImageUrl", "http://oxart.net/" . base64_encode(json_encode($data)) . "/oa-post-it-note.jpg");
    }
}
