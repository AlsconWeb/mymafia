<?php

/**
 * Copyright (c) 2013 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package ow_plugins.oacvote.components
 * @since 1.5
 */
class OACVOTE_CMP_Voter extends OW_Component
{
    /**
     * @var array
     */
    private static $data;

    /**
     * @var OACVOTE_BOL_Service
     */
    private $service;

    /**
     * @var string
     */
    private $entityType;

    /**
     * @var int
     */
    private $entityId;

    /**
     * @var array
     */
    private $statData;

    /**
     * @var int 
     */
    private $myVote;

    /**
     * @var string
     */
    private $id;

    /**
     * @var int
     */
    private $ownerId;

    /**
     * @var string
     */
    private $parentClass;

    /**
     * Constructor.
     */
    public function __construct( $entityId, $entityType, $statData, $myVote, $ownerId, $parentClass )
    {
        parent::__construct();
        $this->service = OACVOTE_BOL_Service::getInstance();
        $this->entityId = $entityId;
        $this->entityType = $entityType;
        $this->statData = $statData;
        $this->myVote = $myVote;
        $this->id = uniqid('oavote');
        $this->ownerId = (int) $ownerId;
        $this->parentClass = trim($parentClass);

        if ( self::$data === null )
        {
            self::$data = array(
                "loginMessage" => $this->service->text("vote_login_msg"),
                "ownerMessage" => $this->service->text("vote_owner_msg_new"),
                "upListLabel" => $this->service->text('up_user_list'),
                "downListLabel" => $this->service->text('down_user_list'),
                "commonListLabel" => $this->service->text('common_user_list'),
                "respondUrl" => OW::getRouter()->urlFor("OACVOTE_CTRL_Voter", "vote"),
                "displayControls" => (bool) $this->service->getConfig(OACVOTE_BOL_Service::CFG_DISPLAY_CTRL),
                "actionFade" => (int) $this->service->getConfig(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION) === OACVOTE_BOL_Service::VAL_NEGATIVE_ACTION_FADE,
                "actionHide" => (int) $this->service->getConfig(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION) === OACVOTE_BOL_Service::VAL_NEGATIVE_ACTION_HIDE,
                "actionLevel" => (int) $this->service->getConfig(OACVOTE_BOL_Service::CFG_ACTION_LEVEL),
                "positiveList" => (bool) $this->service->getConfig(OACVOTE_BOL_Service::CFG_POSITIVE_USER_LIST),
                "negativeList" => (bool) $this->service->getConfig(OACVOTE_BOL_Service::CFG_NEGATIVE_USER_LIST),
                "commonList" => (bool) $this->service->getConfig(OACVOTE_BOL_Service::CFG_COMMMON_USER_LIST),
                "opacityLevel" => $this->service->getConfig(OACVOTE_BOL_Service::CFG_OPACITY_LEVEL),
                "fadeTime" => $this->service->getConfig(OACVOTE_BOL_Service::CFG_OPACITY_LEVEL),
                "currentUserId" => OW::getUser()->isAuthenticated() ? OW::getUser()->getId() : -1,
                "currentUri" => urlencode(OW::getRequest()->getRequestUri())
            );

            OW::getDocument()->addOnloadScript("window.OAVoteData = " . json_encode(self::$data) . ";");
        }
    }

    public function onBeforeRender()
    {
        parent::onBeforeRender();
        $commonUserList = array_merge((empty($this->statData['upUserId']) ? array() : $this->statData['upUserId']), (empty($this->statData['downUserId']) ? array() : $this->statData['downUserId']));

        $dataToAssign = array(
            "cmpId" => $this->id,
            "userVote" => $this->myVote,
            "entityId" => $this->entityId,
            "entityType" => $this->entityType,
            "ownerId" => $this->ownerId,
            "total" => empty($this->statData["sum"]) ? 0 : intval($this->statData["sum"]),
            "count" => empty($this->statData["count"]) ? 0 : intval($this->statData["count"]),
            "up" => empty($this->statData["up"]) ? 0 : intval($this->statData["up"]),
            "down" => empty($this->statData["down"]) ? 0 : intval($this->statData["down"]),
            "ownerBlock" => (OW::getUser()->isAuthenticated() && OW::getUser()->getId() == $this->ownerId),
            "upUserId" => ((bool) $this->service->getConfig("allow_positive_user_list") && !empty($this->statData["upUserId"])) ? $this->statData["upUserId"] : array(),
            "downUserId" => ((bool) $this->service->getConfig("allow_negative_user_list") && !empty($this->statData["downUserId"])) ? $this->statData["downUserId"] : array(),
            "commonUserId" => ((bool) $this->service->getConfig("allow_common_user_list") && !empty($commonUserList) ) ? $commonUserList : array(),
            "parentClass" => $this->parentClass
        );

        $this->assign("data", $dataToAssign);
        $this->assign("cfg", array("ctrl" => self::$data["displayControls"]));
        OW::getDocument()->addOnloadScript("new OAVote(" . json_encode($dataToAssign) . ");");
    }
}
