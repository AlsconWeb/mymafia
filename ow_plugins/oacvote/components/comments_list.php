<?php

/**
 * Copyright (c) 2013 Sardar Madumarov
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

/**
 * @author Sardar Madumarov <madumarov@gmail.com>
 * @package ow_plugins.oacvote.components
 * @since 1.5
 */
class BASE_CMP_CommentsList extends OW_Component
{

    /**
     * Constructor.
     *
     * @param string $entityType
     * @param integer $entityId
     * @param integer $page
     * @param string $displayType
     */
    public function __construct( BASE_CommentsParams $params, $id, $page = 1 )
    {
        parent::__construct();

        $language = OW::getLanguage();

        $batchData = $params->getBatchData();
        $staticData = empty($batchData['_static']) ? array() : $batchData['_static'];
        $batchData = isset($batchData[$params->getEntityType()][$params->getEntityId()]) ? $batchData[$params->getEntityType()][$params->getEntityId()] : array();

        $commentService = BOL_CommentService::getInstance();
        $avatarService = BOL_AvatarService::getInstance();

        $commentCount = isset($batchData['commentsCount']) ? $batchData['commentsCount'] : $commentService->findCommentCount($params->getEntityType(), $params->getEntityId());

        if ( $commentCount === 0 && $params->getDisplayType() !== BASE_CommentsParams::DISPLAY_TYPE_BOTTOM_FORM_WITH_PARTIAL_LIST_AND_MINI_IPC )
        {
            $this->assign('noComments', true);
        }

        $cmpContextId = "comments-list-$id";
        $this->assign('cmpContext', $cmpContextId);

        if ( $commentCount === 0 )
        {
            $commentList = array();
        }
        else if ( $params->getDisplayType() === BASE_CommentsParams::DISPLAY_TYPE_BOTTOM_FORM_WITH_FULL_LIST )
        {
            $commentList = $commentService->findFullCommentList($params->getEntityType(), $params->getEntityId());
        }
        else if ( in_array($params->getDisplayType(), array(BASE_CommentsParams::DISPLAY_TYPE_BOTTOM_FORM_WITH_PARTIAL_LIST, BASE_CommentsParams::DISPLAY_TYPE_BOTTOM_FORM_WITH_PARTIAL_LIST_AND_MINI_IPC)) )
        {
            $commentList = empty($batchData['commentsList']) ? $commentService->findCommentList($params->getEntityType(), $params->getEntityId(), 1, $params->getCommentCountOnPage()) : $batchData['commentsList'];
            $commentList = array_reverse($commentList);

            if ( $commentCount > $params->getCommentCountOnPage() )
            {
                $this->assign('viewAllLink', OW::getLanguage()->text('base', 'comment_view_all', array('count' => $commentCount)));
            }
        }
        else
        {
            $commentList = $commentService->findCommentList($params->getEntityType(), $params->getEntityId(), $page, $params->getCommentCountOnPage());
        }

        $arrayToAssign = array();
        $userIdList = array();
        $commentsIdList = array();

        /* @var $value BOL_Comment */
        foreach ( $commentList as $value )
        {
            $userIdList[] = $value->getUserId();
            $commentsIdList[] = $value->getId();
        }

        $userAvatarArrayList = empty($staticData['avatars']) ? $avatarService->getDataForUserAvatars($userIdList) : $staticData['avatars'];

        $isModerator = OW::getUser()->isAuthorized($params->getPluginKey());
        $isOwnerAuthorized = ( OW::getUser()->isAuthorized($params->getPluginKey(), 'delete_comment_by_content_owner', (int) $params->getOwnerId()) && (int) $params->getOwnerId() === (int) OW::getUser()->getId());
        $actionArray = array('comments' => array(), 'users' => array());
        $isBaseModerator = OW::getUser()->isAuthorized('base');

        /* <oacvote */
        $this->setTemplate(OW::getPluginManager()->getPlugin('oacvote')->getCmpViewDir() . 'comments_list.html');

        $voteService = BOL_VoteService::getInstance();
        $votesInfo = $voteService->findTotalVotesResultForList($commentsIdList, 'oacvote_' . $params->getEntityType());
        $oavoteService = OACVOTE_BOL_Service::getInstance();
        $oaVoteLimit = (int) $oavoteService->getConfig(OACVOTE_BOL_Service::CFG_ACTION_LEVEL);
        $oaFade = (int) $oavoteService->getConfig(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION) == OACVOTE_BOL_Service::VAL_NEGATIVE_ACTION_FADE;
        $oaHide = (int) $oavoteService->getConfig(OACVOTE_BOL_Service::CFG_NEGATIVE_ACTION) == OACVOTE_BOL_Service::VAL_NEGATIVE_ACTION_HIDE;
        $this->assign('oaopacity', $oavoteService->getConfig(OACVOTE_BOL_Service::CFG_OPACITY_LEVEL));
        $this->assign('oaclickMsg', $oavoteService->text('click_to_view'));
        
        $userVoteInfo = null;

        if ( OW::getUser()->isAuthenticated() )
        {
            $userVoteInfo = array();

            if ( $commentsIdList )
            {
                $userVoteInfo = $voteService->findUserVoteForList($commentsIdList, 'oacvote_' . $params->getEntityType(), OW::getUser()->getId());
            }
        }
        /* oacvote> */

        /* @var $value BOL_Comment */
        foreach ( $commentList as $value )
        {
            $deleteButton = false;
            $cAction = null;

            if ( $isOwnerAuthorized || $isModerator || (int) OW::getUser()->getId() === (int) $value->getUserId() )
            {
                $deleteButton = true;
            }

            if ( $isBaseModerator || $deleteButton )
            {
                $cAction = new BASE_CMP_ContextAction();
                $parentAction = new BASE_ContextAction();
                $parentAction->setKey('parent');
                $parentAction->setClass('ow_comments_context');
                $cAction->addAction($parentAction);

                if ( $deleteButton )
                {
                    $delAction = new BASE_ContextAction();
                    $delAction->setLabel($language->text('base', 'contex_action_comment_delete_label'));
                    $delAction->setKey('udel');
                    $delAction->setParentKey($parentAction->getKey());
                    $delId = 'del-' . $value->getId();
                    $delAction->setId($delId);
                    $actionArray['comments'][$delId] = $value->getId();
                    $cAction->addAction($delAction);
                }

                if ( $isBaseModerator && $value->getUserId() != OW::getUser()->getId() )
                {
                    $modAction = new BASE_ContextAction();
                    $modAction->setLabel($language->text('base', 'contex_action_user_delete_label'));
                    $modAction->setKey('cdel');
                    $modAction->setParentKey($parentAction->getKey());
                    $delId = 'udel-' . $value->getId();
                    $modAction->setId($delId);
                    $actionArray['users'][$delId] = $value->getUserId();
                    $cAction->addAction($modAction);
                }
            }

            $cmItemArray = array(
                'displayName' => $userAvatarArrayList[$value->getUserId()]['title'],
                'avatarUrl' => $userAvatarArrayList[$value->getUserId()]['src'],
                'profileUrl' => $userAvatarArrayList[$value->getUserId()]['url'],
                'content' => '<div class="ow_comments_content ow_smallmargin">' . $value->getMessage() . '</div>',
                'date' => UTIL_DateTime::formatDate($value->getCreateStamp()),
                'userId' => $value->getUserId(),
                'commentId' => $value->getId(),
                'avatar' => $userAvatarArrayList[$value->getUserId()],
                'cnxAction' => empty($cAction) ? '' : $cAction->render()
            );

            if ( $value->getAttachment() !== null )
            {
                $tempCmp = new BASE_CMP_OembedAttachment((array) json_decode($value->getAttachment()), $isOwnerAuthorized);

                $cmItemArray['content'] .= '<div class="ow_attachment ow_small" id="att' . $value->getId() . '">' . $tempCmp->render() . '</div>';
            }

            /* <oacvote */
            $totalVotes = isset($votesInfo[$value->getId()]) ? $votesInfo[$value->getId()] : array();
            
            $myVote = ($userVoteInfo === null ? null : ( isset($userVoteInfo[$value->getId()]) ? $userVoteInfo[$value->getId()] : 0 ));
            $voteCmp = new OACVOTE_CMP_Voter($value->getId(), 'oacvote_' . $params->getEntityType(), OW::getUser()->getId(), $totalVotes, $myVote, $value->getUserId());
            $cmItemArray['voteCmp'] = $voteCmp->render();
            $cmItemArray['fade'] = array_key_exists("sum", $totalVotes) && (int)$totalVotes["sum"] < $oaVoteLimit && $oaFade;
            $cmItemArray['hide'] = array_key_exists("sum", $totalVotes) && (int)$totalVotes["sum"] < $oaVoteLimit && $oaHide;
            /* oacvote> */
            $arrayToAssign[] = $cmItemArray;
        }

        $this->assign('comments', $arrayToAssign);

        $pages = false;

        if ( $params->getDisplayType() === BASE_CommentsParams::DISPLAY_TYPE_TOP_FORM_WITH_PAGING )
        {
            $pagesCount = $commentService->findCommentPageCount($params->getEntityType(), $params->getEntityId(), $params->getCommentCountOnPage());

            if ( $pagesCount > 1 )
            {
                $pages = $this->getPages($page, $pagesCount, 8);
                $this->assign('pages', $pages);
            }
        }
        else
        {
            $pagesCount = 0;
        }

        static $dataInit = false;

        if ( !$dataInit )
        {
            $staticDataArray = array(
                'respondUrl' => OW::getRouter()->urlFor('BASE_CTRL_Comments', 'getCommentList'),
                'delUrl' => OW::getRouter()->urlFor('BASE_CTRL_Comments', 'deleteComment'),
                'delAtchUrl' => OW::getRouter()->urlFor('BASE_CTRL_Comments', 'deleteCommentAtatchment'),
                'delConfirmMsg' => OW::getLanguage()->text('base', 'comment_delete_confirm_message'),
                'preloaderImgUrl' => OW::getThemeManager()->getCurrentTheme()->getStaticImagesUrl() . 'ajax_preloader_button.gif'
            );
            OW::getDocument()->addOnloadScript("window.owCommentListCmps.staticData=" . json_encode($staticDataArray) . ";");
            $dataInit = true;
        }

        $jsParams = json_encode(
            array(
                'totalCount' => $commentCount,
                'contextId' => $cmpContextId,
                'displayType' => $params->getDisplayType(),
                'entityType' => $params->getEntityType(),
                'entityId' => $params->getEntityId(),
                'pagesCount' => $pagesCount,
                'commentIds' => $commentsIdList,
                'pages' => $pages,
                'pluginKey' => $params->getPluginKey(),
                'ownerId' => $params->getOwnerId(),
                'commentCountOnPage' => $params->getCommentCountOnPage(),
                'cid' => $id,
                'actionArray' => $actionArray
            )
        );

        OW::getDocument()->addOnloadScript(
            "window.owCommentListCmps.items['$id'] = new OwCommentsList($jsParams);
            window.owCommentListCmps.items['$id'].init();"
        );
    }

    private function getPages( $currentPage, $pagesCount, $displayPagesCount )
    {
        $first = false;
        $last = false;

        $prev = ( $currentPage > 1 );
        $next = ( $currentPage < $pagesCount );

        if ( $pagesCount <= $displayPagesCount )
        {
            $start = 1;
            $displayPagesCount = $pagesCount;
        }
        else
        {
            $start = $currentPage - (int) floor($displayPagesCount / 2);

            if ( $start <= 1 )
            {
                $start = 1;
            }
            else
            {
                $first = true;
            }

            if ( ($start + $displayPagesCount - 1) < $pagesCount )
            {
                $last = true;
            }
            else
            {
                $start = $pagesCount - $displayPagesCount + 1;
            }
        }

        $pageArray = array();

        if ( $first )
        {
            $pageArray[] = array('label' => OW::getLanguage()->text('base', 'paging_label_first'), 'pageIndex' => 1);
        }

        if ( $prev )
        {
            $pageArray[] = array('label' => OW::getLanguage()->text('base', 'paging_label_prev'), 'pageIndex' => ($currentPage - 1));
        }

        if ( $first )
        {
            $pageArray[] = array('label' => '...');
        }

        for ( $i = (int) $start; $i <= ($start + $displayPagesCount - 1); $i++ )
        {
            $pageArray[] = array('label' => $i, 'pageIndex' => $i, 'active' => ( $i === (int) $currentPage ));
        }

        if ( $last )
        {
            $pageArray[] = array('label' => '...');
        }

        if ( $next )
        {
            $pageArray[] = array('label' => OW::getLanguage()->text('base', 'paging_label_next'), 'pageIndex' => ( $currentPage + 1 ));
        }

        if ( $last )
        {
            $pageArray[] = array('label' => OW::getLanguage()->text('base', 'paging_label_last'), 'pageIndex' => $pagesCount);
        }

        return $pageArray;
    }
}
