
 * This software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is a proprietary licensed product. 
 * For more information see License.txt in the plugin folder.

 * ---
 * Copyright (c) 2015, Ebenezer Obasi
 * All rights reserved.
 * info@eobai.com.

 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.

 * This plugin should be bought from the developer. For details contact info@eobasi.com.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Thank you for purchasing Facebook Clone application. We are looking forward to helping you build a professional landing page for your website with this plugin.

Facebook Clone plugin converts your join page [yoursite.com/join] and sign-in page [yousite.com/sign-in] to look like that of Facebook.com. Other than that, it also allow you to set your sign-in page as the default landing page of your website without turning off guest view.

The plugin is created to provide a very clean easy-to-use user interface for your visitors, providing responsive layout to fit in every screen size. 

The join page also has the login form place at the top of the site - the facebook way. This is to enable your website visitors sign-in or register to your website without leaving the landing page.

How To Set Your Landing Page
---------------------------------------

To setup your landing page:

 - Goto http://www.yoursite.com/admin/pages/manage
 - From the top menu section - drag and drop the join or sign-in menu item to the first place.
 - No other action is require, sign-out to confirm that your landing page is set.

Note: This plugin may not work if you have other landing page plugins such as Startpage Pro installed on your website. Disable other landing page plugins before usage.

Regards
Ebenezer Obasi