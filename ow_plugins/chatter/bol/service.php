<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * This plugin should be bought from the developer by paying money to PayPal account: biuro@grafnet.pl
 * Legal purchase is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

class CHATTER_BOL_Service
{
    /**
     * Singleton instance.
     *
     * @var CONTACTUS_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return CONTACTUS_BOL_Service
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    private function __construct()
    {

    }

    public function login($idlogin=0,$passwd="",$op="login")
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $ret=false;
        if ($op=="login"){

            $hash=md5($passwd);
//            if ($is_admin AND $idlogin>0){
//                $_SESSION['is_logged_'.$idlogin]=="1";
//                $ret=true;
//            }else 
            if ($idlogin>0 AND $passwd!=""){
                $sql = "SELECT * FROM " . OW_DB_PREFIX. "chatter_rooms  
                WHERE id='".addslashes($idlogin)."' AND paswd='".addslashes($hash)."' LIMIT 1";
                $arrl = OW::getDbo()->queryForList($sql);

                if (isset($arrl[0]) AND isset($arrl[0]['id']) AND $arrl[0]['id']>0){
                    $_SESSION['is_logged_'.$idlogin]="1";
                    $ret=true;
                }

            }
        }else if ($op=="logout"){
            if ($idlogin>0){
                $_SESSION['is_logged_'.$idlogin]="0";
                $ret=true;
            }
        }
        return $ret;
    }

    public function edit_room_form($room_id=0)
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $content="";

        if (!$id_user){
            OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
            OW::getApplication()->redirect($curent_url);
            exit;
        }

        if (!$is_admin AND !OW::getUser()->isAuthorized('chatter', 'roomscreate')){
            OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
            OW::getApplication()->redirect($curent_url);
            exit;
        }
//echo "SfsdGF";exit;
//---ssa s
        if (isset($_POST['subaction']) AND $_POST['subaction']=="save" AND isset($_POST['ss']) AND $_POST['ss']==substr(session_id(),2,5) AND isset($_POST['room']) ){
//            if (//is_active



            if ($_POST['room']>0 AND $_POST['room']!=$room_id) {
                OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                OW::getApplication()->redirect($curent_url);
                exit;
            }


            if ($_POST['room']>0) $id_room=$_POST['room'];
                else $id_room=0;

            if ($is_admin){
                if (isset($_POST['is_active']) AND $_POST['is_active']=="1") $is_active=1;
                    $is_active=0;
            }

            if (isset($_POST['title']) AND $_POST['title']) $title=$_POST['title'];
                else $title=OW::getLanguage()->text('chatter', 'no_title');

            if (isset($_POST['description']) AND $_POST['description']) $description=$_POST['description'];
                else $description="";


            if (isset($_POST['on_passwd']) AND $_POST['on_passwd']=="1" AND isset($_POST['f_passwd']) AND $_POST['f_passwd']!="") {
                $on_passwd=1;
                $paswd=md5($_POST['f_passwd']);

                $add_indert=", on_passwd='".addslashes($on_passwd)."', paswd='".addslashes($paswd)."' ";
            }else if ($_POST['on_passwd']!="1") {
                $on_passwd=0;
                $paswd="";

                $add_indert=", on_passwd='0', paswd='' ";
            }else{
                $on_passwd=0;
                $paswd="";
                $add_indert="";
            }



            if ($id_room>0){
                $add="";
                if (!$is_admin){
                    $add=" AND id_owner='".addslashes($id_user)."' ";
                }
                $sql="UPDATE " . OW_DB_PREFIX. "chatter_rooms SET 
            name='".addslashes($title)."',
            description='".addslashes($description)."'

                ".$add_indert."

                WHERE id='".addslashes($id_room)."' ".$add;
                OW::getDbo()->query($sql);
            }else{
                $curent_lang=$this->get_curect_lang_id();
                $sql="INSERT INTO " . OW_DB_PREFIX. "chatter_rooms (
                    id , active , id_owner     ,   id_language    , name   , description  ,   created,
                    on_passwd, paswd
                )VALUES(
                    '','1','".addslashes($id_user)."','".addslashes($curent_lang)."','".addslashes($title)."','".addslashes($description)."',NOW(),
                    '".addslashes($on_passwd)."','".addslashes($paswd)."'
                )";
                $id_room=OW::getDbo()->insert($sql);
            }

            if (isset($_POST['delete_cover']) AND $_POST['delete_cover']==$id_room AND $id_room>0){
                $pluginUserDir=OW::getPluginManager()->getPlugin('chatter')->getUserFilesDir();
                $this->file_delete($pluginUserDir.$id_room."_thumb.jpg");
            }

            if (isset($_FILES['coverfile']) AND $_FILES['coverfile']['tmp_name'] AND $_FILES['coverfile']['size']>0 AND $id_room>0){
                $pluginUserDir=OW::getPluginManager()->getPlugin('chatter')->getUserFilesDir();
                $this->file_copy($_FILES['coverfile']['tmp_name'], $pluginUserDir.$id_room."_thumb.jpg");
//                $this->image_resize($pluginUserDir.$id_room."_thumb.jpg",$crop=false,$width=800,$height=600)
                $this->image_resize($pluginUserDir.$id_room."_thumb.jpg",false,160,120);
            }



            OW::getFeedback()->info(OW::getLanguage()->text('chatter', 'saved_successfull'));
            if ($id_room>0){
                OW::getApplication()->redirect($curent_url."chatter/action/edit/".$id_room."/".substr(session_id(),2,5));
            }else{
                OW::getApplication()->redirect($curent_url."chatter");
            }
            exit;

        }
//---ssa e


        if ($room_id>0){

            $curent_lang=$this->get_curect_lang_id();
            $curent_lang_def=$this->get_system_lang_id();//default oxwall website language

/*
            $sql = "SELECT cr.*, crm.name as named FROM " . OW_DB_PREFIX. "chatter_rooms cr 
        LEFT JOIN " . OW_DB_PREFIX. "chatter_rooms crm ON (crm.id=cr.id AND crm.id_language='".addslashes($curent_lang_def)."') 
            WHERE cr.id='".addslashes($room_id)."' AND cr.id_language='".addslashes($curent_lang)."' LIMIT 1";
*/
            $sql = "SELECT cr.* FROM " . OW_DB_PREFIX. "chatter_rooms cr 
            WHERE cr.id='".addslashes($room_id)."' LIMIT 1";



//echo $sql;exit;
            $arrl = OW::getDbo()->queryForList($sql);
            $items="";
            if (isset($arrl[0]) AND isset($arrl[0]['id']) AND $arrl[0]['id']>0){
                $row=$arrl[0];
                if (!$is_admin AND $row['id_owner']!=$id_user){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }
            }else{
                if (!$is_admin){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }
                $row['id']=0;
                $row['name']="";
                $row['active']=1;
                $row['description']="";
//                $row['named']="";
                $row['on_passwd']=0;
                $row['paswd']="";
            }
        }else{
            $row['id']=0;
            $row['name']="";
            $row['active']=1;
            $row['description']="";
//            $row['named']="";
            $row['on_passwd']=0;
            $row['paswd']="";
        }

        $content .="<div class=\"ow_content\">";
        if ($row['id']>0){
            $content .="<form action=\"".$curent_url."chatter/action/edit/".$row['id']."/".substr(session_id(),2,5)."\" method=\"post\" enctype=\"multipart/form-data\">";
        }else{
            $content .="<form action=\"".$curent_url."chatter/action/newroom/create/".substr(session_id(),2,5)."\" method=\"post\" enctype=\"multipart/form-data\">";
        }
        $content .="<input type=\"hidden\" name=\"ss\" value=\"".substr(session_id(),2,5)."\">";
        $content .="<input type=\"hidden\" name=\"subaction\" value=\"save\">";

        if ($row['id']>0){
            $content .="<input type=\"hidden\" name=\"room\" value=\"".$row['id']."\">";
        }else{
            $content .="<input type=\"hidden\" name=\"room\" value=\"new\">";
        }

        $content .="<table class=\"ow_table1 ow_form\" style=\"width: 100%;margin:auto;\">";


    if ($is_admin){
        $content .="<tr>";
        $content .="<td class=\"ow_label\">";
        $content .=OW::getLanguage()->text('chatter', 'is_active');
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $tit=stripslashes($row['active']);

        $content .="<select name=\"is_active\">";
        if ($tit=="1" OR $tit=="") $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('chatter', 'yes')."</option>";
        if ($tit=="0" ) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('chatter', 'no')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";
    }

        $content .="<tr>";
        $content .="<td class=\"ow_label\">";
        $content .=OW::getLanguage()->text('chatter', 'room_name');
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $tit=stripslashes($row['name']);
//            if (!$tit) $tit=stripslashes($row['named']);
        $content .="<input type=\"text\" name=\"title\" value=\"".$tit."\" style=\"width:250px;\">";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td class=\"ow_label\">";
        $content .=OW::getLanguage()->text('chatter', 'room_description');
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $tit=stripslashes($row['description']);
//        $content .="<textarea name=\"description\">".$tit."</textarea>";
        $content .="<input type=\"text\" name=\"description\" value=\"".$tit."\" style=\"max-width:80%;\">";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr>";
        $content .="<td class=\"ow_label\">";
        $content .=OW::getLanguage()->text('chatter', 'room_on_passwd');
        $content .="</td>";
        $content .="<td class=\"ow_value\">";

        $tit=stripslashes($row['paswd']);
//        $content .="<textarea name=\"description\">".$tit."</textarea>";

        if ($row['on_passwd']!=1 OR !$tit) $sel=" checked ";
            else $sel="";
        $content .="<input ".$sel." type=\"radio\" name=\"on_passwd\" value=\"0\">&nbsp;".OW::getLanguage()->text('chatter', 'no_passwd');

        if ($row['on_passwd']==1 AND $tit!="") $sel=" checked ";
            else $sel="";

        $content .=", <input ".$sel." type=\"radio\" name=\"on_passwd\" value=\"1\">&nbsp;".OW::getLanguage()->text('chatter', 'set_passwd').":&nbsp;";
        if ($row['on_passwd']==1 AND $tit!=""){
            $content .="<input type=\"text\" name=\"f_passwd\" value=\"\" style=\"width:160px;\">&nbsp; <i class=\"ow_red\">".OW::getLanguage()->text('chatter', 'set_passwd_info')."</i>";
        }else{
            $content .="<input type=\"text\" name=\"f_passwd\" value=\"\" style=\"width:160px;\">";
        }

        $content .="</td>";
        $content .="</tr>";


        $content .="<tr>";
        $content .="<td class=\"ow_label\">";
        $content .=OW::getLanguage()->text('chatter', 'conver_image');
//        $content .="<br/>";
//        $content .="<i>".OW::getLanguage()->text('chatter', 'conver_image_info')."</i>";
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
//        $tit=stripslashes($row['description']);
//        $content .="<textarea name=\"description\">".$tit."</textarea>";
//        $content .="<input type=\"text\" name=\"description\" value=\"".$tit."\" style=\"max-width:80%;\">";

        $pluginStaticURL=OW::getPluginManager()->getPlugin('chatter')->getStaticUrl();
//        $pluginStaticD=OW::getPluginManager()->getPlugin('startpage')->getStaticDir();
        $pluginUserDir=OW::getPluginManager()->getPlugin('chatter')->getUserFilesDir();
        $pluginUserUrl=OW::getPluginManager()->getPlugin('chatter')->getUserFilesUrl();

        $del_img="";
        if ($row['id']>0){
            $filex=$pluginUserDir.$row['id']."_thumb.jpg";
            if ($this->file_exist($filex)){
                $del_img="<br/><input type=\"checkbox\" name=\"delete_cover\" value=\"".$row['id']."\">&nbsp;".OW::getLanguage()->text('chatter', 'delete_image');
                $img=$pluginUserUrl.$row['id']."_thumb.jpg";
            }else{
                $img=$pluginStaticURL."no_image.jpg";
            }
        }else{
            $img=$pluginStaticURL."no_image.jpg";
        }

        $content .="<div class=\"clearfix\" style=\"margin-top:10px;\">";
            $content .="<div class=\"profile-image\">
                <img src=\"".$img."\" width=\"160px\" height=\"120px\">
            </div>";
            $content .=$del_img;
        $content .="</div>";

        $content .="<div class=\"clearfix\" style=\"margin-top:10px;\">";
            $content .="<input type=\"file\" id=\"coverfile\" name=\"coverfile\">";
            $content .="<br/>";
            $content .="<b>".OW::getLanguage()->text('chatter', 'conver_image_size')."</b>";
        $content .="</div>";

        $content .="</td>";
        $content .="</tr>";


        $content .="<tr>";
        $content .="<td colspan=\"5\">";
//        $content .="<input type=\"submit\" name=\"dosave\" value=\"".OW::getLanguage()->text('chatter', 'admin_save')."\">";
        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('chatter', 'admin_save')."\" name=\"dosave\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
        $content .="</td>";
        $content .="</tr>";

        $content .="</table>";
        $content .="</form>";
        $content .="</div>";

        return $content;
    }

    private function mak_add($add="",$last_id=0,$id_room=0,$id_user=0, $is_faq=0, $is_admin=0,$type="chat",$plugin="")
    {
//        $add=" ppc.id>'".addslashes($last_id)."' ";
        if (!$add) $add=" ppc.id>'0' ";
        if ($is_admin){
            if ($is_faq=="info" AND $id_room>0){
                $add .=" AND ppc.is_fake='1' AND ppc.id_item='".addslashes($id_room)."' ";
            }else if ($id_room>0){
//                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."') ";
                $add .=" AND (ppc.id_item='".addslashes($id_room)."') ";
            }else{
                $add .=" AND ppc.id_item='0'  AND (ppc.is_fake='1' OR (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."') OR ppc.id_recipment='0') ";
            }
        }else if ($id_user>0 AND !$is_faq){
            if ($id_room>0){
                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."') ";
            }else{
                $add .=" AND ppc.id_item='0' AND (ppc.is_fake='0' AND (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' OR ppc.id_recipment='0' )) ";
            }
        }else{
            if ($id_room>0){
                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."') ";
            }else{
                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='0') ";
            }
        }
//echo $plugin."--".$id_room."--".$type;
        if ($is_admin AND !$plugin AND !$id_room){
            if ($type=="support" AND !$is_faq>0){
                $add .=" AND (ppc.id_item>'0' AND is_fake='1') ";
            }else{
                $add .=" AND ( (ppc.id_item='0' OR ppc.id_item='') AND is_fake='0') ";
            }
        }
//echo $plugin;
//echo $add;exit;
        return $add;
    }

    private function mak_sql($add="",$limit_list="",$type="chat")
    {
        $sql="";
        if ($add AND $limit_list){

//            if ($type=="support"){
//            }

            $sql = "SELECT ppc.*, pp.id as pp_id, pp.host, pp.host_post, pp.plugin, pp.items, pp.first_date, pp.last_date, pp.licenseKey, pp.pp_email, pp.is_readed_byadmin, pp.is_banned FROM " . OW_DB_PREFIX. "chatter_chat ppc 
            LEFT JOIN " . OW_DB_PREFIX. "chatter_plugins pp ON (ppc.id_item=pp.id) 
                WHERE ".$add." 

            ORDER BY ppc.post_date ASC,IF(ppc.is_fake = '1', 1 , 0  ), ppc.id_item ASC 
            LIMIT ".$limit_list;
//            ORDER BY IF(ppc.is_fake = '1', 1 , 0  ), ppc.id_item ASC,  ppc.post_date ASC 



//            ORDER BY IF(ppc.is_fake =  '1', 1, 0 ) , ppc.post_date ASC 



//            ORDER BY IF(ppc.is_fake =  '1', 1, 0 ) , ppc.id ASC , ppc.post_date ASC 

//            GROUP BY IF(ppc.is_fake=1, '', ppc.id_item) DESC  

//            ORDER BY ppc.post_date ASC 
//    GROUP BY id_item HAVING is_fake=1 
        }
        return $sql;
    }


    public function do_res_aj($plugin_get="")
    {

//----------ssss s
        header('Access-Control-Allow-Origin: *');

        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;


//SERVER_ADDR - c ip
//REMOTE_ADDR - from what ip

        $cversion="1.0";
        $plugin="";
        $plugin_key="";
        $host_post="";
        $msg="";
        $pp_email="";
        $host_post_ip="";

$host=$this->xgetHostName();
$ip=$this->xgetRealIpAddr();
//$ip=$this->xgetServerAddr();

    $last_id=0;
    $id_room=0;

    $is_faq="";
    $is_fake=0;


    $type="chat";

    if (isset($_POST['last_id']) AND $_POST['last_id']>0) $last_id=$_POST['last_id'];
    if (isset($_POST['room_id']) AND $_POST['room_id']>0) $id_room=$_POST['room_id'];
    if (isset($_POST['id_faq']) AND $_POST['id_faq']!="") $is_faq=$_POST['id_faq'];
    if (isset($_POST['type']) AND $_POST['type']!="") $type=$_POST['type'];
    if (isset($_POST['cplugin']) AND $_POST['cplugin']!="") $plugin=$_POST['cplugin'];


    $max_history_back=OW::getConfig()->getValue('chatter', 'max_history_back');
    if (!$max_history_back) $max_history_back=50;

//print_r($_POST);exit;

/*
Array
(
    [op] => svm
    [room_id] => 0
    [id_faq] => 
    [cplugin] => 
    [ss] => d34ac
    [last_id] => 204
    [ac] => pingm
    [type] => support
)
*/

/*
Array ( 
[op] => svm 
[room_id] => 1512 
[id_faq] => info 
[cplugin] => chatter 
[ss] => d9d66 [last_id] => 83 [ac] => savm [cont] => eeee )
*/
//echo $is_fake."==".$is_faq;
//exit;
    if ($is_faq=="info") $is_fake=1;
        else $is_fake=0;



    if (isset($_POST['cver']) AND $_POST['cver']!="") $cversion=$_POST['cver'];

if (isset($_POST['op']) AND isset($_POST['ac']) AND $_POST['ac']!="" AND $_POST['op']=="svmadm"){//----------------------------------------------------------------------check admin msg over page

            $sql = "SELECT COUNT(*) as allx FROM " . OW_DB_PREFIX. "chatter_plugins         
            WHERE is_readed_byadmin='0' AND is_banned='0' ";

//            WHERE is_readed_byadmin='1' ";

            $arrl = OW::getDbo()->queryForList($sql);
            if (isset($arrl[0]) AND isset($arrl[0]['allx']) AND $arrl[0]['allx']>0){
                $json_arr=array(
                    'status' => 'ok',
                    'status_text' => 'Found',
                    'quest' => '',
                    'type' => '',
                    'numberitems'  => $arrl[0]['allx'],
                );                
            }else{
            $json_arr=array(
                'status' => 'ok',
                'status_text' => 'No found',
                'quest' => '',
                'type' => '',
                'numberitems'  => '0',
            );
            }

    echo json_encode( $json_arr );
    exit;

}else if (isset($_POST['op']) AND isset($_POST['ac']) AND $_POST['ac']!="" AND $_POST['op']=="svm"){//----------------------------------------------------------------------sav msg over page
    
        
    $limit_list=OW::getConfig()->getValue('chatter', 'max_history_back');
    if (!$limit_list) $limit_list=500;
    if ($_POST['ac']=="savm" OR $_POST['ac']=="pingm"){

        if ($_POST['ac']=="savm"){
//------------save s
        
//----acces start
        if ($is_admin OR (!$is_fake AND !$id_room AND OW::getUser()->isAuthorized('chatter', 'publicwrite')) ) {
//            $can_write_publiuc=true;
        }else if (!$is_fake AND !$id_room){
                $json_arr=array(
                    'status' => 'error',
                    'status_text' => 'You dont have access!',
                    'quest' => $_POST['ac'],
                    'type' => 'line',
                    'content'  => '',
                );
                echo json_encode( $json_arr );
                exit;
//            $can_write_publiuc=false;
        }
        if ($is_admin OR (!$is_fake AND $id_room>0 AND OW::getUser()->isAuthorized('chatter', 'roomswrite')) ) {
//            $can_write_room=true;
        }else if (!$is_fake AND $id_room>0){
                $json_arr=array(
                    'status' => 'error',
                    'status_text' => 'You dont have access!',
                    'quest' => $_POST['ac'],
                    'type' => 'line',
                    'content'  => '',
                );
                echo json_encode( $json_arr );
                exit;
//            $can_write_room=false;
        }
//----acces end

        if (isset($_POST['cont']) AND strlen($_POST['cont'])>1){
            $sql="INSERT INTO " . OW_DB_PREFIX. "chatter_chat (
                id , id_item, 
                is_fake, id_sender    ,   id_recipment ,   post_date  ,     content
            )VALUES(
                '','".addslashes($id_room)."',
                '".addslashes($is_fake)."','".addslashes($id_user)."','0',NOW(),'".addslashes($this->html2txt($this->br2n($_POST['cont'])))."'
            )";
//print_r($_POST);
//echo $sql;exit;
            if ( OW::getDbo()->insert($sql)>0){
                $json_arr=array(
                    'status' => 'ok',
                    'status_text' => 'Snd message succesfull!',
                    'quest' => $_POST['ac'],
                    'type' => 'line',
                    'content'  => '',
                );
//                $this->mark_as($id_room,"unread");
            }else{
                $json_arr=array(
                    'status' => 'error',
                    'status_text' => 'You dont have access!',
                    'quest' => $_POST['ac'],
                    'type' => 'line',
                    'content'  => '',
                );
            }
        }else{
            $json_arr=array(
                'status' => 'error',
                'status_text' => 'Type message!',
                'quest' => $_POST['ac'],
                'type' => 'line',
                'content'  => '',
            );
        }
//---------save end
        }//if end



//--------------back msg

/*
        $add="";
        if ($is_admin){
            $add=" ppc.id>'".addslashes($last_id)."' AND (ppc.is_fake='1' OR (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' OR ppc.id_recipment='0')) ";
        }else if ($id_user>0){
            $add=" ppc.id>'".addslashes($last_id)."' AND ppc.is_fake='0' AND (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' OR ppc.id_recipment='0') ";
        }

        if ($is_admin){
            if ($is_faq=="info" AND $id_room>0){
                $add=" ppc.is_fake='1' AND ppc.id_item='".addslashes($id_room)."' ";
            }else if ($id_room>0){
                $add=" ppc.is_fake='0' AND ppc.id_recipment='".addslashes($id_room)."' ";
            }else{
                $add=" ppc.is_fake='1' OR (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' OR ppc.id_recipment='0') ";
            }
        }else if ($id_user>0 AND !$is_faq){
            if ($id_room>0){
                $add=" ppc.is_fake='0' AND ppc.id_recipment='".addslashes($id_room)."' ";
            }else{
                $add=" ppc.is_fake='0' AND (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' OR ppc.id_recipment='0' ) ";
            }
        }else{
            if ($id_room>0){
                $add=" ppc.is_fake='0' AND ppc.id_recipment='".addslashes($id_room)."' ";
            }else{
                $add=" ppc.is_fake='0' AND ppc.id_recipment='0' ";
            }
        }


*/



/*
        $add=" ppc.id>'".addslashes($last_id)."' ";
        if ($is_admin){
            if ($is_faq=="info" AND $id_room>0){
                $add .=" AND ppc.is_fake='1' AND ppc.id_item='".addslashes($id_room)."' ";
            }else if ($id_room>0){
//                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."') ";
                $add .=" AND (ppc.id_item='".addslashes($id_room)."') ";
            }else{
                $add .=" AND (ppc.is_fake='1' OR (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."')) ";
            }
        }else if ($id_user>0 AND !$is_faq){
            if ($id_room>0){
                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."') ";
            }else{
                $add .=" AND (ppc.is_fake='0' AND (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' )) ";
            }
        }else{
            if ($id_room>0){
                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."') ";
            }else{
                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='0') ";
            }
        }
*/
        $add=$this->mak_add(" ppc.id>'".addslashes($last_id)."' ",$last_id,$id_room,$id_user, $is_faq, $is_admin,$type,$plugin);
        $sql=$this->mak_sql($add,$limit_list,$type,$plugin);
//echo $sql;exit;
        if ($add!="" AND $sql!=""){
/*
            $sql = "SELECT ppc.*, pp.id as pp_id, pp.host, pp.host_post, pp.plugin, pp.items, pp.first_date, pp.last_date, pp.licenseKey, pp.pp_email FROM " . OW_DB_PREFIX. "chatter_chat ppc 
            LEFT JOIN " . OW_DB_PREFIX. "chatter_plugins pp ON (ppc.id_item=pp.id) 
                WHERE ".$add." 

            ORDER BY IF(ppc.is_fake =  '1', 1, 0 ) , ppc.id DESC , ppc.post_date ASC 
            LIMIT ".$limit_list;


//            GROUP BY IF(ppc.is_fake=1, '', ppc.id_item) DESC  

//            ORDER BY ppc.post_date ASC 
//    GROUP BY id_item HAVING is_fake=1 
*/
//            $sql=$this->mak_sql($add,$limit_list);

//echo $sql;exit;
            $arrl = OW::getDbo()->queryForList($sql);
            $items="";
            foreach ( $arrl as $row ){
                if (!$last_id OR $last_id<$row['id']) $last_id=$row['id'];
                $items .=$this->render_item($row,$id_room,$is_faq,$type);
            }

            if ($items!="" AND $id_room>0 AND $is_admin){
                $this->mark_as($id_room,"read");
            }

//            $items=$this->get_content($conn,$plugin_key, $ip,$plugin, $host_post,$limit_list,$last_id);
        }



        $json_arr=array(
            'status' => 'ok',
//            'status_text' => 'Update plugin!....LAST_ID='.$last_id,
            'status_text' => $items,
            'quest' => $_POST['ac'],
            'type' => 'line',
            'lastid' => $last_id,
            'content'  => '',
        );
    }else{
        $json_arr=array(
            'status' => 'error',
            'status_text' => 'You dont have access!',
            'quest' => $_POST['ac'],
            'type' => 'line',
            'content'  => '',
        );
    }

    echo json_encode( $json_arr );
    exit;
}else if (isset($_POST['op']) AND isset($_POST['ac']) AND $_POST['ac']!="" AND $_POST['op']=="sv"){//----------------------------------------------------------------------sav msg

    if (!$cversion>"1.0") {
        $json_arr=array(
            'status' => 'error',
            'status_text' => 'Update plugin!',
            'quest' => $_POST['ac'],
            'type' => 'line',
            'content'  => '',
        );
        echo json_encode( $json_arr );
        exit;
    }

    if ($_POST['ac']=="sav"){

        if (isset($_POST['cplugin']) AND $_POST['cplugin']!="") $plugin=$_POST['cplugin'];

        if (isset($_POST['ch']) AND $_POST['ch']!="") $host_post=base64_decode($_POST['ch']);
        if (isset($_POST['chi']) AND $_POST['chi']!="") $ip=base64_decode($_POST['chi']);
        if (isset($_POST['k']) AND $_POST['k']!="") $plugin_key=base64_decode($_POST['k']);

        if (isset($_POST['cont']) AND $_POST['cont']!="") $msg=base64_decode($_POST['cont']);
        if (isset($_POST['pp']) AND $_POST['pp']!="") $pp_email=base64_decode($_POST['pp']);


        $can_access=$this->check_access($conn,$plugin_key, $ip, $host_post, $plugin);

        
//        if ($plugin AND $msg!="" AND $host_post AND $plugin_key AND $can_access>0){




        if ($plugin AND $msg!="" AND $host_post AND $can_access>0){
            $sql="INSERT INTO " . OW_DB_PREFIX. "chatter_chat (
                id , id_item, 
                is_fake, id_sender    ,   id_recipment ,   
                post_date  ,     content
            )VALUES(
                '','".addslashes($can_access)."',
                '1','0','0',
                NOW(),'".addslashes($this->html2txt($this->br2n($_POST['cont'])))."'
            )";
//echo $sql;exit;
//            if ($conn->query($sql) === TRUE) {
            if ( OW::getDbo()->insert($sql)>0){

                $this->mark_as($can_access,"unread");
//exit;
//echo "sss".$sql;exit;
                $json_arr=array(
                    'status' => 'ok',
                    'status_text' => 'Snd message succesfull!',
                    'quest' => $_POST['ac'],
                    'type' => 'line',
                    'content'  => '',
                );

                if ($pp_email!=""){
                    $sql="UPDATE " . OW_DB_PREFIX. "chatter_plugins SET pp_email='".addslashes($pp_email)."' WHERE id='".addslashes($can_access)."' AND (pp_email='' OR pp_email IS NULL) LIMIT 1";
                    OW::getDbo()->query($sql);
                }

            }else{
                $json_arr=array(
                    'status' => 'error',
                    'status_text' => 'You dont have access!',
                    'quest' => $_POST['ac'],
                    'type' => 'line',
                    'content'  => '',
                );
            }
        }else{
            $json_arr=array(
                'status' => 'error',
                'status_text' => 'You dont have access!',
                'quest' => $_POST['ac'],
                'type' => 'line',
                'content'  => '',
            );
        }
    }else{
        $json_arr=array(
            'status' => 'error',
            'status_text' => 'You dont have access!',
            'quest' => $_POST['ac'],
            'type' => 'line',
            'content'  => '',
        );
    }
    
    echo json_encode( $json_arr );
    exit;

}else if (isset($_POST['op']) AND isset($_POST['ac']) AND $_POST['ac']!="" AND $_POST['op']=="ping"){//----------------------------------------------------------------PING

    if (!$cversion>"1.0") {
        $json_arr=array(
            'status' => 'error',
            'status_text' => 'Update plugin!',
            'quest' => $_POST['ac'],
            'type' => 'line',
            'content'  => '',
        );
        echo json_encode( $json_arr );
        exit;
    }


    if ($_POST['ac']=="checkacess"){


        if (isset($_POST['cplugin']) AND $_POST['cplugin']!="") $plugin=$_POST['cplugin'];

        if (isset($_POST['ch']) AND $_POST['ch']!="") $host_post=base64_decode($_POST['ch']);
        if (isset($_POST['chi']) AND $_POST['chi']!="") $ip=base64_decode($_POST['chi']);
        if (isset($_POST['k']) AND $_POST['k']!="") $plugin_key=base64_decode($_POST['k']);

        if (isset($_POST['cont']) AND $_POST['cont']!="") $msg=base64_decode($_POST['cont']);

        $ident_chat=$this->save_ident();


//echo $ip."--".$host_post."--".$this->check_access($conn,$plugin_key, $ip, $host_post, $plugin);exit;
//echo $plugin."--".$plugin_key."--".$this->check_access($conn,$plugin_key, $ip, $host_post, $plugin);exit;

//echo $host_post;exit;
//$plugin_key="", $ip="",$host_post="", $plugin=""
//        if ($plugin AND $plugin_key AND $this->check_access($conn,$plugin_key, $ip, $host_post, $plugin)){
        if ($plugin AND $this->check_access($conn,$plugin_key, $ip, $host_post, $plugin)){
//echo "aaa";exit;
//echo $host_post;exit;



            $cont=$this->get_content($conn,$plugin_key, $ip,$plugin, $host_post,$max_history_back,$last_id,$type);
            $json_arr=array(
                'status' => 'ok',
                'status_text' => 'Access succesfull!',
                'quest' => $_POST['ac'],
                'type' => 'html',
                'content'  => $cont,
            );
//        }else if ($plugin AND $host AND check_access($conn,"", $ip,$host, $plugin)){
        }else if ($plugin AND $this->check_access($conn,"", $ip,$host_post, $plugin)){
//echo "bbb";exit;
//echo $host_post;exit;
            $cont=$this->get_content($conn,0, $ip,$plugin, $host_post,$max_history_back,$last_id,$type);

            $json_arr=array(
                'status' => 'ok',
                'status_text' => 'Access succesfull!',
                'quest' => $_POST['ac'],
                'type' => 'html',
                'content'  => $cont,
            );

        }else{
            $json_arr=array(
                'status' => 'error',
                'status_text' => 'You dont have access!',
                'quest' => $_POST['ac'],
                'type' => 'line',
                'content'  => '',
            );
        }


//echo "X:".$ip."--".$host."--".$plugin."--".check_access($conn,"", $ip,$host, $plugin);exit;
//echo "X:".$ip."--".$host."--".$plugin."--".check_access($conn,"", $ip,$host_post, $plugin);exit;

    }else{
            $json_arr=array(
                'status' => 'error',
                'status_text' => 'You dont have access!',
                'quest' => $_POST['ac'],
                'type' => 'line',
                'content'  => '',
            );
    }
    echo json_encode( $json_arr );
    exit;
}else{//----------------------------------------------------------------dispaly cards





header("Content-type: application/xml; charset=\"utf-8\"");
echo "<"."?"."xml version=\"1.0\" encoding=\"UTF-8\""."?".">";
echo "<data corect=\"true\" chat=\"true\" order=\"rand\" version=\"2.0\">

<item>
<id>1202</id>
<type>plugin</type>
<plugin>preloader</plugin>
<title><![CDATA[Rainbow preloader - progress bar]]></title>
<img>http://www.oxwall.org/ow_userfiles/plugins/store/images/item_1202_9754_1446810889.jpg</img>
<url>http://www.oxwall.org/store/item/1202</url>
<date>x</date>
<body><![CDATA[Rainbow progress bar - show animation while page load. When page loading is display nice animation as progress indicator for your page. Plugin is hide in admin area. In plugin setting you can change to self balls colors. Rainbow animation based on css.]]></body>
<check>true</check>
<order>-1</order>
</item>

</data>
";
/*
<item>
<id>X</id>
<plugin>X</plugin>
<title><![CDATA[X]]></title>
<img>X</img>
<url>X</url>
<date>x</date>
<body><![CDATA[X]]></body>
<check>true</check>
</item>

*/


}//----------------------------------------------------------------dispaly cards END
//----------ssss e
    }


public function xmlsaf($s,$intoQuotes=1) {
    if ($intoQuotes){
        return str_replace(array('&','>','<','"'), array('&amp;','&gt;','&lt;','&quot;'), $s);
    }else{
        return str_replace(array('&','>','<'), array('&amp;','&gt;','&lt;'), $s);
    }
}


public function xgetHostName()
{
    return gethostbyaddr($_SERVER['REMOTE_ADDR']);
}

public function xgetServerAddr()
{
    $ip="";
    if (!empty($_SERVER['SERVER_ADDR']))   //check ip from share internet
    {
      $ip=$_SERVER['SERVER_ADDR'];
    }
    return $ip;
}

public function xgetRealIpAddr()
{
    $ip="";
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
          $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
          $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
}

private function mark_as($id_item=0,$status=""){
    $ret=false;

        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
//    if (!$is_admin) return $ret;

    if ($id_item>0 AND $status!=""){
        if ($status=="unread") {
            $sql="UPDATE " . OW_DB_PREFIX. "chatter_plugins SET 
            is_readed_byadmin='0' 
            WHERE id='".addslashes($id_item)."' AND is_readed_byadmin!='0' LIMIT 1";
            OW::getDbo()->query($sql);
            $ret=true;
        }else if ($status=="read") {
            $sql="UPDATE " . OW_DB_PREFIX. "chatter_plugins SET 
            is_readed_byadmin='1' 
            WHERE id='".addslashes($id_item)."' AND is_readed_byadmin!='1'  LIMIT 1";
            OW::getDbo()->query($sql);
            $ret=true;
        }
    }
    return $ret;
}

public function check_access($conn, $plugin_key="", $ip="",$host_post="", $plugin=""){
    $ret=false;

    if ($ip!="" AND $host_post!="" AND $plugin!=""){
        if ($plugin_key!=""){
//            $sql = "SELECT * FROM " . OW_DB_PREFIX. "chatter_plugins WHERE ip='".addslashes($ip)."' AND (host_post='".addslashes($host_post)."' OR host_post IS NULL) AND plugin='".addslashes($plugin)."' AND licenseKey='".addslashes($plugin_key)."' LIMIT 1"
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "chatter_plugins WHERE ip='".addslashes($ip)."' AND host_post='".addslashes($host_post)."' AND plugin='".addslashes($plugin)."' AND licenseKey='".addslashes($plugin_key)."' LIMIT 1";
        }else{
//            $sql = "SELECT * FROM " . OW_DB_PREFIX. "chatter_plugins WHERE ip='".addslashes($ip)."' AND (host_post='".addslashes($host_post)."' OR host_post IS NULL) AND plugin='".addslashes($plugin)."' LIMIT 1";
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "chatter_plugins WHERE ip='".addslashes($ip)."' AND host_post='".addslashes($host_post)."' AND plugin='".addslashes($plugin)."' LIMIT 1";
        }
//echo $sql;exit;
//echo $sql;
//        $result =$conn->query($sql);
        $arrl = OW::getDbo()->queryForList($sql);

//        if ($result->num_rows > 0){
        if (isset($arrl[0])) {
//            while($row = $result->fetch_assoc()) {
            foreach ( $arrl as $row ){
                if ($row['is_banned']!="1"){
                    $ret=$row['id'];
                }
            }
        }
    }
    return $ret;
}


private function save_ident(){
//--------------sss s
//file_put_contents("XAAAAAAAAAAAAAAAAAAAAAAAxxx.txt", "\n\n---START ---", FILE_APPEND);
//file_put_contents("XAAAAAAAAAAAAAAAAAAAAAAAxxx.txt", "\n\n---POST".print_r($_POST,1), FILE_APPEND);
    $ip=$this->xgetRealIpAddr();
    $host=$this->xgetHostName();

    $new_id=0;
    $plugin="";
    $host_post="";

    if (isset($_POST['cplugin']) AND $_POST['cplugin']!="") $plugin=$_POST['cplugin'];
    if (isset($_POST['ch']) AND $_POST['ch']!="") $host_post=base64_decode($_POST['ch']);
    if (isset($_POST['chi']) AND $_POST['chi']!="") $ip=base64_decode($_POST['chi']);

/*
    [op] => ping
    [ch] => aHR0cDovL3d3dy5veHdhbGwuYTYucGwv
    [chi] => OTUuMTU4Ljg1LjI0Mg==
    [cver] => 2
    [cplugin] => shoppro
    [k] =>.
    [ac] => checkacess
*/

//file_put_contents("XAAAAAAAAAAAAAAAAAAAAAAAxxx.txt", "\n\n---START ---", FILE_APPEND);
//file_put_contents("XAAAAAAAAAAAAAAAAAAAAAAAxxx.txt", "\n\n---POST".print_r($_POST,1), FILE_APPEND);
//file_put_contents("XAAAAAAAAAAAAAAAAAAAAAAAxxx.txt", "\n\n---host post".$host_post, FILE_APPEND);
//file_put_contents("XAAAAAAAAAAAAAAAAAAAAAAAxxx.txt", "\n\n---host".$host, FILE_APPEND);


    if (!$host) $host="NULL";
        else $host=" '".addslashes($host)."' ";

    if ($plugin!="" AND $host_post!=""){

        $sql = "SELECT id FROM " . OW_DB_PREFIX. "chatter_plugins WHERE ip='".addslashes($ip)."' AND host_post='".addslashes($host_post)."' AND plugin='".addslashes($plugin)."' LIMIT 1";
        $arrl = OW::getDbo()->queryForList($sql);
        if (isset($arrl[0])) {
            foreach ( $arrl as $row ){
                if (isset($row['id']) AND $row['id']>0){
                    $sql="UPDATE " . OW_DB_PREFIX. "chatter_plugins SET items=items+1 WHERE id='".addslashes($row['id'])."' LIMIT 1";
                    OW::getDbo()->query($sql);
                    $new_id=$row['id'];
                }
                break;
            }
        
        }else if ($plugin!="" AND $plugin!="NULL"){
            $sql = "INSERT INTO " . OW_DB_PREFIX. "chatter_plugins (
                id, ip, host,host_post, plugin, items, first_date, last_date,
                is_banned,       is_readed_byadmin
            )VALUES (
                '', '".addslashes($ip)."', ".$host.",'".addslashes($host_post)."','".addslashes($plugin)."','1', NOW(), NOW(),
                '0','1'
            ) ON DUPLICATE KEY UPDATE last_date=NOW(), items=items+1 ";
            $new_id=OW::getDbo()->insert($sql);
            if ($new_id>0){
            } else {
            }
        }
    }//if is plugin
    return $new_id;
//--------------sss e 
}

    public function get_new_count($last_check=""){
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $ret=0;
        if ($id_user>0){
//        if ($id_user>0 AND $last_check){
//            if (!$last_check) $last_check=date('Y-d-m :H:i:s');
            if (!$last_check) $last_check= date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." -5 minutes"));

            $last_check= date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))-(60*5));

            $sql = "SELECT COUNT(*) as allx FROM " . OW_DB_PREFIX. "chatter_chat 
            WHERE post_date>'".addslashes($last_check)."'  ";
//            WHERE post_date>'".addslashes($last_check)."' AND (id_sender='".addslashes($id_user)."' OR id_recipment='".addslashes($id_user)."')  ";
//echo $sql;exit;
//            WHERE post_dat>'".addslashes($last_check)."' AND ((id_sender='' OR id_recipment='') AND is_readed='0')  ";
            $arrl = OW::getDbo()->queryForList($sql);
            if (isset($arrl[0]) AND isset($arrl[0]['allx']) AND $arrl[0]['allx']>0){
                $ret=$arrl[0]['allx'];
            }else{
                $ret=0;
            }
        }

//echo $ret."--".$sql;exit;
//$ret=0;
        return $ret;
    }


public function get_content($conn, $id_item=0, $ip="",$plugin="", $host="",$max_posts=50,$last_id=0,$type="chat"){
    $ret="";
//$max_posts=50000;
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;

/*
        if ($is_admin){
            $add=" ppc.id>'".addslashes($last_id)."' AND (ppc.is_fake='1' OR (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."')) ";
        }else if ($id_user>0){
            $add=" ppc.id>'".addslashes($last_id)."' AND ppc.is_fake='0' AND (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."') ";
        }else{
            $add=" ppc.id>'".addslashes($last_id)."' ";
        }
*/

//        $add="";
        $add =" pp.ip='".addslashes($ip)."' AND pp.host_post='".addslashes($host)."' AND pp.plugin='".addslashes($plugin)."' ";
        if ($is_admin){
            $add .=" AND ppc.id>'".addslashes($last_id)."' AND (ppc.is_fake='1' OR (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' OR ppc.id_recipment='0')) ";
        }else if ($id_user>0){
            $add .=" AND ppc.id>'".addslashes($last_id)."' AND ppc.is_fake='0' AND (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' OR ppc.id_recipment='0') ";
        }else{
            $add .=" AND ppc.is_fake='1' ";
        }

        if ($is_admin AND !$plugin){
            if ($type=="support"){
                $add .=" AND (ppc.id_item>'0' AND is_fake='1') ";
            }else{
                $add .=" AND ((ppc.id_item='0' OR ppc.id_item='')  AND is_fake='1') ";
            }
        }


//            WHERE pp.ip='".addslashes($ip)."' AND pp.host_post='".addslashes($host)."' AND pp.plugin='".addslashes($plugin)."' 
//echo "ss";exit;

    if (!$max_posts) $max_posts=50;
//    if ($id_item>0 AND $plugin!="" AND $host!="" AND $ip!=""){
    if ($plugin!="" AND $host!="" AND $ip!=""){
/*
        $sql = "SELECT ppc.*, pp.id as pp_id, pp.host, pp.host_post, pp.plugin, pp.items, pp.first_date, pp.last_date, pp.licenseKey, pp.pp_email FROM " . OW_DB_PREFIX. "chatter_chat ppc 
        LEFT JOIN " . OW_DB_PREFIX. "chatter_plugins pp ON (ppc.id_item=pp.id) 
            WHERE ".$add."  
        ORDER BY ppc.post_date ASC 
        LIMIT ".$max_posts;
*/
            $sql = "SELECT ppc.*, pp.id as pp_id, pp.host, pp.host_post, pp.plugin, pp.items, pp.first_date, pp.last_date, pp.licenseKey, pp.pp_email FROM " . OW_DB_PREFIX. "chatter_chat ppc 
            LEFT JOIN " . OW_DB_PREFIX. "chatter_plugins pp ON (ppc.id_item=pp.id) 
                WHERE ".$add." 
            ORDER BY ppc.post_date ASC 
            LIMIT ".$max_posts;

//echo $sql;exit;

//            WHERE pp.ip='".addslashes($ip)."' AND (pp.host_post='".addslashes($host)."' OR pp.host_post IS NULL) AND pp.plugin='".addslashes($plugin)."' 

//            WHERE pp.ip='".addslashes($ip)."' AND host='".addslashes($host)."' AND plugin='".addslashes($plugin)."' AND licenseKey='".addslashes($plugin_key)."' LIMIT 1";

//        $result =$conn->query($sql);
        $arrl = OW::getDbo()->queryForList($sql);
//        if ($result->num_rows > 0) {
        if (isset($arrl[0])) {
//            while($row = $result->fetch_assoc()) {
            foreach ( $arrl as $row ){
                //echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
                $ret .="<div class=\"clearfix\">";

            $ret .="<span class=\"ow_tiny\">";
                if ($row['id_sender']){
                    $ret .="<span style=\"font-weight:bold;\">";
                    $ret .="<&nbsp;";
                    $ret .="[".$row['post_date']."]";
                    $ret .="</span>";
                }else{
                    $ret .="<span class=\"ow_green\" style=\"font-weight:bold;\">";
                    $ret .=">&nbsp;";
                    $ret .="[".$row['post_date']."]";
                    $ret .="</span>";
                }

            $ret .="</span>";

                $ret .="<div class=\" ow_border\" style=\"margin-left:12px;border-width:1px;\">";
                    $ret .="<div class=\"ow_smallmargin\">";
                        $ret .="<span class=\"ow_small\">";

                            if ($row['content']){
//                                $ret .="&nbsp;";
                                $ret .=$this->n2br($row['content']);
                            }else{
//                                $ret .="hi,<br/>How can I help you?\n\nThis is testing functionality and may not working ...yet.";
//                                $ret .="hi,\nHow is the problem with this plugin that describe it.\nI always try to answer, but I'm 24h online.\nThis is testing functionality and may not working ...yet.";
                                $ret .="Hi,<br/>* How is the problem with this plugin that describe it.<br/>* I always try to answer, but I'm 24h online.<br/>* This is testing functionality and may not working ...yet.";
                            }
                        $ret .="</span>";
                    $ret .="</div>";
                $ret .="</div>";

                $ret .="</div>";
            }//whil
        }//if
    }//if


    if (!$ret){
                $ret .="<div class=\"\" style=\"margin-left:12px;\">";
                    $ret .="<div class=\"ow_smallmargin\">";
                        $ret .="<span class=\"ow_small\" style=\"font-style: italic;\">";
//                            $ret .="hi,<br/>How can I help you?";
                            $ret .="Hi,<br/>* How is the problem with this plugin that describe it.<br/>* I always try to answer, but I'm 24h online.<br/>* This is testing functionality and may not working ...yet.";
                        $ret .="</span>";
                    $ret .="</div>";
                $ret .="</div>";

    }

    return $ret;
}

    public function n2br($document){
        $ret=$document;
        $ret= str_ireplace( "\r\n", "<br/>", $ret);
        $ret= str_ireplace("\n", "<br/>", $ret);
        return $ret;
    }











    public function chat ($id_room=0,$is_faq="",$type="chat")
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;


        $plugin="";
        $pagination="";
        


        $items="";
        $content ="";
$is_pagination=false;
//$is_pagination=true;
$is_ajax=true;

$last_id=0;



        if ($id_room>0){
            $query = "SELECT * FROM " . OW_DB_PREFIX. "chatter_rooms WHERE id='".addslashes($id_room)."' LIMIT 1";
            $arrx = OW::getDbo()->queryForList($query);
            if (isset($arrx[0]) AND isset($arrx[0]['id']) AND $arrx[0]['id']>0){
                $row=$arrx[0];

                if ($row['on_passwd']==1 AND $row['paswd']!=""){
                    $is_passwd=true;
                }else{
                    $is_passwd=false;
                }
//print_r($_SESSION);exit;
                if (isset($_SESSION['is_logged_'.$row['id']]) AND $_SESSION['is_logged_'.$row['id']]=="1"){
                    $is_logged=true;
                }else{
                    $is_logged=false;
                }

                if (!$is_admin AND $is_passwd==true AND $is_logged!=true){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url."chatter/login/".$id_room."/".substr(session_id(),2,5));
                    exit;
                }
            }
        }



if ($is_pagination){
//-------------------page start A
    $orderby="";
    $perpage_max=OW::getConfig()->getValue('chatter', 'mode_perpage');
    //OW::getConfig()->getValue('chatter', 'max_history_back');

    if (!$perpage_max OR $perpage_max==0) $perpage_max=5;
    if (isset($_GET['page'])){
        $curent_page=($_GET['page']-1);
        if (!$curent_page) $curent_page=0;
    }else{
        $curent_page=0;
    }

    $prev_page=$curent_page-1;
    if ($prev_page<0) $prev_page=0;
    $next_page=$curent_page+1;
    $add_limit=($curent_page*$perpage_max);
    if (!$add_limit) $add_limit=0;
    $pagination="";
    $add_where="";

        $query = "SELECT au.id as allp  FROM " . OW_DB_PREFIX. "chatter_chat au 
            WHERE 1";
//            WHERE id_seller='".addslashes($id_user)."' ";

//echo $query;exit;
    $arrx = OW::getDbo()->queryForList($query);
    if (isset($arrx[0])){
        $aaitem=$arrx[0];
        $all_items=count($arrx);
    }else{
        $all_items=0;
    }
    $limit_list=$add_limit.",".$perpage_max;
//echo $all_items;exit;
//-------------------page end A
}else{
    $limit_list=OW::getConfig()->getValue('chatter', 'max_history_back');
    if (!$limit_list) $limit_list=500;

}


/*
        $add =" pp.ip='".addslashes($ip)."' AND pp.host_post='".addslashes($host)."' AND pp.plugin='".addslashes($plugin)."' ";
        if ($is_admin){
            $add .=" AND ppc.id>'".addslashes($last_id)."' AND (ppc.is_fake='1' OR (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' OR ppc.id_recipment='0')) ";
        }else if ($id_user>0){
            $add .=" AND ppc.id>'".addslashes($last_id)."' AND ppc.is_fake='0' AND (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' OR ppc.id_recipment='0') ";
        }else{
            $add .="";
        }
*/

/*
        if ($is_admin){
            $add=" ppc.is_fake='1' OR (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."') ";
        }else if ($id_user>0){
            $add=" ppc.is_fake='0' AND (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."') ";
        }else{
            $add="1";
        }
*/

/*
        if ($is_admin){
            if ($is_faq=="info" AND $id_room>0){
                $add=" ppc.is_fake='1' AND ppc.id_item='".addslashes($id_room)."' ";
            }else if ($id_room>0){
                $add=" ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."' ";
            }else{
                $add=" ppc.is_fake='1' OR (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' ) ";
            }
        }else if ($id_user>0 AND !$is_faq){
            if ($id_room>0){
                $add=" ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."' ";
            }else{
                $add=" ppc.is_fake='0' AND (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."'  ) ";
            }
        }else{
            if ($id_room>0){
                $add=" ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."' ";
            }else{
                $add=" ppc.is_fake='0' AND ppc.id_item='0' ";
            }
        }
*/
/*
        $add=" ppc.id>'".addslashes($last_id)."' ";
        if ($is_admin){
            if ($is_faq=="info" AND $id_room>0){
                $add .=" AND ppc.is_fake='1' AND ppc.id_item='".addslashes($id_room)."' ";
            }else if ($id_room>0){
                $add .=" AND (ppc.id_item='".addslashes($id_room)."') ";
            }else{
                $add .=" AND (ppc.is_fake='1' OR (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."')) ";
            }
        }else if ($id_user>0 AND !$is_faq){
            if ($id_room>0){
                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."') ";
            }else{
                $add .=" AND (ppc.is_fake='0' AND (ppc.id_sender='".addslashes($id_user)."' OR ppc.id_recipment='".addslashes($id_user)."' )) ";
            }
        }else{
            if ($id_room>0){
                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='".addslashes($id_room)."') ";
            }else{
                $add .=" AND (ppc.is_fake='0' AND ppc.id_item='0') ";
            }
        }
*/

        $add=$this->mak_add("",$last_id,$id_room,$id_user, $is_faq, $is_admin,$type,$plugin);
        $sql=$this->mak_sql($add, $limit_list,$type,$plugin);
//echo $sql;exit;
        if ($add!="" AND $sql!=""){

            if ($id_room>0){
                $this->mark_as($id_room,"read");
            }

/*
            $sql = "SELECT ppc.*, pp.id as pp_id, pp.host, pp.host_post, pp.plugin, pp.items, pp.first_date, pp.last_date, pp.licenseKey, pp.pp_email, cr.name as room_name FROM " . OW_DB_PREFIX. "chatter_chat ppc 
            LEFT JOIN " . OW_DB_PREFIX. "chatter_plugins pp ON (ppc.id_item=pp.id) 
        LEFT JOIN " . OW_DB_PREFIX. "chatter_rooms cr ON (cr.id=ppc.id_recipment AND ppc.is_fake='0') 
                WHERE ".$add." 

            ORDER BY IF(ppc.is_fake =  '1', 1, 0 ) , ppc.id DESC , ppc.post_date ASC 

            LIMIT ".$limit_list;
//            GROUP BY IF(ppc.is_fake=1, '', ppc.id_item) DESC  
*/
/*
            $sql = "SELECT ppc.*, pp.id as pp_id, pp.host, pp.host_post, pp.plugin, pp.items, pp.first_date, pp.last_date, pp.licenseKey, pp.pp_email FROM " . OW_DB_PREFIX. "chatter_chat ppc 
            LEFT JOIN " . OW_DB_PREFIX. "chatter_plugins pp ON (ppc.id_item=pp.id) 

                WHERE ".$add." 

            ORDER BY IF(ppc.is_fake =  '1', 1, 0 ) , ppc.id DESC , ppc.post_date ASC 
            LIMIT ".$limit_list;


//            GROUP BY IF(ppc.is_fake=1, '', ppc.id_item) DESC  

//            ORDER BY ppc.post_date ASC 
//    GROUP BY id_item HAVING is_fake=1 
*/
//            $sql=$this->mak_sql($add, $limit_list);

//echo $sql;exit;



            $arrl = OW::getDbo()->queryForList($sql);

            foreach ( $arrl as $row ){
                if (!$last_id OR $last_id<$row['id']) $last_id=$row['id'];
                $items .=$this->render_item($row,$id_room,$is_faq,$type);
            }
        }
        if (!$items){
                $items ="<div ng-repeat=\"message in messages\" class=\"item in nofound_message\" style=\"opacity:1;\">
                    <div class=\"text\" style=\"height:100%;\">
                       <div ng-bind-html=\"message.message | rawHtml\" class=\"ng-binding\">".OW::getLanguage()->text('chatter', 'no_found')."</div>
                    </div>
                </div>";
        }

        $content .="<div class=\"clearfix\">";
            $content .="<div class=\"messages messages-img\" id=\"hiddenWhenLoadding\" style=\"display: block;\">
                <div class=\"chat_bodyCtr\" id=\"chat_bodyCtr\" >
                ".$items."
                </div>
            </div>";
        $content .="</div>";




        if ($is_pagination){
            $url_pages="";
            $pagination=$this->makePagination(($curent_page+1), $all_items, $perpage_max, 1, $curent_url."chatter".$url_pages,"?page=","right",$orderby);
        }




        $content_d="";

    if ($type!="support" AND ($is_admin OR ($id_room>0 AND OW::getUser()->isAuthorized('chatter', 'roomswrite')) OR (!$id_room AND OW::getUser()->isAuthorized('chatter', 'publicwrite'))  )){
        $content_d .="<div class=\"clearfix\">";
        $content_d .="<form action=\"".$curent_url."chatter/sendmsg/".substr(session_id(),2,5)."\" method=\"post\">";

        $content_d .="<input type=\"hidden\" id=\"last_id\" name=\"last_id\" value=\"".$last_id."\">";

        $content_d .="<input type=\"hidden\" name=\"save\" value=\"besave\">";
        $content_d .="<input type=\"hidden\" name=\"ss\" value=\"".substr(session_id(),2,5)."\">";

        $content_d .="<table style=\"width:100%;margin:auto;\" class=\"ow_table1 ow_form\">";

        $content_d .="<tr>";
        $content_d .="<td colspan=\"2\">";

        $content_d .="<div class=\"clearfix\">";
        if ($id_room>0){
            $content_d .="<div class=\"ow_left ow_green\">";
            $content_d .="<b><i>".OW::getLanguage()->text('chatter', 'private_message').":</i></b>";
            $content_d .="</div>";
        }else{
            $content_d .="<div class=\"ow_left \">";
            $content_d .="<b><i>".OW::getLanguage()->text('chatter', 'public_message').":</i></b>";
            $content_d .="</div>";
        }
        $content_d .="</div>";
//--------editor s
                $fname="msgchatter";
                $minheight=100;
                $content_edit="";

//                    if (isset($arrx[0]) AND isset($arrx[0]['description_de'])){
//                        $content_readed .=stripslashes($arrx[0]['description_de']);
//                        $content_edit=$content_readed;
//                    }else{
//                        $content_edit=$content_readed.$content;
//                    }



//                $content_t.="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\">".$description."</textarea>";
//                if ( OW::getPluginManager()->isPluginActive('wysiwygeditor') ){
//                    $content_d .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:350px;min-height:450px;\">".$content_edit."</textarea>";
//                }else{

//                if (!OW::getPluginManager()->isPluginActive('wysiwygeditor') AND !class_exists('WysiwygTextarea')){
//                    include_once(OW_DIR_CORE."form_element.php");
//                }


        if ($is_ajax){
            if ($id_room>0){
                $content_d .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:65px;min-height:65px;\" placeholder=\"".OW::getLanguage()->text('chatter', 'write_private_message')."\">".$content_edit."</textarea>";
            }else{
                $content_d .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:65px;min-height:65px;\" placeholder=\"".OW::getLanguage()->text('chatter', 'write_public_message')."\">".$content_edit."</textarea>";
            }
/*
            if (!$id_room AND !$is_faq){
                $content_d .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:65px;min-height:65px;\" placeholder=\"".OW::getLanguage()->text('chatter', 'write_public_message')."\">".$content_edit."</textarea>";
            }else{
                $content_d .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:65px;min-height:65px;\" placeholder=\"".OW::getLanguage()->text('chatter', 'write_message_content')."\">".$content_edit."</textarea>";
            }
*/
        }else if (class_exists('WysiwygTextarea')){
//            $body = new Textarea($fname);
            $body = new WysiwygTextarea($fname,array( BOL_TextFormatService::WS_BTN_IMAGE, BOL_TextFormatService::WS_BTN_VIDEO ), true);
            $body->setValue($content_edit);
            $body->addAttribute('id',$fname);
            $body->setHasInvitation(true);
//        $minheight="SIZE_S";
            $body->setSize($minheight);//SIZE_S,SIZE_M,SIZE_L 
            $content_d .=$body->renderInput();

        }else if (class_exists('CHATTER_CLASS_Wysiwygtextarea')){

        $body = new CHATTER_CLASS_Wysiwygtextarea($fname,array( BOL_TextFormatService::WS_BTN_IMAGE, BOL_TextFormatService::WS_BTN_VIDEO ), true);
        $body->setValue($content_edit);
        $body->addAttribute('id',$fname);
        $body->setHasInvitation(true);
        $body->setSize($minheight);//SIZE_S,SIZE_M,SIZE_L 
        $content_d .=$body->renderInput();


        }else{
            if ($id_room>0){
                $content_d .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:65px;min-height:65px;\" placeholder=\"".OW::getLanguage()->text('chatter', 'write_private_message')."\">".$content_edit."</textarea>";
            }else{
                $content_d .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:65px;min-height:65px;\" placeholder=\"".OW::getLanguage()->text('chatter', 'write_public_message')."\">".$content_edit."</textarea>";
            }
        }
//                }
//--------editor e
        $content_d .="</td>";
        $content_d .="</tr>";


        $content_d .="<tr>";
        $content_d .="<td colspan=\"2\">";
//        $content .="<input type=\"submit\" name=\"dosave\" value=\"".OW::getLanguage()->text('chatter', 'admin_save')."\">";
        $content_d .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" id=\"submsgp\" value=\"".OW::getLanguage()->text('chatter', 'send_message')."\" name=\"dosave\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
        $content_d .="</td>";
        $content_d .="</tr>";

        $content_d .="</table>";
        $content_d .="</form>";
        $content_d .="</div>";

        $content_d="<div class=\"\" style=\"margin-top:20px;\">".$content_d."</div>";
    }else{//if ($plugin!="support"){
        $content_d .="<form>";
        $content_d .="<input type=\"hidden\" id=\"last_id\" name=\"last_id\" value=\"".$last_id."\">";
        $content_d .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <a href=\"".$curent_url."sign-in?back-uri=chatter\" >
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">

                            <input type=\"button\" id=\"\" value=\"".OW::getLanguage()->text('chatter', 'login_for_write_message')."\" name=\"dosave\" class=\"ow_ic_warning ow_positive\">

                        </span>
                    </span>
                    </a>
                </div>
            </div>";
        $content_d .="</form>";



    }
//echo $plugin;exit;




/*
$cc ="
    $('#submsg').click(function(){       
        shoppro_savAjax($('#msg').val());
    });


</script>";
OW::getDocument()->appendBody($cc);

$script="
setInterval('shoppro_doAjax(ping_ac)',timex_pingx);
";
        OW::getDocument()->addOnloadScript($script);
    }
*/

$sUrl=$curent_url."chatter/".substr(session_id(),2,5);
$cplugin="";
$cplugin=$this->get_room_plugin($id_room);
//if (!$cplugin) $cplugin="support";
if (!$cplugin) $cplugin="";
//if (!$cplugin) $cplugin="";

//echo $cplugin;exit;

$js="<script>

var timex_pingx=5000;
var ping_waiting=0;

function chatter_play_s(nn){
    $('#chatter_'+nn).remove();
    $('body').append('<embed id=\"#chatter_'+nn+'\" src=\"".OW_URL_HOME."ow_static/plugins/chatter/m.mp3\" autostart=\"true\" hidden=\"true\" loop=\"false\">');
}

function chatter_doAjaxPing(op){

    if (ping_waiting==0){
        $.ajax({
            type: 'POST',
            data: 'op=svm&room_id=".$id_room."&id_faq=".$is_faq."&cplugin=".$cplugin."&ss=".substr(session_id(),2,5)."&last_id='+$('#last_id').val()+'&ac=pingm&type=".$type."',
            url: '".$sUrl."?op=svm',
            dataType: 'json',
            crossDomain : true,
            success : function (response) {

                if (response.quest!='ok' && response.status!=undefined){
                    if (response.lastid!=undefined && response.status_text!='' && $('#last_id').val(response.lastid)!=response.lastid){
                        $('#chat_bodyCtr').append(response.status_text);
                    }
                }

                if (response.status_text!='' && response.status_text!=undefined){
                    if (response.lastid!=undefined && response.status_text!='' && $('#last_id').val(response.lastid)!=response.lastid){
                        $('.nofound_message').hide();
";
if (!OW::getConfig()->getValue('chatter', 'disable_sound')){
$js.="
                        chatter_play_s('incoming');
";
}

$js.="
                    }
                }

//                var d = $('.chat_bodyCtr');
//                d.animate({ scrollTop: d.prop('scrollHeight') }, 1000);


                if (response.lastid!=undefined && response.lastid>0){
                    $('#last_id').val(response.lastid);
                }

            },
            error:function(jXHR, textStatus, errorThrown) { 
//                alert('Error while sending message: '+errorThrown+'...');
            },
            complete: function() {
//                $('#msgchatter').attr('value', '');
            },
            timeout: 5000,
            cache: false
        });
    }


}

</script>";
OW::getDocument()->appendBody($js);


//-------

$script="

    $('#submsgp').click(function(){

        ping_waiting=1;

        $.ajax({
            type: 'POST',
            data: 'op=svm&room_id=".$id_room."&id_faq=".$is_faq."&cplugin=".$cplugin."&ss=".substr(session_id(),2,5)."&last_id='+$('#last_id').val()+'&ac=savm&type=".$type."&cont='+$('#msgchatter').val(),
            url: '".$sUrl."?op=svm',
            dataType: 'json',
            crossDomain : true,
            success : function (response) {




                if (response.quest!='ok' && response.status!=undefined){
                    if (response.lastid!=undefined && response.status_text!=''  && $('#last_id').val(response.lastid)!=response.lastid){
                        $('#chat_bodyCtr').append(response.status_text);
                    }
                }

                if (response.status_text!='' && response.status_text!=undefined){
                    if (response.lastid!=undefined && response.status_text!=''  && $('#last_id').val(response.lastid)!=response.lastid){
                        $('.nofound_message').hide();
//                        chatter_play_s('incoming');
                    }
                }

                if (response.lastid!=undefined && response.status_text!=''  && $('#last_id').val(response.lastid)!=response.lastid){
                    var d = $('.chat_bodyCtr');
                    d.animate({ scrollTop: d.prop('scrollHeight') }, 1000);
                }


                if (response.lastid!=undefined && response.lastid>0){
                    $('#last_id').val(response.lastid);
                }
            },
            error:function(jXHR, textStatus, errorThrown) { 
                alert('Error while sending message: '+errorThrown+'...');
                ping_waiting=0;
            },
            complete: function() {
                $('#msgchatter').attr('value', '');
                ping_waiting=0;

            },
            timeout: 5000,
            cache: false
        });

        return false;
    });

    var d = $('#chat_bodyCtr');
    d.animate({ scrollTop: d.prop('scrollHeight')+500 }, 1000);


    setInterval('chatter_doAjaxPing()',timex_pingx);

";
OW::getDocument()->addOnloadScript($script);






        return $pagination.$content.$pagination.$content_d;
    }

    public function ping_check()
    {
//for tab
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $content=array();

//        if ($id_user>0 AND ){
        if ($is_admin){
            $sql = "SELECT COUNT(*) as allx FROM " . OW_DB_PREFIX. "chatter_plugins 
            WHERE is_readed_byadmin='0' AND is_banned='0' ";
            $arrl = OW::getDbo()->queryForList($sql);
            if (isset($arrl[0]) AND isset($arrl[0]['allx']) AND $arrl[0]['allx']>0){
                $content['items']=$arrl[0]['allx'];
                $content['last']=0;
            }else{
                $content['items']=0;
                $content['last']=0;
            }
        }else{
            $content['items']=0;
            $content['last']=0;
        }
        return $content;
    }

    public function allow_moderate_room($for_user=0,$id_room=0)
    {
//for tab
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $content=array();
        if ($is_admin) return true;
        if (!$for_user) return false;

        if ($for_user>0 AND $id_room>0){
            $sql = "SELECT id FROM " . OW_DB_PREFIX. "chatter_rooms 
            WHERE id='".addslashes($id_room)."' AND id_owner='".addslashes($for_user)."' LIMIT 1";
            $arrl = OW::getDbo()->queryForList($sql);
            if (isset($arrl[0]) AND isset($arrl[0]['id']) AND $arrl[0]['id']>0 AND $arrl[0]['id']==$id_room){
                 return true;
            }else{
                 return false;
            }
        }else{
             return false;
        }
        return $content;
    }



    public function pinok()
    {
//for tab - nie uzywany
return;
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $content="";

        if (!$is_admin) return;

    $sUrl=$curent_url."chatter/".substr(session_id(),2,5);

$js="<script>
var last_numberitems=0;

function chatter_play_s_admin(nn){
    $('#chatter_'+nn).remove();
    $('body').append('<embed id=\"#chatter_'+nn+'\" src=\"".OW_URL_HOME."ow_static/plugins/chatter/m.mp3\" autostart=\"true\" hidden=\"true\" loop=\"false\">');
}

function chatter_doAjaxPingAdmin(){
        $.ajax({
            type: 'POST',
            data: 'op=svmadm&room_id=".$id_room."&id_faq=".$is_faq."&cplugin=".$cplugin."&ss=".substr(session_id(),2,5)."&last_id='+$('#last_id').val()+'&ac=pingm&type=".$type."',
            url: '".$sUrl."?op=svmadm',
            dataType: 'json',
            crossDomain : true,
            success : function (response) {
                if (response.numberitems!=undefined && response.numberitems>0){
                    $('#last_id').val(response.lastid);

                    if (response.numberitems>last_numberitems){
                            chatter_play_s_admin('incoming_admn');
                    }
                    $('#support_cointer').html(response.numberitems);

//$('#chatter_below').parent().find('.OW_ConsoleItemCounterNumber').html(response.numberitems);
//$('#chatter_below').parent().css('display','inline-block');

                    $('#support_cointer').css('display','inline-block');
                }else{

//$('#chatter_below').parent().css('display','none');
//$('#chatter_below').parent().find('.OW_ConsoleItemCounterNumber').html('');

                    $('#support_cointer').css('display','none');
                }
                last_numberitems=response.numberitems;
            },
            error:function(jXHR, textStatus, errorThrown) { 
            },
            complete: function() {
            },
            timeout: 5000,
            cache: false
        });



}

</script>";
OW::getDocument()->appendBody($js);



$script="

    setInterval('chatter_doAjaxPingAdmin()',5000);
";
OW::getDocument()->addOnloadScript($script);


        
    }

    public function render_item( $row="",$id_room=0,$is_faq=false,$type="chat" )
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $content="";

        if (is_array($row)){

//            if (!$row['is_fake'] AND $row['id_sender']>0){
            if ($row['id_sender']>0){
                $dname=BOL_UserService::getInstance()->getDisplayName($row['id_sender']);
                $uurl=BOL_UserService::getInstance()->getUserUrl($row['id_sender']);
                $uimg=BOL_AvatarService::getInstance()->getAvatarUrl($row['id_sender']);
                if (!$uimg) {
                    $uimg=$curent_url."ow_static/themes/".OW::getConfig()->getValue('base', 'selectedTheme')."/images/no-avatar.png";
                }
            }else{
                $dname=$row['plugin'];
                $uurl="";
                $uimg=$curent_url."ow_static/themes/".OW::getConfig()->getValue('base', 'selectedTheme')."/images/no-avatar.png";
            }
            if (!isset($row['id_owner'])) $row['id_owner']="";
            if (!isset($row['plugin'])) $row['plugin']="";

            $timeStamp=strtotime($row['post_date']);
            $dd=UTIL_DateTime::formatDate((int) $timeStamp);

            $add_info="";

            if ($dname AND !$row['is_fake']){
                $is=false;
                if ($add_info) $is=true;
                    if ($is==true) $add_info .="<span class=\"ow_tiny\">&nbsp;| </span>";
                $add_info .="<span class=\"ow_tiny chatter_item\">";
//                if ($is==true) $add_info .="&nbsp;| ";
                $add_info .=OW::getLanguage()->text('chatter', 'user_name').":&nbsp;";
                $add_info .="<b>".$dname."</b>";
                $add_info .="</span>";
            }else if ($row['plugin']){
                $is=false;
                if ($add_info) $is=true;
                    if ($is==true) $add_info .="<span class=\"ow_tiny\">&nbsp;| </span>";
                $add_info .="<span class=\"ow_tiny chatter_item\">";
//                if ($is==true) $add_info .="&nbsp;| ";
                $add_info .=OW::getLanguage()->text('chatter', 'plugin_name').":&nbsp;";
                $add_info .="<b>".$row['plugin']."</b>";
                $add_info .="</span>";
            }

            if ($row['is_fake']){
                if ($row['host_post']){
                    $is=false;
                    if ($add_info) $is=true;
                    if ($is==true) $add_info .="<span class=\"ow_tiny\">&nbsp;| </span>";
                    $add_info .="<span class=\"ow_tiny chatter_item\">";
//                    if ($is==true) $add_info .="&nbsp;| ";
                    $add_info .=OW::getLanguage()->text('chatter', 'host_post').":&nbsp;";
                    $add_info .="<b>".$row['host_post']."</b>";
                    $add_info .="</span>";
                }
                if ($row['host']){
                    $is=false;
                    if ($add_info) $is=true;
                    if ($is==true) $add_info .="<span class=\"ow_tiny\">&nbsp;| </span>";
                    $add_info .="<span class=\"ow_tiny chatter_item\">";
//                    if ($is==true) $add_info .="&nbsp;| ";
                    $add_info .=OW::getLanguage()->text('chatter', 'host').":&nbsp;";
                    $add_info .="<b>".$row['host']."</b>";
                    $add_info .="</span>";
                }
                if ($row['items']){
                    $is=false;
                    if ($add_info) $is=true;
                    if ($is==true) $add_info .="<span class=\"ow_tiny\">&nbsp;| </span>";
                    $add_info .="<span class=\"ow_tiny chatter_item\">";
                    $add_info .=OW::getLanguage()->text('chatter', 'items').":&nbsp;";
                    $add_info .="<b>".$row['items']."</b>";
                    $add_info .="</span>";
                }
                if ($row['first_date']){
                    $is=false;
                    if ($add_info) $is=true;
                    if ($is==true) $add_info .="<span class=\"ow_tiny\">&nbsp;| </span>";
                    $add_info .="<span class=\"ow_tiny chatter_item\">";

                    $add_info .=OW::getLanguage()->text('chatter', 'first_date').":&nbsp;";
                    $add_info .="<b>".str_replace(" ","&nbsp;",$row['first_date'])."</b>";
                    $add_info .="</span>";
                }
                if ($row['last_date']){
                    $is=false;
                    if ($add_info) $is=true;
                    if ($is==true) $add_info .="<span class=\"ow_tiny\">&nbsp;| </span>";
                    $add_info .="<span class=\"ow_tiny chatter_item\">";

                    $add_info .=OW::getLanguage()->text('chatter', 'last_date').":&nbsp;";
                    $add_info .="<b>".str_replace(" ","&nbsp;",$row['last_date'])."</b>";
                    $add_info .="</span>";
                }
                if ($row['licenseKey']){
                    $is=false;
                    if ($add_info) $is=true;
                    if ($is==true) $add_info .="<span class=\"ow_tiny\">&nbsp;| </span>";
                    $add_info .="<span class=\"ow_tiny chatter_item\">";

                    $add_info .=OW::getLanguage()->text('chatter', 'licenseKey').":&nbsp;";
                    $add_info .="<b>".$row['licenseKey']."</b>";
                    $add_info .="</span>";
                }
                if ($row['pp_email']){
                    $is=false;
                    if ($add_info) $is=true;
                    if ($is==true) $add_info .="<span class=\"ow_tiny\">&nbsp;| </span>";
                    $add_info .="<span class=\"ow_tiny chatter_item\">";

                    $add_info .=OW::getLanguage()->text('chatter', 'pp_email').":&nbsp;";
                    $add_info .="<b>".$row['pp_email']."</b>";
                    $add_info .="</span>";
                }
            }


            $room_name="";
            if (isset($row['room_name']) AND $row['room_name']) $room_name=stripslashes($row['room_name']);


//            $add_info=$row['plugin']."|".$row['host_post']."|".$row['host'];
                $message=$this->n2br(stripslashes($row['content']));
                $title=$dname;
                if ($row['is_fake'] AND $row['plugin']){
                    $title .="<span class=\"ow_smallmargin ow_tiny\">&nbsp;=>&nbsp;".stripslashes($row['host_post'])."&nbsp;=>&nbsp;".stripslashes($row['plugin'])."</span>";
                }

                            $content .="<div id=\"msg_".$row['id']."\" ng-repeat=\"message in messages\" class=\"item in\" style=\"opacity:1;\">
                                <div class=\"image\">
                                    <img src=\"".$uimg."\" alt=\"".$dname."\" style=\"width:40px;height:40px;\">";

if (!$row['is_fake'] AND ($is_admin OR ($row['id_owner']==$id_user AND $id_user>0)) ){
                $content .="<div class=\"ow_center\" style=\"margin-top:10px;\">
                    <a class=\"ow_lbutton\" onclick=\"return confirm('".$this->corect_tojava(OW::getLanguage()->text('chatter', 'confirm_delete'))."');\"   href=\"".$curent_url."chatter/action/delpost/".$row['id']."_".$row['id_item']."/".substr(session_id(),2,5)."\">".OW::getLanguage()->text('chatter', 'delete')."</a>
                </div>";
}
                                $content .="</div>


                                <div class=\"text\" style=\"height:100%;\">
                                    <div class=\"heading\">";
//                                            $content .="<div class=\"clearfix\">";
/*
                                            if ($row['is_readed_byadmin']=="0"){
                                                $content .="<div class=\"ow_right\">";
                                                $content .="<a class=\"ow_lbutton ow_red ow_disabled\" href=\"#\">".OW::getLanguage()->text('chatter', 'new_message')."</a>";
                                                $content .="</div>";
                                            }
*/

                                            $content .="<span class=\"ng-binding\">".$room_name."</span>";
//                                            $content .="</div>";

//                                        $content .="<a href=\"".$uurl."\" class=\"ng-binding\">".stripslashes($row['content'])."</a>";
                                        if (!$row['is_fake'] AND $id_user==$row['id_sender']){
                                            $content .="<span class=\"ng-binding chatx_sender\">".$title."</span>";
                                        }else if (!$row['is_fake'] AND $id_user==$row['id_recipment']){
                                            $content .="<span class=\"ng-binding chatx_recip\">".$title."</span>";
                                        }else if ($row['is_fake'] AND $id_user==$row['id_sender']){
                                            $content .="<span class=\"ng-binding chatx ow_red\" style=\"font-style: italic;\">".$title."</span>";
                                        }else if ($row['is_fake']){
                                            $content .="<span class=\"ng-binding chatx ow_green\">".$title."</span>";
                                        }else{
                                            $content .="<span class=\"ng-binding chatx\">".$title."</span>";
                                        }

                                        if ($row['is_fake'] AND $is_admin AND (!$id_room OR $is_faq==false)){
                                            $content .="<div class=\"ow_right\"><span class=\"date ng-binding\">".$dd."</span><br/><a class=\"ow_lbutton\" href=\"".$curent_url."chatter/room/".$row['id_item']."/info\">".OW::getLanguage()->text('chatter', 'open_this_room')."</a></div>";
                                        }else{
                                            $content .="<span class=\"date ng-binding\">".$dd."</span>";
                                        }




                                    $content .="</div>";
                                        $content .="<div ng-bind-html=\"message.message | rawHtml\" class=\"ng-binding\">";
                                            $content .="<div class=\"clearfix\">".$message."</div>";
                                            $content .="<div class=\"clearfix\">".$add_info."</div>";
                                        $content .="</div>";



                                        if ($row['is_fake'] AND $is_admin AND (!$id_room OR $is_faq==false) AND $row['is_readed_byadmin']=="0"){
                                                $content .="<div class=\"clearfix\">";
                                                $content .="<a class=\"ow_right ow_lbutton ow_red ow_disabled\" href=\"".$curent_url."chatter/room/".$row['id_item']."/info\" disabled>".OW::getLanguage()->text('chatter', 'new_message')."</a>";
                                                $content .="</div>";
                                        }


                                $content .="</div>
                            </div>";
        }
        return $content;
    }

    public function get_room_name($id_room=0 )
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $content="";

        if ($id_room>0){

            $add="";
            if ($is_admin){
                $add =" AND active='1' ";
            }

            $sql = "SELECT * FROM " . OW_DB_PREFIX. "chatter_rooms WHERE id='".addslashes($id_room)."' ".$add." LIMIT 1";
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $row ){
                $content=stripslashes($row['name']);
            }
        }
        return $content;
    }



    public function rooms(  )
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $content="";
        $cont_r="";

        $add=" 1 ";
        if ($is_admin){
            $add =" active='1' ";
        }

//        $max_rooms_per_page=OW::getConfig()->getValue('chatter', 'max_history_back');

        $max_rooms_per_page=OW::getConfig()->getValue('chatter', 'mode_perpage_rooms');
        if (!$max_rooms_per_page) $max_rooms_per_page=50;


//-------------------page start A
    $orderby="";
    $perpage_max=$max_rooms_per_page;
    //OW::getConfig()->getValue('chatter', 'max_history_back');

    if (!$perpage_max OR $perpage_max==0) $perpage_max=5;
    if (isset($_GET['page'])){
        $curent_page=($_GET['page']-1);
        if (!$curent_page) $curent_page=0;
    }else{
        $curent_page=0;
    }

    $prev_page=$curent_page-1;
    if ($prev_page<0) $prev_page=0;
    $next_page=$curent_page+1;
    $add_limit=($curent_page*$perpage_max);
    if (!$add_limit) $add_limit=0;
    $pagination="";
    $add_where="";

        $query = "SELECT au.id as allp  FROM " . OW_DB_PREFIX. "chatter_rooms au 
            WHERE ".$add;
//            WHERE id_seller='".addslashes($id_user)."' ";

//echo $query;exit;
    $arrx = OW::getDbo()->queryForList($query);
    if (isset($arrx[0])){
        $aaitem=$arrx[0];
        $all_items=count($arrx);
    }else{
        $all_items=0;
    }
    $limit_list=$add_limit.",".$perpage_max;
//echo $all_items;exit;
//-------------------page end A



        $sql = "SELECT * FROM " . OW_DB_PREFIX. "chatter_rooms 
                WHERE ".$add." 
            ORDER BY created DESC  
            LIMIT ".$limit_list;
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $row ){
                $cont_r .=$this->render_room($row);
            }


            if ($is_admin OR ($id_user>0 AND OW::getUser()->isAuthorized('chatter', 'roomscreate')) ){
            $content .="<div class=\"clearfix\">";
                $content .="<div class=\"ow_right\">";

                    $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                    <div class=\"ow_center\">
                        <a href=\"".$curent_url."chatter/action/newroom/create/".substr(session_id(),2,5)."\">
                        <span class=\"ow_button\">
                            <span class=\" ow_positive\">
                                <input type=\"button\" value=\"".OW::getLanguage()->text('chatter', 'create_new_room')."\" name=\"dosave\" class=\"ow_ic_add ow_positive\">
                            </span>
                        </span>
                        </a>
                    </div>
                    </div>";

                $content .="</div>";
            $content .="</div>";
            }



//        $url_pages="";
        $pagination=$this->makePagination(($curent_page+1), $all_items, $perpage_max, 1, $curent_url."chatter/rooms/show","?page=","right",$orderby);

        if ($cont_r){
            $content .="<div class=\"clearfix\">";
                $content .=$pagination;
            $content .="</div>";
        }


        $content .="<div class=\"clearfix\">";

        if ($cont_r){

            $content .="<div clss=\"panel panel-default\">";
                $content .="<div clss=\"panel-body\">";
                    $content .=$cont_r;
                $content .="</div>";
            $content .="</div>";

        }else{
            $content .=OW::getLanguage()->text('chatter', 'rooms_not_found');
        }
        $content .="</div>";


        if ($cont_r){
            $content .="<div class=\"clearfix\">";
                $content .=$pagination;
            $content .="</div>";
        }


        return $content;
    }

    public function render_room( $row="")
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;

        $pluginStaticURL=OW::getPluginManager()->getPlugin('chatter')->getStaticUrl();
//        $pluginStaticD=OW::getPluginManager()->getPlugin('startpage')->getStaticDir();
        $pluginUserDir=OW::getPluginManager()->getPlugin('chatter')->getUserFilesDir();
        $pluginUserUrl=OW::getPluginManager()->getPlugin('chatter')->getUserFilesUrl();

        $content="";
        if (is_array($row)){

            if ($row['on_passwd']==1 AND $row['paswd']!=""){
                $is_passwd=true;
            }else{
                $is_passwd=false;
            }

            if (isset($_SESSION['is_logged_'.$row['id']]) AND $_SESSION['is_logged_'.$row['id']]=="1"){
                $is_logged=true;
            }else{
                $is_logged=false;
            }

            $timeStamp=strtotime($row['created']);
            $dd=UTIL_DateTime::formatDate((int) $timeStamp);

            $content .="<div class=\"col-md-3\">";
                $content .="<div class=\"panel panel-default\">";

if (!$is_admin AND $is_passwd==true AND $is_logged!=true){
$content .="<a href=\"".$curent_url."chatter/login/".$row['id']."/".substr(session_id(),2,5)."\">";
}else{
$content .="<a href=\"".$curent_url."chatter/room/".$row['id']."\">";
}

if (OW::getConfig()->getValue('chatter', 'bg_card_color')!=""){
    $bgcx=" background: ".OW::getConfig()->getValue('chatter', 'bg_card_color')."; ";
}else{
    $bgcx="";
}

                        $content .="<div class=\"panel-body profile\" style=\"".$bgcx."\">";


        if ($row['id']>0){
            $filex=$pluginUserDir.$row['id']."_thumb.jpg";
            if ($this->file_exist($filex)){
                $img=$pluginUserUrl.$row['id']."_thumb.jpg";
            }else{
                $img=$pluginStaticURL."no_image.jpg";
            }
        }else{
            $img=$pluginStaticURL."no_image.jpg";
        }

                            $content .="<div class=\"profile-image\">";
                                $content .="<img src=\"".$img."\" width=\"100px\" height=\"100px\" title=\"".stripslashes($row['name'])."\" alt=\"".stripslashes($row['name'])."\">";

                        $content .="</div>";
            if ($is_passwd==true AND $is_logged!=true){
                $content .="<div class=\"clearfix\" >";
                    $content .="<div class=\"ow_center\" style=\"position: absolute;z-index: 1;text-align: center;width: 100%;\">";
                        $content .="<img src=\"".$pluginStaticURL."protect.png\" >";
                    $content .="</div>";
                $content .="</div>";
            }


                            $content .="<div class=\"profile-data\">
                                <div class=\"profile-data-name\">".stripslashes($row['name'])."</div>
                                <div class=\"profile-data-title\" style=\"word-wrap: break-word;\">".stripslashes($row['description'])."</div>
                            </div>
                        </div>";
$content .="</a>";


                        $content .="<div class=\"panel-body\">
                            <div class=\"contact-info\">";
                                $content .="<p><small>".OW::getLanguage()->text('chatter', 'created_date')."</small><br>".$dd."</p>";

        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">";
if (!$is_admin AND $is_passwd==true AND $is_logged!=true){
                    $content .="<a href=\"".$curent_url."chatter/login/".$row['id']."/".substr(session_id(),2,5)."\">";
}else{
                    $content .="<a href=\"".$curent_url."chatter/room/".$row['id']."\">";
}
                    $content .="<span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"button\" value=\"".OW::getLanguage()->text('chatter', 'open_this_room')."\" name=\"dosave\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                    </a>
                </div>
            </div>";

            if ($is_admin OR ($id_user>0 AND $row['id_owner']==$id_user AND OW::getUser()->isAuthorized('chatter', 'roomscreate')) ){
                $content .="<div class=\"clearfix ow_center\">";
                $content .="<a class=\"ow_lbutton ow_red\" style=\"margin-left:5px;\" onclick=\"return confirm('".$this->corect_tojava(OW::getLanguage()->text('chatter', 'confirm_delete'))."');\"  href=\"".$curent_url."chatter/action/del/".$row['id']."/".substr(session_id(),2,5)."\">".OW::getLanguage()->text('chatter', 'delete')."</a>";
                $content .="<a class=\"ow_lbutton ow_green\" style=\"margin-left:5px;\"  href=\"".$curent_url."chatter/action/edit/".$row['id']."/".substr(session_id(),2,5)."\">".OW::getLanguage()->text('chatter', 'edit')."</a>";
                $content .="</div>";
            }
                            $content .="</div>
                        </div>";
                                
                $content .="</div>";
            $content .="</div>";
        }
        return $content;
    }

    public function get_room_plugin($id_room=0)
    {
        $ret="";
        if ($id_room>0){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "chatter_plugins 
                WHERE id='".addslashes($id_room)."' LIMIT 1"; 
            $arrl = OW::getDbo()->queryForList($sql);
            foreach ( $arrl as $row ){
                if ($row['plugin']!=""){
                    $ret=stripslashes($row['plugin']);
                }
            }
        }
        return $ret;
    }


    public function getDepartmentLabel( $id )
    {
        return OW::getLanguage()->text('pay24', $this->getDepartmentKey($id));
    }

//    public function url_toparam ()
    public function check_url_allow ()
    {

//            return false;
/*
        if (OW::getRequest()->isPost()){
            return false;
        }else if (OW::getRequest()->isAjax()){
            return false;
        }
*/
/*
file_put_contents("TTTTTTTTTTTTTTT.txt", "\n==================================\n", FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", print_r($_GET,1), FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", print_r($_POST,1), FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", "\n==================================\n\n", FILE_APPEND );
*/
        $curent_url=OW_URL_HOME;
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) AND $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        $tmp=str_replace(OW_URL_HOME,"",$pageURL);
        if (substr($tmp,0,1)=="/") $tmp=substr($tmp,1);

        $test_furl=$tmp;
        $test_purl="";
        $test_aurl=array();

        $testx_tmp=explode("?",$test_furl);
        if (isset($testx_tmp[0])) $test_furl=$testx_tmp[0];
        if (isset($testx_tmp[1])) $test_purl=$testx_tmp[1];
        $test_aurl=explode("/",$test_furl);
//echo $test_furl;exit;
//            return false;
        if (isset($_POST['form_name']) AND $_POST['form_name']=="sign-in"){
            return false;
        }else if ( strpos($test_furl,"ajaxim")!==false){
            return false;
        }else if ( strpos($test_furl,"ow_cron")!==false){
            return false;
        }else if ( strpos($test_furl,"ow_static")!==false){
            return false;
        }else if ( strpos($test_furl,"ping")!==false){
            return false;
//        }else if (!OW::getUser()->isAuthenticated()){//for members only
//            return false;
        }else if ( strpos($test_furl,"base/ping/index")!==false OR strpos($test_furl,"base/ping")!==false){
            return false;
        }else{
            return true;
        }
    }


    public function get_news( )
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;

        $sql = "SELECT * FROM " . OW_DB_PREFIX. "news WHERE active='1' AND is_published='1'  ORDER BY `data_added` DESC LIMIT 10";
        $arrl = OW::getDbo()->queryForList($sql);
        $content="";
        foreach ( $arrl as $value )
        {
            $content .="<li>
                <div>
                    <a href=\"".$curent_url."news/".$value['id']."/index.html\">".stripslashes($value['topic_name'])."</a>
                </div>
            </li> ";
        }
        if ($content){
            $content ="<ul>".$content."</ul>";
        }
        return $content;
    }



    public function image_resize($file_source="",$crop=false,$width=800,$height=600)
    {
        return $this->image_copy_resize($file_source,$file_source,$crop,$width,$height);
    }

    public function image_copy_resize($file_source="",$file_dest="",$crop=false,$width=800,$height=600)
    {
        if ($file_source AND $file_dest){
            $image = new UTIL_Image($file_source);
            $mainPhoto = $image ->resizeImage($width, $height,$crop) ->saveImage($file_dest);
            return true;
        }else{
            return false;
        }
    }

    public function corect_exif($path="")
    {
//        if (!OW::getConfig()->getValue('shoppro', 'corect_exif')){
//            return;
//        }
        
//         $storage = OW::getStorage();
//          if ( !$storage->isWritable($path) )
            


        if ($path AND is_file($path) AND function_exists('exif_read_data') AND function_exists('imagerotate')){
//        if ($path AND is_file($path) AND function_exists('exif_read_data')){
//            ini_set('exif.encode_unicode', 'UTF-8');
            $exif = exif_read_data($path);
            if($exif) {
                if (isset($exif['Orientation'])){
                    $ort = $exif['Orientation'];
                }else if (isset($exif['IFD0']['Orientation'])){
                    $ort = $exif['IFD0']['Orientation'];
                }else if (isset($exif['EXIF']['Orientation'])){
                    $ort = $exif['EXIF']['Orientation'];
                }

                if($ort != 1){
                    $img = imagecreatefromjpeg($path) or die('Error opening file '.$path);
                    $deg = 0;
                    switch ($ort) {
                        case 3:
                            $deg = 180;
                        break;
                        case 6:
                            $deg = 270;
                        break;
                        case 8:
                            $deg = 90;
                        break;
                    }
                    if ($deg) {
//                      $imgr = $this->imagerotateXY($img, $deg, 0);        
                      $imgr = imagerotate($img, $deg, 0);        
                    }else{
                      $imgr=$img;
                    }
                    imagejpeg($imgr, $path, 95);
                    imagedestroy($img);
                    imagedestroy($imgr);
                }
            } // if have the exif orientation info
//        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND extension_loaded('magickwand')){
//        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND function_exists("NewMagickWand")){
//        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND extension_loaded('imagick')){
        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND class_exists("Imagick")){
            $exif = exif_read_data($path);
            if($exif) {
                if (isset($exif['Orientation'])){
                    $ort = $exif['Orientation'];
                }else if (isset($exif['IFD0']['Orientation'])){
                    $ort = $exif['IFD0']['Orientation'];
                }else if (isset($exif['EXIF']['Orientation'])){
                    $ort = $exif['EXIF']['Orientation'];
                }

                if($ort != 1){
                    $deg = 0;
                    switch ($ort) {
                        case 3:
                            $deg = -180;
                        break;
                        case 6:
                            $deg = -270;
                        break;
                        case 8:
                            $deg = -90;
                        break;
                    }
                    if ($deg) {
                        $imagick = new Imagick(); 
                        $imagick->readImage($path ); 
                        $imagick->rotateImage(new ImagickPixel('none'), $deg); 
                        $imagick->writeImage($path ); 
                        $imagick->clear(); 
                        $imagick->destroy(); 
                    }
                }
            }
        }
    }




    public function file_get_content($src="")
    {
        $ret="";
        if ($src){
            $storage = OW::getStorage();
            return $storage->fileGetContent($src);
        }else{
            return "";
        }
    }
    public function file_set_content($src="",$content="")
    {
        if ($src){
            $storage = OW::getStorage();
            $storage->fileSetContent($src,$content);
            return true;
        }else{
            return false;
        }
    }
    public function file_copy($src="",$dest="")
    {
        if ($src AND $dest){
            $storage = OW::getStorage();
            return $storage->copyFile($src,$dest);
        }else{
            return false;
        }
    }

    public function dir_copy($src="",$dest="", array $fileTypes = null, $level = -1)
    {
        if ($src AND $dest){
            $storage = OW::getStorage();
            if ( !$this->file_exist($dest) )
            {       
                $this->dir_mkdir($dest);
            }
            UTIL_File::copyDir($src, $dest, $fileTypes, $level);
        }else{
            return false;
        }
    }

    public function file_delete($src="")
    {
        if ($src){
            $storage = OW::getStorage(); 
            if ( $storage->fileExists($src) )
            {
                $storage->removeFile($src);
            }
            return true;
        }else{
            return false;
        }
    }

    public function dir_mkdir($src="")
    {
        if ($src){
            $storage = OW::getStorage(); 
            if ( !$storage->isWritable($src) )
            {
                if ( $storage->mkdir($src) )
                {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function file_exist($src="")
    {
        if ($src){
            $storage = OW::getStorage(); 
            if ( $storage->fileExists($src) )
            {
                return true;
            }else {
                return false;
            }
        }else{
            return false;
        }
    }

    public function file_rename($src="",$dest="")
    {
        if ($src AND $dest){
            $storage = OW::getStorage();
            return $storage->renameFile($src,$dest);
        }else{
            return false;
        }
    }

    public function get_dir($src="",$prefix=null, array $fileTypes = null)
    {
        if ($src){
            $storage = OW::getStorage();
            if ($this->isdir($src)){
                return $storage->getFileNameList($src,$prefix,$fileTypes);
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function isfile($src="")
    {
        if ($src){
            $storage = OW::getStorage(); 
            if ( $storage->isFile($src) )
            {
                return true;
            }else {
                return false;
            }
        }else{
            return false;
        }
    }
    public function isdir($src="")
    {
        if ($src){
            $storage = OW::getStorage(); 
            if ( $storage->isDir($src) )
            {
                return true;
            }else {
                return false;
            }
        }else{
            return false;
        }
    }
    public function get_plugin_dir($plugin="")
    {
        if ($plugin){
            return OW::getPluginManager()->getPlugin($plugin)->getUserFilesDir();
        }else{
            return false;
        }
    }
    public function get_plugin_url($plugin="")
    {
        if ($plugin){
            return OW::getStorage()->getFileUrl(OW::getPluginManager()->getPlugin($plugin)->getUserFilesDir());
        }else{
            return false;
        }
    }


    public function corect_tojava($content){
        $content=str_replace("'","",$content);
        $content=str_replace("+","\+",$content);
        $content=str_replace("\r\n","",$content);
        $content=str_replace("\n","",$content);
        $content=str_replace("\r","",$content);
        return $content;
    }


public function escapeJsonString($value) { 
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}

    public function br2n($text=""){
        $text=str_ireplace("<br>", "\n", $text);
        $text=str_ireplace("<br/>", "\n", $text);
        $text=str_ireplace("<br />", "\n", $text);
        $text=str_ireplace("<br >", "\n", $text);
        return $text;
    }
        
     public function html2txt($document){
        $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
        );
        $text = preg_replace($search, '', $document);
        $text=preg_replace("/(&?!amp;)/i", " ", $text);
        $text=preg_replace("/(&#\d+);/i", " ", $text); // For numeric entities
        $text=preg_replace("/(&\w+);/i", " ", $text); // For literal entities
        return $text;
    }

public function valid_xml ($str) {
    $str = str_replace("\"", "&quot;", $str);   
    $str = str_replace("<", "&lt;", $str);  
    $str = str_replace(">", "&gt;", $str);  
    $str = preg_replace("/&(?![a-zA-Z0-9#]+?;)/", "&amp;", $str);
    return $str;
}


    public function toASCII( $str="" )
    {
        return strtr(utf8_decode($str), 
            utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
            'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
    }





    private function code2utf($num){
    if($num<128)return chr($num);
    if($num<2048)return chr(($num>>6)+192).chr(($num&63)+128);
    if($num<65536)return chr(($num>>12)+224).chr((($num>>6)&63)+128).chr(($num&63)+128);
    if($num<2097152)return chr(($num>>18)+240).chr((($num>>12)&63)+128).chr((($num>>6)&63)+128). chr(($num&63)+128);
    return '';
    }
    private function  deCP1252 ($str) {
    $str = str_replace("&#128", "&#8364;", $str);
    $str = str_replace("&#129", "", $str);
    $str = str_replace("&#130;", "&#8218;", $str);
    $str = str_replace("&#131;", "&#402;", $str);
    $str = str_replace("&#132;", "&#8222;", $str);
    $str = str_replace("&#133;", "&#8230;", $str);
    $str = str_replace("&#134;", "&#8224;", $str);
    $str = str_replace("&#135;", "&#8225;", $str);
    $str = str_replace("&#136;", "&#710;", $str);
    $str = str_replace("&#137;", "&#8240;", $str);
    $str = str_replace("&#138;", "&#352;", $str);
    $str = str_replace("&#139;", "&#8249;", $str);
    $str = str_replace("&#140;", "&#338;", $str);
    $str = str_replace("&#145;", "&#8216;", $str);
    $str = str_replace("&#146;", "&#8217;", $str);
    $str = str_replace("&#147;", "&#8220;", $str);
    $str = str_replace("&#148;", "&#8221;", $str);
    $str = str_replace("&#149;", "&#8226;", $str);
    $str = str_replace("&#150;", "&#8211;", $str);
    $str = str_replace("&#151;", "&#8212;", $str);
    $str = str_replace("&#152;", "&#732;", $str);
    $str = str_replace("&#153;", "&#8482;", $str);
    $str = str_replace("&#154;", "&#353;", $str);
    $str = str_replace("&#155;", "&#8250;", $str);
    $str = str_replace("&#156;", "&#339;", $str);
    $str = str_replace("&#159;", "&#376;", $str);
    return $str;
    }

public function str2utf8($str) {
    mb_detect_order("ASCII, UTF-8, ISO-8859-1");
    if (mb_detect_encoding($str) == "UTF-8") {  
        preg_match_all("/&#([0-9]*?);/", $str, $unicode);
        foreach( $unicode[0] as $key => $value) {
            $str = preg_replace("/".$value."/", $this->code2utf($unicode[1][$key]), $str);
        }
        return $str;        
    } else {
        $mystr = $str;
        $str = "";
        for ($i = 0; $i < strlen($mystr); $i++ ) {
            $code = ord( $mystr[$i] );
            if ($code >= 128 && $code < 160) {
                $str .= "&#".$code.";";
            } else {             
                $str .= $this->code2utf($code);
            }
        }
        $str = $this->deCP1252($str);
        preg_match_all("/&#([0-9]*?);/", $str, $unicode);
        foreach( $unicode[0] as $key => $value) {
            $str = preg_replace("/".$value."/", $this->code2utf($unicode[1][$key]), $str);
        }
        
        return $str;
    }
} 

    public function clear_real_name($f_realname="")//clear 
    {
        $f_realname=strip_tags($f_realname,"");
        //$f_realname = ereg_replace("[^A-Za-z0-9 ]", "",$f_realname); 
        $f_realname=$this->str2utf8($f_realname);

        $from = array("ä","ö","ü","ß","Ä","Ö","Ü"," ","é");  //to correct double whitepaces as well
        $to  = array("&auml;","&ouml;","&uuml;","&szlig;","&Auml;","&Ouml;","&Uuml;","&nbsp;","&#233;");
        $f_realname = str_replace($from, $to, $f_realname);

        $f_realname=preg_replace("~[^A-Za-z0-9 (),]~", "", $f_realname);
        return $f_realname;
    }

    public function make_seo_url($name,$lengthtext=100)
    {
        $seo_sep="_";//- or _
        $seo_title=stripslashes($name);

$seo_title = preg_replace(array('/\s{2,}/', '/[\t\n]/'), $seo_sep, $seo_title);

    $seo_title=str_ireplace("\n"," ",$seo_title);
    $seo_title=str_ireplace("\t"," ",$seo_title);

        $seo_title=str_ireplace("_"," ",$seo_title);
        $seo_title=str_ireplace("  "," ",$seo_title);
        $seo_title=str_ireplace(" ",$seo_sep,$seo_title);
        $seo_title=str_ireplace(chr(160),$seo_sep,$seo_title);
        $seo_title=str_ireplace("~","",$seo_title);
        $seo_title=str_ireplace("(","",$seo_title);
        $seo_title=str_ireplace(")","",$seo_title);
        $seo_title=str_ireplace("]","",$seo_title);
        $seo_title=str_ireplace("[","",$seo_title);
        $seo_title=str_ireplace("}","",$seo_title);
        $seo_title=str_ireplace("{","",$seo_title);
        $seo_title=str_ireplace("/","",$seo_title);
        $seo_title=str_ireplace("\\","",$seo_title);
        $seo_title=str_ireplace("+","",$seo_title);
        $seo_title=str_ireplace(":","",$seo_title);
        $seo_title=str_ireplace(";","",$seo_title);
        $seo_title=str_ireplace("\"","",$seo_title);
        $seo_title=str_ireplace("<","",$seo_title);
        $seo_title=str_ireplace(">","",$seo_title);
        $seo_title=str_ireplace("?","",$seo_title);
        $seo_title=str_ireplace(",",".",$seo_title);
        $seo_title=str_ireplace("!","",$seo_title);
        $seo_title=str_ireplace("`","",$seo_title);
        $seo_title=str_ireplace("'","",$seo_title);
        $seo_title=str_ireplace("@","",$seo_title);
        $seo_title=str_ireplace("#","",$seo_title);
        $seo_title=str_ireplace("$","",$seo_title);
        $seo_title=str_ireplace("%","",$seo_title);
        $seo_title=str_ireplace("^","",$seo_title);
        $seo_title=str_ireplace("&","",$seo_title);
        $seo_title=str_ireplace("*","",$seo_title);
        $seo_title=str_ireplace("|","",$seo_title);
        $seo_title=str_ireplace("=","",$seo_title);
        $seo_title=str_ireplace(" ",$seo_sep,$seo_title);
        $seo_title=str_ireplace("/","",$seo_title);
        $seo_title=str_ireplace("?",$seo_sep,$seo_title);
        $seo_title=str_ireplace("#",$seo_sep,$seo_title);
        $seo_title=str_ireplace("=",$seo_sep,$seo_title);
        $seo_title=str_ireplace("=",$seo_sep,$seo_title);
        $seo_title=str_ireplace("&amp;",$seo_sep,$seo_title);
        $seo_title=str_ireplace($seo_sep.$seo_sep,$seo_sep,$seo_title);
        $seo_title=str_ireplace($seo_sep.$seo_sep,$seo_sep,$seo_title);
        $seo_title=str_ireplace($seo_sep.$seo_sep,$seo_sep,$seo_title);

        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }

    public function make_seo_urlOLD2($name,$lengthtext=100)
    {
        $seo_sep="-";//- or _
        $seo_title=stripslashes($name);
        $seo_title=str_ireplace($seo_sep," ",$seo_title);
        $seo_title=str_ireplace("_"," ",$seo_title);
        $seo_title=str_ireplace("  "," ",$seo_title);
        $seo_title=str_ireplace(" ",$seo_sep,$seo_title);
        $seo_title=str_ireplace(chr(160),$seo_sep,$seo_title);
        $seo_title=str_ireplace("~","",$seo_title);
        $seo_title=str_ireplace("(","",$seo_title);
        $seo_title=str_ireplace(")","",$seo_title);
        $seo_title=str_ireplace("]","",$seo_title);
        $seo_title=str_ireplace("[","",$seo_title);
        $seo_title=str_ireplace("}","",$seo_title);
        $seo_title=str_ireplace("{","",$seo_title);
        $seo_title=str_ireplace("/","",$seo_title);
        $seo_title=str_ireplace("\\","",$seo_title);
        $seo_title=str_ireplace("+","",$seo_title);
        $seo_title=str_ireplace(":","",$seo_title);
        $seo_title=str_ireplace(";","",$seo_title);
        $seo_title=str_ireplace("\"","",$seo_title);
        $seo_title=str_ireplace("<","",$seo_title);
        $seo_title=str_ireplace(">","",$seo_title);
        $seo_title=str_ireplace("?","",$seo_title);
        $seo_title=str_ireplace(",",".",$seo_title);
        $seo_title=str_ireplace("!","",$seo_title);
        $seo_title=str_ireplace("`","",$seo_title);
        $seo_title=str_ireplace("'","",$seo_title);
        $seo_title=str_ireplace("@","",$seo_title);
        $seo_title=str_ireplace("#","",$seo_title);
        $seo_title=str_ireplace("$","",$seo_title);
        $seo_title=str_ireplace("%","",$seo_title);
        $seo_title=str_ireplace("^","",$seo_title);
        $seo_title=str_ireplace("&","",$seo_title);
        $seo_title=str_ireplace("*","",$seo_title);
        $seo_title=str_ireplace("|","",$seo_title);
        $seo_title=str_ireplace("=","",$seo_title);

        $seo_title=str_ireplace("  "," ",$seo_title);
        $seo_title=str_ireplace(" ",$seo_sep,$seo_title);
        $seo_title=str_ireplace("/","",$seo_title);
        $seo_title=str_ireplace("?",$seo_sep,$seo_title);
        $seo_title=str_ireplace("#",$seo_sep,$seo_title);
        $seo_title=str_ireplace("=",$seo_sep,$seo_title);
        $seo_title=str_ireplace("=",$seo_sep,$seo_title);
        $seo_title=str_ireplace("&amp;",$seo_sep,$seo_title);

        $seo_title=str_ireplace($seo_sep.$seo_sep,$seo_sep,$seo_title);
        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }

    public function make_seo_urlOLD($name,$lengthtext=100)
    {
        $seo_title=stripslashes($name);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace(chr(160),"_",$seo_title);
        $seo_title=str_replace("~","",$seo_title);
        $seo_title=str_replace("(","",$seo_title);
        $seo_title=str_replace(")","",$seo_title);
        $seo_title=str_replace("]","",$seo_title);
        $seo_title=str_replace("[","",$seo_title);
        $seo_title=str_replace("}","",$seo_title);
        $seo_title=str_replace("{","",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("\\","",$seo_title);
        $seo_title=str_replace("+","",$seo_title);
        $seo_title=str_replace(":","",$seo_title);
        $seo_title=str_replace(";","",$seo_title);
        $seo_title=str_replace("\"","",$seo_title);
        $seo_title=str_replace("<","",$seo_title);
        $seo_title=str_replace(">","",$seo_title);
        $seo_title=str_replace("?","",$seo_title);
        $seo_title=str_replace(",",".",$seo_title);
        $seo_title=str_replace("!","",$seo_title);
        $seo_title=str_replace("`","",$seo_title);
        $seo_title=str_replace("'","",$seo_title);
        $seo_title=str_replace("@","",$seo_title);
        $seo_title=str_replace("#","",$seo_title);
        $seo_title=str_replace("$","",$seo_title);
        $seo_title=str_replace("%","",$seo_title);
        $seo_title=str_replace("^","",$seo_title);
        $seo_title=str_replace("&","",$seo_title);
        $seo_title=str_replace("*","",$seo_title);
        $seo_title=str_replace("|","",$seo_title);
        $seo_title=str_replace("=","",$seo_title);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("?","_",$seo_title);
        $seo_title=str_replace("#","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("&amp;","_",$seo_title);
        $seo_title=str_replace("__","_",$seo_title);
        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }


 public function get_remote_data($url, $post_paramtrs = false) {
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    if ($post_paramtrs) {
        curl_setopt($c, CURLOPT_POST, TRUE);
        curl_setopt($c, CURLOPT_POSTFIELDS, "var1=check&" . $post_paramtrs);
    } curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:32.0) Gecko/20100105 Firefox/32.0");
    curl_setopt($c, CURLOPT_COOKIE, 'CookieNameAron=Value;');
    curl_setopt($c, CURLOPT_MAXREDIRS, 10);
    $follow_allowed = ( ini_get('open_basedir') || ini_get('safe_mode')) ? false : true;
    if ($follow_allowed) {
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
    }curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 9);
    curl_setopt($c, CURLOPT_REFERER, $url);
    curl_setopt($c, CURLOPT_TIMEOUT, 60);
    curl_setopt($c, CURLOPT_AUTOREFERER, true);
    curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');
    $data = curl_exec($c);
    $status = curl_getinfo($c);
    curl_close($c);
    preg_match('/(http(|s)):\/\/(.*?)\/(.*\/|)/si', $status['url'], $link);
    $data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/|\/)).*?)(\'|\")/si', '$1=$2' . $link[0] . '$3$4$5', $data);
    $data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/)).*?)(\'|\")/si', '$1=$2' . $link[1] . '://' . $link[3] . '$3$4$5', $data);
    if ($status['http_code'] == 200) {
        return $data;
    } elseif ($status['http_code'] == 301 || $status['http_code'] == 302) {
        if (!$follow_allowed) {
            if (empty($redirURL)) {
                if (!empty($status['redirect_url'])) {
                    $redirURL = $status['redirect_url'];
                }
            } if (empty($redirURL)) {
                preg_match('/(Location:|URI:)(.*?)(\r|\n)/si', $data, $m);
                if (!empty($m[2])) {
                    $redirURL = $m[2];
                }
            } if (empty($redirURL)) {
                preg_match('/href\=\"(.*?)\"(.*?)here\<\/a\>/si', $data, $m);
                if (!empty($m[1])) {
                    $redirURL = $m[1];
                }
            } if (!empty($redirURL)) {
                $t = debug_backtrace();
                return call_user_func($t[0]["function"], trim($redirURL), $post_paramtrs);
            }
        }
    } return "ERRORCODE22 with $url!!<br/>Last status codes<b/>:" . json_encode($status) . "<br/><br/>Last data got<br/>:$data";
 }//f

/*
$string = 'This is image inside text string http://localhost/test/something.jpg?xml=10, http://google.com/logo.png'; 
function _callback($matches){
return '<img src="'.$matches[0].'" />'
}

echo preg_replace_callback('/https?:\/\/(.*?)\.(jpg|png|gif)(\?\w+=\w+)?/i', '_callback', $string);
*/


    public function text_to_html($text="",$title="",$oryginal_url="",$url="",$max_width=150,$max_height=150,$margin=5) { 

        $text=str_replace("\r\n","\n",$text);
        $text=str_replace("\n","<br/>",$text);

//        $text=preg_replace('/((http?:\/\/)(.*?)\.(jpg|png|gif))(\?\w+=\w+)?/i','//${3}.${4}', $text);
        $text=preg_replace('/((http?:\/\/)([a-zA-Z0-9\/\._\-]*)\.(jpg|png|gif))(\?\w+=\w+)?/i','//${3}.${4}', $text);

//        $text=preg_replace('/((https?:\/\/)(.*?)\.(jpg|png|gif))(\?\w+=\w+)?/i','//${3}.${4}', $text);
        $text=preg_replace('/((https?:\/\/)([a-zA-Z0-9\/\._\-]*)\.(jpg|png|gif))(\?\w+=\w+)?/i','//${3}.${4}', $text);

//echo "---".$oryginal_url."--".$url;exit;

        $patterns = array();
//        $patterns[0] = '/(\/\/(.*?)\.(jpg|png|gif))(\?\w+=\w+)?/i';
        $patterns[0] = '/(\/\/([a-zA-Z0-9\/\._\-]*)\.(jpg|png|gif))(\?\w+=\w+)?/i';
        $patterns[1] = '/(..\/..\/ow_userfiles\/(.*?)\.(jpg|png|gif))(\?\w+=\w+)?/i';
        $patterns[2] = '/\/(image\/..\/ow_userfiles\/(.*?)\.(jpg|png|gif))(\?\w+=\w+)?/i';

        $replacements = array();
        if ($url){
            $replacements[0] = '<a href="'.$url.'" ><img src="//${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/></a>';
            $replacements[1] = '<a href="'.$url.'" ><img src="'.$oryginal_url.'${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/></a>';
            $replacements[2] = '<a href="'.$url.'" ><img src="'.$oryginal_url.'${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/></a>';
            $text=preg_replace($patterns, $replacements, $text);
        }else{
            $replacements[0] = '<img src="//${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/>';
            $replacements[1] = '<img src="'.$oryginal_url.'${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/>';
            $replacements[2] = '<img src="'.$oryginal_url.'${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/>';
            $text=preg_replace($patterns, $replacements, $text);
        }

        return $text;
    }
/*
RBL!!!

$host = '64.53.200.156';
$rbl  = 'sbl-xbl.spamhaus.org';
// valid query format is: 156.200.53.64.sbl-xbl.spamhaus.org
$rev = array_reverse(explode('.', $host));
$lookup = implode('.', $rev) . '.' . $rbl;
if ($lookup != gethostbyname($lookup)) {
    echo "ip: $host is listed in $rbl\n";
} else {
    echo "ip: $host NOT listed in $rbl\n";
}

*/

public function base64url_encode($data) { 
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 

public function base64url_decode($data) { 
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
} 

    public function safe_content($content="",$more=1){
        $content = preg_replace("/(<iframe[^<]+<\/iframe>)/", '', $content);
        $content = preg_replace('/<script\b[^>]*>(.*?)<\/script>/i', "", $content);
        $content = preg_replace('~<\s*\bscript\b[^>]*>(.*?)<\s*\/\s*script\s*>~is', '', $content);
        $content=str_replace("'","`",$content);
//        if ($more){
//            $content=addslashes($content);
//        }
        return $content;
    }

    public function clear_content($content="")
    {
        $content = preg_replace('/<[^>]*>/', '', $content);
        $content = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', strip_tags($content) );
        return $content;
    }

    public function safe_texttohtml($content=""){
        $content=preg_replace('/(http|https):\/\/(\S*)\.(jpg|gif|png)(\?(\S*))?(?=\s|$|\pP)/i', '<img src="http://$2.$3$4" border="0" alt="" style="max-width:100%;max-height:600px;"/>', $content);
        $content= preg_replace('/\s(\w+:\/\/)(\S+)/', ' <a href="\\1\\2" target="_blank" rel=\"nofollow\">\\1\\2</a>', $content);
        return $content;
    }

    public function safe_htmltotext($content=""){
/*
        $content=preg_replace('/(:?href=\")(.+?)(:?\")/', '$2', $content);
        $content=preg_replace('/(:?src=\")(.+?)(:?\")/', '$2', $content);
        $content=preg_replace('/(<)([img])(\w+)([^>]*>)/', '$2', $content);
*/
        $content=$this->safe_content($content);
        $content=preg_replace('/<a[^>]* href=\"([^\"]*)\"[^>]*>/', '$1 ', $content);
        $content=preg_replace('/<img[^>]* src=\"([^\"]*)\"[^>]*>/', '$1 ', $content);
        $content=$this->html2txt($content);
        return $content;
    }


    public function get_curect_lang_id()
    {
//print_r($_SESSION);exit;
//        if (isset($_SESSION['base.language_id']) AND $_SESSION['base.language_id']>0) return $_SESSION['base.language_id'];
//            else return 0;
        $clan=0;
        if (isset($_SESSION['base_language_id']) AND $_SESSION['base_language_id']>0) $clan= $_SESSION['base_language_id'];
        if (!$clan AND isset($_SESSION['base.language_id']) AND $_SESSION['base.language_id']>0) $clan= $_SESSION['base.language_id'];
//echo $_SESSION['base.language_id'];exit;
        if (!$clan) $this->get_system_lang_id();

        return $clan;
    }

    public function get_system_lang_id()
    {
/*
            $sql = "SELECT bc.value, bl.id FROM " . OW_DB_PREFIX. "base_config bc 
    LEFT JOIN " . OW_DB_PREFIX. "base_language bl ON (bl.tag=bc.value) 
            WHERE `key`='locale' AND `name`='default_language' LIMIT 1";
//echo $sql;exit;
            $arr = OW::getDbo()->queryForList($sql);
            if (isset($arr[0]) AND isset($arr[0]['id']) AND $arr[0]['id']>0){
                return  $arr[0]['id'];
            }else{
                if (isset($_SESSION['base.language_id']) AND $_SESSION['base.language_id']>0) return $_SESSION['base.language_id'];
                    else return 0;
            }
*/
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "base_language  
            WHERE `status`='active' ORDER BY `order` LIMIT 1";

            $arr = OW::getDbo()->queryForList($sql);
            if (isset($arr[0]) AND isset($arr[0]['id']) AND $arr[0]['id']>0){
                return  $arr[0]['id'];
            }else{
                if (isset($_SESSION['base.language_id']) AND $_SESSION['base.language_id']>0) return $_SESSION['base.language_id'];
                    else return 0;
            }
    }

    public function is_allow($action="",$plugin="chatter") {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();

        if ($is_admin){
            return true;
        }else if ($action AND $plugin AND OW::getUser()->isAuthorized($plugin, $action) ){
            return true;
        }else{
            return false;
        }
    }

public function getClientIP() {

    if (isset($_SERVER)) {

        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            return $_SERVER["HTTP_X_FORWARDED_FOR"];

        if (isset($_SERVER["HTTP_CLIENT_IP"]))
            return $_SERVER["HTTP_CLIENT_IP"];

        return $_SERVER["REMOTE_ADDR"];
    }

    if (getenv('HTTP_X_FORWARDED_FOR'))
        return getenv('HTTP_X_FORWARDED_FOR');

    if (getenv('HTTP_CLIENT_IP'))
        return getenv('HTTP_CLIENT_IP');

    return getenv('REMOTE_ADDR');
}

    private function make_xml_friendly( $url,$replay=" " )
    {

        $url=htmlentities($url, ENT_QUOTES, 'UTF-8');

        $find = array("\r\n","\r","\n");
        $url = str_ireplace ($find, '', $url);

        $url= str_replace(array('&','>','<','"'), array('&amp;','&gt;','&lt;','&quot;'), $url);

      $find = array(' ',
      '\r\n',
      '\n',
      '/',
      '“',
      '~',
      '\\',
      '+',
      '<',
      '>');
       $url = str_ireplace ($find, $replay, $url);


        return $url;
    }
    private function make_url_friendly( $url,$replay="_" )
    {
        $url = trim($url);
        $url = strtolower($url);
        $find = array('<b>','</b>');
        $url = str_replace ($find, '', $url);

        $find = array("\r\n","\r","\n");
        $url = str_replace ($find, '', $url);

        $url = preg_replace('/<(\/{0,1})img(.*?)(\/{0,1})\>/', 'image', $url);
        $find = array(' ',
      '&quot;',
      'quot',
      '&',
      'amp;',
      '\r\n',
      '\n',
      '/',
      '\\',
      '“',
      '~',
      '+',
      '<',
      '>');
       $url = str_replace ($find, $replay, $url);

        $find = array('/[^a-z0-9\-<>]/',
      '/[\-]+/',
      '/<[^>]*>/');
       $repl = array('',
      '-',
      '');
       $url = preg_replace ($find, $repl, $url);
       $url = str_replace ('--', '-', $url);

        return $url;
    }
//----'-------

//-------comments
    public function getCommentCount($id_item=0,$prefix="chatter-item") {
        $ret=0;
        if ($id_item>0 AND $prefix!=""){
            $ret =BOL_CommentService::getInstance()->findCommentCount($prefix, $id_item);
        }
        return $ret;
    }
//-----newsfeed wall
    public function delete_from_wall($id_item=0,$prefix="chatter-item") {
        if (!OW::getPluginManager()->isPluginActive('newsfeed')){
            return;
        }

        if ($id_item>0 AND $prefix!=""){
            OW::getEventManager()->trigger(new OW_Event('feed.delete_item', array('entityType' => $prefix, 'entityId' => $id_item)));
        }
    }


    public function add_to_wall($last_insert_id=0,$entityType="chatter-item", $title="",$content="",$url_to_ads="",$for_user=0, $pluginKey="chatter",$icon_class="ow_ic_link")
    {


        if (!OW::getPluginManager()->isPluginActive('newsfeed')){
            return;
        }

        if (!$last_insert_id OR !$entityType){
            return;
        }

        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $curent_url=OW_URL_HOME;                                        

        if (!$id_user){
            return;
        }

        if (!$for_user AND $id_user>0) $for_user=$id_user;

        $title_ok=$this->html2txt($title);

        $content=$this->html2txt($content);

        if (!$title_ok) $title_ok=UTIL_String::truncate($content, 32, '..');
        if (!$title_ok) $title_ok="no title..";

        $title_post_lenght=255;
        if (!$icon_class) $icon_class="";

        if ($url_to_ads){
//            $title="<a href=\"".$url_to_ads."\">".$url_to_ads."</a>\r\n<div class=\"ow_remark\" style=\"paddig-top: 4px\">".UTIL_String::truncate($title, $title_post_lenght, '..')."</div>";
            $title="<a href=\"".$url_to_ads."\"><div class=\"ow_remark\" style=\"paddig-top: 4px\">".UTIL_String::truncate($title_ok, $title_post_lenght, '..')."</div></a>";
        }

        $event = new OW_Event('feed.action', array(
            'pluginKey' => $pluginKey,
            'entityType' => $entityType,
            'entityId' => $last_insert_id,
            'userId' => $for_user,
            'replace' => true,
            'feedType' => 'user',
            'feedId' => $last_insert_id,
            'features' => array('likes','comments'),
            ), array(
                'string' =>  UTIL_String::truncate($title_ok, $title_post_lenght, '..'),
                'content' => UTIL_String::truncate($content, 200, '..'),
                'view' => array("iconClass" => $icon_class)
            )
        );
        OW::getEventManager()->trigger($event);

    }

    public function add_to_wall_friends($last_insert_id=0,$entityType="chatter-item", $title="",$content="",$url_to_ads="",$for_user_owner=0, $pluginKey="chatter",$icon_class="ow_ic_link")
    {


        if (!OW::getPluginManager()->isPluginActive('newsfeed')){
            return;
        }
        if (!OW::getPluginManager()->isPluginActive('friends')){
            return;
        }


        if (!$last_insert_id OR !$entityType){
            return;
        }

        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $curent_url=OW_URL_HOME;                                        


        if (!$for_user_owner) {
            return;
        }

        if (!$id_user){
            return;
        }

            $title_ok=$this->html2txt($title);

            $content=$this->html2txt($content);

            if (!$title_ok) $title_ok=UTIL_String::truncate($content, 32, '..');
            if (!$title_ok) $title_ok="no title..";

            $title_post_lenght=255;
            if (!$icon_class) $icon_class="";
        


        $all_f=$this->get_friends($for_user_owner,100, "friends",0);

        for ($i=0;$i<count($all_f);$i++){

            $for_user=$all_f[$i];


            if ($url_to_ads){
                $title="<a href=\"".$url_to_ads."\"><div class=\"ow_remark\" style=\"paddig-top: 4px\">".UTIL_String::truncate($title_ok, $title_post_lenght, '..')."</div></a>";
            }


        $event =new OW_Event('feed.activity', array(
            'activityType' => 'subscribe',
            'activityId' => $for_user,
            'entityId' => $last_insert_id,
            'entityType' => $entityType,
            'userId' => $for_user,
            'pluginKey' => $pluginKey
        ), array(
            'string' =>  UTIL_String::truncate($title_ok, $title_post_lenght, '..'),
            'content' => UTIL_String::truncate($content, 200, '..'),
            'view' => array("iconClass" => $icon_class)
        ));
        OW::getEventManager()->trigger($event);


/*
            $event = new OW_Event('feed.action', array(
                'pluginKey' => $pluginKey,
                'entityType' => $entityType,
                'entityId' => $last_insert_id,
                'userId' => $for_user,
                'replace' => true,
                'feedType' => 'user',
                'feedId' => $last_insert_id,
                'features' => array('likes','comments'),
                ), array(
                    'string' =>  UTIL_String::truncate($title, $title_post_lenght, '..'),
                    'content' => UTIL_String::truncate($content, 200, '..'),
                    'view' => array("iconClass" => $icon_class)
                )
            );
            OW::getEventManager()->trigger($event);
*/
        }//for

    }

//-------friends
    public function get_friends($for_user=0,$how_many=100,$type="friends",$start=0)
    {
        $ret=array();
        if (!OW::getPluginManager()->isPluginActive('friends') OR !$for_user){
            return $ret;
        }

        $ret=FRIENDS_BOL_Service::getInstance()->findFriendIdList( $for_user, $start, $how_many, $type );
        return $ret;
    }

    public function make_tabs($selected=1,$content="",$name="")
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $content_t ="";
            $content_t .="<div class=\"ow_content\">";

$content_t .="<div class=\"ow_content_menu_wrap\">";
$content_t .="<ul class=\"ow_content_menu clearfix\">";

//        if (OW::getUser()->isAuthorized('chatter', 'public') OR $is_admin){

            if ($selected==1 OR $selected=="index" OR $selected=="chat" OR !$selected) $sel=" active ";
                else $sel="";
//            $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."chatter\"><span class=\"ow_ic_dashboard\">".OW::getLanguage()->text('chatter', 'dashboard')."</span></a></li>";//moje zamówienia
            $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."chatter\"><span class=\"ow_ic_dashboard\">".OW::getLanguage()->text('chatter', 'public_chat')."</span></a></li>";//moje zamówienia

            if ($selected=="rooms") $sel=" active ";
                else $sel="";
            $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."chatter/rooms/show\"><span class=\"ow_ic_folder\">".OW::getLanguage()->text('chatter', 'rooms')."</span></a></li>";//moje zamówienia



        if (OW::getUser()->isAuthorized('chatter', 'roomsread') OR OW::getUser()->isAuthorized('chatter', 'roomswrite')){
            if ($selected=="room" AND $name){
                $sel=" active ";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."chatter/rooms/show\"><span class=\"ow_ic_friends\">".OW::getLanguage()->text('chatter', 'selected_room').": ".$name."</span></a></li>";//moje zamówienia
            }else if ($selected=="roomp" AND $name){
                $sel=" active ";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."chatter/rooms/show\"><span class=\"ow_ic_friends\">".$name."</span></a></li>";//moje zamówienia
            }
        }


        if (OW::getUser()->isAuthorized('chatter', 'roomscreate')){
            if ($selected=="newroom" OR $selected=="editroom"){
                $sel=" active ";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."chatter/action/newroom/create/".substr(session_id(),2,5)."\"><span class=\"ow_ic_add\">".OW::getLanguage()->text('chatter', 'edit_add_room')."</span></a></li>";//moje zamówienia
            }
        }





            if ($is_admin AND OW::getConfig()->getValue('chatter', 'chatter_enable')=="1" AND OW::getConfig()->getValue('chatter', 'feature')=="1"){
                if ($selected=="support") $sel=" active ";
                    else $sel="";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."chatter/rooms/support\"><span class=\"ow_ic_folder\">".OW::getLanguage()->text('chatter', 'support')."<div id=\"support_cointer\" class=\"ow_count_active ow_count\" style=\"display:none;font-size: 11px;margin-left:10px;\">&nbsp;0</div></span></a></li>";//moje zamówienia                
            }




//        }

$content_t .="</ul>";
$content_t .="</div>";
        $content_t .=$content;
        $content_t .="</div>";
        return $content_t;
    }

    public function makePagination($page = 1, $totalitems, $limit = 15, $adjacents = 1, $targetpage = "/", $pagestring = "?page=",$position="right",$contentadd="")
    {               
    //defaults
    if(!$adjacents) $adjacents = 1;
    if(!$limit) $limit = 15;
    if(!$page) $page = 1;
    if(!$targetpage) $targetpage = "/";
    $margin="";
    $padding="";
    $pagestring1="";
    //other vars
    $prev = $page - 1;                                                                  //previous page is page - 1
    $next = $page + 1;                                                                  //next page is page + 1
    $lastpage = ceil($totalitems / $limit);                             //lastpage is = total items / items per page, rounded up.
    $lpm1 = $lastpage - 1;                                                              //last page minus 1
    $space=" ";
//$lastpage++;    
//return "--".$lastpage;    
    if ($position=="center") $position="ow_center";
    else if ($position=="left") $position="ow_left";
    else $position="ow_right";
    $pagination = "";
    if($lastpage > 1)
    {   
        $pagination .= "<div class=\"".$position." ow_paging clearfix ow_smallmargin\"";
        if($margin || $padding)
        {
            $pagination .= " style=\"";
            if($margin)
                $pagination .= "margin: $margin;";
            if($padding)
                $pagination .= "padding: $padding;";
            $pagination .= "\"";
        }
        $pagination .= ">";

        //previous button
        if ($page > 1) 
            $pagination .= "<a href=\"$targetpage$pagestring$prev\">«</a>".$space;
        else
            $pagination .= "<a class=\"disabled\" href=\"".$targetpage.$pagestring1."\">«</a>".$space;
//            $pagination .= "<span class=\"disabled\">«</span>";    
        $pagination .= "&nbsp;&nbsp;&nbsp;";
        
        //pages 
        if ($lastpage < 7 + ($adjacents * 2))   //not enough pages to bother breaking it up
        {       
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
//                    $pagination .= "<span class=\"active\">$counter</span>";
                if ($counter == $page)
                    $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                     
                else
                    $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                     
            }
        }
        elseif($lastpage >= 7 + ($adjacents * 2))       //enough pages to hide some
        {
            //close to beginning; only hide later pages
            if($page < 1 + ($adjacents * 3))            
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
//                        $pagination .= "<span class=\"active\">$counter</span>";
                    if ($counter == $page)
                        $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                    else
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                }
                $pagination .= "<span class=\"elipses\">...</span>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>".$space;               
            }
            //in middle; hide some front and some back
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>".$space;
                $pagination .= "<span class=\"elipses\">...</span>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
//                        $pagination .= "<span class=\"active\">$counter</span>";
                    if ($counter == $page)
                        $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                  
                    else
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                }
                $pagination .= "...";
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>".$space;               
            }
            //close to end; only hide early pages
            else
            {
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>".$space;
                $pagination .= "<span class=\"elipses\">...</span>".$space;
                for ($counter = $lastpage - (1 + ($adjacents * 3)); $counter <= $lastpage; $counter++)
                {
//                      $pagination .= "<span class=\"active\">$counter</span>";
                    if ($counter == $page)
                        $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                    else
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                }
            }
        }
        
        //next button
        $pagination .= "&nbsp;&nbsp;&nbsp;";
        if ($page < $counter - 1) 
            $pagination .= "<a href=\"" . $targetpage . $pagestring . $next . "\">»</a>".$space;
        else
            $pagination .= "<a class=\"disabled\" href=\"" . $targetpage . $pagestring . $lastpage . "\">»</a>".$space;
//            $pagination .= "<a class=\"disabled\" href=\"" . $targetpage . $pagestring . $next . "\">»</a>".$space;
//            $pagination .= "<span class=\"disabled\">»</span>";
        $pagination .= "</div>\n";
    }

        if ($pagination){
            return "<div class=\"clearfix ow_smallmargin\" style=\"width:100%;margin:auto;display: inline-block;\">".$pagination.$contentadd."</div>";
        }else if ($contentadd){
            return "<div class=\"clearfix ow_smallmargin\" style=\"width:100%;margin:auto;display: inline-block;\">".$contentadd."</div>";
        }else{
            return "";
        }

    }



//------- recomended plu

    private function gpk($pl="")
    {
        $ret="";
        if ($pl!="" AND strlen($pl)>1){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "base_plugin WHERE `key`='".addslashes($pl)."' LIMIT 1";
            $arrxx = OW::getDbo()->queryForList($sql);
            if (isset($arrxx[0]) AND isset($arrxx[0]['licenseKey']) AND $arrxx[0]['licenseKey']!="") $ret=base64_encode($arrxx[0]['licenseKey']);
        }
        return $ret;
    }

public function ar_check_ads_v1()
{


    if (!function_exists('curl_init') OR !function_exists('curl_exec')) return;

    $cversion=2.0;
    $ret="";
    $content="";
    $tex=base64_encode(OW_URL_HOME);
//    $texn=base64_encode($_SERVER['SERVER_NAME']);
    $texi=base64_encode($_SERVER['SERVER_ADDR']);
    $_SESSION['wasarrun']=true;
    $cplugin="chatter";
//    $sUrl = "http://www.oxwall.a6.pl/OW_PLUGINFO/index.php";
    $sUrl = "http://www.oxwall.a6.pl/chatter/chatter";

    $pky="";

    $is_chat=false;
//    $is_type="plugin";
    $is_allow_chat=false;
    $is_order="rand";


    $is_didabled=false;
    if (!isset($_SESSION['chat_allowrecomemded']) OR (isset($_SESSION['chat_allowrecomemded']) AND $_SESSION['chat_allowrecomemded']!="0") ){
//    if (OW::getConfig()->getValue('base', 'allowrecomemded') AND OW::getConfig()->getValue('base', 'allowrecomemded')=="1"){
        $is_didabled=false;
    }else{
        $is_didabled=true;
    }

    if (OW::getConfig()->getValue('base', 'allowlivesupportchat') AND OW::getConfig()->getValue('base', 'allowlivesupportchat')=="1"){
        $is_didabled_chat=false;
    }else{
        $is_didabled_chat=true;
    }

//echo $is_didabled."--".$is_didabled_chat;exit;

    if ($is_didabled==false){
//        $content_w =$this->ar_che_get_remote_data($sUrl,"cplugin=".$cplugin."&ch=".$tex);
//        $content_w =$this->ar_che_get_remote_data($sUrl,"cplugin=".$cplugin."&ch=".$tex."&chi=".$texi."&cver=".$cversion);
        $content_w =$this->ar_che_get_remote_data($sUrl,"cplugin=".$cplugin."&cver=".$cversion);
    }


    if ($is_didabled==false AND strlen($content_w)>10 AND substr($content_w,2,11)==="xml version"){
        $content_xml = new SimpleXMLElement($content_w);



        if ($content_xml->attributes()->corect=="true" AND $content_xml->attributes()->version<=$cversion){


            if ($content_xml->attributes()->chat=="true" AND $content_xml->attributes()->version>"1.0"){//chat require bugger ver then 1.0
                $is_chat=true;
            }
//$is_chat=true;
            if ($content_xml->attributes()->order!=""){
                $is_order=$content_xml->attributes()->order;
            }


            $e = array();
            foreach ($content_xml->item as $item) {
                $e[] = $item;
            }

            if ($is_order=="inc"){
                // sort the array inc
                usort($e, function($a, $b) {
                    return $a->id - $b->id;
                });

            }else if ($is_order=="dec"){
                // sort the array dec
                usort($e, function($a, $b) {
                    return $b->id - $a->id;
                });
            }else{
                // sort the array rand
                shuffle($e);
                foreach ($e as $node) {
                    $node->saveXML();
                }
            }
/*
            // sort the array inc
            usort($e, function($a, $b) {
            return $a->order - $b->order;
            });
*/

//foreach ($content_xml->item as $item) {
            foreach ($e as $item) {

        if (isset($item->title) AND $item->title!=""){
            $title=UTIL_String::truncate($item->title, 100, '..');
            $plugin_is=false;
            $plugin=$item->plugin."";
            $pky="";
            if (strlen($item->plugin)>2){
                if (OW::getPluginManager()->isPluginActive($plugin)){
                    $pky=$this->gpk($plugin);
                    $plugin_is=true;
                }
            }


/*
            $id=$item->id;

            $check=$item->check;

        
        //atributy item
        foreach ($item->attributes() as $atr) {
            echo '<li>' . $atr . '</li>';        
        }

            $date=$item->date;

        foreach ($item->date->attributes() as $atr) {
            echo '<li>' . $atr . '</li>';        
        }
*/

                $body=$item->body;
                $body=str_replace("'","",$body);
                $body=str_replace("+","\+",$body);
                $body=str_replace("\r\n","<br/>",$body);
                $body=str_replace("\n","<br/>",$body);
                $body=str_replace("\r","",$body);
                $body=UTIL_String::truncate($body, 250, '..');

                $more=OW::getLanguage()->text('base', 'more');
                $more=str_replace("'","",$more);
                $more=str_replace("+","\+",$more);
                $more=str_replace("\r\n","",$more);
                $more=str_replace("\n","",$more);
                $more=str_replace("\r","",$more);



            if (($item->type=="plugin") AND $plugin_is==false){//---------------------------------------------------------------plugin



                $content .="<div id=\"ad_".$item->id."\" class=\"clearfix ow_border\" style=\"width:100%;margin:auto;max-height:100px;border-width:0px 0 1px 0;min-height:82px;margin-bottom:5px;\">";
                $content .="<div class=\"ow_ipc clearfix\" style=\"margin:5px\">";

                    if (isset($item->img)){
                        $content .="<div class=\"ow_leftX ow_ipc_pictureX\" style=\"margin:3px 5px 0 0;position: relative;width:100%;max-width:100px;max-height:75px;height:1px;\">";
                            $content .="<a href=\"".$item->url."\" target=\"_blank\">";
                                $content .="<img src=\"".$item->img."\" style=\"width:100%;max-width:100px;max-height:75px;position: absolute;\">";
                            $content .="</a>";

                            if ($plugin_is==true OR $plugin_is=="1"){
                                $content .="<div class=\"clearfix\" style=\"position:absolute;top:-5px;left:-4px;\">";
                                    $content .="<span class=\"ow_green\" style=\"font-weight:bold;\">OK!</span>";
                                $content .="</div>";
                            }

                        $content .="</div>";
                    }
                    $content .="<div class=\"ow_left ow_ipc_info\" style=\"margin-left: 105px;    display: inline-block;position: inherit;     max-height: 60px;overflow: hidden;border-width:0;padding:1px 5px 16px 16px;;\">";
                        $content .="<a href=\"".$item->url."\" target=\"_blank\">";
                            $content .="<h1>".$title."</h1>";
                        $content .="</a>";

                        $content .="<a href=\"".$item->url."\" terget=\"_blank\">";
                            $content .=$body;
                        $content .="</a>";

                        $content .="<div class=\"ow_rightX\" style=\"margin-left:10px;display:inline;\">";
                            $content .="<a class=\"ow_lbutton\" href=\"".$item->url."\" target=\"_blank\">".$more."</a>";
                        $content .="</div>";

                    $content .="</div>";

                $content .="</div>";
                $content .="</div>";
            }else if ($item->type=="info"){//---------------------------------------------------------------info

                $content .="<div id=\"ad_".$item->id."\" class=\"clearfix ow_border\" style=\"width:100%;margin:auto;max-height:100px;border-width:1px;min-height:82px;margin-bottom:5px;\">";
                $content .="<div class=\"ow_ipc clearfix\" style=\"margin:5px\">";
/*
                    if (isset($item->img)){
                        $content .="<div class=\"ow_leftX ow_ipc_pictureX\" style=\"margin:3px 5px 0 0;position: relative;width:100%;max-width:100px;max-height:75px;height:1px;\">";
                            $content .="<a href=\"".$item->url."\" target=\"_blank\">";
                                $content .="<img src=\"".$item->img."\" style=\"width:100%;max-width:100px;max-height:75px;position: absolute;\">";
                            $content .="</a>";

                            if ($plugin_is==true OR $plugin_is=="1"){
                                $content .="<div class=\"clearfix\" style=\"position:absolute;top:-5px;left:-4px;\">";
                                    $content .="<span class=\"ow_green\" style=\"font-weight:bold;\">OK!</span>";
                                $content .="</div>";
                            }

                        $content .="</div>";
                    }

                    $content .="<div class=\"ow_left ow_ipc_info\" style=\"margin-left: 105px;    display: inline-block;position: inherit;     max-height: 60px;overflow: hidden;border-width:0;padding:1px 5px 16px 16px;;\">";
                        $content .="<a href=\"".$item->url."\" target=\"_blank\">";
                            $content .="<h1>".$title."</h1>";
                        $content .="</a>";

                        $content .="<a href=\"".$item->url."\" terget=\"_blank\">";
                            $content .=$body;
                        $content .="</a>";

                        $content .="<div class=\"ow_rightX\" style=\"margin-left:10px;display:inline;\">";
                            $content .="<a class=\"ow_lbutton\" href=\"".$item->url."\" target=\"_blank\">".$more."</a>";
                        $content .="</div>";

                    $content .="</div>";
*/


                $content .="</div>";
                $content .="</div>";


            }else if ($item->type=="ads"){//----------------------------------------------------------------ads
                $content .="<div id=\"ad_".$item->id."\" class=\"clearfix ow_border\" style=\"width:100%;margin:auto;max-height:100px;border-width:1px;min-height:82px;margin-bottom:5px;\">";
                $content .="<div class=\"ow_ipc clearfix\" style=\"margin:5px\">";


                $content .="</div>";
                $content .="</div>";                
            }






        }//if title
            }//for
        }//if

    }//if content

//Recommended
//Advertisement
//$content="";


    if ($is_chat==true){


$cc="<script>

function chatter_scrollToBottom(idmain){
var d = $(idmain);
d.animate({ scrollTop: d.prop('scrollHeight') }, 1000);
}

var timex_pingx=10000;//1000 = 1 s
//var mySetTimepingx;
var is_acces=0;
var ping_ac='checkacess';
var chatter_chat_firsttime=1;

function chatter_savAjax(msg){
        $.ajax({
            type: 'POST',
//            data: 'op=sv&ch=".$tex."&chi=".$texi."&cver=".$cversion."&cplugin=".$cplugin."&k=".$pky."&ac=sav&pp='+$('#pp').val()+'&cont='+msg,
            data: 'op=sv&ch=".$tex."&chi=".$texi."&cver=".$cversion."&cplugin=".$cplugin."&k=".$pky."&ac=sav&cont='+msg,
            url: '".$sUrl."?op=sv',
            dataType: 'json',
            crossDomain : true,
            success : function (response) {
                if (response.quest=='checkacess' && response.quest!='ok' && response.status!=undefined){

                }
                $('#ping_status').html(response.status_text);
                chatter_doAjax(ping_ac);
            },
            error:function(jXHR, textStatus, errorThrown) { 
//                $('#ping_status').html(response.status_text);
                alert('Error while sending message: '+errorThrown+'...');
            },
            complete: function() {
                $('#msg').attr('value', '');
            },
            timeout: 5000,
            cache: false
        });
    return false;
}


function chatter_doAjax(op){

        $.ajax({
            type: 'POST',
            data: 'op=ping&ch=".$tex."&chi=".$texi."&cver=".$cversion."&cplugin=".$cplugin."&k=".$pky."&ac='+op,
            url: '".$sUrl."?op=ping',
            dataType: 'json',
            crossDomain : true,
            success : function (response) {

                if (response.quest=='checkacess' && response.quest!='ok' && response.status!=undefined){
                    $('#ping_status').html(response.status_text);
                }

                        if (response.quest=='checkacess' && response.status!='ok'){
                            $('#scroll_messages').hide();
                            $('#new_message').hide();
                            $('#sbutton').hide();
                        }else{
                            $('#scroll_messages').show();
                            $('#new_message').show();
                            $('#sbutton').show();
                        }

                if (response.content && response.content!=null && response.content!=undefined){


                            if (response.type=='line'){
                                $('#support_chat_content').append(response.content);
                            }else{
                                $('#support_chat_content').html(response.content);
                            }

                            if (chatter_chat_firsttime==1){
                                chatter_scrollToBottom('#scroll_messages');
                                chatter_chat_firsttime=0;
                            }
                        

                        if (response.quest=='checkacess' && response.status=='ok'){
                            is_acces=1;
                        }else{
                            is_acces=0;
                        }

                }else{
                }

            },
            error:function(jXHR, textStatus, errorThrown) { 
            },
            complete: function() {
            },
            timeout: 5000,
            cache: false
        });



}

    $('#submsg').click(function(){       
        chatter_savAjax($('#msg').val());
    });

    $('#scroll_messages').hide();
    $('#new_message').hide();
    $('#sbutton').hide();

</script>";
OW::getDocument()->appendBody($cc);

$script="
setInterval('chatter_doAjax(ping_ac)',timex_pingx);
";
        OW::getDocument()->addOnloadScript($script);
    }

    if ($content){


/*
        if (isset($_SERVER['REQUEST_URI']) AND $_SERVER['REQUEST_URI']!="" AND $_SERVER['REQUEST_URI']!="/") {
            $add_url=$_SERVER['REQUEST_URI'];
            if (substr($add_url,0,1)=="/") $add_url=substr($add_url,1);
            if (strpos($add_url,"?")!==false){
                $add_url=substr($add_url,0,strpos($add_url,"?"));
            }
            $en_di="<a class=\"ow_lbutton\" href=\"".OW_URL_HOME.$add_url."?aopx_chat=adsdisble\">Disable this box</a>";
        }else {
            $en_di="<a class=\"ow_lbutton\" href=\"".OW_URL_HOME."admin/?aopx_chat=adsdisble\">Disable this box</a>";
        }
*/
/*
        $en_di="<a class=\"ow_lbutton\" href=\"".OW_URL_HOME."admin/?aopx_chat=adsdisble\">Disable this box</a>";

$script="
    $( 'div.ow_page_container.ow_admin>div.ow_canvas>div.ow_page h1').before( '<div class=\"clearfix\"><div class=\"ow_left\">Recommended for You:</div><div class=\"ow_right\">".$en_di."</div><div id=\"wasarrun\" class=\"clearfix ow_border\" style=\"width:100%;margin:auto;min-height:100px;  max-height:270px;  overflow-x: hidden;    overflow-y: auto;border-width: 1px;margin:5px 0 20px 0;\"><div id=\"wasarrun_content\" class=\"clerfix\">".$content."</div></div></div>');
";
    OW::getDocument()->addOnloadScript($script);
*/

        if ($is_chat==true){

            $ret.="<div class=\"clearfix\">";

                $ret.="<div class=\"ow_left ow_wide\" id=\"xrecom\">";
                    if ($is_didabled==true){
//                        $en_di="<a onclick=\"return confirm('By running this option you agree to send the developer information such as the address of your site, IP address to identify the caller. This allows viewing of recommended plugins, sometimes live chat support, etc.\\nClick OK if you want to turn it expands, nor no, if you do not agree to the above.\\n\\nYou agree to the above?');\" class=\"ow_lbutton  ow_green\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsenablere\">Enable Recommended box</a>";
                        $en_di="<a class=\"ow_lbutton  ow_green\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsenablere\">Enable Recommended box</a>";
                    }else{
                        $en_di="<a class=\"ow_lbutton ow_red\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsdisblere\">Disable this box</a>";
                    }
                    $ret .="<div class=\"clearfix\"><div class=\"ow_left\">Recommended for You:</div><div class=\"ow_right\">".$en_di."</div><div id=\"wasarrun\" class=\"clearfix ow_border\" style=\"width:100%;margin:auto;min-height:100px;  max-height:270px;  overflow-x: hidden;    overflow-y: auto;border-width: 1px;margin:5px 0 20px 0;\"><div id=\"wasarrun_content\" class=\"clerfix\">".$content."</div></div></div>";
                $ret .="</div>";

                $ret .="<div class=\"ow_left ow_narrow\" id=\"xchat\">";
                    if ($is_didabled_chat==false){
                        $ret .="<div class=\"clearfix\" style=\"margin:10px;\">";
                                $ret .="<a class=\"ow_lbutton ow_red ow_right\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsdisble\">Disable chat</a>";
                            $ret .="<h2>Live chat: <span id=\"ping_status\" class=\"ow_green\" style=\"font-size: 70%;\">connecting...</span></h2>";
                            $ret .="<div id=\"scroll_messages\" class=\"ow_border\" style=\"border-width:1px;width:100%;margin:auto;min-height:150px;height:150px;overflow-x:hidden;overflow-y:auto;margin:5px;\"><div id=\"support_chat_content\" style=\"margin:5px;\"></div></div>";
                            $ret .="<div id=\"new_message\" style=\"margin:5px;\"><textarea name=\"msg\" id=\"msg\" style=\"width:100%;height:65px;\" placeholder=\"Describe the problem with this plugin...\"></textarea></div>";

                            $ret .="<div id=\"sbutton\" style=\"margin:5px;\">";
                                $ret .="<div class=\"clearfix ow_submit ow_smallmargin\">";
//                                    $ret .="<input type=\"text\" name=\"pp\" id=\"pp\" class=\"ow_left\" style=\"width:50px;min-width:60%;margin:auto;\" placeholder=\"Your PayPal email\">";
                                    $ret .="<span class=\"ow_button ow_right\" >";
                                        $ret .="<span class=\" ow_positive\">";
                                            $ret .="<input type=\"submit\" id=\"submsg\" name=\"dosave\" value=\"Send\" class=\"ow_ic_save ow_positive\">";
                                        $ret .="</span>";
                                    $ret .="</span>";
                                $ret .="</div>";
                            $ret .="</div>";
                        $ret .="</div>";
                    }else{
                        $en_di="<a onclick=\"return confirm('By running this option you agree to send the developer information such as the address of your site, IP address to identify the caller. This allows viewing of recommended plugins, sometimes live chat support, etc.\\nClick OK if you want to turn it expands, nor no, if you do not agree to the above.You agree to the above?');\" class=\"ow_lbutton  ow_green\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsenable\">Enable Live chat</a>";
                        $ret .="<div class=\"clearfix\"><div class=\"ow_right\">".$en_di."</div></div>";

                    }
                $ret .="</div>";



            $ret .="</div>";
        }else{
            $en_di="<a class=\"ow_lbutton ow_red\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsdisblere\">Disable this box</a>";
            $ret="<div class=\"clearfix\"><div class=\"ow_left\">Recommended for You:</div><div class=\"ow_right\">".$en_di."</div><div id=\"wasarrun\" class=\"clearfix ow_border\" style=\"width:100%;margin:auto;min-height:100px;  max-height:270px;  overflow-x: hidden;    overflow-y: auto;border-width: 1px;margin:5px 0 20px 0;\"><div id=\"wasarrun_content\" class=\"clerfix\">".$content."</div></div></div>";


        }

    }else{
        if ($is_chat==true){
            $ret.="<div class=\"clearfix\">";

                $ret.="<div class=\"ow_left ow_wide\" id=\"xrecom\">";
                    if ($is_didabled==true){
//                        $en_di="<a onclick=\"return confirm('By running this option you agree to send the developer information such as the address of your site, IP address to identify the caller. This allows viewing of recommended plugins, sometimes live chat support, etc.\\nClick OK if you want to turn it expands, nor no, if you do not agree to the above.You agree to the above?');\"  class=\"ow_lbutton  ow_green\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsenablere\">Enable Recommended box</a>";
                        $en_di="<a class=\"ow_lbutton  ow_green\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsenablere\">Enable Recommended box</a>";
                    }else{
                        $en_di="<a class=\"ow_lbutton ow_red\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsdisblere\">Disable this box</a>";
                    }
                $ret .="<div class=\"clearfix\"><div class=\"ow_right\">".$en_di."</div></div>";
                $ret .="</div>";

                $ret .="<div class=\"ow_left ow_narrow\" id=\"xchat\">";
                    if ($is_didabled_chat==false){
                        $ret .="<div class=\"clearfix\" style=\"margin:10px;\">";
                                $ret .="<a class=\"ow_lbutton ow_red ow_right\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsdisble\">Disable chat</a>";
                            $ret .="<h2>Live chat: <span id=\"ping_status\" class=\"ow_green\" style=\"font-size: 70%;\">connecting...</span></h2>";
                            $ret .="<div id=\"support_chat_content\" class=\"ow_border\" style=\"border-width:1px;width:100%;margin:auto;min-height:150px;margin:5px;\"></div>";                    
                            $ret .="<div id=\"new_message\" style=\"margin:5px;\"><textarea name=\"msg\" id=\"msg\" style=\"width:100%;height:65px;\" placeholder=\"Describe the problem with this plugin...\"></textarea></div>";

                            $ret .="<div  style=\"margin:5px;\">";
                                $ret .="<div class=\"clearfix ow_submit ow_smallmargin\">";
//                                    $ret .="<input type=\"text\" name=\"pp\" id=\"pp\" class=\"ow_left\" style=\"width:50px;min-width:60%;margin:auto;\" placeholder=\"Your PayPal email\">";
                                    $ret .="<span class=\"ow_button ow_right\" >";
                                        $ret .="<span class=\" ow_positive\">";
                                            $ret .="<input type=\"submit\" id=\"submsg\" name=\"dosave\" value=\"Send\" class=\"ow_ic_save ow_positive\">";
                                        $ret .="</span>";
                                    $ret .="</span>";
                                $ret .="</div>";
                            $ret .="</div>";
                        $ret .="</div>";
                    }else{
                        $en_di="<a onclick=\"return confirm('By running this option you agree to send the developer information such as the address of your site, IP address to identify the caller. This allows viewing of recommended plugins, sometimes live chat support, etc.\\nClick OK if you want to turn it expands, nor no, if you do not agree to the above.You agree to the above?');\" class=\"ow_lbutton  ow_green\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsenable\">Enable Live chat</a>";
                        $ret .="<div class=\"clearfix\"><div class=\"ow_right\">".$en_di."</div></div>";
                    }
                $ret .="</div>";

            $ret .="</div>";
        }else{
            if ($is_didabled==true){
//                $en_di="<a onclick=\"return confirm('By running this option you agree to send the developer information such as the address of your site, IP address to identify the caller. This allows viewing of recommended plugins, sometimes live chat support, etc.\\nClick OK if you want to turn it expands, nor no, if you do not agree to the above.\\n\\nYou agree to the above?');\" class=\"ow_lbutton  ow_green\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsenablere\">Enable Recommended box</a>";
                $en_di="<a class=\"ow_lbutton  ow_green\" href=\"".OW_URL_HOME."admin/plugins/chatter?aopx_chat=adsenablere\">Enable Recommended box</a>";
                $ret="<div class=\"clearfix\"><div class=\"ow_right\">".$en_di."</div></div>";
            }
        }
    }
//echo $ret;exit;
    return $ret;
}//func

 public function ar_che_get_remote_data($url, $post_paramtrs = false) {
    if (!function_exists('curl_init') OR !function_exists('curl_exec')) return;
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    if ($post_paramtrs) {
        curl_setopt($c, CURLOPT_POST, TRUE);
        curl_setopt($c, CURLOPT_POSTFIELDS, "var1=check&" . $post_paramtrs);
    } 
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; rv:32.0) Gecko/20100105 Firefox/32.0");
    curl_setopt($c, CURLOPT_COOKIE, 'CookieNameAron=Value;');
    curl_setopt($c, CURLOPT_MAXREDIRS, 10);
    $follow_allowed = ( ini_get('open_basedir') || ini_get('safe_mode')) ? false : true;
    if ($follow_allowed) {
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
    }curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 9);
    curl_setopt($c, CURLOPT_REFERER, $url);
    curl_setopt($c, CURLOPT_TIMEOUT, 60);
    curl_setopt($c, CURLOPT_AUTOREFERER, true);
    curl_setopt($c, CURLOPT_ENCODING, 'gzip,deflate');
    $data = curl_exec($c);
    $status = curl_getinfo($c);
    curl_close($c);
    preg_match('/(http(|s)):\/\/(.*?)\/(.*\/|)/si', $status['url'], $link);
    $data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/|\/)).*?)(\'|\")/si', '$1=$2' . $link[0] . '$3$4$5', $data);
    $data = preg_replace('/(src|href|action)=(\'|\")((?!(http|https|javascript:|\/\/)).*?)(\'|\")/si', '$1=$2' . $link[1] . '://' . $link[3] . '$3$4$5', $data);
    if ($status['http_code'] == 200) {
        return $data;
    } elseif ($status['http_code'] == 301 || $status['http_code'] == 302) {
        if (!$follow_allowed) {
            if (empty($redirURL)) {
                if (!empty($status['redirect_url'])) {
                    $redirURL = $status['redirect_url'];
                }
            } if (empty($redirURL)) {
                preg_match('/(Location:|URI:)(.*?)(\r|\n)/si', $data, $m);
                if (!empty($m[2])) {
                    $redirURL = $m[2];
                }
            } if (empty($redirURL)) {
                preg_match('/href\=\"(.*?)\"(.*?)here\<\/a\>/si', $data, $m);
                if (!empty($m[1])) {
                    $redirURL = $m[1];
                }
            } if (!empty($redirURL)) {
                $t = debug_backtrace();
                return call_user_func($t[0]["function"], trim($redirURL), $post_paramtrs);
            }
        }
    } return "ERRORCODE22 with $url!!<br/>Last status codes<b/>:" . json_encode($status) . "<br/><br/>Last data got<br/>:$data";
 }//f


}
