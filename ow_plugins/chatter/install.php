<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * This plugin should be bought from the developer by paying money to PayPal account: biuro@grafnet.pl
 * Legal purchase is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


OW::getLanguage()->importPluginLangs(OW::getPluginManager()->getPlugin('chatter')->getRootDir().'langs.zip', 'chatter');
OW::getPluginManager()->addPluginSettingsRouteName('chatter', 'chatter.admin');

//BOL_LanguageService::getInstance()->addPrefix('contactus', 'Contact Us');

$config = OW::getConfig();
if ( !$config->configExists('chatter', 'chatter_enable') ){
    $config->addConfig('chatter', 'chatter_enable', '1', '');
}
if ( !$config->configExists('chatter', 'show_for_who') ){
    $config->addConfig('chatter', 'show_for_who', '0', '');
}
if ( !$config->configExists('chatter', 'feature') ){
    $config->addConfig('chatter', 'feature', '0', '');
}
if ( !$config->configExists('chatter', 'max_history_back') ){
    $config->addConfig('chatter', 'max_history_back', '500', '');
}

if ( !$config->configExists('chatter', 'mode_perpage') ){
    $config->addConfig('chatter', 'mode_perpage', '50', '');
}

if ( !$config->configExists('chatter', 'mode_perpage_rooms') ){
    $config->addConfig('chatter', 'mode_perpage_rooms', '50', '');
}

if ( !$config->configExists('chatter', 'display_in_menu') ){
    $config->addConfig('chatter', 'display_in_menu', '1', '');
}
if ( !$config->configExists('chatter', 'display_in_console') ){
    $config->addConfig('chatter', 'display_in_console', '0', '');
}
if ( !$config->configExists('chatter', 'disable_sound') ){
    $config->addConfig('chatter', 'disable_sound', '1', '');
}

if ( !$config->configExists('chatter', 'bg_card_color') ){
    $config->addConfig('chatter', 'bg_card_color', '#9A9A9A', '');
}


$authorization = OW::getAuthorization();
$groupName = 'chatter';
$authorization->addGroup($groupName);

$authorization->addAction($groupName, 'roomscreate');

$authorization->addAction($groupName, 'roomsread');
$authorization->addAction($groupName, 'roomswrite');

$authorization->addAction($groupName, 'publicread',true);
$authorization->addAction($groupName, 'publicwrite',true);


/*
if ( !$config->configExists('chatter', 'chatter_protect_key') ){
    $key=substr(md5(date('d-m-Y H:i:s')),0,10);
    $config->addConfig('chatter', 'chatter_protect_key', $key, 'Protect Key');
}

if ( !$config->configExists('chatter', 'chatter_generateforusers') ){
    $config->addConfig('chatter', 'chatter_generateforusers', '1', 'Site Map for members too');
}

if ( !$config->configExists('chatter', 'chatter_max_items_eachplugin') ){
    $config->addConfig('chatter', 'chatter_max_items_eachplugin', '10', 'Max items for each plugins');
}
if ( !$config->configExists('chatter', 'chatter_max_items_sitemap') ){
    $config->addConfig('chatter', 'chatter_max_items_sitemap', '9999', 'Max items sitemap');
}
*/

$sql="DROP TABLE IF EXISTS `" . OW_DB_PREFIX . "chatter_chat`;";
OW::getDbo()->query($sql);
$sql="CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "chatter_chat` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) NOT NULL,
  `is_fake` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '0',
  `id_sender` int(11) NOT NULL,
  `id_recipment` int(11) NOT NULL,
  `is_readed` enum('0','1','2','3') COLLATE utf8_bin NOT NULL DEFAULT '0',
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `id_item` (`id_item`),
  KEY `date` (`post_date`),
  KEY `id_recipment` (`id_recipment`),
  KEY `id_sender` (`id_sender`),
  KEY `is_fake` (`is_fake`),
  KEY `is_readed` (`is_readed`),
  FULLTEXT KEY `content` (`content`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;";
OW::getDbo()->query($sql);

$sql="DROP TABLE IF EXISTS `" . OW_DB_PREFIX . "chatter_plugins`;";
OW::getDbo()->query($sql);
$sql="CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "chatter_plugins` (
  `id` bigint(22) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) COLLATE utf8_bin NOT NULL,
  `is_banned` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '0',
  `is_readed_byadmin` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `host` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `host_post` varchar(120) COLLATE utf8_bin DEFAULT NULL,
  `plugin` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `allow_chat` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '0',
  `chat_id_user` int(11) NOT NULL DEFAULT '0',
  `items` int(11) NOT NULL,
  `first_date` datetime NOT NULL,
  `last_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `licenseKey` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `pp_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip` (`ip`,`host_post`,`plugin`),
  KEY `allow_chat` (`allow_chat`),
  KEY `chat_id_user` (`chat_id_user`),
  KEY `ip_2` (`ip`),
  KEY `host` (`host`),
  KEY `plugin` (`plugin`),
  KEY `chat_id_user_2` (`chat_id_user`),
  KEY `licenseKey` (`licenseKey`),
  KEY `host_post` (`host_post`),
  KEY `pp_email` (`pp_email`),
  KEY `is_banned` (`is_banned`),
  KEY `is_readed_byadmin` (`is_readed_byadmin`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;";
OW::getDbo()->query($sql);

$sql="DROP TABLE IF EXISTS `" . OW_DB_PREFIX . "chatter_rooms`;";
OW::getDbo()->query($sql);
$sql="CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "chatter_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `id_owner` int(11) NOT NULL,
  `id_language` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_bin NOT NULL,
  `description` varchar(255) COLLATE utf8_bin NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `on_passwd` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '0',
  `paswd` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_language` (`id_language`),
  KEY `active` (`active`),
  KEY `id_owner` (`id_owner`),
  KEY `created` (`created`),
  KEY `on_passwd` (`on_passwd`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;";
OW::getDbo()->query($sql);

