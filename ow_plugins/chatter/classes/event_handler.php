<?php

class CHATTER_CLASS_EventHandler
{
    private static $classInstance;

    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    private function __construct() { }

    public function addAdminNotification( BASE_CLASS_EventCollector $coll )
    {
    }

    public function addAccessException( BASE_CLASS_EventCollector $e )
    {
//        $e->add(array('controller' => 'CHATTER_CTRL_Chatter','action' => 'index'));
        $e->add(array('controller' => 'CHATTER_CTRL_Chatter','action' => 'index'));
        $e->add(array('controller' => 'CHATTER_CTRL_Chatter','action' => 'indexp'));
        $e->add(array('controller' => 'CHATTER_CTRL_Chatter','action' => 'indexaj'));
    }

    public function init()
    {

//        if (OW::getPluginManager()->isPluginActive('shoppro')){
            OW::getEventManager()->bind('console.collect_items', array($this, 'onCollectConsoleItems'));
//        OW::getEventManager()->bind('console.load_list', array($this, 'onLoadConsoleList'));
//        }

        $em = OW::getEventManager();
//        $em->bind('admin.add_admin_notification', array($this, 'addAdminNotification'));
        $em->bind('base.members_only_exceptions', array($this, 'addAccessException'));
        $em->bind('base.password_protected_exceptions', array($this, 'addAccessException'));
        $em->bind('base.splash_screen_exceptions', array($this, 'addAccessException'));
    }


//--mmmenu
    public function onCollectConsoleItems( BASE_CLASS_ConsoleItemCollector $event )
    {
        if (OW::getUser()->isAuthenticated())
//        if (OW::getUser()->isAdmin() AND OW::getConfig()->getValue('chatter', 'feature')=="1")
//        if (OW::getUser()->isAdmin())
        {

            if (OW::getConfig()->getValue('chatter', 'display_in_console')=="1"){
                $item = new CHATTER_CMP_ConsoleChatter();
                $event->addItem($item, 100);
            }
        }
    }

}