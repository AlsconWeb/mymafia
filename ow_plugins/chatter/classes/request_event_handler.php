<?php

class CHATTER_CLASS_RequestEventHandler
{
    /**
     * Class instance
     *
     * @var FRIENDS_CLASS_RequestEventHandler
     */
    private static $classInstance;

    /**
     * Returns class instance
     *
     * @return FRIENDS_CLASS_RequestEventHandler
     */
    public static function getInstance()
    {
        if ( !isset(self::$classInstance) )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    const CONSOLE_ITEM_KEY = 'chat_notyfications';

    /**
     *
     * @var FRIENDS_BOL_Service
     */
    private $service;
    private $last_date=false;

    private function __construct()
    {
        $this->service = CHATTER_BOL_Service::getInstance();
        $this->last_date=date('Y-d-m :H:i:s');
    }

    public function collectItems( BASE_CLASS_ConsoleItemCollector $event )
    {
        if (OW::getUser()->isAuthenticated())
        {

            $item = new CHATTER_CMP_ConsoleChatterNotyfications();
//            $count = $this->service->count(null, OW::getUser()->getId(), FRIENDS_BOL_Service::STATUS_PENDING);
            
            $count = $this->service->get_new_count($this->last_date);
//            $this->last_date=date('Y-d-m :H:i:s');
//            $this->last_date=date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." -5 minutes"));
            $this->last_date=date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))-(60*5));

//            $count =0;
            if (!$count )
            {
                $item->setIsHidden(true);
            }

            $event->addItem($item, 5);
        }
    }

    /* Console list */

    public function ping( BASE_CLASS_ConsoleDataEvent $event )
    {
        $userId = OW::getUser()->getId();
        $data = $event->getItemData(self::CONSOLE_ITEM_KEY);

//        $newInvitationCount = $this->service->count(null, $userId, FRIENDS_BOL_Service::STATUS_PENDING, null, false);
        $newInvitationCount = $this->service->get_new_count($this->last_date);
//        $this->last_date=date();
//        $this->last_date=date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." -5 minutes"));
        $this->last_date=date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))-(60*5));


//        $newInvitationCount =9;

        $data['counter'] = array(
            'all' => 0,
            'new' => $newInvitationCount
        );
//$event->setIsHidden(true);
        $event->setItemData('chat_notyfications', $data);
    }

    public function loadList( BASE_CLASS_ConsoleListEvent $event )
    {
        $params = $event->getParams();
        $userId = OW::getUser()->getId();
return;
        if ( $params['target'] != self::CONSOLE_ITEM_KEY )
        {
            return;
        }


        $requestIds = array();
        $this->service->markViewedByIds($requestIds);
    }

    public function init()
    {
        OW::getEventManager()->bind('console.collect_items', array($this, 'collectItems'));
        OW::getEventManager()->bind('console.ping', array($this, 'ping'));
        OW::getEventManager()->bind('console.load_list', array($this, 'loadList'));
    }
}