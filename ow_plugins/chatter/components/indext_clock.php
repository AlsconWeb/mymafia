<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * This plugin should be bought from the developer by paying money to PayPal account: biuro@grafnet.pl
 * Legal purchase is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://test.a6.pl
 * Full license available at: http://test.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


class CHATTER_CMP_IndextClock extends BASE_CLASS_Widget
{

    public function __construct( BASE_CLASS_WidgetParameter $params )
    {
        parent::__construct();
/* 
        $params = $paramObject->customParamList;
 
        if ( !empty($params['content']) )
        {
            $this->content = $paramObject->customizeMode && !empty($_GET['disable-js']) ? UTIL_HtmlTag::stripJs($params['content']) : $params['content'];
        }
 
        if ( isset($params['nl_to_br']) )
        {
            $this->nl2br = (bool) $params['nl_to_br'];
        }
*/
    }

    public static function getSettingList()
    {

        $options = array();
        $settingList=array();
/*
        for ( $i = 3; $i <= 10; $i++ )
        {
            $options[$i] = $i;
        }

        $settingList['count'] = array(
            'presentation' => self::PRESENTATION_SELECT,
            'label' => OW::getLanguage()->text('shoppro', 'cmp_widget_post_count'),
            'optionList' => $options,
            'value' => 3,
        );
        $settingList['previewLength'] = array(
            'presentation' => self::PRESENTATION_TEXT,
            'label' => OW::getLanguage()->text('shoppro', 'blog_widget_preview_length_lbl'),
            'value' => 50,
        );
*/
        return $settingList;
    }

    public static function getStandardSettingValueList()
    {
        $list = array(
            self::SETTING_TITLE => OW::getLanguage()->text('chatter', 'txw_clock'),
            self::SETTING_SHOW_TITLE => true,
                self::SETTING_WRAP_IN_BOX => true,
            self::SETTING_ICON => 'ow_ic_write'
        );

        return $list;
    }

    public static function getAccess()
    {
        return self::ACCESS_ALL;
    }
	
    public function onBeforeRender() // The standard method of the component that is called before rendering
    {
            $content="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
//                    $curent_url .= $_SERVER["SERVER_NAME"]."/";
        $curent_url=OW_URL_HOME; 
        $pluginStaticU=OW::getPluginManager()->getPlugin('nicemenu')->getStaticUrl();
//        $pluginStaticD=OW::getPluginManager()->getPlugin('startpage')->getStaticDir();


//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/chatter/ext/jquery.movingboxes.min.js');
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/chatter/ext/slidershop.js?'.rand(100,500));
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/chatter/ext/slidershop.js?a=1');


//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/chatter/w_clock/jquery.tzineClock.js?a=1');
//        OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/chatter/w_clock/jquery.tzineClock.css');

        OW::getDocument()->addScript($pluginStaticU.'nicemenu.js');
        OW::getDocument()->addScript($pluginStaticU.'js'.DS.'bootstrap.min.js');

        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap-responsive.css');
        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap.css');
//        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap-responsive.min.css');
//        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap.min.css');

/*
$script="
<script>
$(document).ready(function() {
    $('#fancyClock').tzineClock();

});
</script>
";
//$content.=$script;
OW::getDocument()->addOnloadScript($script);
*/


		$this->assign('content', $content);
	}
	
}

