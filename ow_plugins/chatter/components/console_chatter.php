<?php

/**
 * This software is intended for use with Oxwall Free Community Software http://www.oxwall.org/ and is
 * licensed under The BSD license.

 * ---
 * Copyright (c) 2011, Oxwall Foundation
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 * following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice, this list of conditions and
 *  the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *  the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  - Neither the name of the Oxwall Foundation nor the names of its contributors may be used to endorse or promote products
 *  derived from this software without specific prior written permission.

 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @author Podyachev Evgeny <joker.OW2@gmail.com>
 * @package ow_plugin.mailbox.components
 * @since 1.0
 * */

class CHATTER_CMP_ConsoleChatter extends BASE_CMP_ConsoleDropdownClick
{
    protected $counter = array(
        'number' => 0,
        'active' => false
    );

    /**
     *
     * @var BASE_CMP_ConsoleList
     */
    protected $list;

    public function __construct()
    {



$pluginStaticU=OW::getPluginManager()->getPlugin('chatter')->getStaticUrl();
//$pluginStaticD=OW::getPluginManager()->getPlugin('startpage')->getStaticDir();
//$plname="cart";
//        $source=OW_DIR_PLUGIN.$plname. DS.'static'. DS;
//        $pluginStaticDir = OW_DIR_STATIC .'plugins'.DS.$plname.DS;
//$ico="<div style=\"background:transparent  url(".$pluginStaticU."Cart.png) center center;

//$ico="<div style=\"background:transparent  url(".$pluginStaticU."icon_cart.gif) center center;

/*
$ico="<div style=\"background:transparent  url(".$pluginStaticU."icon-star-red.png) center center;
width: 18px;
height: 18px;
display: inline-block;
vertical-align: middle;
border:0;
margin: 2px 10px 0px -5px;
\"></div>";
*/
$ico="<div class=\"ow_section_icon ow_ic_mail\" style=\"
background-repeat: no-repeat;
background-position: center;
padding:0;
width: 18px;
height: 18px;
display: inline-block;
vertical-align: middle;
border:0;
margin: 2px 10px 0px -5px;
\"></div>";

//        parent::__construct( OW::getLanguage()->text('cart', 'messages_console_title'), 'cart' );
        parent::__construct( $ico.OW::getLanguage()->text('chatter', 'chatter_console'), 'chatter' );

//        $template = OW::getPluginManager()->getPlugin('search')->getCmpViewDir() . 'console_dropdown_list.html';
        $template = OW::getPluginManager()->getPlugin('chatter')->getCmpViewDir() . 'console_chatter.html';
        $this->setTemplate($template);
//echo $template;exit;

/*
        $this->list = new MAILBOX_CMP_ConsoleList($this->getKey());
        $this->addClass('ow_mailbox_items_list');

        $toolbar = new MAILBOX_CMP_Toolbar();
        OW::getDocument()->appendBody($toolbar->render());
*/
//        $this->list = new CART_CMP_ConsoleList($this->getKey());
//        $this->addClass('ow_mailbox_items_list');

//        OW::getDocument()->appendBody("sdfgsdfsdfsdf");
    }
/*
    protected function initJs()
    {
        $js = UTIL_JsGenerator::newInstance();
        $js->addScript('OW.Console.addItem(new OW_ConsoleDropdownList({$uniqId}, {$contentIniqId}), {$key});', array(
            'uniqId' => $this->consoleItem->getUniqId(),
            'key' => $this->getKey(),
            'contentIniqId' => $this->consoleItem->getContentUniqId()
        ));
        OW::getDocument()->addOnloadScript($js);
        $this->list->initJs();

        $js = UTIL_JsGenerator::newInstance();

        $js->addScript(
            'OW.MailboxConsole = new OW_MailboxConsole({$key}, {$params});',
            array(
                'key' => $this->getKey(),
                'params' => array(
                    'issetMails' => (boolean) MAILBOX_BOL_ConversationService::getInstance()->getInboxConversationList(OW::getUser()->getId(), 0, 1)
                )
        ));

        OW::getDocument()->addOnloadScript($js);

        return $this->consoleItem->getUniqId();
    }
*/

    public function setCounter( $number, $active = true )
    {
//        $this->counter['number'] = $number;
//        $this->counter['active'] = $active;
        $this->counter['number'] = $number;
        $this->counter['active'] = $active;
    }

    public function onBeforeRender()
    {
    $content="";





//        $content .="<div class=\"clearfix ow_console_invt_txtX\" style=\"width:100%;margin:auto;\">";//-------------------------------------------------------div start
//        $content .="<div class=\"clearfix ow_console_invt_cont\" style=\"width:100%;margin:auto;\">";//-------------------------------------------------------div start

//        $content .="<div style=\"margin:10px;\">";//-------------------------------------------------------div start 2

        parent::onBeforeRender();

///        $this->assign('counter', $this->counter);
//        $this->setContent($this->list->render());
//        $this->setContent(2,false);
//$this->setContent('ffff');
//$content .="<input type=\"text\" value=\"".$this->getKey()."\">";




$curent_url=OW_URL_HOME;
$config = OW::getConfig();
$id_user = OW::getUser()->getId();
$is_admin = OW::getUser()->isAdmin();

//    $this->setCounter( $alli,true);
        $this->assign('curent_url_chatter', $curent_url."chatter");

//function current_url() {
    //return sprintf("http://%s%s",$_SERVER["HTTP_HOST"],$_SERVER["REQUEST_URI"]);
  //}



    $language = OW::getLanguage();
//    $router = OW_Router::getInstance();
        $id_user = OW::getUser()->getId();//citent login user (uwner)
//        $is_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
        $curent_url=OW_URL_HOME;
    if (!$id_user) return;

//        OW::getDocument()->addOnloadScript($script);//


    $sUrl=$curent_url."chatter/ping/".substr(session_id(),2,5);
/*
            data: 'op=svmadm&room_id=".$id_room."&id_faq=".$is_faq."&cplugin=".$cplugin."&ss=".substr(session_id(),2,5)."&last_id='+$('#last_id').val()+'&ac=pingm&type=".$type."',
*/

$js="<script>
var chatter_last_numberitems=0;

function chatter_ping_play_s_admin(nn){
    $('#chatter_ping_'+nn).remove();
    $('body').append('<embed id=\"#chatter_ping_'+nn+'\" src=\"".OW_URL_HOME."ow_static/plugins/chatter/m.mp3\" autostart=\"true\" hidden=\"true\" loop=\"false\">');
}

function chatter_ping_doAjaxPingAdmin(){
        $.ajax({
            type: 'POST',
            data: 'op=chatterping&uid=".$id_user."&&ss=".substr(session_id(),2,5)."&last_no='+chatter_last_numberitems+'&ac=pingm&last_id='+$('#chater_last_id').val(),
            url: '".$sUrl."?op=chatterping',
            dataType: 'json',
            crossDomain : true,
            success : function (response) {

                if (response.numberitems!=undefined && response.numberitems>0){

                    if (response.numberitems>chatter_last_numberitems){
//alert('ssss'+response.numberitems+'---'+chatter_last_numberitems);
                        $('#chatter_ping_below').parent().find('.OW_ConsoleItemCounterNumber').html(response.numberitems);
                        $('#chatter_ping_below').parent().find('.OW_ConsoleItemCounter').css('display','inline-block');
//                        $('#chatter_ping_below').parent().css('display','inline-block');
                        chatter_ping_play_s_admin('chatter_incoming_admn');
                    }else{
                        $('#chatter_ping_below').parent().find('.OW_ConsoleItemCounterNumber').html(response.numberitems);
                        $('#chatter_ping_below').parent().find('.OW_ConsoleItemCounter').css('display','inline-block');
                    }
                }else{//error
//                    $('#chatter_ping_below').parent().css('display','none');
                    $('#chatter_ping_below').parent().find('.OW_ConsoleItemCounterNumber').html('0');
                    $('#chatter_ping_below').parent().find('.OW_ConsoleItemCounter').css('display','none');
                }




                if (response.lastid!=undefined){
                    $('#chater_last_id').val(response.lastid);
                }

                if (response.numberitems!=undefined){
                    chatter_last_numberitems=response.numberitems;
                }
//alert('ssss'+response.numberitems+'---'+chatter_last_numberitems);
            },
            error:function(jXHR, textStatus, errorThrown) { 
            },
            complete: function() {
            },
            timeout: 5000,
            cache: false
        });



}


//$('#chatter_ping_below').parent().css('display','none');
$('#chatter_ping_below').parent().find('.OW_ConsoleItemCounterNumber').html('0');
$('#chatter_ping_below').parent().find('.OW_ConsoleItemCounter').css('display','none');

</script>";

OW::getDocument()->appendBody($js);



$script="
    setInterval('chatter_ping_doAjaxPingAdmin()',5000);
";
if ($is_admin AND OW::getConfig()->getValue('chatter', 'feature')=="1"){
    OW::getDocument()->addOnloadScript($script);
}



        $alli=0;
        $this->setCounter( $alli,true);
        $this->setContent($content);

        $this->assign('counter', $this->counter);
        $this->assign('content', $content);
    }
}