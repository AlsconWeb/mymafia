<?php


class CHATTER_CMP_ConsoleChatterNotyfications extends BASE_CMP_ConsoleDropdownList
{
    public function __construct()
    {
        parent::__construct( OW::getLanguage()->text('chatter', 'console_notyfications_title'), 'chat_notyfications' );


        $this->addClass('ow_chatter_notyfications');
    }

    public function initJs()
    {
        parent::initJs();

        $jsUrl = OW::getPluginManager()->getPlugin('chatter')->getStaticJsUrl() . 'notyfications.js';
        OW::getDocument()->addScript($jsUrl);

        $js = UTIL_JsGenerator::newInstance();
        $js->addScript('OW.ChatterNotyfication = new OW_ChatterNotyfications({$key}, {$params});', array(
            'key' => $this->getKey(),
            'params' => array(
                'rsp' => OW::getRouter()->urlFor('CHATTER_CTRL_Chatter', 'ajax')
            )
        ));

        OW::getDocument()->addOnloadScript($js);

        $js="
            $('.ow_chatter_notyfications').click(function(){
                location.href = '".OW_URL_HOME."chatter';
                $('.ow_chatter_notyfications').hide();
            });
        ";
        OW::getDocument()->addOnloadScript($js);

    }
}