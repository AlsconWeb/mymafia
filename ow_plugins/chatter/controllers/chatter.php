<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * This plugin should be bought from the developer by paying money to PayPal account: biuro@grafnet.pl
 * Legal purchase is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/



class CHATTER_CTRL_Chatter extends OW_ActionController
{

    public function index($params)
    {

        $curent_url=OW_URL_HOME;
$output_chatter2="";
$content="";
       $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
        $curent_url=OW_URL_HOME;

        $this->setPageTitle(OW::getLanguage()->text('chatter', 'main_menu_item')); //title menu
        $this->setPageHeading(OW::getLanguage()->text('chatter', 'main_menu_item')); //title page

        if (!isset($params)) $params=array();
        if (!isset($params['com'])) $params['com']="";
        if (!isset($params['op'])) $params['op']="";
        if (!isset($params['action'])) $params['action']="";
        if (!isset($params['ss'])) $params['ss']="";
        if (!isset($params['plug'])) $params['plug']="";
        if (!isset($params['idroom'])) $params['idroom']=0;
        if (!isset($params['isfaq'])) $params['isfaq']=false;
        if (!isset($params['idlogin'])) $params['idlogin']=0;
//print_r($params);exit;
        $tab="index";
        $tab_rname="";
//         OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/shoppro/extr/jquery.raty.min.js');
//        OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/chatter/bootstrap.min.css');
//        OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/chatter/css.css');


//        CHATTER_BOL_Service::getInstance()->pinok();//for tab

        $content .="<div class=\"clearfix\">";

//print_r($params);exit;
//http://www.oxwall.a6.pl/chatter/action/edit/2/b206f
//                        chatter/action/:action/:op/:ss'
//echo $params['idlogin']."--".$params['ss']."--".substr(session_id(),2,5);exit;
        if ($params['idlogin']>0 AND $params['ss']==substr(session_id(),2,5)){

                    if (isset($_POST['logdone']) AND $_POST['logdone']=="login" AND isset($_POST['passwd']) AND $_POST['passwd']!="" AND isset($_POST['ss']) AND $_POST['ss']==substr(session_id(),2,5)){
                        if (CHATTER_BOL_Service::getInstance()->login($params['idlogin'],$_POST['passwd'],"login")){
                            OW::getFeedback()->info(OW::getLanguage()->text('chatter', 'login_succesfull'));
                            OW::getApplication()->redirect($curent_url."chatter/room/".$params['idlogin']);
                            exit;
                        }else{
                            OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                            OW::getApplication()->redirect($curent_url."chatter/rooms/show");
                            exit;
                        }
                    }

                    $content .="<form method=\"post\" action=\"".$curent_url."chatter/login/".$params['idlogin']."/".substr(session_id(),2,5)."\" >";
                        $content .="<input type=\"hidden\" name=\"ss\" value=\"".substr(session_id(),2,5)."\">";
                        $content .="<input type=\"hidden\" name=\"logdone\" value=\"login\">";
                        $content .="<table class=\"ow_table1 ow_form\" style=\"width:100%;margin:auto;\">";
                        $content .="<tr>";
                            $content .="<th colspan=\"2\">";
                                $content .=OW::getLanguage()->text('chatter', 'enter_password_for_room');
                            $content .="</th>";
                        $content .="</tr>";

                        $content .="<tr>";
                            $content .="<td class=\"ow_label\">";
                                $content .=OW::getLanguage()->text('chatter', 'password');
                            $content .="</td>";
                            $content .="<td  class=\"ow_value\">";
                                $content .="<input type=\"password\" name=\"passwd\" value=\"\" style=\"max-width:200px;\">";
                            $content .="</td>";
                        $content .="</tr>";

        $content .="<tr>";
        $content .="<td colspan=\"2\">";
//        $content .="<input type=\"submit\" name=\"dosave\" value=\"".OW::getLanguage()->text('chatter', 'admin_save')."\">";
        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('chatter', 'login')."\" name=\"dosave\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
        $content .="</td>";
        $content .="</tr>";

                        $content .="</table>";
                    $content .="</form>";

        }else if ($params['action']!="" AND $params['op']!="" AND $params['ss']!="" AND $params['ss']==substr(session_id(),2,5) AND $id_user>0){

            if ($params['action']=="delpost" AND $params['op']!=""){
//                if (!$is_admin AND !OW::getUser()->isAuthorized('chatter', 'roomscreate')){
                if (!$id_user){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }
//                $tab="newroom";
//                $content .=CHATTER_BOL_Service::getInstance()->edit_room_form(0);
                $parx=explode("_",$params['op']);
                if (!$parx[1]) $parx[1]=0;

                if (isset($parx[0]) AND isset($parx[1]) AND $parx[0]>0 AND CHATTER_BOL_Service::getInstance()->allow_moderate_room($id_user, $parx[1])){

                    $sql="DELETE FROM " . OW_DB_PREFIX. "chatter_chat WHERE id='".addslashes($parx[0])."' AND id_item='".addslashes($parx[1])."' AND is_fake='0' LIMIT 1";
                    OW::getDbo()->query($sql);

                    OW::getFeedback()->info(OW::getLanguage()->text('chatter', 'deleted_succesfull'));
                    if ($parx[1]>0){
                        OW::getApplication()->redirect($curent_url."chatter/room/".$parx[1]);
                    }else{
                        OW::getApplication()->redirect($curent_url."chatter");
                    }
                    exit;
                }else{
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }
            }else if ($params['action']=="newroom" AND $params['op']=="create"){
                if (!$is_admin AND !OW::getUser()->isAuthorized('chatter', 'roomscreate')){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }
                $tab="newroom";
                $content .=CHATTER_BOL_Service::getInstance()->edit_room_form(0);
            }else if ($params['action']=="edit" AND $params['op']>0){
                if (!$is_admin AND !OW::getUser()->isAuthorized('chatter', 'roomscreate')){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }
                $tab="editroom";
                $content .=CHATTER_BOL_Service::getInstance()->edit_room_form($params['op']);
            }else if ($params['action']=="del" AND $params['op']>0){
                if (!$is_admin AND !OW::getUser()->isAuthorized('chatter', 'roomscreate')){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }

                $content .="DEL TODO ";
                $add="";
                if (!$is_admin) $add=" AND id_owner='".addslashes($id_user)."' ";

                $sql="DELETE FROM " . OW_DB_PREFIX. "chatter_rooms WHERE id='".addslashes($params['op'])."' ".$add." LIMIT 1";
                OW::getDbo()->query($sql);
                $sql="DELETE FROM " . OW_DB_PREFIX. "chatter_chat WHERE id_item='".addslashes($params['op'])."' AND is_fake='0' ";
                OW::getDbo()->query($sql);
                    OW::getFeedback()->info(OW::getLanguage()->text('chatter', 'deleted_succesfull'));
                    OW::getApplication()->redirect($curent_url."chatter/rooms/show");
                    exit;
            }else{
                OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                OW::getApplication()->redirect($curent_url);
                exit;
            }
        }else if ($params['op']=="show"){
            if (!$is_admin AND (!OW::getUser()->isAuthorized('chatter', 'roomsread') AND !OW::getUser()->isAuthorized('chatter', 'roomswrite')) ){
                OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                OW::getApplication()->redirect($curent_url);
                exit;
            }

            OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/chatter/bootstrap.min.css');
            OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/chatter/css2.css');
            $tab="rooms";
            $content .=CHATTER_BOL_Service::getInstance()->rooms();
        }else{
            OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/chatter/css.css');

            if ($params['op']=="support"){
                if (!$is_admin){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }
                $tab="support";
                $tab_rname=CHATTER_BOL_Service::getInstance()->get_room_name($params['idroom']);
            }else if ($params['idroom']>0 AND $params['isfaq']!=false){
                if (!$is_admin AND (!OW::getUser()->isAuthorized('chatter', 'roomsread') AND !OW::getUser()->isAuthorized('chatter', 'roomswrite')) ){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }
                $tab="roomp";
                $tab_rname=OW::getLanguage()->text('chatter', 'chat_plugin');
            }else if ($params['idroom']>0){
                if (!$is_admin AND (!OW::getUser()->isAuthorized('chatter', 'roomsread') AND !OW::getUser()->isAuthorized('chatter', 'roomswrite')) ){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }
                $tab="room";
                $tab_rname=CHATTER_BOL_Service::getInstance()->get_room_name($params['idroom']);
            }else{
                if (!$is_admin AND (!OW::getUser()->isAuthorized('chatter', 'publicread') AND !OW::getUser()->isAuthorized('chatter', 'publicwrite')) ){
                    OW::getFeedback()->error(OW::getLanguage()->text('chatter', 'you_dont_have_permission'));
                    OW::getApplication()->redirect($curent_url);
                    exit;
                }
                $tab="chat";
            }
            $content .=CHATTER_BOL_Service::getInstance()->chat($params['idroom'],$params['isfaq'],$tab);
        }
        $content .="</div>";

        $content =CHATTER_BOL_Service::getInstance()->make_tabs($tab,$content,$tab_rname);
	$this->assign('content', $content);
    }//end index

    public function indexp($params)//ping
    {
        if (!isset($params)) $params=array();
        if (!isset($params['ss'])) $params['ss']="";
        if ($params['ss']==substr(session_id(),2,5)){

            $items=CHATTER_BOL_Service::getInstance()->ping_check();
            $json_arr=array(
                'numberitems'=> $items['items'],
                'lastid'=>$items['last'],
                'status' => 'ok',
                'content'  => ''
            );
        }else{
            $json_arr=array(
                'numberitems'=> '',
                'lastid'=>'',
                'status' => 'error',
                'content'  => ''
            );
        }

//        header('Content-Type: application/json');
        header('Content-Type:application/json;charset=utf-8');
        echo json_encode( $json_arr );
        exit;

    }

    public function indexaj($params)
    {


        $curent_url=OW_URL_HOME;
        $output_chatter2="";
        $content="";
        if (!isset($params)) $params=array();
        if (!isset($params['plug'])) $params['plug']="";

        if ($params['plug']=="") {
            $json_arr=array(
                'status' => 'error',
                'status_text' => 'Update plugin!',
                'quest' => $_POST['ac'],
                'type' => 'line',
                'content'  => '',
            );
            echo json_encode( $json_arr );
            exit;
        }

        CHATTER_BOL_Service::getInstance()->do_res_aj($params['plug']);
        exit;
        $this->assign('content', $content);
    }


    public function make_item($arrx,$tagmain="item")
    {
        $ret ="";
        foreach ($arrx as $nam=>$val){
            if ($nam AND $val){
                $ret .="<".$nam.">".$val."</".$nam.">\n";
            }
        }
        if ($ret AND $tagmain){
            $ret="<".$tagmain.">\n".$ret."</".$tagmain.">\n";
        }
        return $ret;
    }

    public function sent()
    {

    }
/*
     public function html2txt($document){
        $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
        );
        $text = preg_replace($search, '', $document);
        $text=preg_replace("/(&?!amp;)/i", " ", $text);
        $text=preg_replace("/(&#\d+);/i", " ", $text); // For numeric entities
        $text=preg_replace("/(&\w+);/i", " ", $text); // For literal entities
        return $text;
    }
*/
    public function make_seo_url($name,$lengthtext=100)
    {
        $seo_title=stripslashes($name);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace(chr(160),"_",$seo_title);
        $seo_title=str_replace("~","",$seo_title);
        $seo_title=str_replace("(","",$seo_title);
        $seo_title=str_replace(")","",$seo_title);
        $seo_title=str_replace("]","",$seo_title);
        $seo_title=str_replace("[","",$seo_title);
        $seo_title=str_replace("}","",$seo_title);
        $seo_title=str_replace("{","",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("\\","",$seo_title);
        $seo_title=str_replace("+","",$seo_title);
        $seo_title=str_replace(":","",$seo_title);
        $seo_title=str_replace(";","",$seo_title);
        $seo_title=str_replace("\"","",$seo_title);
        $seo_title=str_replace("<","",$seo_title);
        $seo_title=str_replace(">","",$seo_title);
        $seo_title=str_replace("?","",$seo_title);
        $seo_title=str_replace(",",".",$seo_title);
        $seo_title=str_replace("!","",$seo_title);
        $seo_title=str_replace("`","",$seo_title);
        $seo_title=str_replace("'","",$seo_title);
        $seo_title=str_replace("@","",$seo_title);
        $seo_title=str_replace("#","",$seo_title);
        $seo_title=str_replace("$","",$seo_title);
        $seo_title=str_replace("%","",$seo_title);
        $seo_title=str_replace("^","",$seo_title);
        $seo_title=str_replace("&","",$seo_title);
        $seo_title=str_replace("*","",$seo_title);
        $seo_title=str_replace("|","",$seo_title);
        $seo_title=str_replace("=","",$seo_title);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("?","_",$seo_title);
        $seo_title=str_replace("#","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("&amp;","_",$seo_title);
        $seo_title=str_replace("__","_",$seo_title);
        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }


    private function make_xml_friendly( $url,$replay=" " )
    {
        $find = array(' ',
      '&quot;',
      'quot',
      '&',
      'amp;',
      '\r\n',
      '\n',
      '/',
      '\\',
      '+',
      '<',
      '>');
       $url = str_replace ($find, $replay, $url);
        return $url;
    }
    private function make_url_friendly( $url,$replay="-" )
    {
        $url = trim($url);
        $url = strtolower($url);
        $find = array('<b>','</b>');
        $url = str_replace ($find, '', $url);
        $url = preg_replace('/<(\/{0,1})img(.*?)(\/{0,1})\>/', 'image', $url);
        $find = array(' ',
      '&quot;',
      'quot',
      '&',
      'amp;',
      '\r\n',
      '\n',
      '/',
      '\\',
      '+',
      '<',
      '>');
       $url = str_replace ($find, $replay, $url);

        $find = array('/[^a-z0-9\-<>]/',
      '/[\-]+/',
      '/<[^>]*>/');
       $repl = array('',
      '-',
      '');
       $url = preg_replace ($find, $repl, $url);
       $url = str_replace ('--', '-', $url);

        return $url;
    }
//----'-------
    private function text( $prefix, $key, array $vars = null )
    {
    }


    public function ajax()
    {
//echo "sdfgdfsg";exit;
        if ( !OW::getRequest()->isAjax() )
        {
            throw new Redirect403Exception();
        }

        if ( !OW::getUser()->isAuthenticated() )
        {
            throw new AuthenticateException();
        }

        $command = $_POST['command'];
        $data = json_decode($_POST['data'], true);

        $result = '';

        switch($command)
        {
            case 'friends-accept':
                $userId = (int) OW::getUser()->getId();
                $requesterId = (int) $data['id'];

                $service = CHATTER_BOL_Service::getInstance();

/*
                $frendshipDto = $service->accept($userId, $requesterId);

                if ( !empty($frendshipDto) )
                {
                    $service->onAccept($userId, $requesterId, $frendshipDto);
                }

*/
                $feedback = OW::getLanguage()->text('friends', 'feedback_request_accepted');
                $result = "OW.info('{$feedback}');";
                break;
            
            case 'friends-ignore':
                $userId = (int) OW::getUser()->getId();
                $requesterId = (int) $data['id'];

                $service = CHATTER_BOL_Service::getInstance();

/*
                $service->ignore($requesterId, $userId);

*/
                $feedback = OW::getLanguage()->text('friends', 'feedback_request_ignored');
                $result = "OW.info('{$feedback}');";
                break;
        }

        echo json_encode(array(
            'script' => $result
        ));

        exit;
    }

}


