<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * This plugin should be bought from the developer by paying money to PayPal account: biuro@grafnet.pl
 * Legal purchase is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


class CHATTER_CTRL_Admin extends ADMIN_CTRL_Abstract
{

    public function dept()
    {
        $content="";
        $this->setPageTitle(OW::getLanguage()->text('chatter', 'admin_dept_title'));
        $this->setPageHeading(OW::getLanguage()->text('chatter', 'admin_dept_heading'));    
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
        $curent_url=OW_URL_HOME;
        $config = OW::getConfig();
        $content="";
        if (!isset($_POST['save'])) $_POST['save']="";
        if ($is_admin AND $id_user>0 AND $_POST['save']=="besave"){
/*
            if ($_POST['c_protectkey']){
                $chatter_protect_key=$_POST['c_protectkey'];
            }else{
                $chatter_protect_key=substr(md5(date('d-m-Y H:i:s')),0,10);
            }
            if ($_POST['c_generateforusers']){
                $generateforusers=1;
            }else{
                $generateforusers=0;
            }
            $chatter_max_items_eachplugin=$_POST['c_maxitemseachplugin'];
            if (!$chatter_max_items_eachplugin) $chatter_max_items_eachplugin=10;
            if ($chatter_max_items_eachplugin>50) $chatter_max_items_eachplugin=50;
            $chatter_max_items_sitemap=$_POST['c_maxitemsforsitemap'];
            if (!$chatter_max_items_sitemap) $chatter_max_items_sitemap=1000;
            if ($chatter_max_items_sitemap>9999) $chatter_max_items_sitemap=9999;

            $config->saveConfig('chatter', 'chatter_protect_key', $chatter_protect_key);
            $config->saveConfig('chatter', 'chatter_generateforusers', $generateforusers);
            $config->saveConfig('chatter', 'chatter_max_items_eachplugin', $chatter_max_items_eachplugin);
            $config->saveConfig('chatter', 'chatter_max_items_sitemap', $chatter_max_items_sitemap);
*/
//            $config->saveConfig('chatter', 'chatter_enable', $_POST['c_chatter_enable']);
            $config->saveConfig('chatter', 'show_for_who', $_POST['c_show_for_who']);

            $config->saveConfig('chatter', 'max_history_back', $_POST['c_max_history_back']);

            $config->saveConfig('chatter', 'mode_perpage_rooms', $_POST['c_mode_perpage_rooms']);

            $config->saveConfig('chatter', 'bg_card_color', $_POST['c_bg_card_color']);


            if ($_POST['c_display_in_menu']!=OW::getConfig()->getValue('chatter', 'display_in_menu')){
                OW::getNavigation()->deleteMenuItem('chatter', 'main_menu_item');
                if ($_POST['c_display_in_menu']=="1"){
                    OW::getNavigation()->addMenuItem(OW_Navigation::MAIN, 'chatter.index', 'chatter', 'main_menu_item', OW_Navigation::VISIBLE_FOR_ALL);
                }
            }
            $config->saveConfig('chatter', 'display_in_menu', $_POST['c_display_in_menu']);



            $config->saveConfig('chatter', 'display_in_console', $_POST['c_display_in_console']);

            $config->saveConfig('chatter', 'disable_sound', $_POST['c_disable_sound']);



//OW::getFeedback()->info(OW::getLanguage()->text('shoppro', 'change_status_succesfull'));
//                    OW::getFeedback()->error(OW::getLanguage()->text('shoppro', 'change_status_error'));
            OW::getApplication()->redirect($curent_url."admin/plugins/chatter");
        }



//--------ee s
    if (isset($_GET['aopx_chat']) AND $_GET['aopx_chat']=="adsdisble"){
        if ( !OW::getConfig()->configExists('base', 'allowlivesupportchat') ){
            OW::getConfig()->addConfig('base', 'allowlivesupportchat', "0", '');
        }
        OW::getConfig()->saveConfig('base', 'allowlivesupportchat', '0');
        OW::getFeedback()->info('Disabled advertisement. For enable it use URL:<br/><br/>'.OW_URL_HOME.'admin/plugins/chatter?aopx_chat=adsenable');
        OW::getApplication()->redirect($curent_url."admin/plugins/chatter");    
        exit;
    }else if (isset($_GET['aopx_chat']) AND $_GET['aopx_chat']=="adsenable"){
        if ( !OW::getConfig()->configExists('base', 'allowlivesupportchat') ){
            OW::getConfig()->addConfig('base', 'allowlivesupportchat', "1", '');
        }
        OW::getConfig()->saveConfig('base', 'allowlivesupportchat', '1');
        OW::getFeedback()->info('Enabled advertisement succesfull. For disable use URL:<br/><br/>'.OW_URL_HOME.'admin/plugins/chatter?aopx_chat=adsdisble');
        OW::getApplication()->redirect($curent_url."admin/plugins/chatter");    
        exit;    
//-----
    }else if (isset($_GET['aopx_chat']) AND $_GET['aopx_chat']=="adsdisblere"){
//        if ( !OW::getConfig()->configExists('base', 'allowrecomemded') ){
//            OW::getConfig()->addConfig('base', 'allowrecomemded', "0", '');
//        }
        $_SESSION['chat_allowrecomemded']="0";
//        OW::getConfig()->saveConfig('base', 'allowrecomemded', '0');
        OW::getFeedback()->info('Disabled advertisement. For enable it use URL:<br/><br/>'.OW_URL_HOME.'admin/plugins/chatter?aopx_chat=adsenablere');
        OW::getApplication()->redirect($curent_url."admin/plugins/chatter");    
        exit;
    }else if (isset($_GET['aopx_chat']) AND $_GET['aopx_chat']=="adsenablere"){
//        if ( !OW::getConfig()->configExists('base', 'allowrecomemded') ){
//            OW::getConfig()->addConfig('base', 'allowrecomemded', "1", '');
//        }
        $_SESSION['chat_allowrecomemded']="1";
//        OW::getConfig()->saveConfig('base', 'allowrecomemded', '1');
        OW::getFeedback()->info('Enabled advertisement succesfull. For disable use URL:<br/><br/>'.OW_URL_HOME.'admin/plugins/chatter?aopx_chat=adsdisblere');
        OW::getApplication()->redirect($curent_url."admin/plugins/chatter");    
        exit;
    }
    $content .=CHATTER_BOL_Service::getInstance()->ar_check_ads_v1();
//--------ee e


        $content .="<form action=\"".$curent_url."admin/plugins/chatter\" method=\"post\">";
        $content .="<input type=\"hidden\" name=\"save\" value=\"besave\">";
        $content .="<table style=\"width:auto;\">";
/*
//-----------------------
        if (OW::getConfig()->getValue('base', 'guests_can_view')!="1"){
            $content .="<tr>";
            $content .="<td colspan=\"2\">";
            $content .="<span class=\"ow_red\">";
            $content .="<span style=\"font-weight:bold;\" class=\"ow_red\">".OW::getLanguage()->text('chatter', 'info_guests')."</span>";

            $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_right\">
                    <a href=\"".$curent_url."admin/permissions\" target=\"_blank\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"button\" value=\"".OW::getLanguage()->text('chatter', 'change_page_permission')."\" name=\"dosavexx\" class=\"ow_ic_link ow_positive\">
                        </span>
                    </span>
                    </a>
                </div>
            </div>";

            $content .="</span>";
            $content .="</td>";
            $content .="</tr>";

            $content .="<tr>";
            $content .="<td colspan=\"2\">";
            $content .="<div class=\"clearfix\" style=\"height:30px;\"> ";
            $content .="</div>";
            $content .="</td>";
            $content .="</tr>";
        }
//-----------------------
*/

/*
        $content .="<tr class=\"ow_alt1\">";
        $content .="<td  class=\"ow_label\" style=\"min-width:300px;\">";
        $content .="<b>".OW::getLanguage()->text('chatter', 'chatter_enable').":</b>";
        $content .="</td>";
        $content .="<td  class=\"ow_value\">";
        $content .="<select name=\"c_chatter_enable\">";
        if (OW::getConfig()->getValue('chatter', 'chatter_enable')=="1" OR OW::getConfig()->getValue('chatter', 'chatter_enable')=="") $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('chatter', 'yes')."</option>";
        if (OW::getConfig()->getValue('chatter', 'chatter_enable')=="0" ) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('chatter', 'no')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";
*/

        $content .="<tr class=\"ow_alt1\">";
        $content .="<td  class=\"ow_label\" style=\"min-width:300px;\">";
        $content .="<b>".OW::getLanguage()->text('chatter', 'show_for_who').":</b>";
        $content .="</td>";
        $content .="<td  class=\"ow_value\">";
        $content .="<select name=\"c_show_for_who\">";
        if (OW::getConfig()->getValue('chatter', 'show_for_who')=="0" OR OW::getConfig()->getValue('chatter', 'show_for_who')=="") $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('chatter', 'show_for_all')."</option>";
        if (OW::getConfig()->getValue('chatter', 'show_for_who')=="1" ) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('chatter', 'show_for_guests_only')."</option>";
        if (OW::getConfig()->getValue('chatter', 'show_for_who')=="2" ) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"2\">".OW::getLanguage()->text('chatter', 'show_for_usres_only')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('chatter', 'bg_card_color').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('chatter', 'bg_card_color');
        if (!$itels) $itels="#CCCCCC";
        $content .="<input type=\"text\" name=\"c_bg_card_color\" value=\"".$itels."\" style=\"display:inline-block;width:100px;\">&nbsp;Default: #CCCCCC";
//        $content .="<br/><i>".OW::getLanguage()->text('chatter', 'max_history_back_info')."</i>";
        $content .="</td>";
        $content .="</tr>";



        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('chatter', 'max_history_back').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('chatter', 'max_history_back');
        if (!$itels) $itels=500;
        $content .="<input type=\"text\" name=\"c_max_history_back\" value=\"".$itels."\" style=\"display:inline-block;width:100px;\">&nbsp;Default:500";
        $content .="<br/><i>".OW::getLanguage()->text('chatter', 'max_history_back_info')."</i>";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('chatter', 'mode_perpage_rooms').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('chatter', 'mode_perpage_rooms');
        if (!$itels) $itels=50;
        $content .="<input type=\"text\" name=\"c_mode_perpage_rooms\" value=\"".$itels."\" style=\"display:inline-block;width:100px;\">&nbsp;Default:50";
//        $content .="<br/><i>".OW::getLanguage()->text('chatter', 'max_history_back_info')."</i>";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr>";
        $content .="<td class=\"ow_label\"  style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('chatter', 'display_in_menu').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\" >";
        $mode=$config->getValue('chatter', 'display_in_menu');
        $content .="<select name=\"c_display_in_menu\">";
        if ($mode=="0") $sel=" selected ";
            else $sel="";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('chatter', 'no')."</option>";
        if ($mode=="1" OR $mode=="") $sel=" selected ";
            else $sel="";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('chatter', 'yes')."</option>";
        $content .="</select>&nbsp;Default: YES";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr>";
        $content .="<td class=\"ow_label\"  style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('chatter', 'display_in_console').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\" >";
        $mode=$config->getValue('chatter', 'display_in_console');
        $content .="<select name=\"c_display_in_console\">";
        if ($mode=="0") $sel=" selected ";
            else $sel="";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('chatter', 'no')."</option>";
        if ($mode=="1" OR $mode=="") $sel=" selected ";
            else $sel="";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('chatter', 'yes')."</option>";
        $content .="</select>&nbsp;Default: NO";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr>";
        $content .="<td class=\"ow_label\"  style=\"text-align:right;\">";
        $content .="<b>".OW::getLanguage()->text('chatter', 'disable_sound').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\" >";
        $mode=$config->getValue('chatter', 'disable_sound');
        $content .="<select name=\"c_disable_sound\">";
        if ($mode=="0") $sel=" selected ";
            else $sel="";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('chatter', 'no')."</option>";
        if ($mode=="1" OR $mode=="") $sel=" selected ";
            else $sel="";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('chatter', 'yes')."</option>";
        $content .="</select>&nbsp;Default: YES";
        $content .="<br/>";
        $content .="<i>".OW::getLanguage()->text('chatter', 'disable_sound_info')."</i>";
        $content .="</td>";
        $content .="</tr>";

/*
        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('chatter', 'na_clock_').":</b>";
        $content .="</td>";
        $content .="<td nowrap=\"nowrap\">";
        $value=OW::getConfig()->getValue('chatter', 'toclock');
        if ($value=="0") $sel="";
            else $sel=" CHECKED ";
        $content .="<input ".$sel." type=\"checkbox\" name=\"c_toclock\" value=\"1\">";
        $content .="</td>";
        $content .="</tr>";
*/

/*
        $content .="<tr>";
        $content .="<td colspan=\"2\" wrap=\"wrap\" style=\"background-color:#eee;\">";
        $content .=OW::getLanguage()->text('chatter', 'admin_protectkey_informationofuse');
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('chatter', 'admin_userprofiledata').":</b>";
        $content .="</td>";
        $content .="<td >";
        $content .="<select name=\"c_generateforusers\">";
        if (OW::getConfig()->getValue('chatter', 'chatter_generateforusers') OR !OW::getConfig()->getValue('chatter', 'chatter_generateforusers')) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('chatter', 'admin_dontgeneratememberstoo')."</option>";
        if (OW::getConfig()->getValue('chatter', 'chatter_generateforusers')==1) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('chatter', 'admin_generatememberstoo')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('chatter', 'admin_maxitemseachplugin').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('chatter', 'chatter_max_items_eachplugin');
        if (!$itels) $itels=10;
        $content .="<input type=\"text\" name=\"c_maxitemseachplugin\" value=\"".$itels."\" style=\"display:inline-block;width:100px;\">";
        $content .="&nbsp;".OW::getLanguage()->text('chatter', 'admin_maxitemseachplugin_info')."";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('chatter', 'admin_maxitemsforsitemap').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itelss=OW::getConfig()->getValue('chatter', 'chatter_max_items_sitemap');
        if (!$itelss) $itelss=1000;
        $content .="<input type=\"text\" name=\"c_maxitemsforsitemap\" value=\"".$itelss."\" style=\"display:inline-block;width:100px;\">";
        $content .="&nbsp;".OW::getLanguage()->text('chatter', 'admin_maxitemsforsitemap_info')."";
        $content .="</td>";
        $content .="</tr>";
*/
        $content .="<tr>";
        $content .="<td colspan=\"2\">";
//        $content .="<input type=\"submit\" name=\"dosave\" value=\"".OW::getLanguage()->text('chatter', 'admin_save')."\">";
        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('chatter', 'admin_save')."\" name=\"dosave\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
        $content .="</td>";
        $content .="</tr>";
        $content .="</table>";
        $content .="</form>";
        $this->assign('content', $content);
    }

    public function delete( $params )
    {
        $this->redirect(OW::getRouter()->urlForRoute('chatter.admin'));
    }
}
