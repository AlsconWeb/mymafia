<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * This plugin should be bought from the developer by paying money to PayPal account: biuro@grafnet.pl
 * Legal purchase is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


OW::getRouter()->addRoute(new OW_Route('chatter.index', 'chatter', "CHATTER_CTRL_Chatter", 'index'));
OW::getRouter()->addRoute(new OW_Route('chatter.indexrsxx', 'chatter/chatter/ajax', "CHATTER_CTRL_Chatter", 'ajax'));

OW::getRouter()->addRoute(new OW_Route('chatter.indexrs', 'chatter/room/:idroom', "CHATTER_CTRL_Chatter", 'index'));
OW::getRouter()->addRoute(new OW_Route('chatter.indexrsf', 'chatter/room/:idroom/:isfaq', "CHATTER_CTRL_Chatter", 'index'));

OW::getRouter()->addRoute(new OW_Route('chatter.indexrp', 'chatter/ping/:ss', "CHATTER_CTRL_Chatter", 'indexp'));
OW::getRouter()->addRoute(new OW_Route('chatter.indexractx', 'chatter/action/:action/:op/:ss', "CHATTER_CTRL_Chatter", 'index'));

OW::getRouter()->addRoute(new OW_Route('chatter.indexrplog', 'chatter/login/:idlogin/:ss', "CHATTER_CTRL_Chatter", 'index'));

OW::getRouter()->addRoute(new OW_Route('chatter.indexr', 'chatter/rooms/:op', "CHATTER_CTRL_Chatter", 'index'));
OW::getRouter()->addRoute(new OW_Route('chatter.indexajs', 'chatter/:plug/:ss', "CHATTER_CTRL_Chatter", 'indexaj'));
OW::getRouter()->addRoute(new OW_Route('chatter.indexaj', 'chatter/:plug', "CHATTER_CTRL_Chatter", 'indexaj'));
//OW::getRouter()->addRoute(new OW_Route('chatter.admin', 'admin/plugins/chatter', "CHATTER_CTRL_Admin", 'dept'));
OW::getRouter()->addRoute(new OW_Route('chatter.admin', 'admin/plugins/chatter', "CHATTER_CTRL_Admin", 'dept'));


/*
$config = OW::getConfig();
if ( !$config->configExists('chatter', 'chatter_enable') ){
    $config->addConfig('chatter', 'chatter_enable', '1', '');
}
if ( !$config->configExists('chatter', 'feature') ){
    $config->addConfig('chatter', 'feature', '0', '');
}
*/
//OW::getConfig()->saveConfig('chatter', 'feature', '1');

function chatter_add_auth_labels( BASE_CLASS_EventCollector $event )
{
    $language = OW::getLanguage();
    $event->add(
        array(
            'chatter' => array(
                'label' => $language->text('chatter', 'auth_group_label'),
                'actions' => array(
                    'roomscreate' => $language->text('chatter', 'auth_action_label_roomscreate'),

                    'roomsread' => $language->text('chatter', 'auth_action_label_roomsread'),
                    'roomswrite' => $language->text('chatter', 'auth_action_label_roomswrite'),

                    'publicread' => $language->text('chatter', 'auth_action_label_publicread'),
                    'publicwrite' => $language->text('chatter', 'auth_action_label_publicwrite')

                )
            )
        )
    );
}
OW::getEventManager()->bind('admin.add_auth_labels', 'chatter_add_auth_labels');

/*
$config = OW::getConfig();
if ( !$config->configExists('chatter', 'toclock') ){
    $config->addConfig('chatter', 'toclock', '1', '');
}
if ( !$config->configExists('chatter', 'tonews') ){
    $config->addConfig('chatter', 'tonews', '1', '');
}
*/

if (!isset($_SERVER["SERVER_NAME"])) $_SERVER["SERVER_NAME"]="";
if (!isset($_SERVER["REQUEST_URI"])) $_SERVER["REQUEST_URI"]="";

//OW::getRouter()->addRoute(new OW_Route('pay24.dopay', 'pay24/:option/:iditem', "PAY24_CTRL_Dopay", 'index'));
//OW::getRouter()->addRoute(new OW_Route('pay24.back', 'pay24/back', "PAY24_CTRL_Dopay", 'index'));;
//OW::getRouter()->addRoute(new OW_Route('chatter.payresult', 'chatter/:com', "CHATTER_CTRL_Chatter", 'index'));;

/*
function chatter_setrestriction_tick(){
    $id_user = OW::getUser()->getId();
    if (OW::getConfig()->getValue('chatter', 'chatter_enable')=="1"){
        if ( 
            OW::getConfig()->getValue('chatter', 'show_for_who')=="0" OR !OW::getConfig()->getValue('chatter', 'show_for_who') OR 
            (OW::getConfig()->getValue('chatter', 'show_for_who')=="1" AND !$id_user) OR 
            (OW::getConfig()->getValue('chatter', 'show_for_who')=="2" AND $id_user)
        ) {
            CHATTER_BOL_Service::getInstance()->minix();
        }
    }
}

if (!OW::getRequest()->isPost() AND !OW::getRequest()->isAjax() AND 
    strpos($_SERVER["REQUEST_URI"],"base/media-panel")===false AND 

    strpos($_SERVER["REQUEST_URI"],"admin/")===false AND 

    strpos($_SERVER["REQUEST_URI"],"base-document/maintenance")===false AND 
    strpos($_SERVER["REQUEST_URI"],"/splash-screen")===false AND 

    strpos($_SERVER["REQUEST_URI"],"/admin/")===false AND 
    strpos($_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"],"ow_cron")===false AND 
    strpos($_SERVER["REQUEST_URI"],"admin/")===false AND 
    CHATTER_BOL_Service::getInstance()->check_url_allow()==true
    ){
        OW::getEventManager()->bind('core.finalize', 'chatter_setrestriction_tick');
//echo "--".OW::getRequest()->isPost();exit;
//    OW::getEventManager()->bind('core.app_init', 'setrestriction_tick');
        
}

*/



/*
function contactus_handler_after_install( BASE_CLASS_EventCollector $event )
{
    if ( count(CONTACTUS_BOL_Service::getInstance()->getDepartmentList()) < 1 )
    {
        $url = OW::getRouter()->urlForRoute('contactus.admin');
        $event->add(OW::getLanguage()->text('contactus', 'after_install_notification', array('url' => $url)));
    }
}

OW::getEventManager()->bind('admin.add_admin_notification', 'contactus_handler_after_install');
*/

/*
function chatter_ads_enabled( BASE_EventCollector $event )
{
    $event->add('chatter');
}
OW::getEventManager()->bind('ads.enabled_plugins', 'chatter_ads_enabled');
OW::getRequestHandler()->addCatchAllRequestsExclude('base.suspended_user', 'CHATTER_CTRL_Chatter');
*/

//--------------------------------------------------------

/*

function chatter_questions_email_save( OW_Event $e )
{
    $params = $e->getParams();
    $data = $e->getData();

    foreach ( $data as $key => $value )
    {
        if ( $key == 'password' )
        {
//$params['userId']

        }
    }

    $e->setData($data);
}
OW::getEventManager()->bind('base.questions_save_data', 'chatter_questions_email_save');

*/


/*
function chatter_add_admin_notification( BASE_CLASS_EventCollector $e )
{
    if (OW::getConfig()->getValue('base', 'guests_can_view')!="1")
    {
        $pluginUrl = OW::getRouter()->urlForRoute('chatter.admin');
        $pluginUrlInfo = '<a href="' . $pluginUrl . '" class="ow_red">' . OW::getLanguage()->text('chatter', 'plugin_setting') . '</a>';
    
        $language = OW::getLanguage();
        $e->add(OW::getLanguage()->text('chatter', 'admin_you_must_agree_for_share')." ".$pluginUrlInfo);
    }
}
//<key name="admin_you_must_agree_for_share"><value>RSS requires your attention.</value></key>
//<key name="plugin_setting"><value>Please review the plugin settings and change them if it needed.</value></key>
//<key name="info_guests"><value>NOTE: You have disabled site visitors. Plugin can not more working properly, patronize other parties may not be able to download content from your site. Set: "Guests can view the site" to "YES". If you want the page was not available to other users, you can use the plugin "Start Page" - this plugs has a similar option "Guests can view the site = NO", but working with other plugins.</value></key>

//<key name="admin_you_must_agree_for_share"><value>RSS wymaga twojej uwagi.</value></key>
//<key name="plugin_setting"><value>Proszę zobaczyć szczegóły w ustawieniach pluginu.</value></key>
//<key name="info_guests"><value>UWAGA! Masz wyłączoną stronę dla gości. Plugin może nie działąć poprawnie, ponieaż inne strony mogą nie mieć możliwości pobrania treści z twojej strony. Ustaw opcję: "Guests can view the site" na "YES". Jeśli chcesz aby strona była niedostępna dla innych użytkowników możesz użyć pluginu "Start Page" - this plugi has similar option to "Guests can view the site = NO", but working z innymi pluginami.</value></key>

OW::getEventManager()->bind('admin.add_admin_notification', 'chatter_add_admin_notification');
*/

CHATTER_CLASS_EventHandler::getInstance()->init();

//if ( OW::getPluginManager()->getPlugin('chatter')->getDto()->build >= 5836 ){
    CHATTER_CLASS_RequestEventHandler::getInstance()->init();
//}

