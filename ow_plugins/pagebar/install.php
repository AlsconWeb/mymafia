<?php

/**
 * Copyright (c) 2015, Kairat Bakytow
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

$config = OW::getConfig();

if ( !$config->configExists('pagebar', 'color') )
{
    $config->addConfig('pagebar', 'color', 'red');
}

if ( !$config->configExists('pagebar', 'action') )
{
    $config->addConfig('pagebar', 'action', 'minimal');
}

OW::getPluginManager()->addPluginSettingsRouteName('pagebar', 'pagebar.admin');

OW::getLanguage()->importPluginLangs(dirname(__FILE__) . DS . 'langs.zip', 'pagebar');
