<?php

/**
 * Copyright (c) 2015, Kairat Bakytow
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

/**
 *
 * @author Kairat Bakytow
 * @package ow_plugins.pagebar.controllers
 * @since 1.1
 */
class PAGEBAR_CTRL_Admin extends ADMIN_CTRL_Abstract
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index( $params = null )
    {
        $color = array('black', 'blue', 'green', 'orange', 'pink', 'purple',
            'red', 'silver', 'white', 'yellow');
        $action = array('barber-shop', 'big-counter', 'bounce', 'center-atom',
            'center-circle', 'center-radar', 'center-simple', 'corner-indicator',
            'fill-left', 'flash', 'flat-top', 'loading-bar', 'mac-osx', 'minimal');
        
        $form = new Form('save');
        
        $selectColor = new Selectbox('color');
        $selectColor->setRequired();
        $selectColor->setOptions(array_combine($color, $color));
        $selectColor->setValue(OW::getConfig()->getValue('pagebar', 'color'));
        $selectColor->setLabel(OW::getLanguage()->text('pagebar', 'color'));
        $form->addElement($selectColor);
        
        $actionSelect = new Selectbox('action');
        $actionSelect->setRequired();
        $actionSelect->setOptions(array_combine($action, $action));
        $actionSelect->setValue(OW::getConfig()->getValue('pagebar', 'action'));
        $actionSelect->setLabel(OW::getLanguage()->text('pagebar', 'action'));
        $form->addElement($actionSelect);
        
        $submit = new Submit('save');
        $submit->setLabel(OW::getLanguage()->text('pagebar', 'save'));
        $form->addElement($submit);
        
        if ( OW::getRequest()->isPost() && $form->isValid($_POST) )
        {
            OW::getConfig()->saveConfig('pagebar', 'color', $form->getElement('color')->getValue());
            OW::getConfig()->saveConfig('pagebar', 'action', $form->getElement('action')->getValue());
            
            OW::getFeedback()->info('Settings successfully saved');
        }
        
        $this->addForm($form);
    }
}
