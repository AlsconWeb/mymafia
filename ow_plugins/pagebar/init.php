<?php

/**
 * Copyright (c) 2015, Kairat Bakytow
 * All rights reserved.

 * ATTENTION: This commercial software is intended for use with Oxwall Free Community Software http://www.oxwall.org/
 * and is licensed under Oxwall Store Commercial License.
 * Full text of this license can be found at http://www.oxwall.org/store/oscl
 */

OW::getRouter()->addRoute(new OW_Route('pagebar.admin', 'pagebar/admin', 'PAGEBAR_CTRL_Admin', 'index'));

function pagebar_on_after_route( OW_Event $event )
{
    if ( OW::getRequest()->isAjax() )
    {
        return;
    }
    
    $handler = OW::getRequestHandler()->getHandlerAttributes();
    
    if ( array_key_exists('ADMIN_CTRL_Abstract', class_parents($handler[OW_RequestHandler::ATTRS_KEY_CTRL])) )
    {
        return;
    }
    
    $config = OW::getConfig();
    OW::getDocument()->addStyleSheet(
        OW::getPluginManager()->getPlugin('pagebar')->getStaticCssUrl() . $config->getValue('pagebar', 'color') . '/' . $config->getValue('pagebar', 'action'). '.css'
    );
    
    $javascript = OW::getDocument()->getJavaScripts();
    array_unshift($javascript['items'][-100]['text/javascript'], OW::getPluginManager()->getPlugin('pagebar')->getStaticJsUrl() . 'pace.js');
    OW::getDocument()->setJavaScripts($javascript);
}
OW::getEventManager()->bind(OW_EventManager::ON_AFTER_ROUTE, 'pagebar_on_after_route');