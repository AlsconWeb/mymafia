<?php

class SMTINFO_CMP_Widgetcontrol extends BASE_CLASS_Widget 
{
   
    public function __construct(BASE_CLASS_WidgetParameter $params) 
    {
        parent::__construct();

        $lang = OW::getLanguage();

$user = OW::getUser()->getId();
$user_service = BOL_UserService::getInstance();
$avat_service = BOL_AvatarService::getInstance();
$displayname  = $user_service->getDisplayName($user);
$this->assign("displayname",$displayname);


   $smtinf = OW::getLanguage()->text('smtinfo', 'widget_smtinf'); 
   $userName = BOL_UserService::getInstance()->getDisplayName(OW::getUser()->getId());
   $siteEmail = OW::getConfig()->getValue('base', 'site_email');
   $siteName = OW::getConfig()->getValue('base', 'site_name');
	
	$smtinf = str_replace("{userName}",$userName,$smtinf);
	$smtinf = str_replace("{siteEmail}",$siteEmail,$smtinf);
	$smtinf = str_replace("{siteName}",$siteName,$smtinf);
    $this->assign("smtinf",$smtinf);

	$smtinfcolor = OW::getLanguage()->text('smtinf', 'widget_smtinfo_color');

	$this->assign("smtinfcolor",$smtinfcolor);

			$colortexto = Ow::getConfig()->getValue('smtinfo', 'colorbg');
			$this->assign("colortexto",$colortexto);
    }//close function
 

    public static function getStandardSettingValueList() 
    {
        return array(
            self::SETTING_TITLE => OW::getLanguage()->text('smtinfo', 'smtinfo_widget_title'),
            self::SETTING_ICON => self::ICON_CLOCK,
            self::SETTING_SHOW_TITLE => true,
            self::SETTING_WRAP_IN_BOX => true
        );
		
    }//close function
	

    public static function getAccess() 
    {
        return self::ACCESS_ALL;
    }//close function

}//close class

