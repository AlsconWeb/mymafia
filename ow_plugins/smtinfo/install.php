<?php

$config = OW::getConfig();
BOL_LanguageService::getInstance()->addPrefix('smtinfo', 'SMTINFO');
OW::getLanguage()->importPluginLangs(OW::getPluginManager()->getPlugin('smtinfo')->getRootDir().'langs.zip', 'smtinfo');
OW::getPluginManager()->addPluginSettingsRouteName('smtinfo', 'smtinfo_admin_index');

if ( !OW::getConfig()->configExists('smtinfo', 'colorbg') )
{
    OW::getConfig()->addConfig('smtinfo', 'colorbg', '', 'COLOR bg');
        $config = OW::getConfig();

        $config->saveConfig('smtinfo', 'colorbg', "#457D8E");
}
