<?php

class SMTINFO_CTRL_Admin extends ADMIN_CTRL_Abstract {

    public function __construct() {
        parent::__construct();

        if (OW::getRequest()->isAjax()) {
            return;
        }
    }

    public function index() {

			$candela = Ow::getConfig()->getValue('smtinfo', 'colorbg');
			$this->assign("candela",$candela);
        $language = OW::getLanguage();
        $config = OW::getConfig();

        $this->setPageHeading(OW::getLanguage()->text('smtinfo', 'admin_settings_title'));
        $this->setPageTitle(OW::getLanguage()->text('smtinfo', 'admin_settings_title'));
        $this->setPageHeadingIconClass('ow_ic_gear_wheel');

        $infSaveForm = new ConfigForm();
        $this->addForm($infSaveForm);

        if ( OW::getRequest()->isPost() && $infSaveForm->isValid($_POST) )
        {
            $infSaveForm->process();
            OW::getFeedback()->info($language->text('smtinfo', 'settings_updated'));
            $this->redirect();
        }

   $textinfadmin = OW::getLanguage()->text('smtinfo', 'smtinfo_infadmin'); 
		
	$this->assign("textinfadmin",$textinfadmin);

$urleal =  OW_URL_HOME."admin/settings/languages?&prefix=smtinfo";
	$this->assign("urleal",$urleal);

   $imgwadmin = OW::getPluginManager()->getPlugin('smtinfo')->getStaticUrl() . 'images/wadminflag.png'; 
		
	$this->assign("imgwadmin",$imgwadmin); 
	
	   $imgmore = OW::getPluginManager()->getPlugin('smtinfo')->getStaticUrl() . 'images/more.png'; 
		
	$this->assign("imgmore",$imgmore); 
    }

}
/**
 * Save Configurations form class
 */
class ConfigForm extends Form
{

    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        parent::__construct('infSaveForm');

        $language = OW::getLanguage();

        $configs = OW::getConfig()->getValues('smtinfo');

        $element = new TextField('colorbg');
        $element->setValue($configs['colorbg']);
        $element->setRequired();

        $validator = new StringValidator(0, 40);
        $validator->setErrorMessage($language->text('smtinfo', 'color_key_too_long'));

        $element->addValidator($validator);
        $this->addElement($element);

        // submit
        $submit = new Submit('save');
        $submit->setValue($language->text('base', 'edit_button'));
        $this->addElement($submit);
    }

    /**
     * Updates forum plugin configuration
     *
     * @return boolean
     */
    public function process()
    {
        $values = $this->getValues();

        $colorBg = empty($values['colorbg']) ? '' : $values['colorbg'];

        $config = OW::getConfig();

        $config->saveConfig('smtinfo', 'colorbg', $colorBg);

        return array('result' => true);
    }

	
}