<?php



class UPTOLIKE_CMP_ShareButtons extends OW_Component
{
    protected $class = "";
    protected $title = null;
    protected $description = null;
    protected $url = null;
    protected $imageUrl = null;
    protected $displayBlock = true;
    /**
     * Class constructor
     */
    public function __construct( $params = array() )
    {

    /*    if ( !OW::getConfig()->getValue('socialsharing', 'api_key') )
        {
            $this->setVisible(false);
        }

        if ( OW::getConfig()->getValue('base', 'guests_can_view') != 1 || OW::getConfig()->getValue('base', 'maintenance'))
        {
            $this->setVisible(false);
        }
*/
        parent::__construct();

    }


    public function onBeforeRender()
    {
        $config = OW::getConfig();

		
		$apiKey = $config->getValue('uptolike', 'u_script');

        if ( empty($apiKey) )
        {
            $this->setVisible(false);
        }
		$this->assign('u_script', $apiKey);
		

		$this->setTemplate(OW::getPluginManager()->getPlugin('uptolike')->getCmpViewDir().'uptolike_block.html');
			
		

        return parent::onBeforeRender();
    }


}

