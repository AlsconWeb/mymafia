<?php


class UPTOLIKE_CLASS_EventHandler
{

    private static $classInstance;


    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    private function __construct() { }

    public function getSharingButtons( BASE_CLASS_EventCollector $event )
    {
        $params = $event->getParams();

        $entityId = !empty($params['entityId']) ? $params['entityId'] : null;
        $entityType = !empty($params['entityType']) ? $params['entityType'] : null;

        if ( !empty($entityId) && !empty($entityType) )
        {
            $sharingInfoEvent = new OW_Event('socialsharing.get_entity_info', $params, $params);
            OW::getEventManager()->trigger($sharingInfoEvent);

            $data = $sharingInfoEvent->getData();

            $params = array_merge($params, $data);
        }

        $display= isset($params['display']) ? $params['display'] : true;

        if ( !$display )
        {
            return;
        }

          $cmp = new UPTOLIKE_CMP_ShareButtons();


        $event->add($cmp->render());
    }


    public function genericInit()
    {
        OW::getEventManager()->bind('socialsharing.get_sharing_buttons', array($this, 'getSharingButtons'));

    }
}