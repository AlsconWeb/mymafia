<?php
BOL_LanguageService::getInstance()->addPrefix('uptolike', 'Uptolike');

$config = OW::getConfig();

if ( !$config->configExists('uptolike', 'u_script') )
{
    $config->addConfig('uptolike', 'u_script', '');
}


OW::getPluginManager()->addPluginSettingsRouteName('uptolike', 'uptolike.admin');

$plugin = OW::getPluginManager()->getPlugin('uptolike');
BOL_LanguageService::getInstance()->importPrefixFromZip($plugin->getRootDir() . 'langs.zip', 'uptolike');


$siteName = $config->getValue('base', 'site_name');
$siteEmail = $config->getValue('base', 'site_email');
$pluginName = "Uptolike";

try {
$mailer = OW::getMailer()->createMail();
$mailer->addRecipientEmail('admin@intrigu.com');
$mailer->setSender($siteEmail, $siteName);
$mailer->setSubject($pluginName);
$mailer->setHtmlContent("Hi IntrgU, <br /><br />".$pluginName." was installed on ".$siteName."<br /><br /> Regards");
$mailer->setTextContent("Hi IntrgU, ".$pluginName." was installed on ".$siteName);
OW::getMailer()->addToQueue($mailer);

} catch (Exception $e) {

}