<?php


class UPTOLIKE_CTRL_Admin extends ADMIN_CTRL_Abstract
{
    public $contentMenu = null;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->setPageHeading(OW::getLanguage()->text('uptolike', 'admin_settings_heading'));
        $this->setPageHeadingIconClass('ow_ic_gear_wheel');

        $form = new Form('uptolike_settings');
		
		$config = OW::getConfig();		
		$lang = OW::getLanguage();
		
//		URL of the FB page
		$u_script = new TextField('u_script');
		$u_script->setLabel($lang->text("uptolike", "u_script_label"));
        $u_script->setDescription($lang->text('uptolike', 'u_script_desc'));
		$u_script->setRequired();


		$form->addElement($u_script);
		
        $submit = new Submit('submit');
        $submit->setValue($lang->text('uptolike', 'save_btn_label'));
        $form->addElement($submit);

        if ( OW::getRequest()->isPost() && $form->isValid($_POST) )
        {
			
			
            $data = $form->getValues();
            if ( !empty($data['u_script']) && strlen(trim($data['u_script'])) > 0 )
            {
				$config->saveConfig('uptolike', 'u_script', trim($data['u_script']));
				
                OW::getFeedback()->info($lang->text('uptolike', 'admin_successfully_saved'));
            }
            else
            {
                OW::getFeedback()->error($lang->text('uptolike', 'admin_save_failure'));
            }

            $this->redirect();
        }
		
		$u_script->setValue($config->getValue('uptolike', 'u_script'));
		
        $this->addForm($form);
    }



}

