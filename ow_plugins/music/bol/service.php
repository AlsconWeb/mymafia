<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

class MUSIC_BOL_Service
{
    /**
     * Singleton instance.
     *
     * @var CONTACTUS_BOL_Service
     */
    private static $classInstance;

    /**
     * Returns an instance of class (singleton pattern implementation).
     *
     * @return CONTACTUS_BOL_Service
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }

    private function __construct()
    {

    }

    public function getDepartmentLabel( $id )
    {
        return OW::getLanguage()->text('pay24', $this->getDepartmentKey($id));
    }

//    public function url_toparam ()
    public function check_url_allow ()
    {

//            return false;
/*
        if (OW::getRequest()->isPost()){
            return false;
        }else if (OW::getRequest()->isAjax()){
            return false;
        }
*/
/*
file_put_contents("TTTTTTTTTTTTTTT.txt", "\n==================================\n", FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", print_r($_GET,1), FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", print_r($_POST,1), FILE_APPEND );
file_put_contents("TTTTTTTTTTTTTTT.txt", "\n==================================\n\n", FILE_APPEND );
*/
        $curent_url=OW_URL_HOME;
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) AND $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        $tmp=str_replace(OW_URL_HOME,"",$pageURL);
        if (substr($tmp,0,1)=="/") $tmp=substr($tmp,1);

        $test_furl=$tmp;
        $test_purl="";
        $test_aurl=array();

        $testx_tmp=explode("?",$test_furl);
        if (isset($testx_tmp[0])) $test_furl=$testx_tmp[0];
        if (isset($testx_tmp[1])) $test_purl=$testx_tmp[1];
        $test_aurl=explode("/",$test_furl);
//echo $test_furl;exit;
//            return false;
        if (isset($_POST['form_name']) AND $_POST['form_name']=="sign-in"){
            return false;
        }else if ( strpos($test_furl,"ajaxim")!==false){
            return false;
        }else if ( strpos($test_furl,"ow_cron")!==false){
            return false;
        }else if ( strpos($test_furl,"ow_static")!==false){
            return false;
        }else if ( strpos($test_furl,"ping")!==false){
            return false;
//        }else if (!OW::getUser()->isAuthenticated()){//for members only
//            return false;
        }else if ( strpos($test_furl,"base/ping/index")!==false OR strpos($test_furl,"base/ping")!==false){
            return false;
        }else{
            return true;
        }
    }


    public function get_news( )
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;

        $sql = "SELECT * FROM " . OW_DB_PREFIX. "news WHERE active='1' AND is_published='1'  ORDER BY `data_added` DESC LIMIT 10";
        $arrl = OW::getDbo()->queryForList($sql);
        $content="";
        foreach ( $arrl as $value )
        {
            $content .="<li>
                <div>
                    <a href=\"".$curent_url."news/".$value['id']."/index.html\">".stripslashes($value['topic_name'])."</a>
                </div>
            </li> ";
        }
        if ($content){
            $content ="<ul>".$content."</ul>";
        }
        return $content;
    }



    public function get_music_list ($for_user=0)
    {
        $content="";
        $script="";
        $css="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
        $pluginStaticURL=OW::getPluginManager()->getPlugin('music')->getStaticUrl();
        $pluginUserFileURL=OW::getPluginManager()->getPlugin('music')->getUserFilesUrl();
        $pluginUserFileDIR=OW::getPluginManager()->getPlugin('music')->getUserFilesDir();



        $maxlimit=50;

//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/music/music.js');
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/music/music_main.js');

//        $content .="<div id=\"dnews\" class=\"news-wrapper\">
//        <div class=\"news\"></div>
//        </div>";
        
            if (!$for_user){
                return;
            }

            $sql = "SELECT * FROM " . OW_DB_PREFIX. "music
            WHERE id_owner='".addslashes($for_user)."' AND active='1' AND display_in_profile='1' ORDER BY auto_play DESC, u_title LIMIT ".$maxlimit;
            $arrl = OW::getDbo()->queryForList($sql);
            $_SESSION['profile_music_autoplay']=0;
            foreach ( $arrl as $value ){
//                $title=mb_substr($this->html2txt(stripslashes($value['u_title'])),0,100);
                $title=UTIL_String::truncate($this->html2txt(stripslashes($value['u_title'])), 100, '..');
//                $url=stripslashes($value['u_url']).stripslashes($value['u_file']);

//                $description=mb_substr($this->html2txt(stripslashes($value['u_desc'])),0,255);
        
                if ($value['auto_play']=="1") $_SESSION['profile_music_autoplay']=1;

                $url="";
                if ($value['u_url']){
                    $url=stripslashes($value['u_url']);
                }else{
                    $url=$pluginUserFileURL.$value['id_owner']."/".stripslashes($value['u_file']);
                }




                if ($value['u_url'] AND strlen($value['u_url'])<10){
                    //not corect URL
                }else if (!$value['u_url'] AND (!$value['u_file'] OR !$this->file_exist($pluginUserFileDIR.$value['id_owner']."/".stripslashes($value['u_file'])))){
                    //mp3 niot exist
                }else{
                    $content .="<div class=\"clearfix\">";

                        if (OW::getConfig()->getValue('music', 'display_title')=="1"){
                            $content .="<div class=\"ow_left\">";
                                $content .=$title;
                            $content .="</div>";
                        }
                        $content .="<div class=\"clearfix\">";

                            $content .="<audio id=\"player_".$value['id']."\" class=\"audio_music\" src=\"".$url."\" type=\"audio/mp3\" controls=\"controls\" />";
                        $content .="</div>";

                        if (OW::getConfig()->getValue('music', 'display_title')!="1"){
                            $content .="<div class=\"clearfix\" style=\"height:3px;\">";
                            $content .="</div>";
                        }

//                        $content .="<div class=\"ow_left\">";
//                            $content .=$description;
//                        $content .="</div>";

                    $content .="</div>";
                }

            }


        return $content;
    }

    public function get_edit_form($params)
    {
        $content="";
        $script="";
        $css="";
        $curent_url=OW_URL_HOME;

        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
//        $pluginStaticURL=OW::getPluginManager()->getPlugin('music')->getStaticUrl();
        $pluginUserFileURL=OW::getPluginManager()->getPlugin('music')->getUserFilesUrl();
        $pluginUserFileDIR=OW::getPluginManager()->getPlugin('music')->getUserFilesDir();

        if (!isset($params['id'])) $params['id']="";
        if (!isset($params['ss'])) $params['ss']="";
        if (!isset($params['op'])) $params['op']="";

        $id_page=$params['id'];



        if (
                !$id_user 
                    OR 
                (
                    ($id_page>0 AND ($params['op']!="edit" AND $params['op']!="del") ) 
                        OR 
                    (!$id_page AND  $params['op']=="new") 
                ) 
                OR 
                $params['ss']!=substr(session_id(),3,5)
        ){
//            OW::getFeedback()->info(OW::getLanguage()->text('music', 'you_donthave_permission'));
            OW::getFeedback()->error(OW::getLanguage()->text('music', 'you_donthave_permission'));
            OW::getApplication()->redirect($curent_url."music");
            exit;
        }


        if (isset($_POST['do_save']) AND $_POST['do_save']=="true" AND isset($_POST['ss']) AND $_POST['ss']==$params['ss'] AND $params['ss']==substr(session_id(),3,5) AND $id_user>0){


                $for_user=$id_user;
                if ($is_admin AND $params['id']>0){
                    $sql = "SELECT * FROM " . OW_DB_PREFIX. "music
                    WHERE id='".addslashes($params['id'])."' LIMIT 1";
                    $arrl = OW::getDbo()->queryForList($sql);
                    if (isset($arrl[0]) AND isset($arrl[0]['id']) AND $arrl[0]['id']>0){
                        $for_user=$arrl[0]['id_owner'];
                    }
                }



            $timestamp=strtotime(date('Y-m-d H:i:s'));

                if (isset($_POST['u_title']) AND $_POST['u_title']){
                    $title=$_POST['u_title'];
                }else{
                    $title=OW::getLanguage()->text('music', 'no_title');
                }

                if (isset($_POST['u_order']) AND $_POST['u_order']>0){
                    $u_order=$_POST['u_order'];
                }else{
                    $u_order=0;
                }

                if (isset($_POST['u_artist']) AND $_POST['u_artist']){
                    $u_author=$_POST['u_artist'];
                }else{
                    $u_author="";
                }


                if (isset($_POST['mdesc']) AND $_POST['mdesc']){
                    $mdesc=$_POST['mdesc'];
                }else{
                    $mdesc="";
                }

                if (isset($_POST['display_in_profile']) AND $_POST['display_in_profile']=="1"){
                    $display_in_profile=1;
                }else{
                    $display_in_profile=0;
                }


                $file_ex_change="";



                if (isset($_POST['url_external']) AND strlen($_POST['url_external'])>10){
                    $url_external=$_POST['url_external'];
                    $file_external="";
                    $file_ex_change=" u_file='".addslashes($file_external)."', ";
                }else if (isset($_FILES['mp3']) AND isset($_FILES['mp3']['name']) AND isset($_FILES['mp3']['size']) AND $_FILES['mp3']['size']>1){
//                    if (file_exists($_FILES['mp3']['tmp_name'])) {
                    if ($this->file_exist($_FILES['mp3']['tmp_name'])) {

    $file_size = $_FILES['mp3']['size'];
    $file_type = $_FILES['mp3']['type'];
    $file_size_quota=OW::getConfig()->getValue('music', 'file_size_quota');
    if (!$file_size_quota) $file_size_quota=0;

    if ($file_size_quota>0 AND $file_size > $file_size_quota){
        OW::getFeedback()->error(OW::getLanguage()->text('music', 'file_size_quota').": ".$file_size_quota ." [byte]");
        OW::getApplication()->redirect($curent_url."music/add");
        exit;
    }

/*<key name="unlimited"><value>Unlimited</value></key>
    $mime_filter = array(
      'image/gif', 'image/jpeg', 'image/png', 
      'application/pdf',
      'audio/mpeg', 'audio/mpeg3', 'audio/x-mpeg', 'audio/x-mpeg-3','audio/mp3');
*/
    $mime_filter = array(
      'audio/mpeg', 'audio/mpeg3', 'audio/x-mpeg', 'audio/x-mpeg-3','audio/mp3');

    if (!in_array($file_type, $mime_filter)) {
        OW::getFeedback()->error(OW::getLanguage()->text('music', 'file_is_not_mp3'));
        OW::getApplication()->redirect($curent_url."music/add");
        exit;
    }


                        $this->dir_mkdir($pluginUserFileDIR.$for_user);
                        $this->file_copy($_FILES['mp3']['tmp_name'],$pluginUserFileDIR.$for_user.DS.$_FILES['mp3']['name']);

//echo $pluginUserFileDIR.$id_user.DS.$_FILES['mp3']['name'];exit;
                        $url_external="";
                        $file_external=$_FILES['mp3']['name'];
                        $file_ex_change=" u_file='".addslashes($file_external)."', ";
                    }else{
                        if (isset($_POST['idpagesave']) AND $_POST['idpagesave']>0 AND $_POST['idpagesave']==$params['id'] ){//update
                            //is updatye
                        }else{
                            OW::getFeedback()->error(OW::getLanguage()->text('music', 'fill_all_required_fields'));
                            OW::getApplication()->redirect($curent_url."music/add");
                            exit;
                        }
                    }
                }else{
                        if (isset($_POST['idpagesave']) AND $_POST['idpagesave']>0 AND $_POST['idpagesave']==$params['id'] ){//update
                            //is updatye
                        }else{
                            OW::getFeedback()->error(OW::getLanguage()->text('music', 'fill_all_required_fields'));
                            OW::getApplication()->redirect($curent_url."music/add");
                            exit;
                        }
                }

//print_r($_POST);exit;



            if (isset($_POST['idpagesave']) AND $_POST['idpagesave']>0 AND $_POST['idpagesave']==$params['id'] ){//update
                $addif="";
                if (!$is_admin){
                    $addif =" AND id_owner='".addslashes($for_user)."' ";
                }


//--del f st
                if ($url_external){;
                    if ($is_admin){
                        $ww="";
                    }else{
                        $ww=" AND id_owner='".$for_user."' ";
                    }

                    $sql = "SELECT * FROM " . OW_DB_PREFIX. "music
                    WHERE id='".addslashes($params['id'])."' ".$ww." LIMIT 1";
                    $arrl = OW::getDbo()->queryForList($sql);
                    if (isset($arrl[0]) AND isset($arrl[0]['id']) AND $arrl[0]['id']>0){
                        if (!$arrl[0]['u_url'] AND ($arrl[0]['u_file'] AND $this->file_exist($pluginUserFileDIR.$arrl[0]['id_owner']."/".stripslashes($arrl[0]['u_file'])))) {
                            $this->file_delete($pluginUserFileDIR.$arrl[0]['id_owner']."/".stripslashes($arrl[0]['u_file']));
                        }
                    }
                }
//--del f end


                $auto_play=0;
                $auto_play_add="";
                if (isset($_POST['autoplayset']) AND $_POST['autoplayset']=="1"){
                    $sql="UPDATE " . OW_DB_PREFIX. "music SET auto_play='0' 
                    WHERE id_owner='".addslashes($for_user)."' ";
                    OW::getDbo()->query($sql);
                    $auto_play=1;
                    $auto_play_add=", auto_play='1' ";
                }else if (isset($_POST['autoplay_isset']) AND $_POST['autoplay_isset']=="1"){
                    $sql="UPDATE " . OW_DB_PREFIX. "music SET auto_play='0' 
                    WHERE id_owner='".addslashes($for_user)."' ";
                    OW::getDbo()->query($sql);
                    $auto_play=0;
                    $auto_play_add=" ";
                }


//                    u_file='".addslashes($file_external)."',
                $sql="UPDATE " . OW_DB_PREFIX. "music SET 
                    u_title='".addslashes($title)."',
                    u_order='".addslashes($u_order)."',
                    u_url='".addslashes($url_external)."',
                    display_in_profile='".addslashes($display_in_profile)."',
                    u_author='".addslashes($u_author)."',
".$file_ex_change."
                    u_desc='".addslashes($mdesc)."',
                    add_date='".addslashes($timestamp)."'  
                    ".$auto_play_add." 
                WHERE id='".addslashes($_POST['idpagesave'])."' ".$addif;
                OW::getDbo()->query($sql);

            }else if ($_POST['idpagesave']=="new"){//new

                $auto_play=0;
                if (isset($_POST['autoplayset']) AND $_POST['autoplayset']=="1"){
                    $sql="UPDATE " . OW_DB_PREFIX. "music SET auto_play='0' 
                    WHERE id_owner='".addslashes($for_user)."' ";
                    OW::getDbo()->query($sql);
                    $auto_play=1;
                }else if (isset($_POST['autoplay_isset']) AND $_POST['autoplay_isset']=="1"){
                    $sql="UPDATE " . OW_DB_PREFIX. "music SET auto_play='0' 
                    WHERE id_owner='".addslashes($for_user)."' ";
                    OW::getDbo()->query($sql);
                    $auto_play=0;
                    $auto_play_add=" ";
                }

                $sql="INSERT INTO " . OW_DB_PREFIX. "music (
                    id ,active ,id_owner ,
                    auto_play,  share,   add_date  ,
                    u_title, u_author  ,      u_order ,u_url  , u_file , u_desc,
                    display_in_profile
                )VALUES(
                    '0','1','".addslashes($for_user)."',
                    '".addslashes($auto_play)."','public','".addslashes($timestamp)."',
                    '".addslashes($title)."','".addslashes($u_author)."','".addslashes($u_order)."','".addslashes($url_external)."','".addslashes($file_external)."','".addslashes($mdesc)."',
                    '".addslashes($display_in_profile)."'
                )";
                $newid = OW::getDbo()->insert($sql);

            }


            OW::getFeedback()->info(OW::getLanguage()->text('music', 'saved_succesfull'));
            OW::getApplication()->redirect($curent_url."music/mymusic");
            exit;
        }

        if ($is_admin){
            $ww="";
        }else{
            $ww=" AND id_owner='".$id_user."' ";
        }


        $mdesc="";
        $title="";
        $url_external="";
        $file_external="";
        $auto_play=0;
        $display_in_profile=1;
        $artist="";

        if ($params['id']>0){
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "music
            WHERE id='".addslashes($params['id'])."' ".$ww." LIMIT 1";
            $arrl = OW::getDbo()->queryForList($sql);
            if (isset($arrl[0]) AND isset($arrl[0]['id']) AND $arrl[0]['id']>0){
                $value=$arrl[0];
                $mdesc=stripslashes($value['u_desc']);
                $title=stripslashes($value['u_title']);
                $url_external=stripslashes($value['u_url']);
                $file_external=stripslashes($value['u_file']);
                $auto_play=stripslashes($value['auto_play']);
                $display_in_profile=stripslashes($value['display_in_profile']);
                $artist=stripslashes($value['u_author']);
            }
        }

            $full_path="";
            $full_name="";
            if ($url_external){
                $full_path=$url_external;
                $full_name="";
            }else{
                $full_path="";
                $full_name=$file_external;
//                $url=$pluginUserFileURL.$value['id_owner'].DS.stripslashes($value['u_file']);
            }

            


            $content .="<form action=\"".$curent_url."music/edit/".$id_page."/".$params['ss']."\" method=\"POST\" enctype=\"multipart/form-data\">";

            $content .="<input type=\"hidden\" name=\"idpagesave\" value=\"".$id_page."\">";
            $content .="<input type=\"hidden\" name=\"ss\" value=\"".$params['ss']."\">";
            $content .="<input type=\"hidden\" name=\"do_save\" value=\"true\">";
            $content .="<table style=\"width:100%;margin:auto\">";


            $content .="<tr>";
            $content .="<td align=\"right\" style=\"width:150px;\">";
            $content .=OW::getLanguage()->text('music', 'set_as_autoplay').":";
            $content .="</td>";
            $content .="<td>";
            if ($auto_play==1) $sel=" CHECKED ";
                else $sel="";
            $content .="<input ".$sel." type=\"checkbox\" name=\"autoplayset\" value=\"1\" style=\"\">&nbsp;".OW::getLanguage()->text('music', 'set_as_autoplay_text');

            if ($auto_play==1){
                $content .="<input type=\"hidden\" name=\"autoplay_isset\" value=\"1\" style=\"\">";
            }

            $content .="</td>";
            $content .="</tr>";


            $content .="<tr>";
            $content .="<td align=\"right\" style=\"width:150px;\">";
            $content .=OW::getLanguage()->text('music', 'display_in_profile').":";
            $content .="</td>";
            $content .="<td>";
            if ($display_in_profile==1) $sel=" CHECKED ";
                else $sel="";
            $content .="<input ".$sel." type=\"checkbox\" name=\"display_in_profile\" value=\"1\" style=\"\">&nbsp;".OW::getLanguage()->text('music', 'set_as_display_in_profile');

            $content .="</td>";
            $content .="</tr>";

            $content .="<tr>";
            $content .="<td align=\"right\" style=\"width:150px;\">";
            $content .=OW::getLanguage()->text('music', 'title').":";
            $content .="</td>";
            $content .="<td>";
            $content .="<input type=\"text\" name=\"u_title\" value=\"".$title."\" style=\"width:100%;\" placeholder=\"".OW::getLanguage()->text('music', 'mp3_title_placeholder')."\">";
            $content .="</td>";
            $content .="</tr>";

            $content .="<tr>";
            $content .="<td align=\"right\" style=\"width:150px;\">";
            $content .=OW::getLanguage()->text('music', 'artist').":";
            $content .="</td>";
            $content .="<td>";
            $content .="<input type=\"text\" name=\"u_artist\" value=\"".$artist."\" style=\"width:100%;\"  placeholder=\"Michael Jackson\">";
            $content .="</td>";
            $content .="</tr>";

            $content .="<tr>";
            $content .="<td colspan=\"2\" style=\"height:25px;\">";
            $content .="</td>";
            $content .="</tr>";


            $content .="<tr>";
            $content .="<td align=\"right\" style=\"width:150px;\">";
            $content .=OW::getLanguage()->text('music', 'edit_external_url').":";
            $content .="</td>";
            $content .="<td>";
            $content .="<input type=\"text\" name=\"url_external\" value=\"".$full_path."\" style=\"width:100%;\" placeholder=\"http://www.URL_TO_MP3.com/mp3_file.mp3\">";
            $content .="</td>";
            $content .="</tr>";

            $content .="<tr>";
            $content .="<td align=\"right\" style=\"width:150px;\">";
            $content .="<b>---- ".OW::getLanguage()->text('music', 'or')." ----</b>";
            $content .="</td>";
            $content .="<td>";
            $content .="</td>";
            $content .="</tr>";


            $content .="<tr>";
            $content .="<td align=\"right\" style=\"width:150px;\">";
            $content .=OW::getLanguage()->text('music', 'upload_mp3file').":";
            $content .="</td>";
            $content .="<td>";
            if (OW::getConfig()->getValue('music', 'file_size_quota')>0){
                $quota=OW::getConfig()->getValue('music', 'file_size_quota');
                $quota=$quota/1024/1024;
                $content .="<input type=\"file\" name=\"mp3\" value=\"\" style=\"\" placeholder=\"\">&nbsp;".OW::getLanguage()->text('music', 'max_file_size').": ".number_format($quota,2)." [MB]";
            }else{
                $content .="<input type=\"file\" name=\"mp3\" value=\"\" style=\"\" placeholder=\"\">&nbsp;".OW::getLanguage()->text('music', 'max_file_size').": ".OW::getLanguage()->text('music', 'unlimited');
            }

            if ($full_name){
                $content .="<br/>";
                $content .="<i><b>".$full_name."</b></i>";
//                $content .="&nbsp;<input type=\"checkbox\" name=\"delete_it\" value=\"del_".$id_page."\">&nbsp;<b class=\"ow_red\">".OW::getLanguage()->text('music', 'delete')."</b>";
            }
            $content .="<br/>";
                $content .="<br/>";
            $content .="</td>";
            $content .="</tr>";


/*
//        if (OW::getConfig()->getValue('html', 'allow_meta_tags')=="1"){
            $content .="<tr>";
            $content .="<td align=\"right\" style=\"width:150px;\">";
            $content .=OW::getLanguage()->text('music', 'description').":";
            $content .="</td>";
            $content .="<td>";
            $content .="<textarea name=\"mdesc\" id=\"mdesc\" style=\"width:100%;height:200px;min-height:100px;\">".$mdesc."</textarea>";
            $content .="</td>";
            $content .="</tr>";
//        }
*/
        $content .="<tr>";
        $content .="<td align=\"right\" style=\"width:150px;\">";
        $content .=OW::getLanguage()->text('music', 'description').":";
        $content .="</td>";
        $content .="<td>";
//        $content .="<textarea class=\"html\" name=\"content\" style=\"width:100%;height:350px\">".$content_edit."</textarea>";
//--------editor s
                $fname="mdesc";
                $minheight=450;

//                if (!OW::getPluginManager()->isPluginActive('wysiwygeditor') AND !class_exists('WysiwygTextarea')){
//                    include_once(OW_DIR_CORE."form_element.php");
//                }

//                $content_t.="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\">".$description."</textarea>";
                if ( OW::getPluginManager()->isPluginActive('wysiwygeditor') ){
//                    $ret.="<textarea class=\"html\" name=\"".$name."\" id=\"".$name."\" style=\"\">".$description."</textarea>";
                    $content .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:350px;min-height:450px;\" placeholder=\"".OW::getLanguage()->text('music', 'enter_desc_lyrics')."\">".$content_edit."</textarea>";
                }else if (class_exists('WysiwygTextarea')){

//        $body = new Textarea($fname);
        $body = new WysiwygTextarea($fname,array( BOL_TextFormatService::WS_BTN_IMAGE, BOL_TextFormatService::WS_BTN_VIDEO ), true);
        $body->setValue($mdesc);
        $body->addAttribute('id',$fname);
        $body->setHasInvitation(true);
        $body->setSize($minheight);//SIZE_S,SIZE_M,SIZE_L 
        $content .=$body->renderInput();

                }else if (class_exists('MUSIC_CLASS_Wysiwygtextarea')){

        $body = new MUSIC_CLASS_Wysiwygtextarea($fname,array( BOL_TextFormatService::WS_BTN_IMAGE, BOL_TextFormatService::WS_BTN_VIDEO ), true);
        $body->setValue($mdesc);
        $body->addAttribute('id',$fname);
        $body->setHasInvitation(true);
        $body->setSize($minheight);//SIZE_S,SIZE_M,SIZE_L 
        $content .=$body->renderInput();


                }else{
                    $content .="<textarea class=\"html\" name=\"".$fname."\" id=\"".$fname."\" style=\"width:100%;height:350px;min-height:450px;\" placeholder=\"".OW::getLanguage()->text('music', 'enter_desc_lyrics')."\">".$content_edit."</textarea>";
                }
//--------editor e

        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td align=\"right\" style=\"width:150px;\">";
        $content .="</td>";
        $content .="<td>";
//        $content .="<input type=\"submit\" name=\"".OW::getLanguage()->text('html', 'edit_save')."\" value=\"Save\">";
        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" name=\"save\" value=\"".OW::getLanguage()->text('music', 'save')."\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
        $content .="</td>";
        $content .="</tr>";

        $content .="</table>";
        $content .="</form>";
/*
        }else{
//            OW::getFeedback()->info(OW::getLanguage()->text('music', 'you_donthave_permission'));
            OW::getFeedback()->error(OW::getLanguage()->text('music', 'you_donthave_permission'));
            OW::getApplication()->redirect($curent_url."music");
            exit;
        }
*/

        return $content;
    }

    public function get_list ($oplist="")
    {
        $content="";
        $script="";
        $css="";
        $curent_url=OW_URL_HOME;

        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
//        $pluginStaticURL=OW::getPluginManager()->getPlugin('music')->getStaticUrl();
        $pluginUserFileURL=OW::getPluginManager()->getPlugin('music')->getUserFilesUrl();
        $pluginUserFileDIR=OW::getPluginManager()->getPlugin('music')->getUserFilesDir();

//        $perpage=10;

        $add="";
        if ($oplist=="mymusic" AND $id_user>0){
            $add=" id_owner='".addslashes($id_user)."' ";
        }else if (!$is_admin){
            $add=" active='1' AND share='public' ";
        }else{
            $add=" 1 ";
        }


                $add_query="";
                if (isset($_GET['q']) AND $_GET['q']) {
                    $add_query=" AND 
                    (u_title LIKE '%".addslashes($_GET['q'])."%' OR u_author LIKE '%".addslashes($_GET['q'])."%' 
                        OR 
                        LOWER(u_title) LIKE '%".addslashes($_GET['q'])."%' OR LOWER(u_author) LIKE '%".addslashes($_GET['q'])."%' 
                    )";
                }


//-------------------page start A
    $perpage_max=OW::getConfig()->getValue('music', 'perpage');
    if (!$perpage_max OR $perpage_max==0) $perpage_max=30;
    if (isset($_GET['page'])){
        $curent_page=($_GET['page']-1);
        if (!$curent_page) $curent_page=0;
    }else{
        $curent_page=0;
    }

    $prev_page=$curent_page-1;
    if ($prev_page<0) $prev_page=0;
    $next_page=$curent_page+1;
    $add_limit=($curent_page*$perpage_max);
    if (!$add_limit) $add_limit=0;
    $pagination="";


/*
        $query = "SELECT pr.id as allp FROM " . OW_DB_PREFIX. "shoppro_products pr
            LEFT JOIN " . OW_DB_PREFIX. "shoppro_products_options po ON (po.id_product=pr.id AND (po.items>'0' OR po.unlimited='1') AND po.active='1' )
        WHERE ".$add_where." GROUP BY pr.id";
*/
        $query = "SELECT id as allp FROM " . OW_DB_PREFIX. "music 
            WHERE ".$add.$add_query;



//echo $query;exit;
        $arrx = OW::getDbo()->queryForList($query);
        if (isset($arrx[0])){
            $aaitem=$arrx[0];
//            $all_items=$aaitem['allp'];
            $all_items=count($arrx);
        }else{
            $all_items=0;
        }
//-------------------page end A




            $sql = "SELECT * FROM " . OW_DB_PREFIX. "music
            WHERE ".$add." ".$add_query." ORDER BY auto_play DESC, u_title LIMIT ".$add_limit.",".$perpage_max;
//echo $sql;exit;
            $arrl = OW::getDbo()->queryForList($sql);
            $ret_items="";
            foreach ( $arrl as $value ){

                $dname=BOL_UserService::getInstance()->getDisplayName($value['id_owner']);
                $uurl=BOL_UserService::getInstance()->getUserUrl($value['id_owner']);
                $uimg=BOL_AvatarService::getInstance()->getAvatarUrl($value['id_owner']);
                if (!$uimg) {
                    $uimg=$curent_url."ow_static/themes/".OW::getConfig()->getValue('base', 'selectedTheme')."/images/no-avatar.png";
                }


//                $title=mb_substr($this->html2txt(stripslashes($value['u_title'])),0,255);
//                $description=mb_substr($this->html2txt(stripslashes($value['u_desc'])),0,255);
//                $description=$this->html2txt(stripslashes($value['u_desc']));
//                $description=stripslashes($value['u_desc']);

                $title=UTIL_String::truncate(stripslashes($value['u_title']), 100, '...');
                $description=UTIL_String::truncate(stripslashes($value['u_desc']), 255, '...');

                $author=UTIL_String::truncate(stripslashes($value['u_author']), 64, '...');

//                $url=stripslashes($value['u_url']).stripslashes($value['u_file']);

                if ($value['u_url']){
                    $url=stripslashes($value['u_url']);
                }else{
                    $url=$pluginUserFileURL.$value['id_owner']."/".stripslashes($value['u_file']);
                }

                $file_ok=true;


                if ( $value['u_url'] AND strlen($value['u_url'])<10) {
                    //not corect URL
                    $file_ok=false;
                }else if (!$value['u_url'] AND (!$value['u_file'] OR !$this->file_exist($pluginUserFileDIR.$value['id_owner']."/".stripslashes($value['u_file'])))) {
                    //mp3 niot exist
                    $file_ok=false;
                }

//echo $this->file_exist($pluginUserFileDIR.$value['id_owner']."/".stripslashes($value['u_file']));

//echo $pluginUserFileDIR.$value['id_owner']."/".stripslashes($value['u_file']);
//print_r($value);
//exit;
                if ($file_ok==true  OR ($file_ok==false AND $oplist=="mymusic" AND $id_user>0)){

                $ret_items .="<tr>";
                $ret_items .="<td style=\"max-width:45px;\">";

                    $ret_items .="<div class=\"clearfix\">";
                        $ret_items .="<div class=\"clearfix ow_center\">";
                            $ret_items .="<a href=\"".$uurl."\">";
                            $ret_items .="<img src=\"".$uimg."\" alt=\"".$dname."\" title=\"".$dname."\" width=\"45px\" style=\"border:0;\" align=\"left\" >";
                            $ret_items .="</a>";
                        $ret_items .="</div>";
                    $ret_items .="</div>";

                $ret_items .="</td>";
                $ret_items .="<td style=\"width:100%;margin:auto;\">";


                $auto_play="";
                $display_in_profile="";

                if ($id_user>0){

                    if ($value['auto_play']==1){
//                    $auto_play="<span style=\"margin:0 5px 0 5px;\">[".OW::getLanguage()->text('music', 'autoplay')."]</span>";
//                    $auto_play="<a class=\"ow_lbutton ow_green\" href=\"javascript:void(0);\" style=\"margin-right:5px;\">".OW::getLanguage()->text('music', 'autoplay')."</a>";
                        $auto_play="<span class=\"ow_lbutton ow_red\" style=\"margin-right:5px;\">".OW::getLanguage()->text('music', 'autoplay')."</span>";
                    }


                    if ($value['display_in_profile']==1){
//                    $display_in_profile="<span style=\"margin:0 5px 0 5px;\">[".OW::getLanguage()->text('music', 'display_in_profile')."]</span>";
//                    $display_in_profile="<a class=\"ow_lbutton ow_blue\" href=\"javascript:void(0);\" style=\"margin-right:5px;\">".OW::getLanguage()->text('music', 'is_in_profile')."</a>";
                        $display_in_profile="<span class=\"ow_lbutton ow_green\" style=\"margin-right:5px;\">".OW::getLanguage()->text('music', 'is_in_profile')."</span>";
                    }
                }//end if $id_use

                    $ret_items .="<div class=\"clearfix\">";
                        $ret_items .="<h2 style=\"margin:0 0 5px 0;\">".stripslashes($value['u_title'])."</h2>";
                    $ret_items .="</div>";


                    $ret_items .="<div class=\"clearfix\">";
                        if ($file_ok==true){
                            $ret_items .="<audio id=\"player_".$value['id']."\" class=\"audio_music\" src=\"".$url."\" type=\"audio/mp3\" controls=\"control\" preload=\"none\" />";
                        }else{
                            $ret_items .="<i class=\"ow_red\">".OW::getLanguage()->text('music', 'fil_not_exist')."</i>";
                        }
                    $ret_items .="</div>";

//                    $ret_items .="<div class=\"clearfix\">";
//                    $ret_items .="</div>";
                    if ($auto_play){
                        $ret_items .=$auto_play;
                    }
                    if ($display_in_profile){
                         $ret_items .=$display_in_profile;
                    }


                    if ($is_admin OR $id_user==$value['id_owner']){

                        $ret_items .="<div class=\"ow_box_toolbar_cont clearfix ow_right\">
                            <div class=\"ow_box_toolbar ow_remark\">
                                <span style=\"\" class=\"ow_nowrap\">
                                    <a href=\"".$curent_url."music/edit/".$value['id']."/".substr(session_id(),3,5)."\" id=\"online_pop\">".OW::getLanguage()->text('music', 'edit')."</a>
                                </span>
                                <span style=\"\" class=\"ow_nowrap\">
                                    <a onclick=\"return confirm('".$this->corect_tojava(OW::getLanguage()->text('music', 'confirm_delete'))."');\"  href=\"".$curent_url."music/del/".$value['id']."/".substr(session_id(),3,5)."\" id=\"online_pop\" class=\"ow_red\">".OW::getLanguage()->text('music', 'delete')."</a>
                                </span>
                            </div>
                        </div>";
                    }


                    $ret_items .="<div class=\"clearfix\">";
                        $ret_items .=$description;
                    $ret_items .="</div>";

                        if ($author){
                            $ret_items .="<div class=\"clearfix\">";
                                $ret_items .="<div class=\"ow_right\">";
                                    $ret_items .="<i class=\"ow_small\">".OW::getLanguage()->text('music', 'artist').": ".$author."</i>";
                                $ret_items .="</div>";
                            $ret_items .="</div>";
                        }


                $ret_items .="</td>";
/*
                $ret_items .="<td style=\"max-width:200px;\">";

                    $ret_items .="<div style=\"float:right;\">
                    <a class=\"ow_lbutton\" href=\"http://test4.a6.pl/news/5/index.html\">".OW::getLanguage()->text('music', 'edit')."</a>
                    <a class=\"ow_lbutton ow_red\" href=\"http://test4.a6.pl/news/5/index.html\">".OW::getLanguage()->text('music', 'delete')."</a>
                </div>";


                $ret_items .="</td>";
*/
                $ret_items .="</tr>";

                }//else if end
            }


        $content .="<div class=\"clearfix\" id=\"main_music\" class=\"ow_hidden\">";
        if ($ret_items){

//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/music/mediaelement-and-player.js');
            OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/music/mediaelement-and-player.min.js');

//        OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/music/mediaelementplayer.min.css');
//            OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/music/mediaelementplayer.css');
            OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/music/mediaelementplayer_full.css');

$script="
$(document).ready(function() {
//    $('audio,video').mediaelementplayer({
//    $('.audio_music').mediaelementplayer({
    $('audio').mediaelementplayer({
        loop: true,
        audioWidth: '100%',
        audioHeight: '30px',
        enableAutosize: true,
        alwaysShowHours: true,
        showTimecodeFrameCount: true,
alwaysShowControls: true,

    iPadUseNativeControls: false,
    iPhoneUseNativeControls: false,
    AndroidUseNativeControls: false,

//alwaysShowControls: true,

//setDimensions: true,


        features: ['playpause','backlight','progress','current','duration','tracks','volume','fullscreen','loop'],
        startVolume: 0.5,
        pauseOtherPlayers: true,
        enableKeyboard: true


/*
        autoplay:true,
        success: function (me) {
                me.play();
//            whenPlayerReadyToPlay(me, function () {
//                me.play();
//            })
        }
*/


    });
//    $('#player_1').play();

/*
//$('#stopall').click(function() {
    $('#player_1').each(function() {
          $(this)[0].player.play();              
    });
//});
*/

//    $('#main_music').show();

});
";
//$content.=$script;
OW::getDocument()->addOnloadScript($script);


    $orderby="";
    $url_pages="";
    if ($oplist=="mymusic" AND $id_user>0){
        $idcat="mymusic";
    }else{
        $idcat="";
    }

    if (isset($_GET['q']) AND $_GET['q']){
        $url_pages="q=".$_GET['q'];
    }

    $pagination=$this->makePagination(($curent_page+1), $all_items, OW::getConfig()->getValue('music', 'perpage'), 1, $curent_url."music/".$idcat."?".$url_pages,"&page=","right",$orderby);
//    $pagination_bottom=SHOPPRO_BOL_Service::getInstance()->makePagination(($curent_page+1), $all_items, OW::getConfig()->getValue('shoppro', 'mode_perpage'), 1, $curent_url."shoppro/".$idcat."?".$url_pages,"&page=","right");


            $content .=$pagination;

            $content .="<div clss=\"clearfix\">";
                if ($oplist=="mymusic" AND $id_user>0){
                    $content .="<form action=\"".$curent_url."music/mymusic\" method=\"get\">";
                }else{
                    $content .="<form action=\"".$curent_url."music\" method=\"get\">";
                }


        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">";

                $vv="";
                if (isset($_GET['q']) AND $_GET['q']) {
                    $vv=$_GET['q'];
                }
                $content .="<input class=\"ow_left\" type=\"text\" name=\"q\" value=\"".$vv."\" style=\"width: inherit;
    margin-right:5px;min-width:240px;\" >";

                    $content .="<span class=\"ow_button ow_left\" style=\"margin-right:5px;\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('music', 'search')."\" name=\"\" class=\"ow_ic_lens ow_positive\">
                        </span>

                    </span>";




                $content .="</div>
            </div>";

                $content .="</form>";
            $content .="</div>";

            $content .="<table class=\"ow_table_1 ow_form\" style=\"width:100%;margin:auto;\">";

            $content .="<tr>
                <th class=\"ow_name ow_txtleft\" colspan=\"\">
                <span class=\"ow_section_icon ow_ic_gear_wheel\">".OW::getLanguage()->text('music', 'owner')."</span>
                </th>
                <th class=\"ow_name ow_txtleft\" colspan=\"\">
                <span class=\"ow_section_icon ow_ic_gear_wheel\">".OW::getLanguage()->text('music', 'music_play')."</span>
                </th>";
/*
                $content .="<th class=\"ow_name ow_txtleft\" colspan=\"\">
                <span class=\"ow_section_icon ow_ic_gear_wheel\">".OW::getLanguage()->text('music', 'action')."</span>
                </th>";
*/
            $content .="</tr>";

            $content .=$ret_items;
            $content .="</table>";

            $content .=$pagination;

        }else{
                $content .=OW::getLanguage()->text('music', 'no_found');            
        }
        $content .="</div>";

        return $content;
    }

/*
$pluginStaticURL2=OW::getPluginManager()->getPlugin('topmenu')->getStaticUrl();


        $script="";
        OW::getDocument()->addOnloadScript($script);

*/



    public function image_resize($file_source="",$crop=false,$width=800,$height=600)
    {
        return $this->image_copy_resize($file_source,$file_source,$crop,$width,$height);
    }

    public function image_copy_resize($file_source="",$file_dest="",$crop=false,$width=800,$height=600)
    {
        if ($file_source AND $file_dest){
            $image = new UTIL_Image($file_source);
            $mainPhoto = $image ->resizeImage($width, $height,$crop) ->saveImage($file_dest);
            return true;
        }else{
            return false;
        }
    }

    public function corect_exif($path="")
    {
//        if (!OW::getConfig()->getValue('shoppro', 'corect_exif')){
//            return;
//        }
        
//         $storage = OW::getStorage();
//          if ( !$storage->isWritable($path) )
            


        if ($path AND is_file($path) AND function_exists('exif_read_data') AND function_exists('imagerotate')){
//        if ($path AND is_file($path) AND function_exists('exif_read_data')){
//            ini_set('exif.encode_unicode', 'UTF-8');
            $exif = exif_read_data($path);
            if($exif) {
                if (isset($exif['Orientation'])){
                    $ort = $exif['Orientation'];
                }else if (isset($exif['IFD0']['Orientation'])){
                    $ort = $exif['IFD0']['Orientation'];
                }else if (isset($exif['EXIF']['Orientation'])){
                    $ort = $exif['EXIF']['Orientation'];
                }

                if($ort != 1){
                    $img = imagecreatefromjpeg($path) or die('Error opening file '.$path);
                    $deg = 0;
                    switch ($ort) {
                        case 3:
                            $deg = 180;
                        break;
                        case 6:
                            $deg = 270;
                        break;
                        case 8:
                            $deg = 90;
                        break;
                    }
                    if ($deg) {
//                      $imgr = $this->imagerotateXY($img, $deg, 0);        
                      $imgr = imagerotate($img, $deg, 0);        
                    }else{
                      $imgr=$img;
                    }
                    imagejpeg($imgr, $path, 95);
                    imagedestroy($img);
                    imagedestroy($imgr);
                }
            } // if have the exif orientation info
//        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND extension_loaded('magickwand')){
//        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND function_exists("NewMagickWand")){
//        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND extension_loaded('imagick')){
        }else  if ($path AND is_file($path) AND function_exists('exif_read_data') AND class_exists("Imagick")){
            $exif = exif_read_data($path);
            if($exif) {
                if (isset($exif['Orientation'])){
                    $ort = $exif['Orientation'];
                }else if (isset($exif['IFD0']['Orientation'])){
                    $ort = $exif['IFD0']['Orientation'];
                }else if (isset($exif['EXIF']['Orientation'])){
                    $ort = $exif['EXIF']['Orientation'];
                }

                if($ort != 1){
                    $deg = 0;
                    switch ($ort) {
                        case 3:
                            $deg = -180;
                        break;
                        case 6:
                            $deg = -270;
                        break;
                        case 8:
                            $deg = -90;
                        break;
                    }
                    if ($deg) {
                        $imagick = new Imagick(); 
                        $imagick->readImage($path ); 
                        $imagick->rotateImage(new ImagickPixel('none'), $deg); 
                        $imagick->writeImage($path ); 
                        $imagick->clear(); 
                        $imagick->destroy(); 
                    }
                }
            }
        }
    }




    public function file_get_content($src="")
    {
        $ret="";
        if ($src){
            $storage = OW::getStorage();
            return $storage->fileGetContent($src);
        }else{
            return "";
        }
    }
    public function file_set_content($src="",$content="")
    {
        if ($src){
            $storage = OW::getStorage();
            $storage->fileSetContent($src,$content);
            return true;
        }else{
            return false;
        }
    }
    public function file_copy($src="",$dest="")
    {
        if ($src AND $dest){
            $storage = OW::getStorage();
            return $storage->copyFile($src,$dest);
        }else{
            return false;
        }
    }

    public function dir_copy($src="",$dest="", array $fileTypes = null, $level = -1)
    {
        if ($src AND $dest){
            $storage = OW::getStorage();
            if ( !$this->file_exist($dest) )
            {       
                $this->dir_mkdir($dest);
            }
            UTIL_File::copyDir($src, $dest, $fileTypes, $level);
        }else{
            return false;
        }
    }

    public function file_delete($src="")
    {
        if ($src){
            $storage = OW::getStorage(); 
            if ( $storage->fileExists($src) )
            {
                $storage->removeFile($src);
            }
            return true;
        }else{
            return false;
        }
    }

    public function dir_mkdir($src="")
    {
        if ($src){
            $storage = OW::getStorage(); 
            if ( !$storage->isWritable($src) )
            {
                if ( $storage->mkdir($src) )
                {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function file_exist($src="")
    {
        if ($src){
            $storage = OW::getStorage(); 
            if ( $storage->fileExists($src) )
            {
                return true;
            }else {
                return false;
            }
        }else{
            return false;
        }
    }
    public function get_plugin_dir($plugin="")
    {
        if ($plugin){
            return OW::getPluginManager()->getPlugin($plugin)->getUserFilesDir();
        }else{
            return false;
        }
    }
    public function get_plugin_url($plugin="")
    {
        if ($plugin){
            return OW::getStorage()->getFileUrl(OW::getPluginManager()->getPlugin($plugin)->getUserFilesDir());
        }else{
            return false;
        }
    }


    public function corect_tojava($content){
        $content=str_replace("'","",$content);
        $content=str_replace("+","\+",$content);
        $content=str_replace("\r\n","",$content);
        $content=str_replace("\n","",$content);
        $content=str_replace("\r","",$content);
        return $content;
    }


public function escapeJsonString($value) { 
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}
        
     public function html2txt($document){
        $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
        );
        $text = preg_replace($search, '', $document);
        $text=preg_replace("/(&?!amp;)/i", " ", $text);
        $text=preg_replace("/(&#\d+);/i", " ", $text); // For numeric entities
        $text=preg_replace("/(&\w+);/i", " ", $text); // For literal entities
        return $text;
    }

public function valid_xml ($str) {
    $str = str_replace("\"", "&quot;", $str);   
    $str = str_replace("<", "&lt;", $str);  
    $str = str_replace(">", "&gt;", $str);  
    $str = preg_replace("/&(?![a-zA-Z0-9#]+?;)/", "&amp;", $str);
    return $str;
}


    public function toASCII( $str="" )
    {
        return strtr(utf8_decode($str), 
            utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
            'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
    }





    private function code2utf($num){
    if($num<128)return chr($num);
    if($num<2048)return chr(($num>>6)+192).chr(($num&63)+128);
    if($num<65536)return chr(($num>>12)+224).chr((($num>>6)&63)+128).chr(($num&63)+128);
    if($num<2097152)return chr(($num>>18)+240).chr((($num>>12)&63)+128).chr((($num>>6)&63)+128). chr(($num&63)+128);
    return '';
    }
    private function  deCP1252 ($str) {
    $str = str_replace("&#128", "&#8364;", $str);
    $str = str_replace("&#129", "", $str);
    $str = str_replace("&#130;", "&#8218;", $str);
    $str = str_replace("&#131;", "&#402;", $str);
    $str = str_replace("&#132;", "&#8222;", $str);
    $str = str_replace("&#133;", "&#8230;", $str);
    $str = str_replace("&#134;", "&#8224;", $str);
    $str = str_replace("&#135;", "&#8225;", $str);
    $str = str_replace("&#136;", "&#710;", $str);
    $str = str_replace("&#137;", "&#8240;", $str);
    $str = str_replace("&#138;", "&#352;", $str);
    $str = str_replace("&#139;", "&#8249;", $str);
    $str = str_replace("&#140;", "&#338;", $str);
    $str = str_replace("&#145;", "&#8216;", $str);
    $str = str_replace("&#146;", "&#8217;", $str);
    $str = str_replace("&#147;", "&#8220;", $str);
    $str = str_replace("&#148;", "&#8221;", $str);
    $str = str_replace("&#149;", "&#8226;", $str);
    $str = str_replace("&#150;", "&#8211;", $str);
    $str = str_replace("&#151;", "&#8212;", $str);
    $str = str_replace("&#152;", "&#732;", $str);
    $str = str_replace("&#153;", "&#8482;", $str);
    $str = str_replace("&#154;", "&#353;", $str);
    $str = str_replace("&#155;", "&#8250;", $str);
    $str = str_replace("&#156;", "&#339;", $str);
    $str = str_replace("&#159;", "&#376;", $str);
    return $str;
    }

public function str2utf8($str) {
    mb_detect_order("ASCII, UTF-8, ISO-8859-1");
    if (mb_detect_encoding($str) == "UTF-8") {  
        preg_match_all("/&#([0-9]*?);/", $str, $unicode);
        foreach( $unicode[0] as $key => $value) {
            $str = preg_replace("/".$value."/", $this->code2utf($unicode[1][$key]), $str);
        }
        return $str;        
    } else {
        $mystr = $str;
        $str = "";
        for ($i = 0; $i < strlen($mystr); $i++ ) {
            $code = ord( $mystr[$i] );
            if ($code >= 128 && $code < 160) {
                $str .= "&#".$code.";";
            } else {             
                $str .= $this->code2utf($code);
            }
        }
        $str = $this->deCP1252($str);
        preg_match_all("/&#([0-9]*?);/", $str, $unicode);
        foreach( $unicode[0] as $key => $value) {
            $str = preg_replace("/".$value."/", $this->code2utf($unicode[1][$key]), $str);
        }
        
        return $str;
    }
} 

    public function clear_real_name($f_realname="")//clear 
    {
        $f_realname=strip_tags($f_realname,"");
        //$f_realname = ereg_replace("[^A-Za-z0-9 ]", "",$f_realname); 
        $f_realname=$this->str2utf8($f_realname);

        $von = array("ä","ö","ü","ß","Ä","Ö","Ü"," ","é");  //to correct double whitepaces as well
        $zu  = array("&auml;","&ouml;","&uuml;","&szlig;","&Auml;","&Ouml;","&Uuml;","&nbsp;","&#233;");
        $f_realname = str_replace($from, $to, $f_realname);

        $f_realname=preg_replace("~[^A-Za-z0-9 (),]~", "", $f_realname);
        return $f_realname;
    }

    public function make_seo_url($name,$lengthtext=100)
    {
        $seo_sep="_";//- or _
        $seo_title=stripslashes($name);

$seo_title = preg_replace(array('/\s{2,}/', '/[\t\n]/'), $seo_sep, $seo_title);

    $seo_title=str_ireplace("\n"," ",$seo_title);
    $seo_title=str_ireplace("\t"," ",$seo_title);

        $seo_title=str_ireplace("_"," ",$seo_title);
        $seo_title=str_ireplace("  "," ",$seo_title);
        $seo_title=str_ireplace(" ",$seo_sep,$seo_title);
        $seo_title=str_ireplace(chr(160),$seo_sep,$seo_title);
        $seo_title=str_ireplace("~","",$seo_title);
        $seo_title=str_ireplace("(","",$seo_title);
        $seo_title=str_ireplace(")","",$seo_title);
        $seo_title=str_ireplace("]","",$seo_title);
        $seo_title=str_ireplace("[","",$seo_title);
        $seo_title=str_ireplace("}","",$seo_title);
        $seo_title=str_ireplace("{","",$seo_title);
        $seo_title=str_ireplace("/","",$seo_title);
        $seo_title=str_ireplace("\\","",$seo_title);
        $seo_title=str_ireplace("+","",$seo_title);
        $seo_title=str_ireplace(":","",$seo_title);
        $seo_title=str_ireplace(";","",$seo_title);
        $seo_title=str_ireplace("\"","",$seo_title);
        $seo_title=str_ireplace("<","",$seo_title);
        $seo_title=str_ireplace(">","",$seo_title);
        $seo_title=str_ireplace("?","",$seo_title);
        $seo_title=str_ireplace(",",".",$seo_title);
        $seo_title=str_ireplace("!","",$seo_title);
        $seo_title=str_ireplace("`","",$seo_title);
        $seo_title=str_ireplace("'","",$seo_title);
        $seo_title=str_ireplace("@","",$seo_title);
        $seo_title=str_ireplace("#","",$seo_title);
        $seo_title=str_ireplace("$","",$seo_title);
        $seo_title=str_ireplace("%","",$seo_title);
        $seo_title=str_ireplace("^","",$seo_title);
        $seo_title=str_ireplace("&","",$seo_title);
        $seo_title=str_ireplace("*","",$seo_title);
        $seo_title=str_ireplace("|","",$seo_title);
        $seo_title=str_ireplace("=","",$seo_title);
        $seo_title=str_ireplace(" ",$seo_sep,$seo_title);
        $seo_title=str_ireplace("/","",$seo_title);
        $seo_title=str_ireplace("?",$seo_sep,$seo_title);
        $seo_title=str_ireplace("#",$seo_sep,$seo_title);
        $seo_title=str_ireplace("=",$seo_sep,$seo_title);
        $seo_title=str_ireplace("=",$seo_sep,$seo_title);
        $seo_title=str_ireplace("&amp;",$seo_sep,$seo_title);
        $seo_title=str_ireplace($seo_sep.$seo_sep,$seo_sep,$seo_title);
        $seo_title=str_ireplace($seo_sep.$seo_sep,$seo_sep,$seo_title);
        $seo_title=str_ireplace($seo_sep.$seo_sep,$seo_sep,$seo_title);

        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }

    public function make_seo_urlOLD2($name,$lengthtext=100)
    {
        $seo_sep="-";//- or _
        $seo_title=stripslashes($name);
        $seo_title=str_ireplace($seo_sep," ",$seo_title);
        $seo_title=str_ireplace("_"," ",$seo_title);
        $seo_title=str_ireplace("  "," ",$seo_title);
        $seo_title=str_ireplace(" ",$seo_sep,$seo_title);
        $seo_title=str_ireplace(chr(160),$seo_sep,$seo_title);
        $seo_title=str_ireplace("~","",$seo_title);
        $seo_title=str_ireplace("(","",$seo_title);
        $seo_title=str_ireplace(")","",$seo_title);
        $seo_title=str_ireplace("]","",$seo_title);
        $seo_title=str_ireplace("[","",$seo_title);
        $seo_title=str_ireplace("}","",$seo_title);
        $seo_title=str_ireplace("{","",$seo_title);
        $seo_title=str_ireplace("/","",$seo_title);
        $seo_title=str_ireplace("\\","",$seo_title);
        $seo_title=str_ireplace("+","",$seo_title);
        $seo_title=str_ireplace(":","",$seo_title);
        $seo_title=str_ireplace(";","",$seo_title);
        $seo_title=str_ireplace("\"","",$seo_title);
        $seo_title=str_ireplace("<","",$seo_title);
        $seo_title=str_ireplace(">","",$seo_title);
        $seo_title=str_ireplace("?","",$seo_title);
        $seo_title=str_ireplace(",",".",$seo_title);
        $seo_title=str_ireplace("!","",$seo_title);
        $seo_title=str_ireplace("`","",$seo_title);
        $seo_title=str_ireplace("'","",$seo_title);
        $seo_title=str_ireplace("@","",$seo_title);
        $seo_title=str_ireplace("#","",$seo_title);
        $seo_title=str_ireplace("$","",$seo_title);
        $seo_title=str_ireplace("%","",$seo_title);
        $seo_title=str_ireplace("^","",$seo_title);
        $seo_title=str_ireplace("&","",$seo_title);
        $seo_title=str_ireplace("*","",$seo_title);
        $seo_title=str_ireplace("|","",$seo_title);
        $seo_title=str_ireplace("=","",$seo_title);

        $seo_title=str_ireplace("  "," ",$seo_title);
        $seo_title=str_ireplace(" ",$seo_sep,$seo_title);
        $seo_title=str_ireplace("/","",$seo_title);
        $seo_title=str_ireplace("?",$seo_sep,$seo_title);
        $seo_title=str_ireplace("#",$seo_sep,$seo_title);
        $seo_title=str_ireplace("=",$seo_sep,$seo_title);
        $seo_title=str_ireplace("=",$seo_sep,$seo_title);
        $seo_title=str_ireplace("&amp;",$seo_sep,$seo_title);

        $seo_title=str_ireplace($seo_sep.$seo_sep,$seo_sep,$seo_title);
        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }

    public function make_seo_urlOLD($name,$lengthtext=100)
    {
        $seo_title=stripslashes($name);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace(chr(160),"_",$seo_title);
        $seo_title=str_replace("~","",$seo_title);
        $seo_title=str_replace("(","",$seo_title);
        $seo_title=str_replace(")","",$seo_title);
        $seo_title=str_replace("]","",$seo_title);
        $seo_title=str_replace("[","",$seo_title);
        $seo_title=str_replace("}","",$seo_title);
        $seo_title=str_replace("{","",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("\\","",$seo_title);
        $seo_title=str_replace("+","",$seo_title);
        $seo_title=str_replace(":","",$seo_title);
        $seo_title=str_replace(";","",$seo_title);
        $seo_title=str_replace("\"","",$seo_title);
        $seo_title=str_replace("<","",$seo_title);
        $seo_title=str_replace(">","",$seo_title);
        $seo_title=str_replace("?","",$seo_title);
        $seo_title=str_replace(",",".",$seo_title);
        $seo_title=str_replace("!","",$seo_title);
        $seo_title=str_replace("`","",$seo_title);
        $seo_title=str_replace("'","",$seo_title);
        $seo_title=str_replace("@","",$seo_title);
        $seo_title=str_replace("#","",$seo_title);
        $seo_title=str_replace("$","",$seo_title);
        $seo_title=str_replace("%","",$seo_title);
        $seo_title=str_replace("^","",$seo_title);
        $seo_title=str_replace("&","",$seo_title);
        $seo_title=str_replace("*","",$seo_title);
        $seo_title=str_replace("|","",$seo_title);
        $seo_title=str_replace("=","",$seo_title);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("?","_",$seo_title);
        $seo_title=str_replace("#","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("&amp;","_",$seo_title);
        $seo_title=str_replace("__","_",$seo_title);
        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }

/*
$string = 'This is image inside text string http://localhost/test/something.jpg?xml=10, http://google.com/logo.png'; 
function _callback($matches){
return '<img src="'.$matches[0].'" />'
}

echo preg_replace_callback('/https?:\/\/(.*?)\.(jpg|png|gif)(\?\w+=\w+)?/i', '_callback', $string);
*/


    public function text_to_html($text="",$title="",$oryginal_url="",$url="",$max_width=150,$max_height=150,$margin=5) { 

        $text=str_replace("\r\n","\n",$text);
        $text=str_replace("\n","<br/>",$text);

//        $text=preg_replace('/((http?:\/\/)(.*?)\.(jpg|png|gif))(\?\w+=\w+)?/i','//${3}.${4}', $text);
        $text=preg_replace('/((http?:\/\/)([a-zA-Z0-9\/\._\-]*)\.(jpg|png|gif))(\?\w+=\w+)?/i','//${3}.${4}', $text);

//        $text=preg_replace('/((https?:\/\/)(.*?)\.(jpg|png|gif))(\?\w+=\w+)?/i','//${3}.${4}', $text);
        $text=preg_replace('/((https?:\/\/)([a-zA-Z0-9\/\._\-]*)\.(jpg|png|gif))(\?\w+=\w+)?/i','//${3}.${4}', $text);

//echo "---".$oryginal_url."--".$url;exit;

        $patterns = array();
//        $patterns[0] = '/(\/\/(.*?)\.(jpg|png|gif))(\?\w+=\w+)?/i';
        $patterns[0] = '/(\/\/([a-zA-Z0-9\/\._\-]*)\.(jpg|png|gif))(\?\w+=\w+)?/i';
        $patterns[1] = '/(..\/..\/ow_userfiles\/(.*?)\.(jpg|png|gif))(\?\w+=\w+)?/i';
        $patterns[2] = '/\/(image\/..\/ow_userfiles\/(.*?)\.(jpg|png|gif))(\?\w+=\w+)?/i';

        $replacements = array();
        if ($url){
            $replacements[0] = '<a href="'.$url.'" ><img src="//${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/></a>';
            $replacements[1] = '<a href="'.$url.'" ><img src="'.$oryginal_url.'${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/></a>';
            $replacements[2] = '<a href="'.$url.'" ><img src="'.$oryginal_url.'${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/></a>';
            $text=preg_replace($patterns, $replacements, $text);
        }else{
            $replacements[0] = '<img src="//${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/>';
            $replacements[1] = '<img src="'.$oryginal_url.'${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/>';
            $replacements[2] = '<img src="'.$oryginal_url.'${1}" style="max-width:'.$max_width.'px;max-height:'.$max_height.'px;float: left; margin:'.$margin.'px;" alt="'.$title.'" title="'.$title.'"/>';
            $text=preg_replace($patterns, $replacements, $text);
        }

        return $text;
    }
/*
RBL!!!

$host = '64.53.200.156';
$rbl  = 'sbl-xbl.spamhaus.org';
// valid query format is: 156.200.53.64.sbl-xbl.spamhaus.org
$rev = array_reverse(explode('.', $host));
$lookup = implode('.', $rev) . '.' . $rbl;
if ($lookup != gethostbyname($lookup)) {
    echo "ip: $host is listed in $rbl\n";
} else {
    echo "ip: $host NOT listed in $rbl\n";
}

*/

public function base64url_encode($data) { 
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 

public function base64url_decode($data) { 
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
} 

    public function safe_content($content="",$more=1){
        $content = preg_replace("/(<iframe[^<]+<\/iframe>)/", '', $content);
        $content = preg_replace('/<script\b[^>]*>(.*?)<\/script>/i', "", $content);
        $content = preg_replace('~<\s*\bscript\b[^>]*>(.*?)<\s*\/\s*script\s*>~is', '', $content);
        $content=str_replace("'","`",$content);
//        if ($more){
//            $content=addslashes($content);
//        }
        return $content;
    }

    public function clear_content($content="")
    {
        $content = preg_replace('/<[^>]*>/', '', $content);
        $content = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', strip_tags($content) );
        return $content;
    }

    public function safe_texttohtml($content=""){
        $content=preg_replace('/(http|https):\/\/(\S*)\.(jpg|gif|png)(\?(\S*))?(?=\s|$|\pP)/i', '<img src="http://$2.$3$4" border="0" alt="" style="max-width:100%;max-height:600px;"/>', $content);
        $content= preg_replace('/\s(\w+:\/\/)(\S+)/', ' <a href="\\1\\2" target="_blank" rel=\"nofollow\">\\1\\2</a>', $content);
        return $content;
    }

    public function safe_htmltotext($content=""){
/*
        $content=preg_replace('/(:?href=\")(.+?)(:?\")/', '$2', $content);
        $content=preg_replace('/(:?src=\")(.+?)(:?\")/', '$2', $content);
        $content=preg_replace('/(<)([img])(\w+)([^>]*>)/', '$2', $content);
*/
        $content=$this->safe_content($content);
        $content=preg_replace('/<a[^>]* href=\"([^\"]*)\"[^>]*>/', '$1 ', $content);
        $content=preg_replace('/<img[^>]* src=\"([^\"]*)\"[^>]*>/', '$1 ', $content);
        $content=$this->html2txt($content);
        return $content;
    }


    public function get_curect_lang_id()
    {
//print_r($_SESSION);exit;
//        if (isset($_SESSION['base.language_id']) AND $_SESSION['base.language_id']>0) return $_SESSION['base.language_id'];
//            else return 0;
        $clan=0;
        if (isset($_SESSION['base.language_id']) AND $_SESSION['base.language_id']>0) $clan= $_SESSION['base.language_id'];
        if (!$clan) $this->get_system_lang_id();

        return $clan;
    }

    public function get_system_lang_id()
    {
/*
            $sql = "SELECT bc.value, bl.id FROM " . OW_DB_PREFIX. "base_config bc 
    LEFT JOIN " . OW_DB_PREFIX. "base_language bl ON (bl.tag=bc.value) 
            WHERE `key`='locale' AND `name`='default_language' LIMIT 1";
//echo $sql;exit;
            $arr = OW::getDbo()->queryForList($sql);
            if (isset($arr[0]) AND isset($arr[0]['id']) AND $arr[0]['id']>0){
                return  $arr[0]['id'];
            }else{
                if (isset($_SESSION['base.language_id']) AND $_SESSION['base.language_id']>0) return $_SESSION['base.language_id'];
                    else return 0;
            }
*/
            $sql = "SELECT * FROM " . OW_DB_PREFIX. "base_language  
            WHERE `status`='active' ORDER BY `order` LIMIT 1";

            $arr = OW::getDbo()->queryForList($sql);
            if (isset($arr[0]) AND isset($arr[0]['id']) AND $arr[0]['id']>0){
                return  $arr[0]['id'];
            }else{
                if (isset($_SESSION['base.language_id']) AND $_SESSION['base.language_id']>0) return $_SESSION['base.language_id'];
                    else return 0;
            }
    }

    public function is_allow($action="",$plugin="music") {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();

        if ($is_admin){
            return true;
        }else if ($action AND $plugin AND OW::getUser()->isAuthorized($plugin, $action) ){
            return true;
        }else{
            return false;
        }
    }

public function getClientIP() {

    if (isset($_SERVER)) {

        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            return $_SERVER["HTTP_X_FORWARDED_FOR"];

        if (isset($_SERVER["HTTP_CLIENT_IP"]))
            return $_SERVER["HTTP_CLIENT_IP"];

        return $_SERVER["REMOTE_ADDR"];
    }

    if (getenv('HTTP_X_FORWARDED_FOR'))
        return getenv('HTTP_X_FORWARDED_FOR');

    if (getenv('HTTP_CLIENT_IP'))
        return getenv('HTTP_CLIENT_IP');

    return getenv('REMOTE_ADDR');
}

    private function make_xml_friendly( $url,$replay=" " )
    {

        $url=htmlentities($url, ENT_QUOTES, 'UTF-8');

        $find = array("\r\n","\r","\n");
        $url = str_ireplace ($find, '', $url);

        $url= str_replace(array('&','>','<','"'), array('&amp;','&gt;','&lt;','&quot;'), $url);

      $find = array(' ',
      '\r\n',
      '\n',
      '/',
      '“',
      '~',
      '\\',
      '+',
      '<',
      '>');
       $url = str_ireplace ($find, $replay, $url);


        return $url;
    }
    private function make_url_friendly( $url,$replay="_" )
    {
        $url = trim($url);
        $url = strtolower($url);
        $find = array('<b>','</b>');
        $url = str_replace ($find, '', $url);

        $find = array("\r\n","\r","\n");
        $url = str_replace ($find, '', $url);

        $url = preg_replace('/<(\/{0,1})img(.*?)(\/{0,1})\>/', 'image', $url);
        $find = array(' ',
      '&quot;',
      'quot',
      '&',
      'amp;',
      '\r\n',
      '\n',
      '/',
      '\\',
      '“',
      '~',
      '+',
      '<',
      '>');
       $url = str_replace ($find, $replay, $url);

        $find = array('/[^a-z0-9\-<>]/',
      '/[\-]+/',
      '/<[^>]*>/');
       $repl = array('',
      '-',
      '');
       $url = preg_replace ($find, $repl, $url);
       $url = str_replace ('--', '-', $url);

        return $url;
    }
//----'-------


    public function make_tabs($selected=1,$content="",$id=0)
    {
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
$content_t ="";
            $content_t .="<div class=\"ow_content\">";




$content_t .="<div class=\"ow_content_menu_wrap\">";
$content_t .="<ul class=\"ow_content_menu clearfix\">";




            if ($selected==1 OR $selected=="index" OR !$selected) $sel=" active ";
                else $sel="";
            $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."music\"><span class=\"ow_ic_files\">".OW::getLanguage()->text('music', 'music')."</span></a></li>";//moje zamówienia

            if ($id_user){
                if ($selected=="mymusic") $sel=" active ";
                    else $sel="";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."music/mymusic\"><span class=\"ow_ic_doc\">".OW::getLanguage()->text('music', 'my_music')."</span></a></li>";//moje zamówienia

                if ($selected=="add") $sel=" active ";
                    else $sel="";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."music/add/new/".substr(session_id(),3,5)."\"><span class=\"ow_ic_add\">".OW::getLanguage()->text('music', 'add_new_music')."</span></a></li>";//moje zamówienia
            }

            if ($selected=="edit") {
                $sel=" active ";
                $content_t .="<li class=\"_store_my_items ".$sel."\"><a href=\"".$curent_url."music/edit/".$id."/".substr(session_id(),3,5)."\"><span class=\"ow_ic_edit\">".OW::getLanguage()->text('music', 'editmusic')."</span></a></li>";//moje zamówienia
            }


$content_t .="</ul>";
$content_t .="</div>";
        $content_t .=$content;
        $content_t .="</div>";
        return $content_t;
    }



    public function makePagination($page = 1, $totalitems, $limit = 15, $adjacents = 1, $targetpage = "/", $pagestring = "?page=",$position="right",$contentadd="")
    {               
    //defaults
    if(!$adjacents) $adjacents = 1;
    if(!$limit) $limit = 15;
    if(!$page) $page = 1;
    if(!$targetpage) $targetpage = "/";
    $margin="";
    $padding="";
    $pagestring1="";
    //other vars
    $prev = $page - 1;                                                                  //previous page is page - 1
    $next = $page + 1;                                                                  //next page is page + 1
    $lastpage = ceil($totalitems / $limit);                             //lastpage is = total items / items per page, rounded up.
    $lpm1 = $lastpage - 1;                                                              //last page minus 1
    $space=" ";
//$lastpage++;    
//return "--".$lastpage;    
    if ($position=="center") $position="ow_center";
    else if ($position=="left") $position="ow_left";
    else $position="ow_right";
    $pagination = "";
    if($lastpage > 1)
    {   
        $pagination .= "<div class=\"".$position." ow_paging clearfix ow_smallmargin\"";
        if($margin || $padding)
        {
            $pagination .= " style=\"";
            if($margin)
                $pagination .= "margin: $margin;";
            if($padding)
                $pagination .= "padding: $padding;";
            $pagination .= "\"";
        }
        $pagination .= ">";

        //previous button
        if ($page > 1) 
            $pagination .= "<a href=\"$targetpage$pagestring$prev\">«</a>".$space;
        else
            $pagination .= "<a class=\"disabled\" href=\"".$targetpage.$pagestring1."\">«</a>".$space;
//            $pagination .= "<span class=\"disabled\">«</span>";    
        $pagination .= "&nbsp;&nbsp;&nbsp;";
        
        //pages 
        if ($lastpage < 7 + ($adjacents * 2))   //not enough pages to bother breaking it up
        {       
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
//                    $pagination .= "<span class=\"active\">$counter</span>";
                if ($counter == $page)
                    $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                     
                else
                    $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                     
            }
        }
        elseif($lastpage >= 7 + ($adjacents * 2))       //enough pages to hide some
        {
            //close to beginning; only hide later pages
            if($page < 1 + ($adjacents * 3))            
            {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
//                        $pagination .= "<span class=\"active\">$counter</span>";
                    if ($counter == $page)
                        $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                    else
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                }
                $pagination .= "<span class=\"elipses\">...</span>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>".$space;               
            }
            //in middle; hide some front and some back
            elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
            {
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>".$space;
                $pagination .= "<span class=\"elipses\">...</span>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                {
//                        $pagination .= "<span class=\"active\">$counter</span>";
                    if ($counter == $page)
                        $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                  
                    else
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                }
                $pagination .= "...";
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>".$space;               
            }
            //close to end; only hide early pages
            else
            {
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>".$space;
                $pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>".$space;
                $pagination .= "<span class=\"elipses\">...</span>".$space;
                for ($counter = $lastpage - (1 + ($adjacents * 3)); $counter <= $lastpage; $counter++)
                {
//                      $pagination .= "<span class=\"active\">$counter</span>";
                    if ($counter == $page)
                        $pagination .= "<a class=\"active\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                    else
                        $pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>".$space;                                 
                }
            }
        }
        
        //next button
        $pagination .= "&nbsp;&nbsp;&nbsp;";
        if ($page < $counter - 1) 
            $pagination .= "<a href=\"" . $targetpage . $pagestring . $next . "\">»</a>".$space;
        else
            $pagination .= "<a class=\"disabled\" href=\"" . $targetpage . $pagestring . $lastpage . "\">»</a>".$space;
//            $pagination .= "<a class=\"disabled\" href=\"" . $targetpage . $pagestring . $next . "\">»</a>".$space;
//            $pagination .= "<span class=\"disabled\">»</span>";
        $pagination .= "</div>\n";
    }

        if ($pagination){
            return "<div class=\"clearfix ow_smallmargin\">".$pagination.$contentadd."</div>";
//            return "<div class=\"clearfix ow_smallmargin\">".$pagination."</div>";
        }else if ($contentadd){
            return "<div class=\"clearfix ow_smallmargin\">".$contentadd."</div>";
//            return "<div class=\"clearfix ow_smallmargin\"></div>";
        }else{
            return "";
        }

    }


}
