<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

//OW::getNavigation()->deleteMenuItem('music', 'bottom_menu_item');
//OW::getNavigation()->deleteMenuItem('music', 'main_menu_item');

OW::getNavigation()->deleteMenuItem('music', 'main_menu_item');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexlClock');

/*
//OW::getNavigation()->deleteMenuItem('music', 'bottom_menu_item');

//BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexsClock');
//BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexlClock');
//BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexrClock');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndextClock');
//BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexbClock');


BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexNews');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexNewsx');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexForum');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexForumx');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexShop');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexShopx');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexGames');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexGamesx');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexPhoto');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexPhotox');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexVideo');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexVideox');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexMap');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexMapx');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexFanpage');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexFanpagex');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexBlog');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexBlogx');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexGroup');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexGroupx');

BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexEvent');
BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexEventx');

//BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexlNews');
//BOL_ComponentAdminService::getInstance()->deleteWidget('MUSIC_CMP_IndexrNews');
*/
