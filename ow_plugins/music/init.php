<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


OW::getRouter()->addRoute(new OW_Route('music.index', 'music', "MUSIC_CTRL_Music", 'index'));
OW::getRouter()->addRoute(new OW_Route('music.indexop', 'music/:op/:id/:ss', "MUSIC_CTRL_Music", 'index'));
OW::getRouter()->addRoute(new OW_Route('music.indexon', 'music/:op/', "MUSIC_CTRL_Music", 'index'));

//OW::getRouter()->addRoute(new OW_Route('music.admin', 'admin/plugins/music', "MUSIC_CTRL_Admin", 'dept'));
OW::getRouter()->addRoute(new OW_Route('music.admin', 'admin/plugins/music', "MUSIC_CTRL_Admin", 'dept'));


/*
$config = OW::getConfig();
if ( !$config->configExists('music', 'toclock') ){
    $config->addConfig('music', 'toclock', '1', '');
}
if ( !$config->configExists('music', 'tonews') ){
    $config->addConfig('music', 'tonews', '1', '');
}
*/

if (!isset($_SERVER["SERVER_NAME"])) $_SERVER["SERVER_NAME"]="";
if (!isset($_SERVER["REQUEST_URI"])) $_SERVER["REQUEST_URI"]="";



//OW::getRouter()->addRoute(new OW_Route('pay24.dopay', 'pay24/:option/:iditem', "PAY24_CTRL_Dopay", 'index'));
//OW::getRouter()->addRoute(new OW_Route('pay24.back', 'pay24/back', "PAY24_CTRL_Dopay", 'index'));;
//OW::getRouter()->addRoute(new OW_Route('music.payresult', 'music/:com', "MUSIC_CTRL_Music", 'index'));;



/*
function music_setrestriction_tick(){
    if (OW::getConfig()->getValue('music', 'music_enable')=="1"){
        MUSIC_BOL_Service::getInstance()->minix();
    }
}

if (!OW::getRequest()->isPost() AND !OW::getRequest()->isAjax() AND 
    strpos($_SERVER["REQUEST_URI"],"base/media-panel")===false AND 
    strpos($_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"],"ow_cron")===false AND 
    MUSIC_BOL_Service::getInstance()->check_url_allow()==true
    ){

    OW::getEventManager()->bind('core.finalize', 'music_setrestriction_tick');
//echo "--".OW::getRequest()->isPost();exit;
//    OW::getEventManager()->bind('core.app_init', 'setrestriction_tick');
}



function aaa(){
//    OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/shoppro/m/jquery.collapse.js');
//    OW::getDocument()->addScript('http://www.tinymce.com/js/tinymce_3_x/jscripts/tiny_mce/tiny_mce_gzip.php?js=1&plugins=fullpage,inlinepopups&themes=advanced&languages=en&diskcache=true&src=false');
//    OW::getDocument()->addScript('http://tinymce.cachefly.net/4.0/tinymce.min.js');

$script1="
tinyMCE.init({
        theme : \"advanced\",
        mode : \"textareas\",
        plugins : \"fullpage\",
        theme_advanced_buttons3_add : \"fullpage\"
});
";
$script1="
tinymce.init({selector:'textarea',theme_advanced_buttons3_add : 'fullpage', plugins : 'fullpage',theme : 'advanced'});


tinymce.PluginManager.load('moxiemanager', 'http://www.tinymce.com/js/moxiemanager/plugin.min.js');

tinyMCE.init({
    // General options
    mode : \"textareas\",
    theme : \"advanced\",
    plugins : \"autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,moxiemanager\",

    // Theme options
    theme_advanced_buttons1 : \"save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect\",
    theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor\",
    theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen\",
    theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage\",
    theme_advanced_toolbar_location : \"top\",
    theme_advanced_toolbar_align : \"left\",
    theme_advanced_statusbar_location : \"bottom\",
    theme_advanced_resizing : true,

    skin : \"o2k7\",
    skin_variant : \"silver\",

    // Example content CSS (should be your site CSS)
    content_css : \"http://www.tinymce.com/tryit/3_x/css/example.css\",

    // Drop lists for link/image/media/template dialogs
    template_external_list_url : \"http://www.tinymce.com/tryit/3_x/js/template_list.js\",
    external_link_list_url : \"http://www.tinymce.com/tryit/3_x/js/link_list.js\",
    external_image_list_url : \"http://www.tinymce.com/tryit/3_x/js/image_list.js\",
    media_external_list_url : \"http://www.tinymce.com/tryit/3_x/js/media_list.js\",

    // Replace values for the template plugin
    template_replace_values : {
        username : \"Some User\",
        staffid : \"991234\"
    },

    autosave_ask_before_unload : false // Disable for example purposes
});



";
$script="
<script>
$(document).ready(function(){

$('#mce_13').bind('hide', function(){
    console.log('asdasda')
    alert('dddd');
})

//alert('ss');
$('.mce-tinymce').show();
$('#post_body').show();


$('#mce_13').addClass('post_body');
//$('#post_body').remove();
$('#post_body').addClass('post_body2');
$('.post_body2').attr('id','post_body2');
$('#post_body2').append('<iframe id=\"post_body\" style=\"display:none;visible:none;\"></iframe>');




$('#mce_13').show();

//    add-topic-form
$('form[name=\"add-topic-form\"]' ).submit(function( event ) {
      alert( 'Handler for .submit() called.' );
      event.preventDefault();
    return false;
});
});
</script>
";

$script33="
<sc"."ript type=\"text/javascript\" src=\"http://tinymce.cachefly.net/4.0/jquery.tinymce.min.js\"><"."/script>
<script type=\"text/javascript\" src=\"http://tinymce.cachefly.net/4.0/tinymce.min.js\"></script>

<script>
document."."write(\"<script type=\\\"text/javascript\\\" src=\\\"http://tinymce.cachefly.net/4.0/tinymce.min.js\\\"></script>\");
</script>
";
$script3="
<sc"."ript type=\"text/javascript\" src=\"http://tinymce.cachefly.net/4.0/jquery.tinymce.min.js\"><"."/script>
<script type=\"text/javascript\" src=\"http://tinymce.cachefly.net/4.0/tinymce.min.js\"></script>

<script>
tinymce.init({selector:'textarea',theme_advanced_buttons3_add : 'fullpage', plugins : 'fullpage'});
</script>

";

//    OW::getDocument()->addOnloadScript($script1);
//    OW::getDocument()->addOnloadScript($script);

    OW::getDocument()->appendBody($script3);
    OW::getDocument()->appendBody($script);



}

//OW::getEventManager()->bind('core.finalize', 'aaa');new tinymc
*/


/*
function testaa()
{
    new MUSIC_CMP_Toolbar();
//echo "sss";exit;
}

//OW::getEventManager()->bind('core.finalize', 'testaa');
//OW::getEventManager()->bind('OW_EventManager::APPLICATION_INIT', 'testaa');
//OW::getEventManager()->bind(OW_EventManager::ON_FINALIZE, 'testaa');
//OW::getEventManager()->bind('core.app_init', 'testaa');


function testaa2( )
{
//OW_Navigation::getInstance()->deactivateMenuItems(OW_Navigation::ADMIN_SETTINGS);
OW_Navigation::getInstance()->deactivateMenuItems(BOL_NavigationService::MENU_TYPE_MAIN);
//OW_Navigation::getInstance()->deactivateMenuItems(OW_Navigation::MENU_TYPE_MAIN);
//    OW::getNavigation()->deactivateMenuItems(OW_Navigation::MENU_TYPE_MAIN);
}

//OW::getEventManager()->bind('core.finalize', 'testaa2');
OW::getEventManager()->bind('core.app_init', 'testaa2');
*/


/*
$plugin = OW::getPluginManager()->getPlugin('music');
//require_once $plugin->getRootDir() . 'classes' . DS . 'controllers' . DS . 'component_panel.php';
//require_once $plugin->getRootDir() . 'classes' . DS . 'components' . DS . 'drag_and_drop_entity_panel.php';

require_once $plugin->getRootDir() . 'classes' . DS . 'components' . DS . 'menu.php';
*/


/*
function contactus_handler_after_install( BASE_CLASS_EventCollector $event )
{
    if ( count(CONTACTUS_BOL_Service::getInstance()->getDepartmentList()) < 1 )
    {
        $url = OW::getRouter()->urlForRoute('contactus.admin');
        $event->add(OW::getLanguage()->text('contactus', 'after_install_notification', array('url' => $url)));
    }
}

OW::getEventManager()->bind('admin.add_admin_notification', 'contactus_handler_after_install');
*/

/*
function music_ads_enabled( BASE_EventCollector $event )
{
    $event->add('music');
}
OW::getEventManager()->bind('ads.enabled_plugins', 'music_ads_enabled');
OW::getRequestHandler()->addCatchAllRequestsExclude('base.suspended_user', 'MUSIC_CTRL_Music');
*/

//--------------------------------------------------------



function music_add_auth_labels( BASE_CLASS_EventCollector $event )
{
    $language = OW::getLanguage();
    $event->add(
        array(
            'music' => array(
                'label' => $language->text('music', 'auth_music'),
                'actions' => array(
                    'addmusic' => $language->text('music', 'auth_action_label_addmusic'),
                    'view' => $language->text('music', 'auth_action_label_view')
                )
            )
        )
    );
}
OW::getEventManager()->bind('admin.add_auth_labels', 'music_add_auth_labels');


/*

function music_questions_email_save( OW_Event $e )
{
    $params = $e->getParams();
    $data = $e->getData();

    foreach ( $data as $key => $value )
    {
        if ( $key == 'password' )
        {
//$params['userId']

        }
    }

    $e->setData($data);
}
OW::getEventManager()->bind('base.questions_save_data', 'music_questions_email_save');

*/

