<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://test.a6.pl
 * Full license available at: http://test.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


class MUSIC_CMP_IndexlClock extends BASE_CLASS_Widget
{

    public $ciduser = 0;

    public function __construct( BASE_CLASS_WidgetParameter $params )
    {
        parent::__construct();
        $this->ciduser = $params->additionalParamList['entityId'];

/* 
        $params = $paramObject->customParamList;
 
        if ( !empty($params['content']) )
        {
            $this->content = $paramObject->customizeMode && !empty($_GET['disable-js']) ? UTIL_HtmlTag::stripJs($params['content']) : $params['content'];
        }
 
        if ( isset($params['nl_to_br']) )
        {
            $this->nl2br = (bool) $params['nl_to_br'];
        }

            if (!isset($arr[0]) OR !count($arr[0])){
                $this->setVisible(false);
                return;
            }

*/
    }

    public static function getSettingList()
    {

        $options = array();
        $settingList=array();
/*
        for ( $i = 3; $i <= 10; $i++ )
        {
            $options[$i] = $i;
        }

        $settingList['count'] = array(
            'presentation' => self::PRESENTATION_SELECT,
            'label' => OW::getLanguage()->text('shoppro', 'cmp_widget_post_count'),
            'optionList' => $options,
            'value' => 3,
        );
        $settingList['previewLength'] = array(
            'presentation' => self::PRESENTATION_TEXT,
            'label' => OW::getLanguage()->text('shoppro', 'blog_widget_preview_length_lbl'),
            'value' => 50,
        );
*/
        return $settingList;
    }

    public static function getStandardSettingValueList()
    {
        $list = array(
            self::SETTING_TITLE => OW::getLanguage()->text('music', 'txw_clock'),
            self::SETTING_SHOW_TITLE => true,
                self::SETTING_WRAP_IN_BOX => true,
            self::SETTING_ICON => 'ow_ic_write'
        );

        return $list;
    }

    public static function getAccess()
    {
        return self::ACCESS_ALL;
    }
	
    public function onBeforeRender() // The standard method of the component that is called before rendering
    {

        
            $content="";
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
//                    $curent_url .= $_SERVER["SERVER_NAME"]."/";
        $curent_url=OW_URL_HOME; 
        $pluginStaticU=OW::getPluginManager()->getPlugin('music')->getStaticUrl();
//        $pluginStaticD=OW::getPluginManager()->getPlugin('startpage')->getStaticDir();

//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/music/mediaelement-and-player.js');
        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/music/mediaelement-and-player.min.js');
//        OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/music/mediaelementplayer.min.css');
        OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/music/mediaelementplayer.css');


//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/music/ext/jquery.movingboxes.min.js');
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/music/ext/slidershop.js?'.rand(100,500));
//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/music/ext/slidershop.js?a=1');


//        OW::getDocument()->addScript(OW_URL_HOME.'ow_static/plugins/music/w_clock/jquery.tzineClock.js?a=1');
//        OW::getDocument()->addStyleSheet(OW_URL_HOME.'ow_static/plugins/music/w_clock/jquery.tzineClock.css');
/*
        OW::getDocument()->addScript($pluginStaticU.'nicemenu.js');
        OW::getDocument()->addScript($pluginStaticU.'js'.DS.'bootstrap.min.js');

        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap-responsive.css');
        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap.css');
//        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap-responsive.min.css');
//        OW::getDocument()->addStyleSheet($pluginStaticU.'css'.DS.'bootstrap.min.css');
*/

                    $_SESSION['profile_music_autoplay']=0;
                    $content .=MUSIC_BOL_Service::getInstance()->get_music_list($this->ciduser);
//                    $content .=MUSIC_BOL_Service::getInstance()->get_music_list();



$script="
$(document).ready(function() {
//    $('audio,video').mediaelementplayer({
//    $('.audio_music').mediaelementplayer({
//    $('audio').mediaelementplayer({

    $('div.profile-MUSIC_CMP_IndexlClock audio').mediaelementplayer({

        loop: true,
        audioWidth: '100%',
        audioHeight: 30,
        enableAutosize: true,
        alwaysShowHours: false,

    iPadUseNativeControls: false,
    iPhoneUseNativeControls: false,
    AndroidUseNativeControls: false,

        features: ['playpause','progress','current','duration','tracks','volume','fullscreen'],
        startVolume: 0.5,
        pauseOtherPlayers: true,
        enableKeyboard: true


/*
        autoplay:true,
        success: function (me) {
                me.play();
//            whenPlayerReadyToPlay(me, function () {
//                me.play();
//            })
        }
*/


    });
//    $('#player_1').play();

";

//$('#stopall').click(function() {
//    $('#player_1').each(function() {
/*
alert($('audio').attr('id'));
//    $('#player_').each(function() {
    $('audio').each(function() {
          $(this)[0].player.play();
    });
*/
//          $('audio').attr('id').player.play();

if ($_SESSION['profile_music_autoplay']==1){
$script .="
//          $('#'+$('audio').attr('id'))[0].player.play();
          $('#'+$('div.profile-MUSIC_CMP_IndexlClock audio').attr('id'))[0].player.play();
";
}

$script .="

//});

});
";
//$content.=$script;
OW::getDocument()->addOnloadScript($script);



/*
//                $content .="<div class=\"clearfix\">";
                    $content .="<div class=\"ow_left\">";
                        $content .="xxxx";
                    $content .="</div>";
                    $content .="<div class=\"clearfix\">";
                        $content .="<audio id=\"player_1\" class=\"audio_music\" src=\"".OW_URL_HOME."ow_static/plugins/music/echo-hereweare.mp4\" type=\"audio/mp3\" controls=\"controls\" />";
                    $content .="</div>";
//                $content .="</div>";
*/
                if ($is_admin OR ($id_user AND $id_user==$this->ciduser)){
                    $content .="<div class=\"clearfix\">";
                    $content .="<div class=\"ow_center ow_submit ow_smallmargin\" style=\"margin-top:20px;\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <a href=\"".OW_URL_HOME."music/add/new/".substr(session_id(),3,5)."\">
                            <input type=\"button\" value=\"".OW::getLanguage()->text('music', 'add_music')."\" name=\"dosave\" class=\"ow_ic_edit ow_positive\">
                            </a>
                        </span>
                    </span>
                </div>
            </div>";
                    $content .="</div>";
                }

		$this->assign('content', $content);
	}
	
}

