<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


OW::getLanguage()->importPluginLangs(OW::getPluginManager()->getPlugin('music')->getRootDir().'langs.zip', 'music');
OW::getPluginManager()->addPluginSettingsRouteName('music', 'music.admin');

//BOL_LanguageService::getInstance()->addPrefix('contactus', 'Contact Us');

$config = OW::getConfig();
if ( !$config->configExists('music', 'display_title') ){
    $config->addConfig('music', 'display_title', '1', '');
}
if ( !$config->configExists('music', 'perpage') ){
    $config->addConfig('music', 'perpage', '30', '');
}
if ( !$config->configExists('music', 'file_size_quota') ){
    $config->addConfig('music', 'file_size_quota', '2097152', '');
}

$authorization = OW::getAuthorization();
$groupName = 'music';
$authorization->addGroup($groupName);
$authorization->addAction($groupName, 'addmusic');
//$authorization->addAction($groupName, 'addclassifieds',true);//fo guests too
//$authorization->addAction($groupName, 'addclassifieds');
$authorization->addAction($groupName, 'view', true);


/*
$authorization = OW::getAuthorization();
$groupName = 'music';
$authorization->addGroup($groupName);
$authorization->addAction($groupName, 'add_comment');
$authorization->addAction($groupName, 'add');
$authorization->addAction($groupName, 'view', true);
*/

/*
if ( !$config->configExists('music', 'music_protect_key') ){
    $key=substr(md5(date('d-m-Y H:i:s')),0,10);
    $config->addConfig('music', 'music_protect_key', $key, 'Protect Key');
}

if ( !$config->configExists('music', 'music_generateforusers') ){
    $config->addConfig('music', 'music_generateforusers', '1', 'Site Map for members too');
}

if ( !$config->configExists('music', 'music_max_items_eachplugin') ){
    $config->addConfig('music', 'music_max_items_eachplugin', '10', 'Max items for each plugins');
}
if ( !$config->configExists('music', 'music_max_items_sitemap') ){
    $config->addConfig('music', 'music_max_items_sitemap', '9999', 'Max items sitemap');
}

*/

$sql="DROP TABLE IF EXISTS `" . OW_DB_PREFIX . "music`; ";
OW::getDbo()->query($sql);

$sql="CREATE TABLE IF NOT EXISTS `" . OW_DB_PREFIX . "music` (
  `id` bigint(22) NOT NULL AUTO_INCREMENT,
  `active` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `id_owner` int(11) NOT NULL,
  `auto_play` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '0',
  `display_in_profile` enum('0','1') COLLATE utf8_bin NOT NULL DEFAULT '1',
  `share` enum('public','private') COLLATE utf8_bin NOT NULL DEFAULT 'public',
  `add_date` int(11) DEFAULT NULL,
  `u_title` varchar(255) COLLATE utf8_bin NOT NULL,
  `u_author` varchar(128) COLLATE utf8_bin NOT NULL,
  `u_order` int(10) NOT NULL DEFAULT '0',
  `u_url` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `u_file` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `u_desc` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `id_owner` (`id_owner`),
  KEY `auto_play` (`auto_play`),
  KEY `u_title` (`u_title`),
  KEY `u_author` (`u_author`),
  KEY `active` (`active`),
  KEY `u_order` (`u_order`),
  KEY `add_date` (`add_date`),
  KEY `display_in_profile` (`display_in_profile`),
  FULLTEXT KEY `u_desc` (`u_desc`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;";
OW::getDbo()->query($sql);

