<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/


class MUSIC_CTRL_Admin extends ADMIN_CTRL_Abstract
{

    public function dept()
    {
        $content="";
        $this->setPageTitle(OW::getLanguage()->text('music', 'admin_dept_title'));
        $this->setPageHeading(OW::getLanguage()->text('music', 'admin_dept_heading'));    
        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();//iss admin
        $curent_url=OW_URL_HOME;
        $config = OW::getConfig();
        $content="";
        if (!isset($_POST['save'])) $_POST['save']="";
        if ($is_admin AND $id_user>0 AND $_POST['save']=="besave"){
/*
            if ($_POST['c_protectkey']){
                $music_protect_key=$_POST['c_protectkey'];
            }else{
                $music_protect_key=substr(md5(date('d-m-Y H:i:s')),0,10);
            }
            if ($_POST['c_generateforusers']){
                $generateforusers=1;
            }else{
                $generateforusers=0;
            }
            $music_max_items_eachplugin=$_POST['c_maxitemseachplugin'];
            if (!$music_max_items_eachplugin) $music_max_items_eachplugin=10;
            if ($music_max_items_eachplugin>50) $music_max_items_eachplugin=50;
            $music_max_items_sitemap=$_POST['c_maxitemsforsitemap'];
            if (!$music_max_items_sitemap) $music_max_items_sitemap=1000;
            if ($music_max_items_sitemap>9999) $music_max_items_sitemap=9999;

            $config->saveConfig('music', 'music_protect_key', $music_protect_key);
            $config->saveConfig('music', 'music_generateforusers', $generateforusers);
            $config->saveConfig('music', 'music_max_items_eachplugin', $music_max_items_eachplugin);
            $config->saveConfig('music', 'music_max_items_sitemap', $music_max_items_sitemap);
*/
            $config->saveConfig('music', 'display_title', $_POST['c_display_title']);
            $config->saveConfig('music', 'perpage', $_POST['c_perpage']);
            if (!$_POST['c_file_size_quota']) $_POST['c_file_size_quota']=0;
            $config->saveConfig('music', 'file_size_quota', $_POST['c_file_size_quota']);

            OW::getApplication()->redirect($curent_url."admin/plugins/music");
        }


        $content .="<form action=\"".$curent_url."admin/plugins/music\" method=\"post\">";
        $content .="<input type=\"hidden\" name=\"save\" value=\"besave\">";
//        $content .="<table style=\"width:auto;\">";
        $content .="<table style=\"width:auto;margin:auto;\" class=\"ow_table_1 ow_form ow_stdmargin\">";



        $content .="<tr class=\"ow_alt1\">";
        $content .="<td  class=\"ow_label\" style=\"min-width:300px;\">";
        $content .="<b>".OW::getLanguage()->text('music', 'display_title').":</b>";
        $content .="</td>";
        $content .="<td  class=\"ow_value\">";
        $content .="<select name=\"c_display_title\">";
        if (OW::getConfig()->getValue('music', 'display_title')=="1" OR OW::getConfig()->getValue('music', 'display_title')=="") $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('music', 'yes')."</option>";
        if (OW::getConfig()->getValue('music', 'display_title')=="0" ) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('music', 'no')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td class=\"ow_label\" >";
        $content .="<b>".OW::getLanguage()->text('music', 'file_size_quota').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $itels=OW::getConfig()->getValue('music', 'file_size_quota');
        if ($itels=="") $itels=0;
        $content .="<input type=\"text\" name=\"c_file_size_quota\" value=\"".$itels."\" style=\"display:inline-block;width:100px;\">&nbsp;Default in bytes: 2097152";
//        $content .="&nbsp;".OW::getLanguage()->text('music', 'admin_maxitemseachplugin_info')."";
        $content .="</td>";
        $content .="</tr>";


        $content .="<tr>";
        $content .="<td class=\"ow_label\" >";
        $content .="<b>".OW::getLanguage()->text('music', 'perpage').":</b>";
        $content .="</td>";
        $content .="<td class=\"ow_value\">";
        $itels=OW::getConfig()->getValue('music', 'perpage');
        if (!$itels) $itels=30;
        $content .="<input type=\"text\" name=\"c_perpage\" value=\"".$itels."\" style=\"display:inline-block;width:100px;\">&nbsp;Default: 30";
//        $content .="&nbsp;".OW::getLanguage()->text('music', 'admin_maxitemseachplugin_info')."";
        $content .="</td>";
        $content .="</tr>";

/*
        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('music', 'na_clock_').":</b>";
        $content .="</td>";
        $content .="<td nowrap=\"nowrap\">";
        $value=OW::getConfig()->getValue('music', 'toclock');
        if ($value=="0") $sel="";
            else $sel=" CHECKED ";
        $content .="<input ".$sel." type=\"checkbox\" name=\"c_toclock\" value=\"1\">";
        $content .="</td>";
        $content .="</tr>";
*/

/*
        $content .="<tr>";
        $content .="<td colspan=\"2\" wrap=\"wrap\" style=\"background-color:#eee;\">";
        $content .=OW::getLanguage()->text('music', 'admin_protectkey_informationofuse');
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('music', 'admin_userprofiledata').":</b>";
        $content .="</td>";
        $content .="<td >";
        $content .="<select name=\"c_generateforusers\">";
        if (OW::getConfig()->getValue('music', 'music_generateforusers') OR !OW::getConfig()->getValue('music', 'music_generateforusers')) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"0\">".OW::getLanguage()->text('music', 'admin_dontgeneratememberstoo')."</option>";
        if (OW::getConfig()->getValue('music', 'music_generateforusers')==1) $sel=" selected ";
            else  $sel=" ";
        $content .="<option ".$sel." value=\"1\">".OW::getLanguage()->text('music', 'admin_generatememberstoo')."</option>";
        $content .="</select>";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('music', 'admin_maxitemseachplugin').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itels=OW::getConfig()->getValue('music', 'music_max_items_eachplugin');
        if (!$itels) $itels=10;
        $content .="<input type=\"text\" name=\"c_maxitemseachplugin\" value=\"".$itels."\" style=\"display:inline-block;width:100px;\">";
        $content .="&nbsp;".OW::getLanguage()->text('music', 'admin_maxitemseachplugin_info')."";
        $content .="</td>";
        $content .="</tr>";

        $content .="<tr>";
        $content .="<td >";
        $content .="<b>".OW::getLanguage()->text('music', 'admin_maxitemsforsitemap').":</b>";
        $content .="</td>";
        $content .="<td >";
        $itelss=OW::getConfig()->getValue('music', 'music_max_items_sitemap');
        if (!$itelss) $itelss=1000;
        $content .="<input type=\"text\" name=\"c_maxitemsforsitemap\" value=\"".$itelss."\" style=\"display:inline-block;width:100px;\">";
        $content .="&nbsp;".OW::getLanguage()->text('music', 'admin_maxitemsforsitemap_info')."";
        $content .="</td>";
        $content .="</tr>";
*/
        $content .="<tr>";
        $content .="<td colspan=\"2\">";
//        $content .="<input type=\"submit\" name=\"dosave\" value=\"".OW::getLanguage()->text('music', 'admin_save')."\">";
        $content .="<div class=\"clearfix ow_submit ow_smallmargin\">
                <div class=\"ow_center\">
                    <span class=\"ow_button\">
                        <span class=\" ow_positive\">
                            <input type=\"submit\" value=\"".OW::getLanguage()->text('music', 'admin_save')."\" name=\"dosave\" class=\"ow_ic_save ow_positive\">
                        </span>
                    </span>
                </div>
            </div>";
        $content .="</td>";
        $content .="</tr>";
        $content .="</table>";
        $content .="</form>";
        $this->assign('content', $content);
    }

    public function delete( $params )
    {
        $this->redirect(OW::getRouter()->urlForRoute('music.admin'));
    }
}
