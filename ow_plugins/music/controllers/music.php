<?php

/***
 * This software is intended for use with Oxwall Free Community Software
 * http://www.oxwall.org/ and is a proprietary licensed product.
 * For more information see License.txt in the plugin folder.

 * =============================================================================
 * Copyright (c) 2012 by Aron. All rights reserved.
 * =============================================================================


 * Redistribution and use in source and binary forms, with or without modification, are not permitted provided.
 * Pass on to others in any form are not permitted provided.
 * Sale are not permitted provided.
 * Sale this product are not permitted provided.
 * Gift this product are not permitted provided.
 * Licence for this plugin should be bought from the developer by paying money to PayPal account: aro@grafnet.pl
 * Legal purchase licence for this plugin is possible only on the web page URL: http://www.oxwall.org/store
 * Modyfing of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Modifying source code, all information like:copyright must remain.
 * Official website only: http://oxwall.a6.pl
 * Full license available at: http://oxwall.a6.pl


 * BECAUSE YOU BUY LICENSE TO GET OPEN SOURCE PLUGIN CODE AND CAN DOWNLOAD CODE FROM OXWALL WEBPAGE
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/



class MUSIC_CTRL_Music extends OW_ActionController
{

    public function index($params)
    {

        $curent_url=OW_URL_HOME;
$output_music2="";
$content="";

        if (!isset($params['op'])) $params['op']="";
        if (!isset($params['id'])) $params['id']="";
        if (!isset($params['ss'])) $params['ss']="";

        $this->setPageTitle(OW::getLanguage()->text('music', 'main_menu_item')); //title menu
        $this->setPageHeading(OW::getLanguage()->text('music', 'main_menu_item')); //title page

      $curent_url=OW_URL_HOME;

        $id_user = OW::getUser()->getId();//citent login user (uwner)
        $is_admin = OW::getUser()->isAdmin();
        $curent_url=OW_URL_HOME;
//        $pluginStaticURL=OW::getPluginManager()->getPlugin('music')->getStaticUrl();
        $pluginUserFileURL=OW::getPluginManager()->getPlugin('music')->getUserFilesUrl();
        $pluginUserFileDIR=OW::getPluginManager()->getPlugin('music')->getUserFilesDir();

        
        if ($id_user AND ($params['op']=="edit" OR $params['op']=="del" OR $params['op']=="add") ){


            if ($params['op']=="edit" OR $params['op']=="add"){
//echo "sss";exit;
                if (!OW::getUser()->isAuthorized('music', 'view') AND !OW::getUser()->isAuthorized('music', 'addmusic') AND !$is_admin){
                    OW::getFeedback()->info(OW::getLanguage()->text('music', 'you_donthave_permission'));
                    OW::getApplication()->redirect($curent_url."index");
                    exit;
                }else{
                    $content .=MUSIC_BOL_Service::getInstance()->get_edit_form($params);
                    $content=MUSIC_BOL_Service::getInstance()->make_tabs($params['op'],$content,$params['id']);
                }
            }else if ($params['op']=="del"){



                if ($params['id']>0 AND $params['ss']==substr(session_id(),3,5) AND $id_user>0){
                    $add="";
                    if (!$is_admin){                                    
                        $add=" AND id_owner='".addslashes($id_user)."' ";
                    }


                    $file_del="";
                    $sql = "SELECT * FROM " . OW_DB_PREFIX. "music
                    WHERE id='".addslashes($params['id'])."' ".$add." LIMIT 1";
                    $arrl = OW::getDbo()->queryForList($sql);
                    if (isset($arrl[0]) AND isset($arrl[0]['id']) AND $arrl[0]['id']>0){
                        $file_del=stripslashes($arrl[0]['u_file']);
                        if ($file_del){
                            MUSIC_BOL_Service::getInstance()->file_delete($pluginUserFileDIR.$arrl[0]['id_owner'].DS.$arrl[0]['u_file']);
                        }
                    }


                    $sql="DELETE FROM " . OW_DB_PREFIX. "music WHERE id='".addslashes($params['id'])."' ".$add." LIMIT 1";
                    OW::getDbo()->query($sql);

                    OW::getFeedback()->info(OW::getLanguage()->text('music', 'deleted_succesfull'));
                    OW::getApplication()->redirect($curent_url."music/mymusic");
                    exit;
                }
                OW::getFeedback()->info(OW::getLanguage()->text('music', 'deleted_error'));
                OW::getApplication()->redirect($curent_url."music");
                exit;

            }else{//unknow OP pram
//                OW::getFeedback()->info(OW::getLanguage()->text('music', ''));
                OW::getApplication()->redirect($curent_url."music");
                exit;
            }
        }else if ($params['op']=="mymusic" AND $id_user>0){
                $content .=MUSIC_BOL_Service::getInstance()->get_list('mymusic');
                $content=MUSIC_BOL_Service::getInstance()->make_tabs('mymusic',$content);
        }else{
            if (!OW::getUser()->isAuthorized('music', 'view') AND !$is_admin){
                OW::getFeedback()->info(OW::getLanguage()->text('music', 'you_donthave_permission'));
                OW::getApplication()->redirect($curent_url."index");
                exit;
            }else{
                $content .=MUSIC_BOL_Service::getInstance()->get_list();
                $content=MUSIC_BOL_Service::getInstance()->make_tabs('index',$content);
            }
        }


	$this->assign('content', $content);
    }//end index

    public function make_item($arrx,$tagmain="item")
    {
        $ret ="";
        foreach ($arrx as $nam=>$val){
            if ($nam AND $val){
                $ret .="<".$nam.">".$val."</".$nam.">\n";
            }
        }
        if ($ret AND $tagmain){
            $ret="<".$tagmain.">\n".$ret."</".$tagmain.">\n";
        }
        return $ret;
    }

    public function sent()
    {

    }
/*
     public function html2txt($document){
        $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
        );
        $text = preg_replace($search, '', $document);
        $text=preg_replace("/(&?!amp;)/i", " ", $text);
        $text=preg_replace("/(&#\d+);/i", " ", $text); // For numeric entities
        $text=preg_replace("/(&\w+);/i", " ", $text); // For literal entities
        return $text;
    }
*/
    public function make_seo_url($name,$lengthtext=100)
    {
        $seo_title=stripslashes($name);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace(chr(160),"_",$seo_title);
        $seo_title=str_replace("~","",$seo_title);
        $seo_title=str_replace("(","",$seo_title);
        $seo_title=str_replace(")","",$seo_title);
        $seo_title=str_replace("]","",$seo_title);
        $seo_title=str_replace("[","",$seo_title);
        $seo_title=str_replace("}","",$seo_title);
        $seo_title=str_replace("{","",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("\\","",$seo_title);
        $seo_title=str_replace("+","",$seo_title);
        $seo_title=str_replace(":","",$seo_title);
        $seo_title=str_replace(";","",$seo_title);
        $seo_title=str_replace("\"","",$seo_title);
        $seo_title=str_replace("<","",$seo_title);
        $seo_title=str_replace(">","",$seo_title);
        $seo_title=str_replace("?","",$seo_title);
        $seo_title=str_replace(",",".",$seo_title);
        $seo_title=str_replace("!","",$seo_title);
        $seo_title=str_replace("`","",$seo_title);
        $seo_title=str_replace("'","",$seo_title);
        $seo_title=str_replace("@","",$seo_title);
        $seo_title=str_replace("#","",$seo_title);
        $seo_title=str_replace("$","",$seo_title);
        $seo_title=str_replace("%","",$seo_title);
        $seo_title=str_replace("^","",$seo_title);
        $seo_title=str_replace("&","",$seo_title);
        $seo_title=str_replace("*","",$seo_title);
        $seo_title=str_replace("|","",$seo_title);
        $seo_title=str_replace("=","",$seo_title);
        $seo_title=str_replace(" ","_",$seo_title);
        $seo_title=str_replace("/","",$seo_title);
        $seo_title=str_replace("?","_",$seo_title);
        $seo_title=str_replace("#","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("=","_",$seo_title);
        $seo_title=str_replace("&amp;","_",$seo_title);
        $seo_title=str_replace("__","_",$seo_title);
        $seo_title = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $seo_title);
        $seo_title =strtolower($seo_title);
        $seo_title=mb_substr($seo_title,0,100);
        return $seo_title;
    }


    private function make_xml_friendly( $url,$replay=" " )
    {
        $find = array(' ',
      '&quot;',
      'quot',
      '&',
      'amp;',
      '\r\n',
      '\n',
      '/',
      '\\',
      '+',
      '<',
      '>');
       $url = str_replace ($find, $replay, $url);
        return $url;
    }
    private function make_url_friendly( $url,$replay="-" )
    {
        $url = trim($url);
        $url = strtolower($url);
        $find = array('<b>','</b>');
        $url = str_replace ($find, '', $url);
        $url = preg_replace('/<(\/{0,1})img(.*?)(\/{0,1})\>/', 'image', $url);
        $find = array(' ',
      '&quot;',
      'quot',
      '&',
      'amp;',
      '\r\n',
      '\n',
      '/',
      '\\',
      '+',
      '<',
      '>');
       $url = str_replace ($find, $replay, $url);

        $find = array('/[^a-z0-9\-<>]/',
      '/[\-]+/',
      '/<[^>]*>/');
       $repl = array('',
      '-',
      '');
       $url = preg_replace ($find, $repl, $url);
       $url = str_replace ('--', '-', $url);

        return $url;
    }
//----'-------
    private function text( $prefix, $key, array $vars = null )
    {
    }
}


