<?php
/**********************************************************
Plugin Name: Most Rated Blogs
Author: OldFuture
Version: 1.0.0
Website: www.oldfuture-it.com
License: http://www.opensource.org/licenses/bsd-license.php
Copyright (c) 2013,  www.oldfuture-it.com
**********************************************************/
$dashWidget = BOL_ComponentAdminService::getInstance()->addWidgetToPlace(
        BOL_ComponentAdminService::getInstance()->addWidget('MOSTRATEDBLOGS_CMP_MostratedBlogsWidget', false),
        BOL_ComponentAdminService::PLACE_DASHBOARD
);

BOL_ComponentAdminService::getInstance()->addWidgetToPosition($dashWidget, BOL_ComponentAdminService::SECTION_RIGHT);

$mainWidget = BOL_ComponentAdminService::getInstance()->addWidgetToPlace(
        BOL_ComponentAdminService::getInstance()->addWidget('MOSTRATEDBLOGS_CMP_MostratedBlogsWidget', false),
        BOL_ComponentAdminService::PLACE_INDEX
);

BOL_ComponentAdminService::getInstance()->addWidgetToPosition($mainWidget, BOL_ComponentAdminService::SECTION_LEFT);