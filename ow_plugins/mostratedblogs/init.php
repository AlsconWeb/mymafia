<?php
/**********************************************************
Plugin Name: Most Rated Blogs
Author: OldFuture
Version: 1.0.0
Website: www.oldfuture-it.com
License: http://www.opensource.org/licenses/bsd-license.php
Copyright (c) 2013,  www.oldfuture-it.com
**********************************************************/

OW::getRouter()->addRoute(new OW_Route('mostratedblogs.admin', 'admin/plugins/ofratedblogs', "MOSTRATEDBLOGS_CTRL_Admin", 'index'));