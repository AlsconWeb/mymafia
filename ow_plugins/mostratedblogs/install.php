<?php
/**********************************************************
Plugin Name: Most Rated Blogs
Author: OldFuture
Version: 1.0.0
Website: www.oldfuture-it.com
License: http://www.opensource.org/licenses/bsd-license.php
Copyright (c) 2013,  www.oldfuture-it.com
**********************************************************/
$config = OW::getConfig();

if (!$config->configExists('mostratedblogs', 'no_of_blogs'))
{
    $config->addConfig('mostratedblogs', 'no_of_blogs', 7, 'no. blogs to show');
}

BOL_LanguageService::getInstance()->addPrefix('mostratedblogs', 'Most Rated Blogs');
OW::getPluginManager()->addPluginSettingsRouteName('mostratedblogs', 'mostratedblogs.admin');
OW::getLanguage()->importPluginLangs(OW::getPluginManager()->getPlugin('mostratedblogs')->getRootDir().'langs.zip', 'mostratedblogs');