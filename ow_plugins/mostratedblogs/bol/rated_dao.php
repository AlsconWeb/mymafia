<?php
/**********************************************************
Plugin Name: Most Rated Blogs
Author: OldFuture
Version: 1.0.0
Website: www.oldfuture-it.com
License: http://www.opensource.org/licenses/bsd-license.php
Copyright (c) 2013,  www.oldfuture-it.com
**********************************************************/

class MOSTRATEDBLOGS_BOL_RatedDao extends OW_BaseDao
{
    /**
     * Singleton instance.
     *
     * @var MOSTRATEDBLOGS_BOL_RatedDao
     */
    private static $classInstance;

    /**
     * Constructor.
     */
    protected function __construct()
    {
        parent::__construct();
    }

    /**
     * Returns an instance of class.
     *
     * @return MOSTRATEDBLOGS_BOL_RatedDao
     */
    public static function getInstance()
    {
        if ( self::$classInstance === null )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }
	
	public function getDtoClassName()
    {
        return 'MOSTRATEDBLOGS_BOL_Rated';
    }
    
	public function getTableName()
    {
        return OW_DB_PREFIX . 'blogs_post';
    }
	
	public function getBlogRateTable()
	{
		return OW_DB_PREFIX . 'base_rate';
	}
	
	public function findTopRatedList($limit)
    {	
        $query = "SELECT p.*, TRIM(TRAILING '.00' FROM ROUND(SUM(r.score) / COUNT(r.id), 2)) AS t
				FROM ".$this->getTableName()." as p
				LEFT JOIN ".$this->getBlogRateTable()." as r ON r.entityType = 'blog-post'
				AND p.id = r.entityId 
				WHERE p.isDraft = 0
				AND r.score > 0
				GROUP BY p.id
				ORDER BY t DESC
				LIMIT ".$limit."";
		
        return $this->dbo->queryForObjectList($query, MOSTRATEDBLOGS_BOL_RatedDao::getInstance()->getDtoClassName(), array());
    }
}