<?php
/**********************************************************
Plugin Name: Most Rated Blogs
Author: OldFuture
Version: 1.0.0
Website: www.oldfuture-it.com
License: http://www.opensource.org/licenses/bsd-license.php
Copyright (c) 2013,  www.oldfuture-it.com
**********************************************************/

class MOSTRATEDBLOGS_BOL_Rated extends OW_Entity
{
   /***None. No database insertion needed for the plugin except config settings, defined in the install.php file ***/  
}