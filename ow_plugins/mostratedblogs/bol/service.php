<?php
/**********************************************************
Plugin Name: Most Rated Blogs
Author: OldFuture
Version: 1.0.0
Website: www.oldfuture-it.com
License: http://www.opensource.org/licenses/bsd-license.php
Copyright (c) 2013,  www.oldfuture-it.com
**********************************************************/

class MOSTRATEDBLOGS_BOL_Service
{
   private static $classInstance;
    
    /**
     * Returns class instance
     *
     * @return MOSTRATEDBLOGS_BOL_Service
     */
    public static function getInstance()
    {
        if ( null === self::$classInstance )
        {
            self::$classInstance = new self();
        }

        return self::$classInstance;
    }
    private function __construct()
    {
        $this->ratedDao = MOSTRATEDBLOGS_BOL_RatedDao::getInstance();
    }
   
}