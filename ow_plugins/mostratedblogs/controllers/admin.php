<?php
/**********************************************************
Plugin Name: Most Rated Blogs
Author: OldFuture
Version: 1.0.0
Website: www.oldfuture-it.com
License: http://www.opensource.org/licenses/bsd-license.php
Copyright (c) 2013,  www.oldfuture-it.com
**********************************************************/

class MOSTRATEDBLOGS_CTRL_Admin extends ADMIN_CTRL_Abstract
{
    private function setMenu()
    {
        $language = OW::getLanguage();        
        $item = new BASE_MenuItem();
        $item->setLabel($language->text('mostratedblogs', 'general_settings'));
        $item->setUrl(OW::getRouter()->urlForRoute('mostratedblogs.admin'));
        $item->setKey('settings');
        $item->setOrder(0);
        $menu = new BASE_CMP_ContentMenu($item);
        return $menu;
    }
	
	public function index()
	{
		$service = MOSTRATEDBLOGS_BOL_Service::getInstance();
		$language = OW::getLanguage();
		
		$this->addComponent('menu', $this->setMenu());
		$this->setPageHeading($language->text('mostratedblogs', 'admin_plugin_heading'));
		
		$form = new Form('form-config');
        $this->addForm($form);
		
		$counter = new TextField('counter');
        $counter->setRequired(true);
        $counter->addValidator(new IntValidator(1, 12));
        $counter->setLabel($language->text('mostratedblogs', 'no_of_blogs'));
        $form->addElement($counter);
		
		$submit = new Submit('save');
        $submit->setLabel($language->text('mostratedblogs', 'save'));
        $form->addElement($submit);
		$form->getElement('counter')->setValue(OW::getConfig()->getValue('mostratedblogs', 'no_of_blogs'));
		
		if (OW::getRequest()->isPost() && $form->isValid($_POST))
        {
        	$formValues = $form->getValues();
        	OW::getConfig()->saveConfig('mostratedblogs', 'no_of_blogs', (int) $formValues['counter']);
        	OW::getFeedback()->info($language->text('mostratedblogs', 'settings_saved'));
        	$this->redirect();
        }
	}
}
