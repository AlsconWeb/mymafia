<?php
/**********************************************************
Plugin Name: Most Rated Blogs
Author: OldFuture
Version: 1.0.0
Website: www.oldfuture-it.com
License: http://www.opensource.org/licenses/bsd-license.php
Copyright (c) 2013,  www.oldfuture-it.com
**********************************************************/

class MOSTRATEDBLOGS_CMP_MostratedBlogsWidget extends BASE_CLASS_Widget
{
   public function __construct( BASE_CLASS_WidgetParameter $params )
    {
        parent::__construct();
		$language = OW::getLanguage();
		$service = MOSTRATEDBLOGS_BOL_RatedDao::getInstance();
		$tblogs = $service->findTopRatedList(OW::getConfig()->getValue('mostratedblogs', 'no_of_blogs'));

        if(empty($tblogs)){
            $this->setVisible(false);
        	return;
        }
		$userIdList = array();
		foreach($tblogs as $t){
			array_push($userIdList, $t->authorId);
		}

        $url = OW::getRouter()->urlForRoute('blogs');
		$avatars = BOL_AvatarService::getInstance()->getDataForUserAvatars($userIdList, true, true, true, true);
		$this->assign('tblogs', $tblogs);
		$this->assign('url', $url);
		$this->assign('avatars', $avatars);
    }

    public static function getStandardSettingValueList()
    {
        return array(
            self::SETTING_WRAP_IN_BOX => true,
            self::SETTING_SHOW_TITLE => true,
            self::SETTING_ICON => self::ICON_STAR,
            self::SETTING_TITLE => OW::getLanguage()->text('mostratedblogs', 'title_bar')
        );
    }
}
