(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.yellow_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E5D900").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.yellow_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.red_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF5232").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.red_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.purple_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF5D89").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.purple_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.lightp_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F9D0D4").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.lightp_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.green_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00BE1B").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.green_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


(lib.blue_block = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#596BBD").s().p("AhCBDIAAiFICFAAIAACFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.blue_block, new cjs.Rectangle(-6.7,-6.6,13.4,13.4), null);


// stage content:

(lib.logo_info = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var i = 0;
		var pairs=[];
		while (i < 200) {
		var r=Math.round(Math.random()*5);
		var bl;
			
		  if (r==0)
		  {
			  bl=new lib.blue_block();
		  }
		  if (r==1)
		  {
			  bl=new lib.green_block();
		  }
		  if (r==2)
		  {
			  bl=new lib.lightp_block();
		  }
		  if (r==3)
		  {
			  bl=new lib.purple_block();
		  }
		  if (r==4)
		  {
			  bl=new lib.red_block();
		  }
		  if (r==5)
		  {
			  bl=new lib.yellow_block();
		  }
		bl.x=0+Math.random()*1200;
		bl.y=0+Math.random()*1200;
		bl.dvx=300-Math.random()*600;
		bl.dvy=300-Math.random()*600;
		bl.rot_speed=1-Math.random()*2;
		bl.f=Math.random()*2000;
		bl.fs=0.001+Math.random()/1000;
		bl.fall_speed_a=0.1+Math.random();
		bl.fall_speed=0;
		
		pairs.push(bl);
		  
		this.addChild(bl);
		
		i++;
		}
		
		
		var f=0;
		var fall_speed=0;
		var fall_anim;
		var fall_up=false;
		var go_anim;
		
		scroll_anim_func_up=function ()
		{
		       clearInterval(fall_anim);
		    
			clearInterval(go_anim);
		go_anim=setInterval(pair_control,1);		
		}
		scroll_anim_func=function ()
		{
		    clearInterval(fall_anim);
		    
			clearInterval(go_anim);
		    fall_anim=setInterval(down_all,1);
		}
		scroll_anim_func_up();
		function down_all()
		{
		
		var all_fall=true;
		pairs.forEach(function(item, i, arr) 
		{
		  item.y+=item.fall_speed;
		  item.fall_speed+=item.fall_speed_a;
		  item.x+=0.5-Math.random();
		  if (item.y<2000)
		  {
			  all_fall=false;
		  }
		});	
		if (all_fall==true)
		{
			
		pairs.forEach(function(item, i, arr) 
		{
		  item.fall_speed=0;
		});	
			clearInterval(fall_anim);
		}
		}
		function pair_control()
		{
			var R=300;
		    f+=0.01;
			
		pairs.forEach(function(item, i, arr) {
		  item.f+=item.fs;
		  item.rotation+=item.rot_speed;
		  item.x=600+item.dvx+R*Math.cos(item.f);
		  item.y=300+R*Math.sin(item.f);
		});
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;
// library properties:
lib.properties = {
	id: 'B538056CE7A67D45BD58ED4698A763DF',
	width: 1200,
	height: 1200,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B538056CE7A67D45BD58ED4698A763DF'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;